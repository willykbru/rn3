import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  //align
  centerFlexContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerItemContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerContent:{
    justifyContent: 'center',
  },
  centerItem:{
    alignItems: 'center',
  },
  flexEndContent:{
    justifyContent: 'flex-end',
  },
  flexEndSelf:{
    alignSelf: 'flex-end',
  },
  spaceBetween:{
    justifyContent:'space-between',
  },
  spaceAround:{
     justifyContent: 'space-around',
  },
  //elevation
  elev1:{
     elevation: 1,
  },
  elev2:{
     elevation: 2,
  },
  elev3:{
     elevation: 3,
  },
  elev4:{
     elevation: 4,
  },
  elev5:{
     elevation: 5,
  },
  //border
  radius2:{
    borderRadius:2,
  },
  radius3:{
    borderRadius: 3,
  },
  radius5:{
    borderRadius: 5,
  },
  radius20:{
    borderRadius:20,
  },
  radius50:{
    borderRadius: 50,
  },
  border1:{
    borderWidth: 1,
  },
  borderbottom1:{
    borderBottomWidth:1,
  },
  borderBotP5:{
    borderBottomWidth:.5,
  },
  borderTop1:{
    borderTopWidth:1,
  },
  borderTransparent:{
    borderColor: 'transparent'
  },
  borderColor2553:{
    borderColor: 'rgba(255,0,0,0.3)',
  },
  borderColorSilver:{
    borderColor: 'rgba(192,192,192,0.3)',
  },
  borderColorGrey:{
    borderColor: 'grey',
  },
  borderColorffe5e5:{
    borderColor: '#ffe5e5',
  },
  borderColorTopWhiteOp8:{
    borderTopColor:'rgba(255,255,255, 0.8)',
  },
  borderColorTopSilver:{
    borderTopColor:'#f3f3f3',
  },
  borderColorTopGrey:{
    borderTopColor:'grey',
  },
  borderColorBotSilver:{
    borderBottomColor: '#f3f3f3',
  },
  borderColorBotGrey:{
    borderBottomColor: '#A9A9A9',
  },
  //color
  bgRedE5: {
    backgroundColor: '#e50000',
  },
  bgRedFF: {
    backgroundColor: '#ff0000',
  },
  bgRed: {
    backgroundColor: 'red',
  },
  bgSilver:{
    backgroundColor: '#e6e6e6',
  },
  bgRedOp6:{
    backgroundColor:'rgba(204,0,0,0.6)',
  },
  bgBlackOp6:{
    backgroundColor: 'rgba(0,0,0, 0.6)',
  },
  bgWhite:{
    backgroundColor: 'rgb(255,255,255)',
  },
  bgWhiteOp4:{
    backgroundColor: 'rgba(255,255,255, 0.4)',
  },
  bgWhiteOp6:{
    backgroundColor: 'rgba(255,255,255, 0.6)',
  },
  bgWhiteOp8:{
    backgroundColor: 'rgba(255,255,255,0.8)',
  },
  colorWhite:{
    color: 'white',
  },
  colorRed:{
    color: 'red',
  },
  colorGrey:{
    color: 'grey',
  },
  //direction
  directionColumn:{
    flexDirection:'column',
  },
  directionRow:{
    flexDirection:'row',
  },
  //font
  font10:{
    fontSize: 10,
  },
  font11:{
    fontSize: 11,
  },
  font12:{
    fontSize: 12,
  },
  font14:{
    fontSize: 14,
  },
  font16:{
    fontSize: 16,
  },
  font18:{
    fontSize: 18,
  },
  font20:{
    fontSize: 20,
  },
  baseText: {
	   fontFamily: 'Pakenham_Rg_Regular',
     fontSize:16,
   },
   baseText10: {
	   fontFamily: 'Pakenham_Rg_Regular',
     fontSize:10,
   },
  //margin
  bottom5:{
    marginBottom: 5,
  },
  bottom10:{
    marginBottom: 10,
  },
  bottom20:{
    marginBottom: 20,
  },
  left2:{
    marginLeft: 4,
  },
  left4:{
    marginLeft: 4,
  },
  left5:{
    marginLeft: 5,
  },
  left10: {
    marginLeft: 10,
  },
  left15: {
    marginLeft: 15,
  },
  left20: {
    marginLeft: 20,
  },
  left30: {
    marginLeft: 30,
  },
  left40: {
    marginLeft: 40,
  },
  right5: {
    marginRight: 5,
  },
  right10: {
    marginRight: 10,
  },
  right20: {
    marginRight: 20,
  },
  right40: {
    marginRight: 40,
  },
  top2:{
      marginTop: 2,
  },
  top5:{
    marginTop: 5,
  },
  top10:{
    marginTop: 10,
  },
  top15:{
    marginTop: 15,
  },
  top20:{
    marginTop: 20,
  },
  top30:{
    marginTop: 30,
  },
  top40:{
    marginTop: 40,
  },
  top50:{
    marginTop: 50,
  },
  marBottom: {
    marginBottom: 20,
  },
  marTop: {
    marginTop: 10,
  },
  bottom5:{
	marginBottom: 5,
  },
  bottom10:{
	marginBottom: 10,
  },
  //resize mode
  resizeContain:{
	resizeMode: 'contain',
  },
  //font style
  bold: {
    fontWeight: 'bold',
  },
  italic:{
    fontStyle:'italic',
  },
  strike:{
    textDecorationLine: 'line-through',
  },
  underline:{
    textDecorationLine:'underline',
  },
  //flex
  flex1:{
    flex: 1,
  },
  flex2:{
    flex: 2,
  },
  //padding
  padlr5:{
	paddingLeft:5,
	paddingRight:5,
  },
  padv0:{
    paddingVertical: 0,
  },
  pad5:{
    padding: 5,
  },
  pad10:{
    padding: 10,
  },
  pad20:{
    padding: 20,
  },
   padTop5:{
    paddingTop: 5,
  },
  padTop20:{
    paddingTop: 20,
  },
  padBot20:{
    paddingBottom: 20,
  },
  //size
  height25: {
    height: 25,
  },
  height30:{
    height: 30,
  },
  height35:{
    height: 35,
  },
  height40:{
    height: 40,
  },
  height45:{
    height: 45, 
  },
  height50:{
    height: 50
  },
  height70:{
    height:70,
  },
  height75:{
    height:75,
  },
  height100:{
    height:100,
  },
  height150:{
    height:150,
  },
  height200:{
    height:200,
  },
  height300:{
    height: 300
  },
  height10p: {
    height: '10%',
  },
  height15p: {
    height: '15%',
  },
  height20p: {
    height: '20%',
  },
  height25p:{
    height: '25%',
  },
  height30p:{
    height: '30%',
  },
  height35p:{
    height: '35%',
  },
  height40p:{
    height: '40%',
  },
  height50p:{
    height: '50%',
  },
  height60p:{
    height: '60%',
  },
  height70p:{
    height: '70%',
  },
  height80p:{
    height: '80%',
  },
  height100p:{
    height: '100%',
  },
  width25:{
    width: 25,
  },
  width48:{
    width: 48,
  },
  width40:{
    width: 40,
  },
  width50:{
    width: 50,
  },
  width60:{
    width: 60,
  },
  width65:{
    width: 65,
  },
  width70:{
    width: 70,
  },
  width80:{
    width: 80,
  },
  width100:{
    width: 100,
  },
  width150:{
    width: 150,
  },
  width200:{
    width: 200
  },
  width300:{
    width: 300
  },
  width400:{
    width: 400
  },
  width5p:{
    width: '5%',
  },
  width10p:{
    width: '10%',
  },
  width15p: {
    width: '15%',
  },
  width20p:{
    width: '20%',
  },
  width25p:{
    width: '25%',
  },
  width30p:{
    width: '30%',
  },
  width32p:{
    width: '32%',
  },
  width35p:{
    width: '35%',
  },
  width40p:{
    width: '40%',
  },
  width45p:{
    width: '45%',
  },
  width48p:{
	width: '48%',
  },
  width50p:{
    width:'50%',
  },
  width55p:{
    width: '55%',
  },
  width60p:{
    width:'60%',
  },
  width65p:{
    width:'65%',
  },
  width70p: {
    width: '70%',
  },
  width80p:{
    width: '80%',
  },
  width85p:{
    width: '85%',
  },
  width88p:{
    width: '88%',
  },
  width90p:{
    width: '90%',
  },
  width95p:{
    width: '95%',
  },
  width96p:{
    width: '96%',
  },
  width98p:{
    width: '98%',
  },
  width100p:{
    width: '100%',
  },
  fullArea:{
    width: '100%',
    height:'100%',
  },
  //positioning
  absolute:{
    width: '100%',
    height:'100%',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  posAbsolute:{
	position: 'absolute',
  },
  //index
  zindex0:{
	zIndex : 0,
  },
  zindex1:{
	zIndex : 1,
  },
  //textalign
  alignLeft:{
	  textAlign: 'left',
  },
  alignCenter:{
	  textAlign: 'center',
  },
  alignRight:{
	  textAlign: 'right',
  },
  //others
  root: {
    backgroundColor: 'white',
  },
  rootContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  empty: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'silver',
  },
  icon: {
    marginBottom: 16
  },
  backIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  borderDetailBeli:{
    width: '95%',
    borderWidth: 1,
    borderColor: '#ffe5e5',
    borderRadius: 2,
  },
  button30p:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'rgb(255,255,255)',
    borderRadius:15,
    width: '30%',
    justifyContent: 'center',
  },
  containerPhoto: {
    flex: 1,
    width: '100%',
    borderRadius: 5,
    backgroundColor: '#ecf0f1',
    marginTop:10,
  },
  defaultButton:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'rgba(204,0,0,0.8)',
    borderRadius:15,
    width: '80%',
    justifyContent: 'center',
  },
  defaultButtonWhite:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'rgba(255,255,255,0.8)',
    borderRadius:15,
    width: '80%',
    justifyContent: 'center',
  },
  defaultButtonText:{
    color:'#fff',
    textAlign:'center',
  },
  defaultButtonTextBlack:{
    color:'#000',
    textAlign:'center',
  },
  defaultButtonTextRed:{
    color:'rgb(204,0,0)',
    textAlign:'center',
  },
  detailPribadiMainContainer :{
    justifyContent: 'center',
    width: '90%',
  },
   detailPribadiMainContainer100 :{
    justifyContent: 'center',
    width: '100%',
  },
  verifikasiMainContainer :{
    justifyContent: 'center',
    margin: 10,
    flex:1,
  },
  loginMainContainer :{
    justifyContent: 'center',
    alignItems:'center',
    flex:1,
    width: '80%',
    borderRadius: 10,
    borderWidth: .5,
  },
  textTitle:{
    color: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  homeMenuButton: {
    borderWidth: 1,
    borderColor: '#bdbdbd',
    borderRadius: 3,
    width: '33.3%',
    height: 100
  },
  homeMenuContents: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  homeMenuImageSize: {
    width: 70,
    height: 70,
  },
  kodeVoucher:{
    backgroundColor:'rgba(204,0,0,0.6)',
    borderRadius:5,
    justifyContent: 'center',
  },
  kodeVoucherTInput:{
    height: 40,
    borderBottomWidth:.5,
    borderBottomColor: '#444444',
    width:'35%',
  },
  modalButton: {
    backgroundColor: '#e50000',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(229,229,229, 0.5)',
  },
  modalContent: {
   backgroundColor: 'white',
   padding: 22,
   justifyContent: 'center',
   alignItems: 'center',
   borderRadius: 4,
   borderColor: 'rgba(229,229,229, 0.5)',
  },
  pickerView:{
    borderBottomWidth:.5,
    borderBottomColor: 'white',
    width: '80%',
    marginBottom: 10,
  },
  pickerViewTr:{
    width: '80%',
    marginBottom: 10,
  },
  pickerViewFullBlack:{
    borderBottomWidth:.5,
    marginBottom: 10,
  },
  pickerViewBlack:{
    borderBottomWidth:.5,
    width: '80%',
    marginBottom: 10,
  },
  pickerViewSilver:{
    borderBottomWidth:.5,
    width: '80%',
    marginBottom: 10,
    borderBottomColor: 'silver'
  },
  searchInsurancebutton: {
    height: 50,
    backgroundColor: '#ff0000',
    borderColor:'#d43f3a',
    alignSelf: 'stretch',
    marginTop: 10,
    justifyContent: 'center'
  },
  statusBar: {
    backgroundColor: '#ff4c4c',
    height:20,
  },
  btnLogin:{
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    backgroundColor:'#e50000',
    borderRadius:10,
    width: '80%'

  },
  btnForgotPassword:{
    paddingTop:15,
    paddingBottom:15,
    backgroundColor:'#e50000',
    borderRadius:5,
    width: '100%'
  },
  btnUpload:{
    paddingTop:3,
    paddingBottom:3,
    paddingRight:4,
    paddingLeft:4,
    backgroundColor:'#e50000',
    borderRadius:20,
    width: '100%'
  },
  btnLoginText:{
      color:'#fff',
      textAlign:'center',
  },
  textComponentStyle: {
    fontSize: 20,
     color: "#000",
     textAlign: 'center',
     marginBottom: 15
  },
  textInputStyleClass: {
    textAlign: 'center',
    marginBottom: 7,
    height: 40,
    backgroundColor: 'rgba(192,192,192,0.3)',
    borderRadius: 5 ,
  },
  loginInputStyle: {
    textAlign: 'center',
    marginBottom: 7,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    width: '90%',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
  fontpakenham:{
	fontFamily: 'Pakenham_Rg_Regular',
  },
  fontContinuum:{
	fontFamily: 'Continuum_Medium',
  },
  fontBlairMdITC:{
	fontFamily: 'BlairMdITC_TT_Medium',
  }
});

export default styles;
