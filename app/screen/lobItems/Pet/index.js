import React, {Component} from 'react';
import {View, TouchableOpacity,StyleSheet, Picker, Text, TextInput, Button,Alert} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

class Pet extends React.Component {
  constructor(props) {
        super(props);
          this.nextNav = this.nextNav.bind(this);
          this.prevNav = this.prevNav.bind(this);

        this.state = {
            objectCoverage: '',
            gender: '',
            animalRace:'',
            petPrice:'',
            animalBirthdate:'',
        };
    }

  prevNav(prevScreen){
      this.props.resetRoute(prevScreen);
    }

  nextNav(screen) {
        this.props.resetRoute(screen,
          {
            prevScreen: "Pet",

        });
    }

  render(){
    const { prevScreen } = this.props.navigation.state.params;


    return(
      <Container>
        <Header style={{backgroundColor: '#ff0000'}}>
          <Left>
          <TouchableOpacity style={styles.menuIcon} onPress={() => this.prevNav(prevScreen)}>
            <Icon style={{color: 'white'}}  name='arrow-round-back' />
          </TouchableOpacity>
        </Left>
        <Body><Text style={{color: 'white'}}>Asuransi Hewan Peliharaan</Text></Body>
        </Header>
        <Card>
          <CardItem>
        <Content>
      <View>

        <Text>Objek Pertanggungan </Text>
        <Picker
          mode= 'dialog'
          selectedValue={this.state.objectCoverage}
          onValueChange={(itemValue, itemIndex) => this.setState({objectCoverage: itemValue})}>
          <Picker.Item label='Pilih' value='0' />
        <Picker.Item label='Anjing' value='Anjing' />
        <Picker.Item label='Kucing' value='Kucing' />
        </Picker>

        <Text>Jenis Kelamin </Text>
        <Picker
          mode='dialog'
          selectedValue={this.state.gender}
          onValueChange={(itemValue, itemIndex) => this.setState({gender: itemValue})}>
          <Picker.Item label='Pilih' value='0' />
          <Picker.Item label="Jantan" value="Jantan" />
          <Picker.Item label="Betina" value="Betina" />
        </Picker>

        <Text>Harga Pertanggungan </Text>

        <Text>Jenis / Ras Hewan </Text>
        <Picker
          mode='dialog'
          selectedValue={this.state.animalRace}
          onValueChange={(itemValue, itemIndex) => this.setState({animalRace: itemValue})}>
          <Picker.Item label='Pilih' value='0' />
          <Picker.Item label="A" value="A" />
          <Picker.Item label="B" value="B" />
          <Picker.Item label="C" value="C" />
        </Picker>

        <Text>Tanggal Lahir Hewan</Text>
        <DatePicker
         style={{width: 200}}
         date={this.state.animalBirthdate}
         mode="date"
         placeholder="Select Date To"
         format="YYYY-MM-DD"
         minDate="1970-01-14"
         maxDate="2018-07-01"
         confirmBtnText="Confirm"
         cancelBtnText="Cancel"
         customStyles={{
           dateIcon: {
             position: 'absolute',
             left: 0,
             top: 4,
             marginLeft: 0
           },
           dateInput: {
             marginLeft: 36
           }
           // ... You can check the source to find the other keys.
         }}
         onDateChange={(animalBirthdate) => {this.setState({animalBirthdate: animalBirthdate})}}
        />


      <View style={styles.marTop}></View>

    </View>
  </Content>
</CardItem>
</Card>
    <Footer  style={ styles.footers }>
      <View style={ styles.footerView }>
        <Text style={ styles.footerText }>Jagain.com | Copyright @ 2018</Text>
      </View>
    </Footer>

  </Container>

    );
  }

}
const styles = StyleSheet.create({
  menuIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
  },
  headers: {
    backgroundColor: '#ff0000',
  },
  marTop:{
    marginTop: 10,
  },
  banner: {
    marginBottom: 20,
  },
  button: {
    height: 50,
    backgroundColor: '#ff0000',
    borderColor:'#d43f3a',
    alignSelf: 'stretch',
    marginTop: 10,
    justifyContent: 'center'
  },
  contents: {
    flex: 1,
    flexDirection: 'row',
    marginTop:40
  },
  footers: {
    backgroundColor: '#ff0000',
  },
  whCol:{
    color: 'white',
  },
  footerView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  footerText: {
    color: 'white'
  },
  button: {
    borderWidth: 1,
    borderColor: '#bdbdbd',
    borderRadius: 8,
    width: '33.3%',
    height: 80
  },
});

function mapStateToProps(state) {
  return {
    navReducer: state.navReducer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Pet);
