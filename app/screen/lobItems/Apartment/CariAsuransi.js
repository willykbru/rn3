import React, {Component} from 'react';
import {View, BackHandler, Modal, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ModalFilterPicker from 'react-native-modal-filter-picker';


import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiApartment extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      isVisible: false,
    }
  }

  toListAsuransi(apartmentState, dataApartment, lob) {
    if(!dataApartment.risk_address){
     errorMessage = 'Lokasi Resiko Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!apartmentState.province_id || apartmentState.province_id == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!apartmentState.district_id || apartmentState.district_id == 'Pilih'){
      errorMessage = 'Kabupaten / Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!apartmentState.kecamatan_id || apartmentState.kecamatan_id == 'Pilih'){
     errorMessage = 'Kecamatan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!apartmentState.lantai || apartmentState.lantai == 'Pilih'){
      errorMessage = 'Fungsi Bangunan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!apartmentState.content_tsi){
      errorMessage = 'Harga Isi Bangunan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else{
      const body = Object.assign({}, apartmentState);

      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }

  provinsilist(apartmentLabelState, lob){
    const title = 'Provinsi';
    const value = apartmentLabelState.label_provinsi;
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  districtlist(apartmentLabelState, lob){
    const title = 'Kabupaten / Kota';
    const value = apartmentLabelState.label_district;
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  kecamatanlist(apartmentLabelState, lob){
    const title = 'Kecamatan';
    const value = apartmentLabelState.label_kecamatan;
    const options = this.props.listKecamatan;
    const type = Object.assign({},{state: 'kecamatan_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  kelurahanlist(apartmentLabelState, lob){
    const title = 'Kelurahan';
    const value = apartmentLabelState.label_kelurahan;
    const options = this.props.listKelurahan;
    const type = Object.assign({},{state: 'kelurahan_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  lantailist(apartmentLabelState, lob){
    const title = 'Fungsi Bangunan';
    const value = apartmentLabelState.label_lantai;
    const options = this.props.listOccupation;
    const type = Object.assign({},{state: 'lantai', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  contenttsitinput(apartmentState){
    const value = apartmentState.content_tsi;
    var content_tsi = appFunction.numToCurrency(value);
	if(value == 0){
		content_tsi = '';
	}
    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Harga Isi Bangunan
        </Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={[styles.font16, styles.height40, styles.baseText]}
          keyboardType='numeric'
          onChangeText={(value) => this.changeharga(value, 'content_tsi')}
          value={`${content_tsi}`}
		  placeholder = '0'
        />
      </View>
    );
  }

  riskaddresstinput(dataApartment){
    const value = dataApartment.risk_address;
    return(
	<JagainTextInput
		  title = 'Lokasi Resiko'
          value={value}
          onChangeText={(value) => this.props.setRiskAddressApartment(appFunction.dotalphaNumericOnly(value))}                 
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  multiline = {true}
          numberOfLines = {4}
	  />
    );
  }

  changeharga(value, tipe){
    var harga = value;
	if(value == ''){
		harga = 0;
	}

    if(value !== "" && value !== undefined && value !== null){
      harga = appFunction.currencyToNum(value);
    }
      this.props.setContentTsiApartment(harga);
  }


  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };

  componentWillReceiveProps(NextProps){
    const {apartmentState} = NextProps;
    const oldapartmentState = this.props.apartmentState;
    if(apartmentState != oldapartmentState){
      if(apartmentState.province_id != oldapartmentState.province_id){
        if(apartmentState.province_id && apartmentState.province_id != 'Pilih'){
          this.props.resetListDistrict();
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetDistrictApartment();
          this.props.resetKecamatanApartment();
          this.props.resetKelurahanApartment();
          this.props.getDistrict(apartmentState.province_id);
        }else if(!apartmentState.province_id || apartmentState.province_id  == 'Pilih'){
          this.props.resetListDistrict();
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetDistrictApartment();
          this.props.resetKecamatanApartment();
          this.props.resetKelurahanApartment();
        }
      }
      if(apartmentState.district_id != oldapartmentState.district_id){
        if(apartmentState.district_id && apartmentState.district_id!= 'Pilih'){
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetKecamatanApartment();
          this.props.resetKelurahanApartment();
          this.props.getKecamatan(apartmentState.district_id);
        }else if(!apartmentState.district_id || apartmentState.district_id  == 'Pilih'){
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetKecamatanApartment();
          this.props.resetKelurahanApartment();
        }
      }
      if(apartmentState.kecamatan_id != oldapartmentState.kecamatan_id){
        if(apartmentState.kecamatan_id && apartmentState.kecamatan_id!= 'Pilih'){
          this.props.resetListKelurahan();
          this.props.resetKelurahanApartment();
          this.props.getKelurahan(apartmentState.kecamatan_id);
        }else if(!apartmentState.kecamatan_id || apartmentState.kecamatan_id  == 'Pilih'){
          this.props.resetListKelurahan();
          this.props.resetKelurahanApartment();
        }
      }
    }
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    this.props.getProvinsi();
	this.props.resetListDistrict();
	this.props.resetListKecamatan();
	this.props.resetListKelurahan();
    this.props.getOkupasiApartment();
	
	if(this.props.apartmentState.province_id && this.props.apartmentState.province_id != 'Pilih'){
		this.props.getDistrict(this.props.apartmentState.province_id);
	}else if(this.props.apartmentState.district_id && this.props.apartmentState.district_id!= 'Pilih'){
		this.props.getKecamatan(this.props.apartmentState.district_id);	
	}else if(this.props.apartmentState.kecamatan_id && this.props.apartmentState.kecamatan_id!= 'Pilih'){
		this.props.getKelurahan(this.props.apartmentState.kecamatan_id);	
	}

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {apartmentState} = this.props;
    const apartmentLabelState = this.props.apartmentLabelState;
    const {dataApartment} = this.props;
    const lob = 'Apartment';
    //const {listPlatKendaraan} = this.props;
    //console.log(apartmentState);

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
      
         
       
       
        {this.riskaddresstinput(dataApartment)}
        {this.provinsilist(apartmentLabelState, lob)}
        {this.districtlist(apartmentLabelState, lob)}
        {this.kecamatanlist(apartmentLabelState, lob)}
        {this.kelurahanlist(apartmentLabelState, lob)}
        {this.lantailist(apartmentLabelState, lob)}
        {this.contenttsitinput(apartmentState)}
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(apartmentState, dataApartment, lob)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    visible: state.visible,
    apartmentState: state.apartmentState,
    dataApartment:state.dataApartment,
    apartmentLabelState: state.apartmentLabelState,
    listProvinsi: state.listProvinsi,
    listDistrict: state.listDistrict,
    listKecamatan: state.listKecamatan,
    listKelurahan: state.listKelurahan,
    listOccupation: state.listOccupation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiApartment);
