import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

import LoadingPage from '../../../components/loading/LoadingPage';
import SubmitButton from '../../../components/button/SubmitButton';

//import { TextInputMask } from 'react-native-masked-text'
import PopoverTooltip from 'react-native-popover-tooltip';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class ListAsuransiApartment extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      modalVisible:false,
    }
  }

  usetooltip(text){
    var {height, width} = Dimensions.get('window');
    return(
      <PopoverTooltip
        buttonComponent={
          <Icon name="ios-information-circle-outline" style={styles.colorRed} />
        }
        items={[
          {
            label: text,
            onPress: () => {}
          }
        ]}
        tooltipContainerStyle = {{ width: width*.8, }}
        // animationType='timing'
        // using the default timing animation
       />
     );
  }
  
  lihatBenefit(listAsuransi){
	  const {travelState} = this.props;
	  var benefit_url = '';
	  var dataBenefit = null;
	   benefit_url =  appFunction.webJagain + 'apartemen/mbenapr?is=' + listAsuransi.insurance_id + '&pc=' + listAsuransi.package_id + '&lg=id';
						  	  
	  dataBenefit = Object.assign({},{
		  insurance_name: listAsuransi.insurance_name,
		  insurance_id: listAsuransi.insurance_id,
		  package_name: listAsuransi.package_name,
		  benefit_url,
	  });
	  
	  this.props.setDataBenefit(dataBenefit);
	  this.props.navigate('Benefit');
  }

  doBeli(listAsuransi){
    this.props.setAsuransiApartment(listAsuransi);
    this.toNextScreen();
  }

  toNextScreen(){
    if(this.props.status.isLogin == false){
      this.props.navigate('Login');
    }else{
      this.props.navigate('DetailBeli');
    }
  }

  openModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:true});
    }
  }

  closeModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:false});
    }
  }

  renderListAsuransi(apartmentState, listAsuransi){
    var arrListAsuransi = [];
    //const { listAsuransi } = this.state;
    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
    }
    //console.log(listAsuransi.length);

    var asuransi = [];
    var totalPremi = [];
    var premiDasar = [];
    var premiEQVET = [];
    var premiFLOOD = [];
    var packetName = [];
    var packetID = [];
    var discount = [];
    var showTotalPremi = [];
	
	const discsrc = {
      '5'  : require('../../../assets/icons/discount/disc-5.png'),
      '10' : require('../../../assets/icons/discount/disc-10.png'),
      '15' : require('../../../assets/icons/discount/disc-15.png'),
      '20' : require('../../../assets/icons/discount/disc-20.png'),
	  '25' : require('../../../assets/icons/discount/disc-25.png'),
    }

    if(listAsuransi.length > 0){
      for (var i = 0; i < listAsuransi.length; i++) {
         asuransi.push(listAsuransi[i].insurance_name);
         totalPremi.push(listAsuransi[i].total_premi);
         premiDasar.push(listAsuransi[i].premi_dasar);
         premiEQVET.push(listAsuransi[i].premi_eqvet);
         premiFLOOD.push(listAsuransi[i].premi_flood);
         packetName.push(listAsuransi[i].package_name);
         packetID.push(listAsuransi[i].package_id);
         discount.push(listAsuransi[i].disc_insurance);
      }

       for(let i = 0; i < asuransi.length; i++){
         if(discount[i] > 0){
           showTotalPremi.push(
             <View key = {i}>
               <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}Total:{"\n"}{"\n"}
                </Text>
                <View style={styles.directionColumn}>
                  <Text style={[styles.font12, styles.strike]}>
                    {"\n"}Rp. {totalPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
                  </Text>
                  <Text style={[styles.font14, styles.bold, styles.baseText]}>
                    Rp. {(totalPremi[i]-(totalPremi[i]*discount[i])/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}{"\n"}{"\n"}
                  </Text>
                </View>
              </View>
            </View>
          );
        }else {
          showTotalPremi.push(
            <View key = {i}>
               <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}Total:{"\n"}{"\n"}
                </Text>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}{"\n"}Rp. {totalPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}{"\n"}{"\n"}
                </Text>
                <Text>{"\n"}{"\n"}</Text>
              </View>
            </View>
          );
        }

        arrListAsuransi.push(
          <View style={[styles.centerItemContent, styles.top10]} key = {i}>
           {discount[i] ? 
			<View style={[styles.directionRow, styles.flexEndSelf, {position:'absolute', top:-15, zIndex: 1}]}>
			  <View style={[styles.directionRow]}> 
				<View>
				 <Image style={[styles.width70, styles.height70]} source={discsrc[discount[i].toString()]} />
				</View>
			  </View>
			</View>
			: null}
	
              <View style={[styles.bgWhite, styles.radius5, styles.bottom10, styles.top10, styles.pad5, { zIndex: 0}]}> 
              <View style={[styles.directionRow, styles.left5]}>
                <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                  <TouchableHighlight>
                    <View style={styles.centerItemContent}>
                       <View style={[styles.centerItemContent, styles.height50]}>
                           <Image source={asimgsrc[asuransi[i]]} style={[styles.height25, styles.width100, styles.resizeContain]}/>
                        </View>
                        <Text style={[styles.font10, styles.bold, styles.baseText]}>{"\n"}{"\n"}{packetName[i]} </Text>
                      </View>
                    </TouchableHighlight>
                    <View style={styles.bottom10}></View>
                  </View>
                  <View style={styles.width55p}>
                    <View style={styles.directionColumn}>
                      <View style={styles.directionColumn}>
                        <Text style={[styles.font12, styles.bold, styles.baseText]}>
                          {asuransi[i]}{"\n"}{"\n"}
                        </Text>
                        <View style={[styles.borderTransparent, styles.radius2, styles.border1, styles.radius2]}>
                          <Text style={[styles.font12, styles.bold, styles.underline, styles.baseText]}>
                            Perluasan:
                          </Text>
                           {this.validasiCakupan(listAsuransi[i], apartmentState)}
                        </View>
                      </View>
                      {showTotalPremi[i]}
                      <View style={[styles.directionRow, styles.bottom10]}>
                        <View>
						<Button underlayColor='blue'
                            style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                            onPress={() => this.lihatBenefit(listAsuransi[i])}>
                            <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Benefit</Text>
                          </Button>
						</View>
                        <View>
                          <Button underlayColor='blue'
                            style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                            onPress={() => this.doBeli(listAsuransi[i])}>
                            <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Beli</Text>
                          </Button>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )
        }
      }else{
        arrListAsuransi.push(
          <View style={styles.centerItemContent} key = 'listasuransi'>
            <View style={[styles.bgWhite, styles.bottom5, styles.radius5, styles.top10, styles.width85p]}>
              <View style={styles.centerItemContent}>
                <Text style={[styles.baseText]}>Tidak Ada Asuransi Yang Sesuai Dengan Data Yang Anda Input.</Text>
              </View>
            </View>
          </View>

        );
      }
      return arrListAsuransi;
    }

    validasiCakupan(listAsuransi, apartmentState){
      const premi_dasar = listAsuransi.premi_dasar;
      const premi_eqvet = listAsuransi.premi_eqvet;
      const premi_flood = listAsuransi.premi_flood;
      const eqvet = apartmentState.eqvet;
      const flood = apartmentState.flood;
      var listPerluasan = [];
      var tipePerluasan = [];
      var valuePerluasan = [];

      //console.log(premiDasar);
      if(premi_dasar == 0 || premi_dasar){
          valuePerluasan.push(premi_dasar);
          tipePerluasan.push('Premi Dasar');
      }
      if(eqvet){
          valuePerluasan.push(premi_eqvet);
          tipePerluasan.push('EQVET');
      }
      if(flood){
          valuePerluasan.push(premi_flood);
          tipePerluasan.push('TSFWD');
      }

      for(let i = 0; i < tipePerluasan.length; i++){
        listPerluasan.push(
          <View key={i}>
               <View style={{flex: 2, flexDirection:'row', justifyContent:'space-between'}}>
               <Text style={[styles.font12, styles.baseText]}>
                   {tipePerluasan[i]}
               </Text>
               <Text style={[styles.font12, styles.baseText]}>
                 Rp. {valuePerluasan[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
               </Text>
             </View>
           </View>
        );
      }
        return listPerluasan;
    }

    renderButton = (text, onPress) => (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.modalButton}>
          <Text style ={[styles.bold, styles.colorWhite, styles.baseText]}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    );

   renderModalContent = () => (
     <View style={styles.modalContent}>
       <Text style={[styles.baseText]}>Hello!</Text>
       {this.renderButton('Close', () => this.closeModal('benefit'))}
     </View>
   );

   renderPerluasan= () => (
     <View style={styles.modalContent}>
       <HeaderListAsuransiMV />
       {this.renderButton('Simpan Perluasan', () => this.closeModal('perluasan','save'))}
     </View>
   );



   componentDidMount() {

   }

  render(){
    var {height, width} = Dimensions.get('window');
    const {apartmentState, listAsuransi, itemsIsLoading, loadingState} = this.props;
    //console.log('render',listAsuransi);

    return(
      <ScrollView>
		{ 	loadingState.loadingListAsuransiApartment ?
            <LoadingPage /> :
			 this.renderListAsuransi(apartmentState, listAsuransi)
      }
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    listAsuransi: state.listAsuransi,
    apartmentState: state.apartmentState,
    status: state.status,
    itemsIsLoading:state.itemsIsLoading,
	loadingState:state.loadingState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAsuransiApartment);
