import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import Tooltip from '../../../components/tooltip/Tooltip';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';
import WheelPicker from '../../../components/picker/WheelPicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';
import UploadImage from '../../../components/upload/UploadImage';

class DetailPribadiApartment extends Component{

  constructor(props) {
    super(props);
  }

  genderlist(value){
    const title = 'Title';
    const lob = 'Apartment';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	<View style = {[styles.width40p]}>
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
        iconName='md-arrow-dropdown'
      />
	 </View>
    );
  }
  
  namatext(nama_pemilik){
	  return(
	  <JagainTextInput
	   title = 'Nama Tertanggung'
		 value={nama_pemilik}
			autoCapitalize='words'
		  // Adding hint in Text Input using Place holder.
		   onChangeText={(nama_pemilik) => this.props.setNamaPemilikDtApt(appFunction.alphabetSpaceOnly(nama_pemilik))}
          
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
  }
  
  qqcheckbox(status_qq_bool, statusqq){
	  const tootltipText = "Maksud dari QQ adalah: dalam kapasitasnya sebagai wakil dari…";

	  return(

		<View style={styles.directionRow}>
			<Text style={[styles.baseText]}>QQ</Text>
			<CheckBox isChecked={ status_qq_bool } checkBoxColor='#ff4c4c' onClick={() => this.changeExtStatus(statusqq)} />
			<Tooltip tooltipText={tootltipText} />
		</View>
	  );
  }
  
  changeExtStatus(statusqq){
    var change_statusqq = "Y";
    if(statusqq == 'N')
      change_statusqq = "Y";
    else
      change_statusqq = "N";
    this.props.setQQDtApt(change_statusqq);
  }

  renderQQInput(statusqq, namabankleasing){
    if( statusqq == 'Y' ){
      return(
	  <JagainTextInput
	   title = 'Nama Bank'
		value={namabankleasing}
				autoCapitalize='words'
		  // Adding hint in Text Input using Place holder.
		   onChangeText={(namabankleasing) => this.props.setNamaBankDtApt(appFunction.alphabetSpaceOnly(namabankleasing))}
          
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
       
      );
    }
  }
  
  alamattext(alamat_pemilik){
	  return(
	  <JagainTextInput
	   title = 'Alamat'
		value={alamat_pemilik}
			  onChangeText={(alamat_pemilik) => this.props.setAlamatPemilikDtApt(appFunction.dotalphaNumericOnly(alamat_pemilik))}
			multiline = {true}
			numberOfLines = {4}
          
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
  }
  
  noktptext(ktp){
	  return(
	  <JagainTextInput
	   title = 'No KTP'
		value={ktp}
				keyboardType='numeric'
				onChangeText={(ktp) => this.props.setKTPDtApt(appFunction.numericOnly(ktp))}
          maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
	  
  }
  
  tgllahirtext(tgl_lahir){
	  return(
	   <JagainDatePicker
		 title='Tanggal Lahir'
         date={tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
		onDateChange={(tgl_lahir) => this.props.setTglLahirDtApt(tgl_lahir)}
        />
	  );
  }

  doPostData(body, dataApartment){
    var errorMessage =  '';
    if(!dataApartment.tittle || dataApartment.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataApartment.nama_pemilik){
      errorMessage = 'Nama Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataApartment.alamat_pemilik){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataApartment.ktp){
      errorMessage = 'No KTP Belum Diisi';
      appFunction.toastError(errorMessage);
    }
    //if(!photo_belakang)
    //if(!photo_depan)
  //  if(!photo_kanan)
    //if(!photo_kiri)
    //if(!photo_ktp)
    //if(!photo_stnk)
    else if(dataApartment.statusqq == 'Y' && !dataApartment.namabankleasing){
      errorMessage = 'Nama Bank Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataApartment.tgl_lahir){
      errorMessage = 'Tanggal Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataApartment.photo_ktp){
      errorMessage = 'Photo KTP Belum Diupload';
      appFunction.toastError(errorMessage);
    }else{
      this.props.setPostData(body);
      //this.props.doPostDataApartment(body);
      this.props.navigate('Pembayaran');
    }
  }



  _pickImgktp = async () => {
    await ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      includeBase64: true,
      mediaType:'photo',
      }).then(image => {
        if(image.size / 1024 <= 20480){
          this.props.setPhotoKtpDtApt(image.data);
        }else{
          appFunction.toastError('Foto Harus Kurang Dari 20MB');
        }
    });
  }
  
  fileUploaded(file, type, title){  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value, type)}  />
	  );
  }
  
  setType(value){

          this.props.setPhotoKtpDtApt(value);
  }

  fileUploaded(file, title){  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value)}  />
	  );
  }
  
  dataTertanggung(){
	const {
      alamat_pemilik,
      nama_pemilik,
      namabankleasing,
      ktp,
      photo_belakang,
      photo_depan,
      photo_kanan,
      photo_kiri,
      photo_ktp,
      statusqq,
      tgl_lahir,
      tittle,
    } = this.props.dataApartment;
  
	  
	 var status_qq_bool = false;
    if(statusqq == "Y")
      status_qq_bool = true;
    else
      status_qq_bool = false;

	

	  return(
		   <View style={[styles.detailPribadiMainContainer]}>
        <WheelPicker itemList={appFunction.getNameProfile(this.props.listPesertaAsuransiDummy)} />
				{this.genderlist(tittle)}
				<View style={[styles.top10]} />
				{this.namatext(nama_pemilik)}	  
				<View style={[styles.top10]} />
				{this.qqcheckbox(status_qq_bool, statusqq)}
				<View style={[styles.top10]} />
				{this.renderQQInput(statusqq, namabankleasing)}
				<View style={[styles.top10]} />
				{this.alamattext(alamat_pemilik)}
				<View style={[styles.top10]} />
				{this.noktptext(ktp)}
				<View style={[styles.top10]} />
				{this.tgllahirtext(tgl_lahir)}	
				<View style={[styles.top10]} />
				{this.fileUploaded(photo_ktp,'KTP')}
			</View>
	  );
  }

  /* list peserta */
  listPeserta(){
    
  }

  /* end list peserta */


	  componentDidMount(){
		this.props.resetPostData();
	  }

	render() {
    var {height, width} = Dimensions.get('window');
    const {dataApartment, session, apartmentState, dataVoucher, asuransiApartment} = this.props;

    const body = Object.assign({}, apartmentState);

    const dataPostApartment = Object.assign({},dataApartment,
      {visitor_id:session.visitor_id},
      {kode_voucher:dataVoucher.kode_voucher},
     );

     const postData =  Object.assign({},{data_properties:dataPostApartment}, body, {package_id:asuransiApartment.package_id });
    console.log('profilename',appFunction.getNameProfile(this.props.listPesertaAsuransiDummy));
    console.log(this.props.listPesertaAsuransiDummy);
	  return (
			<View style = {[styles.centerItemContent]}>
        
				{this.dataTertanggung()}
				<View style={{height: height*.05}}></View>
				<SubmitButton title='Submit' onPress={() => this.doPostData(postData,dataApartment)} />
            </View>
	  );
  }
}

function mapStateToProps(state) {
  return {
    dataApartment: state.dataApartment,
    apartmentState: state.apartmentState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    asuransiApartment: state.asuransiApartment,
    listPesertaAsuransiDummy:state.listPesertaAsuransiDummy,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiApartment);
