import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'
import PopoverTooltip from 'react-native-popover-tooltip';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';
import CheckBoxTooltip from '../../../components/checkBox/CheckBoxTooltip';

class HeaderListAsuransiMV extends Component{
  constructor(props){
    super(props);
	this.state={
		visible: false,
	}
  }
  
  changeExtValue(value, perluasan, tipe = '') {
	
    var numValue = value;
    if(tipe == ''){
       numValue = appFunction.currencyToNum(value);
     }
     const {mvState} = this.props;

    if(mvState.vehicle_type == 'Mobil' && value){
      if(numValue > 100000000){
        numValue = 100000000;
      //  console.log('mobil', numValue)
      }
    }else if(mvState.vehicle_type == 'Motor' && value){
      if(perluasan == 'TPL'){
          if(numValue > 20000000){
            numValue = 20000000;
          }
          //console.log('motplil', numValue)
        }else if(perluasan == 'PAD'){
          if(numValue > 10000000){
            numValue = 10000000;
          //  console.log('mopadil', numValue)
          }
        }
    }else{
         numValue = 0;
       }
    this.props.setValuePerluasanMV(numValue, perluasan);
	
  }

  changeExtStatus(status, perluasan){
    const changeStatus = !status;
    this.props.setStatusPerluasanMV(changeStatus, perluasan);
	
  }

  header(perluasanMV, mvState){
    const perluasan = ['TPL','PLL','PAD', 'PAP', 'SRCC', 'TS', 'EQVET', 'TSFWD', 'ATPM'];
    const tpltext = 'TPL : Third Party Liability adalah Kerugian yang diderita pihak ketiga yang berada di luar kendaraan, yang secara langsung disebabkan oleh kendaraan yang dipertanggungkan baik yang diselesaikan melalui musyawarah, arbitrase maupun melalui pengadilan, kedua-duanya yang mendapat persetujuan terlebih dahulu dari Penanggung. Jaminan ini meliputi Kerusakan atas harta benda, cedera badan dan atau kematian.';
    const plltext = 'PLL : Publik Legal Liability adalah perluasan risiko terhadap kemungkinan adanya tuntutan para Penumpang (Pihak ke tiga) di dalam kendaraan bermotor yang dipertanggungkan, yang secara langsung disebabkan oleh kecelakaan kendaraan bermotor tersebut. Jaminan ini tidak berlaku bagi Suami atau istri, anak, orang tua dan saudara sekandung Tertanggung, orang-orang yang bekerja pada dan orang-orang yang berada di bawah pengawasan Tertanggung, orang yang tinggal bersama Tertanggung, Pengurus, pemegang saham, komisaris atau pegawai, jika Tertanggung adalah badan hukum.';
    const padtext = 'PA Driver : Adalah perluasan risiko terhadap risiko kecelakaan diri pengemudi kendaraan bermotor yang dipertanggungkan, yang secara langsung disebabkan oleh kecelakaan kendaraan bermotor tersebut. Jaminan ini diberikan dalam hal cidera badan atau kematian dan biaya perawatan atau pengobatan.';
    const paptext = 'PA Passenger : Adalah perluasan risiko terhadap risiko kecelakaan diri penumpang yang berada di dalam kendaraan bermotor yang dipertanggungkan, yang secara langsung disebabkan oleh kecelakaan kendaraan bermotor tersebut. Jaminan ini diberikan dalam hal cidera badan atau kematian dan biaya perawatan atau pengobatan.';
    const srcctext = 'SRCC : Strike, Riot, Civil Commotion: Adalah perluasan yang mengcover risiko kerugian/kerusakan atas kendaraan bermotor yang dipertanggungkan yang disebabkan secara langsung sebagai akibat adanya :\
                \na) kerusuhan; pemogokan; penghalangan bekerja; perbuatan jahat; tawuran;\
                \nb) huru-hara;\
                \nc) pembangkitan rakyat tanpa penggunaan senjata api;\
                \nd) revolusi tanpa penggunaan senjata api;\
                \ne) Pencegahan yang wajar sehubungan dengan risiko-risiko tersebut diatas.\
                \nf) Penjarahan yang terjadi selama kerusuhan atau huru hara.';
    const tstext = 'TS : Terrorism & Sabotage : Adalah perluasan risiko terhadap risiko kerugian/kerusakan atas kendaraan bermotor yang dipertanggungkan yang disebabkan secara langsung sebagai akibat adanya makar, terorisme, sabotase dan pencegahan yang wajar sehubungan dengan risiko-risiko tersebut.';
    const eqvettext = 'EQVET : Earthquake and Volcano Eruption adalah perluasan risiko terhadap risiko kerugian/kerusakan atas Kendaraan Bermotor yang dipertanggungkan yang disebabkan secara langsung oleh Gempa Bumi, Tsunami dan atau Letusan gunung berapi.';
    const tsfwdtext = 'TSFWD : Typhoon, Storm, Flood and Water Damage adalah perluasan risiko terhadap risiko kerugian/kerusakan atas kendaraan bermotor yang dipertanggungkan yang disebabkan secara langsung oleh Angin topan, Badai, Hujan es, Banjir dan atau Tanah longsor.';
    const atpmtext = 'ATPM : Agen Tunggal Pemegang Merk/Bengkel Resmi adalah jaminan untuk memperbaiki kerusakan kendaraan bermotr di Bengkel Resmi yang terdaftar di perusahaan asuransi.';

    const tpl_value = perluasanMV.tpl_value;
    const pll_value = perluasanMV.pll_value;
    const padriver_value = perluasanMV.padriver_value;
    const pap_value = perluasanMV.pap_value;
    const pap_people = perluasanMV.pap_people;
    const tpl = perluasanMV.tpl;
    //const tplDisabledStatus = !tpl;
    const pll = perluasanMV.pll;
    const padriver = perluasanMV.padriver;
    const pap = perluasanMV.pap;
    const srcc = perluasanMV.srcc;
    const ts = perluasanMV.ts;
    const eqvet = perluasanMV.eqvet;
    const tsfwd = perluasanMV.tsfwd;
    const atpm = perluasanMV.atpm;
    //console.log(tpl);
    //console.log(tplDisabledStatus);
    if( mvState.vehicle_type == "Mobil"){
      return (
        <View>
          <View style={styles.directionRow}>
            <View style={[styles.directionColumn, styles.width65p, {paddingLeft:20}]}>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title={perluasan[0]}
                  onClick={() => this.changeExtStatus(tpl, perluasan[0])}
                  tooltipText={tpltext}
                  isChecked={tpl}
                  color='#ff4c4c' />
                <View>
                  <TextInput underlineColorAndroid='transparent'
                  editable = { tpl }
                  maxLength={11}
                  keyboardType='numeric'
                  style={[styles.text, styles.baseText]}
                  onChangeText={(value) => this.changeExtValue(value, perluasan[0])}>
                    {appFunction.numToCurrency(tpl_value)}
                  </TextInput>
                  <Slider
                    step={500000}
                    disabled={!tpl}
                    maximumValue={100000000}
                    minimumValue={0}
                    onValueChange={(value) => this.changeExtValue(value, perluasan[0], 'slider')}
                    value={tpl_value}
                    thumbTintColor='red'
                    minimumTrackTintColor='red'
                  />
                </View>
              </View>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title={perluasan[1]}
                  onClick={() => this.changeExtStatus(pll, perluasan[1])}
                  tooltipText={plltext}
                  isChecked={pll}
                  color='#ff4c4c' />

                <View>
                  <TextInput
                  underlineColorAndroid='transparent'
                  editable = { pll }
                  maxLength={11}
                  keyboardType='numeric'
                  style={[styles.text, styles.baseText]}
                  onChangeText={(value) => this.changeExtValue(value, perluasan[1])}>
                    {appFunction.numToCurrency(pll_value)}
                  </TextInput>
                  <Slider
                    step={500000}
                    disabled={!pll}
                    maximumValue={100000000}
                    minimumValue={0}
                    onValueChange={(value) => this.changeExtValue(value, perluasan[1], 'slider')}
                    value={pll_value}
                    thumbTintColor='red'
                    minimumTrackTintColor='red'
                  />
                </View>
              </View>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title='PA Driver'
                  onClick={() => this.changeExtStatus(padriver, perluasan[2])}
                  tooltipText={padtext}
                  isChecked={padriver}
                  color='#ff4c4c' />

                <View>
                  <TextInput
                  underlineColorAndroid='transparent'
                  editable = { padriver }
                  maxLength={11}
                  keyboardType='numeric'
                  style={[styles.text, styles.baseText]}
                  onChangeText={(value) => this.changeExtValue(value, perluasan[2])}>
                    {appFunction.numToCurrency(padriver_value)}
                  </TextInput>
                  <Slider
                    step={500000}
                    disabled={!padriver}
                    maximumValue={100000000}
                    minimumValue={0}
                    onValueChange={(value) => this.changeExtValue(value, perluasan[2], 'slider')}
                    value={padriver_value}
                    thumbTintColor='red'
                    minimumTrackTintColor='red'
                  />
                </View>
              </View>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title='PA Passanger'
                  onClick={() => this.changeExtStatus(pap, perluasan[3])}
                  tooltipText={paptext}
                  isChecked={pap}
                  color='#ff4c4c' />
                <View style={styles.directionRow}>
                  <View style={styles.width50p}>
                    <Picker
                      mode='dialog'
                      enabled={pap}
                      selectedValue={pap_people}
					  itemTextStyle={[styles.text, styles.baseText]}
                      onValueChange={(value) => this.props.setAmountPerluasanMV(value, perluasan[3])}>
                      <Picker.Item label='1' value={1} />
                      <Picker.Item label='2' value={2} />
                      <Picker.Item label='3' value={3} />
                      <Picker.Item label='4' value={4} />
                      <Picker.Item label='5' value={5} />
                    </Picker>
                  </View>
                  <View style={styles.width60p}>
                    <Picker
                      mode='dialog'
                      enabled={pap}
                      selectedValue={pap_value}
					  itemTextStyle={[styles.text, styles.baseText]}
                      onValueChange={(value) => this.props.setValuePerluasanMV(value, perluasan[3])}>
                      <Picker.Item label='10 Jt' value={10000000} />
                      <Picker.Item label='20 Jt' value={20000000} />
                      <Picker.Item label='30 Jt' value={30000000} />
                      <Picker.Item label='40 Jt' value={40000000} />
                      <Picker.Item label='50 Jt' value={50000000} />
                    </Picker>
                  </View>
                </View>
              </View>
            </View>
            <View style={[styles.width35p, styles.directionColumn]}>
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[4]}
                onClick={() => this.changeExtStatus(srcc, perluasan[4])}
                tooltipText={srcctext}
                isChecked={srcc}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[5]}
                onClick={() => this.changeExtStatus(ts, perluasan[5])}
                tooltipText={tstext}
                isChecked={ts}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[6]}
                onClick={() => this.changeExtStatus(eqvet, perluasan[6])}
                tooltipText={eqvettext}
                isChecked={eqvet}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[7]}
                onClick={() => this.changeExtStatus(tsfwd, perluasan[7])}
                tooltipText={tsfwdtext}
                isChecked={tsfwd}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[8]}
                onClick={() => this.changeExtStatus(atpm, perluasan[8])}
                tooltipText={atpmtext}
                isChecked={atpm}
                color='#ff4c4c' />
            </View>
          </View>
        </View>
      );
    }else if(mvState.vehicle_type == "Motor"){
      return (
        <View>
          <View style={styles.directionRow}>
            <View style={[styles.directionColumn, styles.width65p, {paddingLeft:20}]}>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title={perluasan[0]}
                  onClick={() => this.changeExtStatus(tpl, perluasan[0])}
                  tooltipText={tpltext}
                  isChecked={tpl}
                  color='#ff4c4c' />
                <View>
                  <TextInput underlineColorAndroid='transparent'
                  editable = { tpl }
                  maxLength={10}
                  keyboardType='numeric'
                  style={[styles.text, styles.baseText]}
                  onChangeText={(value) => this.changeExtValue(value, perluasan[0])}>
                    {appFunction.numToCurrency(tpl_value)}
                  </TextInput>
                  <Slider
                    step={500000}
                    disabled={!tpl}
                    maximumValue={20000000}
                    minimumValue={0}
                    onValueChange={(value) => this.changeExtValue(value, perluasan[0], 'slider')}
                    value={tpl_value}
                    thumbTintColor='red'
                    minimumTrackTintColor='red'
                  />
                </View>
              </View>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title='PA Driver'
                  onClick={() => this.changeExtStatus(padriver, perluasan[2])}
                  tooltipText={padtext}
                  isChecked={padriver}
                  color='#ff4c4c' />

                <View>
                  <TextInput
                  underlineColorAndroid='transparent'
                  editable = { padriver }
                  maxLength={10}
                  keyboardType='numeric'
                  style={[styles.text, styles.baseText]}
                  onChangeText={(value) => this.changeExtValue(value, perluasan[2])}>
                    {appFunction.numToCurrency(padriver_value)}
                  </TextInput>
                  <Slider
                    step={500000}
                    disabled={!padriver}
                    maximumValue={10000000}
                    minimumValue={0}
                    onValueChange={(value) => this.changeExtValue(value, perluasan[2], 'slider')}
                    value={padriver_value}
                    thumbTintColor='red'
                    minimumTrackTintColor='red'
                  />
                </View>
              </View>
              <View style={styles.directionColumn}>
                <CheckBoxTooltip parentStyle={[styles.width90p]}
                  title='PA Passanger'
                  onClick={() => this.changeExtStatus(pap, perluasan[3])}
                  tooltipText={paptext}
                  isChecked={pap}
                  color='#ff4c4c' />
                <View style={styles.directionRow}>
                  <View style={styles.width50p}>
                    <Picker
                      mode='dialog'
                      enabled={pap}
                      selectedValue={pap_people}
					  itemTextStyle={[styles.text, styles.baseText]}
                      onValueChange={(value) => this.props.setAmountPerluasanMV(value, perluasan[3])}>
                      <Picker.Item label='1' value={1} />
                    </Picker>
                  </View>
                  <View style={styles.width60p}>
                    <Picker
                      mode='dialog'
                      enabled={pap}
                      selectedValue={pap_value}
					  itemTextStyle={[styles.text, styles.baseText]}
                      onValueChange={(value) => this.setValuePerluasanMV(value, perluasan[3])}>
                      <Picker.Item label='10 Jt' value={10000000} />
                    </Picker>
                  </View>
                </View>
              </View>
            </View>
            <View style={[styles.width35p, styles.directionColumn]}>
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[4]}
                onClick={() => this.changeExtStatus(srcc, perluasan[4])}
                tooltipText={srcctext}
                isChecked={srcc}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[5]}
                onClick={() => this.changeExtStatus(ts, perluasan[5])}
                tooltipText={tstext}
                isChecked={ts}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[6]}
                onClick={() => this.changeExtStatus(eqvet, perluasan[6])}
                tooltipText={eqvettext}
                isChecked={eqvet}
                color='#ff4c4c' />
              <CheckBoxTooltip parentStyle={[styles.bottom3]}
                title={perluasan[7]}
                onClick={() => this.changeExtStatus(tsfwd, perluasan[7])}
                tooltipText={tsfwdtext}
                isChecked={tsfwd}
                color='#ff4c4c' />
            </View>
          </View>
        </View>
      );
    }
  }

	
	scrollToBottom() {
	  this.refs.scrollView.scrollToEnd(); // will scroll to the top at y-position 0
	}
	
	openPerluasan(visible) {
	  //this.refs.scrollView.scrollToEnd(); // will scroll to the top at y-position 0  
	  this.setState({visible: !visible});
	}

  componentWillReceiveProps(NextProps){
	  if(NextProps.perluasanMV != this.props.perluasanMV){
		  const lob = 'MV';
		  const body = Object.assign({}, this.props.mvState, NextProps.perluasanMV);
		  delete body.merek;
		  delete body.seri_tipe;
		  delete body.penggunaan;
		  //console.log('closemodal', body)
		  this.props.getListAsuransi(body, lob);
	  }

  }

  render(){
    const {perluasanMV, mvState} = this.props;
	const {visible} = this.state;
	//const {arrow} = this.state;
	//console.log(visible);

    return(
	 
	<View>
	<View style={[styles.bgWhite, styles.top5, styles.elev1 ,styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent]}>
				<Text style={[styles.baseText, styles.font11, styles.right5, styles.padTop5, styles.bold,{color: '#19198c'}]}>Perluasan - Silakan pilih perluasan yang anda inginkan</Text>
				<View style={[styles.flexEndSelf]}><TouchableHighlight underlayColor='transparent' onPress={()=>{this.openPerluasan(visible)}}>{visible? <Icon name='ios-arrow-dropup-outline' /> : <Icon name='ios-arrow-dropdown-outline' />}</TouchableHighlight></View>
			</View>
		</View>
		{visible? 
		
      <ScrollView showsVerticalScrollIndicator={true} ref="scrollView">
	  <View style={[styles.bgWhite]}>
        <View style={[styles.centerItem, styles.width95p, styles.top10]}>
          {this.header(perluasanMV, mvState)}
        </View> 
	  </View> 
	  </ScrollView>
	  
	   : null}
	</View>
	
    );
  }
}

function mapStateToProps(state) {
  return {
    perluasanMV: state.perluasanMV,
    mvState: state.mvState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderListAsuransiMV);
