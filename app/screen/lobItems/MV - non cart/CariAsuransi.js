import React, {Component} from 'react';
import {View, BackHandler, Image, ScrollView, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import JagainErrorText from '../../../components/text/JagainErrorText';
import JagainTextInput from '../../../components/text_input/JagainTextInput';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiMV extends React.Component {
  constructor(props) {
    super(props);
	this.focusErrorField = this.focusErrorField.bind(this);
	this.inputs={}
	
	this.state={
		statusError: '',
		textError:'',
	}
  }

  toListAsuransi(mvState, lob) {
    var errorMessage= '';
    if(!mvState.vehicle_type || mvState.vehicle_type == 'Pilih'){
		//this.setState({statusError: 'vehicle_type',textError:'Jenis Kendaraan Belum Dipilih',});
		//this.focusErrorField('vehicle_type');
      errorMessage = 'Jenis Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.merek || mvState.merek == 'Pilih'){
		//this.setState({statusError: 'merek',textError:'Merek Kendaraan Belum Dipilih',});
		//this.focusErrorField('merek');
      errorMessage = 'Merek Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.seri_tipe || mvState.seri_tipe == 'Pilih'){
     //this.setState({statusError: 'seri_tipe',textError:'Seri Kendaraan Belum Dipilih',});
		//this.focusErrorField('seri_tipe');
    errorMessage = 'Seri Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.tsi){
       //this.setState({statusError: 'tsi',textError:'Harga Kendaraan Belum Diisi',});
		//this.focusErrorField('tsi');
      errorMessage = 'Harga Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!mvState.build_year || mvState.build_year == 'Pilih'){
      //this.setState({statusError: 'build_year',textError:'Tahun Produksi Kendaraan Belum Dipilih',});
		//this.focusErrorField('build_year');
      errorMessage = 'Tahun Produksi Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.plat || mvState.plat == 'Pilih'){
     //this.setState({statusError: 'plat',textError:'Plat Kendaraan Belum Dipilih',});
		//this.focusErrorField('plat');
	 errorMessage = 'Plat Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(mvState.aksesoris && mvState.aksesoris !== ' ' && !Boolean(mvState.harga_aksesoris)){
      //this.setState({statusError: 'harga_aksesoris',textError:'Harga Aksesoris Kendaraan Belum Diisi',});
	  
		//this.focusErrorField('harga_aksesoris');
	 errorMessage = 'Harga Aksesoris Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if((!mvState.aksesoris || mvState.aksesoris == ' ') && Boolean(mvState.harga_aksesoris)){
		//this.setState({statusError: 'aksesoris',textError:'Aksesoris Kendaraan Belum Diisi',});
		
		//this.focusErrorField('aksesoris');
      errorMessage = 'Aksesoris Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else{
      const {perluasanMV} = this.props;
      const body = Object.assign({}, mvState, perluasanMV);
      delete body.merek;
      delete body.seri_tipe;
      delete body.penggunaan;

      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }

  jeniskendaraanlist(mvState, lob){
    const title = 'Jenis Kendaraan';
    const value = mvState.vehicle_type;
    const options = this.props.listJenisKendaraan;
    const type = Object.assign({},{state: 'vehicle_type', lob});


    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  yearlist(mvState, lob){
    const title = 'Tahun Produksi Kendaraan';
    const value = mvState.build_year;
    const options = appFunction.yearlist();
    const type = Object.assign({},{state: 'build_year', lob});


    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  merklist(mvState, lob){
    const title = 'Merek Kendaraan';
    const value = mvState.merek;
    const options = this.props.listMerekKendaraan;
    const type = Object.assign({},{state: 'merek', lob});


    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  serilist(mvState, lob){
    const title = 'Seri & Tipe';
    const value = mvState.seri_tipe;
    const options = this.props.listSeriKendaraan;
    const type = Object.assign({},{state: 'seri_tipe', lob});

    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  platlist(mvLabelState, lob){
      const title = 'Plat Area/Wilayah';
      const value = mvLabelState.plat;
      const options = this.props.listPlatKendaraan;
      const type = Object.assign({},{state: 'plat', lob});
      //console.log(options);
      return(
        <FilterLabelPicker title={title}
          value={value}
          options={options}
          type={type}
        />
      );
    }

  usetext(mvState){
    const value = mvState.penggunaan;
    //console.log('use',value);

    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Penggunaan
        </Text>

        <TextInput
          underlineColorAndroid='transparent'
          editable={false}
          style={[styles.font16, styles.height40, styles.baseText]}
          keyboardType='numeric'
          //onChangeText={(use) => this.setState({use:use})}
          value={value}
        />
      </View>
    );
  }

  coveragelist(otherDataKendaraan, lob){
    const title = 'Tipe Coverage';
    const value = otherDataKendaraan.label_coverage;
    const options = this.props.listCoverageKendaraan;
    const type = Object.assign({},{state: 'coverage', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  hargatinput(mvState){
    var value = mvState.tsi;
    value = appFunction.numToCurrency(value);
	if(value == 0)
		value = '';
    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Harga Kendaraan
        </Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={[styles.font16, styles.height40, styles.baseText]}
          keyboardType='numeric'
          onChangeText={(tsi) => this.changeharga(mvState, tsi)}
          value={`${value}`}
		  placeholder= '0'
        />
      </View>
    );
  }

  aksesoristinput(mvState){
    const value = mvState.aksesoris;
    return(
     
        <JagainTextInput
			title='Aksesoris Tambahan'
			 colorOnFocus = '#00e500' 
			//autoCapitalize
			 value={value}
			  onChangeText={(aksesoris) => {this.props.setAksesorisKendaraan(aksesoris)}}
			  multiline = {true}
			  numberOfLines = {4}
			  widthOnFocus = {3}
        />
     
    );
  }

  hargaaksesoristinput(mvState){
    var value = appFunction.numToCurrency(mvState.harga_aksesoris);
	if(value == 0)
		value = '';
    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Total Harga Aksesoris
        </Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={[styles.font16,  styles.height40, styles.baseText]}
          keyboardType='numeric'
          onChangeText={this.setAccessoriesPrice.bind(this)}
          value={`${value}`}
		  placeholder= '0'
        />
      </View>
    );
  }

  changeharga(mvState, tsi){
    //const {typeVehicle} =this.state;
	var value = tsi;
	if(tsi == ''){
		value = 0;
	}
    const vp = appFunction.currencyToNum(value);
    //console.log(vp);
    var errorMessage = '';
    if(mvState.vehicle_type == 'Motor'){
      if(vp > 100000000){
		  // this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Motor Yang Dapat Dicover 100 Juta',});
        errorMessage = 'Harga Maksimal Motor Yang Dapat Dicover 100 Juta';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(100000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }else{
      if(vp > 2500000000){
		  //this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar',});
        errorMessage = 'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(2500000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }
  }
  
  changestateharga(mvState){
    //const {typeVehicle} =this.state;
	var value = mvState.tsi;
	if(mvState.tsi == ''){
		value = 0;
	}
    const vp = value;
    //console.log(vp);
    var errorMessage = '';
    if(mvState.vehicle_type == 'Motor'){
      if(vp > 100000000){
		  // this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Motor Yang Dapat Dicover 100 Juta',});
        errorMessage = 'Harga Maksimal Motor Yang Dapat Dicover 100 Juta';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(100000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }else{
      if(vp > 2500000000){
		  //this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar',});
        errorMessage = 'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(2500000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }
  }



  setAccessoriesPrice (value) {
	  var val = value;
	  
	  if(value == ''){
		  val = 0;
	  }
		  
	
    const accessoriesPrice = appFunction.currencyToNum(val);
    this.props.setHargaAksesorisKendaraan(accessoriesPrice);
  }




  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  }

  componentWillReceiveProps(NextProps){
    const {mvState} = NextProps;
    const oldMvState = this.props.mvState;
    if(mvState != oldMvState){
      if(mvState.vehicle_type != oldMvState.vehicle_type){
        if(mvState.vehicle_type == 'Motor'){
          this.props.resetListSeri([]);
          this.props.resetSeriKendaraan();
          this.props.resetMerekKendaraan();
          this.props.getMerekMotor();
          this.props.getCoverageKendaraan(mvState.vehicle_type);
		  this.props.setCoverageKendaraan('total loss only');
          this.props.setLabelCoverageKendaraan('Total Loss Only');
		  this.changestateharga(mvState);
        }else if(mvState.vehicle_type == 'Mobil'){
          this.props.resetListSeri([]);
          this.props.resetSeriKendaraan();
          this.props.resetMerekKendaraan();
          this.props.getMerekMobil();
          this.props.getCoverageKendaraan(mvState.vehicle_type);
          this.props.setCoverageKendaraan('comprehensive');
          this.props.setLabelCoverageKendaraan('Comprehensive');
		   this.changestateharga(mvState);
        }else if(!mvState.vehicle_type || mvState.vehicle_type  == 'Pilih'){
          this.props.resetListMerek([]);
          this.props.resetSeriKendaraan();
          this.props.resetListCoverage();
          this.props.resetMerekKendaraan();
          this.props.resetCoverageKendaraan();
        }
      }
      if(mvState.merek != oldMvState.merek){
        if(mvState.merek && mvState.merek != 'Pilih'){
          if(mvState.vehicle_type == 'Motor'){
            this.props.getSeriMotor(mvState.merek);
            this.props.resetSeriKendaraan();
          }else if(mvState.vehicle_type == 'Mobil'){
            this.props.getSeriMobil(mvState.merek);
            this.props.resetSeriKendaraan();
			
          }else if(!mvState.vehicle_type || mvState.vehicle_type  == 'Pilih'){
            this.props.resetListSeri([]);
            this.props.resetSeriKendaraan();
          }
        }else if(!mvState.merek || mvState.merek == 'Pilih'){
          this.props.resetListSeri([]);
            this.props.resetSeriKendaraan();
        }
      }
    }
  }
  
	focusErrorField(key) {
    this.inputs[key].focus();
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    //this.props.getPenggunaanKendaraan();
    //this.props.getPlatkendaraan();
    this.props.resetPerluasanMV();
    this.props.getPlatKendaraan();

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {mvState, otherDataKendaraan} = this.props;
    const {textError, statusError} = this.state;
    const mvLabelState = this.props.labelState;
    const lob = 'MV';
    //const {listPlatKendaraan} = this.props;
   // console.log(mvState);
   

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
        {this.jeniskendaraanlist(mvState, lob)}
		{this.merklist(mvState, lob)}
		{this.serilist(mvState, lob)}
		{this.platlist(mvLabelState, lob)}
		{this.yearlist(mvState, lob)}
		{this.hargatinput(mvState)}
		{this.aksesoristinput(mvState)}
		{this.hargaaksesoristinput(mvState)}
		{this.usetext(mvState)}
        {this.coveragelist(otherDataKendaraan, lob)}
        <SubmitButton title='Cari Asuransi' onPress={() => {this.toListAsuransi(mvState, lob);}} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    perluasanMV: state.perluasanMV,
    visible: state.visible,
    mvState: state.mvState,
    labelState: state.labelState,
    listMerekKendaraan: state.listMerekKendaraan,
    listSeriKendaraan: state.listSeriKendaraan,
    listPlatKendaraan: state.listPlatKendaraan,
    listJenisKendaraan: state.listJenisKendaraan,
    listCoverageKendaraan: state.listCoverageKendaraan,
    otherDataKendaraan: state.otherDataKendaraan,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiMV);
