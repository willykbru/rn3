import React, { Component } from 'react';
import { Modal, Alert, BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import UploadImage from '../../../components/upload/UploadImage';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import Tooltip from '../../../components/tooltip/Tooltip';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainTextInputStyle from '../../../components/text_input/JagainTextInputStyle';
import JagainTextInputEditable from '../../../components/text_input/JagainTextInputEditable';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';

class RenewalMV extends Component{

  constructor(props) {
    super(props);
  }
  
   changeExtStatus(status_qq){
    var change_status_qq = 'Y';
    if(status_qq == 'N')
      change_status_qq = 'Y';
    else
      change_status_qq = 'N';
    this.props.setQQDK(change_status_qq);
  }

  genderlist(value){
    const title = 'Title';
    const lob = 'MV';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
      <View style={[styles.width40p]}>
        <FilterPicker title={title}
          value={value}
          options={options}
          type={type}
          disabled={true}
        />
      </View>
    );
  }
  

  
  namatext(nama_pemilik){
	  return(    
      <JagainTextInputEditable
        editable={false}
        title='Nama Tertanggung'
        value={nama_pemilik}
        // Adding hint in Text Input using Place holder. 
        onChangeText={() => null}
      />
	  );
  }
  
  qqcheckbox(status_qq_bool, status_qq){
	const tootltipText = "Maksud dari QQ adalah: dalam kapasitasnya sebagai wakil dari…";
	
	 return(
		<View style={styles.directionRow}>
                    <Text style={[styles.baseText]}>QQ</Text>
                    <CheckBox isChecked={ status_qq_bool } disabled={true} checkBoxColor='#ff4c4c' onClick={() => null} />
					<Tooltip tooltipText={tootltipText} />
                  </View>
	 );
  }
  
  
  renderQQInput(status_qq, nama_stnk){
    if( status_qq == 'Y' ){
      return(
        <JagainTextInputEditable
          editable={false}
          title='Nama'
          value={nama_stnk}
          // Adding hint in Text Input using Place holder. 
          onChangeText={() => null}
        /> 
      );
    }
  }
  
  alamattext(alamat_pemilik){
	  return(
	     <JagainTextInput
		  title = 'Alamat'
		  value={alamat_pemilik}
        onChangeText={(alamat_pemilik) => this.props.setAlamatRenewalKendaraan(appFunction.dotalphaNumericOnly(alamat_pemilik))}                   
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  multiline = {true}
          numberOfLines = {4}
	  />
	  );
	  
  }
  
  noKTPtext(no_ktp){
	  
	  return(
      <JagainTextInputEditable
        editable={false}
        title='No KTP'
        value={no_ktp}
        // Adding hint in Text Input using Place holder. 
        onChangeText={() => null}
      /> 
	  );
  }
  
  birthdatepicker(tgl_lahir){
	  return(
	  <JagainDatePicker
     title='Tanggal Lahir'
     disabled={true}
         date={tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
      iconSource={require('../../../assets/icons/component/birth-date.png')}
      placeholder="Tanggal Lahir"
                    onDateChange={(tgl_lahir) => this.props.setTglLahirDK(tgl_lahir)}
        />
	  
	  );
	  
  }
  
  
  
  nomorplatkendaraantext(no_plat){
	  return(
	  
        <JagainTextInputEditable
          editable={false}
          title='Nomor Plat Kendaraan'
        value={no_plat}
          // Adding hint in Text Input using Place holder. 
          onChangeText={() => null}
        /> 
	 
	  );
	  
  }
  
  warnakendaraantext(warna){
	  return(
      <JagainTextInputEditable
        editable={false}
        title='Warna Kendaraan Anda'
        value={warna}
        // Adding hint in Text Input using Place holder. 
        onChangeText={() => null}
      /> 
	  
	  );
	  
  }
  
  norangkakendaraantext(no_rangka){
	  return(
      <JagainTextInputEditable
        editable={false}
        title='No. Rangka'
        value={no_rangka}
        // Adding hint in Text Input using Place holder. 
        onChangeText={() => null}
      /> 
	    
	  );
  }
  
  nomesinkendaraantext(no_mesin){
	  return(
      <JagainTextInputEditable
        editable={false}
        title='No. Mesin'
        value={no_mesin}
        // Adding hint in Text Input using Place holder. 
        onChangeText={() => null}
      />
	 
	  );
  }

  
  setType(value, type){
    if (type == 'ktp') {
      this.props.setRenewalKendaraanPhotoKtp(value);
    } 
	if(type == 'stnk'){
    this.props.setRenewalKendaraanPhotoStnk(value);
	} 
	if(type == 'depan'){
    this.props.setRenewalKendaraanPhotoDepan(value);
	}  
	if(type == 'belakang'){
    this.props.setRenewalKendaraanPhotoBelakang(value);
	}  
	if(type == 'kanan'){
    this.props.setRenewalKendaraanPhotoKanan(value);
	}  
	if(type == 'kiri'){
    this.props.setRenewalKendaraanPhotoKiri(value);
	}  
  }
  
  fileUploaded(file, type, title){
	  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value, type)}  />    
	  );
  }

 
  
  doPostData(dataKendaraan, postData){
    var errorMessage =  '';
   if(!dataKendaraan.alamat_pemilik){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }
   else if (!dataKendaraan.photo_ktp) {
     errorMessage = 'Photo KTP Belum Diupload';
     appFunction.toastError(errorMessage);
   }else if(!dataKendaraan.photo_stnk){
      errorMessage = 'Photo STNK Lembar Biru Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_depan){
      errorMessage = 'Photo Kendaraan Tampak Depan Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_belakang){
      errorMessage = 'Photo Kendaraan Tampak Belakan Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_kanan){
      errorMessage = 'Photo Kendaraan Tampak Kanan Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_kiri){
      errorMessage = 'Photo Kendaraan Tampak Kiri Belum Diupload';
      appFunction.toastError(errorMessage);
    }else{
      //this.props.doPostDataMV(body);
      //this.props.navigate('Pembayaran');
     console.log('postData',postData)
      Alert.alert(
        'Renewal Polis',
        'Yakin Ingin Renewal? \nAsuransi: '
        + this.props.asuransiMV.insurance_name + ' \nHarga Premi: Rp.' + appFunction.numToCurrency(this.props.asuransiMV.net_premi) + '.00',
        [
          { text: 'Batal', onPress: () => console.log(postData), style: 'cancel' },
          {
            text: 'OK', onPress: () => {
              this.props.doPostDataMV(postData);
              
                this.props.setScreen('PembayaranRenewal', 'renewal');
            
            }
          },
        ],
        { cancelable: false }
      )	
    }
  }
  
  dataTertanggung(){
			
	const {
      alamat_pemilik,
      nama_pemilik,
      nama_stnk,
      no_ktp,
      no_mesin,
      no_plat,
    no_rangka,
    photo_ktp,
      photo_belakang,
      photo_depan,
      photo_kanan,
      photo_kiri,
      photo_stnk,
      status_qq,
      tgl_lahir,
      tittle,
      warna
  } = this.props.renewalDataKendaraan;	
			
		var status_qq_bool = false;
		if(status_qq == "Y")
		  status_qq_bool = true;
		else
		  status_qq_bool = false;
	 
	  return(
		<View style={[styles.detailPribadiMainContainer]}>
            {this.genderlist(tittle)}
            <View style={[styles.top10]} />
			{this.namatext(nama_pemilik)}
            <View style={[styles.top10]} />
			{this.qqcheckbox(status_qq_bool, status_qq)} 
			<View style={[styles.top10]} />  
            {this.renderQQInput(status_qq, nama_stnk)}
            <View style={[styles.top10]} />
			{this.alamattext(alamat_pemilik)}        
            <View style={[styles.top10]} />
			{this.noKTPtext(no_ktp)}      
            <View style={[styles.top10]} />
			{this.birthdatepicker(tgl_lahir)}      
            <View style={[styles.top10]} />
        {this.nomorplatkendaraantext(no_plat)}      
            <View style={[styles.top10]} />
			{this.warnakendaraantext(warna)}     
            <View>
            <View style={[styles.top10]} />
			{this.norangkakendaraantext(no_rangka)}
            <View style={[styles.top10]} />
			{this.nomesinkendaraantext(no_mesin)}
            </View>
        <View style={[styles.top10]} />
        {this.fileUploaded(photo_ktp, 'ktp', 'KTP')}
			<View style={[styles.top10]} />
			{this.fileUploaded(photo_stnk, 'stnk', 'STNK Lembar Biru')}
            <View style={[styles.top10]} />
			{this.fileUploaded(photo_depan, 'depan', 'Tampak Depan')}
            <View style={[styles.top10]} />
			{this.fileUploaded(photo_belakang, 'belakang', 'Tampak Belakang')}
            <View style={[styles.top10]} />
			{this.fileUploaded(photo_kanan, 'kanan', 'Tampak Kanan')}
            <View style={[styles.top10]} />                        
			{this.fileUploaded(photo_kiri, 'kiri', 'Tampak Kiri')}
        </View>
	  );  
  } 
   componentDidMount(){

	
		this.props.resetPostData();
	
  }

	render() {
    var {height, width} = Dimensions.get('window');
    const { renewalDataKendaraan, renewalKendaraan, renewalPerluasanKendaraan, session,  asuransiMV} = this.props;
    
   

    const body = Object.assign({}, renewalKendaraan, renewalPerluasanKendaraan);
    delete body.merek;
    delete body.seri_tipe;
    delete body.penggunaan;

    const dataPostKendaraan = Object.assign({}, renewalDataKendaraan,
      { merek: renewalKendaraan.merek},
      { no_plat: renewalDataKendaraan.no_plat},
      { penggunaan: renewalKendaraan.penggunaan},
      { seri_tipe: renewalKendaraan.seri_tipe},
      {visitor_id:session.visitor_id},
      {kode_voucher:""},
     );

     const postData =  Object.assign({},{data_kendaraan:dataPostKendaraan}, body, {insurance_id:asuransiMV.insurance_id });

 
	  return (
		<View style = {[styles.centerItemContent]}>
        {this.dataTertanggung()}
            <View style={{height: height*.05}}></View>
        <SubmitButton title='Submit' onPress={() => this.doPostData(renewalDataKendaraan, postData )} />
        </View>
	  );
  }
}

function mapStateToProps(state) {
  return {
    renewalDataKendaraan: state.renewalDataKendaraan,
    renewalKendaraan: state.renewalKendaraan,
    session:state.session,
    renewalLabel: state.renewalLabel,
    renewalPerluasanKendaraan: state.renewalPerluasanKendaraan,
    asuransiMV: state.asuransiMV,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RenewalMV);
