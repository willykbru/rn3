import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Modal from 'react-native-modal';

import md5 from "react-native-md5";
import * as appFunction from '../../../function'
import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';

import CheckBox from 'react-native-check-box';
import styles from '../../../assets/styles';


class PembayaranTravel extends Component{

constructor(props) {
    super(props);
    this.state = {
      agreement: false,
    }
  }

    disclaimertext(dataBeliTravel, agreement){
	  var net_premi = dataBeliTravel.premium_charged - (dataBeliTravel.premium_charged * dataBeliTravel.discount/100);
		if(dataBeliTravel.description == 'ok'){
			  if(dataBeliTravel.voucher_persen !== 0){
				net_premi = net_premi-(net_premi*dataBeliTravel.voucher_persen/100);
			  }else{
				net_premi = net_premi-dataBeliTravel.voucher_amount; 
			  }
		}
	 
      return(
        <View style={[styles.centerItemContent, styles.top5, styles.width90p]}>
          <View style={styles.centerItemContent, styles.bottom10}>
            <Text style={[styles.font14, styles.baseText]}>
              Terima kasih telah mengajukan permohonan polis asuransi.
              Kami akan otomatis mengirimkan email konfirmasi pembelian
              setelah Anda menyelesaikan pembayaran. Anda mungkin
              akan dikenakan biaya tambahan jika transfer dilakukan
              dari rekening selain yang tertera pada kwitansi pembayaran.
            </Text>
          </View>
          <View style={[styles.bgBlackOp6, styles.centerItemContent, styles.radius5, styles.elev5]}>
            <View style={[styles.centerItem, styles.left5, styles.right5, styles.top10, styles.bottom10]}>
              <View style={[styles.directionRow,{alignItems: 'flex-end'}]}>
                <Text style={[styles.colorWhite, styles.baseText]}>Jumlah </Text>
                <Text style={[styles.colorWhite, styles.font16, styles.baseText]}> Rp. {net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
              <Text style={[styles.colorWhite, styles.font14, styles.baseText]}>
                Untuk pembayaran dengan metode transfer. Anda tidak
                perlu melakukan konfirmasi pembayaran. Sistem Jagain
                akan otomatis mendapatkan pemberitahuan ketika transfer berhasil.
              </Text>
            </View>
			<View style={[styles.bottom5, styles.left5, styles.directionRow, styles.width100p]}>
				<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText]}>
					* 
				</Text>
				<TouchableHighlight underlayColor='blue' onPress={()=> {this.props.navigate('Agreement')} }>
					<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText, styles.underline]}>
					syarat dan ketentuan
					</Text>
				</TouchableHighlight>
			</View>
            <View style={[styles.directionRow, styles.width90p, styles.padTop5]}>
              <CheckBox isChecked={ agreement } checkBoxColor='white' onClick={() => this.changeExtStatus(agreement)} />
              <Text style={[styles.colorWhite, styles.font14, styles.baseText]}>
                Saya setuju dengan syarat dan ketentuan yang berlaku di Jagain
              </Text>
            </View>
          </View>
        </View>
      );
    }

    doBayar(agreement){
      if(!agreement)
        appFunction.toastError('Syarat dan Ketentuan Belum Disetujui');
      else
        this.props.navigate('PaymentGateway');
    }

    changeExtStatus(agreement){

      var agreeStat = agreement;
        agreeStat = !agreement;

        this.setState(() => {
          return {
            agreement: agreeStat,
          };
        });

    }

    _renderButton = (text, onPress) => (
         <TouchableOpacity onPress={onPress}>
           <View style={styles.modalButton}>
             <Text style={[styles.baseText]}>{text}</Text>
           </View>
         </TouchableOpacity>
       );

    _renderModalContent = () => (
        <View style={styles.modalContent}>
          <Text style={[styles.baseText]}>Hello!</Text>
          {this._renderButton('Close', () => this.closeModal())}
        </View>
      );


    detailPremi(dataBeliTravel){
		var perluasanpremi = [];
		var netpremi = [];
		var discvoucher=[];

		const premi = dataBeliTravel.premium_charged;
		const net_premi = dataBeliTravel.premium_charged - (dataBeliTravel.premium_charged * dataBeliTravel.discount/100);

		var disc_premi = premi*dataBeliTravel.discount/100;

		if(dataBeliTravel.discount !== 0){
		if(dataBeliTravel.description == 'ok'){
		  if(dataBeliTravel.voucher_persen !== 0){
			netpremi.push(
			  <View key='netpremi' >
				<View style={[styles.directionRow, styles.bottom]}>
				<View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				 </View>
				</View>
				<View style={styles.directionRow}>
				  <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
					<View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.voucher_persen} %</Text></View>
				  </View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{(premi*dataBeliTravel.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				</View>
				<View style={styles.directionRow}>
				  <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
					<View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.discount} %</Text></View>
				  </View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				</View>
				<View style={styles.borderbottom1}></View>
				<View style={styles.directionRow}>
				<View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(net_premi-(net_premi*dataBeliTravel.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				</View>
			  </View>
			);
		  }else{
			netpremi.push(
			  <View key='netpremi' >
				<View style={[styles.directionRow, styles.bottom]}>
				<View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
				</View>
				<View style={styles.directionRow}>
				  <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliTravel.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>	
				</View>
				<View style={styles.directionRow}>
				  <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
					<View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.discount} %</Text></View>
				  </View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				</View>
				<View style={styles.borderbottom1}></View>
				<View style={styles.directionRow}>
				<View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(net_premi-dataBeliTravel.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				 </View>
				</View>
			  </View>
			);
		  }
		}else{
		  netpremi.push(
			<View key='netpremi' >
			  <View style={[styles.directionRow, styles.bottom]}>
			  <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
			  </View>
			  <View style={styles.directionRow}>
				<View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
				  <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
				  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.discount} %</Text></View>
				</View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
			  </View>
			  <View style={styles.borderbottom1}></View>
			  <View style={styles.directionRow}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
			  </View>
			</View>
		  );
		}

		}else{
		if(dataBeliTravel.description == 'ok'){
		  if(dataBeliTravel.voucher_persen !== 0){
			  netpremi.push(
				<View key='netpremi' >
				  <View style={[styles.directionRow, styles.bottom]}>
				  <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				  <View style={styles.directionRow}>
				   <View style={[styles.width50p, styles.directionRow]}>
					 <Text style={[styles.italic, styles.baseText, styles.spaceBetween, styles.flex2]}>Discount Voucher</Text>
					 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.voucher_persen} %</Text></View>
				   </View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{(net_premi*dataBeliTravel.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
				  </View>
				  <View style={styles.borderbottom1}></View>
				  <View style={styles.directionRow}>
				  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(net_premi-(net_premi*dataBeliTravel.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				</View>
			);
		  }else{
			netpremi.push(
			  <View key='netpremi' >
				<View style={[styles.directionRow, styles.bottom]}>
				<View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
				</View>
				<View style={styles.directionRow}>
				  <View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliTravel.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				</View>
				<View style={styles.borderbottom1}></View>
				<View style={styles.directionRow}>
				<View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(net_premi-dataBeliTravel.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
				</View>
			  </View>
			);
		  }
		}else{
			  netpremi.push(
				  <View key='totpremi' style={styles.directionRow}>
				  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
			  );
			}
		}

		return(
		<View style={styles.centerItemContent}>
		<View style={styles.borderDetailBeli}>
		<View style={styles.directionRow}>
		  <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		  </View>
		</View>
		{perluasanpremi}
		<View style={styles.borderbottom1}></View>
		{netpremi}
		</View>
		</View>
		);
	}
	
	componentWillReceiveProps(NextProps){
		const {dataTransaksi, asuransiTravel, dataVoucher} = NextProps;
		const kode_transaksi = dataTransaksi.kode_transaksi;
		const insurance_name = asuransiTravel.insurance_name;
		var tax =  null;
		var transactions = {};
		
		const premi = asuransiTravel.premium_charged;
		const disc_premi = premi*asuransiTravel.discount/100;
		const net_premi = asuransiTravel.premium_charged - disc_premi;
		
		if(this.props.dataTransaksi != NextProps.dataTransaksi){		
			if(asuransiTravel.disc_insurance !== 0){
				if(dataVoucher.description == 'ok'){
					if(dataVoucher.voucher_persen !== 0){
						tax = net_premi-(net_premi*dataVoucher.voucher_persen/100); 			
					}else{
						tax = net_premi-dataVoucher.voucher_amount;
					}
				}else{
					tax = net_premi;
				}
			}else{
				if(dataVoucher.description == 'ok'){
					if(dataVoucher.voucher_persen !== 0){
						tax = net_premi-(net_premi*dataVoucher.voucher_persen/100);
					}else{
						tax = net_premi-dataVoucher.voucher_amount;
					}
				}else{
					tax = net_premi;
				}	
			}
			transactions = {kode_transaksi, insurance_name, tax};	
		
			appFunction.setTransactionGA(transactions);
		}	
	}



	render() {

    const {travelState, otherDataTravel, asuransiTravel, dataVoucher,session, dataTransaksi, dataTravel, loadingState, travelLabelState} = this.props;
    const dataBeliTravel = Object.assign({}, travelLabelState, travelState,dataTransaksi, session, asuransiTravel, dataVoucher, dataTravel, otherDataTravel);

    var {height, width} = Dimensions.get('window');
    const {agreement} = this.state;
    //console.log(dataTransaksi.link_pembayaran);
    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira2.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA'    : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }

	if(loadingState.loadingDataTransaksi){
		return(
			<View style={[styles.centerItemContent]}>
				<LoadingPage />
			</View>
		)
	}

    return(
      <View style={styles.centerItem}>
          <View style={styles.centerItem}>
            <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, {marginTop:height*.03,}, styles.elev5]}>
              <View style={styles.left15}><Text style={[styles.baseText]}>Data Pemesan</Text></View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5, styles.borderTop1, styles.borderColorTopGrey]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Nama</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataBeliTravel.visitor_name}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Telpon</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataBeliTravel.visitor_phone}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Email</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataBeliTravel.username}</Text></View>
              </View>
            </View>
          </View>

          <View style={styles.centerItem}>
            <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, {marginTop:height*.03,}, styles.elev5]}>
              <View style={[styles.directionRow, styles.left15, styles.bottom5, styles.borderTop1, styles.borderColorTopWhiteOp8]}>
                <View style={styles.width45p}><Text style={[styles.baseText]}>Nomor Pemesanan</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.kode_transaksi}</Text></View>
              </View>
			  <View style={styles.centerItemContent}>
              <View style={[styles.height70, styles.directionRow, styles.radius2, styles.bottom20, styles.centerItemContent]}>
                <Image source={asimgsrc[dataBeliTravel.insurance_name]} style={[styles.height25, styles.width100, styles.resizeContain]}/>
                <View style={[styles.height70, styles.left10, styles.centerContent]}><Text style={[styles.baseText]}>{dataBeliTravel.insurance_name}</Text></View>
              </View>
			  </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Jenis Liburan</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.label_trip_type}</Text></View>
			  </View>
			  <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Jenis Perjalanan</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.travel_type}</Text></View>
			  </View>
			  { dataBeliTravel.trip_type == 'short' ? dataBeliTravel.travel_type == 'Domestik' ?

			  <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Kota Tujuan</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.label_city}</Text></View>		  
			  </View>
			  :
			  <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Negara Tujuan</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.label_country}</Text></View>
			  </View>
			  : null
			  }
			  
			  <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Tanggal Berangkat</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliTravel.start_date))}</Text></View>
			  </View>
			  <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Tanggal Kembali</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliTravel.end_date))}</Text></View>
			  </View>
			  <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Jenis Paket</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.label_package_type}</Text></View>
			  </View> 
			  { dataBeliTravel.package_type == 'family' ?
			   <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
				<View style={styles.width45p}><Text style={[styles.baseText]}>Daftar Keluarga</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliTravel.label_daftar_keluarga}</Text></View>
			   </View>
			   : null
			  }
			
			<View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Provinsi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width50p}><Text style={[styles.baseText]}>{travelLabelState.label_provinsi}</Text></View>
            </View>
			<View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Kabupaten / Kota</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width50p}><Text style={[styles.baseText]}>{travelLabelState.label_district}</Text></View>
            </View>
			
            {this.detailPremi(dataBeliTravel)}
            <View style={{height: height*.03}}></View>
           </View>
           {this.disclaimertext(dataBeliTravel, agreement)}
         </View>
         <View style={{height: height*.03}}></View>
         <SubmitButton title='Konfirmasi' onPress={() => this.doBayar(agreement)} />
        </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    dataTransaksi: state.dataTransaksi,
    travelState:state.travelState,
	otherDataTravel: state.otherDataTravel,
    asuransiTravel:state.asuransiTravel,
    dataVoucher:state.dataVoucher,
    session:state.session,
    dataTravel: state.dataTravel,
	loadingState: state.loadingState,
	travelLabelState:state.travelLabelState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PembayaranTravel);
