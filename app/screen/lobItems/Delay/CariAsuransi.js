import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import CheckBox from 'react-native-check-box';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import RadioButton from '../../../components/radio/RadioButton';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import DatePicker from 'react-native-datepicker';
//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiDelay extends React.Component {
  constructor(props) {
    super(props);
  }

  toListAsuransi(delayState, dataDelay, otDelayState, lob) {
    if(dataDelay.travel_type == 'Pilih'){
      errorMessage = 'Jenis Perjalanan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.travel_type == 'Domestik' && dataDelay.city_origin == 'Pilih'){
      errorMessage = 'Kota Asal Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.travel_type == 'Domestik' && dataDelay.city_destination == 'Pilih'){
      errorMessage = 'Kota Tujuan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.travel_type == 'Domestik' && dataDelay.city_destination == dataDelay.city_origin){
      errorMessage = 'Kota Tujuan Tidak Boleh Sama Dengan Kota Asal';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.travel_type == 'International' && dataDelay.country_destination == 'Pilih'){
      errorMessage = 'Negara Tujuan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(otDelayState.perjalanan == 'Pilih'){
      errorMessage = 'Perjalanan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if((otDelayState.perjalanan == 'Pulang Pergi' || otDelayState.perjalanan == 'Sekali Jalan') && !dataDelay.start_date){
      errorMessage = 'Tanggal Keberangkatan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(otDelayState.perjalanan == 'Pulang Pergi' && !dataDelay.end_date){
      errorMessage = 'Tanggal Kembali Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!otDelayState.maskapai){
      errorMessage = 'Maskapai Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.kode_booking){
      errorMessage = 'Kode Booking Belum Diisi';
      appFunction.toastError(errorMessage);
    }else{
      const body = Object.assign({}, delayState);
	//console.log(body);
      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }
  
   perjalananlist(otDelayState, lob){
    const title = 'Perjalanan';
	const value = otDelayState.perjalanan;
	
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Sekali Jalan',
      label:'Sekali Jalan'
    },
    {
      key:'Pulang Pergi',
      label:'Pulang Pergi'
    }];

    const type = Object.assign({},{state: 'perjalanan', lob});

    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }
  
  cityoriginlist(delayLabelState, lob){
	var array = [...this.props.listCity]; // make a separate copy of the array
	array.splice(1,1);  
	  
    const title = 'Kota Asal';
    const value = delayLabelState.label_city_origin;
    const options = array;
    const type = Object.assign({},{state: 'city_origin', lob});
	
	


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  countrydestinationlist(delayLabelState, lob){
	var array = [...this.props.listCountry]; // make a separate copy of the array
	array.splice(1,1);  
	
    const title = 'Negara Tujuan';
    const value = delayLabelState.label_country_destination;
    const options = array;
    const type = Object.assign({},{state: 'country_destination', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }
  
  citydestinationlist(delayLabelState, lob){
	var array = [...this.props.listCity]; // make a separate copy of the array
	array.splice(1,1);   
	
    const title = 'Kota Tujuan';
    const value = delayLabelState.label_city_destination;
    const options = array;
    const type = Object.assign({},{state: 'city_destination', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }
  
  traveltypelist(dataDelay, lob){
    const title = 'Jenis Perjalanan';
	const value = dataDelay.travel_type;
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'International',
      label:'International'
    },
    {
      key:'Domestik',
      label:'Domestik'
    }];

    const type = Object.assign({},{state: 'travel_type', lob});

    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }
  
  jenispakettext(delayState){
    const value = delayState.type;
    //console.log('use',value);

    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Jenis Paket
        </Text>

        <TextInput
          underlineColorAndroid='transparent'
          editable={false}
          style={[styles.font16, styles.height40, styles.baseText]}
          keyboardType='numeric'
          //onChangeText={(use) => this.setState({use:use})}
          value={value}
        />
      </View>
    );
  }
  
  tanggalberangkatpicker(dataDelay){

    return(
      <View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Tanggal Berangkat</Text>
        <DatePicker
         style={{width: 200}}
         androidMode="spinner"
         date={dataDelay.start_date}
         mode="date"
         placeholder="Tanggal Berangkat"
         format="YYYY-MM-DD"
         minDate={appFunction.todayDate()}
         maxDate={appFunction.nextYear(10)}
         confirmBtnText="Confirm"
         cancelBtnText="Cancel"
         customStyles={{
           dateIcon: {
           position: 'absolute',
           left: 0,
           top: 4,
           marginLeft: 0
           },
           dateInput: {
             marginLeft: 0,
           }
         }}
         onDateChange={(start_date) => {this.props.setStartDateDtDly(start_date);}}
        />
      </View>
    );
  }
  
    
  tanggalkembalipicker(dataDelay){
	  
	  
    return(
      <View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Tanggal Kembali</Text>
        <DatePicker
         style={{width: 200}}
         androidMode="spinner"
         date={dataDelay.end_date}
         mode="date"
         placeholder="Tanggal Kembali"
         format="YYYY-MM-DD"
         minDate={appFunction.todayDate()}
         maxDate={appFunction.nextYear(10)}
         confirmBtnText="Confirm"
         cancelBtnText="Cancel"
         customStyles={{
           dateIcon: {
           position: 'absolute',
           left: 0,
           top: 4,
           marginLeft: 0
           },
           dateInput: {
             marginLeft: 0,
           }
         }}
         onDateChange={(end_date) => {this.props.setEndDateDtDly(end_date);}}
        />
      </View>
    );
  }
  
  maskapaiinput(otDelayState){
    const value = otDelayState.maskapai;
    return(
      <View style={styles.pickerViewBlack}>
      <Text style= {[styles.font16, styles.baseText]}>
          Maskapai Keberangkatan
        </Text>
        <TextInput
          style={[styles.font16, styles.height40, styles.baseText]}
          // Adding hint in Text Input using Place holder.
          value={value}
          onChangeText={(maskapai) => this.props.setMaskapaiDelay(maskapai.toUpperCase())}
          // Making the Under line Transparent.
          underlineColorAndroid='transparent'
        />
      </View>
    );
  }
  
  kodebookinginput(dataDelay){
    const value = dataDelay.kode_booking;
    return(
      <View style={styles.pickerViewBlack}>
      <Text style= {[styles.font16, styles.baseText]}>
          Kode Booking
        </Text>
        <TextInput
          style={[styles.font16, styles.height40, styles.baseText]}
          // Adding hint in Text Input using Place holder.
          value={value}
          onChangeText={(kode_booking) => this.props.setKodeBookingDtDly(kode_booking.toUpperCase())}
          // Making the Under line Transparent.
          underlineColorAndroid='transparent'
        />
      </View>
    );
  }
  
  destination(dataDelay, delayLabelState, lob){
	if(dataDelay.travel_type !== 'Pilih'){
		if(dataDelay.travel_type == 'International'){
			return(
			<View style={[styles.width100p, styles.centerItemContent]}>				
				{this.countrydestinationlist(delayLabelState, lob)}
			</View>
			);
		}else{
			return(
			<View style={[styles.width100p, styles.centerItemContent]}>
				{this.cityoriginlist(delayLabelState, lob)}
				{this.citydestinationlist(delayLabelState, lob)}
			</View>	
			);
		}
	}else{
		return(
		<View style={[styles.width100p, styles.centerItemContent]}>
			{this.cityoriginlist(delayLabelState, lob)}
			{this.citydestinationlist(delayLabelState, lob)}
		</View>
		);
	} 
  }
  
  viewtanggalkembali(dataDelay, otDelayState){
	if(otDelayState.perjalanan == 'Sekali Jalan'){
		return null;
	}else{
		return(
		<View style={[styles.width100p, styles.centerItemContent]}>
			{this.tanggalkembalipicker(dataDelay)}
		</View>
		);
	}
  }
  

  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };
  
  componentWillReceiveProps(NextProps){
	  const {dataDelay} = this.props;
	  const {otDelayState} = this.props;
	  const nextDataDelay = NextProps.dataDelay;
	  const nextOtDelayState = NextProps.otDelayState;
	
	  if(nextDataDelay != dataDelay){
		  if(nextDataDelay.travel_type != dataDelay.travel_type){
			  if(nextDataDelay.travel_type == 'Pilih'){
				this.props.resetCityOriginDtDly();
				this.props.setLabelCityOriginDelay('Pilih');
				this.props.setCityDestinationDtDly('Pilih');
				this.props.setLabelCityDestinationDelay('Pilih');
				this.props.setCountryDestinationDtDly('Pilih');
				this.props.setLabelCountryDestinationDelay('Pilih'); 
				this.props.resetListCity();
				this.props.resetListCountry();
			  }else if(nextDataDelay.travel_type == 'International'){
				this.props.resetCityOriginDtDly();
				this.props.setLabelCityOriginDelay('Pilih');
				this.props.setCityDestinationDtDly('Pilih');
				this.props.setLabelCityDestinationDelay('Pilih');  
				this.props.resetListCity();
				this.props.getCountry();
			  }else if(nextDataDelay.travel_type == 'Domestik'){
				this.props.setCountryDestinationDtDly('Pilih');
				this.props.setLabelCountryDestinationDelay('Pilih'); 
				this.props.resetListCountry();
				this.props.getCity();
			  }
		  }
		 
		  
	  }
	  
	  if(nextOtDelayState != otDelayState){
		   if(nextOtDelayState.perjalanan != otDelayState.perjalanan){
			  if(nextOtDelayState.perjalanan == 'Sekali Jalan'){
				this.props.setEndDateDtDly(null);
			  }
		  }
	  }
	  
  }


  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    this.props.getTokenLocalStorage().done();
	
	if(this.props.dataDelay.city_destination == ''){
		this.props.setCityDestinationDtDly('Pilih');	
	}
	if(this.props.dataDelay.city_origin == ''){
		this.props.setCityOriginDtDly('Pilih');	
		
	}
	if(this.props.dataDelay.country_destination == ''){
		this.props.setCountryDestinationDtDly('Pilih');
	}

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {delayState, delayLabelState, otDelayState, dataDelay} = this.props;
    const lob = 'Delay';
    //const {listPlatKendaraan} = this.props;
    //console.log(delayState);
    //console.log(dataDelay);
	

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
		{this.traveltypelist(dataDelay, lob)}
		{this.destination(dataDelay, delayLabelState, lob)}
        {this.perjalananlist(otDelayState, lob)}
        {this.maskapaiinput(otDelayState)}
		{this.tanggalberangkatpicker(dataDelay)}
		{this.viewtanggalkembali(dataDelay, otDelayState)}
		{this.kodebookinginput(dataDelay)}
		{this.jenispakettext(delayState)}
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(delayState, dataDelay, otDelayState, lob)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
	otDelayState: state.otDelayState,
    delayState: state.delayState,
    dataDelay:state.dataDelay,
    delayLabelState: state.delayLabelState,
	listCity: state.listCity,
	listCountry: state.listCountry,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiDelay);
