import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

//import { TextInputMask } from 'react-native-masked-text'
import PopoverTooltip from 'react-native-popover-tooltip';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class ListAsuransiDelay extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      modalVisible:false,
    }
  }

  usetooltip(text){
    var {height, width} = Dimensions.get('window');
    return(
      <PopoverTooltip
        buttonComponent={
          <Icon name="ios-information-circle-outline" style={styles.colorRed} />
        }
        items={[
          {
            label: text,
            onPress: () => {}
          }
        ]}
        tooltipContainerStyle = {{ width: width*.8, }}
        // animationType='timing'
        // using the default timing animation
       />
     );
  }

  doBeli(arrListAsuransi){
    this.props.setAsuransiDelay(arrListAsuransi);
    this.toNextScreen();
  }

  toNextScreen(){
    if(this.props.status.isLogin == false){
      this.props.navigate('Login');
    }else{
      this.props.navigate('DetailBeli');
    }
  }

  openModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:true});
    }
  }

  closeModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:false});
    }
  }

 renderListAsuransi(listAsuransi){
    var arrListAsuransi = [];

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	
	const discsrc = {
      '5'  : require('../../../assets/icons/discount/disc-5.png'),
      '10' : require('../../../assets/icons/discount/disc-10.png'),
      '15' : require('../../../assets/icons/discount/disc-15.png'),
      '20' : require('../../../assets/icons/discount/disc-20.png'),
	  '25' : require('../../../assets/icons/discount/disc-25.png'),
    }

    var asuransi = [];
    var totalPremi = [];
    var netPremi = [];
    //var premiDasar = [];
    var packetName = [];
    var packetID = [];
    var discount = [];
    var showTotalPremi = [];

    if(listAsuransi.length > 0){
      for (var i = 0; i < listAsuransi.length; i++) {
         asuransi.push(listAsuransi[i].insurance_name);
         totalPremi.push(listAsuransi[i].premium_charged);
         netPremi.push(listAsuransi[i].premium_charged - (listAsuransi[i].premium_charged*listAsuransi[i].disc_insurance/100));
         //premiDasar.push(listAsuransi[i].premi_dasar);
         packetName.push(listAsuransi[i].package_name);
         packetID.push(listAsuransi[i].package_id);
         discount.push(listAsuransi[i].disc_insurance);
      }

       for(let i = 0; i < asuransi.length; i++){
         if(discount[i] > 0){
           showTotalPremi.push(
             <View key = {i}>
               <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}Total:{"\n"}{"\n"}
                </Text>
                <View style={styles.directionColumn}>
                  <Text style={[styles.font12, styles.strike]}>
                    {"\n"}Rp. {totalPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
                  </Text>
                  <Text style={[styles.font14, styles.bold, styles.baseText]}>
                    Rp. {netPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}{"\n"}{"\n"}
                  </Text>
                </View>
              </View>
            </View>
          );
        }else {
          showTotalPremi.push(
            <View key = {i}>
               <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}Total:{"\n"}{"\n"}
                </Text>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}{"\n"}Rp. {netPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}{"\n"}{"\n"}
                </Text>
                <Text style={[styles.baseText]}>{"\n"}{"\n"}</Text>
              </View>
            </View>
          );
        }

        arrListAsuransi.push(
          <View style={[styles.centerItemContent, styles.top10]} key = {i}>
             {discount[i] ? 
			<View style={[styles.directionRow, styles.flexEndSelf, {position:'absolute', top:-15, zIndex: 1}]}>
			  <View style={[styles.directionRow]}> 
				<View>
				 <Image style={[styles.width70, styles.height70]} source={discsrc[discount[i].toString()]} />
				</View>
			  </View>
			</View>
			: null}
	
              <View style={[styles.bgWhite, styles.radius5, styles.bottom10, styles.top10, styles.pad5, { zIndex: 0}]}> 
              <View style={[styles.directionRow, styles.left5]}>
                <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                  <TouchableHighlight>
                    <View style={styles.centerItemContent}>
                       <View style={[styles.centerItemContent, styles.height50]}>
                           <Image source={asimgsrc[asuransi[i]]} style={[styles.height25, styles.width100]}/>
                        </View>
                      </View>
                    </TouchableHighlight>
                    <View style={styles.bottom10}></View>
                  </View>
                  <View style={styles.width55p}>
                    <View style={styles.directionColumn}>
                      <View style={styles.directionColumn}>
                        <Text style={[styles.font12, styles.bold, styles.baseText]}>
                          {asuransi[i]}{"\n"}{"\n"}
                        </Text>
                        <View style={[styles.borderTransparent, styles.radius2, styles.border1, styles.radius2]}>
                          <Text style={[styles.font10, styles.bold]}>Paket: </Text>
                          <Text style={[styles.font10, styles.bold]}>{packetName[i]} </Text>
                        </View>
                      </View>
                      {showTotalPremi[i]}
                      <View style={[styles.directionRow, styles.bottom10]}>
                        
                        <View>
                          <Button underlayColor='blue'
                            style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                            onPress={() => this.doBeli(listAsuransi[i])}>
                            <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Beli</Text>
                          </Button>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )
        }
      }else{
        arrListAsuransi.push(
          <View style={styles.centerItemContent} key = 'listasuransi'>
            <View style={[styles.bgWhite, styles.bottom5, styles.radius5, styles.top10, styles.width85p]}>
              <View style={styles.centerItemContent}>
                <Text style={[styles.baseText]}>Tidak Ada Asuransi Yang Sesuai Dengan Data Yang Anda Input.</Text>
              </View>
            </View>
          </View>

        );
      }
      return arrListAsuransi;
    }


   componentDidMount() {
	if(this.props.dataDelay.city_destination == 'Pilih'){
		this.props.setCityDestinationDtDly('');	
	}
	if(this.props.dataDelay.city_origin == 'Pilih'){
		this.props.setCityOriginDtDly('');	
		
	}
	if(this.props.dataDelay.country_destination == 'Pilih'){
		this.props.setCountryDestinationDtDly('');
	}
   }

  render(){
    var {height, width} = Dimensions.get('window');
    const {listAsuransi, itemsIsLoading, loadingState} = this.props;
    //console.log('render',arrListAsuransi);

    return(
      <ScrollView>
	  { 	loadingState.loadingListAsuransiDelay ?
           <LoadingPage /> :
			 this.renderListAsuransi(listAsuransi)
      }
       
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    listAsuransi: state.listAsuransi,
    status: state.status,
    itemsIsLoading:state.itemsIsLoading,
	loadingState:state.loadingState,
	dataDelay:state.dataDelay,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAsuransiDelay);
