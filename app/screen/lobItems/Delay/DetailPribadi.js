import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Radio, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';

class DetailPribadiDelay extends Component{

  constructor(props) {
    super(props);
    this.state={
      awvisible:false,
      dsvisible:false,
      prvisible:false,
	  warganegara: 'wni',
    }
  }
  
  selectwarganegara(warganegara){  
	  this.props.setKitasDtDly(warganegara);
  }

  kewarganegaraanradio(kewarganegaraan){
    
    var init1 = true;
	var init2 = false;
	var value = 'wni'; 
    if(kewarganegaraan == 'wni'){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
    return(
	<View>
		<View style={[styles.directionRow]}>
			<Text>Kewarganegaraan</Text>
		</View>
		 <View style={[styles.directionRow]}>
		  <View style={[styles.directionRow, styles.right10]}>
			  <Radio selected={init1} onPress={() => {this.selectwarganegara('wni')}} />
		   <Text>WNI</Text>
		   </View>
		   <View style={[styles.directionRow]}>
			 <Radio selected={init2} onPress={() => {this.selectwarganegara('wna')}} />
			 <Text>WNA</Text>
			</View>
		</View>
	  </View>
    );
  }
  
  selectgender(value){	  
	  this.props.setGenderDtDly(value);
  }

  genderradio(member_gender){
    
    var init1 = true;
	var init2 = false;
	var value = 'L'; 
    if(member_gender == 'L'){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
    return(
	<View>
		<View style={[styles.directionRow]}>
			<Text>Jenis Kelamin</Text>
		</View>
		 <View style={[styles.directionRow]}>
		  <View style={[styles.directionRow, styles.right10]}>
			  <Radio selected={init1} onPress={() => {this.selectgender('L')}} />
		   <Text>Laki-Laki</Text>
		   </View>
		   <View style={[styles.directionRow]}>
			 <Radio selected={init2} onPress={() => {this.selectgender('P')}} />
			 <Text>Perempuan</Text>
			</View>
		</View>
	  </View>
    );
  }
  
  birthdatepicker(birth_date){

	  var now = new Date();
	  var today = now.toISOString().substring(0, 10); // 30-Dec-2011
	  
	  var d = new Date();
		var year = d.getFullYear();
		var month = d.getMonth();
		var day = d.getDate();
		var nextDateYear = new Date(year + 1, month, day);
	  
    return(
      <View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Tanggal Lahir</Text>
        <DatePicker
         style={{width: 200}}
         androidMode="spinner"
         date={birth_date}
         mode="date"
         placeholder="Tanggal Lahir"
         format="YYYY-MM-DD"
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
         confirmBtnText="Confirm"
         cancelBtnText="Cancel"
         customStyles={{
           dateIcon: {
           position: 'absolute',
           left: 0,
           top: 4,
           marginLeft: 0
           },
           dateInput: {
             marginLeft: 0,
           }
         }}
         onDateChange={(birth_date) => {this.props.setTglLahirDtDly(birth_date);}}
        />
      </View>
    );
  }



  ahliwarislist(member_hubahliwaris){
    const lob = 'Delay';
    const title = 'Hubungan Ahli Waris';
    const value = member_hubahliwaris;
    const options = this.props.listAhliWaris;
    const type = Object.assign({},{state: 'hub_ahliwaris', lob});


    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
        bordered={true}
      />
    );
  }

  provinsilist(delayLabelState){
      const lob = 'Delay';
    const title = 'Provinsi';
    const value = delayLabelState.label_provinsi;
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
      <FilterLabelPicker title={title}

        value={value}
        options={options}
        type={type}
        bordered={true}
      />
    );
  }


  districtlist(delayLabelState){
      const lob = 'Delay';
    const title = 'Kabupaten / Kota';
    const value = delayLabelState.label_district;
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
        bordered={true}
      />
    );
  }


  titlelist(value){
    const title = 'Title';
    const lob = 'Delay';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
        bordered={true}
      />
    );
  }

  doPostData(body, dataDelay){
    var errorMessage =  '';
	
    if(dataDelay.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.first_name){
      errorMessage = 'Nama Depan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_gender){
      errorMessage = 'Jenis Kelamin Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.birth_date){
      errorMessage = 'Tanggal Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_kitas){
      errorMessage = 'Kewarganegaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_idno){
      errorMessage = 'Nomor Identitas Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.tempat_lahir){
      errorMessage = 'Tempat Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.province_id == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.district_id == 'Pilih'){
      errorMessage = 'Kabupaten/Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    }
  
    //else if(!dataDelay.photo_ktp){
    //  errorMessage = 'Photo Identitas Belum Diupload';
    //  appFunction.toastError(errorMessage);
    //}
	
	else if(!dataDelay.member_phone){
      errorMessage = 'Telp Rumah Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_hp){
      errorMessage = 'Telp Seluler Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_email){
      errorMessage = 'E-Mail Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_alamat){
      errorMessage = 'Alamat Rumah Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataDelay.member_ahliwaris){
      errorMessage = 'Ahli Waris Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(dataDelay.member_hubahliwaris == 'Pilih'){
      errorMessage = 'Hubungan Ahli Waris Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else{
		//console.log(body);
      this.props.doPostDataDelay(body);
      this.props.navigate('Pembayaran');
	  //appFunction.toastError('sukses');
    }
  }



  _pickImgktp = async () => {
    await ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      includeBase64: true,
      mediaType:'photo',
      }).then(image => {
        if(image.size / 1024 <= 20480){
          this.props.setPhotoKtpDtDly(image.data);
        }else{
          appFunction.toastError('Foto Harus Kurang Dari 20MB');
        }
    });
  }

  componentWillReceiveProps(NextProps){
    if(NextProps.dataDelay.province_id != this.props.dataDelay.province_id)
      if(NextProps.dataDelay.province_id != 'Pilih')
        this.props.getDistrict(NextProps.dataDelay.province_id);
        else {
          this.props.resetDistrictDtDly();
          this.props.resetListDistrict();
        }
  }

  componentDidMount(){
    this.props.getAhliWaris();
    this.props.getProvinsi();
  }


	render() {
    var {height, width} = Dimensions.get('window');
    const {dataDelay, session, delayState, dataVoucher, asuransiDelay, delayLabelState} = this.props;
	var {warganegara} = this.state;
	
	

    const body = Object.assign({}, delayState);

    const dataPostDelay = Object.assign({},dataDelay,
      {visitor_id:session.visitor_id},
      {kode_voucher:dataVoucher.kode_voucher},
     );

     const postData =  Object.assign({},{data_delay:dataPostDelay}, body, {package_id:asuransiDelay.package_id });
    console.log(postData);

    const {
	  birth_date,
	  city_destination,
	  city_origin,
	  country_destination,
      district_id,
      end_date,
      first_name,
      kode_booking,
      kode_voucher,
      last_name,
      member_ahliwaris,
      member_alamat,
      member_email,
      member_gender,
      member_hp,
      member_hubahliwaris,
      member_idno,
      member_kitas,
      member_phone,
      mobile_apps,
      no_ktp,
      photo_ktp,
      province_id,
      start_date,
      tempat_lahir,
      tittle,
      travel_type,
      visitor_id,
    }  = this.props.dataDelay;
    var init = 0;

    let imageUriKtp = photo_ktp ? `data:image/jpg;base64,${photo_ktp}` : null;
    //imageUri && console.log({uri: imageUri.slice(0, 100)});

	 var ktpkitas = 'KTP';
    if(member_kitas == 'wni'){
      ktpkitas = 'KTP';
      init = 0;
    }else{
      ktpkitas = 'KITAS';
      init = 1;
    }

   
    



	  return (

      <View style = {[styles.centerItemContent]}>

                <View style={[styles.detailPribadiMainContainer]}>

                    <View style = {[styles.width40p]}>
                    {this.titlelist(tittle)}
                  </View>

                    <Text style={[styles.baseText]}>Nama Depan</Text>               
            	        <TextInput
                        value={first_name}
            	          // Adding hint in Text Input using Place holder.
            	          onChangeText={(first_name) => this.props.setFirstNameDtDly(appFunction.alphabetSpaceOnly(first_name))}
            	          // Making the Under line Transparent.
            	          underlineColorAndroid='transparent'
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
            	        />
						<View style={[styles.top10]} />
						<Text style={[styles.baseText]}>Nama Belakang</Text>               
            	        <TextInput
                        value={last_name}
            	          // Adding hint in Text Input using Place holder.
            	          onChangeText={(last_name) => this.props.setLastNameDtDly(appFunction.alphabetSpaceOnly(last_name))}
            	          // Making the Under line Transparent.
            	          underlineColorAndroid='transparent'
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
            	        />
						<View style={[styles.top10]} />
					  <View style={[{width:'80%'}]}>
						  {this.genderradio(member_gender)}
					  </View>
					  <View style={[styles.top10]} />
					  
						  {this.birthdatepicker(birth_date)}
						  
						  <View style={[styles.top10]} />
					  <View style={[{width:'80%'}]}>
						  {this.kewarganegaraanradio(member_kitas)}
					  </View>
						  
						  <View style={[styles.top10]} />
                      <Text style={[styles.baseText]}>No {ktpkitas}</Text>
                   
                      <TextInput
                        // Adding hint in Text Input using Place holder.
                        value={member_idno}
                        keyboardType='numeric'
                        onChangeText={(member_idno) => this.props.setIdNoDtDly(appFunction.alphaNumericDashOnly(member_idno))}
                        // Making the Under line Transparent.
                        underlineColorAndroid='transparent'
                        maxLength={16}
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
                      />
					  
					  <View style={[styles.top10]} />
						<Text style={[styles.baseText]}>Tempat Lahir</Text>               
            	        <TextInput
                        value={tempat_lahir}
            	          // Adding hint in Text Input using Place holder.
            	          onChangeText={(tempat_lahir) => this.props.setTempatLahirDtDly(appFunction.alphabetSpaceOnly(tempat_lahir))}
            	          // Making the Under line Transparent.
            	          underlineColorAndroid='transparent'
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
            	        />
						  
						  <View style={[styles.top10]} />
                        {this.provinsilist(delayLabelState)}
                          <View style={[styles.top10]} />
                        {this.districtlist(delayLabelState)}
						
						<View style={[styles.top10]} />

                        <Text style={[styles.baseText]}>{ktpkitas}</Text>
                        <View style={[styles.border1, styles.borderColorGrey, styles.directionColumn]}>
                         <View style={[styles.directionRow]}>
                            <View style={[styles.directionRow, styles.width80p]}>
                              <Image style={[styles.width25, styles.height25]} source={require('../../../assets/icons/button/upload.png')}/>
                              <Text>{photo_ktp ? 'Sudah Terupload' : 'Belum Upload'}</Text>
                            </View>
                            <View>
                              <UploadButton title='Upload' onPress={this._pickImgktp} />
                            </View>
                          </View>
                           {photo_ktp
                             ? <Image
                                 source={{uri: imageUriKtp}}
                                 style={[styles.width200, styles.height150]}
                               />
                             : null}
                         </View>
						 
						 <View style={[styles.top10, styles.bottom10, styles.width100p]} >
							<Text style={[styles.bold]}>Data Pendukung :</Text>
						 </View>
						 
						 <View style={[styles.top10]} />
                      <Text style={[styles.baseText]}>Telp Rumah</Text>
                   
                      <TextInput
                        // Adding hint in Text Input using Place holder.
                        value={member_phone}
                        keyboardType='numeric'
                        onChangeText={(member_phone) => this.props.setPhoneDtDly(appFunction.numericPlusOnly(member_phone))}
                        // Making the Under line Transparent.
                        underlineColorAndroid='transparent'
                        maxLength={16}
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
                      />
            
					 <View style={[styles.top10]} />
                      <Text style={[styles.baseText]}>Telp Seluler</Text>
                   
                      <TextInput
                        // Adding hint in Text Input using Place holder.
                        value={member_hp}
                        keyboardType='numeric'
                        onChangeText={(member_hp) => this.props.setHandphoneDtDly(appFunction.numericPlusOnly(member_hp))}
                        // Making the Under line Transparent.
                        underlineColorAndroid='transparent'
                        maxLength={16}
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
                      />
					  
					  <View style={[styles.top10]} />
                      <Text style={[styles.baseText]}>E-Mail</Text>
                   
                      <TextInput
                        // Adding hint in Text Input using Place holder.
                        value={member_email}
                        onChangeText={(member_email) => this.props.setEmailDtDly(appFunction.emailCharacterOnly(member_email))}
                        // Making the Under line Transparent.
                        underlineColorAndroid='transparent'
                        maxLength={50}
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
                      />
					  
                      <View style={[styles.top10]} />
                    <Text style={[styles.baseText]}>Alamat Rumah</Text>
            	        <TextInput
            	          // Adding hint in Text Input using Place holder.
                        value={member_alamat}
            	          onChangeText={(member_alamat) => this.props.setAlamatDtDly(appFunction.dotalphaNumericOnly(member_alamat))}
                        multiline = {true}
                        numberOfLines = {8}
            	          // Making the Under line Transparent.
            	          underlineColorAndroid='transparent'
                        style={[styles.baseText, styles.border1, styles.borderColorGrey, styles.padv0]}
            	        />
                                  
                      <View style={[styles.top10]} />
                          <Text style={[styles.baseText]}>Nama Ahli Waris</Text>
                        
                          <TextInput
                            value={member_ahliwaris}
                            // Adding hint in Text Input using Place holder.
                            onChangeText={(member_ahliwaris) => this.props.setNamaAhliWarisDtDly(member_ahliwaris)}
                            // Making the Under line Transparent.
                            underlineColorAndroid='transparent'
                            style={[styles.baseText,styles.border1, styles.borderColorGrey, styles.padv0]}
                          />
                        
                          <View style={[styles.top10]} />
                        {this.ahliwarislist(member_hubahliwaris)}
						
                          

                        
                </View>

              <View style={{height: height*.05}}></View>
              <SubmitButton title='Submit' onPress={() => this.doPostData(postData,dataDelay)} />
            </View>

	  );
  }
}

function mapStateToProps(state) {
  return {
    dataDelay: state.dataDelay,
    delayState: state.delayState,
    delayLabelState: state.delayLabelState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    asuransiDelay: state.asuransiDelay,
    listAhliWaris: state.listAhliWaris,
    listProvinsi: state.listProvinsi,
    listDistrict:state.listDistrict,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiDelay);
