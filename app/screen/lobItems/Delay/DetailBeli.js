import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, Button, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';


class DetailBeliDelay extends React.Component {
  constructor(props){
     super(props);
   }


toDetailPribadi(){
  this.props.navigate('DetailPribadi');
}


detailPremi(dataBeliDelay){
  var perluasanpremi = [];
  var netpremi = [];
  var discvoucher=[];
  
  const premi = dataBeliDelay.premium_charged;
  const net_premi = dataBeliDelay.premium_charged - (dataBeliDelay.premium_charged * dataBeliDelay.disc_insurance/100);

  var disc_premi = premi*dataBeliDelay.disc_insurance/100;

  if(dataBeliDelay.disc_insurance !== 0){
    if(dataBeliDelay.description == 'ok'){
      if(dataBeliDelay.voucher_persen !== 0){
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
             </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliDelay.voucher_persen} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(premi*dataBeliDelay.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliDelay.disc_insurance} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-(net_premi*dataBeliDelay.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
          </View>
        );
      }else{
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
            <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliDelay.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>	
            </View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliDelay.disc_insurance} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
	          </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-dataBeliDelay.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
             </View>
			</View>
          </View>
        );
      }
    }else{
      netpremi.push(
        <View key='netpremi' >
          <View style={[styles.directionRow, styles.bottom]}>
          <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
          </View>
		  </View>
          <View style={styles.directionRow}>
            <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
              <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
              <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliDelay.disc_insurance} %</Text></View>
            </View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
          </View>
          <View style={styles.borderbottom1}></View>
          <View style={styles.directionRow}>
          <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.bold, styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		  </View>
          </View>
        </View>
      );
  }

  }else{
    if(dataBeliDelay.description == 'ok'){
      if(dataBeliDelay.voucher_persen !== 0){
          netpremi.push(
            <View key='netpremi' >
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
              <View style={styles.directionRow}>
               <View style={[styles.width50p, styles.directionRow]}>
                 <Text style={[styles.italic, styles.baseText, styles.spaceBetween, styles.flex2]}>Discount Voucher</Text>
                 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliDelay.voucher_persen} %</Text></View>
               </View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(net_premi*dataBeliDelay.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
               </View>
			  </View>
              <View style={styles.borderbottom1}></View>
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-(net_premi*dataBeliDelay.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
			  </View>
            </View>
        );
      }else{
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
            </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliDelay.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-dataBeliDelay.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
          </View>
        );
      }
    }else{
          netpremi.push(
              <View key='totpremi' style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
          );
        }
  }

  return(
    <View style={styles.centerItemContent}>
  <View style={styles.borderDetailBeli}>
    <View style={styles.directionRow}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
	  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
		<Text style={[styles.baseText]}>:  Rp. </Text>
		<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
	  </View>
    </View>
    {perluasanpremi}
    <View style={styles.borderbottom1}></View>
    {netpremi}
  </View>
</View>
);
}


       componentDidMount() {
         //this.getLocalStorage().done();

       }

  render(){

    const {otDelayState, delayState, asuransiDelay, dataVoucher, delayLabelState, dataDelay} = this.props;
    const dataBeliDelay = Object.assign({}, delayLabelState, delayState, asuransiDelay, dataVoucher, dataDelay, otDelayState);
    //console.log('label',dataVoucher);
	const lob = Object.assign({}, {lob : 'AC'}); 
	const dataCheckVoucher = Object.assign({},dataVoucher, asuransiDelay, lob);
      var {height, width} = Dimensions.get('window');
      var jenis_kelamin = 'Laki-Laki';
      if(dataBeliDelay.gender == 'L'){
        jenis_kelamin = 'Laki-Laki';
      }else{
        jenis_kelamin = 'Perempuan';
      }

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }



    return(
      <View style={styles.centerItemContent}>
        <View style={styles.centerItem}>
        <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.bottom10, styles.left4, styles.radius3, styles.elev2, styles.pad5, {borderColor: '#ffe5e5', marginTop:height*.1, paddingTop:0}]}>
          <View style={styles.centerItemContent}>
           <View style={[styles.centerItemContent, styles.height70, styles.directionRow, styles.radius2, styles.bottom20]}>
             <Image source={asimgsrc[dataBeliDelay.insurance_name]} style={[styles.height25, styles.width100]}/>
             <View style={[styles.left10, styles.bgRed, styles.height70, styles.centerContent, styles.padlr5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliDelay.insurance_name}</Text></View>
           </View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Perjalanan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.travel_type}</Text></View>
          </View>
		  { dataBeliDelay.travel_type == 'Domestik' ?
		  <View style={[styles.width100p]}>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Kota Asal</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.label_city_origin}</Text></View>
          </View>
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Kota Tujuan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.label_city_destination}</Text></View>
          </View>
		  </View>
		  :
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Negara Tujuan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.label_country_destination}</Text></View>
          </View>
		  }
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Perjalanan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.perjalanan}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Maskapai Keberangkatan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.maskapai}</Text></View>
          </View>
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Tanggal Berangkat</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliDelay.start_date))}</Text></View>
          </View>
		  {
			  dataBeliDelay.perjalanan == 'Pulang Pergi' ? 
			  <View style={[styles.directionRow, styles.bottom]}>
				<View style={styles.width50p}><Text style={[styles.baseText]}>Tanggal Kembali</Text></View>
				<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
				<View style={styles.width45p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliDelay.end_date))}</Text></View>
			  </View>
			  : null
		  }
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Kode Booking</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.kode_booking}</Text></View>
          </View>
		   <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Paket</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.type}</Text></View>
          </View>
           <View style={[styles.directionRow, styles.bottom]}>
             <View style={styles.width50p}><Text style={[styles.baseText]}>Provide By</Text></View>
             <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
             <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliDelay.insurance_name}</Text></View>
           </View>

           <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kode Voucher</Text></View>
               <Text style={[styles.baseText]}>:  </Text>
             <TextInput
               style={[styles.kodeVoucherTInput, styles.baseText]}
               value={dataVoucher.kode_voucher}
               onChangeText={(kode_voucher) => this.props.setKodeVoucher(appFunction.alphaNumericOnly(kode_voucher))}
               underlineColorAndroid='transparent'
             />
           <TouchableHighlight underlayColor='white' onPress={() => this.props.checkVoucher(dataCheckVoucher, 'checkVoucher')}>
              <Icon name='checkmark-circle' color='red' />
            </TouchableHighlight>
           </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}></View>
                <Text style={[styles.colorRed, styles.font12, styles.italic, styles.baseText]}>{dataBeliDelay.description == 'ok'? 'Status Voucher Valid' : dataBeliDelay.description}</Text>
            </View>
           {this.detailPremi(dataBeliDelay)}
           <View style={{height: height*.03}}></View>
         </View>
      </View>
        <SubmitButton title='Lanjut' onPress={() => this.toDetailPribadi()} />
       </View>

    );
  }

}

function mapStateToProps(state) {
  return {
    delayState: state.delayState,
    asuransiDelay: state.asuransiDelay,
    delayLabelState: state.delayLabelState,
    dataVoucher: state.dataVoucher,
    dataDelay: state.dataDelay,
	otDelayState:state.otDelayState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeliDelay);
