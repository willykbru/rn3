import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Radio, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import UploadImage from '../../../components/upload/UploadImage';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';
import WheelPicker from '../../../components/picker/WheelPicker';

//import ImagePicker from 'react-native-image-picker';
//import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';

class DetailPribadiTravel extends Component{

  constructor(props) {
    super(props);
    this.state={
      ppvisible:true,
      psvisible:false,
      apvisible:false,
      advisible:false,
      atvisible:false,
	  dpvisible:true,
	  borderCol : 'grey',
    }
  }
  
  
  openPerluasan(visible, status) {
	  //this.refs.scrollView.scrollToEnd(); // will scroll to the top at y-position 0  
	if(status == 'pemegang_polis')
		this.setState({ppvisible: !visible}); 
	if(status == 'pasangan')
		this.setState({psvisible: !visible});
	if(status == 'anak_pertama')
		this.setState({apvisible: !visible});
	if(status == 'anak_kedua')
		this.setState({advisible: !visible});
	if(status == 'anak_ketiga')
		this.setState({atvisible: !visible});
	if(status == 'data_pendukung')
		this.setState({dpvisible: !visible});
	}
  
  statustitletext(value, status){ 
	var visible = false;
	
	if(status == 'pemegang_polis')
		visible = this.state.ppvisible;
	if(status == 'pasangan')
		visible = this.state.psvisible;
	if(status == 'anak_pertama')
		visible = this.state.apvisible;
	if(status == 'anak_kedua')
		visible = this.state.advisible;
	if(status == 'anak_ketiga')
		visible = this.state.atvisible;
	if(status == 'data_pendukung')
		visible = this.state.dpvisible;  
	  
	  return(
		<View style={[styles.bgWhite, styles.top5, styles.elev1 ,styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent]}>
				<View style={[styles.width80p, styles.left10]}><Text style={[styles.baseText, styles.font11, styles.right5, styles.padTop5, styles.bold]}>{value}</Text></View>
				<View style={[styles.flexEndSelf, styles.width20p, styles.left10]}><TouchableHighlight underlayColor='transparent' onPress={()=>{this.openPerluasan(visible, status)}}>{visible? <Icon name='ios-arrow-dropup-outline' /> : <Icon name='ios-arrow-dropdown-outline' />}</TouchableHighlight></View>
			</View>
		</View>
	  );
  }
  
  setTitle(listDataTravel){
	  const title = listDataTravel.dataPemegangPolisTravel.tittle;
	  if(title == 'Mr.')
		this.props.setTittlePsDtTrv('Mrs.');
	  else if(title == 'Mrs.')
		this.props.setTittlePsDtTrv('Mr.');
  }
  
  titlelist(listDataTravel){
    const title = 'Title';
    const lob = 'Travel';
	const value = listDataTravel.dataPemegangPolisTravel.tittle;
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	  <View style = {[styles.width100p]}>
		  <FilterPicker title={title}
			value={value}
			options={options}
			type={type}
			
			useFunction={true}
			onPick={(value)=> this.setTitle(listDataTravel)}
		  />
	  </View>
    );
  }
  
  setnamadepan(value, status){  
	if(status == 'pemegang_polis'){
		this.props.setFirstNamePpDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'pasangan'){
		this.props.setFirstNamePsDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'anak_pertama'){
		this.props.setFirstNameApDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'anak_kedua'){
		this.props.setFirstNameAdDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'anak_ketiga'){
		this.props.setFirstNameAtDtTrv(appFunction.alphabetSpaceOnly(value));
	}	  
  }
  
  setnamabelakang(value, status){  
	if(status == 'pemegang_polis'){
		this.props.setLastNamePpDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'pasangan'){
		this.props.setLastNamePsDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'anak_pertama'){
		this.props.setLastNameApDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'anak_kedua'){
		this.props.setLastNameAdDtTrv(appFunction.alphabetSpaceOnly(value));
	}
	if(status == 'anak_ketiga'){
		this.props.setLastNameAtDtTrv(appFunction.alphabetSpaceOnly(value));
	}	  
  }
  
  selectgender(value, status){	  
	if(status == 'pemegang_polis'){
		this.props.setGenderPpDtTrv(value);
	}
	if(status == 'pasangan'){
		this.props.setGenderPsDtTrv(value);
	}
	if(status == 'anak_pertama'){
		this.props.setGenderApDtTrv(value);
		if(value == 'L'){
			this.props.setTittleApDtTrv('Mr.');
		}else{
			this.props.setTittleApDtTrv('Ms.');
		}
	}
	if(status == 'anak_kedua'){
		this.props.setGenderAdDtTrv(value);
		if(value == 'L'){
			this.props.setTittleAdDtTrv('Mr.');
		}else{
			this.props.setTittleAdDtTrv('Ms.');
		}
	}
	if(status == 'anak_ketiga'){
		this.props.setGenderAtDtTrv(value);
		if(value == 'L'){
			this.props.setTittleAtDtTrv('Mr.');
		}else{
			this.props.setTittleAtDtTrv('Ms.');
		}
	}	  
  }
  
  setbirthdate(value, status){	  
	if(status == 'pemegang_polis'){
		this.props.setTglLahirPpDtTrv(value);
	}
	if(status == 'pasangan'){
		this.props.setTglLahirPsDtTrv(value);
	}
	if(status == 'anak_pertama'){
		this.props.setTglLahirApDtTrv(value);
	}
	if(status == 'anak_kedua'){
		this.props.setTglLahirAdDtTrv(value);
	}
	if(status == 'anak_ketiga'){
		this.props.setTglLahirAtDtTrv(value);
	}	  
  }
  
  settempatlahir(value, status){	  
	if(status == 'pemegang_polis'){
		this.props.setTempatLahirPpDtTrv(value);
	}
	if(status == 'pasangan'){
		this.props.setTempatLahirPsDtTrv(value);
	}
	if(status == 'anak_pertama'){
		this.props.setTempatLahirApDtTrv(value);
	}
	if(status == 'anak_kedua'){
		this.props.setTempatLahirAdDtTrv(value);
	}
	if(status == 'anak_ketiga'){
		this.props.setTempatLahirAtDtTrv(value);
	}	  
  }
  
  setidno(value, status){	  
	if(status == 'pemegang_polis'){
		this.props.setIdNoPpDtTrv(value);
	}
	if(status == 'pasangan'){
		this.props.setIdNoPsDtTrv(value);
	}
	if(status == 'anak_pertama'){
		this.props.setIdNoApDtTrv(value);
	}
	if(status == 'anak_kedua'){
		this.props.setIdNoAdDtTrv(value);
	}
	if(status == 'anak_ketiga'){
		this.props.setIdNoAtDtTrv(value);
	}	  
  }
  
  setkitas(value, status){	  
	if(status == 'pemegang_polis'){
		this.props.setKitasPpDtTrv(value);
	}
	if(status == 'pasangan'){
		this.props.setKitasPsDtTrv(value);
	}
	if(status == 'anak_pertama'){
		this.props.setKitasApDtTrv(value);
	}
	if(status == 'anak_kedua'){
		this.props.setKitasAdDtTrv(value);
	}
	if(status == 'anak_ketiga'){
		this.props.setKitasAtDtTrv(value);
	}	  
  }
  
  selectwarganegara(value, status){ 
	if(value == 'wna')
		this.setkitas('', status);
	
  
	if(status == 'pemegang_polis'){
		this.props.setKewarganegaraanPpDtTrv(value);
	}
	if(status == 'pasangan'){
		this.props.setKewarganegaraanPsDtTrv(value);
	}
	if(status == 'anak_pertama'){
		this.props.setKewarganegaraanApDtTrv(value);
	}
	if(status == 'anak_kedua'){
		this.props.setKewarganegaraanAdDtTrv(value);
	}
	if(status == 'anak_ketiga'){
		this.props.setKewarganegaraanAtDtTrv(value);
	}	   
  }
  
  namadepantext(listDataTravel, status){
	var first_name = '';
	
	if(status == 'pemegang_polis'){
		first_name = listDataTravel.dataPemegangPolisTravel.member_firstname;
	}
	if(status == 'pasangan'){
		first_name = listDataTravel.dataPasanganTravel.member_firstname;
	}
	if(status == 'anak_pertama'){
		first_name = listDataTravel.dataAnakPertamaTravel.member_firstname;
	}
	if(status == 'anak_kedua'){
		first_name = listDataTravel.dataAnakKeduaTravel.member_firstname;
	}
	if(status == 'anak_ketiga'){
		first_name = listDataTravel.dataAnakKetigaTravel.member_firstname;
	}
		
	 return(
	  <JagainTextInput
		  title = 'Nama Depan'
		  autoCapitalize = 'words'
		  value={first_name}
		  onChangeText={(value) => this.setnamadepan(appFunction.alphabetSpaceOnly(value), status)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	 ); 
	  
  }
  
  namabelakangtext(listDataTravel, status){
	var last_name = '';
	
	if(status == 'pemegang_polis'){
		last_name = listDataTravel.dataPemegangPolisTravel.member_lastname;
	}
	if(status == 'pasangan'){
		last_name = listDataTravel.dataPasanganTravel.member_lastname;
	}
	if(status == 'anak_pertama'){
		last_name = listDataTravel.dataAnakPertamaTravel.member_lastname;
	}
	if(status == 'anak_kedua'){
		last_name = listDataTravel.dataAnakKeduaTravel.member_lastname;
	}
	if(status == 'anak_ketiga'){
		last_name = listDataTravel.dataAnakKetigaTravel.member_lastname;
	}
	  
	  
	  return(
	   <JagainTextInput
		  title = 'Nama Belakang'
		  autoCapitalize = 'words'
		  value={last_name}
		  onChangeText={(value) => this.setnamabelakang(appFunction.alphabetSpaceOnly(value), status)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
  }
  
  
  genderradio(listDataTravel, status){
	var gender = '';
	
	if(status == 'pemegang_polis'){
		gender = listDataTravel.dataPemegangPolisTravel.member_gender;
	}
	if(status == 'pasangan'){
		gender = listDataTravel.dataPasanganTravel.member_gender;
	}
	if(status == 'anak_pertama'){
		gender = listDataTravel.dataAnakPertamaTravel.member_gender;
	}
	if(status == 'anak_kedua'){
		gender = listDataTravel.dataAnakKeduaTravel.member_gender;
	}
	if(status == 'anak_ketiga'){
		gender = listDataTravel.dataAnakKetigaTravel.member_gender;
	}  
	  
    
    var init1 = true;
	var init2 = false;
	var value = 'L'; 
    if(gender == 'L'){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
    return(
	<View style={[styles.width80p]}>
		<View style={[styles.directionRow]}>
			<Text>Jenis Kelamin</Text>
		</View>
		 <View style={[styles.directionRow]}>
		  <View style={[styles.directionRow, styles.right10]}>
			  <Radio selected={init1} onPress={() => {this.selectgender('L', status)}} />
		   <Text style={[styles.left5]}>Laki-Laki</Text>
		   </View>
		   <View style={[styles.directionRow]}>
			 <Radio selected={init2} onPress={() => {this.selectgender('P', status)}} />
			 <Text style={[styles.left5]}>Perempuan</Text>
			</View>
		</View>
	  </View>
    );
  }
  
  
  birthdatepicker(listDataTravel, status){

	  var birth_date = '';
	
	if(status == 'pemegang_polis'){
		birth_date = listDataTravel.dataPemegangPolisTravel.member_birthdate;
	}
	if(status == 'pasangan'){
		birth_date = listDataTravel.dataPasanganTravel.member_birthdate;
	}
	if(status == 'anak_pertama'){
		birth_date = listDataTravel.dataAnakPertamaTravel.member_birthdate;
	}
	if(status == 'anak_kedua'){
		birth_date = listDataTravel.dataAnakKeduaTravel.member_birthdate;
	}
	if(status == 'anak_ketiga'){
		birth_date = listDataTravel.dataAnakKetigaTravel.member_birthdate;
	}  
	  
    return(
		<JagainDatePicker
		 title='Tanggal Lahir'
         date={birth_date}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
         onDateChange={(value) => this.setbirthdate(value, status)}
        />
    );
  }
  
  tempatlahirtext(listDataTravel, status){
	  var tempatlahir = '';
	
	if(status == 'pemegang_polis'){
		tempatlahir = listDataTravel.dataPemegangPolisTravel.tempat_lahir;
	}
	if(status == 'pasangan'){
		tempatlahir = listDataTravel.dataPasanganTravel.tempat_lahir;
	}
	if(status == 'anak_pertama'){
		tempatlahir = listDataTravel.dataAnakPertamaTravel.tempat_lahir;
	}
	if(status == 'anak_kedua'){
		tempatlahir = listDataTravel.dataAnakKeduaTravel.tempat_lahir;
	}
	if(status == 'anak_ketiga'){
		tempatlahir = listDataTravel.dataAnakKetigaTravel.tempat_lahir;
	}  
	  
	  return(
	  <JagainTextInput
		  title = 'Tempat Lahir'
		  autoCapitalize = 'words'
		  value={tempatlahir}
		  onChangeText={(value) => this.settempatlahir(appFunction.alphabetSpaceOnly(value), status)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
  }
  
  idnotext(listDataTravel, status){
	  
	  var idno = '';
	
	if(status == 'pemegang_polis'){
		idno = listDataTravel.dataPemegangPolisTravel.member_idno;
	}
	if(status == 'pasangan'){
		idno = listDataTravel.dataPasanganTravel.member_idno;
	}
	if(status == 'anak_pertama'){
		idno = listDataTravel.dataAnakPertamaTravel.member_idno;
	}
	if(status == 'anak_kedua'){
		idno = listDataTravel.dataAnakKeduaTravel.member_idno;
	}
	if(status == 'anak_ketiga'){
		idno = listDataTravel.dataAnakKetigaTravel.member_idno;
	}  
	  
	  return(
	  <JagainTextInput
		  title = 'No KTP / Passport'
		  autoCapitalize = 'words'
		  value={idno}
		  onChangeText={(value) => this.setidno(appFunction.alphaNumericDashOnly(value), status)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  maxLength={16}
	  />
	  );
	  
  }
  
    nokitastext(listDataTravel, status){
		 var kitas = '';
	
	if(status == 'pemegang_polis'){
		kitas = listDataTravel.dataPemegangPolisTravel.member_kitas;
	}
	if(status == 'pasangan'){
		kitas = listDataTravel.dataPasanganTravel.member_kitas;
	}
	if(status == 'anak_pertama'){
		kitas = listDataTravel.dataAnakPertamaTravel.member_kitas;
	}
	if(status == 'anak_kedua'){
		kitas = listDataTravel.dataAnakKeduaTravel.member_kitas;
	}
	if(status == 'anak_ketiga'){
		kitas = listDataTravel.dataAnakKetigaTravel.member_kitas;
	}  
		
		
	  return(
	  <JagainTextInput
		  title = 'No KITAS'
		  keyboardType='numeric'
		  value={kitas}
	      onChangeText={(value) => this.setkitas(appFunction.alphaNumericDashOnly(value), status)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  maxLength={16}
	  />
	  );
	  
  }
  
  

  kewarganegaraanradio(listDataTravel, status){
	var kewarganegaraan = '';
	
	if(status == 'pemegang_polis'){
		kewarganegaraan = listDataTravel.otherDataTravel.kewarganegaraan_pp;
	}
	if(status == 'pasangan'){
		kewarganegaraan = listDataTravel.otherDataTravel.kewarganegaraan_ps;
	}
	if(status == 'anak_pertama'){
		kewarganegaraan = listDataTravel.otherDataTravel.kewarganegaraan_ap;
	}
	if(status == 'anak_kedua'){
		kewarganegaraan = listDataTravel.otherDataTravel.kewarganegaraan_ad;
	}
	if(status == 'anak_ketiga'){
		kewarganegaraan = listDataTravel.otherDataTravel.kewarganegaraan_at;
	}   
	  
    
    var init1 = true;
	var init2 = false;
	var value = 'wni'; 
    if(kewarganegaraan == 'wni'){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
    return(
	<View>
		<View style={[styles.directionRow]}>
			<Text>Kewarganegaraan</Text>
		</View>
		 <View style={[styles.directionRow]}>
		  <View style={[styles.directionRow, styles.right10]}>
			  <Radio selected={init1} onPress={() => {this.selectwarganegara('wni', status)}} />
		   <Text style={[styles.left5]}>WNI</Text>
		   </View>
		   <View style={[styles.directionRow]}>
			 <Radio selected={init2} onPress={() => {this.selectwarganegara('wna', status)}} />
			 <Text style={[styles.left5]}>WNA</Text>
			</View>
		</View>
	  </View>
    );
  }
  
  pemegangPolisInput(listDataTravel, status){
	  const title = listDataTravel.dataPemegangPolisTravel.tittle;
	  const kewarganegaraanpp = listDataTravel.otherDataTravel.kewarganegaraan_pp;
	  
	  return(
			<View style={[styles.bottom5]}>
				<WheelPicker/>
		<View style={[styles.top5]} />
		{this.titlelist(listDataTravel)}
		<View style={[styles.top5]} />
		{this.namadepantext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.namabelakangtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.genderradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.birthdatepicker(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.tempatlahirtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.idnotext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.kewarganegaraanradio(listDataTravel, status)}
		{ kewarganegaraanpp== 'wna' ? this.nokitastext(listDataTravel, status): null }
	  </View>
	  );
  }
  
  pasanganInput(listDataTravel, status){
	  const title = listDataTravel.dataPemegangPolisTravel.tittle;
	  const kewarganegaraanps = listDataTravel.otherDataTravel.kewarganegaraan_ps;
	  
	  return(
	  <View style={[styles.bottom5]}>
		<View style={[styles.top5]} />
		{this.namadepantext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.namabelakangtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.genderradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.birthdatepicker(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.tempatlahirtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.idnotext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.kewarganegaraanradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{ kewarganegaraanps== 'wna' ? this.nokitastext(listDataTravel, status): null }
	  </View>
	  );
  }
  
  anakPertamaInput(listDataTravel, status){
	  const title = listDataTravel.dataPemegangPolisTravel.tittle;
	  const kewarganegaraanap = listDataTravel.otherDataTravel.kewarganegaraan_ap;
	  
	  return(
	  <View style={[styles.bottom5]}>
		<View style={[styles.top5]} />
		{this.namadepantext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.namabelakangtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.genderradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.birthdatepicker(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.tempatlahirtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.idnotext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.kewarganegaraanradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{ kewarganegaraanap== 'wna' ? this.nokitastext(listDataTravel, status): null }
	  </View>
	  );
  }
  
  anakKeduaInput(listDataTravel, status){
	  const title = listDataTravel.dataPemegangPolisTravel.tittle;
	  const kewarganegaraanad = listDataTravel.otherDataTravel.kewarganegaraan_ad;
	  
	  return(
	  <View style={[styles.bottom5]}>
		<View style={[styles.top5]} />
		{this.namadepantext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.namabelakangtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.genderradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.birthdatepicker(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.tempatlahirtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.idnotext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.kewarganegaraanradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{ kewarganegaraanad== 'wna' ? this.nokitastext(listDataTravel, status): null }
	  </View>
	  );
  }
  
  anakKetigaInput(listDataTravel, status){
	  const title = listDataTravel.dataPemegangPolisTravel.tittle;
	  const kewarganegaraanat = listDataTravel.otherDataTravel.kewarganegaraan_at;
	  
	  return(
	  <View style={[styles.bottom5]}>
		<View style={[styles.top5]} />
		{this.namadepantext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.namabelakangtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.genderradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.birthdatepicker(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.tempatlahirtext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.idnotext(listDataTravel, status)}
		<View style={[styles.top5]} />
		{this.kewarganegaraanradio(listDataTravel, status)}
		<View style={[styles.top5]} />
		{ kewarganegaraanat== 'wna' ? this.nokitastext(listDataTravel, status): null }
	  </View>
	  );
  }
  
  
	dataPemegangPolis(listDataTravel){
		const status = 'pemegang_polis';
		
		return(
		<View>
			{this.statustitletext('Data Pemegang Polis', status)}
			{this.state.ppvisible ? this.pemegangPolisInput(listDataTravel, status) : null}
		</View> 
		);
	}
  
  dataPasangan(listDataTravel){
		const status = 'pasangan';
		
		return(
		<View>
			{this.statustitletext('Data Pasangan', status)}
			{this.state.psvisible ? this.pasanganInput(listDataTravel, status) : null}
		</View> 
		);
  }
  
  dataAnakPertama(listDataTravel){
		const status = 'anak_pertama';
		
		return(
		<View>
			{this.statustitletext('Data Anak Pertama', status)}
			{this.state.apvisible ? this.anakPertamaInput(listDataTravel, status) : null}
		</View> 
		);
  }
  
  dataAnakKedua(listDataTravel){
		const status = 'anak_kedua';
		
		return(
		<View>
			{this.statustitletext('Data Anak Kedua', status)}
			{this.state.advisible ? this.anakKeduaInput(listDataTravel, status) : null}
		</View> 
		);
  }
  
  dataAnakKetiga(listDataTravel){
		const status = 'anak_ketiga';
		
		return(
		<View>
			{this.statustitletext('Data Anak Ketiga', status)}
			{this.state.atvisible ? this.anakKetigaInput(listDataTravel, status) : null}
		</View> 
		);
  }
  
  
  dataPendukung(listDataTravel){
		const status = 'data_pendukung';
		
		return(
		<View>
			{this.statustitletext('Data Pendukung', status)}
			{this.state.dpvisible ? this.dataPendukungInput(listDataTravel) : null}
		</View> 
		);
	}
  
  
  
 phonetext(listDataTravel){
	 const member_phoneno = listDataTravel.dataPemegangPolisTravel.member_phoneno;
	 
	 return(
	  <JagainTextInput
		  title = 'Telp Rumah'
		  keyboardType='numeric'
		  value={member_phoneno}
			onChangeText={(value) => this.props.setPhoneDtTrv(appFunction.numericPlusOnly(value))}		
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  maxLength={16}
	  />
	 
	  
		);
 }
  
 handphonetext(listDataTravel){
	 const member_hp = listDataTravel.dataPemegangPolisTravel.member_hp;
	 
	 return(
	   <JagainTextInput
		  title = 'Telp Seluler'
		  keyboardType='numeric'
			value={member_hp}
			onChangeText={(value) => this.props.setHandphoneDtTrv(appFunction.numericPlusOnly(value))}		
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  maxLength={16}
	  />
		);
 }

 emailtext(listDataTravel){
	 const member_email = listDataTravel.dataPemegangPolisTravel.member_email;
	 
	return(
	 <JagainTextInput
		  title = 'E-Mail'
		  keyboardType = 'email-address'
		 autoCapitalize='none'
			value={member_email}			
				onChangeText={(member_email) => this.props.setEmailDtTrv(appFunction.emailCharacterOnly(member_email))}	
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  maxLength={50}
	  />
	);
 }
 
  alamattext(listDataTravel){
	const member_alamat = listDataTravel.dataPemegangPolisTravel.member_alamat;
	 
	return(
	<JagainTextInput
		  title = 'Alamat Rumah'
	      value={member_alamat}			
		  onChangeText={(member_alamat) => this.props.setAlamatDtTrv(appFunction.dotalphaNumericOnly(member_alamat))}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  multiline = {true}
	      numberOfLines = {8}
	  />
	);
  }
  
  ahliwaristext(listDataTravel){
	  const member_ahliwaris = listDataTravel.dataPemegangPolisTravel.member_ahliwaris;
	  
	  return(
	  <JagainTextInput
		  title = 'Nama Ahli Waris'
		  value={member_ahliwaris}	
		  onChangeText={(member_ahliwaris) => this.props.setNamaAhliWarisDtTrv(member_ahliwaris)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
  }


  ahliwarislist(listDataTravel){
	const value = listDataTravel.dataPemegangPolisTravel.member_hub_ahliwaris;
	 
    const lob = 'Travel';
    const title = 'Hubungan Ahli Waris';
    const options = this.props.listAhliWaris;
    const type = Object.assign({},{state: 'member_hub_ahliwaris', lob});


    return(
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
        
      />
    );
  }

  provinsilist(listDataTravel){
	const value = listDataTravel.travelLabelState.label_provinsi;
	 
    const lob = 'Travel';
    const title = 'Provinsi';
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
        
      />
    );
  }


  districtlist(listDataTravel){
	const value = listDataTravel.travelLabelState.label_district;
	 
    const lob = 'Travel';
    const title = 'Kabupaten / Kota';
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
        
      />
    );
  }  

   fileIdentitas(listDataTravel){
	  const file_identitas = listDataTravel.dataPemegangPolisTravel.file_identitas;
	  
	  //let imageUriKtp = file_identitas ? `data:image/jpg;base64,${file_identitas}` : null;
    //imageUri && console.log({uri: imageUri.slice(0, 100)});
	  return(
	  
		<UploadImage 
		filetitle='KTP' 
		filename={file_identitas} 
		onUploadFile ={(value)=>
          this.props.setFileIdentitasDtTrv(value)}  />
			
    
	  );
  }
  
  dataPendukungInput(listDataTravel){		
	  return(
		<View>
			<View style={[styles.top5]} />
			{this.fileIdentitas(listDataTravel)}
			<View style={[styles.top5]} />
			{this.phonetext(listDataTravel)}
			<View style={[styles.top5]} />
			{this.handphonetext(listDataTravel)}
			<View style={[styles.top5]} />
			{this.emailtext(listDataTravel)}
			<View style={[styles.top5]} />
			{this.alamattext(listDataTravel)}
			<View style={[styles.top5]} />
			{this.provinsilist(listDataTravel)}
			<View style={[styles.top5]} />
			{this.districtlist(listDataTravel)}
			<View style={[styles.top5]} />
			{this.ahliwaristext(listDataTravel)}
			<View style={[styles.top5]} />
			{this.ahliwarislist(listDataTravel)}
		</View>
		);
	  
  }

  doPostData(daftar_keluarga, listDataTravel, postDataTravel){
    var errorMessage =  '';
	const anakPertama = [1, 2, 3, 5, 6, 7];
	const anakKedua = [2, 3, 6, 7];
	const anakKetiga = [3, 7];
	const pasangan = [4, 5, 6, 7]
	
	if(listDataTravel.dataPemegangPolisTravel.tittle == 'Pilih'){
      errorMessage = 'Title Pemegang Polis Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_firstname){
      errorMessage = 'Nama Depan Pemegang Polis Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_gender){
      errorMessage = 'Jenis Kelamin Pemegang Polis Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_birthdate){
      errorMessage = 'Tanggal Lahir Pemegang Polis Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.tempat_lahir){
      errorMessage = 'Tempat Lahir Pemegang Polis Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_idno){
      errorMessage = 'Nomor KTP / Passport Pemegang Polis Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(listDataTravel.otherDataTravel.kewarganegaraan_pp == 'wna' && !listDataTravel.dataPemegangPolisTravel.member_kitas){
      errorMessage = 'Nomor KITAS Pemegang Polis Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPasanganTravel.member_firstname && pasangan.includes(daftar_keluarga)){
	  errorMessage = 'Nama Depan Pasangan Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataPasanganTravel.member_gender && pasangan.includes(daftar_keluarga)){
	  errorMessage = 'Jenis Kelamin Pasangan Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataPasanganTravel.member_birthdate && pasangan.includes(daftar_keluarga)){
	  errorMessage = 'Tanggal Lahir Pasangan Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataPasanganTravel.tempat_lahir && pasangan.includes(daftar_keluarga)){
	  errorMessage = 'Tempat Lahir Pasangan Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataPasanganTravel.member_idno && pasangan.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KTP / Passport Pasangan Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(listDataTravel.otherDataTravel.kewarganegaraan_ps == 'wna' && !listDataTravel.dataPasanganTravel.member_kitas && pasangan.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KITAS Pasangan Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakPertamaTravel.member_firstname && anakPertama.includes(daftar_keluarga)){
	  errorMessage = 'Nama Depan Anak Pertama Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakPertamaTravel.member_gender && anakPertama.includes(daftar_keluarga)){
	  errorMessage = 'Jenis Kelamin Anak Pertama Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakPertamaTravel.member_birthdate && anakPertama.includes(daftar_keluarga)){
	  errorMessage = 'Tanggal Lahir Anak Pertama Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakPertamaTravel.tempat_lahir && anakPertama.includes(daftar_keluarga)){
	  errorMessage = 'Tempat Lahir Anak Pertama Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakPertamaTravel.member_idno && anakPertama.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KTP / Passport Anak Pertama Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(listDataTravel.otherDataTravel.kewarganegaraan_ap == 'wna' && !listDataTravel.dataAnakPertamaTravel.member_kitas && anakPertama.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KITAS  Anak Pertama Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKeduaTravel.member_firstname && anakKedua.includes(daftar_keluarga)){
	  errorMessage = 'Nama Depan Anak Kedua Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKeduaTravel.member_gender && anakKedua.includes(daftar_keluarga)){
	  errorMessage = 'Jenis Kelamin Anak Kedua Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKeduaTravel.member_birthdate && anakKedua.includes(daftar_keluarga)){
	  errorMessage = 'Tanggal Lahir Anak Kedua Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKeduaTravel.tempat_lahir && anakKedua.includes(daftar_keluarga)){
	  errorMessage = 'Tempat Lahir Anak Kedua Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKeduaTravel.member_idno && anakKedua.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KTP / Passport Anak Kedua Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(listDataTravel.otherDataTravel.kewarganegaraan_ad == 'wna' && !listDataTravel.dataAnakKeduaTravel.member_kitas && anakKedua.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KITAS Anak Kedua Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKetigaTravel.member_firstname && anakKetiga.includes(daftar_keluarga)){
	  errorMessage = 'Nama Depan Anak Ketiga Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKetigaTravel.member_gender && anakKetiga.includes(daftar_keluarga)){
	  errorMessage = 'Jenis Kelamin Anak Ketiga Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKetigaTravel.member_birthdate && anakKetiga.includes(daftar_keluarga)){
	  errorMessage = 'Tanggal Lahir Anak Ketiga Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKetigaTravel.tempat_lahir && anakKetiga.includes(daftar_keluarga)){
	  errorMessage = 'Tempat Lahir Anak Ketiga Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(!listDataTravel.dataAnakKetigaTravel.member_idno && anakKetiga.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KTP / Passport Anak Ketiga Belum Diisi';
	  appFunction.toastError(errorMessage);
	}else if(listDataTravel.otherDataTravel.kewarganegaraan_at == 'wna' && !listDataTravel.dataAnakKetigaTravel.member_kitas && anakKetiga.includes(daftar_keluarga)){
	  errorMessage = 'Nomor KITAS Anak Ketiga Belum Diisi';
	  appFunction.toastError(errorMessage);
	}
	else if(!listDataTravel.dataPemegangPolisTravel.file_identitas){
      errorMessage = 'Photo Identitas Belum Diupload';
      appFunction.toastError(errorMessage);
    }
	else if(appFunction.validateTelpon(listDataTravel.dataPemegangPolisTravel.member_phoneno)){
      errorMessage = 'Telp Rumah Harus diawali dengan 6 atau 0 dan Harus diantara 9-15 Karakter';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_hp){
      errorMessage = 'Telp Seluler Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(appFunction.validateHandphone(listDataTravel.dataPemegangPolisTravel.member_hp)){
      errorMessage = 'Telp Seluler Harus diawali dengan 6 atau 0 dan Harus diantara 9-16 Karakter';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_email){
      errorMessage = 'E-Mail Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(appFunction.validateEmail(listDataTravel.dataPemegangPolisTravel.member_email)){
      errorMessage = 'E-Mail Tidak Valid';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_alamat){
      errorMessage = 'Alamat Rumah Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(listDataTravel.dataPemegangPolisTravel.province_id == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(listDataTravel.dataPemegangPolisTravel.district_id == 'Pilih'){
      errorMessage = 'Kabupaten/Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!listDataTravel.dataPemegangPolisTravel.member_ahliwaris){
      errorMessage = 'Nama Ahli Waris Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(listDataTravel.dataPemegangPolisTravel.member_hub_ahliwaris == 'Pilih'){
      errorMessage = 'Hubungan Ahli Waris Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else{
      this.props.setPostData(postDataTravel);
		//console.log(body);
		//this.props.doPostDataTravel(postDataTravel);
		this.props.navigate('Pembayaran');
	  //appFunction.toastError('sukses');
	  //console.log('sukses', postDataTravel);
	  //errorMessage = 'sukses bro';
      //appFunction.toastError(errorMessage);
    }	
  }
  
  componentWillReceiveProps(NextProps){
    if(NextProps.dataPemegangPolisTravel.province_id != this.props.dataPemegangPolisTravel.province_id){
			this.props.resetDistrictDtTrv();
					this.props.resetListDistrict();
			if(NextProps.dataPemegangPolisTravel.province_id != 'Pilih')
				this.props.getDistrict(NextProps.dataPemegangPolisTravel.province_id);
		}
		if(this.props.pesertaAsuransi != NextProps.pesertaAsuransi){
			if(NextProps.pesertaAsuransi.id){
				this.setnamadepan(NextProps.pesertaAsuransi.name, 'pemegang_polis');
				this.selectgender(NextProps.pesertaAsuransi.jenisKelamin, 'pemegang_polis');
				this.setbirthdate(NextProps.pesertaAsuransi.tanggalLahir, 'pemegang_polis');
				this.settempatlahir(NextProps.pesertaAsuransi.tempatLahir, 'pemegang_polis');
				this.setidno(NextProps.pesertaAsuransi.noIdentitas, 'pemegang_polis');
				this.props.setFileIdentitasDtTrv(NextProps.pesertaAsuransi.fileIdentitas);
				this.props.setHandphoneDtTrv(NextProps.pesertaAsuransi.phone);
				this.props.setEmailDtTrv(NextProps.pesertaAsuransi.email);
				this.props.setAlamatDtTrv(NextProps.pesertaAsuransi.address);
			}else{
				this.setnamadepan('', 'pemegang_polis');
				this.selectgender('L', 'pemegang_polis');
				this.setbirthdate(null, 'pemegang_polis');
				this.settempatlahir('', 'pemegang_polis');
				this.setidno('', 'pemegang_polis');
				this.props.setFileIdentitasDtTrv(null);
				this.props.setHandphoneDtTrv('');
				this.props.setEmailDtTrv('');
				this.props.setAlamatDtTrv('');
			}
		}
  }

  componentDidMount(){
    this.props.getAhliWaris();
    this.props.getProvinsi();
		this.props.resetPostData();
  }


	render() {
    var {height, width} = Dimensions.get('window');
    const {dataTravel, session, travelState, dataVoucher, asuransiTravel, travelLabelState} = this.props;
	const { otherDataTravel, dataPemegangPolisTravel, dataPasanganTravel, dataAnakPertamaTravel, dataAnakKeduaTravel, dataAnakKetigaTravel } = this.props;
	const listDataTravel = Object.assign({},
		{otherDataTravel}, {dataPemegangPolisTravel}, {dataPasanganTravel}, {dataAnakPertamaTravel}, {dataAnakKeduaTravel}, {dataAnakKetigaTravel}, {travelLabelState}
	);
    const body = Object.assign({}, travelState);
    const dataPostTravel = Object.assign({},dataTravel,
      {visitor_id:session.visitor_id},
      {kode_voucher:dataVoucher.kode_voucher},
    );	
    const postData =  Object.assign({},{data_travel:dataPostTravel}, body, {package_id:asuransiTravel.package_id });
	const {daftar_keluarga} = this.props.travelState;
	const pasangan = [4, 5, 6, 7]
	const anakPertama = [1, 2, 3, 5, 6, 7];
	const anakKedua = [2, 3, 6, 7];
	const anakKetiga = [3, 7];
	
	var arrayDataMember = new Array();
	arrayDataMember.push(dataPemegangPolisTravel);
	
	
	if(daftar_keluarga == 1){	
		arrayDataMember.push(dataAnakPertamaTravel);
	}else if(daftar_keluarga == 2){	
		arrayDataMember.push(dataAnakPertamaTravel);
		arrayDataMember.push(dataAnakKeduaTravel);
	}else if(daftar_keluarga == 3){	
		arrayDataMember.push(dataAnakPertamaTravel);
		arrayDataMember.push(dataAnakKeduaTravel);
		arrayDataMember.push(dataAnakKetigaTravel);
	}else if(daftar_keluarga == 4){	
		arrayDataMember.push(dataPasanganTravel);
	}else if(daftar_keluarga == 5){	
		arrayDataMember.push(dataPasanganTravel);
		arrayDataMember.push(dataAnakPertamaTravel);
	}else if(daftar_keluarga == 6){	
		arrayDataMember.push(dataPasanganTravel);
		arrayDataMember.push(dataAnakPertamaTravel);
		arrayDataMember.push(dataAnakKeduaTravel);
	}else if(daftar_keluarga == 7){	
		arrayDataMember.push(dataPasanganTravel);
		arrayDataMember.push(dataAnakPertamaTravel);
		arrayDataMember.push(dataAnakKeduaTravel);
		arrayDataMember.push(dataAnakKetigaTravel);
	}
	
	const postDataTravel = Object.assign({}, postData, {data_member: arrayDataMember});
	
	//console.log(postDataTravel);
	
	  return (
		<View style = {[styles.centerItemContent]}>
            <View style={[styles.detailPribadiMainContainer]}>
				{this.dataPemegangPolis(listDataTravel)}
				{pasangan.includes(daftar_keluarga) ?  this.dataPasangan(listDataTravel) : null}
				{anakPertama.includes(daftar_keluarga) ?  this.dataAnakPertama(listDataTravel) : null}
				{anakKedua.includes(daftar_keluarga) ?  this.dataAnakKedua(listDataTravel) : null}
				{anakKetiga.includes(daftar_keluarga) ?  this.dataAnakKetiga(listDataTravel) : null}	
				{this.dataPendukung(listDataTravel)}
				<View style={{height: height*.05}}></View>
				<View style={[styles.centerItemContent]}>
				<SubmitButton title='Submit' onPress={() => this.doPostData(daftar_keluarga, listDataTravel, postDataTravel)} />
				</View>
			</View>
		</View>
	  );
  }
}

function mapStateToProps(state) {
  return {
    dataTravel: state.dataTravel,
    otherDataTravel: state.otherDataTravel,
	dataPemegangPolisTravel: state.dataPemegangPolisTravel,
	dataPasanganTravel: state.dataPasanganTravel,
	dataAnakPertamaTravel:state.dataAnakPertamaTravel,
	dataAnakKeduaTravel:state.dataAnakKeduaTravel,
	dataAnakKetigaTravel:state.dataAnakKetigaTravel,
    travelState: state.travelState,
    travelLabelState: state.travelLabelState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    asuransiTravel: state.asuransiTravel,
    listAhliWaris: state.listAhliWaris,
    listProvinsi: state.listProvinsi,
		listDistrict:state.listDistrict,
		pesertaAsuransi:state.pesertaAsuransi,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiTravel);
