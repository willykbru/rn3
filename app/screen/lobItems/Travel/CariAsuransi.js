import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import CheckBox from 'react-native-check-box';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import RadioButton from '../../../components/radio/RadioButton';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import DatePicker from 'react-native-datepicker';
//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiTravel extends React.Component {
  constructor(props) {
    super(props);
  }

  toListAsuransi(travelState, lob) {

	  if(travelState.trip_type == 'Pilih'){
		errorMessage = 'Jenis Liburan Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(travelState.travel_type == 'Pilih'){
		errorMessage = 'Jenis Perjalanan Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(travelState.travel_type == 'domestik' && travelState.city_id == 'Pilih' && travelState.trip_type == 'short'){
		errorMessage = 'Kota Tujuan Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(travelState.travel_type == 'international' && travelState.country_id == 'Pilih' && travelState.trip_type == 'short'){
		errorMessage = 'Negara Tujuan Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(!travelState.start_date){
		errorMessage = 'Tanggal Berangkat Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(!travelState.end_date){
		errorMessage = 'Tanggal Kembali Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(appFunction.yesterdayDate() > travelState.start_date){
		errorMessage = 'Tanggal Berangkat Tidak Boleh Kurang Dari Tanggal Hari Ini';
		appFunction.toastError(errorMessage);
	  }else if(travelState.end_date < travelState.start_date){
		errorMessage = 'Tanggal Kembali Tidak Boleh Kurang Dari Tanggal Berangkat';
		appFunction.toastError(errorMessage);
	  }else if(travelState.package_type == 'Pilih'){
		errorMessage = 'Jenis Paket Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else if(travelState.package_type == 'family' && travelState.daftar_keluarga == 'Pilih'){
		errorMessage = 'Daftar Keluarga Belum Dipilih';
		appFunction.toastError(errorMessage);
	  }else{
		if(this.props.travelState.city_id == 'Pilih'){
			this.props.resetCityTravel('');
		}

		if(this.props.travelState.country_id == 'Pilih'){
			this.props.resetCountryTravel('');
		}

		if(this.props.travelState.daftar_keluarga == 'Pilih'){
			this.props.resetDaftarKeluargaTravel(0);
		}


      this.props.navigate('ListAsuransi');
    }
  }

  triplist(travelLabelState, travelState, lob){
    const title = 'Jenis Liburan';
	const value = travelLabelState.label_trip_type;

    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'annual',
      label:'Annual Trip'
    },
    {
      key:'short',
      label:'Short Trip'
    }];

    const type = Object.assign({},{state: 'trip_type', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
		useFunction={true}
		onPick={(value, label)=>{if(value !== 'annual'){

			//console.log(label);
			this.props.setEndDateTravel(null);
			this.props.setLabelEndDateTravel(null);
			}else{
				this.props.resetCityTravel('Pilih');
				this.props.resetCountryTravel('Pilih');
				if(travelLabelState.label_start_date){
					this.props.setEndDateTravel(appFunction.add365Days(travelState.start_date));
					this.props.setLabelEndDateTravel(appFunction.formatFullDate(appFunction.add365Days(travelState.start_date)));
				}
			}
		}}
      />
    );
  }

  traveltypelist(travelLabelState, lob){
    const title = 'Jenis Perjalanan';
	const value = travelLabelState.label_travel_type;
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'international',
      label:'International'
    },
    {
      key:'domestik',
      label:'Domestik'
    }];

    const type = Object.assign({},{state: 'travel_type', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
		useFunction={true}
		onPick={(value)=>{

			if(value == 'domestik'){
				//this.props.resetCityTravel('Pilih');
				this.props.resetCountryTravel('Pilih');
			}else if(value == 'international'){
				this.props.resetCityTravel('Pilih');
			}else if(value == 'Pilih'){
				this.props.resetCityTravel('Pilih');
				this.props.resetCountryTravel('Pilih');
			}
		}}
      />
    );
  }

  packagetypelist(travelLabelState, lob){
    var array = [...this.props.listPackageTravel]; // make a separate copy of the array
	//array.splice(1,1);
	  //console.log(array);
	const title = 'Jenis Paket';
	const value = travelLabelState.label_package_type;
	const options = array;
	const type = Object.assign({},{state: 'package_type', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
		onPick={(value, label)=>{if(value !== 'family'){ this.props.resetDaftarKeluargaTravel('Pilih');}}}
		useFunction={true}
      />
    );
  }

  daftarkeluargalist(travelLabelState, lob){
    var array = [...this.props.listDaftarKeluargaTravel]; // make a separate copy of the array
	//array.splice(1,1);

	const title = 'Daftar Keluarga';
	const value = travelLabelState.label_daftar_keluarga;
	const options = array;
	const type = Object.assign({},{state: 'daftar_keluarga', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
        useFunction={true}
        onPick={()=>{}}
      />
    );
  }



  destinationlist(travelState, travelLabelState, lob){
	if(travelState.trip_type == 'Pilih'){
		const title = 'Tujuan';
		const value = travelLabelState.label_city;
		const options = [{
			key:'Pilih',
			label:'Pilih'
		}];
		const type = Object.assign({},{state: 'city_id', lob});

		return(
		  <FilterLabelPicker title={title}
			value={value}
			options={options}
			type={type}
      useFunction={true}
      onPick={()=>{}}
		  />
		);

	}else if(travelState.trip_type == 'short' && travelState.travel_type == 'domestik'){
		var array = [...this.props.listCity]; // make a separate copy of the array

		const title = 'Kota Tujuan';
		const value = travelLabelState.label_city;
		const options = array;
		const type = Object.assign({},{state: 'city_id', lob});




		return(
		  <FilterLabelPicker title={title}
			value={value}
			options={options}
			type={type}
      useFunction={true}
      onPick={()=>{}}
		  />
		);
	}else if(travelState.trip_type == 'short' && travelState.travel_type == 'international'){
		var array = [...this.props.listCountry]; // make a separate copy of the array

		const title = 'Negara Tujuan';
		const value = travelLabelState.label_country;
		const options = array;
		const type = Object.assign({},{state: 'country_id', lob});




		return(
		  <FilterLabelPicker title={title}
			value={value}
			options={options}
			type={type}
      useFunction={true}
      onPick={()=>{}}
		  />
		);
	}else if(travelState.trip_type == 'short' && travelState.travel_type == 'Pilih'){
		const title = 'Kota Tujuan';
		const value = travelLabelState.label_city;
		const options = [{
			key:'Pilih',
			label:'Pilih'
		}];
		const type = Object.assign({},{state: 'city_id', lob});

		return(
		  <FilterLabelPicker title={title}
			value={value}
			options={options}
			type={type}
      useFunction={true}
      onPick={()=>{}}
		  />
		);
	}else if(travelState.trip_type == 'annual'){
		return(<View style={[styles.width80p]}><Text style={[styles.bold]}>Anda tidak perlu mengisi tujuan, anda akan ter-cover selama 365 hari</Text></View>);
	}
  }

  tanggalberangkatpicker(travelLabelState, travelState){


    return(
	 <JagainDatePicker
		 title='Tanggal Berangkat'
         date={travelLabelState.label_start_date}
         minDate={appFunction.todayFullDate()}
         maxDate={appFunction.nextYear(10)}
		 formatDate={'DD MMMM YYYY'}
		 iconSource={require('../../../assets/icons/component/date-picker.png')}
		 placeholder='Tanggal Berangkat'
          onDateChange={(start_date) => {
				//console.log(appFunction.toIsoDate(start_date));
				this.props.setStartDateTravel(appFunction.toIsoDate(start_date));
				this.props.setLabelStartDateTravel(start_date);
				if(travelState.trip_type == 'annual'){
					//console.log(appFunction.add365Days(start_date));
					this.props.setEndDateTravel(appFunction.add365Days(start_date));
					this.props.setLabelEndDateTravel(appFunction.formatFullDate(appFunction.add365Days(start_date)));
				}else{
					this.props.setEndDateTravel(null);
					this.props.setLabelEndDateTravel(null);
				}
			}
		  }
        />
    );
  }


  tanggalkembalipicker(travelLabelState, travelState){
	  var disable = false;
	  if(travelState.trip_type == 'annual') {
		  disable = true;
	  }else{
		  disable = false;
	  }

    return(
	 <JagainDatePicker
		 title='Tanggal Kembali'
         date={travelLabelState.label_end_date}
         minDate={travelLabelState.label_start_date}
         maxDate={appFunction.nextYear(10)}
		 disabled={disable}
		 formatDate={'DD MMMM YYYY'}
		 iconSource={require('../../../assets/icons/component/date-picker.png')}
		 placeholder='Tanggal Kembali'
         onDateChange={(end_date) => {
			 this.props.setEndDateTravel(appFunction.toIsoDate(end_date));
				this.props.setLabelEndDateTravel(end_date);
		 }}
        />
    );
  }


  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };


  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	this.props.getCity();
	this.props.getCountry();
	this.props.getPackageTravel();
	this.props.getDaftarKeluargaTravel();

	if(this.props.travelState.city_id == ''){
		this.props.resetCityTravel('Pilih');
	}

	if(this.props.travelState.country_id == ''){
		this.props.resetCountryTravel('Pilih');
	}

	if(this.props.travelState.daftar_keluarga == 0){
		this.props.resetDaftarKeluargaTravel('Pilih');
	}

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {travelState, travelLabelState} = this.props;
    const lob = 'Travel';
    //const {listPlatKendaraan} = this.props;
    //console.log(travelState);
    //console.log('daftar',travelState);



    return(
      <View style={[styles.centerItemContent, styles.top20]} >
	  {this.triplist(travelLabelState, travelState, lob)}
	  {this.traveltypelist(travelLabelState, lob)}
	  {this.destinationlist(travelState, travelLabelState, lob)}
	  {this.tanggalberangkatpicker(travelLabelState, travelState)}
	  {this.tanggalkembalipicker(travelLabelState, travelState)}
	  {this.packagetypelist(travelLabelState, lob)}
	  {travelState.package_type == 'family' ? this.daftarkeluargalist(travelLabelState, lob) : null}
       <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(travelState, lob)} />
	  </View>
    );
  }
}

function mapStateToProps(state) {
  return {
	listDaftarKeluargaTravel:state.listDaftarKeluargaTravel,
	listPackageTravel:state.listPackageTravel,
    travelState: state.travelState,
    travelLabelState: state.travelLabelState,
	listCity: state.listCity,
	listCountry: state.listCountry,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiTravel);
