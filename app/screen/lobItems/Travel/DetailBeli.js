import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, Button, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';


class DetailBeliTravel extends React.Component {
  constructor(props){
     super(props);
   }


toDetailPribadi(){
  if(this.props.dataVoucher.description !== 'ok'){ 
	this.props.resetDataVoucher();
	this.props.setKodeVoucher('');
  }
  this.props.navigate('DetailPribadi');
}


detailPremi(dataBeliTravel){
  var perluasanpremi = [];
  var netpremi = [];
  var discvoucher=[];
  
  const premi = dataBeliTravel.premium_charged;
  const net_premi = dataBeliTravel.premium_charged - (dataBeliTravel.premium_charged * dataBeliTravel.discount/100);

  var disc_premi = premi*dataBeliTravel.discount/100;

  if(dataBeliTravel.discount !== 0){
    if(dataBeliTravel.description == 'ok'){
      if(dataBeliTravel.voucher_persen !== 0){
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
             </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.voucher_persen} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(premi*dataBeliTravel.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.discount} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-(net_premi*dataBeliTravel.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
          </View>
        );
      }else{
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
            <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliTravel.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>	
            </View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.discount} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
	          </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-dataBeliTravel.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
             </View>
			</View>
          </View>
        );
      }
    }else{
      netpremi.push(
        <View key='netpremi' >
          <View style={[styles.directionRow, styles.bottom]}>
          <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
          </View>
		  </View>
          <View style={styles.directionRow}>
            <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
              <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
              <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.discount} %</Text></View>
            </View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
          </View>
          <View style={styles.borderbottom1}></View>
          <View style={styles.directionRow}>
          <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.bold, styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		  </View>
          </View>
        </View>
      );
  }

  }else{
    if(dataBeliTravel.description == 'ok'){
      if(dataBeliTravel.voucher_persen !== 0){
          netpremi.push(
            <View key='netpremi' >
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
              <View style={styles.directionRow}>
               <View style={[styles.width50p, styles.directionRow]}>
                 <Text style={[styles.italic, styles.baseText, styles.spaceBetween, styles.flex2]}>Discount Voucher</Text>
                 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliTravel.voucher_persen} %</Text></View>
               </View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(net_premi*dataBeliTravel.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
               </View>
			  </View>
              <View style={styles.borderbottom1}></View>
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-(net_premi*dataBeliTravel.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
			  </View>
            </View>
        );
      }else{
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
            </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliTravel.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(net_premi-dataBeliTravel.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
          </View>
        );
      }
    }else{
          netpremi.push(
              <View key='totpremi' style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
          );
        }
  }

  return(
    <View style={styles.centerItemContent}>
  <View style={styles.borderDetailBeli}>
    <View style={styles.directionRow}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
	  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
		<Text style={[styles.baseText]}>:  Rp. </Text>
		<Text style={[styles.baseText]}>{premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
	  </View>
    </View>
    {perluasanpremi}
    <View style={styles.borderbottom1}></View>
    {netpremi}
  </View>
</View>
);
}


      componentDidMount() {
			const {package_id, package_name, insurance_name, net_premi} = this.props.asuransiTravel;
			var impressions = {package_id, package_name, lob: 'Asuransi Travel', insurance_name, price:net_premi};			
			appFunction.setImpressionGA(impressions); 
       }
	   
  render(){

    const {travelState, asuransiTravel, dataVoucher, travelLabelState, dataTravel} = this.props;
    const dataBeliTravel = Object.assign({}, travelLabelState, travelState, asuransiTravel, dataVoucher, dataTravel);
    //console.log('label',dataVoucher);
	const lob = Object.assign({}, {lob : 'TR'}); 
	const dataCheckVoucher = Object.assign({},dataVoucher, asuransiTravel, lob);
      var {height, width} = Dimensions.get('window');
	  //console.log(dataBeliTravel);
	  
	  var editableKodeVoucher = true;
	  
	  if(dataBeliTravel.description == 'ok'){
		  editableKodeVoucher = false;
	  }
    
    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira2.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }



    return(
      <View style={styles.centerItemContent}>
        <View style={styles.centerItem}>
        <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.bottom10, styles.left4, styles.radius3, styles.elev5, styles.pad5, {borderColor: '#ffe5e5', marginTop:height*.1, paddingTop:0}]}>
          <View style={styles.centerItemContent}>
           <View style={[styles.centerItemContent, styles.height70, styles.directionRow, styles.radius2, styles.bottom20]}>
             <Image source={asimgsrc[dataBeliTravel.insurance_name.toUpperCase()]} style={[appFunction.sizeLogoAsuransi(1.5)]}/>
           </View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Liburan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.label_trip_type}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Perjalanan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.travel_type}</Text></View>
          </View>
		  { dataBeliTravel.trip_type == 'short' ? dataBeliTravel.travel_type == 'domestik' ?

          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Kota Tujuan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.label_city}</Text></View>		  
		  </View>
		  :
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Negara Tujuan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.label_country}</Text></View>
          </View>
		  : null
		  }
          
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Tanggal Berangkat</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliTravel.start_date))}</Text></View>
          </View>
		  <View style={[styles.directionRow, styles.bottom]}>
			<View style={styles.width50p}><Text style={[styles.baseText]}>Tanggal Kembali</Text></View>
			<View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
			<View style={styles.width45p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliTravel.end_date))}</Text></View>
		  </View>
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Paket</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.label_package_type}</Text></View>
          </View> 
		  { dataBeliTravel.package_type == 'family' ?
		   <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Daftar Keluarga</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.label_daftar_keluarga}</Text></View>
           </View>
		   : null
		  }
           <View style={[styles.directionRow, styles.bottom]}>
             <View style={styles.width50p}><Text style={[styles.baseText]}>Provide By</Text></View>
             <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
             <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliTravel.insurance_name}</Text></View>
           </View>

           <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kode Voucher</Text></View>
               <Text style={[styles.baseText]}>:  </Text>
             <TextInput
               style={[styles.kodeVoucherTInput, styles.baseText]}
               value={dataVoucher.kode_voucher}
               onChangeText={(kode_voucher) => this.props.setKodeVoucher(appFunction.alphaNumericOnly(kode_voucher))}
			   onEndEditing={() => this.props.checkVoucher(dataCheckVoucher, 'checkVoucher')}
			   maxLength={20}
               underlineColorAndroid='transparent'
			   placeholder='Kode Voucher'
			   editable={editableKodeVoucher}
             />
           <TouchableHighlight underlayColor='white'>
              { dataVoucher.kode_voucher ? dataBeliTravel.description == 'ok'? <Icon name='checkmark-circle' style={{color:'green'}} /> : <Icon name='checkmark-circle' style={{color:'red'}} /> : <Icon name='checkmark-circle'/>}
            </TouchableHighlight>
           </View>
            <View style={[styles.directionRow, styles.bottom5]}>
              <View style={styles.width50p}>
			  {dataBeliTravel.description == 'ok'?
			  <TouchableHighlight onPress={()=>this.props.resetKodeVoucher()} style={[{backgroundColor: '#00FF00', width: 100, height: 25}, styles.radius5, styles.elev3, styles.centerItemContent]}>
				<Text style={[styles.baseText]}>
					Edit Voucher
				</Text>
			  </TouchableHighlight>
			  :
			  null
			  }
			  </View>
                {dataBeliTravel.description == 'ok'? <Text style={[{color:'green'}, styles.font12, styles.italic, styles.baseText]}>Voucher Valid</Text>: <Text style={[styles.colorRed, styles.font12, styles.italic, styles.baseText]}>{dataBeliTravel.description}</Text>}
            </View>
			<View style={{height: height*.03}}></View>
           {this.detailPremi(dataBeliTravel)}
           <View style={{height: height*.03}}></View>
         </View>
      </View>
        <SubmitButton title='Lanjut' onPress={() => this.toDetailPribadi()} />
       </View>

    );
  }

}

function mapStateToProps(state) {
  return {
    travelState: state.travelState,
    asuransiTravel: state.asuransiTravel,
    travelLabelState: state.travelLabelState,
    dataVoucher: state.dataVoucher,
    dataTravel: state.dataTravel,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeliTravel);
