import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiProperty extends React.Component {
  constructor(props) {
    super(props);
  }

  toListAsuransi(propertyState, dataProperty, lob) {
    if(!dataProperty.risk_address){
     errorMessage = 'Lokasi Resiko Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!propertyState.province_id || propertyState.province_id == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!propertyState.district_id || propertyState.district_id == 'Pilih'){
      errorMessage = 'Kabupaten / Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!propertyState.kecamatan_id || propertyState.kecamatan_id == 'Pilih'){
     errorMessage = 'Kecamatan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!propertyState.tipe || propertyState.tipe == 'Pilih'){
      errorMessage = 'Fungsi Bangunan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!propertyState.building_tsi){
      errorMessage = 'Harga Bangunan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(propertyState.building_tsi + propertyState.content_tsi > 50000000000){
      errorMessage = 'Jumlah Harga Bangunan dan Harga Isi Bangunan Maksimal 50 Miliar';
      appFunction.toastError(errorMessage);
    }else{
      const body = Object.assign({}, propertyState);

      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }

  provinsilist(propertyLabelState, lob){
    const title = 'Provinsi';
    const value = propertyLabelState.provinsi_property;
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  districtlist(propertyLabelState, lob){
    const title = 'Kabupaten / Kota';
    const value = propertyLabelState.district_property;
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});

    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  kecamatanlist(propertyLabelState, lob){
    const title = 'Kecamatan';
    const value = propertyLabelState.kecamatan_property;
    const options = this.props.listKecamatan;
    const type = Object.assign({},{state: 'kecamatan_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  kelurahanlist(propertyLabelState, lob){
    const title = 'Kelurahan';
    const value = propertyLabelState.kelurahan_property;
    const options = this.props.listKelurahan;
    const type = Object.assign({},{state: 'kelurahan_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  tipelist(propertyLabelState, lob){
	  
	var array = [...this.props.listOccupation]; // make a separate copy of the array
	array.splice(4,1);
    const title = 'Fungsi Bangunan';
    const value = propertyLabelState.tipe_property;
    const options = array;
    const type = Object.assign({},{state: 'tipe', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  buildingtsitinput(propertyState){
    const value = propertyState.building_tsi;
    var building_tsi = appFunction.numToCurrency(value);
	if(value == 0){
		building_tsi = '';
	}
    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Harga Bangunan
        </Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={[styles.font16, styles.height40, styles.baseText]}
          keyboardType='numeric'
          onChangeText={(value) => this.changeharga(value, 'building_tsi')}
          value={`${building_tsi}`}
		  placeholder = '0'
        />
      </View>
    );
  }


  contenttsitinput(propertyState){
    const value = propertyState.content_tsi;
    var content_tsi = appFunction.numToCurrency(value);
	if(value == 0){
		content_tsi = '';
	}
    return(
      <View style={styles.pickerViewBlack}>
        <Text style= {[styles.font16, styles.baseText]}>
          Harga Isi Bangunan
        </Text>
        <TextInput
          underlineColorAndroid='transparent'
          style={[styles.font16, styles.height40, styles.baseText]}
          keyboardType='numeric'
          onChangeText={(value) => this.changeharga(value, 'content_tsi')}
          value={`${content_tsi}`}
		  placeholder = '0'
        />
      </View>
    );
  }

  riskaddresstinput(dataProperty){
    const value = dataProperty.risk_address;
    return(
	<JagainTextInput
		  title = 'Lokasi Resiko'
          value={value}
          onChangeText={(value) => this.props.setRiskAddressProperty(appFunction.dotalphaNumericOnly(value))}            
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  multiline = {true}
          numberOfLines = {4}
	  />
     
    );
  }

  changeharga(value, tipe){
	var harga = value;
	if(value == ''){
		harga = 0;
	}
   

    if(value !== "" && value !== undefined && value !== null){
      harga = appFunction.currencyToNum(value);
    }

    if(tipe == 'building_tsi'){
      this.props.setBuildingTsiProperty(harga);
    }else if(tipe == 'content_tsi'){
      this.props.setContentTsiProperty(harga);
    }
  }


  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };

  componentWillReceiveProps(NextProps){
    const {propertyState} = NextProps;
    const oldPropertyState = this.props.propertyState;
    if(propertyState != oldPropertyState){
      if(propertyState.province_id != oldPropertyState.province_id){
        if(propertyState.province_id && propertyState.province_id != 'Pilih'){
          this.props.resetListDistrict();
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetDistrictProperty();
          this.props.resetKecamatanProperty();
          this.props.resetKelurahanProperty();
          this.props.getDistrict(propertyState.province_id);
        }else if(!propertyState.province_id || propertyState.province_id  == 'Pilih'){
          this.props.resetListDistrict();
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetDistrictProperty();
          this.props.resetKecamatanProperty();
          this.props.resetKelurahanProperty();
        }
      }
      if(propertyState.district_id != oldPropertyState.district_id){
        if(propertyState.district_id && propertyState.district_id!= 'Pilih'){
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetKecamatanProperty();
          this.props.resetKelurahanProperty();
          this.props.getKecamatan(propertyState.district_id);
        }else if(!propertyState.district_id || propertyState.district_id  == 'Pilih'){
          this.props.resetListKecamatan();
          this.props.resetListKelurahan();
          this.props.resetKecamatanProperty();
          this.props.resetKelurahanProperty();
        }
      }
      if(propertyState.kecamatan_id != oldPropertyState.kecamatan_id){
        if(propertyState.kecamatan_id && propertyState.kecamatan_id!= 'Pilih'){
          this.props.resetListKelurahan();
          this.props.resetKelurahanProperty();
          this.props.getKelurahan(propertyState.kecamatan_id);
        }else if(!propertyState.kecamatan_id || propertyState.kecamatan_id  == 'Pilih'){
          this.props.resetListKelurahan();
          this.props.resetKelurahanProperty();
        }
      }
    }
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    this.props.getProvinsi();
	this.props.resetListDistrict();
	this.props.resetListKecamatan();
	this.props.resetListKelurahan();
    this.props.getOkupasiProperty();
	
	if(this.props.propertyState.province_id && this.props.propertyState.province_id != 'Pilih'){
		this.props.getDistrict(this.props.propertyState.province_id);
	}else if(this.props.propertyState.district_id && this.props.propertyState.district_id!= 'Pilih'){
		this.props.getKecamatan(this.props.propertyState.district_id);	
	}else if(this.props.propertyState.kecamatan_id && this.props.propertyState.kecamatan_id!= 'Pilih'){
		this.props.getKelurahan(propertyState.kecamatan_id);	
	}

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {propertyState} = this.props;
    const propertyLabelState = this.props.labelState;
    const {dataProperty} = this.props;
    const lob = 'Property';
    //const {listPlatKendaraan} = this.props;
    console.log(propertyState);

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
        {this.riskaddresstinput(dataProperty)}
        {this.provinsilist(propertyLabelState, lob)}
        {this.districtlist(propertyLabelState, lob)}
        {this.kecamatanlist(propertyLabelState, lob)}
        {this.kelurahanlist(propertyLabelState, lob)}
        {this.tipelist(propertyLabelState, lob)}
        {this.buildingtsitinput(propertyState)}
        {this.contenttsitinput(propertyState)}
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(propertyState, dataProperty, lob)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    visible: state.visible,
    propertyState: state.propertyState,
    dataProperty:state.dataProperty,
    labelState: state.labelState,
    listProvinsi: state.listProvinsi,
    listDistrict: state.listDistrict,
    listKecamatan: state.listKecamatan,
    listKelurahan: state.listKelurahan,
    listOccupation: state.listOccupation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiProperty);
