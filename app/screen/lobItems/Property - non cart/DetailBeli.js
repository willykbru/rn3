import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, Button, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';


class DetailBeliProperty extends React.Component {
  constructor(props){
     super(props);
   }


toDetailPribadi(){
  if(this.props.dataVoucher.description !== 'ok'){ 
	this.props.resetDataVoucher();
	this.props.setKodeVoucher('');
  }
  this.props.navigate('DetailPribadi');
}


  detailPremi(dataBeliProperty){
    var perluasanpremi = [];
    var netpremi = [];
    var discvoucher=[];
    const disc_voucher = dataBeliProperty.total_premi*dataBeliProperty.voucher_persen/100;

    if(dataBeliProperty.disc_insurance !== 0){
      if(dataBeliProperty.description == 'ok'){
        if(dataBeliProperty.voucher_persen !== 0){
          netpremi.push(
            <View key='netpremi' >
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliProperty.total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
              <View style={styles.directionRow}>
                <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                  <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.voucher_persen} %</Text></View>
                </View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_voucher.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
              </View>
              <View style={styles.directionRow}>
                <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                  <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.disc_insurance} %</Text></View>
                </View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
              </View>
              <View style={styles.borderbottom1}></View>
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-disc_voucher).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
            </View>
          );
        }else{
          netpremi.push(
            <View key='netpremi' >
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{dataBeliProperty.total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
              <View style={styles.directionRow}>
                <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
              </View>
              <View style={styles.directionRow}>
                <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                  <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{disc_insurance} %</Text></View>
                </View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
              </View>
              <View style={styles.borderbottom1}></View>
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-dataBeliProperty.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
            </View>
          );
        }
      }else{
        netpremi.push(
          <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliProperty.total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.disc_insurance} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
          </View>
        );
    }

    }else{
      if(dataBeliProperty.description == 'ok'){
        if(dataBeliProperty.voucher_persen !== 0){
            netpremi.push(
              <View key='netpremi' >
                <View style={[styles.directionRow, styles.bottom]}>
                <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
                </View>
                <View style={styles.directionRow}>
                 <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                   <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                   <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.voucher_persen} %</Text></View>
                 </View>
				 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:   -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{(dataBeliProperty.net_premi*dataBeliProperty.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				 </View>
                </View>

                <View style={styles.borderbottom1}></View>
                <View style={styles.directionRow}>
                <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-(dataBeliProperty.net_premi*dataBeliProperty.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
                </View>
              </View>
          );
        }else{
          netpremi.push(
            <View key='netpremi' >
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
              <View style={styles.directionRow}>
                <View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher:</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
              </View>
              <View style={styles.borderbottom1}></View>
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-dataBeliProperty.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
            </View>
          );
        }
      }else{
            netpremi.push(
                <View key='totpremi' style={styles.directionRow}>
                <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
                </View>
            );
          }
    }

    if(dataBeliProperty.eqvet == true){
      perluasanpremi.push(
        <View key='eqvet' style={styles.directionRow}>
          <View style={styles.width50p}><Text style={[styles.baseText]}>EQVET</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  +Rp. </Text>
			<Text style={[styles.baseText]}>{dataBeliProperty.premi_eqvet.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		  </View>
        </View>
      );
    }
    if(dataBeliProperty.flood == true){
      perluasanpremi.push(
        <View key='flood' style={styles.directionRow}>
          <View style={styles.width50p}><Text style={[styles.baseText]}>FLOOD</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  +Rp. </Text>
			<Text style={[styles.baseText]}>{dataBeliProperty.premi_flood.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		  </View>
        </View>
      );
    }

    return(
      <View style={styles.centerItemContent}>
    <View style={styles.borderDetailBeli}>
      <View style={styles.directionRow}>
        <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
		<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.baseText]}>{dataBeliProperty.premi_dasar.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		</View>
      </View>
      {perluasanpremi}
      <View style={styles.borderbottom1}></View>
      {netpremi}
    </View>
  </View>
  );
  }


       componentDidMount() {
			const {package_id, package_name, insurance_name, net_premi} = this.props.asuransiProperty;
			var impressions = {package_id, package_name, lob: 'Asuransi Properti', insurance_name, price:net_premi};			
			appFunction.setImpressionGA(impressions);
       }

  render(){

    const {propertyState, asuransiProperty, dataVoucher, labelState, dataProperty} = this.props;
    const dataBeliProperty = Object.assign({}, labelState, propertyState, asuransiProperty, dataVoucher, dataProperty);
    //console.log('label',dataVoucher);
	const lob = Object.assign({}, {lob : 'PR'}); 
	const dataCheckVoucherProperty = Object.assign({},dataVoucher, asuransiProperty, lob);
      var {height, width} = Dimensions.get('window');

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }

	 var editableKodeVoucher = true;
	  
	  if(dataCheckVoucherProperty.description == 'ok'){
		  editableKodeVoucher = false;
	  }

    return(
      <View style={styles.centerItemContent}>
        <View style={styles.centerItem}>
          <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.bottom10, styles.left4, styles.radius3, styles.elev5, styles.pad5, {borderColor: '#ffe5e5', marginTop:height*.02, paddingTop:0}]}>
            <View style={styles.centerItemContent}>
              <View style={[styles.centerItemContent, styles.height70, styles.directionRow, styles.radius2, styles.bottom20]}>
                 <Image source={asimgsrc[dataBeliProperty.insurance_name]} style={[appFunction.sizeLogoAsuransi(1.5)]}/>
              </View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Property</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.tipe_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Lokasi Resiko</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.risk_address}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provinsi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.provinsi_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kabupaten / Kota</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.district_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kecamatan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.kecamatan_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kelurahan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.kelurahan_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga bangunan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {dataBeliProperty.building_tsi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Isi bangunan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {dataBeliProperty.content_tsi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text></View>
             </View>
             <View style={[styles.directionRow, styles.bottom]}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>Provide By</Text></View>
               <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
               <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliProperty.insurance_name}</Text></View>
             </View>
			  <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kode Voucher</Text></View>
               <Text style={[styles.baseText]}>:  </Text>
             <TextInput
               style={[styles.kodeVoucherTInput, styles.baseText]}
               value={dataVoucher.kode_voucher}
               onChangeText={(kode_voucher) => this.props.setKodeVoucher(appFunction.alphaNumericOnly(kode_voucher))}
				onEndEditing={() => this.props.checkVoucher(dataCheckVoucherProperty, 'checkVoucher')}
				maxLength={20}
               underlineColorAndroid='transparent'
			   placeholder='Kode Voucher'
			   editable={editableKodeVoucher}
             />
           <TouchableHighlight underlayColor='white'>
              { dataVoucher.kode_voucher ? dataBeliProperty.description == 'ok'? <Icon name='checkmark-circle' style={{color:'green'}} /> : <Icon name='checkmark-circle' style={{color:'red'}} /> : <Icon name='checkmark-circle'/>}
            </TouchableHighlight>
           </View>
            <View style={[styles.directionRow, styles.bottom5]}>
              <View style={styles.width50p}>
			  {dataCheckVoucherProperty.description == 'ok'?
			  <TouchableHighlight onPress={()=>this.props.resetKodeVoucher()} style={[{backgroundColor: '#00FF00', width: 100, height: 25}, styles.radius5, styles.elev3, styles.centerItemContent]}>
				<Text style={[styles.baseText]}>
					Edit Voucher
				</Text>
			  </TouchableHighlight>
			  :
			  null
			  }
			  </View>
                {dataBeliProperty.description == 'ok'? <Text style={[{color:'green'}, styles.font12, styles.italic, styles.baseText]}>Status Voucher Valid</Text>: <Text style={[styles.colorRed, styles.font12, styles.italic, styles.baseText]}>{dataBeliProperty.description}</Text>}
            </View>
             <View style={{height: height*.03}}></View>
			 {this.detailPremi(dataBeliProperty)}
             <View style={{height: height*.03}}></View>
            </View>
          </View>
        <SubmitButton title='Lanjut' onPress={() => this.toDetailPribadi()} />
      </View>
    );
  }

}

function mapStateToProps(state) {
  return {
    propertyState: state.propertyState,
    asuransiProperty: state.asuransiProperty,
    labelState: state.labelState,
    dataVoucher: state.dataVoucher,
    dataProperty: state.dataProperty,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeliProperty);
