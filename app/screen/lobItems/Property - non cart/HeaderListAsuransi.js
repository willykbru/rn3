import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'
import PopoverTooltip from 'react-native-popover-tooltip';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';
import CheckBoxTooltip from '../../../components/checkBox/CheckBoxTooltip';

class HeaderListAsuransiProperty extends Component{

  constructor(props){
    super(props);
  }

  changeExtStatus(status, perluasan){
    const changeStatus = !status;
    this.props.setStatusPerluasanProperty(changeStatus, perluasan);
  }

  header(propertyState){
    const perluasan = ['EQVET', 'TSFWD'];
    const eqvettext = 'EQVET : Earthquake and Volcano Eruption adalah perluasan risiko terhadap risiko kerugian/kerusakan atas Properti yang dipertanggungkan yang disebabkan secara langsung oleh Gempa Bumi, Tsunami dan atau Letusan gunung berapi.';
    const tsfwdtext = 'TSFWD : Typhoon, Storm, Flood and Water Damage adalah perluasan risiko terhadap risiko kerugian/kerusakan atas Properti yang dipertanggungkan yang disebabkan secara langsung oleh Angin topan, Badai, Banjir dan atau Tanah longsor.';

    const eqvet = propertyState.eqvet;
    const tsfwd = propertyState.flood;

    //console.log(tpl);
    //console.log(tplDisabledStatus);
      return (
          <View style={[styles.directionRow, styles.centerItemContent]}>
            <View style={[styles.width35p, styles.directionColumn]}>
              <CheckBoxTooltip parentStyle={[styles.bottom10]}
                title={perluasan[0]}
                onClick={() => this.changeExtStatus(eqvet, perluasan[0])}
                tooltipText={eqvettext}
                isChecked={eqvet}
                color='#ff4c4c' />
            </View>
            <View style={[styles.width35p, styles.directionColumn, styles.left10]}>
              <CheckBoxTooltip parentStyle={[styles.bottom10]}
                title={perluasan[1]}
                onClick={() => this.changeExtStatus(tsfwd, perluasan[1])}
                tooltipText={tsfwdtext}
                isChecked={tsfwd}
                color='#ff4c4c' />
            </View>
          </View>
      );
  }

  componentWillReceiveProps(NextProps){
    if(NextProps.propertyState != this.props.propertyState){
      const body = Object.assign({}, NextProps.propertyState);
      this.props.getListAsuransi(body, 'Property');
    }
  }


  render(){
    const {propertyState} = this.props;

    return(
	 <View>
		<View style={[styles.bgWhite, styles.top5, styles.elev1 ,styles.pad5]}>
			<Text style={[styles.baseText]}>Perluasan</Text>
		</View>
		<View style={[{backgroundColor: '#fff'},styles.centerItemContent]}>
		  <View style={[styles.centerItemContent, styles.width95p, styles.top20]}>
			  {this.header(propertyState)}
		  </View>
		</View>
	 </View>
   
    );
	
  }
}

function mapStateToProps(state) {
  return {
    propertyState: state.propertyState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderListAsuransiProperty);
