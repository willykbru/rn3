import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Modal from 'react-native-modal';

import md5 from "react-native-md5";
import * as appFunction from '../../../function'
import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';

import CheckBox from 'react-native-check-box';
import styles from '../../../assets/styles';


class PembayaranProperty extends Component{

constructor(props) {
    super(props);
    this.state = {
      agreement: false,
    }
  }

    disclaimertext(dataBeliProperty, agreement){
      return(
        <View style={[styles.centerItemContent, styles.top5, styles.width90p]}>
          <View style={styles.centerItemContent, styles.bottom10}>
            <Text style={[styles.font14, styles.baseText]}>
              Terima kasih telah mengajukan permohonan polis asuransi.
              Kami akan otomatis mengirimkan email konfirmasi pembelian
              setelah Anda menyelesaikan pembayaran. Anda mungkin
              akan dikenakan biaya tambahan jika transfer dilakukan
              dari rekening selain yang tertera pada kwitansi pembayaran.
            </Text>
          </View>
          <View style={[styles.bgBlackOp6, styles.centerItemContent, styles.radius5, styles.elev5]}>
            <View style={[styles.centerItem, styles.left5, styles.right5, styles.top10, styles.bottom10]}>
              <View style={[styles.directionRow,{alignItems: 'flex-end'}]}>
                <Text style={[styles.colorWhite, styles.baseText]}>Jumlah </Text>
                <Text style={[styles.colorWhite, styles.font16, styles.baseText]}> Rp. {dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
              <Text style={[styles.colorWhite, styles.font14, styles.baseText]}>
                Untuk pembayaran dengan metode transfer. Anda tidak
                perlu melakukan konfirmasi pembayaran. Sistem Jagain
                akan otomatis mendapatkan pemberitahuan ketika transfer berhasil.
              </Text>
            </View>
            <View style={[styles.bottom5, styles.left5, styles.directionRow, styles.width100p]}>
				<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText]}>
					* 
				</Text>
				<TouchableHighlight underlayColor='blue' onPress={()=> {this.props.navigate('Agreement')} }>
					<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText, styles.underline]}>
					syarat dan ketentuan
					</Text>
				</TouchableHighlight>
			</View>
            <View style={[styles.directionRow, styles.width90p, styles.padTop5]}>
              <CheckBox isChecked={ agreement } checkBoxColor='white' onClick={() => this.changeExtStatus()} />
              <Text style={[styles.colorWhite, styles.font14, styles.baseText, styles.bold]}>
                Saya setuju syarat dan ketentuan dengan yang berlaku di Jagain
              </Text>			  
            </View>
          </View>
        </View>
      );
    }

    doBayar(agreement){
      if(!agreement)
        appFunction.toastError('Syarat dan Ketentuan Belum Disetujui');
      else
        this.props.navigate('PaymentGateway');
    }

    changeExtStatus(agreement){

      var agreeStat = agreement;
        agreeStat = !agreement;

        this.setState(() => {
          return {
            agreement: agreeStat,
          };
        });

    }

    _renderButton = (text, onPress) => (
         <TouchableOpacity onPress={onPress}>
           <View style={styles.modalButton}>
             <Text style={[styles.baseText]}>{text}</Text>
           </View>
         </TouchableOpacity>
       );

    _renderModalContent = () => (
        <View style={styles.modalContent}>
          <Text style={[styles.baseText]}>Hello!</Text>
          {this._renderButton('Close', () => this.closeModal())}
        </View>
      );


	detailPremi(dataBeliProperty){
	  var perluasanpremi = [];
		var netpremi = [];
		var discvoucher=[];
		const disc_voucher = dataBeliProperty.total_premi*dataBeliProperty.voucher_persen/100;

		if(dataBeliProperty.disc_insurance !== 0){
		  if(dataBeliProperty.description == 'ok'){
			if(dataBeliProperty.voucher_persen !== 0){
			  netpremi.push(
				<View key='netpremi' >
				  <View style={[styles.directionRow, styles.bottom]}>
				  <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{dataBeliProperty.total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				  <View style={styles.directionRow}>
					<View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					  <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
					  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.voucher_persen} %</Text></View>
					</View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_voucher.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				  </View>
				  <View style={styles.directionRow}>
					<View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					  <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
					  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.disc_insurance} %</Text></View>
					</View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				  </View>
				  <View style={styles.borderbottom1}></View>
				  <View style={styles.directionRow}>
				  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-disc_voucher).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				</View>
			  );
			}else{
			  netpremi.push(
				<View key='netpremi' >
				  <View style={[styles.directionRow, styles.bottom]}>
				  <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataBeliProperty.total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				  <View style={styles.directionRow}>
					<View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				  </View>
				  <View style={styles.directionRow}>
					<View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					  <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
					  <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{disc_insurance} %</Text></View>
					</View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				  </View>
				  <View style={styles.borderbottom1}></View>
				  <View style={styles.directionRow}>
				  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-dataBeliProperty.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				</View>
			  );
			}
		  }else{
			netpremi.push(
			  <View key='netpremi' >
				<View style={[styles.directionRow, styles.bottom]}>
				<View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.baseText]}>{dataBeliProperty.total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
				</View>
				<View style={styles.directionRow}>
				  <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
					<View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.disc_insurance} %</Text></View>
				  </View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  -Rp. </Text>
					<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.disc_value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				</View>
				<View style={styles.borderbottom1}></View>
				<View style={styles.directionRow}>
				<View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				</View>
				</View>
			  </View>
			);
		}

		}else{
		  if(dataBeliProperty.description == 'ok'){
			if(dataBeliProperty.voucher_persen !== 0){
				netpremi.push(
				  <View key='netpremi' >
					<View style={[styles.directionRow, styles.bottom]}>
					<View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
					</View>
					<View style={styles.directionRow}>
					 <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
					   <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
					   <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliProperty.voucher_persen} %</Text></View>
					 </View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:   -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{(dataBeliProperty.net_premi*dataBeliProperty.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
					</View>

					<View style={styles.borderbottom1}></View>
					<View style={styles.directionRow}>
					<View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-(dataBeliProperty.net_premi*dataBeliProperty.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
					</View>
				  </View>
			  );
			}else{
			  netpremi.push(
				<View key='netpremi' >
				  <View style={[styles.directionRow, styles.bottom]}>
				  <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				  <View style={styles.directionRow}>
					<View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher:</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataBeliProperty.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				  </View>
				  <View style={styles.borderbottom1}></View>
				  <View style={styles.directionRow}>
				  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					<Text style={[styles.baseText]}>:  Rp. </Text>
					<Text style={[styles.bold, styles.baseText]}>{(dataBeliProperty.net_premi-dataBeliProperty.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				  </View>
				  </View>
				</View>
			  );
			}
		  }else{
				netpremi.push(
					<View key='totpremi' style={styles.directionRow}>
					<View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{dataBeliProperty.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
					</View>
				);
			  }
		}

		if(dataBeliProperty.eqvet == true){
		  perluasanpremi.push(
			<View key='eqvet' style={styles.directionRow}>
			  <View style={styles.width50p}><Text style={[styles.baseText]}>EQVET</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliProperty.premi_eqvet.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
			</View>
		  );
		}
		if(dataBeliProperty.flood == true){
		  perluasanpremi.push(
			<View key='flood' style={styles.directionRow}>
			  <View style={styles.width50p}><Text style={[styles.baseText]}>FLOOD</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliProperty.premi_flood.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
			</View>
		  );
		}

		return(
		  <View style={styles.centerItemContent}>
		<View style={styles.borderDetailBeli}>
		  <View style={styles.directionRow}>
			<View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliProperty.premi_dasar.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
		  </View>
		  {perluasanpremi}
		  <View style={styles.borderbottom1}></View>
		  {netpremi}
		</View>
	  </View>
	  );
	}
	
	componentWillReceiveProps(NextProps){
		const {dataTransaksi, asuransiProperty, dataVoucher} = NextProps;
		const kode_transaksi = dataTransaksi.kode_transaksi;
		const insurance_name = asuransiProperty.insurance_name;
		const disc_voucher = asuransiProperty.total_premi*dataVoucher.voucher_persen/100;
		var tax =  null;
		var transactions = {};
		
		if(this.props.dataTransaksi != NextProps.dataTransaksi){		
			if(asuransiProperty.disc_insurance !== 0){
				if(dataVoucher.description == 'ok'){
					if(dataVoucher.voucher_persen !== 0){
						tax = asuransiProperty.net_premi-disc_voucher; 			
					}else{
						tax = asuransiProperty.net_premi-dataVoucher.voucher_amount;
					}
				}else{
					tax = asuransiProperty.net_premi;
				}
			}else{
				if(dataVoucher.description == 'ok'){
					if(dataVoucher.voucher_persen !== 0){
						tax = asuransiProperty.net_premi-(asuransiProperty.net_premi*dataVoucher.voucher_persen/100)
					}else{
						tax = asuransiProperty.net_premi-dataVoucher.voucher_amount;
					}
				}else{
					tax = asuransiProperty.net_premi;
				}	
			}
			transactions = {kode_transaksi, insurance_name, tax};	
		
			appFunction.setTransactionGA(transactions);
		}	
	}


	render() {

    const {propertyState, asuransiProperty, dataVoucher,session, dataTransaksi, dataProperty, labelState, loadingState} = this.props;
    const dataBeliProperty = Object.assign({}, propertyState, asuransiProperty, dataVoucher,session, dataTransaksi);

    var {height, width} = Dimensions.get('window');
    const {agreement} = this.state;
    //console.log(dataTransaksi.link_pembayaran);
    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA'    : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	
	if(loadingState.loadingDataTransaksi){
		return(
			<View style={[styles.centerItemContent]}>
				<LoadingPage />
            </View> 
		)
	}



    return(
      <View style={styles.centerItem}>
          <View style={styles.centerItem}>
            <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, {marginTop:height*.03,}, styles.elev5]}>
              <View style={styles.left15}><Text style={[styles.baseText]}>Data Pemesan</Text></View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5, styles.borderTop1, styles.borderColorTopGrey]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Nama</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataBeliProperty.visitor_name}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Telpon</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataBeliProperty.visitor_phone}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Email</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataBeliProperty.username}</Text></View>
              </View>
            </View>
          </View>

          <View style={styles.centerItem}>
            <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, {marginTop:height*.03,}, styles.elev5]}>
              <View style={[styles.directionRow, styles.left15, styles.bottom5, styles.borderTop1, styles.borderColorTopWhiteOp8]}>
                <View style={styles.width45p}><Text style={[styles.baseText]}>Nomor Pemesanan</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width50p}><Text style={[styles.baseText]}>{dataBeliProperty.kode_transaksi}</Text></View>
              </View>
              <View style={styles.centerItemContent}>
              <View style={[styles.height70, styles.directionRow, styles.radius2, styles.bottom20, styles.centerItemContent]}>
                <Image source={asimgsrc[dataBeliProperty.insurance_name]} style={[styles.height25, styles.width100, styles.resizeContain]}/>
                <View style={[styles.height70, styles.left10, styles.centerContent]}><Text style={[styles.baseText]}>{dataBeliProperty.insurance_name}</Text></View>
              </View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
			  <View style={[styles.width65p, styles.directionRow]}><Text style={[styles.baseText]}>{dataProperty.nama_pemilik}</Text>{dataProperty.statusqq == 'Y' ? <Text> QQ {dataProperty.namabankleasing}</Text> : null}</View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Alamat Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataProperty.alamat_pemilik}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Lokasi Resiko</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataProperty.risk_address}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Tipe</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{labelState.tipe_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Provinsi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{labelState.provinsi_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Kabupaten / Kota</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{labelState.district_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Kecamatan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{labelState.kecamatan_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Kelurahan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{labelState.kelurahan_property}</Text></View>
            </View>
            {this.detailPremi(dataBeliProperty)}
            <View style={{height: height*.03}}></View>
           </View>
           {this.disclaimertext(dataBeliProperty, agreement)}
         </View>
         <View style={{height: height*.03}}></View>
         <SubmitButton title='Konfirmasi' onPress={() => this.doBayar(agreement)} />
        </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    dataTransaksi: state.dataTransaksi,
    propertyState:state.propertyState,
    asuransiProperty:state.asuransiProperty,
    dataVoucher:state.dataVoucher,
    session:state.session,
    dataProperty: state.dataProperty,
    labelState: state.labelState,
	loadingState: state.loadingState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PembayaranProperty);
