import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import Tooltip from '../../../components/tooltip/Tooltip';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';
import WheelPicker from '../../../components/picker/WheelPicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';
import UploadImage from '../../../components/upload/UploadImage';

class DetailPribadiProperty extends Component{

  constructor(props) {
    super(props);
  }

  genderlist(value){
    const title = 'Title';
    const lob = 'Property';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	<View style = {[styles.width40p]}>
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
        iconName='md-arrow-dropdown'
      />
	  </View>
    );
  }
  
  namatext(nama_pemilik){
	  return(
	   <JagainTextInput
	   title = 'Nama Tertanggung'
		value={nama_pemilik}
			autoCapitalize='words'
			  // Adding hint in Text Input using Place holder.
			  onChangeText={(nama_pemilik) => this.props.setNamaPemilikDP(appFunction.alphabetSpaceOnly(nama_pemilik))}
			  // Making the Under line Transparent.
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
  qqcheckbox(status_qq_bool, statusqq){
	  const tootltipText = "Maksud dari QQ adalah: dalam kapasitasnya sebagai wakil dari…";

	  return(
		<View style={styles.directionRow}>
			<Text style={[styles.baseText]}>QQ</Text>
			<CheckBox isChecked={ status_qq_bool } checkBoxColor='#ff4c4c' onClick={() => this.changeExtStatus(statusqq)} />
			<Tooltip tooltipText={tootltipText} />
		</View>
	  );
  }
  
  changeExtStatus(statusqq){
    var change_statusqq = "Y";
    if(statusqq == 'N')
      change_statusqq = "Y";
    else
      change_statusqq = "N";
    this.props.setQQDP(change_statusqq);
  }

  renderQQInput(statusqq, namabankleasing){
    if( statusqq == 'Y' ){
      return(
	   <JagainTextInput
	   title = 'Nama Bank'
		 value={namabankleasing}
			  autoCapitalize='words'
              // Adding hint in Text Input using Place holder.
              onChangeText={(namabankleasing) => this.props.setNamaBankDP(appFunction.alphabetSpaceOnly(namabankleasing))}
			  // Making the Under line Transparent.
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
      );
    }
  }
  
  alamattext(alamat_pemilik){
	  return(
	   <JagainTextInput
	   title = 'Alamat'
		value={alamat_pemilik}
			  onChangeText={(alamat_pemilik) => this.props.setAlamatPemilikDP(appFunction.dotalphaNumericOnly(alamat_pemilik))}
			multiline = {true}
			numberOfLines = {4}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
  noktptext(ktp){
	  return(
	   <JagainTextInput
	   title = 'No KTP'
		value={ktp}
				keyboardType='numeric'
				onChangeText={(ktp) => this.props.setKTPDP(appFunction.numericOnly(ktp))}
				maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
	  
  }
  
  tgllahirtext(tgl_lahir){
	  return(
	   <JagainDatePicker
		 title='Tanggal Lahir'
         date={tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
		 onDateChange={(tgl_lahir) => this.props.setTglLahirDP(tgl_lahir)}
        />
	  );
  }

  doPostData(body, dataProperty){
    var errorMessage =  '';
    if(!dataProperty.tittle || dataProperty.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.nama_pemilik){
      errorMessage = 'Nama Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.alamat_pemilik){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.ktp){
      errorMessage = 'No KTP Belum Diisi';
      appFunction.toastError(errorMessage);
    }
    //if(!photo_belakang)
    //if(!photo_depan)
  //  if(!photo_kanan)
    //if(!photo_kiri)
    //if(!photo_ktp)
    //if(!photo_stnk)
    else if(dataProperty.statusqq =='Y' && !dataProperty.namabankleasing){
      errorMessage = 'Nama Bank Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.tgl_lahir){
      errorMessage = 'Tanggal Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.photo_ktp){
      errorMessage = 'Photo KTP Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.photo_depan){
      errorMessage = 'Photo Tampak Depan Properti Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.photo_belakang){
      errorMessage = 'Photo Tampak Belakang Properti Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.photo_kanan){
      errorMessage = 'Photo Tampak Kanan Properti Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataProperty.photo_kiri){
      errorMessage = 'Photo Tampak Kiri Properti Belum Diupload';
      appFunction.toastError(errorMessage);
    }else{
      this.props.setPostData(body);
      //this.props.doPostDataProperty(body);
      this.props.navigate('Pembayaran');
    }
  }
  
  setType(value, type){
	if(type == 'ktp'){
          this.props.setPhotoKtpDP(value);
	} 
	if(type == 'depan'){
          this.props.setPhotoDepanDP(value);
	}  
	if(type == 'belakang'){
          this.props.setPhotoBelakangDP(value);
	}  
	if(type == 'kanan'){
          this.props.setPhotoKananDP(value);
	}  
	if(type == 'kiri'){
          this.props.setPhotoKiriDP(value);
	}  
  }

  fileUploaded(file, type, title){  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value, type)}  />
	  );
  }
  
  dataTertanggung(){
	const {
      alamat_pemilik,
      nama_pemilik,
      namabankleasing,
      ktp,
      photo_belakang,
      photo_depan,
      photo_kanan,
      photo_kiri,
      photo_ktp,
      statusqq,
      tgl_lahir,
      tittle,
    }  = this.props.dataProperty;
  
	  
	var status_qq_bool = false;
    if(statusqq == "Y")
      status_qq_bool = true;
    else
      status_qq_bool = false;

	

	  return(
      <View style={[styles.detailPribadiMainContainer]}>
        <WheelPicker />
				{this.genderlist(tittle)}
				<View style={[styles.top10]} />
				{this.namatext(nama_pemilik)}	  
				<View style={[styles.top10]} />
				{this.qqcheckbox(status_qq_bool, statusqq)}
				<View style={[styles.top10]} />
				{this.renderQQInput(statusqq, namabankleasing)}
				<View style={[styles.top10]} />
				{this.alamattext(alamat_pemilik)}
				<View style={[styles.top10]} />
				{this.noktptext(ktp)}
				<View style={[styles.top10]} />
				{this.tgllahirtext(tgl_lahir)}	
				<View style={[styles.top10]} />
				{this.fileUploaded(photo_ktp, 'ktp', 'KTP')}
				<View style={[styles.top10]} />
				{this.fileUploaded(photo_depan, 'depan', 'Tampak Depan')}
				<View style={[styles.top10]} />
				{this.fileUploaded(photo_belakang, 'belakang', 'Tampak Belakang')}
				<View style={[styles.top10]} />
				{this.fileUploaded(photo_kanan, 'kanan', 'Tampak Kanan')}
				<View style={[styles.top10]} />                        
				{this.fileUploaded(photo_kiri, 'kiri', 'Tampak Kiri')}
			</View>
	  );
  }
  
     componentDidMount(){

	
		this.props.resetPostData();
	
  }



	render() {
    var {height, width} = Dimensions.get('window');
    const {dataProperty, session, propertyState, dataVoucher, asuransiProperty} = this.props;

    const body = Object.assign({}, propertyState);

    const dataPostProperty = Object.assign({},dataProperty,
      {visitor_id:session.visitor_id},
      {kode_voucher:dataVoucher.kode_voucher},
     );

    const postData =  Object.assign({},{data_properties:dataPostProperty}, body, {package_id:asuransiProperty.package_id });
    //console.log(postData);

    

	  return (
		<View style = {[styles.centerItemContent]}>
			{this.dataTertanggung()}
			<View style={{height: height*.05}}></View>
			<SubmitButton title='Submit' onPress={() => this.doPostData(postData,dataProperty)} />
		</View>

	  );
  }
}

function mapStateToProps(state) {
  return {
    dataProperty: state.dataProperty,
    propertyState: state.propertyState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    asuransiProperty: state.asuransiProperty,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiProperty);
