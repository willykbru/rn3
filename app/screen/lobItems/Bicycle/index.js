import React, {Component} from 'react';
import {View, TouchableOpacity,StyleSheet, Picker, Text, TextInput, Button,Alert} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

class MV extends React.Component {
  constructor(props) {
        super(props);
          this.nextNav = this.nextNav.bind(this);
          this.prevNav = this.prevNav.bind(this);

        this.state = {
            bicyclePrice: '',
            purchaseYear:'',
            typeCoverage:'',
            bicycleMerk:'',
        };
    }

  prevNav(prevScreen){
      this.props.resetRoute(prevScreen);
    }

  nextNav(screen, typeVehicle) {
        this.props.resetRoute(screen,
          {
            prevScreen: "MV",

        });
    }

  render(){
    const { prevScreen } = this.props.navigation.state.params;


    return(
      <Container>
        <Header style={{backgroundColor: '#ff0000'}}>
          <Left>
          <TouchableOpacity style={styles.menuIcon} onPress={() => this.prevNav(prevScreen)}>
            <Icon style={{color: 'white'}}  name='arrow-round-back' />
          </TouchableOpacity>
        </Left>
        <Body><Text style={{color: 'white'}}>Asuransi Sepeda</Text></Body>
        </Header>
        <Card>
          <CardItem>
        <Content>
      <View>


        <Text>Merek & Tipe Sepeda </Text>
        <Picker
          mode= 'dialog'
          selectedValue={this.state.bicycleMerk}
          onValueChange={(itemValue, itemIndex) => this.setState({bicycleMerk: itemValue})}>
          <Picker.Item label='Pilih' value='0' />
        <Picker.Item label='Mobil' value='Mobil' />
        <Picker.Item label='Motor' value='Motor' />
        </Picker>


        <Text>Tahun Pembelian </Text>

        <Text>Harga Sepeda </Text>

        <Text>Tipe Coverage </Text>
        <Picker
          mode='dialog'
          selectedValue={this.state.typeCoverage}
          onValueChange={(itemValue, itemIndex) => this.setState({typeCoverage: itemValue})}>
          <Picker.Item label='Pilih' value='0' />
          <Picker.Item label="Comprehensive" value="Comprehensive" />
          <Picker.Item label="Total Loss Only" value="TLO" />
        </Picker>



      <View style={styles.marTop}></View>

    </View>
  </Content>
</CardItem>
</Card>
    <Footer  style={ styles.footers }>
      <View style={ styles.footerView }>
        <Text style={ styles.footerText }>Jagain.com | Copyright @ 2018</Text>
      </View>
    </Footer>

  </Container>

    );
  }

}
const styles = StyleSheet.create({
  menuIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
  },
  headers: {
    backgroundColor: '#ff0000',
  },
  marTop:{
    marginTop: 10,
  },
  banner: {
    marginBottom: 20,
  },
  button: {
    height: 50,
    backgroundColor: '#ff0000',
    borderColor:'#d43f3a',
    alignSelf: 'stretch',
    marginTop: 10,
    justifyContent: 'center'
  },
  contents: {
    flex: 1,
    flexDirection: 'row',
    marginTop:40
  },
  footers: {
    backgroundColor: '#ff0000',
  },
  whCol:{
    color: 'white',
  },
  footerView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  footerText: {
    color: 'white'
  },
  button: {
    borderWidth: 1,
    borderColor: '#bdbdbd',
    borderRadius: 8,
    width: '33.3%',
    height: 80
  },
});

function mapStateToProps(state) {
  return {
    navReducer: state.navReducer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MV);
