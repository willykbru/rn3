import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import RadioButton from '../../../components/radio/RadioButton';

import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import DatePicker from 'react-native-datepicker';
//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiHealth extends React.Component {
  constructor(props) {
    super(props);
	this.state={
		rawatinap:true,
		keadaansehat:true,
		ibuhamil:false,
		kehamilan: false,
	}
  }

  toListAsuransi(healthState, dataHealth, pageState, lob) {
    if(!dataHealth.tgl_lahir){
      errorMessage = 'Tanggal Lahir Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataHealth.no_identitas){
      errorMessage = 'No KTP/KITAS Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(pageState.rawatinap){
      errorMessage = 'Tertanggung Yang Sedang Rawat Inap Tidak Bisa Melanjutkan Ke Proses Pembelian Polis';
      appFunction.toastError(errorMessage);
    }else if(!pageState.keadaansehat){
      errorMessage = 'Tertanggung Yang Tidak Dalam Keadaan Sehat Tidak Bisa Melanjutkan Ke Proses Pembelian Polis';
      appFunction.toastError(errorMessage);
    }else if(dataHealth.gender == 'P' && pageState.ibuhamil && !pageState.kehamilan){
      errorMessage = 'Tertanggung Yang Usia Kandungannya > 7 Bulan / 28 Minggu Tidak Bisa Melanjutkan Ke Proses Pembelian Polis';
      appFunction.toastError(errorMessage);
    }else{
      const body = Object.assign({}, healthState);

      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }

  birthdatedpicker(healthState){
    return(
		 <JagainDatePicker
		 title='Tanggal Lahir'
         date={healthState.tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
         onDateChange={(tgl_lahir) => {this.props.setTglLahirDtHlt(tgl_lahir); this.props.setTglLahirHealth(tgl_lahir)}}
        />
    );
  }

  genderradio(dataHealth){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Laki-Laki',
		label2:'Perempuan',
	}

    if(dataHealth.gender == 'L'){
      selected = true;
    }else{
      selected = false;
    }	
	
    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Jenis Kelamin </Text>
		<RadioButton 
		selected={selected} 
		lbl={lbl} 
		onPress1={() => {this.props.setGenderDtHlt('L')}} 
		onPress2={() => {this.props.setGenderDtHlt('P')}} />
		</View>
    );
  }
  
  rawatinapradio(rawatinap){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Ya',
		label2:'Tidak',
	}

    return(
		<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Apakah saat ini Bapak/Ibu sedang dirawat inap di rumah sakit? </Text>
		<RadioButton 
		selected={rawatinap} 
		lbl={lbl} 
		onPress1={() => {this.setState({rawatinap:true})}} 
		onPress2={() => {this.setState({rawatinap:false})}} />
		</View>
    );
  }
  
  
    keadaansehatradio(keadaansehat){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Ya',
		label2:'Tidak',
	}

    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Apakah Bapak/Ibu dalam keadaan sehat? </Text>
		<RadioButton 
		selected={keadaansehat} 
		lbl={lbl} 
		onPress1={() => {this.setState({keadaansehat:true})}} 
		onPress2={() => {this.setState({keadaansehat:false})}} />
	 </View>
    );
  }
  
    ibuhamilradio(ibuhamil){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Ya',
		label2:'Tidak',
	}

    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Apakah saat ini Ibu sedang dalam keadaan hamil? </Text>
		<RadioButton 
		selected={ibuhamil} 
		lbl={lbl} 
		onPress1={() => {this.setState({ibuhamil:true})}} 
		onPress2={() => {this.setState({ibuhamil:false})}} />
	 </View>
    );
  }
  
  kehamilanradio(kehamilan){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1: 'usia kandungan < 7 Bulan / 28 Minggu', 
		label2: 'usia kandungan > 7 Bulan / 28 Minggu',
	}

    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Mohon disebutkan usia kehamilan Ibu saat ini? </Text>
		<RadioButton 
		selected={kehamilan} 
		lbl={lbl} 
		direction='column'
		onPress1={() => {this.setState({kehamilan:true})}} 
		onPress2={() => {this.setState({kehamilan:false})}} />
	 </View>
    );
  }

  
  ktpkitastinput(dataHealth){

    return(
	<JagainTextInput
	   title = 'No. KTP/KITA S'
		 value={dataHealth.no_identitas}
          keyboardType='numeric'
          onChangeText={(no_identitas) => {this.props.setNoIdentitasDtHlt(appFunction.numericOnly(no_identitas))}}
          maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
    );
  }


  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };


  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {healthState, accidentLabelState, dataHealth} = this.props;
	const {rawatinap, keadaansehat, ibuhamil, kehamilan} = this.state;
    const lob = 'Health';
    //const {listPlatKendaraan} = this.props;
    //console.log(healthState);
    //console.log(dataHealth);
    console.log(healthState);
	const pageState = {rawatinap, keadaansehat, ibuhamil, kehamilan}

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
        {this.birthdatedpicker(healthState)}
        <View style={[styles.top10]} />
        {this.genderradio(dataHealth)}
		{this.rawatinapradio(rawatinap)}
		{this.keadaansehatradio(keadaansehat)}
		{dataHealth.gender == 'P' ? this.ibuhamilradio(ibuhamil): null}
		{dataHealth.gender == 'P' && ibuhamil ? this.kehamilanradio(kehamilan): null}
        {this.ktpkitastinput(dataHealth)}
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(healthState, dataHealth, pageState, lob)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    healthState: state.healthState,
    dataHealth:state.dataHealth,
    healthLabelState: state.healthLabelState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiHealth);
