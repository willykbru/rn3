import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import CheckBox from 'react-native-check-box';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import RadioButton from '../../../components/radio/RadioButton';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import DatePicker from 'react-native-datepicker';
//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiLife extends React.Component {
  constructor(props) {
    super(props);
	this.state={
		agreement: false,
	}
  }

  toListAsuransi(lifeState, dataLife, agreement, lob) {
    if(!dataLife.tgl_lahir){
      errorMessage = 'Tanggal Lahir Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.no_identitas){
      errorMessage = 'No KTP/KITAS Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!agreement){
      errorMessage = 'Syarat dan Ketentuan Harus Disetujui';
      appFunction.toastError(errorMessage);
    }else if(lifeState.pekerjaan == 'iburumahtangga' && dataLife.gender !== 'P'){
 		errorMessage = 'Pekerjaan dan Jenis Kelamin Tidak Sesuai';
       appFunction.toastError(errorMessage);
	}else{
      const body = Object.assign({}, lifeState);
	//console.log(body);
      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }
  
    pekerjaanlist(lifeLabelState, lob){
    const title = 'Pekerjaan';
    const value = lifeLabelState.label_pekerjaan;
    const options = this.props.listOccupation;
    const type = Object.assign({},{state: 'pekerjaan', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }


  birthdatedpicker(lifeState){
    return(
	 <JagainDatePicker
		 title='Tanggal Lahir'
         date={lifeState.tgl_lahir}
         minDate={appFunction.lastYear(61)}
         maxDate={appFunction.lastYear(17)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
         onDateChange={(tgl_lahir) => {this.props.setTglLahirDtLfe(tgl_lahir); this.props.setTglLahirLife(tgl_lahir)}}
        />
    );
  }

  genderradio(dataLife){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Laki-Laki',
		label2:'Perempuan',
	}

    if(dataLife.gender == 'L'){
      selected = true;
    }else{
      selected = false;
    }	
	
    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Jenis Kelamin </Text>
		<RadioButton 
		selected={selected} 
		lbl={lbl} 
		onPress1={() => {this.props.setGenderDtLfe('L')}} 
		onPress2={() => {this.props.setGenderDtLfe('P')}} />
		</View>
    );
  }
  
  changeAgrStatus(agreement){
     
      var agreeStat = agreement;
        agreeStat = !agreement;

        this.setState(() => {
          return {
            agreement: agreeStat,
          };
        });

    }
  
  agreement(agreement){
	const agreementText = 'Saya Calon Tertanggung dan Calon Pemegang Polis:\
							\n\
							\n1) Bahwa saat ini Saya dalam keadaan sehat.\
							\n\
							\n2) Dalam 5 tahun terakhir, Saya tidak pernah atau dianjurkan dioperasi/dirawat di rumah sakit atau dalam masa pengobatan/dianjurkan untuk berobat atau melakukan pemeriksaan kesehatan tertentu.\
							\n\
							\n3) Saya tidak pernah atau tidak sedang menderita salah satu atau beberapa penyakit: cacat, tumor/kanker, TBC, asma, kencing manis, hati, ginjal, jantung, stroke, tekanan darah tinggi, gangguan jiwa atau penyakit lainnya.\
							\n\
							\n4) Saya tidak pernah atau tidak sedang menderita penyakit sehingga disarankan untuk : Pencangkokan Organ Tubuh, Cuci Darah karena Gagal Ginjal, Menerima tindakan lain oleh tenaga medis.\
							\n\
							\nSaya menyatakan bahwa keterangan tersebut diatas adalah yang sebenarnya dan merupakan bagian yang tidak terpisahkan dari proses permintaan asuransi yang saya ajukan. Apabila pernyataan yang saya berikan tersebut tidak benar, maka perusahaan asuransi berhak membatalkan asuransi dan berhak tidak membayar klaim yang dibuat atas dasar permohonan ini dan tidak diwajibkan mengembalikan uang premi yang telah dibayar atas nama saya. Selanjutnya saya memberi kuasa kepada setiap Dokter/Rumah Sakit/Perusahaan Asuransi/Badan Hukum/Perorangan atau Organisasi lainnya, yang mempunyai catatan atau mengetahui keadaan atau kesehatan saya untuk memberitahukan kepada perusahaan asuransi atau mereka yang diberi kuasa olehnya, segala keterangan tentang diri dan kesehatan baik semasa saya masih hidup maupun sesudah saya meninggal dunia.';  
	  return(
		<View style={[styles.centerItemContent, styles.top10, styles.width90p]}>
			<Text>{agreementText}{'\n'}</Text> 
			<View style={[styles.directionRow, styles.top5, styles.width95p]}>
			  <CheckBox isChecked={ agreement } checkBoxColor='black' onClick={() => this.changeAgrStatus(agreement)} />
			  <Text style={[ styles.font14, styles.baseText]}>
				Saya Setuju dan memahami Syarat dan Ketentuan serta Manfaat dari Produk ini
			  </Text>
			</View>
		</View>
		);
  }

  
  ktpkitastinput(dataLife){

    return(
	<JagainTextInput
	   title = 'No. KTP/KITA S'
		 value={dataLife.no_identitas}
          keyboardType='numeric'
		  // Adding hint in Text Input using Place holder.
		  onChangeText={(no_identitas) => {this.props.setNoIdentitasDtLfe(appFunction.numericOnly(no_identitas))}}
          maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
    );
  }


  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };


  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    this.props.getOkupasiLife();

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {lifeState, lifeLabelState, dataLife} = this.props;
	const {agreement} = this.state;
    const lob = 'Life';
    //const {listPlatKendaraan} = this.props;
    //console.log(lifeState);
    //console.log(dataLife);
 
	

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
		{this.pekerjaanlist(lifeLabelState, lob)}
        {this.birthdatedpicker(lifeState)}
        <View style={[styles.top10]} />
        {this.genderradio(dataLife)}
        {this.ktpkitastinput(dataLife)}
		{this.agreement(agreement)}
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(lifeState, dataLife, agreement, lob)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    lifeState: state.lifeState,
    dataLife:state.dataLife,
    lifeLabelState: state.lifeLabelState,
	listOccupation: state.listOccupation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiLife);
