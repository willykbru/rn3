import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Radio, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import UploadImage from '../../../components/upload/UploadImage';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';


//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';

class DetailPribadiLife extends Component{

  constructor(props) {
    super(props);
  } 
  
   selectkewarganegaran(value){  
	  this.props.setKewarganegaraanDtLfe(value);
  }

  kewarganegaraanradio(kewarganegaraan){
    
    var init1 = true;
	var init2 = false;
	var value = 'wni'; 
    if(kewarganegaraan == 'wni'){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
    return(
	<View style={[styles.width80p]}>
		<View style={[styles.directionRow]}>
			<Text>Kewarganegaraan</Text>
		</View>
		 <View style={[styles.directionRow]}>
		  <View style={[styles.directionRow, styles.right10]}>
			  <Radio selected={init1} onPress={() => {this.selectkewarganegaran('wni')}} />
		   <Text>WNI</Text>
		   </View>
		   <View style={[styles.directionRow]}>
			 <Radio selected={init2} onPress={() => {this.selectkewarganegaran('wna')}} />
			 <Text>WNA</Text>
			</View>
		</View>
	  </View>
    );
  }

  ahliwarislist(hub_ahliwaris){
    const lob = 'Life';
    const title = 'Hubungan Ahli Waris';
    const options = this.props.listAhliWaris;
    const type = Object.assign({},{state: 'hub_ahliwaris', lob});

    return(
      <FilterPicker title={title}
        value={hub_ahliwaris}
        options={options}
        type={type}
      />
    );
  }

  provinsilist(label_provinsi){
      const lob = 'Life';
    const title = 'Provinsi';
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={label_provinsi}
        options={options}
        type={type}
      />
    );
  }


  districtlist(label_district){
      const lob = 'Life';
    const title = 'Kabupaten / Kota';
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={label_district}
        options={options}
        type={type}
      />
    );
  }


  genderlist(value){
    const title = 'Title';
    const lob = 'Life';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	<View style = {[styles.width40p]}>
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
	  </View>
    );
  }
  
  namatext(nama){
	  return(
	   <JagainTextInput
	   title = 'Nama Tertanggung'
		 value={nama}
			autoCapitalize='words'
		  // Adding hint in Text Input using Place holder.
			  onChangeText={(nama) => this.props.setNamaPemilikDtLfe(appFunction.alphabetSpaceOnly(nama))}
          
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
   alamattext(alamat){
	  return(
	   <JagainTextInput
	   title = 'Alamat'
		 value={alamat}
			  onChangeText={(alamat) => this.props.setAlamatPemilikDtLfe(appFunction.dotalphaNumericOnly(alamat))}
			multiline = {true}
			numberOfLines = {4}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
		
	  );
  }
  
  ktpkitastext(no_identitas, ktpkitas){
	  return(
	   <JagainTextInput
	   title =  {'No '+ktpkitas}
		 value={no_identitas}
				keyboardType='numeric'
				onChangeText={(no_identitas) => this.props.setNoIdentitasDtLfe(appFunction.numericOnly(no_identitas))}
			maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  );
	  
  }
  
  
  namaahliwaristext(nama_ahliwaris){
	  return(
	   <JagainTextInput
	   title =  'Nama Ahli Waris'
		value={nama_ahliwaris}
				// Adding hint in Text Input using Place holder.
				onChangeText={(nama_ahliwaris) => this.props.setNamaAhliWarisDtLfe(nama_ahliwaris)}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  

  doPostData(body, dataLife){
    var errorMessage =  '';
    if(!dataLife.tittle || dataLife.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.nama){
      errorMessage = 'Nama Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.alamat){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.province_id || dataLife.province_id == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.district_id || dataLife.district_id == 'Pilih'){
      errorMessage = 'Kabupaten / Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.no_identitas){
      errorMessage = 'No Identitas Belum Diisi';
      appFunction.toastError(errorMessage);
    }
    //if(!photo_belakang)
    //if(!photo_depan)
  //  if(!photo_kanan)
    //if(!photo_kiri)
    //if(!photo_ktp)
    //if(!photo_stnk)
    else if(!dataLife.file_identitas){
      errorMessage = 'Foto KTP / KITAS Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.nama_ahliwaris){
      errorMessage = 'Nama Ahli Waris Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataLife.hub_ahliwaris || dataLife.hub_ahliwaris == 'Pilih'){
      errorMessage = 'Hubungan Ahli Waris Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else{
      this.props.doPostDataLife(body);
      this.props.navigate('Pembayaran');
    }
  }
  
  setType(value){
          this.props.setFileIdentitasDtLfe(value);
  }

  fileUploaded(file, title){  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value)}  />
	  );
  }
  
  dataTertanggung(){
	const {
      alamat,
      nama,
      no_identitas,
      file_identitas,
      tittle,
      kewarganegaraan,
      province_id,
      district_id,
      nama_ahliwaris,
      hub_ahliwaris,
    }  = this.props.dataLife;
	
	const {
		label_provinsi,
		label_district,
	} = this.props.lifeLabelState;
	  
  
    var ktpkitas = 'KTP';
  
    if(kewarganegaraan == 'wni'){
      ktpkitas = 'KTP';
    }else{
      ktpkitas = 'KITAS';
    }
	
	  return(
		   <View style={[styles.detailPribadiMainContainer]}>
				{this.genderlist(tittle)}
				<View style={[styles.top10]} />
				{this.namatext(nama)}	  
				<View style={[styles.top10]} />
				{this.alamattext(alamat)}
				<View style={[styles.top10]} />
				{this.provinsilist(label_provinsi)}
				<View style={[styles.top10]} />
				{this.districtlist(label_district)}
				<View style={[styles.top10]} />
				{this.kewarganegaraanradio(kewarganegaraan)}
				<View style={[styles.top10]} />
				{this.ktpkitastext(no_identitas, ktpkitas)}
				<View style={[styles.top10]} />
				{this.fileUploaded(file_identitas, ktpkitas)}
				<View style={[styles.top10]} />
				{this.namaahliwaristext(nama_ahliwaris)}
				<View style={[styles.top10]} />
				{this.ahliwarislist(hub_ahliwaris)}
			</View>
	  );
  }

  componentWillReceiveProps(NextProps){
    if(NextProps.dataLife.province_id != this.props.dataLife.province_id){
		this.props.resetDistrictDtLfe();
        this.props.resetListDistrict();
		if(NextProps.dataLife.province_id != 'Pilih')
        this.props.getDistrict(NextProps.dataLife.province_id);   
    }
  }

  componentDidMount(){
    this.props.getAhliWaris();
    this.props.getProvinsi();
  }


	render() {
		var {height, width} = Dimensions.get('window');
		const {dataLife, session, lifeState, dataVoucher, asuransiLife, lifeLabelState} = this.props;

		const body = Object.assign({}, lifeState);

		const dataPostLife = Object.assign({},dataLife,
		  {visitor_id:session.visitor_id},
		  {kode_voucher:dataVoucher.kode_voucher},
		 );

		const postData =  Object.assign({},{data_life:dataPostLife}, body, {package_id:asuransiLife.package_id });
		
		return (
			<View style = {[styles.centerItemContent]}>
				{this.dataTertanggung()}
				<View style={{height: height*.05}}></View>
				<SubmitButton title='Submit' onPress={() => this.doPostData(postData,dataLife)} />
			</View>

		);
	}
}

function mapStateToProps(state) {
  return {
    dataLife: state.dataLife,
    lifeState: state.lifeState,
    lifeLabelState: state.lifeLabelState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    asuransiLife: state.asuransiLife,
    listAhliWaris: state.listAhliWaris,
    listProvinsi: state.listProvinsi,
    listDistrict:state.listDistrict,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiLife);
