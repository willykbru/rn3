import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, Button, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';


class DetailBeliLife extends React.Component {
  constructor(props){
     super(props);
   }


toDetailPribadi(){
  this.props.navigate('DetailPribadi');
}


detailPremi(dataBeliLife){
  //var perluasanpremi = [];
 var netpremi = [];
  var discvoucher=[];


  
    if(dataBeliLife.description == 'ok'){
      if(dataBeliLife.voucher_persen !== 0){
          netpremi.push(
            <View key='netpremi' >
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliLife.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
              <View style={styles.directionRow}>
               <View style={[styles.width50p, styles.directionRow]}>
                 <Text style={[styles.italic, styles.baseText, styles.spaceBetween, styles.flex2]}>Discount Voucher</Text>
                 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliLife.voucher_persen} %</Text></View>
               </View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(dataBeliLife.premi*dataBeliLife.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
               </View>
			  </View>
              <View style={styles.borderbottom1}></View>
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliLife.premi-(dataBeliLife.premi*dataBeliLife.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
			  </View>
            </View>
        );
      }else{
        netpremi.push(
           <View key='netpremi' >
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliLife.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
            </View>
			</View>
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliLife.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
            <View style={styles.borderbottom1}></View>
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliLife.premi-dataBeliLife.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>
          </View>
        );
      }
    }else{
          netpremi.push(
              <View key='totpremi' style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.bold, styles.baseText]}>{dataBeliLife.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
          );
        }
  

  return(
    <View style={styles.centerItemContent}>
  <View style={styles.borderDetailBeli}>
    <View style={styles.directionRow}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
	  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
		<Text style={[styles.baseText]}>:  Rp. </Text>
		<Text style={[styles.baseText]}>{dataBeliLife.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
	  </View>
    </View>
    <View style={styles.borderbottom1}></View>
    {netpremi}
  </View>
</View>
);
}


        componentDidMount() {
			const {package_id, package_name, insurance_name, premi} = this.props.asuransiLife;
			var impressions = {};
			
			impressions = {package_id, package_name, lob: 'Asuransi Life', insurance_name, price:premi};			
			appFunction.setImpressionGA(impressions);
       }

  render(){

    const {lifeState, asuransiLife, dataVoucher, dataLife, lifeLabelState} = this.props;
    const dataBeliLife = Object.assign({}, lifeState, lifeLabelState, asuransiLife, dataVoucher, dataLife);
    console.log('label',dataBeliLife);
	const dataCheckVoucher = Object.assign({},dataVoucher, asuransiLife);
      var {height, width} = Dimensions.get('window');
      var jenis_kelamin = 'Laki-Laki';
      if(dataBeliLife.gender == 'L'){
        jenis_kelamin = 'Laki-Laki';
      }else{
        jenis_kelamin = 'Perempuan';
      }

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }



    return(
      <View style={styles.centerItemContent}>
        <View style={styles.centerItem}>
        <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.bottom10, styles.left4, styles.radius3, styles.elev5, {borderColor: '#ffe5e5', marginTop:height*.1, paddingTop:0}]}>
          <View style={styles.centerItemContent}>
           <View style={[styles.centerItemContent, styles.height70, styles.directionRow, styles.radius2, styles.bottom20]}>
             <Image source={asimgsrc[dataBeliLife.insurance_name]} style={[appFunction.sizeLogoAsuransi(1.5)]}/>
           </View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Tanggal Lahir</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliLife.tgl_lahir))}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Kelamin</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{jenis_kelamin}</Text></View>
          </View>
		  <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Pekerjaan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliLife.label_pekerjaan}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>No. KTP/KITA S</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliLife.no_identitas}</Text></View>
          </View>
           <View style={[styles.directionRow, styles.bottom]}>
             <View style={styles.width50p}><Text style={[styles.baseText]}>Provide By</Text></View>
             <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
             <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliLife.insurance_name}</Text></View>
           </View>

          
           {this.detailPremi(dataBeliLife)}
           <View style={{height: height*.03}}></View>
         </View>
      </View>
        <SubmitButton title='Lanjut' onPress={() => this.toDetailPribadi()} />
       </View>

    );
  }

}

function mapStateToProps(state) {
  return {
    lifeState: state.lifeState,
    asuransiLife: state.asuransiLife,
    lifeLabelState: state.lifeLabelState,
    dataVoucher: state.dataVoucher,
    dataLife: state.dataLife,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeliLife);
