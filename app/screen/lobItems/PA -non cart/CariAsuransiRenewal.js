import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker'; 
import FilterLblPicker from '../../../components/picker/filterLblPicker';
import RadioButton from '../../../components/radio/RadioButton';

import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import DatePicker from 'react-native-datepicker';
//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiRenewalPA extends React.Component {
  constructor(props) {
    super(props);
  }

  toListAsuransi(renewalLabel, renewalAccident) {
    if (!renewalLabel.pekerjaan || renewalLabel.pekerjaan == 'Pilih'){
      errorMessage = 'Pekerjaan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else{

      const postData = Object.assign({}, { pekerjaan: renewalAccident.pekerjaan, package_id: 0, tgl_lahir: renewalAccident.tgl_lahir });
      if (this.props.detailPolis.pekerjaan == renewalAccident.pekerjaan){
        this.props.setDataRenewalSama(true);
      } else {
        this.props.setDataRenewalSama(false);
      }
      this.props.getListAsuransi(postData, 'Accident');
      this.props.setScreen('ListAsuransiRenewal', 'renewal');
      //this.props.navigate('ListAsuransiRenewal');
    }
  }

  pekerjaanlist(renewalLabel, lob){
    const title = 'Pekerjaan';
    const value = renewalLabel.pekerjaan;
    const options = this.props.listOccupation;
    const type = Object.assign({},{state: 'pekerjaan', lob});
    

    return(
      <FilterLblPicker title={title}
        value={value}
        options={options}
        type={type}
        useFunction={true}
        onPick={(value, label) => {this.props.setPekerjaanRenewalPA(value); this.props.setLblPekerjaanRenewal(label);}}
      />
    );
  }

  birthdatedpicker(accidentState){
    return(
	 <JagainDatePicker
		 title='Tanggal Lahir'
         date={accidentState.tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}

		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
         onDateChange={(tgl_lahir) => {this.props.setTglLahirDtAcd(tgl_lahir); this.props.setTglLahirAccident(tgl_lahir)}}
        />
    );
  }
  
  genderradio(dataAccident){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Laki-Laki',
		label2:'Perempuan',
	}

    if(dataAccident.gender == 'L'){
      selected = true;
    }else{
      selected = false;
    }	
	
    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Jenis Kelamin </Text>
		<RadioButton 
		selected={selected} 
		lbl={lbl} 
		onPress1={() => {this.props.setGenderDtAcd('L')}} 
		onPress2={() => {this.props.setGenderDtAcd('P')}} />
	 </View>
    );
  }

  ktpkitastinput(dataAccident){

    return(
	<JagainTextInput
	   title = 'No. KTP/KITA S'
		value={dataAccident.no_identitas}
          keyboardType='numeric'
		  // Adding hint in Text Input using Place holder.
		  onChangeText={(no_identitas) => {this.props.setNoIdentitasDtAcd(appFunction.numericOnly(no_identitas))}}
          maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
    );
  } 

  componentDidMount() {
    this.props.setLblPekerjaanRenewal(appFunction.findLabel(this.props.listOccupation, this.props.renewalAccident.pekerjaan));
  }

  render(){
    const { accidentState, accidentLabelState, dataAccident, renewalLabel, renewalAccident, listOccupation} = this.props;
    const lob = 'Accident';
    //const {listPlatKendaraan} = this.props;
    //console.log(accidentState);
 /*   var gender_props = [
      {label: 'Laki-Laki', value: 'L' },
      {label: 'Perempuan', value: 'P' }
    ];
	*/
    //console.log('listOccupation',listOccupation);
    //console.log('renewalAccident', renewalAccident);

    return(
      <View style={[styles.centerItemContent, styles.top20]}>	   
        {this.pekerjaanlist(renewalLabel, lob)}
		<View style={[styles.top10]} />
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(renewalLabel, renewalAccident)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    visible: state.visible,
    accidentState: state.accidentState,
    dataAccident:state.dataAccident,
    accidentLabelState: state.accidentLabelState,
    listOccupation: state.listOccupation,
    renewalLabel: state.renewalLabel,
    renewalAccident: state.renewalAccident,
    detailPolis: state.detailPolis,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiRenewalPA);
