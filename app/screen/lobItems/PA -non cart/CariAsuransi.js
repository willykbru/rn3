import React, {Component} from 'react';
import {View, BackHandler, Image, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import RadioButton from '../../../components/radio/RadioButton';

import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import DatePicker from 'react-native-datepicker';
//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiPA extends React.Component {
  constructor(props) {
    super(props);
  }

  toListAsuransi(accidentState, dataAccident, lob) {
    if(!accidentState.pekerjaan || accidentState.pekerjaan == 'Pilih'){
      errorMessage = 'Pekerjaan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!accidentState.tgl_lahir){
      errorMessage = 'Tanggal Lahir Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.no_identitas){
      errorMessage = 'No Identitas Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(accidentState.pekerjaan == 'iburumahtangga' && dataAccident.gender !== 'P'){
 		errorMessage = 'Pekerjaan dan Jenis Kelamin Tidak Sesuai';
       appFunction.toastError(errorMessage);
	}else{
      const body = Object.assign({}, accidentState);

      this.props.getListAsuransi(body, lob);
      this.props.navigate('ListAsuransi');
    }
  }

  pekerjaanlist(accidentLabelState, lob){
    const title = 'Pekerjaan';
    const value = accidentLabelState.label_pekerjaan;
    const options = this.props.listOccupation;
    const type = Object.assign({},{state: 'pekerjaan', lob});


    return(
      <FilterLabelPicker title={title}
        value={value}
        options={options}
        type={type}
      />
    );
  }

  birthdatedpicker(accidentState){
    return(
	 <JagainDatePicker
		 title='Tanggal Lahir'
         date={accidentState.tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
         onDateChange={(tgl_lahir) => {this.props.setTglLahirDtAcd(tgl_lahir); this.props.setTglLahirAccident(tgl_lahir)}}
        />
    );
  }
  
  genderradio(dataAccident){
    //console.log(dataAccident.gender);
    var selected = true;
	const lbl = {
		label1:'Laki-Laki',
		label2:'Perempuan',
	}

    if(dataAccident.gender == 'L'){
      selected = true;
    }else{
      selected = false;
    }	
	
    return(
	<View style={styles.pickerViewTr}>
        <Text style={[styles.font16, styles.baseText]}>Jenis Kelamin </Text>
		<RadioButton 
		selected={selected} 
		lbl={lbl} 
		onPress1={() => {this.props.setGenderDtAcd('L')}} 
		onPress2={() => {this.props.setGenderDtAcd('P')}} />
	 </View>
    );
  }

  ktpkitastinput(dataAccident){

    return(
	<JagainTextInput
	   title = 'No. KTP/KITA S'
		value={dataAccident.no_identitas}
          keyboardType='numeric'
		  // Adding hint in Text Input using Place holder.
		  onChangeText={(no_identitas) => {this.props.setNoIdentitasDtAcd(appFunction.numericOnly(no_identitas))}}
          maxLength={16}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
    );
  }


  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  };


  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    this.props.getOkupasiAccident();

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {accidentState, accidentLabelState, dataAccident} = this.props;
    const lob = 'Accident';
    //const {listPlatKendaraan} = this.props;
    //console.log(accidentState);
 /*   var gender_props = [
      {label: 'Laki-Laki', value: 'L' },
      {label: 'Perempuan', value: 'P' }
    ];
	*/
    console.log(dataAccident);
    console.log(accidentState);

    return(
      <View style={[styles.centerItemContent, styles.top20]}>	   
        {this.pekerjaanlist(accidentLabelState, lob)}
        {this.birthdatedpicker(accidentState)}
        <View style={[styles.top10]} />
		<View style={[{width:'80%'}]}>
		{this.genderradio(dataAccident)}
		</View>
		<View style={[styles.top10]} />
        {this.ktpkitastinput(dataAccident)}
        <SubmitButton title='Cari Asuransi' onPress={() => this.toListAsuransi(accidentState, dataAccident, lob)} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    visible: state.visible,
    accidentState: state.accidentState,
    dataAccident:state.dataAccident,
    accidentLabelState: state.accidentLabelState,
    listOccupation: state.listOccupation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiPA);
