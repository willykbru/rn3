import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, Button, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';


class DetailBeliPA extends React.Component {
  constructor(props){
     super(props);
   }


toDetailPribadi(){
  if(this.props.asuransiAccident.gratis == 'Y' && this.props.dataVoucher.description !== 'ok'){
		appFunction.toastError('Harus Menggunakan Kode Voucher Khusus');
	}else{
		this.props.navigate('DetailPribadi');
	}
}


detailPremi(dataBeliAccident){
  var perluasanpremi = [];
  var netpremi = [];
  var discvoucher=[];

  var disc_premi = dataBeliAccident.premi*dataBeliAccident.disc_insurance/100;

  if(dataBeliAccident.disc_insurance !== 0){
    if(dataBeliAccident.description == 'ok'){
      if(dataBeliAccident.voucher_persen !== 0){
        netpremi.push(
          <View key='netpremi' >
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={[styles.directionRow, styles.bottom]}>
			
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
				{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>{dataBeliAccident.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>: <Text style={[styles.baseText]}>GRATIS</Text>}
             </View>
			</View>	
			:null}
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliAccident.voucher_persen} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(dataBeliAccident.premi*dataBeliAccident.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
			</View>: null}
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliAccident.disc_insurance} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>  
            </View>
			 : null}
            { dataBeliAccident.gratis == 'N' ? <View style={styles.borderbottom1}></View> : <View style={{borderBottomWidth: .5}}></View>}
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
				{ dataBeliAccident.gratis == 'N' ?
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliAccident.net_premi-(dataBeliAccident.premi*dataBeliAccident.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				:
				<Text style={[styles.bold, styles.baseText]}>GRATIS</Text>
				
				}
			  </View>
            </View>
			{ dataBeliAccident.gratis !== 'N' ? 
			  <View style={[styles.directionRow]}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}></Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			  <Text style={[styles.baseText]}> </Text>
			  <View style={[styles.directionColumn]}>
			  <Text style={[styles.bold, {fontSize:10}]}>MENGGUNAKAN VOUCHER</Text>
			  </View>
			  </View>
			  </View>
			  : null
			  }
          </View>
        );
      }else{
        netpremi.push(
          <View key='netpremi' >
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliAccident.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
            </View>	
			:null}	
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliAccident.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>	
            </View>
			:null}	
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliAccident.disc_insurance} %</Text></View>
              </View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
	          </View>
            </View>
			:null}
            { dataBeliAccident.gratis == 'N' ? <View style={styles.borderbottom1}></View> : <View style={{borderBottomWidth: .5}}></View>}
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : null }
			{ dataBeliAccident.gratis == 'N' ?
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliAccident.net_premi-dataBeliAccident.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
            :
				<Text style={[styles.bold, styles.baseText]}>GRATIS</Text>
            }			
			 </View>
			</View>
			{ dataBeliAccident.gratis !== 'N' ? 
			  <View style={[styles.directionRow]}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}></Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			  <Text style={[styles.baseText]}> </Text>
			  <View style={[styles.directionColumn]}>
			  <Text style={[styles.bold, {fontSize:10}]}>MENGGUNAKAN VOUCHER</Text>
			  </View>
			  </View>
			  </View>
			  : null
			  }
          </View>
        );
      }
    }else{
      netpremi.push(
        <View key='netpremi' >
			{ dataBeliAccident.gratis == 'N' ? 
          <View style={[styles.directionRow, styles.bottom]}>
          <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			<Text style={[styles.baseText]}>:  Rp. </Text>
			<Text style={[styles.baseText]}>{dataBeliAccident.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
          </View>
		  </View>
			:null}
			{ dataBeliAccident.gratis == 'N' ? 
          <View style={styles.directionRow}>
            <View style={[styles.width50p, styles.directionRow, styles.spaceBetween, styles.flex2]}>
              <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
              <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliAccident.disc_insurance} %</Text></View>
            </View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			</View>
          </View>
			:null}
          { dataBeliAccident.gratis == 'N' ? <View style={styles.borderbottom1}></View> : <View style={{borderBottomWidth: .5}}></View>}
          <View style={styles.directionRow}>
          <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
		  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
			{ dataBeliAccident.gratis == 'N' ?
			<Text style={[styles.bold, styles.baseText]}>{dataBeliAccident.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			:
			<Text style={[styles.bold, styles.baseText]}>GRATIS</Text>
			}
		  </View>
          </View>
		   { dataBeliAccident.gratis !== 'N' ? 
			  <View style={[styles.directionRow]}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}></Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			  <Text style={[styles.baseText]}> </Text>
			  <View style={[styles.directionColumn]}>
			  <Text style={[styles.bold, {fontSize:10}]}>MENGGUNAKAN VOUCHER</Text>
			  </View>
			  </View>
			  </View>
			  : null 
			  }
        </View>
      );
  }

  }else{
    if(dataBeliAccident.description == 'ok'){
      if(dataBeliAccident.voucher_persen !== 0){
          netpremi.push(
            <View key='netpremi' >
			{ dataBeliAccident.gratis == 'N' ? 
              <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliAccident.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
              </View>
			:null}
			{ dataBeliAccident.gratis == 'N' ? 
              <View style={styles.directionRow}>
               <View style={[styles.width50p, styles.directionRow]}>
                 <Text style={[styles.italic, styles.baseText, styles.spaceBetween, styles.flex2]}>Discount Voucher</Text>
                 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataBeliAccident.voucher_persen} %</Text></View>
               </View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{(dataBeliAccident.net_premi*dataBeliAccident.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
               </View>
			  </View>
			: null}
              { dataBeliAccident.gratis == 'N' ? <View style={styles.borderbottom1}></View> : <View style={{borderBottomWidth: .5}}></View>}
              <View style={styles.directionRow}>
              <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
				{ dataBeliAccident.gratis == 'N' ? 
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliAccident.net_premi-(dataBeliAccident.net_premi*dataBeliAccident.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				:
				<Text style={[styles.bold, styles.baseText]}>GRATIS</Text>
				}
			  </View>
			  </View>
			  { dataBeliAccident.gratis !== 'N' ? 
			  <View style={[styles.directionRow]}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}></Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			  <Text style={[styles.baseText]}> </Text>
			  <View style={[styles.directionColumn]}>
			  <Text style={[styles.bold, {fontSize:10}]}>MENGGUNAKAN VOUCHER</Text>
			  </View>
			  </View>
			  </View>
			  : null
			  }
            </View>
        );
      }else{
        netpremi.push(
          <View key='netpremi' >
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
				<Text style={[styles.baseText]}>{dataBeliAccident.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
            </View>
			</View>
			:null}
			{ dataBeliAccident.gratis == 'N' ? 
            <View style={styles.directionRow}>
              <View style={[styles.width50p, styles.directionRow]}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  -Rp. </Text>
				<Text style={[styles.italic, styles.baseText]}>{dataBeliAccident.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			  </View>
            </View>
			:null}
            { dataBeliAccident.gratis == 'N' ? <View style={styles.borderbottom1}></View> : <View style={{borderBottomWidth: .5}}></View>}
            <View style={styles.directionRow}>
            <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
			<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
				{ dataBeliAccident.gratis == 'N' ? 
				<Text style={[styles.bold, styles.baseText]}>{(dataBeliAccident.net_premi-dataBeliAccident.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				:
				<Text style={[styles.bold, styles.baseText]}>GRATIS</Text>
				}
			</View>
            </View>
			{ dataBeliAccident.gratis !== 'N' ? 
			  <View style={[styles.directionRow]}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}></Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			  <Text style={[styles.baseText]}> </Text>
			  <View style={[styles.directionColumn]}>
			  <Text style={[styles.bold, {fontSize:10}]}>MENGGUNAKAN VOUCHER</Text>
			  </View>
			  </View>
			  </View>
			  : null
			  }
			
          </View>
        );
      }
    }else{
          netpremi.push(
              <View key='totpremi'>
			  <View style={[styles.directionRow]}>
				  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Net Premi</Text></View>
				  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
						{ dataBeliAccident.gratis == 'N' ? 
						<Text style={[styles.bold, styles.baseText]}>{dataBeliAccident.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text> :
						<Text style={[styles.bold, styles.baseText]}>GRATIS</Text>		
						}
				  </View>
			  </View>
			  { dataBeliAccident.gratis !== 'N' ? 
			  <View style={[styles.directionRow]}>
			  <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}></Text></View>
			  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			  <Text style={[styles.baseText]}> </Text>
			  <View style={[styles.directionColumn]}>
			  <Text style={[styles.bold, {fontSize:10}]}>MENGGUNAKAN VOUCHER</Text>
			  </View>
			  </View>
			  </View>
			  : null
			  }			  
              </View>
          );
        }
  }

  return(
    <View style={styles.centerItemContent}>
  <View style={styles.borderDetailBeli}>
    <View style={styles.directionRow}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
	  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
		{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>:  Rp. </Text> : <Text style={[styles.baseText]}>:</Text>}
		{ dataBeliAccident.gratis == 'N' ? <Text style={[styles.baseText]}>{dataBeliAccident.premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text> : <Text style={[styles.baseText]}>GRATIS</Text>}
	  </View>
    </View>
    {perluasanpremi}
    { dataBeliAccident.gratis == 'N' ? <View style={styles.borderbottom1}></View> : <View style={{borderBottomWidth: .5}}></View>}
    {netpremi}
  </View>
</View>
);
}

	componentWillReceiveProps(NextProps) {
		if (this.props.dataVoucher !== NextProps.dataVoucher) {
			if (NextProps.dataVoucher.description == 'ok'){
				if (NextProps.asuransiAccident.remark || this.props.asuransiAccident.remark){
					appFunction.setFALogEvent('input_kode_voucher', { kode_voucher: NextProps.dataVoucher.kode_voucher, remark: NextProps.asuransiAccident.remark, status: 'success' });
				}else{
					appFunction.setFALogEvent('input_kode_voucher', { kode_voucher: NextProps.dataVoucher.kode_voucher, status: 'success' });
				}
			}else{
				if (NextProps.asuransiAccident.remark || this.props.asuransiAccident.remark){
					appFunction.setFALogEvent('input_kode_voucher', { kode_voucher: NextProps.dataVoucher.kode_voucher, remark: NextProps.asuransiAccident.remark, status: 'gagal' });
				} else {
					appFunction.setFALogEvent('input_kode_voucher', { kode_voucher: NextProps.dataVoucher.kode_voucher, status: 'gagal' });
				}
			}
		}
	}

       componentDidMount() {
			const {package_id, package_name, insurance_name, net_premi} = this.props.asuransiAccident;
			var impressions = {package_id, package_name, lob: 'Asuransi Kecelakaan', insurance_name, price:net_premi};			
			appFunction.setImpressionGA(impressions);
       }

  render(){

    const {accidentState, asuransiAccident, dataVoucher, accidentLabelState, dataAccident} = this.props;
    const dataBeliAccident = Object.assign({}, accidentLabelState, accidentState, asuransiAccident, dataVoucher, dataAccident);
    //console.log('label',dataVoucher);
	const lob = Object.assign({}, {lob : 'AC'}); 
	const dataCheckVoucher = Object.assign({},dataVoucher, asuransiAccident, lob);
      var {height, width} = Dimensions.get('window');
      var jenis_kelamin = 'Laki-Laki';
      if(dataBeliAccident.gender == 'L'){
        jenis_kelamin = 'Laki-Laki';
      }else{
        jenis_kelamin = 'Perempuan';
      }

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	
	
	var editableKodeVoucher = true;
	  
	  if(dataCheckVoucher.description == 'ok'){
		  editableKodeVoucher = false;
	  }


    return(
      <View style={styles.centerItemContent}>
        <View style={styles.centerItem}>
        <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.bottom10, styles.left4, styles.radius3, styles.elev5, styles.pad5, {borderColor: '#ffe5e5', marginTop:height*.1, paddingTop:0}]}>
          <View style={styles.centerItemContent}>
           <View style={[styles.centerItemContent, styles.height70, styles.directionRow, styles.radius2, styles.bottom20]}>
             <Image source={asimgsrc[dataBeliAccident.insurance_name]} style={[appFunction.sizeLogoAsuransi(1.5)]}/>
           </View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Tanggal Lahir</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataBeliAccident.tgl_lahir))}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Kelamin</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{jenis_kelamin}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Pekerjaan</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliAccident.label_pekerjaan}</Text></View>
          </View>
          <View style={[styles.directionRow, styles.bottom]}>
            <View style={styles.width50p}><Text style={[styles.baseText]}>No. KTP/KITA S</Text></View>
            <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
            <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliAccident.no_identitas}</Text></View>
          </View>
           <View style={[styles.directionRow, styles.bottom]}>
             <View style={styles.width50p}><Text style={[styles.baseText]}>Asuransi dari</Text></View>
             <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
             <View style={styles.width45p}><Text style={[styles.baseText]}>{dataBeliAccident.insurance_name}</Text></View>
           </View>

           <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kode Voucher</Text></View>
               <Text style={[styles.baseText]}>:  </Text>
             <TextInput
               style={[styles.kodeVoucherTInput, styles.baseText]}
			   autoCapitalize='characters'
               value={dataVoucher.kode_voucher}
               onChangeText={(kode_voucher) => this.props.setKodeVoucher(appFunction.alphaNumericOnly(kode_voucher))}
				onEndEditing={() => this.props.checkVoucher(dataCheckVoucher, 'checkVoucher')}
				maxLength={20}
               underlineColorAndroid='transparent'
			   placeholder='Kode Voucher'
			   editable={editableKodeVoucher}
             />
           <TouchableHighlight underlayColor='white'>
              { dataVoucher.kode_voucher ? dataBeliAccident.description == 'ok'? <Icon name='checkmark-circle' style={{color:'green'}} /> : <Icon name='checkmark-circle' style={{color:'red'}} /> : <Icon name='checkmark-circle'/>}
            </TouchableHighlight>
           </View>
            <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}>
			  {dataCheckVoucher.description == 'ok'?
			  <TouchableHighlight onPress={()=>this.props.resetKodeVoucher()} style={[{backgroundColor: '#00FF00', width: 100, height: 25}, styles.radius5, styles.elev3, styles.centerItemContent]}>
				<Text style={[styles.baseText]}>
					Edit Voucher
				</Text>
			  </TouchableHighlight>
			  :
			  null
			  }
			  </View>
                {dataBeliAccident.description == 'ok'? <Text style={[{color:'green'}, styles.font12, styles.italic, styles.baseText]}>Status Voucher Valid</Text>: <Text style={[styles.colorRed, styles.font12, styles.italic, styles.baseText]}>{dataBeliAccident.description}</Text>}
            </View>
           <View style={{height: height*.03}}></View>
           {this.detailPremi(dataBeliAccident)}
           <View style={{height: height*.03}}></View>
         </View>
      </View>
        <SubmitButton title='Lanjut' onPress={() => this.toDetailPribadi()} />
       </View>

    );
  }

}

function mapStateToProps(state) {
  return {
    accidentState: state.accidentState,
    asuransiAccident: state.asuransiAccident,
    accidentLabelState: state.accidentLabelState,
    dataVoucher: state.dataVoucher,
    dataAccident: state.dataAccident,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeliPA);
