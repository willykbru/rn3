import React, { Component } from 'react';
import { Alert, BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Radio, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterPick from '../../../components/picker/filterPick';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import LoadingPage from '../../../components/loading/LoadingPage';
import FilterLblPicker from '../../../components/picker/filterLblPicker';

import JagainTextInputEditable from '../../../components/text_input/JagainTextInputEditable';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';
import UploadImage from '../../../components/upload/UploadImage';

class RenewalPA extends Component{
  constructor(props) {
    super(props);
	this.state = {
		loading: false,
	}
  }
  
  selectkewarganegaraan(value){  
	  this.props.setKewarganegaraanDtAcd(value);
  }


  ahliwarislist(hub_ahliwaris){
    const lob = 'Accident';
    const title = 'Hubungan Ahli Waris';
    const options = this.props.listAhliWaris;
    const type = Object.assign({},{state: 'hub_ahliwaris', lob});


    return(
      <FilterPick title={title}
        value={hub_ahliwaris}
        options={options}
        type={type}
        useFunction={true}
        onPick={(value) => {
          this.props.setHubAhliWarisRenewalPA(value);
        }}
      />
    );
  }

  provinsilist(label_provinsi){
      const lob = 'Accident';
    const title = 'Provinsi';
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
     
      <FilterLblPicker title={title}
        value={label_provinsi}
        options={options}
        type={type}
        useFunction={true}
        onPick={(value, label) => {
          this.props.setProvinceRenewalPA(value); this.props.setProvinceName(label);
          if(value == 'Pilih' || value == ''){
            this.props.resetDistrictRenewalPA();
            this.props.resetDistrictName();
            this.props.resetListDistrict();
          }else{
            this.props.getDistrict(value)
          }
        }}
      />
    );
  }


  districtlist(label_district){
      const lob = 'Accident';
    const title = 'Kabupaten / Kota';
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});


    return(
      <FilterLblPicker title={title}
        value={label_district}
        options={options}
        type={type}
        useFunction={true}
        onPick={(value, label) => { 
          this.props.setDistrictRenewalPA(value); this.props.setDistrictName(label); 
        }}
      />
     
    );
  }


  genderlist(value){
    const title = 'Title';
    const lob = 'Accident';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	<View style = {[styles.width40p]}>
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
		disabled={true}
      />
	</View>
    );
  }
  
  namatext(nama){
	  return(
	     <JagainTextInputEditable
		 editable={false}
	   title = 'Nama Tertanggung'
		 value={nama}
			autoCapitalize='words'
		  // Adding hint in Text Input using Place holder. 
		  onChangeText={(nama) => this.props.setNamaPemilikDtAcd(appFunction.alphabetSpaceOnly(nama))}
          
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
   alamattext(alamat){
	  return(
	     <JagainTextInputEditable
	   title = 'Alamat'
		 value={alamat}
        onChangeText={(alamat) => this.props.setAlamatRenewalPA(appFunction.dotalphaNumericOnly(alamat))}
			multiline = {true}
			numberOfLines = {4}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
  ktpkitastext(no_identitas, ktpkitas){
	  return(
	     <JagainTextInputEditable
		 editable={false}
	   title =  {'No '+ ktpkitas}
		value={no_identitas}
				keyboardType='numeric'
				onChangeText={(no_identitas) => this.props.setNoIdentitasDtAcd(appFunction.numericOnly(no_identitas))}
				maxLength={16}
				
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
	  
  }
  
    tempatlahirtext(tempat_lahir){
	  return(
	     <JagainTextInputEditable
		 editable={false}
	   title = 'Tempat Lahir'
		 value={tempat_lahir}
			autoCapitalize='words'
		  // Adding hint in Text Input using Place holder. 
		  onChangeText={(tempat_lahir) => this.props.setTempatLahirDtAcd(appFunction.alphabetSpaceOnly(tempat_lahir))}   
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
  namaahliwaristext(nama_ahliwaris){
	  return(
	     <JagainTextInputEditable
	   title =  'Nama Ahli Waris'
		value={nama_ahliwaris}
				autoCapitalize='words'
				// Adding hint in Text Input using Place holder.
        onChangeText={(nama_ahliwaris) => this.props.setAhliWarisRenewalPA(nama_ahliwaris)}
				
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  

  doPostData(dataAccident){
    var errorMessage =  '';
    if (!dataAccident.data_accident.tittle || dataAccident.data_accident.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
    } else if (!dataAccident.data_accident.nama){
      errorMessage = 'Nama Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    } else if (!dataAccident.data_accident.tempat_lahir){
      errorMessage = 'Tempat Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    } else if (!dataAccident.data_accident.alamat){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    } else if (!this.props.renewalLabel.province || this.props.renewalLabel.province == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    } else if (!this.props.renewalLabel.district || this.props.renewalLabel.district == 'Pilih'){
      errorMessage = 'Kabupaten / Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    } else if (!dataAccident.data_accident.no_identitas){
      errorMessage = 'No Identitas Belum Diisi';
      appFunction.toastError(errorMessage);
    }
    //if(!photo_belakang)
    //if(!photo_depan)
  //  if(!photo_kanan)
    //if(!photo_kiri)
    //if(!photo_ktp)
    //if(!photo_stnk)
    else if (!dataAccident.data_accident.nama_ahliwaris){
      errorMessage = 'Nama Ahli Waris Belum Diisi';
      appFunction.toastError(errorMessage);
    } else if (!dataAccident.data_accident.hub_ahliwaris || dataAccident.data_accident.hub_ahliwaris == 'Pilih'){
      errorMessage = 'Hubungan Ahli Waris Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else{
	//console.log(dataAccident);
	
	Alert.alert(
			  'Renewal Polis',
			  'Yakin Ingin Renewal? \nPaket: ' + this.props.asuransiAccident.package_name + ' \nAsuransi: ' 
			  + this.props.asuransiAccident.insurance_name + ' \nHarga Premi: Rp.' + appFunction.numToCurrency(this.props.asuransiAccident.net_premi) + '.00',
			  [
				{text: 'Batal', onPress: () => console.log(dataAccident), style: 'cancel'},
				{text: 'OK', onPress: () => { this.props.doPostDataAccident(dataAccident);
				if(this.props.asuransiAccident.gratis == 'N'){
						this.props.setScreen('PembayaranRenewal', 'renewal');
					}else{
						this.setState({loading:true});
					}
				}},
			  ],
			  { cancelable: false }
			)	
	
	
	}
     
      
  }



  setType(value){
          this.props.setFileIdentitasDtAcd(value);
  }

  fileUploaded(file, title){  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value)}  />
	  );
  }
  
  dataTertanggung(){
	const {
      alamat,
      nama,
      no_identitas,
      file_identitas,
      tittle,
      kewarganegaraan,
      province_id,
      district_id,
      nama_ahliwaris,
      hub_ahliwaris,
	  tempat_lahir,
    }  = this.props.renewalDataAccident;
	
	const {
		province,
		district,
	} = this.props.renewalLabel;
	
		var {height, width} = Dimensions.get('window');
	  
    var ktpkitas = 'KTP';
  
    if(kewarganegaraan == 'wni'){
      ktpkitas = 'KTP';
    }else{
      ktpkitas = 'KITAS';
    }
	
	  return(
		   <View style={[styles.detailPribadiMainContainer]}>
				{this.genderlist(tittle)}
				<View style={[styles.top10]} />
				{this.namatext(nama)}	  	  
				<View style={[styles.top10]} />
				{this.tempatlahirtext(tempat_lahir)}
				<View style={[styles.top10]} />
				{this.alamattext(alamat)}
				<View style={[styles.top10]} />
				{this.provinsilist(province)}
				<View style={[styles.top10]} /> 
				{this.districtlist(district)}
				<View style={[styles.top10]} />
				{this.ktpkitastext(no_identitas, ktpkitas)}
				<View style={[styles.top10]} />
				{this.namaahliwaristext(nama_ahliwaris)}
				<View style={[styles.top10]} />
				{this.ahliwarislist(hub_ahliwaris)}
				<View style={{height: height*.05}}></View>
			</View>
	  );
  }

  componentDidMount(){
    this.props.getAhliWaris();
    }

  componentWillReceiveProps(NextProps){
    if (this.props.renewalDataAccident.province_id !== NextProps.renewalDataAccident.province_id){
      this.props.resetDistrictRenewalPA();
      this.props.resetDistrictName();
    }
  }


  


	render() {
		var {height, width} = Dimensions.get('window');
    const { renewalAccident, renewalDataAccident } = this.props;
    const postData = Object.assign({}, renewalAccident, { data_accident: renewalDataAccident}); 
    console.log(postData);
		return (
			<View style = {[styles.centerItemContent]}>
				{ !this.state.loading ? 
				this.dataTertanggung()
				:
					<LoadingPage />	
			} 
			{ !this.state.loading ? 
				<SubmitButton title='Renewal' onPress={() => this.doPostData(postData)} />  :
				null
				
				} 
			</View>

		);
	}
}

function mapStateToProps(state) {
  return {
  renewalAccident: state.renewalAccident,
    renewalDataAccident: state.renewalDataAccident,
	renewalLabel: state.renewalLabel,
    session:state.session,
	detailPolis:state.detailPolis,
    dataVoucher:state.dataVoucher,
    asuransiAccident: state.asuransiAccident,
    listAhliWaris: state.listAhliWaris,
    listProvinsi: state.listProvinsi,
    listDistrict:state.listDistrict,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RenewalPA);
