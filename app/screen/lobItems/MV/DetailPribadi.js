import React, { Component } from 'react';
import { Modal, Alert, BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import UploadImage from '../../../components/upload/UploadImage';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import Tooltip from '../../../components/tooltip/Tooltip';
import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainTextInputStyle from '../../../components/text_input/JagainTextInputStyle';
import JagainTextInputEditable from '../../../components/text_input/JagainTextInputEditable';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';
import WheelPicker from '../../../components/picker/WheelPicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';

class DetailPribadiMV extends Component{

  constructor(props) {
    super(props);
	this.state={
		stylePlatTengah: {
			borderBottomColor: 'black',
			borderBottomWidth: .5,
		},	
		stylePlatBelakang: {
			borderBottomColor: 'black',
			borderBottomWidth: .5,
		},	
	}
  }
  
   changeExtStatus(status_qq){
    var change_status_qq = 'Y';
    if(status_qq == 'N')
      change_status_qq = 'Y';
    else
      change_status_qq = 'N';
    this.props.setQQDK(change_status_qq);
  }

  genderlist(value){
    const title = 'Title';
    const lob = 'MV';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	<View style = {[styles.width40p]}>
      <FilterPicker title={title}
      
        value={value}
        options={options}
        type={type}
        iconName='md-arrow-dropdown'
      />
	  </View>
    );
  }
  

  
  namatext(nama_pemilik){
	  return(
	   <JagainTextInput
		  title = 'Nama Tertanggung'
		  autoCapitalize = 'words'
		  value={nama_pemilik}
          onChangeText={(nama_pemilik) => this.props.setNamaPemilikDK(appFunction.alphabetSpaceOnly(nama_pemilik))}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />  
	  );
  }
  
  qqcheckbox(status_qq_bool, status_qq){
	const tootltipText = "Maksud dari QQ adalah: dalam kapasitasnya sebagai wakil dari…";
	
	 return(
		<View style={styles.directionRow}>
                    <Text style={[styles.baseText]}>QQ</Text>
                    <CheckBox isChecked={ status_qq_bool } checkBoxColor='#ff4c4c' onClick={() => this.changeExtStatus(status_qq)} />
					<Tooltip tooltipText={tootltipText} />
                  </View>
	 );
  }
  
  
  renderQQInput(status_qq, nama_stnk){
    if( status_qq == 'Y' ){
      return(
	   <JagainTextInput
		  title = 'Nama Di STNK'
		  autoCapitalize = 'words'
		  value={nama_stnk} 
		  onChangeText={(nama_stnk) => this.props.setNamaSTNKDK(appFunction.alphabetSpaceOnly(nama_stnk))}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />  
        
      );
    }
  }
  
  alamattext(alamat_pemilik){
	  return(
	     <JagainTextInput
		  title = 'Alamat'
		  value={alamat_pemilik}
          onChangeText={(alamat_pemilik) => this.props.setAlamatPemilikDK(appFunction.dotalphaNumericOnly(alamat_pemilik))}                   
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  multiline = {true}
          numberOfLines = {4}
	  />
	  );
	  
  }
  
  noKTPtext(no_ktp){
	  
	  return(
	  <JagainTextInput
		  title = 'No KTP'
		  autoCapitalize = 'words'
			value={no_ktp}
			keyboardType='numeric'
			onChangeText={(no_ktp) => this.props.setKTPDK(appFunction.numericOnly(no_ktp))}            
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
		  maxLength={16}
	  />
	  );
  }
  
  birthdatepicker(tgl_lahir){
	  return(
	  <JagainDatePicker
		 title='Tanggal Lahir'
         date={tgl_lahir}
         minDate={appFunction.lastYear(71)}
         maxDate={appFunction.lastYear(7)}
		 iconSource={require('../../../assets/icons/component/birth-date.png')}
		 placeholder="Tanggal Lahir"
                    onDateChange={(tgl_lahir) => this.props.setTglLahirDK(tgl_lahir)}
        />
	  
	  );
	  
  }
  
   onFocus(type ,color = 'black', width = .5 ) {
   if(type == 'pt'){
		this.setState({stylePlatTengah:{
		  borderBottomColor: color,
			borderBottomWidth: width
		},

		})
	}else if(type == 'pb'){
		this.setState({stylePlatBelakang:{
		  borderBottomColor: color,
			borderBottomWidth: width
		},

		})
	}
  }

  
  nomorplatkendaraantext(mvState, otherDataKendaraan){
	  return(
	  <View>
	  <Text style={[styles.baseText]}>Nomor Plat Kendaraan Anda</Text>
                  <View style = {[styles.directionRow, styles.spaceBetween]} >

            	        <TextInput
                        value={mvState.plat}
            	          // Adding hint in Text Input using Place holder.
                        editable={false}
            	          onChangeText={() => null}
                        maxLength = {2}
            	          // Making the Under line Transparent.
            	          underlineColorAndroid='transparent'
            	          style={[styles.borderBotP5,  styles.width20p, styles.baseText, styles.padv0]}
            	        />

                    <TextInput
					onBlur={()=>this.onFocus('pt')}
					onFocus={()=>this.onFocus('pt', '#00e500' , 3)}
                      value={otherDataKendaraan.plat_tengah}
          	          // Adding hint in Text Input using Place holder.
          	          onChangeText={(value) => this.props.setPlatTengah(appFunction.numericOnly(value))}
                      maxLength = {4}
          	          // Making the Under line Transparent.
          	          underlineColorAndroid='transparent'
                      keyboardType='numeric'
          	          style={[styles.borderBotP5, this.state.stylePlatTengah,  styles.width30p, styles.baseText, styles.padv0]}
          	        />
          	        <TextInput
					onBlur={()=>this.onFocus('pb')}
					onFocus={()=>this.onFocus('pb', '#00e500' , 3)}
                      value={otherDataKendaraan.plat_belakang}
					  autoCapitalize='characters'
          	          // Adding hint in Text Input using Place holder.
          	          onChangeText={(value) => this.props.setPlatBelakang(appFunction.alphabetOnly(value).toUpperCase())}
                      maxLength = {3}
          	          // Making the Under line Transparent.
          	          underlineColorAndroid='transparent'
          	          style={[styles.borderBotP5, this.state.stylePlatBelakang,  styles.width30p, styles.baseText, styles.padv0]}
          	        />
                  </View>
				  
				  </View>
	  );
	  
  }
  
  warnakendaraantext(warna){
	  return(
	    <JagainTextInput
		  title = 'Warna Kendaraan Anda'
		  value={warna}
			autoCapitalize='words'
           onChangeText={(warna) => this.props.setWarnaDK(appFunction.alphabetSpaceOnly(warna))}               
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  
	  );
	  
  }
  
  norangkakendaraantext(no_rangka){
	  return(
	   <JagainTextInput
	   title = 'No. Rangka'
		 value={no_rangka}
		  autoCapitalize='characters'
		  // Adding hint in Text Input using Place holder.
		  onChangeText={(no_rangka) => this.props.setNoRangkaDK(appFunction.alphaNumericOnly(no_rangka))}
		  maxLength={17}    
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	  
	  );
  }
  
  nomesinkendaraantext(no_mesin){
	  return(
	   <JagainTextInput
	   title = 'No. Mesin'
		  value={no_mesin}
						  autoCapitalize='characters'
		  // Adding hint in Text Input using Place holder.
		  onChangeText={(no_mesin) => this.props.setNoMesinDK(appFunction.alphaNumericOnly(no_mesin))}
						  maxLength={17} 
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
	 
	  );
  }

  
  setType(value, type){
	if(type == 'ktp'){
          this.props.setPhotoKtpDK(value);
	} 
	if(type == 'stnk'){
          this.props.setPhotoStnkDK(value);
	} 
	if(type == 'depan'){
          this.props.setPhotoDepanDK(value);
	}  
	if(type == 'belakang'){
          this.props.setPhotoBelakangDK(value);
	}  
	if(type == 'kanan'){
          this.props.setPhotoKananDK(value);
	}  
	if(type == 'kiri'){
          this.props.setPhotoKiriDK(value);
	}  
  }
  
  fileUploaded(file, type, title){
	  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value, type)}  />    
	  );
  }

 
  
  doPostData(body, dataKendaraan, otherDataKendaraan){
    var errorMessage =  '';
    if(!dataKendaraan.tittle || dataKendaraan.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
	  //Alert.alert(body.data_kendaraan.no_plat);
    }else if(!dataKendaraan.nama_pemilik){
      errorMessage = 'Nama Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.alamat_pemilik){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.no_ktp){
      errorMessage = 'No KTP Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.no_mesin){
      errorMessage = 'No Mesin Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!otherDataKendaraan.plat_tengah){
      errorMessage = 'No Plat Tengah Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!otherDataKendaraan.plat_belakang){
      errorMessage = 'No Plat Belakang Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.no_rangka){
      errorMessage = 'No Rangka Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
	  
    }
    //if(!photo_belakang)
    //if(!photo_depan)
  //  if(!photo_kanan)
    //if(!photo_kiri)
    //if(!photo_ktp)
    //if(!photo_stnk)
    else if(dataKendaraan.status_qq == 'Y' && !dataKendaraan.nama_stnk){
      errorMessage = 'Nama STNK Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.tgl_lahir){
      errorMessage = 'Tanggal Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.warna){
      errorMessage = 'Warna Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_ktp){
      errorMessage = 'Photo KTP Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_stnk){
      errorMessage = 'Photo STNK Lembar Biru Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_depan){
      errorMessage = 'Photo Kendaraan Tampak Depan Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_belakang){
      errorMessage = 'Photo Kendaraan Tampak Belakan Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_kanan){
      errorMessage = 'Photo Kendaraan Tampak Kanan Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataKendaraan.photo_kiri){
      errorMessage = 'Photo Kendaraan Tampak Kiri Belum Diupload';
      appFunction.toastError(errorMessage);
    }else{
      this.props.setPostData(body);
	  this.props.setLabelNoPlatKendaraan(body.data_kendaraan.no_plat);
      //this.props.doPostDataMV(body);
      this.props.navigate('Pembayaran');
    }
  }
  
  dataTertanggung(mvState, otherDataKendaraan){
			
	const {
      alamat_pemilik,
      nama_pemilik,
      nama_stnk,
      no_ktp,
      no_mesin,
      no_plat,
      no_rangka,
      photo_belakang,
      photo_depan,
      photo_kanan,
      photo_kiri,
      photo_ktp,
      photo_stnk,
      status_qq,
      tgl_lahir,
      tittle,
      warna
    }  = this.props.dataKendaraan;	
			
		var status_qq_bool = false;
		if(status_qq == "Y")
		  status_qq_bool = true;
		else
		  status_qq_bool = false;
	 
	  return(
      <View style={[styles.detailPribadiMainContainer]}>
        <WheelPicker />
            {this.genderlist(tittle)}
            <View style={[styles.top10]} />
			{this.namatext(nama_pemilik)}
            <View style={[styles.top10]} />
			{this.qqcheckbox(status_qq_bool, status_qq)} 
			<View style={[styles.top10]} />  
            {this.renderQQInput(status_qq, nama_stnk)}
            <View style={[styles.top10]} />
			{this.alamattext(alamat_pemilik)}        
            <View style={[styles.top10]} />
			{this.noKTPtext(no_ktp)}      
            <View style={[styles.top10]} />
			{this.birthdatepicker(tgl_lahir)}      
            <View style={[styles.top10]} />
			{this.nomorplatkendaraantext(mvState, otherDataKendaraan)}      
            <View style={[styles.top10]} />
			{this.warnakendaraantext(warna)}     
            <View>
            <View style={[styles.top10]} />
			{this.norangkakendaraantext(no_rangka)}
            <View style={[styles.top10]} />
			{this.nomesinkendaraantext(no_mesin)}
            </View>
			<View style={[styles.top10]} />
			{this.fileUploaded(photo_ktp, 'ktp', 'KTP')}
			<View style={[styles.top10]} />
			{this.fileUploaded(photo_stnk, 'stnk', 'STNK Lembar Biru')}
            <View style={[styles.top10]} />
			{this.fileUploaded(photo_depan, 'depan', 'Tampak Depan')}
            <View style={[styles.top10]} />
			{this.fileUploaded(photo_belakang, 'belakang', 'Tampak Belakang')}
            <View style={[styles.top10]} />
			{this.fileUploaded(photo_kanan, 'kanan', 'Tampak Kanan')}
            <View style={[styles.top10]} />                        
			{this.fileUploaded(photo_kiri, 'kiri', 'Tampak Kiri')}
        </View>
	  );  
  } 
   componentDidMount(){

	
		this.props.resetPostData();
	
  }

	render() {
    var {height, width} = Dimensions.get('window');
    const {otherDataKendaraan, dataKendaraan, session, mvState,dataVoucher, asuransiMV} = this.props;
    const {plat_tengah, plat_belakang} = this.props.otherDataKendaraan;
    var platLengkap = null;
    var mvState2 = Object.assign(mvState, { coverage: mvState.coverage.toLowerCase() });;

    if (plat_tengah && plat_belakang) {
      platLengkap = mvState2.plat + ' ' + plat_tengah + ' ' + plat_belakang;
      //Alert.alert(platLengkap);
    }

    console.log(mvState2);

    const { perluasanMV } = this.props;
    const body = Object.assign({}, mvState2, perluasanMV);
    delete body.merek;
    delete body.seri_tipe;
    delete body.penggunaan;

    const dataPostKendaraan = Object.assign({}, dataKendaraan,
      { merek: mvState2.merek },
      { no_plat: platLengkap },
      { penggunaan: mvState2.penggunaan },
      { seri_tipe: mvState2.seri_tipe },
      { visitor_id: session.visitor_id },
      { kode_voucher: dataVoucher.kode_voucher },
    );


     const postData =  Object.assign({},{data_kendaraan:dataPostKendaraan}, body, {insurance_id:asuransiMV.insurance_id });
    //console.log(postData);
 
	  return (
		<View style = {[styles.centerItemContent]}>
			{this.dataTertanggung(mvState, otherDataKendaraan)}
            <View style={{height: height*.05}}></View>
            <SubmitButton title='Submit' onPress={() => this.doPostData(postData,dataKendaraan, otherDataKendaraan)} />
        </View>
	  );
  }
}

function mapStateToProps(state) {
  return {
    dataKendaraan: state.dataKendaraan,
    mvState: state.mvState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    perluasanMV:state.perluasanMV,
    asuransiMV: state.asuransiMV,
	otherDataKendaraan: state.otherDataKendaraan,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiMV);
