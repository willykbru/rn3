import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

//import { TextInputMask } from 'react-native-masked-text'
import PopoverTooltip from 'react-native-popover-tooltip';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import HeaderListAsuransiMV from './HeaderListAsuransi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class ListAsuransiMV extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      modalVisible:false,
      perluasanVisible:false,
    }
  }

  usetooltip(text){
    var {height, width} = Dimensions.get('window');
    return(
      <PopoverTooltip
        buttonComponent={
          <Icon name="ios-information-circle-outline" style={styles.colorRed} />
        }
        items={[
          {
            label: text,
            onPress: () => {}
          }
        ]}
        tooltipContainerStyle = {{ width: width*.8, }}
        // animationType='timing'
        // using the default timing animation
       />
     );
  }
  
  lihatBenefit(listAsuransi){
	  const {travelState} = this.props;
	  var benefit_url = '';
	  var dataBenefit = null;
	   benefit_url =  appFunction.webJagain + 'vehicle/mbenmv?is=' + listAsuransi.insurance_id + '&pc=' + listAsuransi.package_id + '&lg=id';
						  	  
	  dataBenefit = Object.assign({},{
		  insurance_name: listAsuransi.insurance_name,
		  insurance_id: listAsuransi.insurance_id,
		  package_name: listAsuransi.package_name,
		  benefit_url,
	  });
	  
	  this.props.setDataBenefit(dataBenefit);
	  this.props.navigate('Benefit');
  }


  doBeli(listAsuransi){
    this.props.setAsuransiMV(listAsuransi);
    this.toNextScreen();
  }

  toNextScreen(){
    if(this.props.status.isLogin == false){
      this.props.navigate('Login');
    }else{
      this.props.navigate('DetailBeli');
    }
  }

  openModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:true});
    }else if(typemodal=='perluasan'){
      this.setState({perluasanVisible:true});
    }
  }

  closeModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:false});
    }else if(typemodal=='perluasan'){
      const lob = 'MV';
      const body = Object.assign({}, this.props.mvState, this.props.perluasanMV);
      delete body.merek;
      delete body.seri_tipe;
      delete body.penggunaan;
      console.log('closemodal', body)

      this.props.getListAsuransi(body, lob);
      this.setState({perluasanVisible:false});
    }
  }

  validasiCakupan(listAsuransi, perluasanMV){
    const { perluasanVisible } = this.state;
    //const { tplStatus, padStatus, pllStatus, papStatus, eqvetStatus, tsfwdStatus, tsStatus, srccStatus, atpmStatus } = this.state;
    const premidasar = listAsuransi.premidasar;
    const premi_tpl = listAsuransi.premi_tpl;
    const premi_pll = listAsuransi.premi_pll;
    const premi_padriver = listAsuransi.premi_padriver;
    const premi_pap = listAsuransi.premi_pap;
    const premi_srcc = listAsuransi.premi_srcc;
    const premi_ts = listAsuransi.premi_ts;
    const premi_eqvet = listAsuransi.premi_eqvet;
    const premi_tsfwd = listAsuransi.premi_tsfwd;
    const premi_atpm = listAsuransi.premi_atpm;
    const tpl = perluasanMV.tpl;
    const pll = perluasanMV.pll;
    const padriver = perluasanMV.padriver;
    const pap = perluasanMV.pap;
    const srcc = perluasanMV.srcc;
    const ts = perluasanMV.ts;
    const eqvet = perluasanMV.eqvet;
    const tsfwd = perluasanMV.tsfwd;
    const atpm = perluasanMV.atpm;
    var listPerluasan = [];
    var tipePerluasan = [];
    var valuePerluasan = [];

    if(premidasar == 0 || premidasar){
      valuePerluasan.push(premidasar);
      tipePerluasan.push('Premi Dasar');
    }
    if(tpl && !perluasanVisible){
      valuePerluasan.push(premi_tpl);
      tipePerluasan.push('TPL');
    }
    if(pll && !perluasanVisible){
      valuePerluasan.push(premi_pll);
      tipePerluasan.push('PLL');
    }
    if(padriver && !perluasanVisible){
      valuePerluasan.push(premi_padriver);
      tipePerluasan.push('PA Driver');
    }
    if(pap && !perluasanVisible){
      valuePerluasan.push(premi_pap);
      tipePerluasan.push('PA Passanger');
    }
    if(srcc && !perluasanVisible){
      valuePerluasan.push(premi_srcc);
      tipePerluasan.push('SRCC');
    }
    if(ts && !perluasanVisible){
      valuePerluasan.push(premi_ts);
      tipePerluasan.push('TS');
    }
    if(eqvet && !perluasanVisible){
      valuePerluasan.push(premi_eqvet);
      tipePerluasan.push('EQVET');
    }
    if(tsfwd && !perluasanVisible){
      valuePerluasan.push(premi_tsfwd);
      tipePerluasan.push('TSFWD');
    }
    if(atpm && !perluasanVisible){
      valuePerluasan.push(premi_atpm);
      tipePerluasan.push('ATPM');
    }
    for(let i = 0; i < tipePerluasan.length; i++){
      listPerluasan.push(
        <View key={i}>
          <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
            <Text style={[styles.font12, styles.baseText]}>
                {tipePerluasan[i]}
            </Text>
            <Text style={[styles.font12, styles.baseText]}>
              Rp. {valuePerluasan[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
            </Text>
          </View>
         </View>
       );
    }
    return listPerluasan;
  }

  renderListAsuransi(mvState, listAsuransi, perluasanMV){
    var arrListAsuransi = [];
    var listBengkel = [];
	//var discountView = [];
	
    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
     }
	 
	const discsrc = {
      '5'  : require('../../../assets/icons/discount/disc-5.png'),
      '10' : require('../../../assets/icons/discount/disc-10.png'),
      '15' : require('../../../assets/icons/discount/disc-15.png'),
      '20' : require('../../../assets/icons/discount/disc-20.png'),
	  '25' : require('../../../assets/icons/discount/disc-25.png'),
    }
	
     var asuransi = [];
     var idAsuransi = [];
     var totalPremi = [];
     var jmlBengkel = [];
     var discount = [];
     var showTotalPremi= [];
     var discpremi = [];
     var netpremi =[];
     var premiDasar =[];
	 
	 var {height, width} = Dimensions.get('window');

     if(listAsuransi.length > 0){
      // console.log('test');
       for (var i = 0; i < listAsuransi.length; i++) {
         asuransi.push(listAsuransi[i].insurance_name);
         idAsuransi.push(listAsuransi[i].insurance_id);
         totalPremi.push(listAsuransi[i].totalpremi);
         jmlBengkel.push(listAsuransi[i].jumlah_bengkel);
         discount.push(listAsuransi[i].disc_insurance);
         discpremi.push(listAsuransi[i].disc_premi);
         netpremi.push(listAsuransi[i].net_premi);
         premiDasar.push(listAsuransi[i].premidasar);
       }

      // console.log(asuransi);

       for(let i = 0; i < asuransi.length; i++){
         if(premiDasar[i] > 0){
          // console.log(premiDasar);
           if(discount[i] > 0){
		   
             showTotalPremi.push(
               <View key = {i}>
                 <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                  <Text style={[styles.font14, styles.bold, styles.baseText]}>
                    {"\n"}Total:{"\n"}{"\n"}
                  </Text>
                  <View style={styles.directionColumn, styles.right5}>
                    <Text style={[styles.font12, styles.strike, styles.baseText]}>
                      {"\n"}Rp. {totalPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
                    </Text>
                    <Text style={[styles.font14, styles.bold, styles.baseText]}>
                      Rp. {netpremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}{"\n"}{"\n"}
                    </Text>
                  </View>
                </View>
              </View>
            );
          }else {
            showTotalPremi.push(
              <View key = {i}>
                 <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                  <Text style={[styles.font14, styles.bold, styles.baseText]}>
                    {"\n"}Total:{"\n"}{"\n"}
                  </Text>
                  <Text style={[styles.font14, styles.bold, styles.baseText]}>
                    {"\n"}{"\n"}Rp. {totalPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}{"\n"}{"\n"}
                  </Text>
                  <Text style={[styles.baseText]}>{"\n"}{"\n"}</Text>
                </View>
              </View>
            );
          }
          if(mvState.vehicle_type == 'Mobil'){
            listBengkel.push(
              <View style={styles.centerItemContent} key = {i}>
			  <Text> </Text>
				 <TouchableHighlight underlayColor='blue'
					  style={[{backgroundColor: '#66b2ff'}, styles.elev2, styles.radius2]}
					  onPress={() => {this.props.setDataSearch(listAsuransi[i]);this.props.navigate('BengkelRekanan')}}>
					  <Text style={[{fontSize:8, padding: 5}, styles.colorWhite, styles.bold]}>Lihat Bengkel</Text>
					</TouchableHighlight> 
                <Text style={[styles.font10, styles.bold, styles.italic, styles.baseText]}>
                  {"\n"}{jmlBengkel[i]}
                </Text>
				   <Text style={[styles.baseText10]}>
					  Bengkel Rekanan{"\n"}{"\n"}  
				   </Text>
              </View>
            );
          }

          arrListAsuransi.push(
            <View style={[styles.centerItemContent, styles.top10]} key = {i}>
			{discount[i] ? 
			<View style={[styles.directionRow, styles.flexEndSelf, {position:'absolute', top:-15, zIndex: 1}]}>
			  <View style={[styles.directionRow]}> 
				<View>
				 <Image style={[styles.width70, styles.height70]} source={discsrc[discount[i].toString()]} />
				</View>
			  </View>
			</View>
			: null}
	
              <View style={[styles.bgWhite, styles.radius5, styles.bottom10, styles.top10, styles.pad5, { zIndex: 0}]}> 
                <View style={[styles.directionRow, styles.left5]}>
                  <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                    <TouchableHighlight>
                      <View style={styles.centerItemContent}>
                         <View style={[styles.centerItemContent, styles.height50]}>
                             <Image source={asimgsrc[asuransi[i]]} style={[styles.height25, styles.width100, styles.resizeContain]}/>
                          </View>
                          {listBengkel[i]}
                        </View>
                      </TouchableHighlight>
                      <View style={styles.bottom10}></View>
                    </View>
                    <View style={styles.width55p}>
                      <View style={styles.directionColumn}>
                        <View style={styles.directionColumn}>
                          <Text style={[styles.font12, styles.bold, styles.baseText]}>
                            {asuransi[i]}{"\n"}{"\n"}
                          </Text>
                          <View style={[styles.borderTransparent, styles.radius2, styles.border1, styles.radius2]}>
                            <Text style={[styles.font12, styles.bold, styles.underline, styles.baseText]}>
                              Cakupan Perluasan:
                            </Text>
                            {this.validasiCakupan(listAsuransi[i], perluasanMV)}
                          </View>
                        </View>
                        {showTotalPremi[i]}
                        <View style={[styles.directionRow, styles.bottom10]}>
                           <View>
						<Button underlayColor='blue'
                            style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                            onPress={() => this.lihatBenefit(listAsuransi[i])}>
                            <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Benefit</Text>
                          </Button>
						</View>
						<View>
                            <Button underlayColor='blue'
                              style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                              onPress={() => this.doBeli(listAsuransi[i])}>
                              <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Beli</Text>
                            </Button>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            )
          }
        }
      }else{
        arrListAsuransi.push(
          <View style={[styles.centerItemContent]} key = 'listasuransi'>
            <View style={[styles.bgWhite, styles.bottom5, styles.radius5, styles.top10, styles.width90p, styles.pad10]}>
              <View style={styles.centerItemContent}>
                <Text style={[styles.baseText]}>Tidak Ada Asuransi Yang Sesuai Dengan Data Yang Anda Input.</Text>
              </View>
            </View>
          </View>
        );
      }
      return arrListAsuransi;
    }

    renderButton = (text, onPress) => (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.modalButton}>
          <Text style ={[styles.bold, styles.colorWhite, styles.baseText]}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    );

   renderModalContent = () => (
     <View style={styles.modalContent}>
       <Text style={[styles.baseText]}>Hello!</Text>
       {this.renderButton('Close', () => this.closeModal('benefit'))}
     </View>
   );

   renderPerluasan= () => (
     <View style={styles.modalContent}>
       <HeaderListAsuransiMV />
       {this.renderButton('Simpan Perluasan', () => this.closeModal('perluasan','save'))}
     </View>
   );

   componentDidMount() {
		this.props.resetDataSearch()
   }

  render(){

    var {height, width} = Dimensions.get('window');
    const {mvState, listAsuransi, perluasanMV, itemsIsLoading, loadingState} = this.props;
    //console.log('render',listAsuransi);

    return(
	
      <ScrollView>
        { 	loadingState.loadingListAsuransiMV ?
			<LoadingPage /> :
			this.renderListAsuransi(mvState, listAsuransi, perluasanMV)
      }
      </ScrollView>
	 
    );
  }
}

function mapStateToProps(state) {
  return {
    listAsuransi: state.listAsuransi,
    mvState: state.mvState,
    status: state.status,
    perluasanMV:state.perluasanMV,
    loadingState:state.loadingState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAsuransiMV);
