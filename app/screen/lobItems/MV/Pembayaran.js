import React, { Component } from 'react';
import { BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Modal from 'react-native-modal';

import md5 from "react-native-md5";
import * as appFunction from '../../../function'
import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';

import CheckBox from 'react-native-check-box';
import styles from '../../../assets/styles';


class PembayaranMV extends Component{

constructor(props) {

    super(props);

    this.state = {
      agreement:false,
    }

  }



    disclaimertext(dataMV, agreement){
      return(
        <View style={[styles.centerItemContent, styles.top5, styles.width90p]}>
          <View style={[styles.centerItemContent, styles.bottom10]}>
            <Text style={[styles.font14, styles.baseText]}>
              Terima kasih telah mengajukan permohonan polis asuransi.
              Kami akan otomatis mengirimkan email konfirmasi pembelian
              setelah Anda menyelesaikan pembayaran. Anda mungkin
              akan dikenakan biaya tambahan jika transfer dilakukan
              dari rekening selain yang tertera pada kwitansi pembayaran.
            </Text>
          </View>
          <View style={[styles.bgBlackOp6, styles.centerItemContent, styles.radius5, styles.elev5]}>
            <View style={[styles.centerItem, styles.left5, styles.right5, styles.top10, styles.bottom10]}>
              <View style={[styles.directionRow,{alignItems: 'flex-end'}]}>
                <Text style={[styles.colorWhite, styles.baseText]}>Jumlah </Text>
                <Text style={[styles.colorWhite, styles.font16, styles.baseText]}> Rp. {dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
              </View>
              <Text style={[styles.colorWhite, styles.font14, styles.baseText]}>
                Untuk pembayaran dengan metode transfer. Anda tidak
                perlu melakukan konfirmasi pembayaran. Sistem Jagain
                akan otomatis mendapatkan pemberitahuan ketika transfer berhasil.
              </Text>
            </View>
			<View style={[styles.bottom5, styles.left5, styles.directionRow, styles.width100p]}>
				<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText]}>
					* 
				</Text>
				<TouchableHighlight underlayColor='blue' onPress={()=> {this.props.navigate('Agreement')} }>
					<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText, styles.underline]}>
					syarat dan ketentuan
					</Text>
				</TouchableHighlight>
			</View>
            <View style={[styles.directionRow, styles.width90p, styles.padTop5]}>
              <CheckBox isChecked={ agreement } checkBoxColor='white' onClick={() => this.changeExtStatus()} />
              <Text style={[styles.colorWhite, styles.font14, styles.baseText, styles.bold]}>
                Saya setuju syarat dan ketentuan dengan yang berlaku di Jagain
              </Text>			  
            </View>
          </View>
        </View>
      );
    }

    doBayar(agreement){
      if(!agreement)
        appFunction.toastError('Syarat dan Ketentuan Belum Disetujui');
      else
        this.props.navigate('PaymentGateway');
    }

    addToCart(){
      this.props.doPostDataMV(this.props.postDataValue);
    }

    changeExtStatus(){
      const {
        agreement,
      } = this.state;

      var agreeStat = agreement;
        agreeStat = !agreement;

        this.setState(() => {
          return {
            agreement: agreeStat,
          };
        });

    }

    _renderButton = (text, onPress) => (
         <TouchableOpacity onPress={onPress}>
           <View style={styles.modalButton}>
             <Text style={[styles.baseText]}>{text}</Text>
           </View>
         </TouchableOpacity>
       );

    _renderModalContent = () => (
        <View style={styles.modalContent}>
          <Text style={[styles.baseText]}>Hello!</Text>
          {this._renderButton('Close', () => this.closeModal())}
        </View>
      );

	  detailPremi(dataMV){
		var perluasanpremi = [];
		 var netpremi = [];
		 var discvoucher=[];


		 if(dataMV.disc_insurance !== 0){
		   if(dataMV.description == 'ok'){
			 if(dataMV.voucher_persen !== 0){
			   netpremi.push(
				 <View key='netpremi' >
				   <View style={[styles.directionRow, styles.bottom]}>
				   <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.totalpremi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>	
				   </View>
				   <View style={styles.directionRow}>
					 <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
					   <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
					   <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.voucher_persen} %</Text></View>
					 </View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
							<Text style={[styles.baseText]}>:  -Rp. </Text>
							<Text style={[styles.italic, styles.baseText]}>{(dataMV.totalpremi*dataMV.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
				   </View>
				   <View style={styles.directionRow}>
					   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
						 <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
						 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.disc_insurance} %</Text></View>
					   </View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
							<Text style={[styles.baseText]}>:  -Rp. </Text>
							<Text style={[styles.italic, styles.baseText]}>{dataMV.disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
				   </View>
				   <View style={styles.borderbottom1}></View>
				   <View style={styles.directionRow}>
					 <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-(dataMV.totalpremi*dataMV.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
				   </View>
				 </View>
			   );
			 }else{
			   netpremi.push(
				 <View key='netpremi' >
				   <View style={[styles.directionRow, styles.bottom]}>
				   <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.totalpremi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				   </View>
				   <View style={styles.directionRow}>
					 <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataMV.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
				   </View>
				   <View style={styles.directionRow}>
					   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
						 <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
						 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.disc_insurance} %</Text></View>
					   </View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
							<Text style={[styles.baseText]}>:  -Rp. </Text>
							<Text style={[styles.italic, styles.baseText]}>{dataMV.disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
				   </View>
				   <View style={styles.borderbottom1}></View>
				   <View style={styles.directionRow}>
				   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-dataMV.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
				   </View>
				 </View>
			   );
			 }
		   }else{
			 netpremi.push(
			   <View key='netpremi' >
				 <View style={[styles.directionRow, styles.bottom]}>
				 <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.totalpremi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				 </View>
				 </View>
				 <View style={styles.directionRow}>
				   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
					 <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
					 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.disc_insurance} %</Text></View>
				   </View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataMV.disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
				 </View>
				 <View style={styles.borderbottom1}></View>
				   <View style={styles.directionRow}>
				   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
				 </View>
			   </View>
			 );
		 }

		 }else{
		   if(dataMV.description == 'ok'){
			 if(dataMV.voucher_persen !== 0){
				 netpremi.push(
				   <View key='netpremi' >
					 <View style={[styles.directionRow, styles.bottom]}>
					 <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
					  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					  </View>
					 </View>
					 <View style={styles.directionRow}>
					   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
						 <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
						 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev2]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.voucher_persen} %</Text></View>
					   </View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{(dataMV.net_premi*dataMV.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
					 </View>
					 <View style={styles.borderbottom1}></View>
					   <View style={styles.directionRow}>
					   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-(dataMV.net_premi*dataMV.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
					 </View>
				   </View>
			   );
			 }else{
			   netpremi.push(
				 <View key='netpremi' >
				   <View style={[styles.directionRow, styles.bottom]}>
				   <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
					<View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
				   </View>
				   <View style={styles.directionRow}>
					 <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataMV.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
				   </View>
				   <View style={styles.borderbottom1}></View>
				   <View style={styles.directionRow}>
				   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					  <Text style={[styles.baseText]}>:  +Rp. </Text>
					  <Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-dataMV.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
				   </View>
				 </View>
			   );
			 }
		   }else{
				 netpremi.push(
					 <View key='totpremi' style={styles.directionRow}>
					 <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  +Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
					 </View>
				 );
			   }
		 }

		 if(dataMV.tpl == true){
		   perluasanpremi.push(
			 <View key='tpl' style={[styles.directionRow]}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>TPL</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_tpl.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }
		 if(dataMV.pll == true){
		   perluasanpremi.push(
			 <View key='pll' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>PLL</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_pll.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }
		 if(dataMV.pap == true){
		   perluasanpremi.push(
			 <View key='pap' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>PA Passanger</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_pap.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>             
			 </View>
		   );
		 }
		 if(dataMV.padriver == true){
		   perluasanpremi.push(
			 <View key='pad' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>PA Driver</Text></View>  
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_padriver.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }
		  if(dataMV.srcc == true){
		   perluasanpremi.push(
			 <View key='srcc' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>SRCC</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_srcc.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			   
			 </View>
		   );
		 }
		 if(dataMV.ts == true){
		   perluasanpremi.push(
			 <View key='ts' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>TS</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
			   <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_ts.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }
		 if(dataMV.eqvet == true){
		   perluasanpremi.push(
			 <View key='eqvet' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>EQVET</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
			   <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_eqvet.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }
		 if(dataMV.tsfwd == true){
		   perluasanpremi.push(
			 <View key='tsfwd' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>TSFWD</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
			   <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_tsfwd.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }
		 if(dataMV.atpm == true){
		   perluasanpremi.push(
			 <View key='atpm' style={styles.directionRow}>
			   <View style={styles.width50p}><Text style={[styles.baseText]}>ATPM</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  +Rp. </Text>
			   <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_atpm.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
			 </View>
		   );
		 }

		 return(
		   <View style={styles.centerItemContent}>
		 <View style={styles.borderDetailBeli}>
		   <View style={styles.directionRow}>
		   <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
		   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
				<Text style={[styles.baseText]}>:  Rp. </Text>
		   <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premidasar.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		   </View>
		   </View>
		   {perluasanpremi}
		   <View style={styles.borderbottom1}></View>
		   {netpremi}
		 </View>
	   </View>
	   );
	  }

	componentWillReceiveProps(NextProps){
			const { dataTransaksi } = NextProps;
			const { olddataTransaksi } = this.props;
			if (dataTransaksi != olddataTransaksi) {
				if (dataTransaksi.link_pembayaran) {
					this.props.navigate('Main');
					this.props.setScreen('MyCart');
				}
			}
		}

	render() {
    var {lobTitle} = 'Asuransi Kendaraan';
    const {mvState, perluasanMV, asuransiMV, dataVoucher,session, dataTransaksi, dataKendaraan, labelState, loadingState} = this.props;
    const dataMV = Object.assign({}, mvState, perluasanMV, asuransiMV, dataVoucher,session, dataTransaksi);

    var {height, width} = Dimensions.get('window');
    const {agreement} = this.state;
    //console.log(dataTransaksi.link_pembayaran);
    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA'    : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	
	if(loadingState.loadingDataTransaksi){
		return(
			<View style={[styles.centerItemContent]}>
				<LoadingPage />
			</View>
		)
	}

	


    return(
      <View style={styles.centerItem}>
          <View style={styles.centerItem}>
            <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, {marginTop:height*.03,}, styles.elev5]}>
              <View style={styles.left15}><Text style={[styles.baseText]}>Data Pemesan</Text></View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5, styles.borderTop1, styles.borderColorTopGrey]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Nama</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.visitor_name}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Telpon</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.visitor_phone}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Email</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.username}</Text></View>
              </View>
            </View>
          </View>

          <View style={styles.centerItem}>
            <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, {marginTop:height*.03,}, styles.elev5]}>
           
              <View style={styles.centerItemContent}>
              <View style={[styles.height70, styles.directionRow, styles.radius2, styles.bottom20, styles.centerItemContent]}>
                <Image source={asimgsrc[dataMV.insurance_name]} style={[styles.height25, styles.width100]}/>
                <View style={[styles.height70, styles.left10, styles.centerContent]}><Text style={[styles.baseText]}>{dataMV.insurance_name}</Text></View>
              </View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={[styles.width65p, styles.directionRow]}><Text style={[styles.baseText]}>{dataKendaraan.nama_pemilik}</Text>{dataKendaraan.status_qq == 'Y' ? <Text> QQ {dataKendaraan.nama_stnk}</Text> : null}</View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Alamat Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataKendaraan.alamat_pemilik}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Jenis Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.vehicle_type}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Merek Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.merek}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Seri Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.seri_tipe}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
              <View style={styles.width30p}><Text style={[styles.baseText]}>Tahun Produksi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
              <View style={styles.width65p}><Text style={[styles.baseText]}>{dataMV.build_year}</Text></View>
            </View>
            {this.detailPremi(dataMV)}
            <View style={{height: height*.03}}></View>
           </View>
         </View>
         <View style={{height: height*.03}}></View>
         <SubmitButton title='Add To Cart' onPress={() => this.addToCart()} />
        </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    dataTransaksi: state.dataTransaksi,
    mvState:state.mvState,
    perluasanMV:state.perluasanMV,
    asuransiMV:state.asuransiMV,
    dataVoucher:state.dataVoucher,
    session:state.session,
    dataKendaraan:state.dataKendaraan,
	labelState:state.labelState,
	loadingState: state.loadingState,
	postDataValue: state.postDataValue,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PembayaranMV);
