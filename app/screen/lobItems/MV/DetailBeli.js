import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, Button, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

//import { TextInputMask } from 'react-native-masked-text'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';


class DetailBeliMV extends React.Component {
  constructor(props){
     super(props);
   }


	toDetailPribadi(){
	if(this.props.dataVoucher.description !== 'ok'){ 
		this.props.resetDataVoucher();
		this.props.setKodeVoucher('');
	  }
	  this.props.navigate('DetailPribadi');
	}


       detailPremi(dataMV){
         var perluasanpremi = [];
         var netpremi = [];
         var discvoucher=[];


         if(dataMV.disc_insurance !== 0){
           if(dataMV.description == 'ok'){
             if(dataMV.voucher_persen !== 0){
               netpremi.push(
                 <View key='netpremi' >
                   <View style={[styles.directionRow, styles.bottom]}>
                   <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.totalpremi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>	
                   </View>
                   <View style={styles.directionRow}>
                     <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
                       <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                       <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.voucher_persen} %</Text></View>
                     </View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
							<Text style={[styles.baseText]}>:  -Rp. </Text>
							<Text style={[styles.italic, styles.baseText]}>{(dataMV.totalpremi*dataMV.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
                   </View>
                   <View style={styles.directionRow}>
					   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
						 <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
						 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.disc_insurance} %</Text></View>
					   </View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
							<Text style={[styles.baseText]}>:  -Rp. </Text>
							<Text style={[styles.italic, styles.baseText]}>{dataMV.disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
                   </View>
                   <View style={styles.borderbottom1}></View>
                   <View style={styles.directionRow}>
                     <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
                        <Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-(dataMV.totalpremi*dataMV.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
                   </View>
                 </View>
               );
             }else{
               netpremi.push(
                 <View key='netpremi' >
                   <View style={[styles.directionRow, styles.bottom]}>
                   <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				    <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.totalpremi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
                    </View>
				   </View>
                   <View style={styles.directionRow}>
                     <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
                        <Text style={[styles.italic, styles.baseText]}>{dataMV.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
                   </View>
                   <View style={styles.directionRow}>
					   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
						 <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
						 <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.disc_insurance} %</Text></View>
					   </View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
							<Text style={[styles.baseText]}>:  -Rp. </Text>
							<Text style={[styles.italic, styles.baseText]}>{dataMV.disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
                   </View>
                   <View style={styles.borderbottom1}></View>
                   <View style={styles.directionRow}>
                   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-dataMV.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
                   </View>
                 </View>
               );
             }
           }else{
             netpremi.push(
               <View key='netpremi' >
                 <View style={[styles.directionRow, styles.bottom]}>
                 <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.totalpremi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				 </View>
                 </View>
                 <View style={styles.directionRow}>
				   <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
                     <Text style={[styles.italic, styles.baseText]}>Discount Premi</Text>
                     <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.disc_insurance} %</Text></View>
                   </View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
						<Text style={[styles.italic, styles.baseText]}>{dataMV.disc_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
                 </View>
                 <View style={styles.borderbottom1}></View>
                   <View style={styles.directionRow}>
                   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
                        <Text style={[styles.bold, styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
                 </View>
               </View>
             );
         }

         }else{
           if(dataMV.description == 'ok'){
             if(dataMV.voucher_persen !== 0){
                 netpremi.push(
                   <View key='netpremi' >
                     <View style={[styles.directionRow, styles.bottom]}>
                     <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
					  <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					  </View>
                     </View>
                     <View style={styles.directionRow}>
                       <View style={[styles.directionRow, styles.width50p, styles.spaceBetween, styles.flex2]}>
                         <Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text>
                         <View style={[styles.left10, styles.radius20, styles.bgRed, styles.elev5]}><Text style={[styles.colorWhite, styles.baseText]}>{dataMV.voucher_persen} %</Text></View>
                       </View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
                        <Text style={[styles.italic, styles.baseText]}>{(dataMV.net_premi*dataMV.voucher_persen/100).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
                     </View>
                     <View style={styles.borderbottom1}></View>
                       <View style={styles.directionRow}>
                       <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
					   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
                        <Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-(dataMV.net_premi*dataMV.voucher_persen/100)).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					   </View>
                     </View>
                   </View>
               );
             }else{
               netpremi.push(
                 <View key='netpremi' >
                   <View style={[styles.directionRow, styles.bottom]}>
                   <View style={styles.width50p}><Text style={[styles.baseText]}>Total Premi</Text></View>
				    <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  Rp. </Text>
						<Text style={[styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					</View>
                   </View>
                   <View style={styles.directionRow}>
                     <View style={styles.width50p}><Text style={[styles.italic, styles.baseText]}>Discount Voucher</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  -Rp. </Text>
                        <Text style={[styles.italic, styles.baseText]}>{dataMV.voucher_amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
                   </View>
                   <View style={styles.borderbottom1}></View>
                   <View style={styles.directionRow}>
                   <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
				   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
					  <Text style={[styles.baseText]}>:  +Rp. </Text>
                      <Text style={[styles.bold, styles.baseText]}>{(dataMV.net_premi-dataMV.voucher_amount).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
				   </View>
                   </View>
                 </View>
               );
             }
           }else{
                 netpremi.push(
                     <View key='totpremi' style={styles.directionRow}>
                     <View style={styles.width50p}><Text style={[styles.bold, styles.baseText]}>Harga</Text></View>
					 <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
						<Text style={[styles.baseText]}>:  +Rp. </Text>
						<Text style={[styles.bold, styles.baseText]}>{dataMV.net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
					 </View>
                     </View>
                 );
               }
         }

         if(dataMV.tpl == true){
           perluasanpremi.push(
             <View key='tpl' style={[styles.directionRow]}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>TPL</Text></View>
               <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_tpl.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }
         if(dataMV.pll == true){
           perluasanpremi.push(
             <View key='pll' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>PLL</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_pll.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }
         if(dataMV.pap == true){
           perluasanpremi.push(
             <View key='pap' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>PA Passanger</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_pap.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>             
             </View>
           );
         }
         if(dataMV.padriver == true){
           perluasanpremi.push(
             <View key='pad' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>PA Driver</Text></View>  
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_padriver.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }
          if(dataMV.srcc == true){
           perluasanpremi.push(
             <View key='srcc' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>SRCC</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
				<Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_srcc.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
               
             </View>
           );
         }
         if(dataMV.ts == true){
           perluasanpremi.push(
             <View key='ts' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>TS</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
               <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_ts.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }
         if(dataMV.eqvet == true){
           perluasanpremi.push(
             <View key='eqvet' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>EQVET</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
               <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_eqvet.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }
         if(dataMV.tsfwd == true){
           perluasanpremi.push(
             <View key='tsfwd' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>TSFWD</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
               <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_tsfwd.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }
         if(dataMV.atpm == true){
           perluasanpremi.push(
             <View key='atpm' style={styles.directionRow}>
               <View style={styles.width50p}><Text style={[styles.baseText]}>ATPM</Text></View>
			   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  +Rp. </Text>
               <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premi_atpm.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
			   </View>
             </View>
           );
         }

         return(
           <View style={styles.centerItemContent}>
         <View style={styles.borderDetailBeli}>
           <View style={styles.directionRow}>
           <View style={styles.width50p}><Text style={[styles.baseText]}>Premi Dasar</Text></View>
		   <View style={[styles.directionRow, styles.spaceBetween, styles.flex2]}>
			    <Text style={[styles.baseText]}>:  Rp. </Text>
           <Text style={[styles.baseText, styles.alignRight]}>{dataMV.premidasar.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text>
		   </View>
           </View>
           {perluasanpremi}
           <View style={styles.borderbottom1}></View>
           {netpremi}
         </View>
       </View>
       );
       }

 componentDidMount() {
			const {package_id, package_name, insurance_name, net_premi} = this.props.asuransiMV;
			var impressions = {package_id, package_name, lob: 'Asuransi Kendaraan', insurance_name, price:net_premi};			
			appFunction.setImpressionGA(impressions);
       }
	   
  render(){

    const {mvState, perluasanMV, asuransiMV, dataVoucher} = this.props
    const dataMV = Object.assign({}, mvState, perluasanMV, asuransiMV, dataVoucher);
	const lob = Object.assign({}, {lob : 'MV'}); 
	const dataCheckVoucherMV = Object.assign({},dataVoucher, asuransiMV, lob);


      var {height, width} = Dimensions.get('window');

    var lobTitle = 'Detail Asuransi';

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	 
	 var editableKodeVoucher = true;
	  
	  if(dataCheckVoucherMV.description == 'ok'){
		  editableKodeVoucher = false;
	  }

    console.log(mvState);

    return(

<View style={[styles.centerItemContent,{paddingLeft: 10}]}>
<View style={styles.centerItem}>
  <View style={[styles.width88p, styles.bgWhite, styles.border1, styles.bottom10, styles.left4, styles.radius3, styles.elev5, styles.pad5, {borderColor: '#ffe5e5', marginTop:height*.02, paddingTop:0}]}>
    <View style={styles.centerItemContent}>
      <View style={[styles.centerItemContent, styles.height70, styles.directionRow, styles.radius2, styles.bottom20]}>
        <Image source={asimgsrc[dataMV.insurance_name]} style={[appFunction.sizeLogoAsuransi(1.5)]}/>
      </View>
    </View>
    <View style={[styles.directionRow, styles.bottom]}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Jenis Kendaraan</Text></View>
      <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
      <View style={styles.width45p}><Text style={[styles.baseText]}>{dataMV.vehicle_type}</Text></View>
    </View>
    <View style={[styles.directionRow, styles.bottom]}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Tahun</Text></View>
      <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
      <View style={styles.width45p}><Text style={[styles.baseText]}>{dataMV.build_year}</Text></View>
    </View>
    <View style={[styles.directionRow, styles.bottom]}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Kendaraan</Text></View>
      <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
      <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {dataMV.tsi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}</Text></View>
    </View>
    <View style={[styles.directionRow, styles.bottom]}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Penggunaan</Text></View>
      <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
      <View style={styles.width45p}><Text style={[styles.baseText]}>{dataMV.penggunaan}</Text></View>
    </View>
    <View style={[styles.directionRow, styles.bottom]}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Cakupan</Text></View>
      <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
      <View style={styles.width45p}><Text style={[styles.baseText]}>{dataMV.coverage}</Text></View>
    </View>
    <View style={[styles.directionRow, styles.bottom]}>
      <View style={styles.width50p}><Text style={[styles.baseText]}>Provide By</Text></View>
      <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
      <View style={styles.width45p}><Text style={[styles.baseText]}>{dataMV.insurance_name}</Text></View>
    </View>
	
	  <View style={[styles.directionRow, styles.bottom]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Kode Voucher</Text></View>
               <Text style={[styles.baseText]}>:  </Text>
             <TextInput
               style={[styles.kodeVoucherTInput, styles.baseText]}
               value={dataVoucher.kode_voucher}
               onChangeText={(kode_voucher) => this.props.setKodeVoucher(appFunction.alphaNumericOnly(kode_voucher))}
				onEndEditing={() => this.props.checkVoucher(dataCheckVoucherMV, 'checkVoucher')}
				maxLength={20}
               underlineColorAndroid='transparent'
			   placeholder='Kode Voucher'
			   editable={editableKodeVoucher}
             />
           <TouchableHighlight underlayColor='white'>
              { dataVoucher.kode_voucher ? dataMV.description == 'ok'? <Icon name='checkmark-circle' style={{color:'green'}} /> : <Icon name='checkmark-circle' style={{color:'red'}} /> : <Icon name='checkmark-circle'/>}
            </TouchableHighlight>
           </View>
            <View style={[styles.directionRow, styles.bottom5]}>
              <View style={styles.width50p}>
			  {dataCheckVoucherMV.description == 'ok'?
			  <TouchableHighlight onPress={()=>this.props.resetKodeVoucher()} style={[{backgroundColor: '#00FF00', width: 100, height: 25}, styles.radius5, styles.elev3, styles.centerItemContent]}>
				<Text style={[styles.baseText]}>
					Edit Voucher
				</Text>
			  </TouchableHighlight>
			  :
			  null
			  }
			  </View>
                {dataMV.description == 'ok'? <Text style={[{color:'green'}, styles.font12, styles.italic, styles.baseText]}>Status Voucher Valid</Text>: <Text style={[styles.colorRed, styles.font12, styles.italic, styles.baseText]}>{dataMV.description}</Text>}
            </View>
			<View style={{height: height*.03}}></View>
			{this.detailPremi(dataMV)}
			<View style={{height: height*.03}}></View>
   </View>
</View>

     <SubmitButton title='Lanjut' onPress={() => this.toDetailPribadi()} />



</View>




    );
  }

}

function mapStateToProps(state) {
  return {
    mvState: state.mvState,
    perluasanMV: state.perluasanMV,
    asuransiMV: state.asuransiMV,
    dataVoucher: state.dataVoucher,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeliMV);
