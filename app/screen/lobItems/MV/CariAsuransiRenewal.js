import React, {Component} from 'react';
import {View, BackHandler, Image, ScrollView, TouchableOpacity,TouchableHighlight, StyleSheet, Picker, Text, TextInput, Button,Alert, ActivityIndicator, AsyncStorage,Dimensions} from 'react-native';
import {ListItem,Right,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';
//import DatePicker from 'react-native-datepicker';
//import BackIconComponent from '../../header/BackIconComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import SubmitButton from '../../../components/button/SubmitButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import JagainErrorText from '../../../components/text/JagainErrorText';
import JagainTextInput from '../../../components/text_input/JagainTextInput';

import JagainTextInputEditable from '../../../components/text_input/JagainTextInputEditable';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import ModalFilterPicker from 'react-native-modal-filter-picker'
//import Toast from 'react-native-root-toast';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class CariAsuransiRenewalMV extends React.Component {
  constructor(props) {
    super(props);
	this.inputs={}
	
	this.state={
		statusError: '',
		textError:'',
	}
  }

  toListAsuransi(mvState, lob) {
    var errorMessage= '';
    if(!mvState.vehicle_type || mvState.vehicle_type == 'Pilih'){
		//this.setState({statusError: 'vehicle_type',textError:'Jenis Kendaraan Belum Dipilih',});
		//this.focusErrorField('vehicle_type');
      errorMessage = 'Jenis Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.merek || mvState.merek == 'Pilih'){
		//this.setState({statusError: 'merek',textError:'Merek Kendaraan Belum Dipilih',});
		//this.focusErrorField('merek');
      errorMessage = 'Merek Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.seri_tipe || mvState.seri_tipe == 'Pilih'){
     //this.setState({statusError: 'seri_tipe',textError:'Seri Kendaraan Belum Dipilih',});
		//this.focusErrorField('seri_tipe');
    errorMessage = 'Seri Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.tsi){
       //this.setState({statusError: 'tsi',textError:'Harga Kendaraan Belum Diisi',});
		//this.focusErrorField('tsi');
      errorMessage = 'Harga Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!mvState.build_year || mvState.build_year == 'Pilih'){
      //this.setState({statusError: 'build_year',textError:'Tahun Produksi Kendaraan Belum Dipilih',});
		//this.focusErrorField('build_year');
      errorMessage = 'Tahun Produksi Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!mvState.plat || mvState.plat == 'Pilih'){
     //this.setState({statusError: 'plat',textError:'Plat Kendaraan Belum Dipilih',});
		//this.focusErrorField('plat');
	 errorMessage = 'Plat Kendaraan Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(mvState.aksesoris && mvState.aksesoris !== ' ' && !Boolean(mvState.harga_aksesoris)){
      //this.setState({statusError: 'harga_aksesoris',textError:'Harga Aksesoris Kendaraan Belum Diisi',});
	  
		//this.focusErrorField('harga_aksesoris');
	 errorMessage = 'Harga Aksesoris Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if((!mvState.aksesoris || mvState.aksesoris == ' ') && Boolean(mvState.harga_aksesoris)){
		//this.setState({statusError: 'aksesoris',textError:'Aksesoris Kendaraan Belum Diisi',});
		
		//this.focusErrorField('aksesoris');
      errorMessage = 'Aksesoris Kendaraan Belum Diisi';
      appFunction.toastError(errorMessage);
    }else{
      const {renewalPerluasanKendaraan} = this.props;
      const body = Object.assign({}, mvState, renewalPerluasanKendaraan);
      delete body.merek;
      delete body.seri_tipe;
      delete body.penggunaan;
		console.log('body', body);
      this.props.getListAsuransi(body, lob);
	  this.props.setDataRenewalSama(true);
      this.props.setScreen('ListAsuransiRenewal', 'renewal');
    }
  }

  jeniskendaraanlist(mvState, lob){
    const title = 'Jenis Kendaraan';
    const value = mvState.vehicle_type;
    const type = Object.assign({},{state: 'vehicle_type', lob});


    return(
  <JagainTextInputEditable
			 editable={false}
			title = {title}
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
    );
  }

  yearlist(mvState, lob){
    const title = 'Tahun Produksi Kendaraan';
    const value = mvState.build_year;
    const type = Object.assign({},{state: 'build_year', lob});


    return(
<JagainTextInputEditable
			 editable={false}
			title = {title}
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
    );
  }

  merklist(mvState, lob){
    const title = 'Merek Kendaraan';
    const value = mvState.merek;
    const type = Object.assign({},{state: 'merek', lob});


    return(

	  <JagainTextInputEditable
			 editable={false}
			title = {title}
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
    );
  }

  serilist(mvState, lob){
    const title = 'Seri & Tipe';
    const value = mvState.seri_tipe;
    const type = Object.assign({},{state: 'seri_tipe', lob});

    return(
	    <JagainTextInputEditable
			 editable={false}
			title = {title}
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
    );
  }
  
    platlist(mvState, lob){
      const title = 'Plat Area/Wilayah';
      const value = mvState.plat;
      const type = Object.assign({},{state: 'plat', lob});
      //console.log(options);
      return(
         <JagainTextInputEditable
			 editable={false}
			title = {title}
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
      );
    }


  usetext(mvState){
    const value = mvState.penggunaan;
    //console.log('use',value);

    return(
    
		  <JagainTextInputEditable
			 editable={false}
			title = 'Penggunaan'
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />

 
    );
  }

  coveragelist(mvState, lob){
    const title = 'Tipe Coverage';
    const value = mvState.coverage;
    const type = Object.assign({},{state: 'coverage', lob});

    return(
	  
	  <JagainTextInputEditable
			 editable={false}
			title = {title}
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
    );
  }

  hargatinput(mvState){
    var value = mvState.tsi;
    value = appFunction.numToCurrency(value);
	if(value == 0)
		value = 0.00;
    return(
      
		  <JagainTextInputEditable
			 editable={false}
			title = 'Harga Kendaraan'
			 value={`${value}`}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
      
    );
  }

  aksesoristinput(mvState){
    var value = mvState.aksesoris;
	if(!value){
		value = '-'
	}
    return(
	
		   <JagainTextInputEditable
			 editable={false}
			title = 'Aksesoris Tambahan'
			 value={value}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
		  />
     
    );
  }

  hargaaksesoristinput(mvState){
    var value = appFunction.numToCurrency(mvState.harga_aksesoris);
	if(value == 0)
		value = 0.00;
    return(
   
		 <JagainTextInputEditable
			 editable={false}
		   title = 'Total Harga Aksesoris'
			 value={`${value}`}
			  // Adding hint in Text Input using Place holder. 
			  onChangeText={() => null}
	
		  />
   
    );
  }

  changeharga(mvState, tsi){
    //const {typeVehicle} =this.state;
	var value = tsi;
	if(tsi == ''){
		value = 0;
	}
    const vp = appFunction.currencyToNum(value);
    //console.log(vp);
    var errorMessage = '';
    if(mvState.vehicle_type == 'Motor'){
      if(vp > 100000000){
		  // this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Motor Yang Dapat Dicover 100 Juta',});
        errorMessage = 'Harga Maksimal Motor Yang Dapat Dicover 100 Juta';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(100000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }else{
      if(vp > 2500000000){
		  //this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar',});
        errorMessage = 'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(2500000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }
  }
  
  changestateharga(mvState){
    //const {typeVehicle} =this.state;
	var value = mvState.tsi;
	if(mvState.tsi == ''){
		value = 0;
	}
    const vp = value;
    //console.log(vp);
    var errorMessage = '';
    if(mvState.vehicle_type == 'Motor'){
      if(vp > 100000000){
		  // this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Motor Yang Dapat Dicover 100 Juta',});
        errorMessage = 'Harga Maksimal Motor Yang Dapat Dicover 100 Juta';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(100000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }else{
      if(vp > 2500000000){
		  //this.setState({statusError: 'harga_aksesoris',textError:'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar',});
        errorMessage = 'Harga Maksimal Mobil Yang Dapat Dicover 2,5 Miliar';
        appFunction.toastError(errorMessage);
        this.props.setHargaKendaraan(2500000000);
      }else{
        //this.numToCurrency(vehiclePrice);
        this.props.setHargaKendaraan(vp);
      }
    }
  }



  setAccessoriesPrice (value) {
	  var val = value;
	  
	  if(value == ''){
		  val = 0;
	  }
		  
	
    const accessoriesPrice = appFunction.currencyToNum(val);
    this.props.setHargaAksesorisKendaraan(accessoriesPrice);
  }




  onBackPress = () => {
    const{prevScreen} = this.state;

    this.props.navigate('Main');
    return true;
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    this.props.setInsuranceId("", 'MV');
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  render(){
    const {renewalKendaraan, renewalLabel} = this.props;
    const {textError, statusError} = this.state;
    const mvLabelState = this.props.labelState;
    const lob = 'MV';
    //const {listPlatKendaraan} = this.props;
    console.log(JSON.stringify(renewalLabel));
	console.log(JSON.stringify(renewalKendaraan));
   

    return(
      <View style={[styles.centerItemContent, styles.top20]}>
        {this.jeniskendaraanlist(renewalKendaraan, lob)}
		{this.merklist(renewalKendaraan, lob)}
		{this.serilist(renewalKendaraan, lob)}
		{this.platlist(renewalKendaraan, lob)}
		{this.yearlist(renewalKendaraan, lob)}
		{this.hargatinput(renewalKendaraan)}
		{this.aksesoristinput(renewalKendaraan)}
		{this.hargaaksesoristinput(renewalKendaraan)}
		{this.usetext(renewalKendaraan)}
        {this.coveragelist(renewalKendaraan, lob)}
        <SubmitButton title='Cari Asuransi' onPress={() => {this.toListAsuransi(renewalKendaraan, lob);}} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    renewalPerluasanKendaraan: state.renewalPerluasanKendaraan,
    renewalKendaraan: state.renewalKendaraan,
    renewalLabel: state.renewalLabel,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransiRenewalMV);
