import React, {Component} from 'react';
import { BackHandler, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import Modal from 'react-native-modal'; // 2.4.0

import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

//import { TextInputMask } from 'react-native-masked-text'
import PopoverTooltip from 'react-native-popover-tooltip';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import CheckBox from 'react-native-check-box';
//import PopoverTooltip from 'react-native-popover-tooltip';
//import Accordion from 'react-native-accordion';
//import Accordion from '../../rsc/Accordion';
import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

class ListAsuransiRenewalPA extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      modalVisible:false,
    }
  }

  usetooltip(text){
    var {height, width} = Dimensions.get('window');
    return(
      <PopoverTooltip
        buttonComponent={
          <Icon name="ios-information-circle-outline" style={styles.colorRed} />
        }
        items={[
          {
            label: text,
            onPress: () => {}
          }
        ]}
        tooltipContainerStyle = {{ width: width*.8, }}
        // animationType='timing'
        // using the default timing animation
       />
     );
  }
   
  lihatBenefit(listAsuransi){
	  const {travelState} = this.props;
	  var benefit_url = '';
	  var dataBenefit = null;
	   benefit_url =  appFunction.webJagain + 'accident/mbenaci?is=' + listAsuransi.insurance_id + '&pc=' + listAsuransi.package_id + '&lg=id';
								  	  
	  dataBenefit = Object.assign({},{
		  insurance_name: listAsuransi.insurance_name,
		  insurance_id: listAsuransi.insurance_id,
		  package_name: listAsuransi.package_name,
		  benefit_url,
	  });
	  
	  this.props.setDataBenefit(dataBenefit);
	  this.props.navigate('Benefit');
  }

  doBeli(arrListAsuransi){
    this.props.setAsuransiAccident(arrListAsuransi);
    this.toNextScreen(arrListAsuransi);
  }

  toNextScreen(arrListAsuransi){
	  //setOrderIdAccident(order_id)
	  this.props.setPackage(arrListAsuransi.package_id, 'PA');
    this.props.setScreen('DataRenewal', 'renewal');
  }

  openModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:true});
    }
  }

  closeModal(typemodal) {
    if(typemodal == 'benefit'){
      this.setState({modalVisible:false});
    }
  }

  renderListAsuransi(listAsuransi){
    var arrListAsuransi = [];
	const detailPolis = this.props.detailPolis;

    const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	
	const discsrc = {
      '5'  : require('../../../assets/icons/discount/disc-5.png'),
      '10' : require('../../../assets/icons/discount/disc-10.png'),
      '15' : require('../../../assets/icons/discount/disc-15.png'),
      '20' : require('../../../assets/icons/discount/disc-20.png'),
	  '25' : require('../../../assets/icons/discount/disc-25.png'),
    }

    var asuransi = [];
    var totalPremi = [];
    var netPremi = [];
    var premiDasar = [];
    var packetName = [];
    var packetID = [];
    var discount = [];
    var showTotalPremi = [];
	var gratis = [];
  var listPembelianAsuransi= []
  var hitung
	//console.log('listAsuransi ',listAsuransi);	
	//console.log('detailPolis ',detailPolis);

    if(listAsuransi.length > 0){
      for (var i = 0; i < listAsuransi.length; i++) {
		  if(listAsuransi[i].gratis == 'N'){
        if(detailPolis.status_wajib == 'Y'){
            if(detailPolis.insurance_name == listAsuransi[i].insurance_name){
              if (this.props.renewalLabel.data_sama) {
                if (this.props.renewalAccident.package_id == listAsuransi[i].package_id) {
                  asuransi.push(listAsuransi[i].insurance_name);
                  totalPremi.push(listAsuransi[i].premi);
                  netPremi.push(listAsuransi[i].net_premi);
                  premiDasar.push(listAsuransi[i].premi_dasar);
                  packetName.push(listAsuransi[i].package_name);
                  packetID.push(listAsuransi[i].package_id);
                  discount.push(listAsuransi[i].disc_insurance);
                  gratis.push(listAsuransi[i].gratis);
                  listPembelianAsuransi.push(listAsuransi[i]);
                }
              }else{
                asuransi.push(listAsuransi[i].insurance_name);
                totalPremi.push(listAsuransi[i].premi);
                netPremi.push(listAsuransi[i].net_premi);
                premiDasar.push(listAsuransi[i].premi_dasar);
                packetName.push(listAsuransi[i].package_name);
                packetID.push(listAsuransi[i].package_id);
                discount.push(listAsuransi[i].disc_insurance);
                gratis.push(listAsuransi[i].gratis);
                listPembelianAsuransi.push(listAsuransi[i]);
              }
            }
        }else{
          if (this.props.renewalLabel.data_sama){
            if (this.props.renewalAccident.package_id == listAsuransi[i].package_id){
             
              asuransi.push(listAsuransi[i].insurance_name);
              totalPremi.push(listAsuransi[i].premi);
              netPremi.push(listAsuransi[i].net_premi);
              premiDasar.push(listAsuransi[i].premi_dasar);
              packetName.push(listAsuransi[i].package_name);
              packetID.push(listAsuransi[i].package_id);
              discount.push(listAsuransi[i].disc_insurance);
              gratis.push(listAsuransi[i].gratis);
              listPembelianAsuransi.push(listAsuransi[i]);
            }
        }else{
            asuransi.push(listAsuransi[i].insurance_name);
            totalPremi.push(listAsuransi[i].premi);
            netPremi.push(listAsuransi[i].net_premi);
            premiDasar.push(listAsuransi[i].premi_dasar);
            packetName.push(listAsuransi[i].package_name);
            packetID.push(listAsuransi[i].package_id);
            discount.push(listAsuransi[i].disc_insurance);
            gratis.push(listAsuransi[i].gratis);
            listPembelianAsuransi.push(listAsuransi[i]);
        }
      }
      } 
    }

      
        
    
    
      if (this.props.renewalLabel.data_sama) {
        if(asuransi.length == 0){
          if (asuransi.length == 0) {
            for (var i = 0; i < listAsuransi.length; i++) {
              if (listAsuransi[i].gratis == 'N') {
                if (detailPolis.status_wajib == 'Y') {
                  if (detailPolis.insurance_name == listAsuransi[i].insurance_name) {
                    asuransi.push(listAsuransi[i].insurance_name);
                    totalPremi.push(listAsuransi[i].premi);
                    netPremi.push(listAsuransi[i].net_premi);
                    premiDasar.push(listAsuransi[i].premi_dasar);
                    packetName.push(listAsuransi[i].package_name);
                    packetID.push(listAsuransi[i].package_id);
                    discount.push(listAsuransi[i].disc_insurance);
                    gratis.push(listAsuransi[i].gratis);
                    listPembelianAsuransi.push(listAsuransi[i]);
                  }
                } else {
                  asuransi.push(listAsuransi[i].insurance_name);
                  totalPremi.push(listAsuransi[i].premi);
                  netPremi.push(listAsuransi[i].net_premi);
                  premiDasar.push(listAsuransi[i].premi_dasar);
                  packetName.push(listAsuransi[i].package_name);
                  packetID.push(listAsuransi[i].package_id);
                  discount.push(listAsuransi[i].disc_insurance);
                  gratis.push(listAsuransi[i].gratis);
                  listPembelianAsuransi.push(listAsuransi[i]);
                }
              }
            }
          }
        }else{
          arrListAsuransi.push(
          <View style={styles.centerItemContent} key='renewal data sama'>
            <View style={[styles.bgWhite, styles.bottom5, styles.radius5, styles.top10, styles.width85p]}>
              <View style={[styles.centerItemContent, { padding: 20 }]}>
                <Text style={[styles.baseText]}>Ingin Memilih Asuransi Lainnya?</Text>
                  <View style={styles.centerItemContent}>
                  <TouchableHighlight underlayColor='white' onPress={() => this.props.setDataRenewalSama(false)}>
                        <Text style={[styles.underline, styles.bold, styles.colorRed]}>Pilih Asuransi Lain</Text>     
                  </TouchableHighlight>
                  </View>   
              </View>
            </View>
          </View>);
        }
      }
  
	//console.log('detailPolis2 ',detailPolis);

       for(let i = 0; i < asuransi.length; i++){
         if(discount[i] > 0){
           showTotalPremi.push(
             <View key = {i}>
               <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}Total:{"\n"}{"\n"}
                </Text>
                <View style={styles.directionColumn}>
				 { gratis[i] == 'N' ?
                  <Text style={[styles.font12, styles.strike]}>
                    {"\n"}Rp. {totalPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
                  </Text>
				 : null
				 }
                  <Text style={[styles.font14, styles.bold, styles.baseText]}>
					  { gratis[i] == 'N' ? 'Rp. ' + netPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '\n\n' : '\nGRATIS' }
                  </Text>
                </View>
              </View>
            </View>
          );
        }else {
          showTotalPremi.push(
            <View key = {i}>
               <View style = {[styles.directionRow, styles.spaceBetween, styles.flex2]}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}>
                  {"\n"}Total:{"\n"}{"\n"}
                </Text>
				<View style={styles.directionColumn}>
                <Text style={[styles.font14, styles.bold, styles.baseText]}> 
				  { gratis[i] == 'N' ? '\n\nRp. ' + netPremi[i].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '\n\n' : '\nGRATIS' }
                </Text>
				</View>
              </View>
            </View>
          );
        }
  
        arrListAsuransi.push(
          <View style={[styles.centerItemContent, styles.top10]} key = {i}>
             {discount[i] && gratis[i] == 'N'? 
            <View style={[styles.directionRow, styles.flexEndSelf, {position:'absolute', top:-15, zIndex: 1}]}>
              <View style={[styles.directionRow]}> 
              <View>
              <Image style={[styles.width70, styles.height70]} source={discsrc[discount[i].toString()]} />
              </View>
              </View>
            </View>
            : null}
	
              <View style={[styles.bgWhite, styles.radius5, styles.bottom10, styles.top10, styles.pad5, { zIndex: 0}]}> 
              <View style={[styles.directionRow, styles.left5]}>
                <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                  <TouchableHighlight>
                    <View style={styles.centerItemContent}>
                       <View style={[styles.centerItemContent, styles.height50]}>
                           <Image source={asimgsrc[asuransi[i]]} style={[styles.height25, styles.width100, styles.resizeContain]}/>
                        </View>
                      </View>
                    </TouchableHighlight>
                    <View style={styles.bottom10}></View>
                  </View>
                  <View style={styles.width55p}>  
                    <View style={styles.directionColumn}>
                      <View style={styles.directionColumn}>
                        <Text style={[styles.font12, styles.bold, styles.baseText]}>
                          {asuransi[i]}{"\n"}{"\n"}
                        </Text>
                        <View style={[styles.borderTransparent, styles.radius2, styles.border1]}>
                          <Text style={[styles.font10, styles.bold]}>{"\n"}{"\n"}{packetName[i]} </Text>
                        </View>
                      </View>
                      {showTotalPremi[i]}
                      <View style={[styles.directionRow, styles.bottom10]}>
                         <View>
						<Button underlayColor='blue'
                            style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                            onPress={() => this.lihatBenefit(listPembelianAsuransi[i])}>
                            <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Benefit</Text>
                          </Button>
						</View>
                        <View>
                          <Button underlayColor='blue'
                            style={[styles.centerFlexContent, styles.bgRedE5, styles.height25p, styles.width80p]}
                            onPress={() => this.doBeli(listPembelianAsuransi[i])}>
                            <Text style={[styles.font12, styles.colorWhite, styles.bold, styles.baseText]}>Pilih</Text>
                          </Button>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )
        }
      }else{
        arrListAsuransi.push(
          <View key='loading-page'>
           <LoadingPage />
           </View> 
        );
      }
      return arrListAsuransi;
    }

    componentWillUnmount(){
      this.props.setDataRenewalSama(false);
    }



   componentDidMount() {
      this.props.getProvinsi();
      this.props.getDistrict();  
   }

   componentWillReceiveProps(NextProps){
     if (this.props.listProvinsi != NextProps.listProvinsi){
       this.props.setProvinceName(appFunction.findLabel(NextProps.listProvinsi, NextProps.renewalDataAccident.province_id));
       }
     if (this.props.listDistrict != NextProps.listDistrict){
         this.props.setDistrictName(appFunction.findLabel(NextProps.listDistrict, NextProps.renewalDataAccident.district_id));
       }
   }

   

  render(){
    var {height, width} = Dimensions.get('window');
    const {listAsuransi, itemsIsLoading, loadingState, screen} = this.props;
    //console.log('render', this.props.renewalLabel.data_sama);

    return(
      <ScrollView>
	  { 	loadingState.loadingListAsuransiAccident &&  screen.renewalScreen?
           <LoadingPage /> :
			 this.renderListAsuransi(listAsuransi)
      }
        
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    listAsuransi: state.listAsuransi,
    status: state.status,
    screen: state.screen,
    itemsIsLoading:state.itemsIsLoading,
	loadingState:state.loadingState,
  detailPolis:state.detailPolis,
  renewalLabel:state.renewalLabel,
    renewalAccident: state.renewalAccident,
    renewalDataAccident: state.renewalDataAccident,
  listDistrict: state.listDistrict,
  listProvinsi: state.listProvinsi,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAsuransiRenewalPA);
