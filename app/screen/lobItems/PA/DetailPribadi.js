import React, { Component } from 'react';
import { Alert, BackHandler, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Radio, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import md5 from "react-native-md5";
import * as appFunction from '../../../function';
import SubmitButton from '../../../components/button/SubmitButton';
import UploadButton from '../../../components/button/UploadButton';
import FilterPicker from '../../../components/picker/filterPicker';
import FilterLabelPicker from '../../../components/picker/filterLabelPicker';
import LoadingPage from '../../../components/loading/LoadingPage';

import JagainTextInput from '../../../components/text_input/JagainTextInput';
import JagainDatePicker from '../../../components/date_picker/JagainDatePicker';
import WheelPicker from '../../../components/picker/WheelPicker';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import styles from '../../../assets/styles';
import UploadImage from '../../../components/upload/UploadImage';

class DetailPribadiPA extends Component{
  constructor(props) {
    super(props);
	this.state = {
		loading: false,
	}
  }
  
  selectkewarganegaraan(value){  
	  this.props.setKewarganegaraanDtAcd(value);
  }

  kewarganegaraanradio(kewarganegaraan){
    
    var init1 = true;
	var init2 = false;
	var value = 'wni'; 
    if(kewarganegaraan == 'wni'){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
    return(
	
	<View style={[styles.width80p]}>
		<View style={[styles.directionRow]}>
			<Text>Kewarganegaraan</Text>
		</View>
		 <View style={[styles.directionRow]}>
		  <View style={[styles.directionRow, styles.right10]}>
			  <Radio selected={init1} onPress={() => {this.selectkewarganegaraan('wni')}} />
		   <Text>WNI</Text>
		   </View>
		   <View style={[styles.directionRow]}>
			 <Radio selected={init2} onPress={() => {this.selectkewarganegaraan('wna')}} />
			 <Text>WNA</Text>
			</View>
		</View>
	  </View>
    );
  }



  ahliwarislist(hub_ahliwaris){
    const lob = 'Accident';
    const title = 'Hubungan Ahli Waris';
    const options = this.props.listAhliWaris;
    const type = Object.assign({},{state: 'hub_ahliwaris', lob});


    return(
      <FilterPicker title={title}
        value={hub_ahliwaris}
        options={options}
        type={type}
      />
    );
  }

  provinsilist(label_provinsi){
      const lob = 'Accident';
    const title = 'Provinsi';
    const options = this.props.listProvinsi;
    const type = Object.assign({},{state: 'province_id', lob});


    return(
      <FilterLabelPicker title={title}

        value={label_provinsi}
        options={options}
        type={type}
      />
    );
  }


  districtlist(label_district){
      const lob = 'Accident';
    const title = 'Kabupaten / Kota';
    const options = this.props.listDistrict;
    const type = Object.assign({},{state: 'district_id', lob});


    return(
      <FilterLabelPicker title={title}
        value={label_district}
        options={options}
        type={type}
      />
    );
  }


  genderlist(value){
    const title = 'Title';
    const lob = 'Accident';
    var options = [{
      key:'Pilih',
      label:'Pilih'
    },
    {
      key:'Mr.',
      label:'Mr.'
    },
    {
      key:'Mrs.',
      label:'Mrs.'
    },
    {
      key:'Ms.',
      label:'Ms.'
    }];

    const type = Object.assign({},{state: 'tittle', lob});

    return(
	<View style = {[styles.width40p]}>
      <FilterPicker title={title}
        value={value}
        options={options}
        type={type}
      />
	</View>
    );
  }
  
  namatext(nama){
	  return(
	     <JagainTextInput
	   title = 'Nama Tertanggung'
		 value={nama}
			autoCapitalize='words'
		  // Adding hint in Text Input using Place holder. 
		  onChangeText={(nama) => this.props.setNamaPemilikDtAcd(appFunction.alphabetSpaceOnly(nama))}
          
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
   alamattext(alamat){
	  return(
	    <JagainTextInput
	   title = 'Alamat'
		 value={alamat}
			  onChangeText={(alamat) => this.props.setAlamatPemilikDtAcd(appFunction.dotalphaNumericOnly(alamat))}
			multiline = {true}
			numberOfLines = {4}
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
  ktpkitastext(no_identitas, ktpkitas){
	  return(
	    <JagainTextInput
	   title =  {'No '+ ktpkitas}
		value={no_identitas}
				keyboardType='numeric'
				onChangeText={(no_identitas) => this.props.setNoIdentitasDtAcd(appFunction.numericOnly(no_identitas))}
				maxLength={16}
				
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
	  
  }
  
    tempatlahirtext(tempat_lahir){
	  return(
	     <JagainTextInput
	   title = 'Tempat Lahir'
		 value={tempat_lahir}
			autoCapitalize='words'
		  // Adding hint in Text Input using Place holder. 
		  onChangeText={(tempat_lahir) => this.props.setTempatLahirDtAcd(appFunction.alphabetSpaceOnly(tempat_lahir))}   
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  
  namaahliwaristext(nama_ahliwaris){
	  return(
	  <JagainTextInput
	   title =  'Nama Ahli Waris'
		value={nama_ahliwaris}
				autoCapitalize='words'
				// Adding hint in Text Input using Place holder.
				onChangeText={(nama_ahliwaris) => this.props.setNamaAhliWarisDtAcd(nama_ahliwaris)}
				
		  colorOnFocus = '#00e500' 
		  widthOnFocus = {3}
	  />
		
	  );
  }
  

  doPostData(body, dataAccident){
    var errorMessage =  '';
    if(!dataAccident.tittle || dataAccident.tittle == 'Pilih'){
      errorMessage = 'Title Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.nama){
      errorMessage = 'Nama Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.tempat_lahir){
      errorMessage = 'Tempat Lahir Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.alamat){
      errorMessage = 'Alamat Tertanggung Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.province_id || dataAccident.province_id == 'Pilih'){
      errorMessage = 'Provinsi Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.district_id || dataAccident.district_id == 'Pilih'){
      errorMessage = 'Kabupaten / Kota Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.no_identitas){
      errorMessage = 'No Identitas Belum Diisi';
      appFunction.toastError(errorMessage);
    }
    //if(!photo_belakang)
    //if(!photo_depan)
  //  if(!photo_kanan)
    //if(!photo_kiri)
    //if(!photo_ktp)
    //if(!photo_stnk)
    else if(!dataAccident.file_identitas){
      errorMessage = 'Foto KTP / KITAS Belum Diupload';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.nama_ahliwaris){
      errorMessage = 'Nama Ahli Waris Belum Diisi';
      appFunction.toastError(errorMessage);
    }else if(!dataAccident.hub_ahliwaris || dataAccident.hub_ahliwaris == 'Pilih'){
      errorMessage = 'Hubungan Ahli Waris Belum Dipilih';
      appFunction.toastError(errorMessage);
    }else{
      this.props.setPostData(body);
	  // this.props.doPostDataAccident(body);
      if(this.props.asuransiAccident.gratis == 'N'){
			this.props.navigate('Pembayaran');
		}else{
			this.setState({loading:true});
		}
    }
  }



  setType(value){
          this.props.setFileIdentitasDtAcd(value);
  }

  fileUploaded(file, title){  
	  return(
		<UploadImage 
		filetitle={title} 
		filename={file} 
		onUploadFile ={(value)=>
          this.setType(value)}  />
	  );
  }
  
  dataTertanggung(){
	const {
      alamat,
      nama,
      no_identitas,
      file_identitas,
      tittle,
      kewarganegaraan,
      province_id,
      district_id,
      nama_ahliwaris,
      hub_ahliwaris,
	  tempat_lahir,
    }  = this.props.dataAccident;
	
	const {
		label_provinsi,
		label_district,
	} = this.props.accidentLabelState;
	
		var {height, width} = Dimensions.get('window');
	  
    var ktpkitas = 'KTP';
  
    if(kewarganegaraan == 'wni'){
      ktpkitas = 'KTP';
    }else{
      ktpkitas = 'KITAS';
    }
	
	  return(
      <View style={[styles.detailPribadiMainContainer]}>
        <WheelPicker />
				{this.genderlist(tittle)}
				<View style={[styles.top10]} />
				{this.namatext(nama)}	  	  
				<View style={[styles.top10]} />
				{this.tempatlahirtext(tempat_lahir)}
				<View style={[styles.top10]} />
				{this.alamattext(alamat)}
				<View style={[styles.top10]} />
				{this.provinsilist(label_provinsi)}
				<View style={[styles.top10]} />
				{this.districtlist(label_district)}
				<View style={[styles.top10]} />
				{this.kewarganegaraanradio(kewarganegaraan)}
				<View style={[styles.top10]} />
				{this.ktpkitastext(no_identitas, ktpkitas)}
				<View style={[styles.top10]} />
				{this.fileUploaded(file_identitas, ktpkitas)}
				<View style={[styles.top10]} />
				{this.namaahliwaristext(nama_ahliwaris)}
				<View style={[styles.top10]} />
				{this.ahliwarislist(hub_ahliwaris)}
				<View style={{height: height*.05}}></View>
			</View>
	  );
  }

  componentWillReceiveProps(NextProps){
    if(NextProps.dataAccident.province_id != this.props.dataAccident.province_id){
		this.props.resetDistrictDtAcd();
          this.props.resetListDistrict();
      if(NextProps.dataAccident.province_id != 'Pilih')
        this.props.getDistrict(NextProps.dataAccident.province_id);
	}
	
	if(this.props.dataTransaksi !== NextProps.dataTransaksi){
		this.setState({loading: false});
		Alert.alert('Pembelian Polis Sukses', 
					'Selamat, Pembelian Asuransi Jiwa dengan Nomor Transaksi ' + NextProps.dataTransaksi.kode_transaksi + ' berhasil, Silakan cek Email Anda untuk mendownload Polis atau cek di Menu My Polis untuk melihat Polis.',
					[
						{text: 'OK', onPress: () => this.props.navigate('Main')},
					],
					{ cancelable: false }
					); 
	}
       
  }

  componentDidMount(){
    this.props.getAhliWaris();
    this.props.getProvinsi();
	this.props.resetPostData();
  }


	render() {
		var {height, width} = Dimensions.get('window');
		const {dataAccident, session, accidentState, dataVoucher, asuransiAccident, accidentLabelState} = this.props;

		const body = Object.assign({}, accidentState);

		const dataPostAccident = Object.assign({},dataAccident,
		  {visitor_id:session.visitor_id},
		  {kode_voucher:dataVoucher.kode_voucher},
		);
		const postData =  Object.assign({},{data_accident:dataPostAccident}, body, {package_id:asuransiAccident.package_id });


		return (
			<View style = {[styles.centerItemContent]}>
				{ !this.state.loading ? 
				this.dataTertanggung()
				:
					<LoadingPage />	
			} 
			{ !this.state.loading ? 
				<SubmitButton title='Submit' onPress={() => this.doPostData(postData,dataAccident)} />  :
				null
				
				}
			</View>

		);
	}
}

function mapStateToProps(state) {
  return {
	dataTransaksi: state.dataTransaksi,
    dataAccident: state.dataAccident,
    accidentState: state.accidentState,
    accidentLabelState: state.accidentLabelState,
    session:state.session,
    dataVoucher:state.dataVoucher,
    asuransiAccident: state.asuransiAccident,
    listAhliWaris: state.listAhliWaris,
    listProvinsi: state.listProvinsi,
    listDistrict:state.listDistrict,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadiPA);
