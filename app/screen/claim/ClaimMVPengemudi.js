/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, CheckB } from 'react-native';

import { Header, CheckBox } from 'react-native-elements';

import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { RkButton, RkType, RkTheme, RkChoiceGroup, RkChoice } from 'react-native-ui-kitten';

import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';
import ImagePicker from 'react-native-image-crop-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');

var radio_pengemudi_bekerja = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
];

var radio_tertanggung_tahu = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
];

class ClaimMVPengemudi1 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      nama_pengemudi: this.props.dataClaim.nama_pengemudi,
      alamat_pengemudi: this.props.dataClaim.alamat_pengemudi,
      no_sim_pengemudi: this.props.dataClaim.no_sim_pengemudi,
      periode_sim_pengemudi: this.props.dataClaim.periode_sim_pengemudi,
      file_foto_sim: this.props.dataClaim.file_foto_sim_pengemudi,
      pengemudi_bekerja: this.props.dataClaim.pengemudi_bekerja,
      tertanggung_tahu: this.props.dataClaim.tertanggung_tahu,
      pengemudi_bekerjaIndex: (this.props.dataClaim.pengemudi_bekerja == 'Y') ? 0 : 1,
      tertanggung_tahuIndex: (this.props.dataClaim.tertanggung_tahu == 'Y') ? 0 : 1,

      namaPengemudiEditable : true,
      alamatPengemudiEditable : true,

      isPicked: false,
      pengemudiChecked: false,
    };

    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.nama_pengemudiRef = this.updateRef.bind(this, 'nama_pengemudi');
    this.alamat_pengemudiRef = this.updateRef.bind(this, 'alamat_pengemudi');
    this.no_sim_pengemudiRef = this.updateRef.bind(this, 'no_sim_pengemudi');
    this.periode_sim_pengemudiRef = this.updateRef.bind(this, 'periode_sim_pengemudi');
    this.pengemudi_bekerjaRef = this.updateRef.bind(this, 'pengemudi_bekerja');
    this.tertanggung_tahu = this.updateRef.bind(this, 'tertanggung_tahu');

  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('ClaimMVUmum');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {

  }

  componentDidUpdate(){
    // console.log('pengemudi checked',this.state.pengemudiChecked);

  }

  kembali() {
    this.props.navigate('ClaimMVUmum');
  }

  lanjut() {

  }

  shouldComponentUpdate(nextProps, nextState){
    const{detailPolis} = this.props;
    if(this.state.pengemudiChecked != nextState.pengemudiChecked){
      
      // console.log('berubah');
      return true;

    } 
    if(this.state.nama_pengemudi != nextState.nama_pengemudi){
      return true;
    }
    


    return true;
  }


  onChangeText(text) {
    try {
      ['nama_pengemudi', 'alamat_pengemudi', 'no_sim_pengemudi']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  setStategambarSim(value) {
    this.setState({
      file_foto_sim: value,
    });
  }

  setPengemudiChecked(){
    
    const { detailPolis} = this.props;
    const {pengemudiChecked} = this.state;
    // console.log(pengemudiChecked);

    let alamat = null;
    if(detailPolis.alamat_tertanggung == "" || detailPolis.alamat_tertanggung == null){
      alamat = "-";
    }

    if (this.state.pengemudiChecked == false) { // terbaca terbalik antara false dan true
      this.setState({
        nama_pengemudi: detailPolis.nama_tertanggung,
        alamat_pengemudi: alamat,
        namaPengemudiEditable: false,
        alamatPengemudiEditable: true
      });
    } else {
      this.setState({
        nama_pengemudi: "",
        alamat_pengemudi: "",
        namaPengemudiEditable: true,
        alamatPengemudiEditable: true,
      })
    }

    this.setState({
      pengemudiChecked : !pengemudiChecked,
      
    });

    

    console.log('detail polis',detailPolis);
  }

  async onSubmit() {
    const { listDataGambarSim } = this.props;
    console.log('sim', listDataGambarSim);

    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error


    //console.log('detailPolis2', detailPolis);
    ['nama_pengemudi', 'alamat_pengemudi', 'no_sim_pengemudi']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Tidak boleh kosong';
          isError = 1;
        }
      });

    this.setState({ errors });
    // console.log('errors', errors);

    // console.log('isError',isError);
    if (isError == 0 && this.state.periode_sim_pengemudi != '' && listDataGambarSim[0].path != null) {


      await this.setStategambarSim(listDataGambarSim[0].path);

      /*
      RNFS.readFile(file.substring(7), "base64")  //substring(7) -> to remove the file://
.then(res => this.setState({base64: res}))
      */

      const base64image = await RNFS.readFile(listDataGambarSim[0].path.substring(7), 'base64');
      // console.log('base 64 state 2 : ',base64image);
      temp = {
        nama_pengemudi: this.state.nama_pengemudi,
        alamat_pengemudi: this.state.alamat_pengemudi,
        no_sim_pengemudi: this.state.no_sim_pengemudi,
        periode_sim_pengemudi: this.state.periode_sim_pengemudi,
        // file_foto_sim_pengemudi : this.state.file_foto_sim,
        file_foto_sim_pengemudi: base64image,
        tertanggung_tahu: this.state.tertanggung_tahu,
        pengemudi_bekerja: this.state.pengemudi_bekerja,

      }

      await this.props.setDataClaimMV(temp);
      this.props.navigate('ClaimMVPenumpangSaksi');
    } else {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    }
  }

  onSubmitNama_pengemudi() {

  }

  onSubmitAlamat_pengemudi() {

  }

  onSubmitNo_sim_pengemudi() {

  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  uploadSim() {
    // const {listDataGambarSim} = this.props;

    let imageSim = [];

    ImagePicker.openCamera({
      cropping: true,
      width: 1000,
      height: 1000,
      includeExif: false,
      avoidEmptySpaceAroundImage: true,

    }).then(image => {
      console.log('received image', image);
      ImageResizer.createResizedImage(image.path, image.width, image.height, 'JPEG', 100, 0).then((response) => {

        image = {
          'size': response.size,
          'path': response.uri,
          'caption': '',
        };

        this.props.setDataGambarSimKameraTemp(image);

      });

    }).catch(e => alert(e));
  }

  updateTanggal(tanggal) {

    this.setState({
      periode_sim_pengemudi: tanggal,
      isPicked: true,
    });
    // console.log(this.state.periode_sim_pengemudi);
  }

  render() {
    const { listDataGambarSim } = this.props;
    const { claimState, claimLabelState, dataClaim, detailPolis } = this.props;
    // console.log(listDataGambarSim);
    let { errors = {}, ...data } = this.state;
    let { alamatKejadian = null, kecepatanInsiden = null, deskripsiInsiden = null } = data;

    // console.log(dataClaim);
    const {
      periode_sim_pengemudi,
    } = this.state;

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var maxDate = new Date(year + 5, 12, 31);
    var minDate = new Date();

    // console.log('Detail Polis ', JSON.stringify(detailPolis));

    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'Incident Form 2 dari 6',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.nama_pengemudiRef, this.alamat_pengemudiRef, this.no_sim_pengemudiRef];
          }}
        >
          <ScrollView style={styles.container}>
            <View>
              <CheckBox
                title='Pengemudi sama dengan Pemilik Polis'
                checked={this.state.pengemudiChecked}
                onPress={()=>{this.setPengemudiChecked()}}                
              />
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.nama_pengemudiRef}
                  label='Nama Pengemudi'
                  keyboardType='default'
                  onFocus={this.onFocus}
                  onSubmitEditing={this.onSubmitNama_pengemudi}
                  onChangeText={this.onChangeText}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.nama_pengemudi}
                  maxLength={40}
                  value={this.state.nama_pengemudi}
                  editable={this.state.namaPengemudiEditable}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='address-book' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.alamat_pengemudiRef}
                  label='Alamat Pengemudi'
                  keyboardType='default'
                  onFocus={this.onFocus}
                  onSubmitEditing={this.onSubmitAlamat_pengemudi}
                  onChangeText={this.onChangeText}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.alamat_pengemudi}
                  maxLength={80}
                  value={this.state.alamat_pengemudi}
                  editable={this.state.alamatPengemudiEditable}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-speedometer' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='No SIM Pengemudi'
                  keyboardType='numeric'
                  ref={this.no_sim_pengemudiRef}
                  maxLength={12}
                  value={this.state.no_sim_pengemudi}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitNo_sim_pengemudi}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.no_sim_pengemudi}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='calendar' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.fieldTanggal}>
                <DatePicker
                  style={{ width: wp('80%'), }}
                  placeholder="Periode SIM Pengemudi (YYYY-MM-DD)"
                  mode="date"
                  date={periode_sim_pengemudi == null ? null : this.state.periode_sim_pengemudi}
                  androidMode="spinner"
                  format="YYYY-MM-DD"
                  minDate={minDate}
                  maxDate={maxDate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {

                    },
                    dateInput: {
                      borderWidth: 0,
                      alignItems: 'flex-start',
                      paddingTop: 15,
                      borderBottomWidth: .5,
                    },
                    dateText: {
                      fontSize: 18,
                      //color: 'rgba(0,0,0, .38)'
                    }

                    // ... You can check the source to find the other keys.
                  }}
                  showIcon={false}
                  onDateChange={(tanggal) => { this.updateTanggal(tanggal) }}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.fieldImage}>
                {listDataGambarSim[0].path == null ?
                  <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSim()} >

                    <Image source={require('../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} />

                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSim()} >

                    <Image source={{ uri: listDataGambarSim[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />

                  </TouchableOpacity>
                }
              </View>
              <View style={styles.fieldButton}>
                <RkButton rkType='danger' onPress={() => this.uploadSim()}>Unggah Foto SIM</RkButton>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.text}>
                <Text>Apakah Pengemudi bekerja pada tertanggung?</Text>
                <RadioForm
                  formHorizontal={true}
                  animation={true}
                  initial={1}
                >
                  {radio_pengemudi_bekerja.map((obj, i) => {
                    var onPress1 = (value, index) => {
                      this.setState({
                        pengemudi_bekerjaIndex: index,
                        pengemudi_bekerja: value,
                      })
                    }
                    return (
                      <RadioButton labelHorizontal={true} key={i} >
                        {/*  You can set RadioButtonLabel before RadioButtonInput */}
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={this.state.pengemudi_bekerjaIndex === i}
                          onPress={onPress1}
                          buttonStyle={{}}
                          buttonWrapStyle={{ marginRight: 10 }}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          onPress={onPress1}
                          labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                        />
                      </RadioButton>
                    )
                  })}
                </RadioForm>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.text}>
                <Text>Apakah Pengemudi melakukan tugas atas sepengetahuan tertanggung?</Text>
                <RadioForm
                  formHorizontal={true}
                  animation={true}
                  initial={1}
                >
                  {radio_tertanggung_tahu.map((obj, i) => {
                    var onPress2 = (value, index) => {
                      this.setState({
                        tertanggung_tahuIndex: index,
                        tertanggung_tahu: value,
                      })
                    }
                    return (
                      <RadioButton labelHorizontal={true} key={i} >
                        {/*  You can set RadioButtonLabel before RadioButtonInput */}
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={this.state.tertanggung_tahuIndex === i}
                          onPress={onPress2}
                          buttonStyle={{}}
                          buttonWrapStyle={{ marginRight: 10 }}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          onPress={onPress2}
                          labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                        />
                      </RadioButton>
                    )
                  })}
                </RadioForm>
              </View>
            </View>

            <View style={styles.button}>

              <RkButton rkType='danger' onPress={this.onSubmit}>Lanjut</RkButton>

            </View>
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  fieldButton: {
    width: (width * 0.6) - 20,
    height: width * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: .5,
  },
  fieldImage: {
    width: width * 0.4,
    height: width * 0.4,
    borderWidth: .5,

  },
  fieldTanggal: {
    flex: 9,
    paddingLeft: 10,
    paddingBottom: 15
  },
  text: {

  },
  textPicker: {
    height: 56,
    fontSize: 16,
    color: 'rgba(0,0,0, .38)',
    borderBottomWidth: .5,
    borderColor: 'rgba(0,0,0, .38)',
    paddingTop: 20,
  },
});


function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listDataGambarSim: state.listDataGambarSim,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVPengemudi1);
