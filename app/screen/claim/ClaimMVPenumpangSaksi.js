/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';
import ImagePicker from 'react-native-image-crop-picker';
import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';


import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');

let radio_status_saksi = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
];

let radio_penumpang_cedera = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
]

class ClaimMVPenumpangSaksi1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      list_penumpang_cedera: this.props.dataClaim.list_penumpang_cedera == null ? [] : this.props.dataClaim.list_penumpang_cedera,
      list_saksi: this.props.dataClaim.list_saksi == null ? [] : this.props.dataClaim.list_saksi,
      penumpang_cedera: this.props.dataClaim.penumpang_cedera,
      status_saksi: this.props.dataClaim.status_saksi,
      penumpang_cederaIndex: (this.props.dataClaim.penumpang_cedera == 'Y') ? 0 : 1,
      status_saksiIndex: (this.props.dataClaim.status_saksi == 'Y') ? 0 : 1,

    }

    this.onSubmit = this.onSubmit.bind(this);
    this.onTambahPenumpang = this.onTambahPenumpang.bind(this);
    this.onKurangPenumpang = this.onKurangPenumpang.bind(this);
    this.onTambahSaksi = this.onTambahSaksi.bind(this);
    this.onKurangSaksi = this.onKurangSaksi.bind(this);
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('ClaimMVPengemudi');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentDidUpdate() {
    /* penumpang */
    if (this.state.penumpang_cedera == 'Y') {
      let outputPenumpang = [];
      const { list_penumpang_cedera } = this.state;

      if (list_penumpang_cedera.length == 0) {
        let list_penumpang_cedera = this.state.list_penumpang_cedera;
        let count = list_penumpang_cedera.length + 1;

        // console.log(count);
        list_penumpang_cedera.push(
          {
            alamat: '',
            jenis: 'P',
            nama: '',
            id: count,
          }
        );

        this.setState({
          list_penumpang_cedera: list_penumpang_cedera
        });
      }
    }
    /* end penumpang */

    /* saksi */
    if (this.state.status_saksi == 'Y') {
      let outputSaksi = [];
      const { list_saksi } = this.state;

      if (list_saksi.length == 0) {
        let list_saksi = this.state.list_saksi;
        let count = list_saksi.length + 1;

        // console.log(count);
        list_saksi.push(
          {
            alamat: '',
            jenis: 'S',
            nama: '',
            id: count,
          }
        );

        this.setState({
          list_saksi: list_saksi
        });
      }
    }
    /* end saksi */
  }

  componentWillUnmount() {

  }

  onChangeTextNamaPenumpang(item, text) {
    const { list_penumpang_cedera } = this.state;
    let items = list_penumpang_cedera;

    objIndex = list_penumpang_cedera.findIndex((obj => obj.id == item.id));
    // console.log('obj',objIndex);
    list_penumpang_cedera[objIndex].nama = text;

    // console.log(list_penumpang_cedera);
    this.setState({
      list_penumpang_cedera: list_penumpang_cedera
    })
  }

  onChangeTextAlamatPenumpang(item, text) {
    const { list_penumpang_cedera } = this.state;

    let items = list_penumpang_cedera;


    objIndex = list_penumpang_cedera.findIndex((obj => obj.id == item.id));
    // console.log('obj',objIndex);
    list_penumpang_cedera[objIndex].alamat = text;

    // console.log(list_penumpang_cedera);
    this.setState({
      list_penumpang_cedera: list_penumpang_cedera
    })

    // console.log('alamat', this.state.list_penumpang_cedera);
  }

  onChangeTextNamaSaksi(item, text) {
    const { list_saksi } = this.state;
    let items = list_saksi;

    objIndex = list_saksi.findIndex((obj => obj.id == item.id));
    // console.log('obj',objIndex);
    list_saksi[objIndex].nama = text;

    // console.log(list_penumpang_cedera);
    this.setState({
      list_saksi: list_saksi
    })
  }

  onChangeTextAlamatSaksi(item, text) {
    const { list_saksi } = this.state;

    let items = list_saksi;


    objIndex = list_saksi.findIndex((obj => obj.id == item.id));
    // console.log('obj',objIndex);
    list_saksi[objIndex].alamat = text;

    // console.log(list_penumpang_cedera);
    this.setState({
      list_saksi: list_saksi
    })

    // console.log('alamat', this.state.list_penumpang_cedera);
  }

  form_penumpang() {
    let outputPenumpang = [];
    const { list_penumpang_cedera } = this.state;

    console.log('ClaimMVPenumpangSaksi', list_penumpang_cedera);

    if (this.state.penumpang_cedera == 'Y') {
      if (list_penumpang_cedera.length != 0) {
        list_penumpang_cedera.forEach(function (item) {
          namaPenumpang = item.nama;
          outputPenumpang.push(
            <View key={item.id}>
              <View style={styles.row}>
                <View style={styles.icon}><Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' /></View>
                <View style={styles.field}>
                  <TextField
                    label='Nama Penumpang'
                    keyboardType='default'
                    onChangeText={(text) => { this.onChangeTextNamaPenumpang(item, text) }}
                    returnKeyType='next'
                    tintColor='#228B22'
                    maxLength={40}
                    value={item.nama}
                  />
                </View>
              </View>
              <View style={styles.row}>
                <View style={styles.icon}><FontAwesome name='address-book' size={25} color='rgba(0, 0, 0, .38)' /></View>
                <View style={styles.field}>
                  <TextField
                    label='Alamat Penumpang'
                    keyboardType='default'
                    onSubmitEditing={this.onSubmitAlamat_penumpang}
                    onChangeText={(text) => { this.onChangeTextAlamatPenumpang(item, text) }}
                    returnKeyType='next'
                    tintColor='#228B22'
                    maxLength={80}
                    value={item.alamat}
                  />
                </View>
              </View>
              <View style={{ borderBottomWidth: 0.5 }}></View>
            </View>
          );
        }.bind(this));

      } else {
        console.log('ClaimMVPenumpangSaksi', '1');

      }

      return (
        <View>
          {outputPenumpang}
          <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
            <View style={styles.button}>
              <RkButton onPress={this.onTambahPenumpang}>Tambah</RkButton>
            </View>
            <View style={styles.button}>
              <RkButton rkType='danger' onPress={this.onKurangPenumpang}>Hapus</RkButton>
            </View>
          </View>
        </View>
      );
    }
    else {

      return null;
    }
  }

  form_saksi() {
    let outputSaksi = [];
    const { list_saksi } = this.state;

    if (this.state.status_saksi == 'Y') {
      if (list_saksi.length != 0) {
        list_saksi.forEach(function (item) {
          namaSaksi = item.saksi;
          outputSaksi.push(
            <View key={item.id}>
              <View style={styles.row}>
                <View style={styles.icon}><Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' /></View>
                <View style={styles.field}>
                  <TextField
                    label='Nama Saksi'
                    keyboardType='default'
                    onChangeText={(text) => { this.onChangeTextNamaSaksi(item, text) }}
                    returnKeyType='next'
                    tintColor='#228B22'
                    maxLength={40}
                    value={item.nama}
                  />
                </View>
              </View>
              <View style={styles.row}>
                <View style={styles.icon}><FontAwesome name='address-book' size={25} color='rgba(0, 0, 0, .38)' /></View>
                <View style={styles.field}>
                  <TextField
                    label='Alamat Saksi'
                    keyboardType='default'
                    onChangeText={(text) => { this.onChangeTextAlamatSaksi(item, text) }}
                    returnKeyType='next'
                    tintColor='#228B22'
                    maxLength={80}
                    value={item.alamat}
                  />
                </View>
              </View>
              <View style={{ borderBottomWidth: 0.5 }}></View>
            </View>
          );
        }.bind(this));

      } else {
        console.log('ClaimMVPenumpangSaksi', '2');

      }
      return (
        <View>
          {outputSaksi}
          <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
            <View style={styles.button}>
              <RkButton onPress={this.onTambahSaksi}>Tambah</RkButton>
            </View>
            <View style={styles.button}>
              <RkButton rkType='danger' onPress={this.onKurangSaksi}>Hapus</RkButton>
            </View>
          </View>
        </View>
      );
    }
    else {

      return null;
    }
  }

  kembali() {
    this.props.navigate('ClaimMVPengemudi');
  }

  lanjut() {

  }

  onKurangPenumpang() {
    let list_penumpang_cedera = this.state.list_penumpang_cedera;

    // console.log(list_penumpang_cedera);

    list_penumpang_cedera.pop();
    //
    this.setState({
      list_penumpang_cedera: list_penumpang_cedera
    });

    // console.log(list_penumpang_cedera);
  }

  onKurangSaksi() {
    let list_saksi = this.state.list_saksi;

    // console.log(list_saksi);

    list_saksi.pop();
    //
    this.setState({
      list_saksi: list_saksi
    });

    // console.log(list_saksi);
  }

  onSubmit() {
    let claim_mv_detail = [];
    let error = 0;
    const { list_penumpang_cedera, list_saksi } = this.state;
    const { dataClaim } = this.props;

    console.log('data claim 3', dataClaim);


    let status_saksi = this.state.status_saksi;
    let penumpang_cedera = this.state.penumpang_cedera;

    if (penumpang_cedera == 'Y') {
      list_penumpang_cedera.forEach(function (item) {
        if (item.alamat == '' || item.nama == '') {
          error = 1;
        }
        temp = {
          'alamat': item.alamat,
          'jenis': item.jenis,
          'nama': item.nama,
        }
        claim_mv_detail.push(temp);
      });
    }

    if (status_saksi == 'Y') {
      list_saksi.forEach(function (item) {
        if (item.alamat == '' || item.nama == '') {
          error = 1;
        }
        temp = {
          'alamat': item.alamat,
          'jenis': item.jenis,
          'nama': item.nama,
        }
        claim_mv_detail.push(temp);
      });
    }

    temp = {
      claim_mv_detail: claim_mv_detail,
      status_saksi: status_saksi,
      penumpang_cedera: penumpang_cedera,
      list_penumpang_cedera: list_penumpang_cedera,
      list_saksi: list_saksi
    }
    // console.log(temp);

    if (error == 1) {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    } else {
      this.props.setDataClaimMV(temp);
      this.props.navigate('ClaimMVPolisi');
    }
    // Alert.alert('test');
  }

  onTambahPenumpang() {
    let list_penumpang_cedera = this.state.list_penumpang_cedera;
    let count = list_penumpang_cedera.length + 1;

    // console.log(count);
    list_penumpang_cedera.push(
      {
        alamat: '',
        jenis: 'P',
        nama: '',
        id: count,
      }
    );

    this.setState({
      list_penumpang_cedera: list_penumpang_cedera
    });

    // console.log(this.state.list_penumpang_cedera);
  }

  onTambahSaksi() {
    let list_saksi = this.state.list_saksi;
    let count = list_saksi.length + 1;

    // console.log(count);
    list_saksi.push(
      {
        alamat: '',
        jenis: 'S',
        nama: '',
        id: count,
      }
    );

    this.setState({
      list_saksi: list_saksi
    });

  }

  render() {
    const { dataClaim } = this.props;
    console.log('data claim 3', dataClaim);
    // console.log(this.state);
    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'Incident Form 3 dari 6',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.nama_pengemudiRef, this.alamat_pengemudiRef, this.no_sim_pengemudiRef];
          }}
        >
          <ScrollView style={styles.container}>
            <View style={styles.row}>
              <View style={styles.text}>
                <Text>Apakah ada penumpang mengalami cedera?</Text>
                <RadioForm
                  formHorizontal={true}
                  animation={true}
                  initial={1}
                >
                  {radio_penumpang_cedera.map((obj, i) => {
                    var onPress = (value, index) => {
                      this.setState({
                        penumpang_cederaIndex: index,
                        penumpang_cedera: value,

                      })

                      if (value == 'N') {
                        list_penumpang_cedera = [];

                        this.setState({
                          list_penumpang_cedera: list_penumpang_cedera,
                        });
                      }
                    }
                    return (
                      <RadioButton labelHorizontal={true} key={i} >
                        {/*  You can set RadioButtonLabel before RadioButtonInput */}
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={this.state.penumpang_cederaIndex === i}
                          onPress={onPress}
                          buttonStyle={{}}
                          buttonWrapStyle={{ marginRight: 10 }}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          onPress={onPress}
                          labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                        />
                      </RadioButton>
                    )
                  })}
                </RadioForm>
              </View>
            </View>
            {this.form_penumpang()}
            <View style={styles.row}>
              <View style={styles.text}>
                <Text>Apakah ada saksi saat kejadian?</Text>
                <RadioForm
                  formHorizontal={true}
                  animation={true}
                  initial={1}
                >
                  {radio_penumpang_cedera.map((obj, i) => {
                    var onPress2 = (value, index) => {
                      this.setState({
                        status_saksiIndex: index,
                        status_saksi: value,
                      })

                      if (value == 'N') {
                        list_saksi = [];

                        this.setState({
                          list_saksi: list_saksi
                        });
                      }
                    }
                    return (
                      <RadioButton labelHorizontal={true} key={i} >
                        {/*  You can set RadioButtonLabel before RadioButtonInput */}
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={this.state.status_saksiIndex === i}
                          onPress={onPress2}
                          buttonStyle={{}}
                          buttonWrapStyle={{ marginRight: 10 }}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          onPress={onPress2}
                          labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                        />
                      </RadioButton>
                    )
                  })}
                </RadioForm>
              </View>
            </View>
            {this.form_saksi()}
            <View style={styles.button}>
              <RkButton rkType='danger' onPress={this.onSubmit}>Lanjut</RkButton>

            </View>
          </ScrollView>

        </KeyboardAwareScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    margin: 10,
  },
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVPenumpangSaksi1);
