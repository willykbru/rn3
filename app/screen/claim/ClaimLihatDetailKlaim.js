import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, ActivityIndicator, Text, Modal, TouchableHighlight } from 'react-native';

import { Header } from 'react-native-elements';
import { Container, Content, Tab, Tabs, TabHeading, Icon, Button, Footer, Card, CardItem, Body } from 'native-base';




import Icon2 from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import DialogAndroid from 'react-native-dialogs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

class ClaimLihatDetailKlaim1 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
        };
        // console.log(claim_number);

        this.setModalVisible = this.setModalVisible.bind(this);
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    handleBackPress = () => {
        //this.goBack(); // works best when the goBack is async
        this.props.navigate('ClaimLihatKlaim');
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        const { claim_number } = this.props;
        
        this.props.getClaimStatusDetail(claim_number);
        // console.log(claim_number);
    }

    componentWillUnmount() {

    }

    componentDidUpdate() {

    }

    componentWillReceiveProps(NextProps) {
        if (this.props.claim_status_detail != NextProps.claim_status_detail) {
            this.forceUpdate();
        }
    }

    kembali() {
        this.props.navigate('ClaimLihatKlaim');
    }

    onSubmit() {

    }

    render_claim_status_detail() {

        let output_status_detail = [];

        const { claim_status_detail } = this.props;
        // console.log('render 2', claim_status_detail);
        if (claim_status_detail.length > 0) {

            claim_status_detail.forEach(function (item) {
                console.log('item',item);
                if (item.remarks != null && item.remarks != "") {
                    
                    output_status_detail.push(
                        <Card key={item.input_date}>
                            <CardItem style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }} bordered>
                                <View>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{item.claim_status_name}</Text>
                                </View>
                            </CardItem>
                            <CardItem style={{ justifyContent: 'center', alignItems: 'center' }} bordered>
                                <Text style={{ textAlign: 'center' }}>{item.remarks}</Text>
                            </CardItem>
                           
                            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                                <Text style={{ textAlign: 'right' }}>Tanggal Update</Text>
                                <Text style={{ textAlign: 'right' }}>{item.input_date}</Text>
                            </View>
                            

                        </Card>
                    );
                } else {
                    output_status_detail.push(
                        <Card key={item.input_date}>
                            <CardItem  style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }} bordered >
                                <View>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{item.claim_status_name}</Text>
                                </View>
                            </CardItem>
                            <CardItem style={{ justifyContent: 'center', alignItems: 'center' }} bordered>
                                <Text style={{ textAlign: 'center' }}>Tidak ada catatan</Text>
                            </CardItem>
                           
                            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                                <Text style={{ textAlign: 'right' }}>Tanggal Update</Text>
                                <Text style={{ textAlign: 'right' }}>{item.input_date}</Text>
                            </View>
                            

                        </Card>
                    );
                }

            }.bind(this));

            return (

                <ScrollView style={[styles.card, {height: '100%'-70, marginBottom: 15}]}>
                    <Card>
                        <CardItem style={{ justifyContent: 'center', alignItems: 'center' }} bordered>
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 10, marginBottom: 10 }}>
                                <Text style={{ textAlign: 'center', fontWeight:'bold' }}>{claim_status_detail[0].claim_number}</Text>
                            </View>
                        </CardItem>
                    </Card>
                    {output_status_detail}
                </ScrollView>

            );
        }
        return (
            <View style={{ padding: 20 }}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );



    }

    render() {
        const { claim_number } = this.props;
        const { height, width } = Dimensions.get('window');
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 52 }}>
                    <Header
                        leftComponent={
                            <TouchableOpacity onPress={() => { this.kembali() }}>
                                <Icon2 name='md-arrow-round-back' size={25} color='#FFF' />
                            </TouchableOpacity>
                        }
                        centerComponent={{
                            text: 'Detail Klaim',
                            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                        }}
                        outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                    />


                </View>
                <View style={{ height: height - 53 }}>
                    <Container>
                        <Content>
                            {this.render_claim_status_detail()}


                        </Content>
                    </Container>
                </View>

            </View>

        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eceff1',
    },
    tabbar: {
        backgroundColor: '#3f51b5',
    },
    tab: {
        width: 120,
    },
    indicator: {
        backgroundColor: '#ffeb3b',
    },
    label: {
        color: '#fff',
        fontWeight: '400',
    },
    card: {
        paddingLeft: 10,
        paddingRight: 10,
    },
});

function mapStateToProps(state) {
    return {
        claim_number: state.claim_number,
        claim_status_detail: state.claim_status_detail,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimLihatDetailKlaim1);