/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import DialogAndroid from 'react-native-dialogs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');


class ClaimMVInformasi1 extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    // this.props.navigate('ClaimMVPengemudi');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    const { dataTransaksiKlaimMV } = this.props;
    if (dataTransaksiKlaimMV.claim_no == null) {
      // console.log('kosong');
      DialogAndroid.showProgress('Sedang diproses ...', {
        // style: DialogAndroid.progressHorizontal // omit this to get circular
      });
    }

  }

  componentWillUnmount() {

  }

  kembali() {
    // this.props.navigate('Main');
  }

  lanjut() {

  }

  onSubmit() {
    this.props.resetDataTransaksiKlaimMV();
    this.props.navigate('Main');
    // Alert.alert('test');
  }

  render() {

    const { dataTransaksiKlaimMV } = this.props;
    // console.log('informasi', loadingStateKlaim);
    // console.log('dataclaimall',dataClaimAll);
    // console.log('dataclaim informasi',dataClaim);
    // console.log('data klaim mv', dataTransaksiKlaimMV);
    // console.log(loadingStateKlaim);


    if (dataTransaksiKlaimMV.claim_no == null) {
      // console.log('kosong');
      // DialogAndroid.showProgress('Sedang diproses ...', {
      //   // style: DialogAndroid.progressHorizontal // omit this to get circular
      // });

      return (
        <View>
          <Header

            centerComponent={{
              text: 'Informasi Klaim',
              style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
            }}
            outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
          />

        </View>
      );
    } else if (dataTransaksiKlaimMV.claim_no != null) {
      this.props.resetClaimMVAll();
      setTimeout(DialogAndroid.dismiss, 0);

      return (
        <View>
          <Header

            centerComponent={{
              text: 'Informasi Klaim',
              style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
            }}
            outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
          />

          <ScrollView style={styles.container}>


            <View style={{ alignItems: 'center' }}><Text style={{ textAlign: 'center' }}>Anda telah melakukan proses klaim dan mendapatkan no klaim sebagai berikut:</Text></View>
            <View style={{ alignItems: 'center' }}><Text style={{ textAlign: 'center' }}>{dataTransaksiKlaimMV.claim_no}</Text></View>


            <View style={styles.button}>
              <RkButton rkType='danger' onPress={this.onSubmit}>Lanjut</RkButton>
            </View>
          </ScrollView>


        </View>
      );
    }

  }
}

function mapStateToProps(state) {
  return {

    dataTransaksiKlaimMV: state.dataTransaksiKlaimMV,

    loadingStateKlaim: state.loadingStateKlaim,

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVInformasi1);
