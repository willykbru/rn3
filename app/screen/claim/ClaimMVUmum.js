/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');

class ClaimMVUmum1 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      namaLengkap: this.props.detailPolis.nama_tertanggung,
      email: this.props.detailPolis.email_tertanggung,
      noPolisi: this.props.detailPolis.no_plat,
      kecepatanInsiden: this.props.dataClaim.kecepatanInsiden,
      deskripsiInsiden: this.props.dataClaim.deskripsiInsiden,
      alamatKejadian: this.props.dataClaim.alamatKejadian,

      visibleTipeInsiden: false,
      pickedTipeInsiden: null,
      labelTipeInsiden: this.props.dataClaim.labelTipeInsiden,

      visibleLokasiProvinsi: false,
      pickedLokasiProvinsi: null,
      labelProvinsiInsiden: this.props.dataClaim.labelProvinsiInsiden,

      visibleLokasiKota: false,
      pickedLokasiKota: null,
      labelKotaInsiden: this.props.dataClaim.labelKotaInsiden,

      tanggalSekarang: new Date(),
      pickedTanggal: this.props.dataClaim.tanggalKejadian,
      isPickedTanggal: false,

      pickedJam: this.props.dataClaim.jamInsiden,
      jamSekarang: new Date().getHours() + ":" + new Date().getMinutes(),
      isPickedJam: false,

    };

    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.noPolisRef = this.updateRef.bind(this, 'noPolis');
    this.alamatKejadianRef = this.updateRef.bind(this, 'alamatKejadian');
    this.kecepatanInsidenRef = this.updateRef.bind(this, 'kecepatanInsiden');
    this.deskripsiInsidenRef = this.updateRef.bind(this, 'deskripsiInsiden');
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('Main');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    this.props.getProvinsi();
    this.props.getTipeInsiden();

    if (this.props.claimState.provinsiInsiden && this.props.claimState.provinsiInsiden != 'Pilih') {
      this.props.getDistrict(this.props.claimState.provinsiInsiden);
    }
  }

  componentWillUnmount() {

  }

  kembali() {
    this.props.navigate('Main');
  }

  lanjut() {

  }

  onChangeText(text) {
    try {
      ['alamatKejadian', 'kecepatanInsiden', 'deskripsiInsiden']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onSelectLokasiProvinsi(key, label) {
    this.setState({
      pickedLokasiProvinsi: key,
      labelProvinsiInsiden: label,
      visibleLokasiProvinsi: false,
      labelKotaInsiden: null,
    });

    this.props.getDistrict(key);
    //console.log(this.props.getDistrict(key));
  }

  onShowTipeInsiden() {
    this.setState({ visibleTipeInsiden: true });
  }

  onSelectTipeInsiden(picked, label) {
    // console.log('picked ',picked);
    this.setState({
      pickedTipeInsiden: picked,
      labelTipeInsiden: label,
      visibleTipeInsiden: false,
    })
  }

  onCancelTipeInsiden() {
    this.setState({
      visibleTipeInsiden: false,
    })
  }

  onShowLokasiProvinsi() {
    this.setState({
      visibleLokasiProvinsi: true,
    });
  }

  onCancelLokasiProvinsi() {
    this.setState({
      visibleLokasiProvinsi: false,
    })
  }

  onSelectLokasiKota(key, label) {
    this.setState({
      pickedLokasiKota: key,
      labelKotaInsiden: label,
      visibleLokasiKota: false,
    });

  }

  onShowLokasiKota() {
    this.setState({
      visibleLokasiKota: true,
    })
  }

  onCancelLokasiKota() {
    this.setState({
      visibleLokasiKota: false,
    })
  }

  onSubmit() {
    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error

    const { detailPolis, dataPolis } = this.props;



    ['alamatKejadian', 'kecepatanInsiden', 'deskripsiInsiden']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Tidak boleh kosong';
          isError = 1;
        }
      });

    this.setState({ errors });
    // console.log('errors', errors);

    // console.log('isError',isError);
    // console.log('picked jam',this.state.pickedJam);
    console.log('picked labelKotaInsiden', this.state.labelKotaInsiden);
    if (isError == 0 && this.state.pickedJam != '' && this.state.pickedTanggal != '' && this.state.labelTipeInsiden != 'Pilih Tipe Insiden' && this.state.labelTipeInsiden != 'Pilih' && this.state.labelProvinsiInsiden != 'Pilih Provinsi Insiden' && this.state.labelProvinsiInsiden != 'Pilih' && this.state.labelKotaInsiden != 'Pilih Kota Insiden' && this.state.labelKotaInsiden != null && this.state.labelKotaInsiden != 'Pilih') {
      console.log('state 1 ', this.state);
      temp = {
        alamat_tertanggung: detailPolis.alamat_tertanggung,
        email_tertanggung: this.state.email,
        insurance_id: 'INS005',
        nama_tertanggung: this.state.namaLengkap,
        no_ktp: detailPolis.no_ktp,
        alamatKejadian: this.state.alamatKejadian,
        phone_tertanggung: detailPolis.phone_tertanggung,
        policy_enddate: detailPolis.policy_enddate,
        policy_no: detailPolis.policy_no,
        tanggal_kejadian: this.state.pickedTanggal + ' ' + this.state.pickedJam + ':00',
        tipe_kejadian: this.state.pickedTipeInsiden,
        deskripsiInsiden: this.state.deskripsiInsiden,
        kecepatanInsiden: this.state.kecepatanInsiden,
        labelTipeInsiden: this.state.labelTipeInsiden,
        labelKotaInsiden: this.state.labelKotaInsiden,
        labelProvinsiInsiden: this.state.labelProvinsiInsiden,
        tanggalKejadian: this.state.pickedTanggal,
        jamInsiden: this.state.pickedJam,
        nomorMesin: detailPolis.no_mesin,
        nomorPolisi: detailPolis.no_plat,
        nomorRangka: detailPolis.no_rangka,
        merek_kendaraan: detailPolis.merek_kendaraan,
        seri_kendaraan: detailPolis.seri_kendaraan,
        tahun_kendaraan: detailPolis.tahun_kendaraan,

      }
      this.props.setDataClaimMV(temp);

      this.props.navigate('ClaimMVPengemudi');
    } else {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    }
  }

  onSubmitAlamatKejadian() {

  }

  onSubmitKecepatanInsiden() {

  }

  onSubmitDeskripsiInsiden() {

  }

  updateJam(time) {
    this.setState({
      pickedJam: time,
      isPickedJam: true,
    });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  updateTanggal(tanggal) {
    this.setState({
      pickedTanggal: tanggal,
      isPicked: true,
    });
  }

  render() {
    let { errors = {}, ...data } = this.state;
    let { alamatKejadian = null, kecepatanInsiden = null, deskripsiInsiden = null } = data;
    const { claimState, claimLabelState, dataClaim, detailPolis, tipeInsiden, dataPolis } = this.props;
    const optionsProvinsi = this.props.listProvinsi;
    const optionsTipeInsiden = this.props.listTipeInsiden;
    const optionsDistrict = this.props.listDistrict;

    // console.log(dataClaim);
    // console.log('detail polis', detailPolis);
    // console.log('data polis', dataPolis);
    // console.log('data claim', dataClaim);

    // console.log('state 1 ',this.state);
    const {
      visibleTipeInsiden,
      isPickedTanggal,
      isPickedJam,
      pickedTanggal,
      pickedJam,
      pickedTipeInsiden,
      pickedLokasiKota,
      pickedLokasiProvinsi,
      visibleLokasiKota,
      visibleLokasiProvinsi,
    } = this.state;

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var maxDate = new Date(year + 10, 12, 31);
    var minDate = new Date(year - 1, 1, 1);

    // console.log('Detail Polis ', JSON.stringify(detailPolis));

    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'Incident Form 1 dari 6',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.noPolisRef, this.alamatKejadianRef, this.kecepatanInsidenRef, this.deskripsiInsidenRef];
          }}
        >
          <ScrollView style={styles.container}>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-list' size={40} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.noPolisRef}
                  value={detailPolis.policy_no}
                  label='No Polis'
                  keyboardType='default'
                  tintColor='#228B22'
                  editable={false}
                  textColor='rgba(0,0,0, .38)'
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.nama_tertanggungRef}
                  value={detailPolis.nama_tertanggung}
                  label='Nama Tertanggung'
                  keyboardType='default'
                  tintColor='#228B22'
                  editable={false}
                  textColor='rgba(0,0,0, .38)'
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-car' size={30} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.no_platRef}
                  value={detailPolis.no_plat}
                  label='No Plat'
                  keyboardType='default'
                  tintColor='#228B22'
                  editable={false}
                  textColor='rgba(0,0,0, .38)'
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='calendar' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.fieldTanggal}>
                <DatePicker
                  style={{ width: wp('80%'), }}
                  placeholder="Pilih Tanggal Kejadian (DD-MM-YYYY)"
                  mode="date"
                  date={pickedTanggal == null ? null : this.state.pickedTanggal}
                  androidMode="spinner"
                  format="DD-MM-YYYY"
                  minDate={minDate}
                  maxDate={maxDate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {

                    },
                    dateInput: {
                      borderWidth: 0,
                      alignItems: 'flex-start',
                      paddingTop: 15,
                      borderBottomWidth: .5,
                    },
                    dateText: {
                      fontSize: 18,
                      //color: 'rgba(0,0,0, .38)'
                    }

                    // ... You can check the source to find the other keys.
                  }}
                  showIcon={false}
                  onDateChange={(tanggal) => { this.updateTanggal(tanggal) }}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='clock-o' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.fieldTanggal}>
                <DatePicker
                  style={{ width: wp('80%') }}
                  mode="time"
                  date={pickedJam == null ? null : this.state.pickedJam}
                  androidMode="spinner"
                  placeholder="Pilih Jam Kejadian"
                  format="HH:mm"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  ref='datepicker1'
                  customStyles={{
                    dateIcon: {

                    },
                    dateInput: {
                      alignItems: 'flex-start',
                      borderWidth: 0,
                      borderBottomWidth: .5,
                      paddingTop: 15,
                    },
                    dateText: {
                      fontSize: 18,
                      // color: 'rgba(0,0,0, .38)',
                    }
                  }}
                  minuteInterval={10}
                  onDateChange={(datetime) => { this.updateJam(datetime) }}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='address-book' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.alamatKejadianRef}
                  label='Alamat Kejadian'
                  keyboardType='default'
                  onFocus={this.onFocus}
                  onSubmitEditing={this.onSubmitAlamatKejadian}
                  onChangeText={this.onChangeText}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.alamatKejadian}
                  maxLength={80}
                  value={this.state.alamatKejadian}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='map-marker' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TouchableOpacity onPress={() => this.onShowLokasiProvinsi()}>
                  <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelProvinsiInsiden == null ? 'Lokasi Insiden (Provinsi)' : this.state.labelProvinsiInsiden}</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                  visible={visibleLokasiProvinsi}
                  onSelect={(key, label) => this.onSelectLokasiProvinsi(key, label)}
                  onCancel={() => this.onCancelLokasiProvinsi()}
                  options={optionsProvinsi}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}>{/*kosong */}</View>
              <View style={styles.field}>
                <TouchableOpacity onPress={() => this.onShowLokasiKota()}>
                  <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelKotaInsiden == null ? 'Lokasi Insiden (Kota)' : this.state.labelKotaInsiden}</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                  visible={visibleLokasiKota}
                  onSelect={(key, label) => this.onSelectLokasiKota(key, label)}
                  onCancel={() => this.onCancelLokasiKota()}
                  options={optionsDistrict}
                />
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-speedometer' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Kecepatan Ketika Insiden (KM/jam)'
                  keyboardType='numeric'
                  ref={this.kecepatanInsidenRef}
                  maxLength={3}
                  value={this.state.kecepatanInsiden}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitKecepatanInsiden}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.kecepatanInsiden}
                  onFocus={this.onFocus}
                />
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='list' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TouchableOpacity onPress={() => this.onShowTipeInsiden()}>
                  <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelTipeInsiden == null ? 'Pilih Tipe Insiden' : this.state.labelTipeInsiden}</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                  visible={visibleTipeInsiden}
                  onSelect={(picked, label) => this.onSelectTipeInsiden(picked, label)}
                  onCancel={() => this.onCancelTipeInsiden()}
                  options={optionsTipeInsiden}
                />
              </View>
            </View>

            <View style={styles.row}>
              <View style={styles.field}>
                <TextField
                  label='Deskripsi Insiden (max. 500 karakter)'
                  value={this.state.deskripsiInsiden}
                  keyboardType='default'
                  ref={this.deskripsiInsidenRef}
                  maxLength={40}
                  onSubmitEditing={this.onSubmitDeskripsiInsiden}
                  onChangeText={this.onChangeText}
                  returnKeyType='next'
                  tintColor='#228B22'
                  multiline={true}
                  numberOfLines={4}
                  maxLength={500}
                  style={styles.textField1}
                  error={errors.deskripsiInsiden}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.button}>

              <RkButton rkType='danger button1' onPress={this.onSubmit}>Lanjut</RkButton>

            </View>
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  fieldTanggal: {
    flex: 9,
    paddingLeft: 10,
    paddingBottom: 15
  },
  textPicker: {
    height: 56,
    fontSize: 16,
    color: 'rgba(0,0,0, .87)',
    borderBottomWidth: .5,
    borderColor: 'rgba(0,0,0, .38)',
    paddingTop: 20,
  },
  button: {
    alignItems: 'center',
  }
});


function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    listProvinsi: state.listProvinsi,
    listDistrict: state.listDistrict,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listTipeInsiden: state.listTipeInsiden,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVUmum1);
