/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import ImagePicker from 'react-native-image-crop-picker';
import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');

const radio_lapor_polisi = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
]

class ClaimMVPolisi1 extends Component {
  constructor(props) {
    super(props);


    this.state = {
      posisi_kendaraan: this.props.dataClaim.posisi_kendaraan,
      keadaan_kendaraan: this.props.dataClaim.keadaan_kendaraan,
      jumlah_taksiran: this.props.dataClaim.estimasi_kerusakan,
      lapor_polisi: this.props.dataClaim.lapor_polisi,
      lapor_polisiIndex: (this.props.dataClaim.lapor_polisi == 'Y') ? 0 : 1,
    }


    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitLaporanPolisi = this.onSubmitLaporanPolisi.bind(this);

    this.onFocus = this.onFocus.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.keadaanKendaraanRef = this.updateRef.bind(this, 'keadaan_kendaraan');
    this.posisiKendaraanRef = this.updateRef.bind(this, 'posisi_kendaraan');
    this.jumlahTaksiranRef = this.updateRef.bind(this, 'jumlah_taksiran');
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('ClaimMVPenumpangSaksi');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {

  }

  kembali() {
    this.props.navigate('ClaimMVPenumpangSaksi');
  }

  async konfirmasiPolisi(path) {
    // kosong
  }

  lanjut() {

  }

  laporan_polisi() {
    const { listDataLaporanPolisi } = this.props;
    // console.log(listDataLaporanPolisi);
    let { errors = {}, ...data } = this.state;


    if (this.state.lapor_polisi == 'Y') {
      return (
        <View>

          <View style={styles.row}>
            <View style={styles.fieldImage}>
              {
                listDataLaporanPolisi[0].path == null ?

                  <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={this.onSubmitLaporanPolisi} >

                    <Image source={require('../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} />

                  </TouchableOpacity>

                  :

                  <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => { this.konfirmasiPolisi(listDataLaporanPolisi[0].path).done() }} >
                    <Image source={{ uri: listDataLaporanPolisi[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                  </TouchableOpacity>

              }

            </View>
            <View style={styles.fieldButton}>
              <RkButton rkType='danger' onPress={this.onSubmitLaporanPolisi}>Unggah Foto Laporan Polisi</RkButton>
            </View>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }

  onChangeText(text) {
    try {
      ['posisi_kendaraan', 'keadaan_kendaraan', 'jumlah_taksiran']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onPressRadioLaporPolisi(value) {
    this.setState({ lapor_polisi: value })
  }


  onSubmit() {
    const { listDataLaporanPolisi } = this.props;

    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error


    //console.log('detailPolis2', detailPolis);
    ['posisi_kendaraan', 'keadaan_kendaraan', 'jumlah_taksiran']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Tidak boleh kosong';
          isError = 1;
        }
      });

    this.setState({ errors });
    // console.log('errors', errors);

    // console.log('isError',isError);
    if (isError == 0) {
      // console.log(this.state);

      if (this.state.lapor_polisi == 'N') {
        file_foto_laporan_polisi = '';

        temp = {
          lapor_polisi: this.state.lapor_polisi,
          file_foto_laporan_polisi: file_foto_laporan_polisi,
          estimasi_kerusakan: this.state.jumlah_taksiran,
          keadaan_kendaraan: this.state.keadaan_kendaraan,
          posisi_kendaraan: this.state.posisi_kendaraan,

        }
        this.props.setDataClaimMV(temp);

        this.props.navigate('ClaimMVPihakKetiga');
      } else {
        if (listDataLaporanPolisi[0].path != null) {
          RNFS.readFile(listDataLaporanPolisi[0].path.substring(7), 'base64').then((result) => {


            // file_foto_laporan_polisi = listDataLaporanPolisi[0].path;
            file_foto_laporan_polisi = result;


            temp = {
              lapor_polisi: this.state.lapor_polisi,
              file_foto_laporan_polisi: file_foto_laporan_polisi,
              estimasi_kerusakan: this.state.jumlah_taksiran,
              keadaan_kendaraan: this.state.keadaan_kendaraan,
              posisi_kendaraan: this.state.posisi_kendaraan,

            }
            this.props.setDataClaimMV(temp);

            this.props.navigate('ClaimMVPihakKetiga');
          });
        } else {
          Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
        }
      }
    } else {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    }

    // Alert.alert('test');
  }



  onSubmitLaporanPolisi() {
    const { listDataLaporanPolisi } = this.props;

    let imageLaporanPolisi = [];

    ImagePicker.openCamera({
      cropping: false,
      includeExif: false,
      avoidEmptySpaceAroundImage: true,

    }).then(image => {
      //  console.log('received image', image.path);
      ImageResizer.createResizedImage(image.path, image.width * 0.4, image.width * 0.4, 'JPEG', 80, 0).then((response) => {

        image = {
          'size': response.size,
          'path': response.uri,
          'caption': '',
        };

        this.props.setDataGambarLaporanPolisiKameraTemp(image);

      });

    }).catch(e => alert(e));
  }

  onSubmitJumlah_taksiran() {

  }

  onSubmitKeadaan_kendaraan() {

  }

  onSubmitPosisi_kendaraan() {

  }

  onSubmitTempat_pelaporan_polisi() {

  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    // console.log(this.state.lapor_polisi);
    const { dataClaim, listDataLaporanPolisi } = this.props;
    console.log('data claim 4', dataClaim);
    console.log('listDataLaporanPolisi 4', listDataLaporanPolisi);


    let { errors = {}, ...data } = this.state;
    let { posisi_kendaraan = null, keadaan_kendaraan = null, jumlah_taksiran = null } = data;

    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'Incident Form 4 dari 6',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.tempatPelaporanPolisiRef, this.posisiKendaraanRef, this.keadaanKendaraanRef, this.jumlahTaksiranRef];
          }}
        >
          <ScrollView style={styles.container}>
            <View style={styles.row}>
              <View style={styles.text}>
                <Text>Apakah kejadian ini sudah dilaporkan polisi?</Text>
                <RadioForm
                  formHorizontal={true}
                  animation={true}
                  initial={1}
                >
                  {radio_lapor_polisi.map((obj, i) => {
                    var onPress = (value, index) => {
                      this.setState({
                        lapor_polisiIndex: index,
                        lapor_polisi: value,
                      });

                      if (value == 'N') {
                        this.props.hapusDataGambarLaporanPolisiKamera();
                      }
                    }
                    return (
                      <RadioButton labelHorizontal={true} key={i} >
                        {/*  You can set RadioButtonLabel before RadioButtonInput */}
                        <RadioButtonInput
                          obj={obj}
                          index={i}
                          isSelected={this.state.lapor_polisiIndex === i}
                          onPress={onPress}
                          buttonStyle={{}}
                          buttonWrapStyle={{ marginRight: 10 }}
                        />
                        <RadioButtonLabel
                          obj={obj}
                          index={i}
                          onPress={onPress}
                          labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                        />
                      </RadioButton>
                    )
                  })}
                </RadioForm>
              </View>
            </View>
            {this.laporan_polisi()}
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='map-marker' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Posisi Kendaraan Saat Ini'
                  keyboardType='default'
                  ref={this.posisiKendaraanRef}
                  maxLength={80}
                  value={this.state.posisi_kendaraan}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitPosisi_kendaraan}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.posisi_kendaraan}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='car' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Keadaan Kendaraan'
                  keyboardType='default'
                  ref={this.keadaanKendaraanRef}
                  maxLength={80}
                  value={this.state.keadaan_kendaraan}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitKeadaan_kendaraan}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.keadaan_kendaraan}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Foundation name='dollar-bill' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Taksiran harga dari kerusakan kendaraan'
                  keyboardType='numeric'
                  ref={this.jumlahTaksiranRef}
                  value={this.state.jumlah_taksiran}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitJumlah_taksiran}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.jumlah_taksiran}
                  onFocus={this.onFocus}
                  maxLength={10}
                />
              </View>
            </View>
            <View style={styles.button}>

              <RkButton rkType='danger' onPress={this.onSubmit}>Lanjut</RkButton>

            </View>
          </ScrollView>

        </KeyboardAwareScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listDataLaporanPolisi: state.listDataLaporanPolisi,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  fieldButton: {
    width: (width * 0.6) - 20,
    height: width * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: .5,
  },
  fieldImage: {
    width: width * 0.4,
    height: width * 0.4,
    borderWidth: .5,

  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVPolisi1);
