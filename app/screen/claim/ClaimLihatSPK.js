import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';
import PDFView from 'react-native-view-pdf';
import Pdf from 'react-native-pdf';

import { Header } from 'react-native-elements';

import Icon2 from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import RNFetchBlob from 'react-native-fetch-blob';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';


class ClaimLihatSPK1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title : null,
            url : null
        }
    }

    handleBackPress = () => {
        //this.goBack(); // works best when the goBack is async
        this.kembali();
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        
    }

    componentWillMount(){
        // this.props.resetLinkSPK();
        const { linkSPK } = this.props;
        console.log('linkSPK Will Mount', linkSPK);
        if (linkSPK.title == "SPK") {
            this.setState({ 
                title: 'SPK',
                url: linkSPK.items.file_spk, 
            });            
        } else if (linkSPK.title == "SPK Rev. 1") {        
            console.log('test');    
            this.setState({ 
                title: 'SPK Rev. 1',
                url: linkSPK.items.file_spk2,
         });
        } else if (linkSPK.title == "SPK Rev. 2") {
            this.setState({ 
                title: 'SPK Rev. 2',
                url: linkSPK.items.file_spk3,
        });
        } else {
            this.setState({ title: null, url : null });
        }
    }

    componentDidUpdate() {

    }

    shouldComponentUpdate(nextProps, nextState){
        if(this.state.title != nextState.title){
            return true;
        } 

        // if (this.state.url != nextState.url) {
        //     console.log('k');
        //     return true;
        // }

        return true;

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillUnmount(){
        this.props.resetLinkSPK();
    }

    kembali() {
        this.props.navigate('ClaimLihatKlaim');
    }

    onDownload(url){
        // console.log('url', url);
        const {linkSPK} = this.props;
        const dirs = RNFetchBlob.fs.dirs;
        // console.log(dirs.SDCardApplicationDir);
        
        const path = dirs.SDCardApplicationDir + '/files/' + linkSPK.items.claim_number + '_' + linkSPK.title + '.pdf';

        RNFetchBlob
            .config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache: false,
                addAndroidDownloads: {
                    useDownloadManager: true,
                    path,
                    title: linkSPK.items.claim_number + '_' + linkSPK.title + '.pdf',
                    description: 'SPK Berhasil di download',
                    mime: 'application/pdf',
                    notification: false,
                }
            })
            .fetch('GET', url, {
                //some headers ..
            })
            .then((res) => {
                // the temp file path
                console.log('The file saved to ', res.path());
            })
            .catch((errorMessage, statusCode) => {
                // error handling
                console.log('error',errorMessage);
            })
        
    }

    render(){
        const { linkSPK} = this.props;
        // console.log('linkSPK',linkSPK);
        // console.log(this.state);
        let url;
        if(linkSPK.title=='SPK'){
            url = linkSPK.items.file_spk;            
        } else if (linkSPK.title == 'SPK Rev. 1'){
            url = linkSPK.items.file_spk2;
        } else if (linkSPK.title == 'SPK Rev. 2'){
            url = linkSPK.items.file_spk3;
        } else {
            url = '';
        }
        console.log(url);
        const source = { uri: this.state.url, cache: true };
        // const source = { uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf', cache: true };
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};

        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,..."};

        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 52 }}>
                    <Header
                        leftComponent={
                            <TouchableOpacity onPress={() => { this.kembali() }}>
                                <Icon2 name='md-arrow-round-back' size={25} color='#FFF' />
                            </TouchableOpacity>
                        }
                        centerComponent={{
                            text: linkSPK.items.claim_number,
                            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                        }}
                        outerContainerStyles={{ backgroundColor: '#EE0000', height: 53 }}
                    />

                </View>
                <View style={styles.container}>
                    <Pdf
                        source={source}
                        onLoadComplete={(numberOfPages, filePath) => {
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page, numberOfPages) => {
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error) => {
                            console.log(error);
                        }}
                        style={styles.pdf} />
                </View>
                <View style={{height: 52, justifyContent:'center', alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>this.onDownload(url)}>
                        <Text style={{ textDecorationLine: 'underline', textAlign: 'center', color: '#536dfe' }}>Unduh {this.state.title}</Text>                    
                    </TouchableOpacity>                    
                </View>
            </View>
            
        )
    }
}

function mapStateToProps(state) {
    return {
        linkSPK: state.linkSPK,

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimLihatSPK1);