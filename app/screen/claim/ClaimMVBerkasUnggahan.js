/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';
import ImagePicker from 'react-native-image-crop-picker';
import DialogAndroid from 'react-native-dialogs';

const { width, height } = Dimensions.get('window');

class ClaimMVBerkasUnggahan1 extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.buttonTambah = this.buttonTambah.bind(this);
    this.kirim = this.kirim.bind(this);
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('ClaimMVPihakKetiga');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    
  }

  componentDidUpdate() {
    const { loadingStateKlaim } = this.props;
    // console.log('did update',loadingStateKlaim);

  }


  componentWillReceiveProps(nextProps) {
    const { loadingStateKlaim } = this.props;
    if (this.props.loadingStateKlaim != nextProps.loadingStateKlaim) {
      // console.log('will receive props',loadingStateKlaim);
    }
  }

  shouldComponentUpdate(nextProps) {
    // console.log('shouldComponentUpdate');
    return this.props.loadingStateKlaim != nextProps.loadingStateKlaim;
  }

  buttonTambah() {
    const { listDataGambar } = this.props;

    if (listDataGambar.length < 8) {
      return (
        <TouchableOpacity onPress={() => { this.tambahFoto() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: 'transparent' }}>
          <Image source={require('../../assets/icons/component/plus.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
        </TouchableOpacity>
      );
    }
  }



  kembali() {
    this.props.navigate('ClaimMVPihakKetiga');
  }

  async kirim() {
    const { listDataGambar } = this.props;
    // console.log(listDataGambar);
    claim_photo = [];

    for (const item of listDataGambar) {
      const contents = await RNFS.readFile(item.path.substring(7), 'base64');
      temp = {
        file_photo: contents,
        remarks: item.caption,
      }
      claim_photo.push(temp);
    }


    temp2 = {
      claim_photo: claim_photo
    }

    const { dataClaim } = this.props;
    const { loadingStateKlaim } = this.props;
    this.props.setDataClaimMV(temp2);

    tempAll = {
      alamat_tertanggung: dataClaim.alamat_tertanggung,
      claim_mv: {
        alamat_pengemudi: dataClaim.alamat_pengemudi,
        claim_mv_detail: dataClaim.claim_mv_detail,
        claim_photo: claim_photo,
        deskripsi_kejadian: dataClaim.deskripsiInsiden,
        email_pihak3: dataClaim.email_pihak3,
        estimasi_kerusakan: parseFloat(dataClaim.estimasi_kerusakan),
        file_polisi: dataClaim.file_foto_laporan_polisi,
        keadaan_kendaraan: dataClaim.keadaan_kendaraan,
        lapor_polisi: dataClaim.lapor_polisi,
        merek_kendaraan: dataClaim.merek_kendaraan,
        nama_pengemudi: dataClaim.nama_pengemudi,
        nama_pihak3: dataClaim.nama_pihak3,
        no_mesin: dataClaim.nomorMesin,
        no_plat: dataClaim.nomorPolisi,
        no_rangka: dataClaim.nomorRangka,
        no_sim: dataClaim.no_sim_pengemudi,
        pengemudi_bekerja: dataClaim.pengemudi_bekerja,
        penumpang_cidera: dataClaim.penumpang_cedera,
        periode_sim: dataClaim.periode_sim_pengemudi,
        phone_pihak3: dataClaim.phone_pihak3,
        photo_sim: dataClaim.file_foto_sim_pengemudi,
        posisi_kendaraan: dataClaim.posisi_kendaraan,
        seri_kendaraan: dataClaim.seri_kendaraan,
        status_pihak3: dataClaim.status_pihak3,
        status_saksi: dataClaim.status_saksi,
        tahun_kendaraan: dataClaim.tahun_kendaraan,
        tertangung_tau: dataClaim.tertanggung_tahu,
        tipe_kejadian: dataClaim.tipe_kejadian,
        remark_pihak3 : dataClaim.keterangan_pihak_ketiga,
        jenis_pihak3: dataClaim.selected_jenis_pihak_ketiga,
      },
      email_tertanggung: dataClaim.email_tertanggung,
      insurance_id: 'INS005',
      nama_tertanggung: dataClaim.nama_tertanggung,
      no_ktp: dataClaim.no_ktp,
      order_id: dataClaim.order_id,
      phone_tertanggung: dataClaim.phone_tertanggung,
      policy_enddate: dataClaim.policy_enddate,
      policy_no: dataClaim.policy_no,
      tanggal_kejadian: dataClaim.tanggal_kejadian,
      tempat_kejadian: dataClaim.alamatKejadian + ", " + dataClaim.labelKotaInsiden + ", " + dataClaim.labelProvinsiInsiden,
    }

    // console.log('stringify',JSON.stringify(tempAll));
    console.log('biasa',tempAll);

    this.props.setDataClaimAllMV(tempAll);

    // console.log('berkas unggahan',loadingStateKlaim);
    // this.props.doPostDataClaimMV(tempAll);
    // console.log('berkas unggahan',loadingStateKlaim);

    // const temppie = await this.setPostDataClaimMV(tempAll); // false
    // console.log('temppie',temppie);  //false
    // if(temppie  == true){
    //   // console.log('cetak: ','true');
    // } else{
    //   // console.log('cetak: ','false');  // print
    //   setTimeout(DialogAndroid.dismiss, 2000);
    //   this.props.navigate('ClaimMVInformasi');
    //
    // }


    // this.setPostDataClaimMV(tempAll).then((result)=>{
    //   if(result  == true){
    //     // console.log('cetak: ','true');
    //   } else{
    //     // console.log('cetak: ','false');  // print
    //
    //     this.props.navigate('ClaimMVInformasi');
    //
    //   }
    // });
    this.props.doPostDataClaimMV(tempAll);
    this.props.navigate('ClaimMVInformasi');

  }

  // async setPostDataClaimMV(tempAll){
  //   const {loadingStateKlaim} = this.props;
  //
  //   this.props.doPostDataClaimMV(tempAll);
  //   return loadingStateKlaim;
  // }

  async konfirmasi(path) {
    const { listDataGambar } = this.props;
    const { selectedItem } = await DialogAndroid.showPicker(null, null, {
      items: [
        { label: 'Lihat Gambar', id: 'lihatGambar' },
        { label: 'Hapus gambar', id: 'hapusGambar' }
      ], positiveText: null,
    });
    if (selectedItem) {
      if (selectedItem.id == 'lihatGambar') {
        // when negative button is clicked, selectedItem is not present, so it doesn't get here
        this.lihatgambar(path);
      } else if (selectedItem.id == 'hapusGambar') {
        // console.log(this.state.images);
        let i = listDataGambar.length;
        // Alert.alert(i.toString());
        let data = [];
        data.push(path);
        // console.log(data);

        this.props.hapusDataGambarKamera(data);
        // while (i--) {
        //     if(data.indexOf(listDataGambar[i].path)!=-1){
        //         listDataGambar.splice(i,1);
        //         // console.log('state images: ',this.state.images);
        //         this.forceUpdate();
        //     }
        // }
        this.forceUpdate();

      } else {
        // kosong
      }
    }
  }

  lanjut() {

  }

  lihatgambar(path) {
    // Alert.alert(path);
    const { listDataGambar } = this.props;

    //JSON.parse(jsondata).filter(({website}) => website === 'yahoo');

    const temp = listDataGambar.filter((item) => item.path === path);
    // console.log('temp',temp[0]);
    let image = {
      'path': temp[0].path,
      'size': temp[0].size,
      'caption': temp[0].caption,
    }
    // console.log('image', image);
    this.props.setDataGambarKameraTemp(image);
    this.props.navigate('ClaimGambar');
    // console.log('path', temp);
    // console.log('lihat gambar',listDataGambar);

  }

  onSubmit() {
    this.props.navigate('ClaimMVInformasi');
    // Alert.alert('test');
  }

  tambahFoto() {
    // images = this.state.images;
    const { listDataGambar } = this.props;

    image = [];
    ImagePicker.openCamera({
      cropping: false,
      includeExif: false,
      avoidEmptySpaceAroundImage: true,
    }).then(image => {
      // console.log('received image 6', image);
      ImageResizer.createResizedImage(image.path, image.width * 0.4, image.height * 0.4, 'JPEG', 100, 0).then((response) => {

        image = {
          'size': response.size,
          'path': response.uri,
          'caption': '',
        };

        // images.push(image);
        // this.setState({images : images});
        // this.props.setDataGambarKamera(image);
        this.props.setDataGambarKameraTemp(image);
        this.props.navigate('ClaimGambar');
        // console.log('response 6',response);
      });

      // images.push(image);
      // this.setState({images : images});
    }).catch(e => alert(e));
  }

  render() {
    const { listDataGambar, dataClaim } = this.props;
    console.log('data claim 6',dataClaim);
    // console.log('data claim 6', listDataGambar);

    let output = [];
    if (listDataGambar != null) {
      // console.log('images',this.state.images);
      listDataGambar.forEach(function (item) {
        fileuri = item.path;

        output.push(
          <TouchableOpacity
            onPress={() => { this.konfirmasi(item.path).done() }}
            style={{ width: width / 5, height: width / 5, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
            <Image
              source={{ uri: fileuri }} style={{ width: (width / 5), height: (width / 5), resizeMode: 'stretch', backgroundColor: '#FFF' }}
            />
          </TouchableOpacity>
        );
      }.bind(this));
    }

    return (
      <View>
        <Header
          // leftComponent = {{
          //   icon : 'arrow-back',
          //   color: '#FFF',
          //   onPress : () => this.kembali(),
          // }}
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'Incident Form 6 dari 6',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <View>
          <View style={{justifyContent:'center', alignItems:'center'}}><Text style={{textAlign:'center', fontWeight:'bold'}}>Lampiran Foto Kerusakan</Text></View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}><Text style={{ textAlign: 'center' }}>Isi Keterangan Foto apa saja yang akan diunggah secara live</Text></View>
          <View style={{ flexWrap: 'wrap', width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }}>

            {output}


            {this.buttonTambah()}

          </View>

          <View>
            <View style={styles.button}>
              <RkButton rkType='danger' onPress={this.kirim}>Kirim</RkButton>

            </View>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    dataClaimAll: state.dataClaimAll,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listDataGambar: state.listDataGambar,
    loadingStateKlaim: state.loadingStateKlaim,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVBerkasUnggahan1);
