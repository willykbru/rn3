import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, ActivityIndicator, Text, NetInfo } from 'react-native';

import { Header } from 'react-native-elements';
import { Container, Content, Tab, Tabs, TabHeading, Icon, Button, Footer, Card, CardItem, Left, Center, Right } from 'native-base';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';



import Icon2 from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import DialogAndroid from 'react-native-dialogs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import ClaimLihatDetailKlaim from './ClaimLihatDetailKlaim';
import OfflineNotice from '../../components/offline_notice';

class ClaimLihatKlaim1 extends Component {
    constructor(props) {
        super(props);
    }

   



    handleBackPress = () => {
        //this.goBack(); // works best when the goBack is async
        this.props.navigate('Main');
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }


    componentWillUnmount() {
        // this.props.resetListClaimDone();
        // this.props.resetListClaimTerkini();
    }

    componentDidUpdate() {
        // this.isKlaim();
    }



    componentWillReceiveProps(nextProps) {
        if (nextProps.listClaimDone != this.props.listClaimDone) {
            this.forceUpdate();
        }

        if (nextProps.listClaimTerkini != this.props.listClaimTerkini) {
            this.forceUpdate();
        }

        if (nextProps.loadingKlaimHistori != this.props.loadingKlaimHistori) {
            this.forceUpdate();
        }

        if (nextProps.loadingKlaimTerkini != this.props.loadingKlaimTerkini) {
            this.forceUpdate();
        }
    }

    kembali() {
        this.props.navigate('Main');
    }

    onSubmit(claim_number) {
        // Alert.alert(claim_number); 
        this.props.resetClaimStatusDetail();
        this.props.setClaimNumber(claim_number);
        this.props.navigate('ClaimLihatDetailKlaim');


    }

    onPressCardDone(title, item) {
        // console.log('onPressCardDone', item.items);
        temp = {
            "items" : item,
            "title" : title,
        }
        this.props.setLinkSPK(temp); 
        this.props.navigate('ClaimLihatSPK');
    }

    onPressCardTerkini(item) {
        this.onSubmit(item.claim_number);
    }

    view_histori() {
        let output_histori = [];

        const { listClaimDone, loadingKlaimHistori } = this.props;
        // console.log('view_histori', loadingKlaimHistori);
       
        if (listClaimDone.length > 0) {
            listClaimDone.forEach(function (item) {
                let status = this.set_status(item.claim_status);
                
                // console.log(this.setStyle(status));
                if (item.claim_status == 'Y') {
                    output_histori.push(

                        <TouchableOpacity key={item.claim_number} onPress={() => {}}>
                            <Card>
                                <CardItem bordered style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontWeight:'bold' }}>{item.claim_number}</Text>
                                    </View>

                                </CardItem>
                                <CardItem bordered >
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ textAlign: 'center' }}>{item.claim_status_name}</Text>                                            
                                        </View>
                                    </View>
                                </CardItem>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Text style={{ textAlign: 'right' }}>Update Terakhir</Text>
                                    <Text style={{ textAlign: 'right' }}>{item.input_date}</Text>
                                </View>

                            </Card>
                        </TouchableOpacity>
                    );
                } else {
                    // console.log('histori', item);
                    output_histori.push(
                        <Card key={item.claim_number}>
                            <CardItem bordered style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{item.claim_number}</Text>
                                </View>
                            </CardItem>
                            <CardItem bordered >
                                <View style={{ flexDirection: 'row', margin: 10 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ textAlign: 'center' }}>{item.claim_status_name}</Text>
                                        <Text style={{ textAlign: 'center' }}>{item.remark}</Text>
                                    </View>
                                </View>
                            </CardItem>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Text style={{ textAlign: 'right' }}>Update terakhir: </Text>
                                <Text style={{ textAlign: 'right' }}>{item.input_date}</Text>
                            </View>

                        </Card>

                    );
                }
            }.bind(this));
        } else {
            output_histori.push(
                <View style={{ flexDirection: 'row', margin: 10 }} key={0}>
                    <Text>Anda tidak memiliki riwayat klaim.</Text>
                </View>
            );
        }

        if (loadingKlaimHistori.loadingHistoriKlaim == false) {
            return (
                <ScrollView style={styles.card}>
                    {output_histori}
                </ScrollView>
            );
        } else {
            return (
                <View style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            );
        }


    }

    set_status(item) {

        if (item == 'Y') {
            value = {
                title: 'Proses klaim telah selesai',
                color: 'rgb(0,255,0)'
            }

        } else if (item == 'N') {
            value = {
                "title": 'Menunggu proses dari asuransi',
                "color": 'rgb(0,0,0)'
            }
        } else if (item == 'A') {
            value = {
                "title": 'Klaim disetujui',
                "color": 'rgb(0,255,0)'
            }

        } else if (item == 'R') {
            value = {
                "title": 'Klaim ditolak',
                "color": 'red'
            }
        } else {
            value = {
                "title": null,
                "color": 'rgb(0,0,0)'
            }
        }

        return value;
    }

    setStyle(value) {
        return {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: value.color,
            paddingRight: 10,
            paddingLeft: 10,

        }
    }

    view_terkini() {
        let output_terkini = [];

        const { listClaimTerkini, loadingKlaimTerkini } = this.props;
        console.log('view terkini', loadingKlaimTerkini);
       
        if(loadingKlaimTerkini.loadingKlaimTerkini == false){
            if (listClaimTerkini.length > 0) {
                listClaimTerkini.forEach(function (item) {
                    const status = this.set_status(item.claim_status);
                    // console.log('claim terkini', item);
                    if (item.claim_status == 'S') {
                        output_terkini.push(
                            <Card key={item.claim_number}>
                                <CardItem bordered style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{item.claim_number}</Text>
                                    </View>
                                </CardItem>
                                <CardItem bordered >
                                    <View style={{ width: '100%' }}>
                                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ textAlign: 'center' }}>{item.claim_status_name}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            {
                                                item.file_spk != null ?
                                                    <View style={{ flex: 1 }}>
                                                        <TouchableOpacity onPress={() => this.onPressCardDone('SPK', item)}>
                                                            <Text style={{ textDecorationLine: 'underline', textAlign: 'center', color: '#536dfe' }}>SPK</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    : null
                                            }

                                            {
                                                item.file_spk2 != null ?
                                                    <View style={{ flex: 1 }}>
                                                        <TouchableOpacity onPress={() => this.onPressCardDone('SPK Rev. 1', item)}>
                                                            <Text style={{ textDecorationLine: 'underline', textAlign: 'center', color: '#536dfe' }}>SPK Rev. 1</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    : null
                                            }

                                            {
                                                item.file_spk3 != null ?
                                                    <View style={{ flex: 1 }}>
                                                        <TouchableOpacity onPress={() => this.onPressCardDone('SPK Rev. 2', item)}>
                                                            <Text style={{ textDecorationLine: 'underline', textAlign: 'center', color: '#536dfe' }}>SPK Rev. 2</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    : null
                                            }
                                        </View>
                                        <TouchableOpacity onPress={() => { this.onSubmit(item.claim_number) }}>
                                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ textDecorationLine: 'underline', textAlign: 'center', color: '#536dfe', marginTop: 10 }}>Lihat Riwayat Klaim</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </CardItem>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Text style={{ textAlign: 'right' }}>Tanggal Update</Text>
                                    <Text style={{ textAlign: 'right' }}>{item.input_date}</Text>
                                </View>

                            </Card>

                        );
                    } else {
                        output_terkini.push(
                            <TouchableOpacity key={item.claim_number} onPress={() => this.onPressCardTerkini(item)}>
                                <Card>
                                    <CardItem bordered style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{item.claim_number}</Text>
                                        </View>

                                    </CardItem>
                                    <CardItem bordered >
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ flex: 1 }}>
                                                <Text style={{ textAlign: 'center' }}>{item.claim_status_name}</Text>
                                                <Text style={{ textDecorationLine: 'underline', textAlign: 'center', color: '#536dfe' }}>Lihat Catatan</Text>
                                            </View>
                                        </View>
                                    </CardItem>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <Text style={{ textAlign: 'right' }}>Tanggal Update</Text>
                                        <Text style={{ textAlign: 'right' }}>{item.input_date}</Text>
                                    </View>

                                </Card>
                            </TouchableOpacity>
                        );
                    }
                }.bind(this));
            } else {
                output_terkini.push(
                    <View style={{ flexDirection: 'row', margin: 10 }} key={0}>
                        <Text>Anda tidak sedang melakukan klaim.</Text>
                    </View>
                );
            }

            return (
                <ScrollView style={styles.card}>
                    {output_terkini}
                </ScrollView>
            );
        } else {
            <ActivityIndicator size="large" color="#0000ff" />
        }
            
        
        
    }

    render() {
        const { detailPolis } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 52 }}>
                    <Header
                        leftComponent={
                            <TouchableOpacity onPress={() => { this.kembali() }}>
                                <Icon2 name='md-arrow-round-back' size={25} color='#FFF' />
                            </TouchableOpacity>
                        }
                        centerComponent={{
                            text: 'Lihat Status Klaim',
                            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                        }}
                        outerContainerStyles={{ backgroundColor: '#EE0000', height: 53 }}
                    />

                </View>
                
                <View style={{ height: 52, justifyContent: 'center', marginLeft: 10, marginRight: 10 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ textAlign: 'center' }}>No Polis</Text>
                        <Text style={{ textAlign: 'center' }}>{detailPolis.policy_no}</Text>
                    </View>
                </View>
                <View style={{ height: Dimensions.get('window').height - 53 - 24 - 53 }}>
                    <Container>

                        <Tabs>

                            <Tab heading="Terkini" >

                                {this.view_terkini()}


                            </Tab>
                            <Tab heading="Histori" >

                                {this.view_histori()}

                            </Tab>

                        </Tabs>


                    </Container>
                </View>
            </View>

        );
    }


}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eceff1',
    },
    tabbar: {
        backgroundColor: '#3f51b5',
    },
    tab: {
        width: 120,
    },
    indicator: {
        backgroundColor: '#ffeb3b',
    },
    label: {
        color: '#fff',
        fontWeight: '400',
    },
    card: {
        paddingRight: 10,
        paddingLeft: 10,
    }
});

function mapStateToProps(state) {
    return {

        dataTransaksiKlaimMV: state.dataTransaksiKlaimMV,

        loadingStateKlaim: state.loadingStateKlaim,
        dataClaim: state.dataClaim,
        detailPolis: state.detailPolis,
        dataPolis: state.dataPolis,
        listClaimGeneral: state.listClaimGeneral,
        listClaimDone: state.listClaimDone,
        listClaimTerkini: state.listClaimTerkini,

        loadingKlaimHistori: state.loadingKlaimHistori,
        loadingKlaimTerkini: state.loadingKlaimTerkini,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimLihatKlaim1);