/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Image,
  BackHandler,
  Alert,
} from 'react-native';

import PhotoView from 'react-native-photo-view';
import Snackbar from 'react-native-android-snackbar';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

let images = [];
let image = [];
let status = 0; //0 = baru, 1 = edit

class ClaimGambar1 extends Component {

  constructor(props) {
    super(props);
    const { listDataGambarTemp, listDataGambar } = this.props;

    this.state = {
      path: null,
      size: 0,
      caption: null,
    };

  }

  componentDidMount() {

    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    const { listDataGambarTemp, listDataGambar } = this.props;
    images = listDataGambar;
    // console.log('images', listDataGambarTemp);

    if (listDataGambarTemp[0].caption == "") {
      status = 0;
    } else {
      status = 1;
    }

    // Alert.alert(status.toString());
    this.setState({
      path: listDataGambarTemp[0].path,
      size: listDataGambarTemp[0].size,
      caption: listDataGambarTemp[0].caption,
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  handleBackPress = () => {
    // this.props.navigate('ClaimVehicleUpload');
    this.props.navigate('ClaimMVBerkasUnggahan');
    return true;
  }

  submit() {
    // console.log('status :',status);
    if (this.state.caption != '') {

      image = {
        'size': this.state.size,
        'path': this.state.path,
        'caption': this.state.caption,
      };

      if (status == 0) {
        this.props.setDataGambarKamera(image);
      } else {
        // console.log('status 1',image);
        this.props.editDataGambarKamera(image);
      }
      // this.props.navigate('ClaimVehicleUpload');
      this.props.navigate('ClaimMVBerkasUnggahan');
    } else {
      Snackbar.show('Mohon isi keterangan foto', { duration: Snackbar.SHORT });
    }
  }

  render() {

    const { listDataGambarTemp } = this.props;

    //
    // console.log(listDataGambarTemp[0]);
    // //correctPath = originalPath.replace('file:', '');
    // console.log(this.state);
    //



    return (
      <View style={styles.container}>
        <View style={styles.images}>
          <PhotoView
            source={{ uri: listDataGambarTemp[0].path }}
            onLoad={() => console.log("onLoad called")}
            onTap={() => console.log("onTap called")}
            minimumZoomScale={1.0}
            maximumZoomScale={3}
            androidScaleType="fitCenter"
            style={styles.photo} />
        </View>
        <View style={styles.bottom}>
          <View style={styles.containerText}>
            <TextInput style={styles.text} placeholder='Isi Keterangan Foto' placeholderTextColor='rgb(192,192,192),' onChangeText={(text) => { this.setState({ caption: text }) }} value={(this.state.caption != null) ? this.state.caption : ''} />
          </View>
          <View style={styles.button}>
            <TouchableOpacity onPress={() => { this.submit() }}>
              <Image source={require('../../assets/icons/component/right-arrow.png')} style={{ width: 30, height: 30, }} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
  },
  images: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  bottom: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    height: 50,
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  containerText: {
    flex: 9,
  },
  text: {
    color: 'white',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photo: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black'
  },
});

function mapStateToProps(state) {
  return {
    // claimState : state.claimState,
    // claimLabelState : state.claimLabelState,
    listDataGambar: state.listDataGambar,
    listDataGambarTemp: state.listDataGambarTemp,

    // listProvinsi : state.listProvinsi,
    // listDistrict : state.listDistrict,

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimGambar1);
