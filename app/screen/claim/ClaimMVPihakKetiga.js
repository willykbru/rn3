/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Container, Content, Picker, Form, Item, Label, Input, Separator } from "native-base";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');

let radio_pihak_ketiga = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
]

class ClaimMVPihakKetiga1 extends Component {
  constructor(props) {
    super(props);
    const {dataClaim} = this.props;
    this.state = {
      nama_pihak_ketiga: dataClaim.nama_pihak3,
      no_telepon_pihak_ketiga: dataClaim.phone_pihak3,
      email_pihak_ketiga: dataClaim.email_pihak3,
      alamat_pihak_ketiga: dataClaim.alamat_pihak3,
      perkiraan_kecepatan_pihak_ketiga: dataClaim.kecepatan_pihak3,
      status_pihak_ketiga: dataClaim.status_pihak3,
      pihak_ketiga: dataClaim.status_pihak3,
      pihak_ketigaIndex: 1,
      selected_jenis_pihak_ketiga: dataClaim.selected_jenis_pihak_ketiga,
      perkiraan_kecepatan_pihak_ketiga_editable : true,
      keterangan_pihak_ketiga : dataClaim.keterangan_pihak_ketiga,      
    }



    this.onSubmit = this.onSubmit.bind(this);

    this.onFocus = this.onFocus.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.namaPihakKetigaRef = this.updateRef.bind(this, 'nama_pihak_ketiga');
    this.noTeleponPihakKetigaRef = this.updateRef.bind(this, 'no_telepon_pihak_ketiga');
    this.emailPihakKetigaRef = this.updateRef.bind(this, 'email_pihak_ketiga');
    this.alamatPihakKetigaRef = this.updateRef.bind(this, 'alamat_pihak_ketiga');
    this.perkiraanKecepatanPihakKetigaRef = this.updateRef.bind(this, 'perkiraan_kecepatan_pihak_ketiga');
  }

  onValueChange(value) {
    this.setState({
      selected_jenis_pihak_ketiga: value
    });

    if(value == 'M' || value == 'L'){
      this.setState({
        perkiraan_kecepatan_pihak_ketiga_editable : true,
        perkiraan_kecepatan_pihak_ketiga: ''

      })      
    } else {
      this.setState({
        perkiraan_kecepatan_pihak_ketiga_editable : false,
        perkiraan_kecepatan_pihak_ketiga : '0'
      })
    }
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('ClaimMVPolisi');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    const {dataClaim} = this.props;

    if (dataClaim.status_pihak3 == 'Y') {
      this.setState({
        pihak_ketigaIndex: 0,
      });
    } else if (dataClaim.status_pihak3 == 'N') {
      this.setState({
        pihak_ketigaIndex: 1,
      });

    }
  }

  componentWillUnmount() {

  }

  kembali() {
    this.props.navigate('ClaimMVPolisi');
  }

  lanjut() {

  }


  laporan_pihak_ketiga() {
    let { errors = {}, ...data } = this.state;
    let { nama_pihak_ketiga = null, no_telepon_pihak_ketiga = null, email_pihak_ketiga = null, alamat_pihak_ketiga = null, perkiraan_kecepatan_pihak_ketiga = null } = data;

    if (this.state.pihak_ketiga == 'Y') {
      return (
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.namaPihakKetigaRef, this.emailPihakKetigaRef, this.keadaanKendaraanRef, this.jumlahTaksiranRef];
          }}
        >
          
            <Container>
              <Form>
              <View style={styles.row}>               
                <View style={styles.field}>
                  <Text>Jenis Pihak Ketiga</Text>
                </View>
              </View> 
              <View style={styles.row}>              
                <View style={styles.field}>
                  <Picker
                    mode="dialog"
                    placeholder="Jenis Pihak Ketiga"
                    placeholderStyle={{ color: "#2874F0" }}
                    note={false}
                    selectedValue={this.state.selected_jenis_pihak_ketiga}
                    onValueChange={this.onValueChange.bind(this)}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10
                    }}
                    style={{ width: undefined}}
                  >
                    <Picker.Item label="Orang" value="O" />
                    <Picker.Item label="Properti" value="P" />
                    <Picker.Item label="Mobil" value="M" />
                    <Picker.Item label="Lainnya" value="L" />
                  </Picker>

                  {
                    this.state.selected_jenis_pihak_ketiga == 'L' ?
                      <TextInput
                        placeholder='Isi Keterangan Pihak Ketiga'
                        style={{ marginBottom: 10}}       
                        onChangeText={(text) => this.setState({ keterangan_pihak_ketiga : text })}
                        value={this.state.keterangan_pihak_ketiga}
                      />
                      : null
                  }
                  
                </View>
              </View> 
                
              
              <Separator bordered>
                <Text>Data Pihak Ketiga</Text>
              </Separator>
              </Form>          
            
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-person' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Nama Pihak Ketiga'
                  keyboardType='default'
                  ref={this.namaPihakKetigaRef}
                  maxLength={50}
                  value={this.state.nama_pihak_ketiga}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitNama_pihak_ketiga}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.nama_pihak_ketiga}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='mobile-phone' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='No Telepon Pihak Ketiga'
                  keyboardType='phone-pad'
                  ref={this.noTeleponPihakKetigaRef}
                  maxLength={15}
                  value={this.state.no_telepon_pihak_ketiga}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitNo_telepon_pihak_ketiga}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.no_telepon_pihak_ketiga}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><MaterialIcon name='email' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Email Pihak Ketiga'
                  autoCapitalize='none'
                  keyboardType='email-address'
                  ref={this.emailPihakKetigaRef}
                  maxLength={50}
                  value={this.state.email_pihak_ketiga}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitEmail_pihak_ketiga}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.email_pihak_ketiga}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='address-book' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Alamat Pihak Ketiga'
                  keyboardType='default'
                  ref={this.alamatPihakKetigaRef}
                  maxLength={50}
                  value={this.state.alamat_pihak_ketiga}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitAlamat_pihak_ketiga}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.alamat_pihak_ketiga}
                  onFocus={this.onFocus}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-speedometer' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.field}>
                <TextField
                  label='Kecepatan kendaraan Pihak Ketiga (KM/Jam)'
                  keyboardType='numeric'
                  ref={this.perkiraanKecepatanPihakKetigaRef}
                  value={this.state.perkiraan_kecepatan_pihak_ketiga}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitPerkiraan_kecepatan_pihak_ketiga}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.perkiraan_kecepatan_pihak_ketiga}
                  onFocus={this.onFocus}
                  maxLength={3}
                  editable={this.state.perkiraan_kecepatan_pihak_ketiga_editable}
                />
              </View>
            </View>
            </Container>
         
          
        </KeyboardAwareScrollView>

      );
    } else {
      return null;
    }
  }

  onChangeText(text) {
    try {
      ['nama_pihak_ketiga', 'no_telepon_pihak_ketiga', 'email_pihak_ketiga', 'alamat_pihak_ketiga', 'perkiraan_kecepatan_pihak_ketiga']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onPressRadioLaporPolisi(value) {
    this.setState({ status_pihak_ketiga: value })
  }

  onSubmit() {
    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error

    if (this.state.pihak_ketiga == 'Y') {
      if (this.state.selected_jenis_pihak_ketiga == 'L'){
        if(this.state.keterangan_pihak_ketiga == undefined || this.state.keterangan_pihak_ketiga == '' || this.state.keterangan_pihak_ketiga == null){
          Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
          return;
        } 
      } 

      //console.log('detailPolis2', detailPolis);
      ['nama_pihak_ketiga', 'no_telepon_pihak_ketiga', 'email_pihak_ketiga', 'alamat_pihak_ketiga', 'perkiraan_kecepatan_pihak_ketiga']
        .forEach((name) => {
          let value = this[name].value();

          if (!value) {
            errors[name] = 'Tidak boleh kosong';
            isError = 1;
          }
        });

      this.setState({ errors });
      // console.log('errors', errors);

      // console.log('isError',isError);

      // console.log('state', this.state);

      if (isError == 0) {
        if (this.state.pihak_ketiga == 'Y') {
          nama_pihak3 = this.state.nama_pihak_ketiga;
          phone_pihak3 = this.state.no_telepon_pihak_ketiga;
          email_pihak3 = this.state.email_pihak_ketiga;
          alamat_pihak3 = this.state.alamat_pihak_ketiga;
          kecepatan_pihak3 = this.state.perkiraan_kecepatan_pihak_ketiga;
          keterangan_pihak_ketiga = this.state.keterangan_pihak_ketiga; 
          selected_jenis_pihak_ketiga = this.state.selected_jenis_pihak_ketiga;

        } else {
          nama_pihak3 = '';
          phone_pihak3 = '';
          email_pihak3 = '';
          alamat_pihak3 = '';
          kecepatan_pihak3 = '';
          keterangan_pihak_ketiga = '';
          selected_jenis_pihak_ketiga = '';
        }

        temp = {
          nama_pihak3: nama_pihak3,
          phone_pihak3: phone_pihak3,
          email_pihak3: email_pihak3,
          alamat_pihak3: alamat_pihak3,
          kecepatan_pihak3: kecepatan_pihak3,
          status_pihak3: this.state.pihak_ketiga,
          keterangan_pihak_ketiga : keterangan_pihak_ketiga,
          selected_jenis_pihak_ketiga : selected_jenis_pihak_ketiga,
        }

        this.props.setDataClaimMV(temp);

        this.props.navigate('ClaimMVBerkasUnggahan');
      } else {
        Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
      }
    }
    else {
      temp = {
        nama_pihak3: '',
        phone_pihak3: '',
        email_pihak3: '',
        alamat_pihak3: '',
        kecepatan_pihak3: '',
        status_pihak3: this.state.pihak_ketiga,
        keterangan_pihak_ketiga: '',
        selected_jenis_pihak_ketiga: '',
      }

      this.props.setDataClaimMV(temp);

      this.props.navigate('ClaimMVBerkasUnggahan');
    }

    // Alert.alert('test');
  }

  onSubmitNama_pihak_ketiga() {

  }

  onSubmitNo_telepon_pihak_ketiga() {

  }

  onSubmitEmail_pihak_ketiga() {

  }

  onSubmitAlamat_pihak_ketiga() {

  }

  onSubmitPerkiraan_kecepatan_pihak_ketiga() {

  }

  updateRef(name, ref) {
    this[name] = ref;
  }



  render() {
    const { dataClaim } = this.props;
    // console.log('data claim 5', dataClaim);
    // console.log('state', this.state);
    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'Incident Form 5 dari 6',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />

        <ScrollView style={styles.container}>
          <View style={styles.row}>
            <View style={styles.text}>
              <Text>Apakah ada orang lain yang dirugikan, jika ada siapa dan alamatnya?</Text>
              <RadioForm
                formHorizontal={true}
                animation={true}
                initial={1}
              >
                {radio_pihak_ketiga.map((obj, i) => {
                  var onPress = (value, index) => {
                    this.setState({
                      pihak_ketigaIndex: index,
                      pihak_ketiga: value,
                    })

                    // console.log(this.state.pihak_ketigaIndex);
                    // console.log(this.state.pihak_ketiga);
                  }
                  return (
                    <RadioButton labelHorizontal={true} key={i} >
                      {/*  You can set RadioButtonLabel before RadioButtonInput */}
                      <RadioButtonInput
                        obj={obj}
                        index={i}
                        isSelected={this.state.pihak_ketigaIndex === i}
                        onPress={onPress}
                        buttonStyle={{}}
                        buttonWrapStyle={{ marginRight: 10 }}
                      />
                      <RadioButtonLabel
                        obj={obj}
                        index={i}
                        onPress={onPress}
                        labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                      />
                    </RadioButton>
                  )
                })}
              </RadioForm>
            </View>
          </View>

          {this.laporan_pihak_ketiga()}

          <View style={styles.button}>
            <RkButton rkType='danger' onPress={this.onSubmit}>Lanjut</RkButton>
          </View>
        </ScrollView>


      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimMVPihakKetiga1);
