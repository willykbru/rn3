import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, NativeModules, Picker, Button } from 'react-native';

import { Col, Row, Grid } from 'react-native-easy-grid';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Header } from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';

import UploadButton from '../../components/button/UploadButton';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DialogAndroid from 'react-native-dialogs';
import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';

import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import styles from '../../assets/styles';

let images = [];
let photo = [];

class ClaimVehicleUpload1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      imageSim: null,
    }

  }

  pickSingleWithCamera(cropping) {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
      includeExif: true,
    }).then(image => {
      //console.log('received image', image);
      this.setState({
        imageSim: { uri: image.path, width: image.width, height: image.height },
      });
    }).catch(e => alert(e));
  }

  pickSingleBase64(cropit) {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      includeBase64: true,
      includeExif: true,
    }).then(image => {
      console.log('received base64 image');
      this.setState({
        image: { uri: `data:${image.mime};base64,` + image.data, width: image.width, height: image.height },
        images: null
      });
    }).catch(e => alert(e));
  }

  cleanupImages() {
    ImagePicker.clean().then(() => {
      console.log('removed tmp images from tmp directory');
    }).catch(e => {
      alert(e);
    });
  }

  cleanupSingleImage() {
    let image = this.state.image || (this.state.images && this.state.images.length ? this.state.images[0] : null);
    console.log('will cleanup image', image);

    ImagePicker.cleanSingle(image ? image.uri : null).then(() => {
      console.log(`removed tmp image ${image.uri} from tmp directory`);
    }).catch(e => {
      alert(e);
    })
  }

  cropLast() {
    if (!this.state.image) {
      return Alert.alert('No image', 'Before open cropping only, please select image');
    }

    ImagePicker.openCropper({
      path: this.state.image.uri,
      width: 200,
      height: 200
    }).then(image => {
      console.log('received cropped image', image);
      this.setState({
        image: { uri: image.path, width: image.width, height: image.height, mime: image.mime },
        images: null
      });
    }).catch(e => {
      console.log(e);
      Alert.alert(e.message ? e.message : e);
    });
  }

  pickSingle(cropit, circular = false) {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
    }).then(image => {
      console.log('received image', image);
      this.setState({
        image: { uri: image.path, width: image.width, height: image.height, mime: image.mime },
        images: null
      });
    }).catch(e => {
      console.log(e);
      Alert.alert(e.message ? e.message : e);
    });
  }

  scaledHeight(oldW, oldH, newW) {
    return (oldH / oldW) * newW;
  }

  renderImage(image) {
    return <Image style={{ width: 300, height: 300, resizeMode: 'contain' }} source={image} />
  }



  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  handleBackPress = () => {
    this.props.navigate('ClaimVehicle');
    return true;
  }

  render() {
    const { width, height } = Dimensions.get('window');
    const { dataClaim } = this.props;

    return (
      <View style={{ height: hp('100%') - 26 }}>
        <Header
          leftComponent={{
            icon: 'arrow-back',
            color: '#FFF',
            onPress: () => this.kembali(),
          }}
          centerComponent={{
            text: 'Upload Images',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <ScrollView>
          <View style={[styles.centerItemContent, styles.top20, {marginBottom: 20, marginRight:20, marginLeft:20, marginTop:10}]}>
            {this.kalimat()}


          {this.uploadFoto()}

          </View>
        </ScrollView>
      </View >
    );
  }

  lihatgambar(path) {
    // Alert.alert(path);
    const { listDataGambar } = this.props;

    //JSON.parse(jsondata).filter(({website}) => website === 'yahoo');

    const temp = listDataGambar.filter((item) => item.path === path);
    console.log('temp', temp[0]);
    let image = {
      'path': temp[0].path,
      'size': temp[0].size,
      'caption': temp[0].caption,
    }
    console.log('image', image);
    this.props.setDataGambarKameraTemp(image);
    this.props.navigate('ClaimGambar');
    // console.log('path', temp);
    // console.log('lihat gambar',listDataGambar);

  }

  kembali() {
    this.props.navigate('ClaimVehicle');
  }

  // async konfirmasi(path){
  //   const {listDataGambar} = this.props;
  //   const { selectedItem } = await DialogAndroid.showPicker(null, null, {
  //     items: [
  //         { label:'Lihat Gambar', id:'lihatGambar' },
  //         { label:'Hapus gambar', id:'hapusGambar' }
  //     ], positiveText : null,
  //   });
  //   if(selectedItem){
  //     if (selectedItem.id == 'lihatGambar') {
  //         // when negative button is clicked, selectedItem is not present, so it doesn't get here
  //         Alert.alert(path);
  //     } else if( selectedItem.id == 'hapusGambar'){
  //       // console.log(this.state.images);
  //       let i = listDataGambar.length;
  //       // Alert.alert(i.toString());
  //       let data = [];
  //       data.push(path);
  //       // console.log(data);
  //
  //       while (i--) {
  //           if(data.indexOf(listDataGambar[i].path)!=-1){
  //
  //               listDataGambar.splice(i,1);
  //               // console.log('state images: ',this.state.images);
  //               this.forceUpdate();
  //           }
  //       }
  //
  //     } else {
  //       // kosong
  //     }
  //   }
  // }

  async konfirmasi(path) {
    const { listDataGambar } = this.props;
    const { selectedItem } = await DialogAndroid.showPicker(null, null, {
      items: [
        { label: 'Lihat Gambar', id: 'lihatGambar' },
        { label: 'Hapus gambar', id: 'hapusGambar' }
      ], positiveText: null,
    });
    if (selectedItem) {
      if (selectedItem.id == 'lihatGambar') {
        // when negative button is clicked, selectedItem is not present, so it doesn't get here
        this.lihatgambar(path);
      } else if (selectedItem.id == 'hapusGambar') {
        // console.log(this.state.images);
        let i = listDataGambar.length;
        // Alert.alert(i.toString());
        let data = [];
        data.push(path);
        // console.log(data);

        this.props.hapusDataGambarKamera(data);
        // while (i--) {
        //     if(data.indexOf(listDataGambar[i].path)!=-1){
        //         listDataGambar.splice(i,1);
        //         // console.log('state images: ',this.state.images);
        //         this.forceUpdate();
        //     }
        // }
        this.forceUpdate();

      } else {
        // kosong
      }
    }
  }

  async konfirmasiSim(path) {
    const { selectedItem } = await DialogAndroid.showPicker(null, null, {
      items: [
        { label: 'Lihat Gambar', id: 'lihatGambar' },
        { label: 'Hapus gambar', id: 'hapusGambar' }
      ], positiveText: null,
    });
    if (selectedItem) {
      if (selectedItem.id == 'lihatGambar') {
        // when negative button is clicked, selectedItem is not present, so it doesn't get here
        // this.lihatgambar(path);
        Alert.alert('lihat gambar sim');
      } else if (selectedItem.id == 'hapusGambar') {

        // console.log(this.state.images);
        // let i = listDataGambar.length;
        // Alert.alert(i.toString());
        // let data = [];
        // data.push(path);
        // console.log(data);

        this.props.hapusDataGambarSimKameraTemp();
        // while (i--) {
        //     if(data.indexOf(listDataGambar[i].path)!=-1){
        //         listDataGambar.splice(i,1);
        //         // console.log('state images: ',this.state.images);
        //         this.forceUpdate();
        //     }
        // }
        this.forceUpdate();

      } else {
        // kosong
      }
    }
  }

  uploadFoto() {
    const { width, height } = Dimensions.get('window');
    // console.log('data claim', this.state.images);
    const { listDataGambar, listDataGambarSim } = this.props;
    console.log('list data gambar', listDataGambar);
    console.log('list data gambar sim', listDataGambarSim);

    let output = [];
    if (listDataGambar != null) {
      // console.log('images',this.state.images);
      listDataGambar.forEach(function (item) {
        fileuri = item.path;

        output.push(
          <TouchableOpacity
            onPress={() => { this.konfirmasi(item.path).done() }}
            style={{ width: width / 5, height: width / 5, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
            <Image
              source={{ uri: fileuri }} style={{ width: (width / 5), height: (width / 5), resizeMode: 'stretch', backgroundColor: '#FFF' }}
            />
          </TouchableOpacity>
        );
      }.bind(this));
    }

    return (
      <View>
        {
          listDataGambarSim[0].path == null ?

            <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSim()} >

              <Image source={require('../../assets/icons/component/id-card.png')} style={{ width: width / 2, height: width / 2, backgroundColor: 'rgba(0,0,0,.38)', resizeMode: 'contain' }} />

            </TouchableOpacity>


            :

            <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => { this.konfirmasiSim(listDataGambarSim[0].path).done() }} >
              <Image source={{ uri: listDataGambarSim[0].path }} style={{ width: width / 2, height: width / 2, backgroundColor: '#000' }} />
            </TouchableOpacity>

        }
        <Text style={[styles.baseText, { color: 'rgba(0,0,0,.38)' }]}>Upload SIM Pengemudi</Text>
        <View style={{ flexWrap: 'wrap', width: '100%', flexDirection: 'row' }}>
          {output}

          <TouchableOpacity onPress={() => { this.tambahFoto() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: '#000' }}>
            <Image source={require('../../assets/icons/component/plus.png')} style={{ width: width / 5, height: width / 5, backgroundColor: 'rgba(0,0,0,.38)', resizeMode: 'contain' }} />
          </TouchableOpacity>
        </View>


      </View>

    );
  }

  tambahFoto() {

    // images = this.state.images;
    const { listDataGambar } = this.props;

    image = [];
    ImagePicker.openCamera({
      cropping: false,
      width: 500,
      height: 500,
      includeExif: false,
      avoidEmptySpaceAroundImage: true,

    }).then(image => {
      //  console.log('received image', image.path);
      ImageResizer.createResizedImage(image.path, 800, 800, 'JPEG', 80, 0).then((response) => {

        image = {
          'size': response.size,
          'path': response.uri,
          'caption': '',
        };

        // images.push(image);
        // this.setState({images : images});
        // this.props.setDataGambarKamera(image);
        this.props.setDataGambarKameraTemp(image);
        this.props.navigate('ClaimGambar');
        // console.log(response);
      });

      // images.push(image);
      // this.setState({images : images});
    }).catch(e => alert(e));
  }

  lihatSim(alamatGambar) {
    Alert.alert(alamatGambar);
  }

  uploadSim() {
    // images = this.state.images;
    const { listDataGambarSim } = this.props;

    let imageSim = [];

    ImagePicker.openCamera({
      cropping: false,
      width: 500,
      height: 500,
      includeExif: false,
      avoidEmptySpaceAroundImage: true,

    }).then(image => {
      //  console.log('received image', image.path);
      ImageResizer.createResizedImage(image.path, 800, 800, 'JPEG', 80, 0).then((response) => {

        image = {
          'size': response.size,
          'path': response.uri,
          'caption': '',
        };

        this.props.setDataGambarSimKameraTemp(image);

      });

    }).catch(e => alert(e));
  }

  kalimat() {
    return (
      <View>
        <Text style={[styles.baseText]}>Pastikan dokumen KTP & SIM (nama, tanggal lahir, masa berlaku dan tanggal pemuatan) terlihat jelas.</Text>
        <Text></Text>
        <Text style={[styles.baseText]}>Pastikan dokumen STNK (nomor rangka, nomor mesin dan masa berlaku STNK terlihat dengan jelas).</Text>
      </View>
    );

  }

  _pickImageKTP = async () => {
    await ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      includeBase64: true,
      mediaType: 'photo',

    }).then(image => {
      if (image.size / 1024 <= 20480) {
        this.props.setPhotoKtpDtDly(image.data);
      } else {
        // pesan error
        Alert.alert('Foto Harus Kurang Dari 20 MB')
      }
    })
  }

  _pickCameraKTP = async () => {

    await ImagePicker.openCamera({
      width: 800,
      height: 600,
      cropping: true,
      includeBase64: true,
      mediaType: 'photo',
    }).then(image => {
      if (image.size / 1024 <= 20480) {
        this.props.onUploadFile(image.data);
        //this.popupDialog.dismiss();
      } else {
        appFunction.toastError('Foto Harus Kurang Dari 20MB');
        //this.popupDialog.dismiss();
      }
    });
  }

} // end class


function mapStateToProps(state) {
  return {
    // claimState : state.claimState,
    // claimLabelState : state.claimLabelState,
    dataClaim: state.dataClaim,
    listDataGambar: state.listDataGambar,
    listDataGambarSim: state.listDataGambarSim,
    // listProvinsi : state.listProvinsi,
    // listDistrict : state.listDistrict,

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimVehicleUpload1);
