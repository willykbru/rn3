/* app/screen/claim/ClaimVehicle_2.js */
/* Tampilan versi kedua, dibuat karena mengalami perubahan desain */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView } from 'react-native';

import { TextField } from 'react-native-material-textfield';
import { Col, Row, Grid } from "react-native-easy-grid";
import ModalFilterPicker from 'react-native-modal-filter-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Header } from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Snackbar from 'react-native-android-snackbar';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

class ClaimVehicle1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      namaLengkap: this.props.detailPolis.nama_tertanggung,
      email: this.props.detailPolis.email_tertanggung,
      handphone: this.props.detailPolis.phone_tertanggung,
      telepon: '-',
      namaPengemudi: this.props.dataClaim.namaPengemudi,
      noPolisi: this.props.detailPolis.no_plat,
      noRangkaKendaraan: this.props.detailPolis.no_rangka,
      kecepatanInsiden: this.props.dataClaim.kecepatanInsiden,
      deskripsiInsiden: this.props.dataClaim.deskripsiInsiden,

      visibleNoPolis: false,
      pickedNoPolis: null,
      statusNoPolis: null,

      visibleTipeInsiden: false,
      pickedTipeInsiden: null,
      labelTipeInsiden: this.props.dataClaim.labelTipeInsiden,

      visibleLokasiProvinsi: false,
      pickedLokasiProvinsi: null,
      labelProvinsiInsiden: this.props.dataClaim.labelProvinsiInsiden,

      visibleLokasiKota: false,
      pickedLokasiKota: null,
      labelKotaInsiden: this.props.dataClaim.labelKotaInsiden,

      tanggalSekarang: new Date(),
      pickedTanggal: null,
      isPickedTanggal: false,

      pickedJam: null,
      jamSekarang: new Date().getHours() + ":" + new Date().getMinutes(),
      isPickedJam: false,

    };

    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitNamaLengkap = this.onSubmitNamaLengkap.bind(this);
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitHandphone = this.onSubmitHandphone.bind(this);
    this.onSubmitTelepon = this.onSubmitTelepon.bind(this);
    this.onSubmitNamaPengemudi = this.onSubmitNamaPengemudi.bind(this);
    this.onSubmitNoPolisi = this.onSubmitNoPolisi.bind(this);
    this.onSubmitNoRangkaKendaraan = this.onSubmitNoRangkaKendaraan.bind(this);
    this.onSubmitKecepatanInsiden = this.onSubmitKecepatanInsiden.bind(this);

    // this.namaLengkapRef = this.updateRef.bind(this, 'namaLengkap');
    // this.emailRef = this.updateRef.bind(this, 'email');
    // this.handphoneRef = this.updateRef.bind(this, 'handphone');
    // this.teleponRef = this.updateRef.bind(this, 'telepon');
    this.namaPengemudiRef = this.updateRef.bind(this, 'namaPengemudi');
    // this.noPolisiRef = this.updateRef.bind(this, 'noPolisi');
    this.noRangkaKendaraanRef = this.updateRef.bind(this, 'noRangkaKendaraan');
    this.kecepatanInsidenRef = this.updateRef.bind(this, 'kecepatanInsiden');
    this.deskripsiInsidenRef = this.updateRef.bind(this, 'deskripsiInsiden');
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this.props.getProvinsi();
    this.props.getTipeInsiden();

    if (this.props.claimState.provinsiInsiden && this.props.claimState.provinsiInsiden != 'Pilih') {
      this.props.getDistrict(this.props.claimState.provinsiInsiden);
    }

    const { detailPolis } = this.props;
    // console.log('a',detailPolis);

  }

  componentWillUnmount() {
    //BackHandler.removeEventListener('hardwareBackPress');
  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    this.props.navigate('Main');
    return true;
  }


  onChangeText(text) {
    try {
      ['namaPengemudi', 'kecepatanInsiden', 'deskripsiInsiden']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onSubmitNamaLengkap() {
    this.email.focus();
  }

  onSubmitEmail() {
    this.handphone.focus();
  }

  onSubmitHandphone() {
    this.telepon.focus();
  }

  onSubmitTelepon() {
    this.namaPengemudi.focus();
  }

  onSubmitNamaPengemudi() {
    // this.noPolisi.focus();
    this.noRangkaKendaraan.focus();
  }

  onSubmitNoPolisi() {
    // this.noRangkaKendaraan.focus();
  }

  onSubmitNoRangkaKendaraan() {

  }

  onSubmitKecepatanInsiden() {
    this.deskripsiInsidenRef.focus();
  }

  onSubmitDeskripsiInsiden() {

  }


  onSubmit() {
    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error


    //console.log('detailPolis2', detailPolis);
    ['namaPengemudi', 'kecepatanInsiden', 'deskripsiInsiden']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Should not be empty';
          isError = 1;
        }
      });

    this.setState({ errors });
    // console.log('errors', errors);

    // console.log('isError',isError);
    if (isError == 0) {

      this.props.setDataClaimMV(this.state);

      this.props.navigate('ClaimVehicleUpload');
    } else {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    }


  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  lanjut() {
    this.onSubmit();
    //this.props.navigate('ClaimVehicleUpload');

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }



  render() {

    let { errors = {}, ...data } = this.state;
    let { namaPengemudi = null, kecepatanInsiden = null, deskripsiInsiden = null } = data;

    const { width, height } = Dimensions.get('window');

    const { claimState, claimLabelState, dataClaim, detailPolis, tipeInsiden } = this.props;

    const lob = 'Claim';

    const {
      telepon,
      pickedNoPolis,
      visibleNoPolis,
      visibleTipeInsiden,
      isPickedTanggal,
      isPickedJam,
      pickedTanggal,
      pickedJam,
      pickedTipeInsiden,
      pickedLokasiKota,
      pickedLokasiProvinsi,
      visibleLokasiKota,
      visibleLokasiProvinsi,
      noPolisi,
    } = this.state;

    const optionsProvinsi = this.props.listProvinsi;
    const optionsTipeInsiden = this.props.listTipeInsiden;
    const optionsDistrict = this.props.listDistrict;

    if (optionsProvinsi != null) {
      // optionsProvinsi.splice(0,1); // menghapus elemen pertama "PILIH" gagal
      // optionsProvinsi.pop(); // menghapus elemen terakhir / tidak cocok
    }

    if (optionsDistrict != null) {
      // optionsDistrict.splice(0,1); // menghapus elemen pertama "PILIH"
    }

    /* waktu */
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var maxDate = new Date(year + 10, 12, 31);
    var minDate = new Date(year - 2, 1, 1);
    /* end waktu */
    RkTheme.setType('RkButton', 'button1', {
      container: {
        backgroundColor: 'red',
        borderRadius: 10,

      },
      content: {
        color: '#FFF',
      }
    });

    return (
      <View>
        <Header
          leftComponent={{
            icon: 'arrow-back',
            color: '#FFF',
            onPress: () => this.kembali(),
          }}
          centerComponent={{
            text: 'Incident Form',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.namaLengkapRef, this.emailRef, this.handphoneRef, this.teleponRef, this.namaPengemudiRef, this.noRangkaKendaraanRef, this.noPolisiRef, this.kecepatanRef, this.deskripsiInsidenRef];
          }}
        >
          <ScrollView style={styles.container}>
            <Grid>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent} >
                  <TextField
                    ref={this.namaLengkapRef}
                    value={detailPolis.nama_tertanggung}
                    label='Nama Lengkap'
                    keyboardType='default'
                    onFocus={this.onFocus}
                    onSubmitEditing={this.onSubmitNamaLengkap}
                    onChangeText={(namaLengkap) => { this.setState({ namaLengkap: namaLengkap }) }}
                    returnKeyType='next'
                    tintColor='#228B22'
                    error={errors.namaLengkap}
                    editable={false}

                  />
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon} >
                  <Icon name='ios-mail' size={40} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent}>
                  <TextField
                    label='Email'
                    keyboardType='email-address'
                    ref={this.emailRef}
                    value={detailPolis.email_tertanggung}
                    onFocus={this.onFocus}
                    onSubmitEditing={this.onSubmitEmail}
                    returnKeyType='next'
                    tintColor='#228B22'
                    error={errors.email}
                    editable={false}
                  />
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <Icon name='ios-phone-portrait' size={40} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent}>
                  <TextField
                    label='Handphone'
                    keyboardType='phone-pad'
                    ref={this.handphoneRef}
                    value={detailPolis.phone_tertanggung}
                    maxLength={40}
                    onSubmitEditing={this.onSubmitHandphone}
                    returnKeyType='next'
                    tintColor='#228B22'
                    editable={false}
                  />
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <Foundation name='telephone' size={40} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent}>
                  <TextField
                    label='Telepon'
                    keyboardType='phone-pad'
                    ref={this.teleponRef}
                    maxLength={40}
                    value='-'
                    onSubmitEditing={this.onSubmitTelepon}
                    returnKeyType='next'
                    tintColor='#228B22'
                    editable={false}
                  />
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent}>
                  <TextField
                    ref={this.namaPengemudiRef}
                    label='Nama Pengemudi'
                    keyboardType='default'
                    onFocus={this.onFocus}
                    onSubmitEditing={this.onSubmitNamaPengemudi}
                    onChangeText={this.onChangeText}
                    returnKeyType='next'
                    tintColor='#228B22'
                    error={errors.namaPengemudi}
                    maxLength={40}
                    value={this.state.namaPengemudi}
                  />
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='car' size={25} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent}>
                  <TextField
                    label='Nomor Polisi'
                    keyboardType='default'
                    ref={this.noPolisiRef}
                    maxLength={40}
                    onSubmitEditing={this.onSubmitNoPolisi}
                    returnKeyType='next'
                    tintColor='#228B22'
                    value={detailPolis.no_plat}
                    editable={false}
                  />
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='car' size={25} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} justifyContent='center' style={styles.colComponent}>
                  <TextField
                    label='Nomor Rangka kendaraan'
                    keyboardType='numeric'
                    ref={this.noRangkaKendaraanRef}
                    maxLength={40}
                    onFocus={this.onFocus}
                    onSubmitEditing={this.onSubmitNoRangkaKendaraan}

                    returnKeyType='next'
                    tintColor='#228B22'
                    value={detailPolis.no_rangka}
                    error={errors.noRangkaKendaraan}
                    editable={false}
                  />
                </Col>
              </Row>
              <Row height={56} style={{ marginTop: 16 }}>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='list' size={25} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} style={styles.colComponent}>
                  <TextField
                    label='No Polis'
                    keyboardType='default'
                    ref={this.noPolisiRef}
                    maxLength={40}
                    onSubmitEditing={this.onSubmitNoPolisi}

                    returnKeyType='next'
                    tintColor='#228B22'
                    value={detailPolis.policy_no}
                    editable={false}
                  />
                </Col>
              </Row>
              <Row height={56} style={{ marginTop: 16 }}>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='list' size={25} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} style={[styles.colComponent]}>
                  <View style={styles.viewPicker}>
                    <TouchableOpacity onPress={() => this.onShowTipeInsiden()}>
                      <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelTipeInsiden == null ? 'Pilih Tipe Insiden' : this.state.labelTipeInsiden}</Text>
                    </TouchableOpacity>
                    <ModalFilterPicker
                      visible={visibleTipeInsiden}
                      onSelect={(picked, label) => this.onSelectTipeInsiden(picked, label)}
                      onCancel={() => this.onCancelTipeInsiden()}
                      options={optionsTipeInsiden}
                    />
                  </View>
                </Col>
              </Row>
              <Row height={56} style={{ marginTop: 16 }}>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='calendar' size={25} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} style={[styles.colComponent]}>
                  <View style={{ borderBottomWidth: .5, paddingBottom: 15 }}>
                    <DatePicker
                      style={{ width: wp('80%'), }}
                      placeholder="Pilih Tanggal Kejadian (DD-MM-YYYY)"
                      mode="date"
                      date={pickedTanggal == null ? null : this.state.tanggalSekarang}
                      androidMode="spinner"
                      format="DD-MM-YYYY"
                      minDate={minDate}
                      maxDate={maxDate}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {

                        },
                        dateInput: {
                          borderWidth: 0,
                          alignItems: 'flex-start',
                          paddingTop: 15,
                        },
                        dateText: {
                          fontSize: 18,
                          color: 'rgba(0,0,0, .38)'
                        }

                        // ... You can check the source to find the other keys.
                      }}
                      showIcon={false}
                      onDateChange={(tanggal) => { this.updateTanggal(tanggal) }}
                    />
                  </View>
                </Col>
              </Row>
              <Row height={56} style={{ marginTop: 16 }}>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='clock-o' color='rgba(0,0,0, .38)' size={25} />
                </Col>
                <Col size={9} style={[styles.colComponent]}>
                  <View style={{ borderBottomWidth: .5, paddingBottom: 10 }}>
                    <DatePicker
                      style={{ width: wp('80%') }}
                      mode="time"
                      date={pickedJam == null ? null : this.state.pickedJam}
                      androidMode="spinner"
                      placeholder="Pilih Jam Kejadian"
                      format="HH:mm"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      showIcon={false}
                      ref='datepicker1'
                      customStyles={{
                        dateIcon: {

                        },
                        dateInput: {
                          alignItems: 'flex-start',
                          borderWidth: 0,
                          paddingTop: 15,
                        },
                        dateText: {
                          fontSize: 18,
                          color: 'rgba(0,0,0, .38)',
                        }
                      }}
                      minuteInterval={10}
                      onDateChange={(datetime) => { this.updateJam(datetime) }}
                    />
                  </View>
                </Col>
              </Row>
              <Row style={{ marginTop: 16 }}>
                <Col size={1} style={styles.colIcon}>
                  <FontAwesome name='map-marker' size={25} color='rgba(0, 0, 0, .38)' />
                </Col>
                <Col size={9} style={[styles.colComponent]}>
                  <View style={styles.viewPicker}>
                    <TouchableOpacity onPress={() => this.onShowLokasiProvinsi()}>
                      <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelProvinsiInsiden == null ? 'Lokasi Insiden (Provinsi)' : this.state.labelProvinsiInsiden}</Text>
                    </TouchableOpacity>
                    <ModalFilterPicker
                      visible={visibleLokasiProvinsi}
                      onSelect={(key, label) => this.onSelectLokasiProvinsi(key, label)}
                      onCancel={() => this.onCancelLokasiProvinsi()}
                      options={optionsProvinsi}
                    />
                  </View>
                </Col>
              </Row>
              <Row style={{ marginTop: 16 }}>
                <Col size={1} style={styles.colIcon}>
                  {/*kosong */}
                </Col>
                <Col size={9} style={[styles.colComponent]}>
                  <View style={styles.viewPicker}>
                    <TouchableOpacity onPress={() => this.onShowLokasiKota()}>
                      <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelKotaInsiden == null ? 'Lokasi Insiden (Kota)' : this.state.labelKotaInsiden}</Text>
                    </TouchableOpacity>
                    <ModalFilterPicker
                      visible={visibleLokasiKota}
                      onSelect={(key, label) => this.onSelectLokasiKota(key, label)}
                      onCancel={() => this.onCancelLokasiKota()}
                      options={optionsDistrict}
                    />
                  </View>
                </Col>
              </Row>
              <Row>
                <Col size={1} style={styles.colIcon}>
                  <Icon name='ios-speedometer' color='rgba(0,0,0, .38)' size={25} />
                </Col>
                <Col size={9} style={[styles.colComponent]}>
                  <TextField
                    label='Kecepatan Ketika Insiden (KM/jam)'
                    keyboardType='numeric'
                    ref={this.kecepatanInsidenRef}
                    maxLength={3}
                    value={this.state.kecepatanInsiden}
                    onChangeText={this.onChangeText}
                    onSubmitEditing={this.onSubmitKecepatan}
                    returnKeyType='next'
                    tintColor='#228B22'
                    error={errors.kecepatanInsiden}
                    onFocus={this.onFocus}
                  />
                </Col>
              </Row>
              <Row >
                <Col justifyContent='center' style={{ paddingLeft: 20, paddingRight: 20 }}>
                  <TextField
                    label='Deskripsi Insiden (max. 500 karakter)'
                    value={this.state.deskripsiInsiden}
                    keyboardType='default'
                    ref={this.deskripsiInsidenRef}
                    maxLength={40}
                    onSubmitEditing={this.onSubmitDeskripsiInsiden}
                    onChangeText={this.onChangeText}
                    returnKeyType='next'
                    tintColor='#228B22'
                    multiline={true}
                    numberOfLines={4}
                    maxLength={500}
                    style={styles.textField1}
                    error={errors.deskripsiInsiden}
                    onFocus={this.onFocus}
                  />
                </Col>
              </Row>
              <Row style={{ marginTop: 16 }} >
                <Col style={{ paddingLeft: 20, paddingRight: 20, alignItems: 'center', justifyContent: 'center' }} >
                  <RkButton rkType='danger button1' onPress={this.onSubmit}>Lanjut</RkButton>
                </Col>
              </Row>
            </Grid>
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    );
  }

  kembali() {
    this.props.navigate('Main');
  }

  onShowNoPolis() {
    this.setState({ visibleNoPolis: true });
  }

  onSelectNoPolis(key, status) {
    this.setState({
      pickedNoPolis: key,
      statusNoPolis: status,
      visibleNoPolis: false
    })
  }

  onCancelNoPolis() {
    this.setState({
      visibleNoPolis: false
    });
  }

  onShowTipeInsiden() {
    this.setState({ visibleTipeInsiden: true });
  }

  onSelectTipeInsiden(picked, label) {
    this.setState({
      pickedTipeInsiden: picked,
      labelTipeInsiden: label,
      visibleTipeInsiden: false,
    })
  }

  onCancelTipeInsiden() {
    this.setState({
      visibleTipeInsiden: false,
    })
  }

  onSelectLokasiProvinsi(key, label) {
    this.setState({
      pickedLokasiProvinsi: key,
      labelProvinsiInsiden: label,
      visibleLokasiProvinsi: false,
      labelKotaInsiden: null,
    });

    this.props.getDistrict(key);
    //console.log(this.props.getDistrict(key));
  }

  onShowLokasiProvinsi() {
    this.setState({
      visibleLokasiProvinsi: true,
    });
  }

  onCancelLokasiProvinsi() {
    this.setState({
      visibleLokasiProvinsi: false,
    })
  }

  onSelectLokasiKota(key, label) {
    this.setState({
      pickedLokasiKota: key,
      labelKotaInsiden: label,
      visibleLokasiKota: false,
    });

  }

  onShowLokasiKota() {
    this.setState({
      visibleLokasiKota: true,
    })
  }

  onCancelNoPolis() {
    this.setState({
      visibleNoPolis: false,
    })
  }

  onSelectNoPolis(status, noPolis) {
    this.setState({
      pickedNoPolis: noPolis,
      statusNoPolis: status,
      visibleNoPolis: false,
    });
  }

  onShowPolis() {
    this.setState({
      visibleNoPolis: true,
    })
  }

  onCancelLokasiKota() {
    this.setState({
      visibleLokasiKota: false,
    })
  }

  updateTanggal(tanggal) {
    this.setState({
      pickedTanggal: tanggal,
      isPicked: true,
    });
  }

  updateJam(time) {
    this.setState({
      pickedJam: time,
      isPickedJam: true,
    });
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    listProvinsi: state.listProvinsi,
    listDistrict: state.listDistrict,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listTipeInsiden: state.listTipeInsiden,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80,
  },
  colIcon: {
    alignSelf: "flex-end",
    paddingBottom: 10,
    paddingLeft: 10
  },
  colComponent: {
    paddingLeft: 10,
    paddingRight: 15
  },
  textPicker: {
    height: 56,
    fontSize: 16,
    color: 'rgba(0,0,0, .38)',
    borderBottomWidth: .5,
    borderColor: 'rgba(0,0,0, .38)',
    paddingTop: 20,
  },
  textField1: {
    padding: 10,
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ClaimVehicle1);
