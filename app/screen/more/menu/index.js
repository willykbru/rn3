import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Linking, WebView, Dimensions, ScrollView } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';
//import RNAmplitute from 'react-native-amplitude-analytics';

import Slide from '../../../animated/Slide';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import OfflineNotice from '../../../components/offline_notice';

import {
  RkText,
  RkButton,
  RkStyleSheet
} from 'react-native-ui-kitten';

import * as appFunction from '../../../function';


class Menu extends Component {

	constructor(props) {
	    super(props);
	}
	toLob(lob) {
		this.props.setScreen(lob, 'cariasuransi');
		this.props.navigate('CariAsuransi');
	}
	
	menuItems(){
		var lobmenu = []; // menu lob
		var lobpush = []; // tambah menu lob
		//var lob = ['Travel','Bicycle','MV','Delay','Property','Apartment','Cargo','Health','PA','Life','Pet']; // menu-menu lob yang akan di push
		var lob = ['Travel', 'MV','Property','Apartment', 'PA','Health', 'Life'];
		//var lobtitle = ['Asuransi Kendaraan', 'Asuransi Rumah Tinggal', 'Asuransi Apartemen'];
		//console.log(sisalob + ' '+ roundlob);
		//var {height, width} = Dimensions.get('window');
		let items = <View/>;
		let size = appFunction.width() / 3;
		let emptyCount = this._getEmptyCount(size, lob);
	  //  var {height, width} = Dimensions.get('window');

		const imgsrc = {
		  'Travel'  : require('../../../assets/icons/menu/perjalanan.png'),
		  'MV'    : require('../../../assets/icons/menu/kendaraan.png'),
		  'Bicycle'    : require('../../../assets/icons/menu/sepeda.png'),
		  'Delay'    : require('../../../assets/icons/menu/delay.png'),
		  'Property'    : require('../../../assets/icons/menu/properti.png'),
		  'Apartment'    : require('../../../assets/icons/menu/apartemen.png'),
		  'Cargo'    : require('../../../assets/icons/menu/cargo.png'),
		  'Health'    : require('../../../assets/icons/menu/kesehatan.png'),
		  'PA'    : require('../../../assets/icons/menu/kecelakaan.png'),
		  'Life'    : require('../../../assets/icons/menu/jiwa.png'),
		  'Pet'    : require('../../../assets/icons/menu/pet.png'),
		  'More' : require('../../../assets/icons/menu/more.png'),
		}

		const asname = {
		  'Travel'  : 'Perjalanan',
		  'MV'    : 'Kendaraan',
		  'Bicycle'    : 'Sepeda',
		  'Delay'    : 'Delay',
		  'Property'    : 'Rumah',
		  'Apartment'    : 'Apartemen',
		  'Cargo'    : 'Cargo',
		  'Health'    : 'Kesehatan',
		  'PA'    : 'Kecelakaan',
		  'Life'    : 'Jiwa',
		  'Pet'    : 'Pet',
		}

		var count = 0;
		items = lob.map((route, i)=> {
			
			if(i === lob.length-1)
				return (
			  
				<View style={[styles.centerItemContent, styles.width100p, styles.bgWhite]} key={route}>
				  <RkButton rkType='tile'
							style={[appFunction.buttonHomeMenu(size)]}
							onPress={() => {
							  this.toLob(route)
							}}>
							<View style={[appFunction.circle(size)]}>
							  <Image style= {[appFunction.circleImage(size)]} source={imgsrc[route]} />
							</View>
				  </RkButton>
				  <View style={[styles.centerItemContent, styles.width100p]}>
					<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>{asname[route]}</RkText>
				  </View>
				  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
				 
				  
			
				</View>
				
			  );
			else
			  return (
			  
				<View style={[styles.bgWhite]} key={route}>
				  <RkButton rkType='tile'
							style={[appFunction.buttonHomeMenu(size)]}
							onPress={() => {
							  this.toLob(route)
							}}>
							<View style={[appFunction.circle(size)]}>
							  <Image style= {[appFunction.circleImage(size)]} source={imgsrc[route]} />
							</View>
				  </RkButton>
				  <View style={[styles.centerItemContent, styles.width100p]}>
					<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>{asname[route]}</RkText>
				  </View>
				  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
				</View>
			  
			  );
			
				});
		

		return items;
  }
    _getEmptyCount(size, menu) {
    let rowCount = Math.ceil((appFunction.height() - 20) / size);
    return rowCount * 3 - menu.length;
  };

	onBackPress = () => {
		var nscreen = 'Main';

		this.props.navigate(nscreen);
		return true;
	};
	

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress');
	}

	componentDidMount() {
					
		const event_name = 'Halaman More Menu LOB';	
		
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
		appFunction.setScreenGA(event_name, userID); 

		//const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
		//amplitude.logEvent('Halaman More Home Menu');
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
	}
	

  render() {
		const title = 'MORE MENU';

    return (
	<Container backgroundColor='white'>
  <Slide>
	 <Header
        backgroundColor={'white'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'red', padding: 12 } }}
        />
		<OfflineNotice />
		<ScrollView
      contentContainerStyle={[styles.rootContainer, styles.centerContent]}>
      {this.menuItems()}
	  
    </ScrollView>
      </Slide>
	  </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
	  screen: state.screen,
	  session: state. session,
	  status: state.status,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
