import React, { Component } from 'react';
import { BackHandler, Alert, Text, WebView, View, StyleSheet, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';

import Slide from '../../../animated/Slide';
import * as appFunction from '../../../function';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

class Agreement extends Component {

	constructor(props) {

	    super(props);

		}

	onBackPress = () => {
		const {screen} = this.props;
		var nscreen = 'Main';
		
		if(screen.asuransiScreen){
			nscreen = 'Pembayaran';
		}
		
		if(screen.renewalScreen){
			nscreen = 'Renewal';
		}

		this.props.navigate(nscreen);
		return true;
	};

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress');
	}

	componentDidMount() {
		const event_name = 'Halaman Syarat dan Ketentuan';
		appFunction.setScreenGA(event_name, userID);	
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
		
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
		
	
	}

  render() {
		const title = 'SYARAT DAN KETENTUAN';

    return (
	<Container>
  <Slide>
	 <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
      <WebView
        ref={(view) => this.webView = view}
        source={{uri: 'https://blog.jagain.com/content-mobile/agreement.htm'}}
		automaticallyAdjustContentInsets={false}
				 javaScriptEnabled={true}
				 domStorageEnabled={true}
				 decelerationRate="normal"
				 startInLoadingState={true}
      />
      </Slide>
	  </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
	  screen: state.screen,
	  status: state.status,
	  session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Agreement);
