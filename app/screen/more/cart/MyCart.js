import React, { Component } from 'react';
import {Image, StyleSheet, View} from 'react-native';
import { Container, Content, Tab, Tabs, TabHeading, Icon, Text, Button, Footer } from 'native-base';
//import Cart from '../../../components/template/cart';
import PendingCart from '../../dashboard/PendingCart';
import PendingPayment from '../../dashboard/PendingPayment';
import {Header} from 'react-native-elements';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

class MyCart extends Component {
	onBackPress = () => {
		var screen = 'Main';
		this.props.navigate(screen);
		return true;
	  };

	
  render() {
    const {listPendingCart} = this.props;
    return (

      <Container>
	  <Header
        backgroundColor={'white'}
        centerComponent={{ text: 'Cart' , style: { color: 'red', padding: 10 } }}
      />
        <Tabs tabBarUnderlineStyle={{ borderBottomWidth: 2, borderBottomColor: 'white' }} tabBarInactiveTextColor="white">
          <Tab heading={<TabHeading style={{ backgroundColor: 'red' }}><Icon name="ios-list-box-outline" /><Text style={styles.tabsText}>Pemesanan</Text></TabHeading>}>
            <PendingCart />
			<Footer style={{backgroundColor: 'white', borderTopWidth:.2, borderBottomWidth:.2, borderBottomColor: 'silver', borderTopColor:'silver', elevation: 10}}>
			<View style={{position: 'absolute', left: 10, top: 10, flexDirection: 'row'}}>
			<View style={{marginRight: 10}}>
				<Button small primary style={{backgroundColor: '#ff4c4c'}} onPress={() => this.props.setScreen('Home')}>
				  <Text>{listPendingCart.length > 0 ? 'Beli Lagi' : 'Beli' }</Text>
				</Button>
				</View>
				<View>
				{listPendingCart.length > 0 ? <Button small primary style={{backgroundColor: '#ff4c4c'}} onPress={() => {this.props.doBayarCart(Object.assign({}, {username: this.props.session.username})); this.props.navigate('PaymentGateway');}}>
				  <Text>Bayar</Text>
				</Button>:<Button small disabled primary style={{backgroundColor: 'silver'}} onPress={() => null}>
				  <Text>Bayar</Text>
				</Button>}
				
				</View>
			</View>
			</Footer>
          </Tab>
          <Tab heading={<TabHeading style={{ backgroundColor: 'red' }}><Icon name="ios-cash-outline" /><Text style={styles.tabsText}>Pembayaran</Text></TabHeading>}>
            <PendingPayment />
          </Tab>
        </Tabs>	
      </Container>
    );
  }
}


const styles = StyleSheet.create({
    tabsText:{
      fontSize: 12
    },
    headerColor:{
      backgroundColor: '#1b1b1b'
    },
    activeTab:{
      borderBottomColor: '#fff'
    }
  });
  
  
function mapStateToProps(state) {
  return {
    listPendingCart: state.listPendingCart,
    screen: state.screen,
	status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCart);