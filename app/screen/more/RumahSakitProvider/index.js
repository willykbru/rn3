import React, { Component } from 'react';
import { BackHandler, Alert, Text, WebView, View, StyleSheet, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';

import Slide from '../../../animated/Slide';
import * as appFunction from '../../../function';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import LoadingPage from '../../../components/loading/LoadingPage';

class RumahSakitProvider extends Component {

	constructor(props) {

	    super(props);

		}

	onBackPress = () => {
		var nscreen = 'Main';
		
		if(this.props.screen.asuransiScreen){
			nscreen = 'ListAsuransi';
		}
		
		this.props.navigate(nscreen);
		return true;
	};

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress');
	}

	componentDidMount() {
		
		const event_name = 'Halaman Rumah Sakit Provider';	
		
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
		appFunction.setScreenGA(event_name, userID); 
		
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
	}

  render() {
		const title = 'RUMAH SAKIT PROVIDER';
		const {dataSearch} = this.props;
		var myUrl = appFunction.webJagain + 'crs';
		
		//const dSearch
		 const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
	//Alert.alert('oke',dataSearch);
		

    return (
	<Container>
  <Slide> 
	 <Header
  			backgroundColor={'white'}
  			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
  			centerComponent={{ text: title , style: { color: 'red', padding: 12 } }}
  		  />
		  
		  <View style={[{backgroundColor: 'white', borderBottomWidth:.05}, styles.directionRow, styles.centerItemContent, styles.elev3]}>
 
		   <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                  <TouchableHighlight>
                    <View style={styles.centerItemContent}>
                       <View style={[styles.centerItemContent, styles.height50, styles.padTop5]}>
                           <Image source={require('../../../assets/icons/logo/logo-big.png')} style={[styles.height40, styles.width60, styles.resizeContain]}/>
                        </View>
                      </View>
                    </TouchableHighlight>                
                  </View>
		  </View>     
			  <WebView
				ref={(view) => this.webView = view}
				source={{uri: myUrl}}
				automaticallyAdjustContentInsets={false}
				 javaScriptEnabled={true}
				 domStorageEnabled={true}
				 decelerationRate="normal"
				 startInLoadingState={true}  
				/> 	  
      </Slide>
	  </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
	dataSearch: state.dataSearch,
	status: state.status,
	session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RumahSakitProvider);
