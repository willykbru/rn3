import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import * as appFunction from '../../function';

import LoginContent from './login/LoginContent';
import ProfileContent from './profile/ProfileContent';
import Register from './register';

class User extends Component{
  constructor(props) {
      super(props);
      this.state={
        dataLogin:[],
        screen:'',
        username:'',
        token:'',
      }
  }
  
  activeContent(isLogin){
	  if(isLogin){
		  return <ProfileContent />
		}else{
		  return <LoginContent />
		}
  }


render() {
    //const { screen } = this.state;
    const { username } = this.props.session;
    const { token } = this.props;
    const { isLogin } = this.props.status;
    //var isLogin = dataLogin.isLogin;
    //if(username && !appFunction.isEmpty(this.props.session)){
		
		return(
			<View>
				{this.activeContent(isLogin)}
			</View>			
		)
	}
}

function mapStateToProps(state) {
  return {
    token: state.token,
    session: state.session,
    status: state.status,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(User);
