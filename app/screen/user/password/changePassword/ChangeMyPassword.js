import React, { Component } from 'react';
import { BackHandler, Alert, TextInput, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';

import styles from '../../../../assets/styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../../actions';

import * as appFunction from '../../../../function';
import RNAmplitute from 'react-native-amplitude-analytics';

class ChangeMyPassword extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      old_password: '',
        new_password: '',
        confirm_password:'',
        verify_code: '',
		securepasscf:true,
		securepassn:true,
		securepasso:true,
    }
  }

  ubahPassword(data){
    //this.props.sendVerify(username);
    if(!data.old_password){
      appFunction.toastError('Old Password belum diisi');
    }else if(!data.new_password){
      appFunction.toastError('New Password belum diisi');
    }else if(!data.confirm_password){
      appFunction.toastError('Confirm Password belum diisi');
    }else if(!data.body.verify_code){
      appFunction.toastError('Kode Verifikasi Password belum diisi');
    }else if(data.new_password !== data.confirm_password){
      appFunction.toastError('Password yang diinput tidak sesuai dengan Confirm Password');
      //console.log('ubah password', data)
    }else{
      //console.log('test')
      this.props.sendChangePassword(data.body);
        //console.log('berhasil change', data);
    }
  }

  componentWillReceiveProps(NextProps){
    const oldChangePassword = this.props.status.changepassword;
    const {changepassword} = NextProps.status;
    //console.log('myuser', username);
    if(changepassword != oldChangePassword){
      if(changepassword.description == 'success'){
        this.props.resetStatusChangePassword();
        this.props.resetCPScreen();
        this.props.navigate('Main');
      }
    }
  }
  
  componentDidMount() {
	  
	const event_name = 'Halaman Ubah Password';
	const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	amplitude.logEvent(event_name);
	
	
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	
		appFunction.setScreenGA(event_name, userID);	
	
  }

  render () {
    const title = 'Ubah Password';
    const {new_password, confirm_password, old_password, verify_code} = this.state;
    const {username} = this.props.session;
    const passwordMD5 = appFunction.passwordMD5(new_password);
    const opasswordMD5 = appFunction.passwordMD5(old_password);
    //console.log(screen);
    const body = {
      old_password:opasswordMD5,
      password:passwordMD5,
      username,
      verify_code,
    }
    var data = {
      old_password,
      new_password,
      confirm_password,
      body,
    }

    return(
      <View style={[styles.width100p, styles.centerItemContent]}>
        <View style={[styles.top20, styles.width80p]}>
          <Text style={[styles.baseText, styles.bold, styles.font18]}>Kode Verifikasi Telah Dikirim Ke Email</Text>
        </View>
        <View style={[styles.top20, styles.width80p]}>
          <Text style={[styles.baseText,styles.colorGrey]}>Cek email anda dan masukkan kode verifikasi yang telah dikirim ke email anda dikolom <Text style={[styles.italic]}>Kode Verifikasi</Text>.</Text>
        </View>
        <View style={[styles.top40, styles.width80p, styles.borderBotP5, styles.borderColorBotGrey]}>
          {old_password ? <Text style={[styles.baseText, styles.colorGrey]}>Old Password</Text> : null}
		  <View style={[styles.width100p, styles.directionRow]}>
          <TextInput
            placeholder='Old Password'
			autoCapitalize='none'
            value={(old_password)}
            onChangeText={(old_password) => this.setState({old_password})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width85p]}
            secureTextEntry={this.state.securepasso}
          />
		<View style={[styles.width15p]}><TouchableHighlight onPress={()=>{this.setState({securepasso:!this.state.securepasso})}} underlayColor='transparent'>{ !this.state.securepasso?<Image source={require('../../../../assets/icons/component/show-password.png')} style={[styles.height25, styles.width25]}/>:<Image source={require('../../../../assets/icons/component/hide-password.png')} style={[styles.height25, styles.width25]}/>}</TouchableHighlight></View>
		</View>
		</View>
        <View style={[styles.top40, styles.width80p, styles.borderBotP5, styles.borderColorBotGrey]}>
          {new_password ? <Text style={[styles.baseText, styles.colorGrey]}>New Password</Text> : null}
          <View style={[styles.width100p, styles.directionRow]}>
          <TextInput
            placeholder='New Password'
			autoCapitalize='none'
            value={(new_password)}
            onChangeText={(new_password) => this.setState({new_password})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width85p]}
            secureTextEntry={this.state.securepassn}
          />
		 <View style={[styles.width15p]}><TouchableHighlight onPress={()=>{this.setState({securepassn:!this.state.securepassn})}} underlayColor='transparent'>{ !this.state.securepassn?<Image source={require('../../../../assets/icons/component/show-password.png')} style={[styles.height25, styles.width25]}/>:<Image source={require('../../../../assets/icons/component/hide-password.png')} style={[styles.height25, styles.width25]}/>}</TouchableHighlight></View>
		</View>
        </View>
        <View style={[styles.top40, styles.width80p, styles.borderBotP5, styles.borderColorBotGrey]}>
          {confirm_password ? <Text style={[styles.baseText, styles.colorGrey]}>Confirm Password</Text> : null}
          <View style={[styles.width100p, styles.directionRow]}>
          <TextInput
            placeholder='Confirm Password'
			autoCapitalize='none'
            value={(confirm_password)}
            onChangeText={(confirm_password) => this.setState({confirm_password})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width85p]}
            secureTextEntry={this.state.securepasscf}
          />
		<View style={[styles.width15p]}><TouchableHighlight onPress={()=>{this.setState({securepasscf:!this.state.securepasscf})}} underlayColor='transparent'>{ !this.state.securepasscf?<Image source={require('../../../../assets/icons/component/show-password.png')} style={[styles.height25, styles.width25]}/>:<Image source={require('../../../../assets/icons/component/hide-password.png')} style={[styles.height25, styles.width25]}/>}</TouchableHighlight></View>
		</View>
        </View>
        <View style={[styles.top40, styles.width80p, styles.borderBotP5, styles.borderColorBotGrey]}>
          {verify_code ? <Text style={[styles.baseText, styles.colorGrey]}>Kode Verifikasi</Text> : null}
          <TextInput
            placeholder='Kode Verifikasi'
            value={(verify_code)}
            onChangeText={(verify_code) => this.setState({verify_code:appFunction.numericOnly(verify_code)})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width80p]}
            keyboardType= 'numeric'
            maxLength = {5}
          />
        </View>
        <View style={[styles.top20, styles.bottom10, styles.width80p]}>
          <TouchableHighlight
            style={[styles.btnForgotPassword]}
            onPress={() => this.ubahPassword(data)}
            underlayColor='#fff'>
              <Text style={[styles.btnLoginText, styles.baseText]}>Ubah Password</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
    //end of return
  }
  //end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    status: state.status,
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeMyPassword);
