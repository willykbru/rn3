import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import styles from '../../../../assets/styles';
import EmailForgotPassword from './EmailForgotPassword';
import ChangeForgotPassword from './ChangeForgotPassword';

import Slide from '../../../../animated/Slide';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../../actions';

import * as appFunction from '../../../../function';

class ForgotPassword extends React.Component {

  constructor(props) {
    super(props);
  }

  activeContent(verify, fpScreen){
    if(fpScreen == 'EmailForgotPassword'){
      return <EmailForgotPassword />;
    }
    if(fpScreen == 'ChangeForgotPassword'){
      return <ChangeForgotPassword />;
      //appFunction.toastError('sukses kirim email');
    }
  }

  footerContent(fpScreen){
    if(fpScreen == 'EmailForgotPassword'){
      return(
        <View key='footer' style={[styles.directionColumn, styles.centerItemContent, styles.height10p]}>
          <View style={[styles.top10]} />
          <View style={[styles.directionRow]}>
            <Text style={[styles.baseText]}>{"Belum Punya Akun ? "}</Text>
            <TouchableHighlight underlayColor='white'
              onPress={() => this.props.navToRegister()}>
                <Text style={[styles.font14, styles.baseText, styles.underline, styles.bold, {color: 'rgb(204,0,0)'}]}>
                  Daftar Sekarang
                </Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }
  }

  onBackPress = () => {
    const {asuransiScreen} = this.props.screen;
    var screen = 'Main';
    if(asuransiScreen){
      screen= 'Login';
    }
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

render () {
  const title = 'Lupa Password';
  //console.log(screen);
  const {verify} = this.props.status;
  const {fpScreen} = this.props.screen;
  //console.log(fpScreen);
  //console.log(fpScreen);

  return(
    <Container>
	  <Slide>
		  <Header
			backgroundColor={'white'}
			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
			centerComponent={{ text: title , style: { color: 'red', padding: 12 } }}
		  />
		  <Content style={[styles.bgWhite]}>
			{this.activeContent(verify, fpScreen)}
		  </Content>
		  {this.footerContent(fpScreen)}
	  </Slide>
    </Container>
  )
  //end of return
}
//end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    navReducer: state.navReducer,
    status: state.status,
    screen: state.screen,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
