import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import styles from '../../../assets/styles';
import Slide from '../../../animated/Slide';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import VerificationContent from './VerificationContent';

import * as appFunction from '../../../function';

class Verification extends React.Component {

  constructor(props) {
    super(props);
  }


  onBackPress = () => {
    var screen = 'Register';
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

render () {
  const title = 'Verification';
  //console.log(screen);
  //const {registered} = this.props.status;
  //const {fpScreen} = this.props.screen;
  //console.log(fpScreen);
  //console.log(fpScreen);

  return(
    <Container>
  	  <Slide>
  		  <Header
  			backgroundColor={'white'}
  			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
  			centerComponent={{ text: title , style: { color: 'red', padding: 12 } }}
  		  />
        <Content>
           <VerificationContent />
       </Content>
  	  </Slide>
    </Container>
  )
  //end of return
}
//end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    navReducer: state.navReducer,
    status: state.status,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Verification);
