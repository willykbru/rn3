import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import styles from '../../../assets/styles';
import Slide from '../../../animated/Slide';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import EmailActivation from './EmailActivation';
import ActivateAccount from './ActivateAccount';

import * as appFunction from '../../../function';

class Activation extends React.Component {

  constructor(props) {
    super(props);
  }
  
  activeContent(actScreen){
    if(actScreen == 'EmailActivation'){
      return <EmailActivation />;
    }
    if(actScreen == 'ActivateAccount'){
      return <ActivateAccount />;
      //appFunction.toastError('sukses kirim email');
    }
  }
  
  footerContent(actScreen){
    if(actScreen == 'EmailActivation'){
      return(
        <View key='footer' style={[styles.directionColumn, styles.centerItemContent, styles.height10p]}>
          <View style={[styles.top10]} />
          <View style={[styles.directionRow]}>
            <Text style={[styles.baseText]}>{"Belum Melakukan Registrasi ? "}</Text>
            <TouchableHighlight underlayColor='white'
              onPress={() => this.props.navToRegister()}>
                <Text style={[styles.font14, styles.baseText, styles.underline, styles.bold, {color: 'rgb(204,0,0)'}]}>
                  Daftar Sekarang
                </Text>
            </TouchableHighlight>
          </View>
        </View>
      );
    }
  }


  onBackPress = () => {
    const {asuransiScreen} = this.props.screen;
    var screen = 'Main';
    if(asuransiScreen){
      screen= 'Login';
    }
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

render () {
  const title = 'Aktivasi Akun';
  //console.log(screen);
  //const {registered} = this.props.status;
  const {actScreen} = this.props.screen;
  //console.log(fpScreen);
  //console.log(fpScreen);

  return(
    <Container>
  	  <Slide>
  		  <Header
  			backgroundColor={'white'}
  			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
  			centerComponent={{ text: title , style: { color: 'red', padding: 12 } }}
  		  />
        <Content style={[styles.bgWhite]}>
		{this.activeContent(actScreen)}
       </Content>
	   {this.footerContent(actScreen)}
  	  </Slide>
    </Container>
  )
  //end of return
}
//end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    status: state.status,
	screen: state.screen,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Activation);
