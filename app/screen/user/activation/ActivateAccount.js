import React, { Component } from 'react';
import { BackHandler, Alert, TextInput, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';

import styles from '../../../assets/styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import CodeInput from 'react-native-code-input';
import TimerCountdown from 'react-native-timer-countdown';

import * as appFunction from '../../../function';
import RNAmplitute from 'react-native-amplitude-analytics';

class ActivateAccount extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        verify_code: '',
        reactivate: false,
    }
  }

  doActivate(data){
    //this.props.sendVerify(username);
    if(!data.verify_code){
      appFunction.toastError('Kode Verifikasi Password belum diisi');
    }else{
      //console.log('test')
      this.props.doActivate(data);
        //console.log('berhasil change', data);
    }
  }

  reactivate(auth){
    //console.log('kirim ulang');
    this.setState({reactivate: false});
    this.props.doReactivate(auth);
  }

  renderTimerActivate(auth, reactivate){
    if(!reactivate){
      return(
        <View style={[styles.directionRow, styles.centerItemContent]}>
          <Text style={[styles.font12, styles.baseText, { color: 'rgba(255, 25, 25, 0.7)', }]}>Kirim ulang kode dalam </Text>
           <TimerCountdown
               initialSecondsRemaining={30000}
               interval={1000}
               onTimeElapsed={() => this.setState({reactivate: true})}
               allowFontScaling={true}
               style={[styles.baseText, styles.font12, {color: 'rgba(255, 25, 25, 0.7)' }]}
           />
         </View>
      );
    }else{
      return(
          <View style={[styles.centerItemContent]}>
        <TouchableHighlight underlayColor='white'
          style={[styles.centerItemContent, styles.width50p]}
          onPress={() => this.reactivate(auth)}>
            <Text style={[styles.font14, styles.bold, styles.baseText, {color: 'rgba(255, 25, 25, 0.7)'}]}>KIRIM ULANG</Text>
        </TouchableHighlight>
      </View>
      );
    }
  }
  
  componentDidMount(){
	const event_name = 'Halaman Aktivasi Email';  
	  
	const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	amplitude.logEvent(event_name);
	
	
	var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	
	appFunction.setScreenGA(event_name, userID);	
  }

  render () {
    const title = 'Aktivasi Akun';
    const {verify_code, reactivate} = this.state;
    const {username} = this.props.status.activation;
    const height = Dimensions.get('window').height;
  //  const passwordMD5 = appFunction.passwordMD5(new_password);
    //console.log(screen);
    const auth = {
      username,
    }
    const data = {
      auth,
      verify_code,
    }

    return(
      <View style={[styles.width100p, styles.centerItemContent]}>
        <View style={[styles.top20, styles.width80p]}>
          <Text style={[styles.baseText, styles.bold, styles.font18]}>Kode Verifikasi Telah Dikirim Ke Email</Text>
        </View>
        <View style={[styles.top20, styles.width80p]}>
          <Text style={[styles.colorGrey]}>Cek email anda dan masukkan kode verifikasi yang telah dikirim ke email anda dikolom <Text style={[styles.italic]}>Kode Verifikasi</Text>.</Text>
        </View>
        <View style={[styles.top40, styles.width80p, styles.borderBotP5, styles.borderColorBotGrey]}>
          {verify_code ? <Text style={[styles.baseText, styles.colorGrey]}>Kode Verifikasi</Text> : null}
          <TextInput
            placeholder='Kode Verifikasi'
            value={(verify_code)}
            onChangeText={(verify_code) => this.setState({verify_code:appFunction.numericOnly(verify_code)})}
            underlineColorAndroid='transparent'
            style={[styles.width80p]}
            keyboardType= 'numeric'
            maxLength = {5}
          />
        </View>
        <View style={[styles.top20, styles.width80p]}>
          <TouchableHighlight
            style={[styles.btnForgotPassword]}
            onPress={() => this.doActivate(data)}
            underlayColor='#fff'>
              <Text style={[styles.btnLoginText, styles.baseText]}>Kirim</Text>
          </TouchableHighlight>
        </View>
        <View style={{height: height*.05}} />
        {this.renderTimerActivate(auth, reactivate)}
      </View>
    )
    //end of return
  }
  //end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    status: state.status,
	session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivateAccount);
