import React, { Component } from 'react';
import { BackHandler, Alert, TextInput, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';

import styles from '../../../assets/styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import * as appFunction from '../../../function';
import RNAmplitute from 'react-native-amplitude-analytics';

class EmailActivation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        username: '',
    }
  }

  kirimEmail(auth){
    //console.log('kirim email akivasi', username);
    this.props.doEmailActivation(auth);
  }
  
  
  componentDidMount(){
	  const event_name = 'Halaman Kirim Email Aktivasi';
	const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	amplitude.logEvent(event_name);
		  
	var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	
	appFunction.setScreenGA(event_name, userID);	 
  }


  render () {
    const title = 'Aktivasi Akun';
    const {username} = this.state;
	const auth = {
		username
	}

    //console.log(screen);

    return(
      <View style={[styles.width100p, styles.centerItemContent]}>
        <View style={[styles.top20, styles.width80p]}>
          <Text style={[styles.baseText, styles.bold, styles.font18]}>Akun Belum Diaktivasi?</Text>
        </View>
        <View style={[styles.top20, styles.width80p]}>
          <Text style={[styles.baseText,styles.colorGrey]}>Belum Aktivasi Akun? Masukkan email login Anda di bawah ini. Kami akan megirimkan pesan email beserta kode verifikasi untuk aktivasi akun anda.</Text>
        </View>
        <View style={[styles.top40, styles.width80p, styles.borderBotP5, styles.borderColorBotGrey]}>
          {username ? <Text style={[styles.baseText, styles.colorGrey]}>Email</Text> : null}
          <TextInput
            placeholder='Email'
			keyboardType='email-address'
			autoCapitalize='none'
            value={(username)}
            onChangeText={(username) => this.setState({username:appFunction.emailCharacterOnly(username)})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width80p]}
          />
        </View>
        <View style={[styles.top20, styles.width80p]}>
          <TouchableHighlight
            style={[styles.btnForgotPassword]}
            onPress={() => this.kirimEmail(auth)}
            underlayColor='#fff'>
              <Text style={[styles.btnLoginText, styles.baseText]}>Kirim Email</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
    //end of return
  }
  //end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    status: state.status,
	session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EmailActivation);
