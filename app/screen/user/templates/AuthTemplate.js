import React, { Component } from 'react';
import { Dimensions, View, ScrollView, Text, StyleSheet, Image } from 'react-native';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';

export default class AuthTemplate extends Component{

  constructor(props) {
    super(props);
  }

	render() {
    const title = this.props.title;
    const content = this.props.content;
    const footer = this.props.footer;
    let size = appFunction.width() / 4;
    var {height, width} = Dimensions.get('window');

    return (
      <ScrollView
	  keyboardShouldPersistTaps='always'
	  keyboardDismissMode='on-drag'
	  >
        <View style={[styles.directionRow, styles.centerItemContent, {marginTop:height*.05, zIndex: 1}]}>
          <View style={[styles.directionRow]}>
            <View style={[appFunction.circleLogo(size)]}>
              <Image style= {[appFunction.circleLogoImage(size)]} source={require('../../../assets/icons/logo/login.png')} />
            </View>
          </View>
        </View>
        <View style={[styles.centerItemContent,{bottom:25, zIndex: 0}]}>
          <View style={[styles.loginMainContainer]}>
            <View style={{marginTop:height*.05,}} />
            <Text style={[styles.baseText]}>
              {title}
            </Text>
            <Text style= {[styles.textComponentStyle, styles.baseText]}></Text>
            {content}
            <View style={{height: height*.05}} />
          </View>
          {footer}
        </View>
      </ScrollView>
    );
  }
}
