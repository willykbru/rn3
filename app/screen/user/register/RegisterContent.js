import React, { Component } from 'react';
import { BackHandler, Alert, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import AuthTemplate from '../templates/AuthTemplate';
import firebase from 'react-native-firebase';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import CheckBox from 'react-native-check-box';
//import DeviceInformation from 'react-native-device-information';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';
import RNAmplitute from 'react-native-amplitude-analytics';

class RegisterContent extends Component{

  constructor(props) {
      super(props);
      this.state = {
        username: '',
        password: '',
        confirm_password: '',
        name: '',
        phone: '',
		securepass:true,
		securepasscf:true,
		token: '0',
      }
    }
	
  signOut = () => {
	   if(firebase.auth().currentUser){
       console.log('currentuser', firebase.auth().currentUser)
      firebase.auth().signOut();
     }
  }


    doRegister(data){
      if(!data.auth.username){
        appFunction.toastError('Email belum diisi');
      }else if(!data.auth.name){
        appFunction.toastError('Name belum diisi');
      }else if(!data.password){
        appFunction.toastError('Password belum diisi');
      }else if(!data.confirm_password){
        appFunction.toastError('Konfirmasi Password belum diisi');
      }else if(data.confirm_password != data.password){
        appFunction.toastError('Konfirmasi Password tidak sama dengan Password');
      }else if(!data.auth.phone){
        appFunction.toastError('Phone belum diisi');
      }else{

		this.props.doCheckEmail(data.auth);
		
      }
    }

/*
  async mergeLocalStorage( dataLogin ){
    console.log('done' + dataLogin);
    //const {typeVehicle, use, productionYear, typeCoverage, platArea, vehicleMerk, seriType, vehiclePrice } = this.state;

    console.log('ini datalogn' + dataLogin.username + dataLogin.isLogin+ dataLogin.visitorID + dataLogin.visitorName+ dataLogin.visitorPhone);
    //const { username, password } = this.state;


    let STORAGE_object = {
      dataLogin: dataLogin,
    };

    await AsyncStorage.mergeItem('STORAGE', JSON.stringify(dataLogin));
  }
*/

	componentWillReceiveProps(NextProps){
		if(this.props.dataRegister != NextProps.dataRegister)
			if(NextProps.dataRegister.phone)
				this.props.navigate('PhoneVerification');
	}

  onBackPress = () => {
    const {asuransiScreen} = this.props.screen;
    var screen = 'Main';
    if(asuransiScreen){
      screen= 'Login';
    }
    this.props.navigate(screen);
    return true;
  };

  componentDidMount() {	  
	  this.signOut();
	  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	  amplitude.logEvent('Halaman Register');
	  if(this.props.dataRegister.username){
		  this.setState({  username: this.props.dataRegister.username,
			name: this.props.dataRegister.name,
			phone: this.props.dataRegister.phone});
	  }
  }
  
  render() {
    var {height, width} = Dimensions.get('window');
    const {username, name, password, confirm_password, phone, token} = this.state;
    const passwordMD5 = appFunction.passwordMD5(password);
    let size = appFunction.width() / 4;

    var footer = [];
    var content = [];
    var title = 'Sign Up';
    const device = appFunction.deviceName();
    var auth ={
      username,
      password:passwordMD5,
      name,
      phone,
	  token,
    }
    const registered ={
      username,
      password:passwordMD5,
      device,
    }
    var data ={
      auth,
      registered,
      password,
      confirm_password,
    }

    //console.log(this.props.items);


    /*  if(itemResult){
        this.loginClicked(false);
      }*/
      //this.checkSession(itemResult.token);
  //  }this.props.navToActivation()

    footer.push(
      <View key ='footer' style={[styles.directionColumn, styles.centerItemContent]}>
		<View style={[styles.top10]} />
        <View style={[styles.directionRow]}>
          <Text style={[styles.baseText]}>{"Sudah Punya Akun ? "}</Text>
          <TouchableHighlight underlayColor='white'
            onPress={this.onBackPress}>
              <Text style={[styles.font14, styles.baseText, styles.underline, styles.bold, {color: 'rgb(204,0,0)'}]}>
                Login Sekarang
              </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
    content.push(
      <View key='content' style={[styles.centerItemContent, styles.width100p]}>
        <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5]}>
          <View style={[styles.width20p]}><Icon name='ios-mail' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Email'
			keyboardType='email-address'
			autoCapitalize='none'
            value={(username)}
            onChangeText={(username) => this.setState({username:appFunction.emailCharacterOnly(username)})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
          />
        </View>
        <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5, styles.top15]}>
          <View style={[styles.width20p]}><Icon name='ios-person' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Name'
			autoCapitalize='words'
            value={(name)}
            onChangeText={(name) => this.setState({name:appFunction.alphabetSpaceOnly(name)})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
          />
        </View>
        <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5, styles.top15]}>
          <View style={[styles.width20p]}><Icon name='ios-unlock' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Password'
			autoCapitalize='none'
            value={password}
            onChangeText={password => this.setState({password})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
            secureTextEntry={this.state.securepass}
          />
          <View style={[styles.width15p]}><TouchableHighlight onPress={()=>{this.setState({securepass:!this.state.securepass})}} underlayColor='transparent'>{ !this.state.securepass?<Image source={require('../../../assets/icons/component/show-password.png')} style={[styles.height25, styles.width25]}/>:<Image source={require('../../../assets/icons/component/hide-password.png')} style={[styles.height25, styles.width25]}/>}</TouchableHighlight></View>
        </View>
        <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5, styles.top15]}>
          <View style={[styles.width20p]}><Icon name='md-key' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Confirm Password'
			autoCapitalize='none'
            value={confirm_password}
            onChangeText={confirm_password => this.setState({confirm_password})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
            secureTextEntry={this.state.securepasscf}
          />
        <View style={[styles.width15p]}><TouchableHighlight onPress={()=>{this.setState({securepasscf:!this.state.securepasscf})}} underlayColor='transparent'>{ !this.state.securepasscf?<Image source={require('../../../assets/icons/component/show-password.png')} style={[styles.height25, styles.width25]}/>:<Image source={require('../../../assets/icons/component/hide-password.png')} style={[styles.height25, styles.width25]}/>}</TouchableHighlight></View>
        </View>
         <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5, styles.top15]}>
          <View style={[styles.width20p]}><Icon name='ios-phone-portrait' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Phone'
            value={(phone)}
            onChangeText={(phone) => this.setState({phone:appFunction.numericOnly(phone)})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
            maxLength={16}
            keyboardType='numeric'
          />
        </View>
        <View style={{height: height*.05}} />
        <TouchableHighlight
          style={[styles.btnLogin]}
          onPress={() => this.doRegister(data)}
          underlayColor='#fff'>
            <Text style={[styles.btnLoginText, styles.baseText]}>Register</Text>
        </TouchableHighlight>		
      </View>
    );

    return (
      <AuthTemplate title={title} content={content} footer={footer}/>
    );
	}
}

function mapStateToProps(state) {
  return {
    navReducer: state.navReducer,
    dataToken: state.dataToken,
    session: state.session,
	screen: state.screen,
	dataRegister: state.dataRegister,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContent);
