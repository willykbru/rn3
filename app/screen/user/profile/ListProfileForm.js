/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, CheckB } from 'react-native';

import { Header, CheckBox } from 'react-native-elements';

import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import {Icon, Button} from 'native-base';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { RkButton, RkType, RkTheme, RkChoiceGroup, RkChoice } from 'react-native-ui-kitten';

import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';
import ImagePicker from 'react-native-image-crop-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

var radio_kewarganegaraan = [
    { label: 'WNI', value: 'wni' },
    { label: 'WNA', value: 'wna' },
];

var radio_jenis_kelamin = [
    { label: 'Laki-Laki', value: 'L' },
    { label: 'Perempuan', value: 'P' },
];

var radio_jenis_identitas = [
    { label: 'KTP', value: '01' },
    { label: 'KITAS', value: '02' },
    { label: 'PASSPOR', value: '03' },
];

class ListProfileForm extends Component {

    constructor(props) {
        super(props);
        this.state={
            errors: {},
            name: '',
            address: '',
            phone: '',
            file_identitas: '',
            email: '',
            jenis_identitas: '01',
            jenisIdentitasIndex: 0,
            no_identitas: '',
            tempat_lahir: '',
            tanggal_lahir:'',
            jenis_kelamin: 'L',
            jenisKelaminIndex: 0,
            kewarganegaraan: 'wni',
            kewarganegaraanIndex: 0,
            negara_asal:'',
        }

        this.onFocus = this.onFocus.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeText = this.onChangeText.bind(this);

        this.nameRef = this.updateRef.bind(this, 'name');
        this.addressRef = this.updateRef.bind(this, 'address');
        this.phoneRef = this.updateRef.bind(this, 'phone');
        this.fileIdentitasRef = this.updateRef.bind(this, 'file_identitas');
        this.emailRef = this.updateRef.bind(this, 'email');
        this.idCardTypeRef = this.updateRef.bind(this, 'id_card_type');
        this.noIdentitasRef = this.updateRef.bind(this, 'no_identitas');
        this.tempatLahirRef = this.updateRef.bind(this, 'tempat_lahir');
        this.tanggalLahirRef = this.updateRef.bind(this, 'tanggal_lahir');
        this.jenisKelaminRef = this.updateRef.bind(this, 'jenis_kelamin');
        this.kewarganegaraanRef = this.updateRef.bind(this, 'kewarganegaraan');
        this.negaraAsalRef = this.updateRef.bind(this, 'negara_asal');

    }

    handleBackPress = () => {
        //this.goBack(); // works best when the goBack is async
        this.props.navigate('ListProfile');
        return true;
    }

    findValue = (val)=>{
        var fi = [{ value: '01', label: 'KTP' }, { value: '02', label: 'KITAS' }, { value: '03', label: 'PASSPOR' }];
        let i;
        var value;
        for(i=0; i< fi.length; i++){
            if(val === fi[i].value){
                value = fi[i].label;
            }
        }
        return value;
    
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.props.resetPesertaAsuransi();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }


    onChangeText(text) {
        try {
            ['name', 'address', 'phone', 'file_identitas', 'email', id_card_type, 'no_identitas', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'kewarganegaraan', 'negara_asal']
                .map((name) => ({ name, ref: this[name] }))
                .forEach(({ name, ref }) => {
                    if (ref.isFocused()) {
                        this.setState({ [name]: text });
                    }
                });
        } catch (error) {
            console.log('error', error);
        }
    }

    onFocus() {
        let { errors = {}, ...data } = this.state;


        for (let name in errors) {
            let ref = this[name];

            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }
        this.setState({ errors });
    }

    async onSubmit() {
        const { listDataGambarSim } = this.props;
        console.log('sim', listDataGambarSim);

        let errors = {};
        let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error


        //console.log('detailPolis2', detailPolis);
        ['name', 'address', 'phone', 'file_identitas', 'email', id_card_type, 'no_identitas', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'kewarganegaraan', 'negara_asal']
            .forEach((name) => {
                let value = this[name].value();

                if (!value) {
                    errors[name] = 'Tidak boleh kosong';
                    isError = 1;
                }
            });

        this.setState({ errors });
        // console.log('errors', errors);

        // console.log('isError',isError);
        if (isError == 0 && this.state.periode_sim_pengemudi != '' && listDataGambarSim[0].path != null) {


            await this.setStategambarSim(listDataGambarSim[0].path);

            /*
            RNFS.readFile(file.substring(7), "base64")  //substring(7) -> to remove the file://
      .then(res => this.setState({base64: res}))
            */

            const base64image = await RNFS.readFile(listDataGambarSim[0].path.substring(7), 'base64');
            // console.log('base 64 state 2 : ',base64image);
            temp = {
                nama_pengemudi: this.state.nama_pengemudi,
                alamat_pengemudi: this.state.alamat_pengemudi,
                no_sim_pengemudi: this.state.no_sim_pengemudi,
                periode_sim_pengemudi: this.state.periode_sim_pengemudi,
                // file_foto_sim_pengemudi : this.state.file_foto_sim,
                file_foto_sim_pengemudi: base64image,
                tertanggung_tahu: this.state.tertanggung_tahu,
                pengemudi_bekerja: this.state.pengemudi_bekerja,

            }

            await this.props.setDataClaimMV(temp);
            this.props.navigate('ClaimMVPenumpangSaksi');
        } else {
            Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
        }
    }

    onSubmitNama_pengemudi() {

    }

    onSubmitAlamat_pengemudi() {

    }

    onSubmitNo_sim_pengemudi() {

    }

    updateRef(name, ref) {
        this[name] = ref;
    }

    uploadSim() {
        // const {listDataGambarSim} = this.props;

        let imageSim = [];

        ImagePicker.openCamera({
            cropping: true,
            width: 1000,
            height: 1000,
            includeExif: false,
            avoidEmptySpaceAroundImage: true,

        }).then(image => {
            console.log('received image', image);
            ImageResizer.createResizedImage(image.path, image.width, image.height, 'JPEG', 100, 0).then((response) => {

                image = {
                    'size': response.size,
                    'path': response.uri,
                    'caption': '',
                };

                this.props.setDataGambarSimKameraTemp(image);

            });

        }).catch(e => alert(e));
    }

    updateTanggal(tanggal) {

        this.setState({
            tanggal_lahir: tanggal,
            isPicked: true,
        });
        // console.log(this.state.periode_sim_pengemudi);
    }

    render() {
        const { listDataGambarSim } = this.props;
        const { claimState, claimLabelState, dataClaim, detailPolis } = this.props;
        // console.log(listDataGambarSim);
        let { errors = {}, ...data } = this.state;
        
        // console.log(dataClaim);
        console.log('jenis_identitas', this.state.jenis_identitas);
        console.log('jenisIdentitasIndex', this.state.jenisIdentitasIndex);

        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        var maxDate = new Date(year + 5, 12, 31);
        var minDate = new Date();

        // console.log('Detail Polis ', JSON.stringify(detailPolis));

        return (
            <View>
                <Header
                    backgroundColor={'red'}
                    leftComponent={<Button transparent onPress={this.handleBackPress}><Icon style={{color:'white'}} name='ios-arrow-back' /></Button>}
                    centerComponent={{ text: 'UBAH PROFIL', style: { color: 'white', padding: 12 } }}
                />
               
                <KeyboardAwareScrollView keyboardDismissMode="interactive"
                    keyboardShouldPersistTaps="always"
                    getTextInputRefs={() => {
                        return [this.nama_pengemudiRef, this.alamat_pengemudiRef, this.no_sim_pengemudiRef];
                    }}
                >
                    <ScrollView style={styles.container}>
                        <View style={styles.row}>
                            <View style={styles.icon}><FontAwesome name='user-o' color='rgba(0, 0, 0, .38)' size={25}  /></View>
                            <View style={styles.field}>
                                <TextField
                                    ref={this.nameRef}
                                    label='Nama'
                                    keyboardType='default'
                                    onFocus={this.onFocus}
                                    onSubmitEditing={this.onSubmitName}
                                    onChangeText={this.onChangeText}
                                    returnKeyType='next'
                                    tintColor='#228B22'
                                    error={errors.name}
                                    maxLength={40}
                                    value={this.state.name}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.icon}><FontAwesome name='address-book-o' color='rgba(0,0,0, .38)' size={25} /></View>
                            <View style={styles.field}>
                                <TextField
                                    ref={this.addressRef}
                                    label='Alamat'
                                    keyboardType='default'
                                    onFocus={this.onFocus}
                                    onSubmitEditing={this.onSubmitAddress}
                                    onChangeText={this.onChangeText}
                                    returnKeyType='next'
                                    tintColor='#228B22'
                                    error={errors.address}
                                    maxLength={80}
                                    value={this.state.address}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.icon}><FontAwesome name='tablet' color='rgba(0,0,0, .38)' size={25} /></View>
                            <View style={styles.field}>
                                <TextField
                                    label='Handphone'
                                    keyboardType='numeric'
                                    ref={this.phoneRef}
                                    maxLength={12}
                                    value={this.state.phone}
                                    onChangeText={this.onChangeText}
                                    onSubmitEditing={this.onSubmitPhone}
                                    returnKeyType='next'
                                    tintColor='#228B22'
                                    error={errors.phone}
                                    onFocus={this.onFocus}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.icon}><FontAwesome name='envelope-o' color='rgba(0,0,0, .38)' size={25} /></View>
                            <View style={styles.field}>
                                <TextField
                                    ref={this.EmailRef}
                                    label='Email'
                                    keyboardType='default'
                                    onFocus={this.onFocus}
                                    onSubmitEditing={this.onSubmitEmail}
                                    onChangeText={this.onChangeText}
                                    returnKeyType='next'
                                    tintColor='#228B22'
                                    error={errors.email}
                                    maxLength={80}
                                    value={this.state.email}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.icon}><FontAwesome name='map-marker' color='rgba(0,0,0, .38)' size={25} /></View>
                            <View style={styles.field}>
                                <TextField
                                    ref={this.tempatLahirRef}
                                    label='Tempat Lahir'
                                    keyboardType='default'
                                    onFocus={this.onFocus}
                                    onSubmitEditing={this.onSubmitTempatLahir}
                                    onChangeText={this.onChangeText}
                                    returnKeyType='next'
                                    tintColor='#228B22'
                                    error={errors.tempat_lahir}
                                    maxLength={80}
                                    value={this.state.tempat_lahir}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.icon}><FontAwesome name='calendar' size={25} color='rgba(0, 0, 0, .38)' /></View>
                            <View style={styles.fieldTanggal}>
                                <DatePicker
                                    style={{ width: wp('80%'), }}
                                    placeholder="Tanggal Lahir"
                                    mode="date"
                                    date={this.state.tanggal_lahir == null ? null : this.state.tanggal_lahir}
                                    androidMode="spinner"
                                    format="YYYY-MM-DD"
                                    minDate={minDate}
                                    maxDate={maxDate}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {

                                        },
                                        dateInput: {
                                            borderWidth: 0,
                                            alignItems: 'flex-start',
                                            paddingTop: 15,
                                            borderBottomWidth: .5,
                                        },
                                        dateText: {
                                            fontSize: 18,
                                            //color: 'rgba(0,0,0, .38)'
                                        }

                                        // ... You can check the source to find the other keys.
                                    }}
                                    showIcon={false}
                                    onDateChange={(tanggal) => { this.updateTanggal(tanggal) }}
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.text}>
                                <Text>Jenis Kelamin</Text>
                                <RadioForm
                                    formHorizontal={true}
                                    animation={true}
                                    initial='L'
                                >
                                    {radio_jenis_kelamin.map((obj, i) => {
                                        var onPress1 = (value, index) => {
                                            this.setState({
                                                jenisKelaminIndex: index,
                                                jenis_kelamin: value,
                                            })
                                        }
                                        return (
                                            <RadioButton labelHorizontal={true} key={i} >
                                                {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                                <RadioButtonInput
                                                    obj={obj}
                                                    index={i}
                                                    isSelected={this.state.jenisKelaminIndex === i}
                                                    onPress={onPress1}
                                                    buttonStyle={{}}
                                                    buttonWrapStyle={{ marginRight: 10 }}
                                                />
                                                <RadioButtonLabel
                                                    obj={obj}
                                                    index={i}
                                                    onPress={onPress1}
                                                    labelWrapStyle={{ marginRight: (width / 6) - 20 }}
                                                />
                                            </RadioButton>
                                        )
                                    })}
                                </RadioForm>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.text}>
                                <Text>Kewarganegaraan</Text>
                                <RadioForm
                                    formHorizontal={true}
                                    animation={true}
                                    initial='wni'
                                >
                                    {radio_kewarganegaraan.map((obj, i) => {
                                        var onPress1 = (value, index) => {
                                            this.setState({
                                                kewarganegaraanIndex: index,
                                                kewarganegaraan: value,
                                            })
                                        }
                                        return (
                                            <RadioButton labelHorizontal={true} key={i} >
                                                {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                                <RadioButtonInput
                                                    obj={obj}
                                                    index={i}
                                                    isSelected={this.state.kewarganegaraanIndex === i}
                                                    onPress={onPress1}
                                                    buttonStyle={{}}
                                                    buttonWrapStyle={{ marginRight: 10 }}
                                                />
                                                <RadioButtonLabel
                                                    obj={obj}
                                                    index={i}
                                                    onPress={onPress1}
                                                    labelWrapStyle={{ marginRight: (width / 6) - 20 }}
                                                />
                                            </RadioButton>
                                        )
                                    })}
                                </RadioForm>
                            </View>
                        </View>
                        
                        <View style={styles.row}>
                            <View style={styles.text}>
                                <Text>Jenis Identitas</Text>
                                <RadioForm
                                    formHorizontal={true}
                                    animation={true}
                                    initial='01'
                                >
                                    {radio_jenis_identitas.map((obj, i) => {
                                        var onPress1 = (value, index) => {
                                            this.setState({
                                                jenisIdentitasIndex: index,
                                                jenis_identitas: value,
                                            })
                                        }
                                        return (
                                            <RadioButton labelHorizontal={true} key={i} >
                                                {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                                <RadioButtonInput
                                                    obj={obj}
                                                    index={i}
                                                    isSelected={this.state.jenisIdentitasIndex === i}
                                                    onPress={onPress1}
                                                    buttonStyle={{}}
                                                    buttonWrapStyle={{ marginRight: 10 }}
                                                />
                                                <RadioButtonLabel
                                                    obj={obj}
                                                    index={i}
                                                    onPress={onPress1}
                                                    labelWrapStyle={{ marginRight: (width / 6) - 20 }}
                                                />
                                            </RadioButton>
                                        )
                                    })}
                                </RadioForm>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.fieldImage}>
                                <Image source={require('../../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} /> 
                            </View>
                            <View style={styles.fieldButton}>
                                <RkButton style={{ flexWrap: 'wrap', height: height * .095}} rkType='danger' onPress={() => this.uploadSim()}>Unggah Foto {this.findValue(this.state.jenis_identitas)}</RkButton>
                            </View>
                        </View>
                        
                        

                        <View style={styles.button}>

                            <RkButton rkType='danger' onPress={this.onSubmit}>Simpan</RkButton>

                        </View>
                    </ScrollView>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        elevation: 10,
    },
    container: {
        marginBottom: 20,
        marginTop: 0,
        height: hp('100%') - 80,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    field: {
        flex: 9,
        paddingLeft: 10,
    },
    fieldButton: {
        width: (width * 0.5) - 20,
        height: width * 0.4,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: .5,
    },
    fieldImage: {
        width: width * 0.5,
        height: width * 0.4,
        borderWidth: .5,

    },
    fieldTanggal: {
        flex: 9,
        paddingLeft: 10,
        paddingBottom: 15
    },
    text: {

    },
    textPicker: {
        height: 56,
        fontSize: 16,
        color: 'rgba(0,0,0, .38)',
        borderBottomWidth: .5,
        borderColor: 'rgba(0,0,0, .38)',
        paddingTop: 20,
    },
});


function mapStateToProps(state) {
    return {
        pesertaAsuransi: state.pesertaAsuransi,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListProfileForm);
