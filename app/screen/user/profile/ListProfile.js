import React, { Component } from 'react';
import { BackHandler, Alert, Text, WebView, View, StyleSheet, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';

import Slide from '../../../animated/Slide';
import * as appFunction from '../../../function';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import LoadingPage from '../../../components/loading/LoadingPage';

class ListProfile extends Component {

    constructor(props) {

        super(props);
        this.state={remove: false}

    }

    onBackPress = () => {
        var nscreen = 'Main';

        this.props.navigate(nscreen);
        return true;
    }; 

    lihatProfile(){
        var nscreen = 'LihatProfile';
        //this.props.getProfile(id);
        //console.log(id);
        this.props.navigate(nscreen);
        //return true;
    }

    renderProfile(listPesertaAsuransi, remove){
        var viewProfile = [];
        console.log(listPesertaAsuransi);
        
        let i;
        if (listPesertaAsuransi.length > 0){
            for (i = 0; i < listPesertaAsuransi.length; i++){
                viewProfile.push(
                    <View style={[{ backgroundColor: 'white', borderBottomWidth: .05 }, styles.marTop, styles.directionRow, styles.elev3]} key={i}>

                        <View style={[styles.width15p, styles.right10, styles.centerItemContent]}>
                            {listPesertaAsuransi[i].status_profile == '2' ? remove ? <Button style={[styles.bgRed]} onPress={() => null}><Icon style={[styles.colorWhite]} name='md-trash' /></Button> : null : null}
                        </View>
                        <View style={[styles.width70p, { paddingTop: 15, paddingBottom: 15 }]}>
                            <Text>{listPesertaAsuransi[i].name}</Text>
                        </View>
                        <View style={[styles.width15p, styles.right10, styles.centerItemContent]}>
                            <Button transparent onPress={() => {this.lihatProfile()}}><Text style={[styles.baseText, { color: 'forestgreen' }]}>LIHAT</Text></Button>
                        </View>
                    </View>
                )
            }
        }else{
            viewProfile.push(
                <View style={[{ backgroundColor: 'white', borderBottomWidth: .05 }, styles.marTop, styles.directionRow, styles.elev3, styles.centerItemContent]}>

                    <View style={[styles.width70p, { paddingTop: 15, paddingBottom: 15 }]}>
                        <Text>Belum Ada Profil</Text>
                    </View>
                    
                </View>
            )
        }
        return viewProfile;
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    componentWillMount(){
        this.props.getProfileList(this.props.session.username);
    }

    componentDidMount() {

        const event_name = 'Halaman Rumah Sakit Provider';

        var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
        appFunction.setScreenGA(event_name, userID);

        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    render() {
        const title = 'DAFTAR PROFIL';
        const { dataSearch, listPesertaAsuransi } = this.props;
        const { remove } = this.state;
        var myUrl = appFunction.webJagain + 'crs';

        //const dSearch
        const asimgsrc = {
            'ADIRA': require('../../../assets/icons/asuransi/adira.png'),
            'ALLIANZ': require('../../../assets/icons/asuransi/allianz.png'),
            'ASOKAMAS': require('../../../assets/icons/asuransi/asoka.png'),
            'AVRIST': require('../../../assets/icons/asuransi/avrist.png'),
            'AXA': require('../../../assets/icons/asuransi/axa.png'),
            'FPG': require('../../../assets/icons/asuransi/fpg.png'),
            'MAG Fairfax': require('../../../assets/icons/asuransi/mag.jpg'),
            'SIMASNET': require('../../../assets/icons/asuransi/simasnet.png'),
            'ASURANSI SINARMAS': require('../../../assets/icons/asuransi/sinarmas.png'),
            'ZURICH': require('../../../assets/icons/asuransi/zurich.png'),
            'SIMAS JIWA': require('../../../assets/icons/asuransi/simasjiwa.jpg'),
        }
        //Alert.alert('oke',dataSearch);


        return (
            <Container>
                <Slide>
                    <Header
                        backgroundColor={'white'}
                        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
                        centerComponent={{ text: title, style: { color: 'red', padding: 12 } }}
                        rightComponent={<View>{!remove ? <Button transparent onPress={() => this.setState({ remove: true })}><Text style={[styles.baseText]}>HAPUS</Text></Button> : <Button transparent onPress={() => this.setState({ remove: false })}><Text style={[styles.baseText]}>BATAL</Text></Button>}</View>}
                    />
                    {this.renderProfile(listPesertaAsuransi, remove)}
                    
                    
                </Slide>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        screen: state.screen,
        dataSearch: state.dataSearch,
        status: state.status,
        session: state.session,
        listPesertaAsuransiDummy: state.listPesertaAsuransiDummy,
        listPesertaAsuransi: state.listPesertaAsuransi,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListProfile);
