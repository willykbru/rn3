import React, { Component } from 'react';
import { BackHandler, Alert, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
//import DeviceInformation from 'react-native-device-information';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';
import RNAmplitute from 'react-native-amplitude-analytics';

class ProfileContent extends Component{

  constructor(props) {
      super(props);
  	}

    doChangePassword(username){
      this.props.sendVerify(username);
      this.props.navToChangePassword();
    }

    componentDidMount() {
	  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	  amplitude.logEvent('Halaman Profile');
	  
	  
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	
		appFunction.setScreenGA('Halaman Profile', userID);	
    }

	render() {
    var {height, width} = Dimensions.get('window');
    //let size = appFunction.width() / 4;
    const { session } = this.props;
    const { token } = this.props;

    return (
      <ScrollView>
        <View style={[styles.bgWhite, styles.top5, styles.elev5]}>
          <Text style={[styles.bold, styles.font16, styles.left10, styles.baseText]}>Profil Saya</Text>
          <Text style={[styles.bold, styles.left30, styles.font14, styles.top5, styles.baseText]}>{session.visitor_name}</Text>
          <Text style={[styles.top5, styles.left30, styles.bottom5, styles.baseText]}>{session.username}</Text>
          <Text style={[styles.top5, styles.left30, styles.bottom5, styles.baseText]}>{session.visitor_phone}</Text>
        </View>
        <View style={[styles.bgWhite, styles.top10, styles.elev5]}>
          <TouchableHighlight
            style={[styles.padTop20, styles.padBot20, styles.borderbottom1, styles.borderColorBotSilver]}
            onPress={() => this.doChangePassword(session.username)}
            underlayColor='#fff'>
            <View style={[styles.directionRow]}>
              <View style={[styles.width10p]}><Icon name='ios-key-outline' style={[styles.colorGrey, styles.left10, styles.font18]} /></View>
              <View style={[styles.width90p]}><Text style={[styles.baseText, styles.left20]}>Ubah Password</Text></View>
            </View>
          </TouchableHighlight>
           <TouchableHighlight
            style={[styles.padTop20, styles.padBot20]}
            onPress={() => this.props.navigate('ListProfile')}
            underlayColor='#fff'>
            <View style={[styles.directionRow]}>
              <View style={[styles.width10p]}><Icon name='ios-people-outline' style={[styles.colorGrey, styles.left10, styles.font18]} /></View>
              <View style={[styles.width90p]}><Text style={[styles.baseText, styles.left20]}>Daftar Profile Peserta Asuransi</Text></View>
            </View>
          </TouchableHighlight>
        </View>
		<View style={[styles.bgWhite, styles.top10, styles.elev5]}>
          <TouchableHighlight
           style={[styles.padTop20, styles.padBot20, styles.borderbottom1, styles.borderColorBotSilver]}
            onPress={() => this.props.navigate('Agreement')}
            underlayColor='#fff'>
            <View style={[styles.directionRow]}>
              <View style={[styles.width10p]}><Icon name='ios-information-circle-outline' style={[styles.colorGrey, styles.left10, styles.font18]} /></View>
              <View style={[styles.width90p]}><Text style={[styles.baseText, styles.left20]}>Syarat dan Ketentuan</Text></View>
            </View>
          </TouchableHighlight>
		  <TouchableHighlight
            style={[styles.padTop20, styles.padBot20]}
            onPress={() => this.props.navigate('PrivacyPolicy')}
            underlayColor='#fff'>
            <View style={[styles.directionRow]}>
              <View style={[styles.width10p]}><Icon name='ios-document-outline' style={[styles.colorGrey, styles.left10, styles.font18]} /></View>
              <View style={[styles.width90p]}><Text style={[styles.baseText, styles.left20]}>Kebijakan Privasi</Text></View>
            </View>
          </TouchableHighlight>
        </View>
        <View style={[styles.bgWhite, styles.top10, styles.centerItemContent, styles.elev5]}>
          <TouchableHighlight
            style={[styles.padTop20, styles.padBot20, styles.centerItemContent]}
            onPress={() => this.props.doLogout(token)}
            underlayColor='#fff'>
            <View style={[styles.directionRow, styles.centerItemContent]}>
              <View style={{paddingRight: 10}}><Icon name='md-exit' style={[styles.colorRed, styles.left10, styles.font20]} /></View>
              <View><Text style={[styles.colorRed, styles.baseText]}>Logout</Text></View>
              </View>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
	}
}

function mapStateToProps(state) {
  return {
    token: state.token,
    session: state.session,
	status: state.status,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContent);
