import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import styles from '../../../assets/styles';
import Slide from '../../../animated/Slide';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import LoginContent from './LoginContent';

import * as appFunction from '../../../function';

class Login extends React.Component {

  constructor(props) {
    super(props);
  }

  onBackPress = () => {
    var screen = 'ListAsuransi';
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
    //this.props.getTokenLocalStorage().done();
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

render () {
  const title = 'Login';
  //console.log(screen);
  //const {verify} = this.props.status;
  //const {fpScreen} = this.props.screen;
  //console.log(fpScreen);
  //console.log(fpScreen);

  return(
    <Container>
  	  <Slide>
  		  <Header
  			backgroundColor={'red'}
  			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
  			centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
  		  />
  		  <Content>
  			    <LoginContent />
  		  </Content>
  	  </Slide>
    </Container>
  )
  //end of return
}
//end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    navReducer: state.navReducer,
    status: state.status,
    screen: state.screen,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
