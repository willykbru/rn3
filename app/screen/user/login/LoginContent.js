import React, { Component } from 'react';
import { BackHandler, Alert, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import AuthTemplate from '../templates/AuthTemplate';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import SubmitButton from '../../../components/button/SubmitButton';

import CheckBox from 'react-native-check-box';
//import DeviceInformation from 'react-native-device-information';

import * as appFunction from '../../../function';
import styles from '../../../assets/styles';
import RNAmplitute from 'react-native-amplitude-analytics';

class LoginContent extends Component{

  constructor(props) {
      super(props);
      this.state = {
        username: '',
        password: '',
        rememberMe:false,
		securepass: true,

      }
    }

    doLogin(data){
		
      if(!data.isLogin){
        if(data.auth.username && data.auth.password){
          this.props.doLogin(data.auth, this.props.screen.asuransiScreen);
        }else{
          appFunction.toastError('Email / Password belum diisi');
        }
      }
    }
/*
    componentWillReceiveProps(NextProps){
      if(NextProps.status.isLogin != this.props.status.isLogin){
        if(NextProps.status.isLogin){
          if(this.props.screen.asuransiScreen){
            //console.log('navigate', );
            this.props.navigate('DetailBeli');
          }
        }
      }
    }
*/
	componentDidMount() {
		const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
		amplitude.logEvent('Halaman Login');
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	
		appFunction.setScreenGA('Halaman Login', userID);
			
		
		
		
	}

/*
  async mergeLocalStorage( dataLogin ){
    console.log('done' + dataLogin);
    //const {typeVehicle, use, productionYear, typeCoverage, platArea, vehicleMerk, seriType, vehiclePrice } = this.state;

    console.log('ini datalogn' + dataLogin.username + dataLogin.isLogin+ dataLogin.visitorID + dataLogin.visitorName+ dataLogin.visitorPhone);
    //const { username, password } = this.state;


    let STORAGE_object = {
      dataLogin: dataLogin,
    };

    await AsyncStorage.mergeItem('STORAGE', JSON.stringify(dataLogin));
  }
*/


	render() {
    var {height, width} = Dimensions.get('window');
    const {username, password, rememberMe} = this.state;
    const passwordMD5 = appFunction.passwordMD5(password);
    let size = appFunction.width() / 4;
    const {isLogin} = this.props.status;
    var footer = [];
    var content = [];
    var title = 'Sign In';
    const auth ={
      username,
      password:passwordMD5,
      device: 'samsung',
    }
    const data = {
      auth,
      isLogin,
    }

    //console.log(this.props.items);


    /*  if(itemResult){
        this.loginClicked(false);
      }*/
      //this.checkSession(itemResult.token);
  //  }

    footer.push(
      <View key ='footer' style={[styles.directionColumn, styles.centerItemContent]}>
        <View style={[styles.top10]} />
        <View style={[styles.directionRow]}>
          <Text style={[styles.baseText]}>{"Belum Punya Akun ? "}</Text>
          <TouchableHighlight underlayColor='white'
            onPress={() => this.props.navToRegister()}>
              <Text style={[styles.font14, styles.baseText, styles.underline, styles.bold, {color: 'rgb(204,0,0)'}]}>
                Daftar Sekarang
              </Text>
          </TouchableHighlight>
        </View>
		<View style={[styles.top10]} />
		<View style={[styles.directionRow]}>
          <Text style={[styles.baseText]}>{"Akun Belum Diaktivasi ? "}</Text>
          <TouchableHighlight underlayColor='white'
            onPress={() => this.props.navToActivation()}>
              <Text style={[styles.font14, styles.baseText, styles.underline, styles.bold, {color: 'rgb(204,0,0)'}]}>
                Aktivasi Sekarang
              </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
    content.push(
      <View key='content' style={[styles.centerItemContent, styles.width100p]}>
        <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5]}>
          <View style={[styles.width20p]}><Icon name='ios-mail' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Email'
			keyboardType='email-address'
			autoCapitalize='none'
            value={(username)}
            onChangeText={(username) => this.setState({username:appFunction.emailCharacterOnly(username)})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
          />
        </View>
        <View style={[styles.directionRow, styles.bgWhite, styles.width80p, styles.radius5, styles.top15]}>
          <View style={[styles.width20p]}><Icon name='ios-lock' style={[styles.colorGrey, styles.right10, styles.left10]} /></View>
          <TextInput
            placeholder='Password'
			autoCapitalize='none'
            value={password}
            onChangeText={password => this.setState({password})}
            underlineColorAndroid='transparent'
            style={[styles.baseText,styles.width65p, styles.padv0]}
            secureTextEntry={this.state.securepass}
          />
          <View style={[styles.width15p]}><TouchableHighlight onPress={()=>{this.setState({securepass:!this.state.securepass})}} underlayColor='transparent'>{ !this.state.securepass?<Image source={require('../../../assets/icons/component/show-password.png')} style={[styles.height25, styles.width25]}/>:<Image source={require('../../../assets/icons/component/hide-password.png')} style={[styles.height25, styles.width25]}/>}</TouchableHighlight></View>
        </View>
        <View style={{height: height*.05}} />
		<SubmitButton title='Login' onPress={() => this.doLogin(data)} />
        <View style={{height: height*.03}} />
        <View style={[styles.width80p, styles.directionRow, styles.centerItemContent]}>
          <View style={[styles.width60p, styles.centerItemContent]}>
            <Button transparent style={{height: height*.03}}  onPress={() => {this.props.navToForgotPassword()}}>
              <Text style={[styles.baseText, styles.bold, styles.colorRed]}>Lupa Password?</Text>
            </Button>
          </View>
        </View>
      </View>
    );

    return (
      <AuthTemplate title={title} content={content} footer={footer}/>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
    status:state.status,
    dataToken: state.dataToken,
    session: state.session,
    itemsLoading: state.itemsLoading,
    loginClicked: state.loginClicked,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContent);
