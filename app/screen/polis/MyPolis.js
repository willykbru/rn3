import React, { Component } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Container, Content, ScrollableTab, Tab, Tabs, TabHeading, Icon, Text, Button, Footer } from 'native-base';
//import Cart from '../../../components/template/cart';
import PolisValid from './PolisValid';
import PolisExpired from './PolisExpired';
import { Header } from 'react-native-elements';

import FAIcon from 'react-native-vector-icons/FontAwesome'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

class MyPolis extends Component {
    onBackPress = () => {
        var screen = 'Main';
        this.props.navigate(screen);
        return true;
    };

    componentWillUnmount() {
        this.props.resetClaimPolis(); // untuk mereset ajukan klaim
    }

    componentDidMount(){
        const { username } = this.props.session;
        this.props.getSuccessPayment(username);
        this.props.setLoadingClaimMenu(true); // loading untuk claim menu
    }

    getListKlaimPolis(listSuccessPayment) {

    // console.log('test', listSuccessPayment);

        for (let i = 0; i < listSuccessPayment.length; i++) {

            // this.cekClaimAll(listSuccessPayment[i].policyno);
            this.props.cekClaimAll(listSuccessPayment[i].policyno);
        }


    }

    componentWillReceiveProps(NextProps) {
             
        
        if (this.props.listSuccessPayment != NextProps.listSuccessPayment) {
           // console.log('listSuccessPayment',NextProps.listSuccessPayment);


            if (NextProps.listSuccessPayment.length > 0) {

                this.getListKlaimPolis(NextProps.listSuccessPayment);
            }
            this.forceUpdate();
        }

        if (this.props.claimPolis != NextProps.claimPolis) {
            if (NextProps.claimPolis.length == this.props.listSuccessPayment.length) {
                this.props.setLoadingClaimMenu(false);
            }
            // console.log('listSuccessPayment', NextProps.listSuccessPayment)
            // console.log('claimPolis',NextProps.claimPolis);
            this.forceUpdate();

        }

        if (this.props.loadingClaimMenu != NextProps.loadingClaimMenu) {
            this.forceUpdate();
        }



    }


    render() {
        console.log(this.props.loadingClaimMenu);
        return (

            <Container>
                <Header
                    backgroundColor={'white'}
                    centerComponent={{ text: 'My Polis', style: { color: 'red', padding: 10 } }}
                />
                <Tabs
                    locked={true}
                    initialPage={0}  tabBarUnderlineStyle={{ borderBottomWidth: 2, borderBottomColor: 'white' }} tabBarInactiveTextColor="white">
                    
                    <Tab heading={<TabHeading style={{ backgroundColor: 'red' }}><FAIcon size={15} style={{color: 'forestgreen'}} name="book" /><Text style={styles.tabsText}>Polis Valid</Text></TabHeading>}>
                        <PolisValid />
                    </Tab>
                    <Tab heading={<TabHeading style={{ backgroundColor: 'red' }}><FAIcon size={15} style={{ color: 'white' }} name="book" /><Text style={styles.tabsText}>Polis Expired</Text></TabHeading>}>
                        <PolisExpired />
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    tabsText: {
        fontSize: 12,
        color: 'white'
    },
    headerColor: {
        backgroundColor: '#1b1b1b'
    },
    activeTab: {
        borderBottomColor: '#fff'
    }
});


function mapStateToProps(state) {
    return {
        listSuccessPayment: state.listSuccessPayment,
        claimPolis: state.claimPolis,
        loadingClaimMenu: state.loadingClaimMenu,
        screen: state.screen,
        status: state.status,
        session: state.session,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPolis);