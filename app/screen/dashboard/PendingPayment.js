import React, { Component } from 'react';
import { Alert, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../function';
import styles from '../../assets/styles';
import LoadingPage from '../../components/loading/LoadingPage';

import DetailPendingPayment from './DetailPendingPayment';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import Accordion from 'react-native-collapsible/Accordion';
import Icon2 from 'react-native-vector-icons/Ionicons';


class PendingPayment extends Component{

  constructor(props) {
    super(props);
	}
  


    renderTotalPremi(listPendingPayment){
   	   var listTransaksi = [];


		listTransaksi.push(
			<View style={[styles.centerItemContent, styles.pad20]} key = 'datatransaksi-kosong'>
			  <Text style={[styles.baseText, styles.alignCenter]}>Total Premi</Text>
			</View>
		);
	
      	return listTransaksi;
	  
	  
	}
		
	componentWillUnmount(){
		this.props.resetListPendingPayment();
	}

	componentWillMount(){
		this.props.getPendingPayment(this.props.session.username);
	}
	
	
  componentDidMount(){
    const {username} = this.props.session;
    console.log(username);
	this.props.resetDataPembayaran();
  }
  
  deleteIcon(listPendingPayment){
	const {token, session} = this.props;
	
	return( 
		<View style={{position:'absolute', right: 0, top: 10}}>
			<TouchableHighlight underlayColor='transparent'  onPress={() => Alert.alert(
																		  'Hapus Pemesanan Asuransi?',
																		  '1 Pemesanan Asuransi akan dihapus dari keranjang belanja',
																		[
																			{text: 'Hapus', onPress: () => this.props.doDeleteCart(session.username, token, listPendingPayment.kode_transaksi)},
																			{text: 'Batal', onPress: () => console.log('Ask me later pressed')},
																		])}>
				<Icon2 name="ios-trash-outline" style={{textShadowOffset:{width:2, height:1}, shadowColor:'#000000', shadowOpacity:0.8}} size={26} color="#FF1F17" />
			</TouchableHighlight>
		</View>
	)
  }
  
  paymentTitle(listPendingPayment){
	  return(
	  <View style={[styles.directionRow, styles.centerItemContent, {marginTop: 15}]}>
			{ listPendingPayment.kode_transaksi.includes("PR") ? <Text style={[styles.baseText, styles.bold]}>ASURANSI RUMAH TINGGAL</Text> : 
			listPendingPayment.kode_transaksi.includes("MV")? <Text style={[styles.baseText, styles.bold]}>ASURANSI KENDARAAN</Text>: listPendingPayment.kode_transaksi.includes("AP")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI APARTEMEN</Text> : listPendingPayment.kode_transaksi.includes("AC")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI KECELAKAAN</Text> : listPendingPayment.kode_transaksi.includes("HT")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI KESEHATAN</Text> : listPendingPayment.kode_transaksi.includes("LF")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI JIWA</Text> : listPendingPayment.kode_transaksi.includes("TR")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI TRAVEL</Text> : listPendingPayment.kode_transaksi.includes("DL")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI DELAY</Text> : listPendingPayment.kode_transaksi.includes("CG")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI CARGO</Text> : listPendingPayment.kode_transaksi.includes("BC")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI SEPEDA</Text> : listPendingPayment.kode_transaksi.includes("PT")? 
			<Text style={[styles.baseText, styles.bold]}>ASURANSI HEWAN</Text> : null }
		</View>
	  )
	  
	}

	printdate(value){
		var options = { year: 'numeric', day: '2-digit', month: '2-digit', timeZone: "Asia/Jakarta" };
		return (
		<View style={{ marginBottom: 5 }}>
				<Text>{value[0].input_date}</Text>
		</View>
		);
	}
	
	printorderid(value){
		//console.log("order_id",order_id);
		var orderView= [];
		var i;
		if (value.length > 0){
			for (i = 0; i < value.length; i++) {
				orderView.push(
					<View style={{ marginBottom: 5 }} key={i}>
						<Text>{value[i].order_id}</Text>
					</View>
				)
			}
		}
		return orderView;
	}


	renderPendingPayment(listPendingPayment){
		var count = 0;
		//var dataPembayaran = null;
		var i;
		var pendingPayment = [];
			if (listPendingPayment.size > 0) {
				listPendingPayment.forEach(function (value, key, map) {
					
						//console.log('fa',value[0].link_pembayaran);
						//dataPembayaran  = {link_pembayaran: value[0].link_pembayaran, payment_id: key, description: "success", status: "Y"};
						pendingPayment.push(
							<View style={[styles.centerItemContent, styles.width100p]} key={key}>
								
							<View style={[styles.bgWhite, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.radius5]}>
									<View style={[styles.bgWhite, styles.width100p]}>
										<View style={[styles.top5, styles.bottom5, styles.left10]}>
											{this.printdate(value)}
										</View>
										<View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
											<View style={{ width: '90%', borderBottomWidth: 1, borderBottomColor: '#e6e6e6' }} />

										</View>
									</View>
									<View style={[styles.centerItemContent, styles.width90p]}>
										<View style={{marginTop: 10}}/>
										<View style={[styles.directionRow, styles.left5, styles.bottom5, styles.top10]}>
										  <View style={[styles.width80p, styles.centerItemContent, styles.right10]}>
										  
											 <View style={[styles.centerItemContent, styles.width90p, styles.bottom5]}>
											   <Text style={[styles.baseText]}>Kode Pembayaran</Text>
											   <Text style={[styles.baseText, styles.italic, styles.bold]}>{key}</Text>
											 </View>
											 <View style = {[styles.directionRow, styles.centerItemContent]} />
											
										  </View>
										  <View>
											  <Button small primary style={{backgroundColor: '#ff4c4c', padding: 10}}>
												  <TouchableHighlight underlayColor="#e6e6e6" onPress={() => {this.doBayar(value, key) }}>
												<Text style={[styles.baseText, styles.colorWhite]}>Bayar</Text>
												</TouchableHighlight>
												</Button>
										</View>
										</View>
									  
									</View>
									<View style={[styles.bgWhite, styles.top10, styles.width100p]}>
									
											<View style={[styles.width100p,styles.centerItemContent, styles.right10]}>
												<View style={{ width: '90%', borderBottomWidth: 1, borderBottomColor: '#e6e6e6' }} />
												
											</View>
										<View style={{marginLeft:20}}>
											<View style={{ marginTop: 10, marginBottom: 5 }}><Text style={[styles.bold]}>Order ID :</Text></View>
											{this.printorderid(value)}
										</View>
									</View>
								
							</View>
						</View>
						);
						

				}.bind(this));
					
						}else{
						pendingPayment.push(
						<View style={[styles.bgWhite, styles.centerItemContent, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.radius5]} key='payment-null'>
							<View style={[styles.centerItemContent, styles.pad20]} key = 'datatransaksi-kosong'>
							  <Text style={[styles.baseText, styles.alignCenter]}>Belum Ada Pembayaran Pending</Text>
							</View>
								</View>
							)
						
					}
					
					
				return pendingPayment;	
	
	
	}
	
	doBayar = (value, key) => {
		var dataPembayaranValue = {payment_id: key, link_pembayaran: value[0].link_pembayaran, description: "success", status: "Y"}
		this.props.setDataPembayaran(dataPembayaranValue);
		this.props.navigate('PaymentGateway');
		//console.log(key,value);
	}
  


	render() {
    var {height, width} = Dimensions.get('window');
    const {listPendingPayment, loadingState} = this.props;
		console.log('loadingState.loadingListPendingPayment',loadingState.loadingListPendingPayment)
	if(loadingState.loadingListPendingPayment){
		return(
			<LoadingPage />
		)
	}
		console.log('mylistPendingPayment',listPendingPayment);
              
	    return (
		<ScrollView>
			<View style={[styles.centerItemContent,{paddingBottom: height* .001}]}>
			
				<View style={[styles.top5, styles.bottom5]}>
					<Text style={[styles.baseText, styles.font11, styles.italic, styles.pad5, {color: '#ff4c4c'}]}>* Dengan mengklik bayar berarti anda setuju dengan syarat dan ketentuan yang berlaku di Jagain</Text>
				</View>
				<View style={[styles.left5, styles.pad5, styles.directionRow, styles.width100p]}>
					<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText]}>
					* 
					</Text>
					<TouchableHighlight underlayColor='blue' onPress={()=> {this.props.navigate('Agreement')} }>
						<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText, styles.underline]}>
						syarat dan ketentuan
						</Text>
					</TouchableHighlight>
				</View>
						{this.renderPendingPayment(listPendingPayment)}		

					
				
				
					 
			</View>
			</ScrollView>
		);
	}
}


function mapStateToProps(state) {
  return {
    session: state.session,
	token: state.token,
    listPendingPayment: state.listPendingPayment,
	loadingState:state.loadingState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PendingPayment);
