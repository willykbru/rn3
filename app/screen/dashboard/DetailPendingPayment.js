import React, { Component } from 'react';
import { Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator,BackHandler } from 'react-native';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';
import { Header } from 'react-native-elements';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../function';
import styles from '../../assets/styles';
import Slide from '../../animated/Slide';
import LoadingPage from '../../components/loading/LoadingPage';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import Accordion from 'react-native-collapsible/Accordion';
import OfflineNotice from '../../components/offline_notice';



class DetailPendingPayment extends Component {
  constructor(props) {

      super(props);

    }

  renderContent(dataPending, kode_transaksi) {
	  //console.log(dataPending);
		if(kode_transaksi.includes("DL")){
        var net_premi = dataPending.net_premi;
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return (
          <View style={[styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI DELAY</Text>
			</View>
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Paket</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.package_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }

        if(kode_transaksi.includes("AC")){
        var net_premi = dataPending.net_premi;
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return (
          <View style={[styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI KECELAKAAN</Text>
			</View>
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Paket</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.package_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }

	   if(kode_transaksi.includes("HT")){
        var net_premi = dataPending.net_premi;
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return (
          <View style={[styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI KESEHATAN</Text>
			</View>
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Paket</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.package_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }

	  if(kode_transaksi.includes("LF")){
        var net_premi = dataPending.net_premi;
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return (
          <View style={[styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI JIWA</Text>
			</View>
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Paket</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.package_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }
	  
	  if(kode_transaksi.includes("TR")){
        var net_premi = dataPending.net_premi;
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        return (
          <View style={[styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI TRAVEL</Text>
			</View>
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Paket</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.package_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }

	  
      if(kode_transaksi.includes("MV")){
        var tsi = dataPending.tsi;
        var net_premi = dataPending.net_premi;
        if(tsi == null || tsi == undefined || tsi == 0){
          tsi = 0.00;
        }else{
          tsi = tsi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        return (
          <View style={[styles.pad5]}>
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI KENDARAAN</Text>
			</View>
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Tipe Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.tipe_kendaraan}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Merek Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.merek_kendaraan}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Seri Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.seri_kendaraan}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Penggunaan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.penggunaan}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Tipe Coverage</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.coverage}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Kendaraan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {tsi}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }

      if(kode_transaksi.includes("PR") || kode_transaksi.includes("AP")){
        var tsi = dataPending.tsi;
        var net_premi = dataPending.net_premi;
        if(tsi == null || tsi == undefined || tsi == 0){
          tsi = 0.00;
        }else{
          tsi = tsi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        if(net_premi == null || net_premi == undefined || net_premi == 0){
          net_premi = 0.00;
        }else{
          net_premi = net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        return (
          <View style={[styles.pad5]}>
			{ kode_transaksi.includes("PR") ?
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI PROPERTI</Text>
			</View> :
			<View style={[styles.directionRow, styles.centerItemContent, styles.width90p]}>
				<Text style={[styles.baseText, styles.bold]}>ASURANSI APARTEMEN</Text>
			</View>
			}
			<View style={[styles.directionRow, styles.left5, styles.bottom5]}>
			  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
				<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
			  </View>
			</View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Provider</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.insurance_name}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Nama Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.nama}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Fungsi Bangunan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.jenis_property}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Alamat Tertanggung</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>{dataPending.risk_address}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Bangunan</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {tsi}</Text></View>
            </View>
            <View style={[styles.directionRow, styles.width90p]}>
              <View style={styles.width50p}><Text style={[styles.baseText]}>Harga Premi</Text></View>
              <View style={styles.width5p}><Text style={[styles.baseText]}>: </Text></View>
              <View style={styles.width45p}><Text style={[styles.baseText]}>Rp. {net_premi}</Text></View>
            </View>
          </View>
        );
      }



  }

  onBackPress = () => {
    var screen = 'Main';
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	const event_name = 'Halaman Detail Pending Payment (My Cart)';	
		
	var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	appFunction.setScreenGA(event_name, userID);   
	  
	var {kodeTransaksiDataPending} = this.props;
    this.props.getDataPending(kodeTransaksiDataPending);
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }


  render() {
    const {dataPending, loadingState} = this.props;
    const {kodeTransaksiDataPending} = this.props;
	const title = 'DETAIL PENDING';
	//console.log('data',dataPending);


    return (
		<Container>
		 <Slide>
		  <Header
			backgroundColor={'red'}
			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
			centerComponent={{ text: title , style: { color: 'white' } }}
			/>
			<OfflineNotice />
			<View style={[styles.centerItemContent, styles.elev2, styles.bottom5, {borderTopWidth:.2, borderTopColor: 'white'}]} />
			{ 	loadingState.loadingDataPendingPayment ?
			<LoadingPage /> :
		   <Content>
		   <View style={styles.centerItemContent}>
          <View style={[styles.bgWhite, styles.bottom5, styles.radius5, styles.top10, styles.elev2, styles.pad10, styles.width90p]}>
		   {this.renderContent(dataPending, kodeTransaksiDataPending)}
		   </View>
		   </View>
		  </Content>
		  }
		   </Slide>
		</Container>
    );
  }
}



function mapStateToProps(state) {
  return {
	status: state.status,
    session: state.session,
    dataPending:state.dataPending,
    dataTransaksi: state.dataTransaksi,
	kodeTransaksiDataPending: state.kodeTransaksiDataPending,
	loadingState:state.loadingState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPendingPayment);
