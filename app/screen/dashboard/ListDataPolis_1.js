import React, { Component } from 'react';
import { Platform, Picker, Dimensions, TextInput, View, Alert, ScrollView, Text, TouchableHighlight, Linking, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../function';
import styles from '../../assets/styles';
import LoadingPage from '../../components/loading/LoadingPage';
import BaseShape from '../../components/shape/base';

import DetailPendingPayment from './DetailPendingPayment';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import Accordion from 'react-native-collapsible/Accordion';


class ListDataPolis extends Component{

  constructor(props) {
    super(props);
	this.state = {
		navtodatapolis: false,
		list: [],
		listClaim: [],
	}
	}
	
	
	componentWillUnmount(){
		this.props.resetClaimPolis(); // untuk mereset ajukan klaim
	}

	getListKlaimPolis(listSuccessPayment) {

		// console.log('test', listSuccessPayment);

		for (let i = 0; i < listSuccessPayment.length; i++) {

			this.cekClaimAll(listSuccessPayment[i].policyno);

		}


	}

	lihatKlaim(listSuccessPayment) {
		// console.log('listSuccessPayment', listSuccessPayment);
		this.props.getDetailPolis(listSuccessPayment.kode_transaksi);

		temp = {
			'order_id': listSuccessPayment.kode_transaksi
		}
		this.props.setDataClaimMV(temp);
		this.props.resetListClaimDone();
		this.props.resetListClaimTerkini();
		this.props.getClaimDone(listSuccessPayment.policyno);
		this.props.getClaimTerkini(listSuccessPayment.policyno);
		this.setState({ nextNav: 'lihatClaim' })
	}

	doClaim(listSuccessPayment) {

		// console.log(listSuccessPayment);
		this.props.getDetailPolis(listSuccessPayment.kode_transaksi);

		temp = {
			'order_id': listSuccessPayment.kode_transaksi
		}
		this.props.setDataClaimMV(temp);

		this.setState({ nextNav: 'claim' })

	}
  
  doGetPolis(orderid){
	  const {polisScreen} = this.props.screen;
	  this.setState({navtodatapolis: true});
	  this.props.setOID(orderid);
	  this.props.getDataPolis(orderid);	 
	  //Linking.openURL('http://google.com');
  }
  
    //do renewal polis
  
  doRenewalPolis(listSuccessPayment){ 
	 // Alert.alert(listSuccessPayment.kode_transaksi); 
	  var lob = '';
	 this.props.getDetailPolis(listSuccessPayment.kode_transaksi);
	 if(listSuccessPayment.kode_transaksi.includes("AC")){
		lob = 'PA';
		 this.props.setOrderId(listSuccessPayment.kode_transaksi);
		 this.props.getOkupasiAccident();
	 }else if(listSuccessPayment.kode_transaksi.includes("MV")){
		 lob = 'MV';
		 this.props.setOrderId(listSuccessPayment.kode_transaksi);
		//  console.log('kode transaksi',listSuccessPayment.kode_transaksi)
	 } else if(listSuccessPayment.kode_transaksi.includes("PR")){
		 lob = 'PR';
		 this.props.setOrderId(listSuccessPayment.kode_transaksi);
		//  console.log('kode transaksi',listSuccessPayment.kode_transaksi)
	 } 
	// console.log('listSuccessPayment',listSuccessPayment);
	 
	 this.props.setScreen(lob, 'cariasuransi');
	 this.setState({ nextNav: lob })
	 
	}
	
	async cekClaimAll(policy_no) {
		var url = appFunction.serviceURL + '/claim/getclaimlist?policy_no=' + policy_no;

		// console.log('url', url);
		let value = true;

		// fetch(url, {
		// 		method: 'GET',
		// 		headers: {
		// 			'Accept': 'application/json',
		// 			'Content-Type': 'application/json',
		// 		},
		// 	}).then((response) => {
		// 		if (!response.ok) {
		// 			Alert.alert('Error', response.statusText);
		// 		}
		// 		return response;
		// 	}).then((response) => response.json()).then((items) => {
		// 		console.log('policy_no', items);


		// 		return items;

		// 	}).catch((error) => { console.log('error getClaimTerkini', error) });

		try {
			const response = await fetch(url);

			const json = await response.json();


			// json.forEach(item => {
			// 	if(item.claim_status == 'N' || item.claim_status =='A'){
			// 		value = false;
			// 	}
			// 	// console.log(item.claim_number, value);
			// });
			// console.log('stringify', JSON.stringify(json));
			// let temp = {
			// 	"claim_number" : policy_no,
			// 	"list_claim" : json
			// }
			// console.log('temp',JSON.stringify(json));
			var temp = {
				"policyno": policy_no,
				"list_claim": json

			}
			// console.log('temp', JSON.stringify(temp));
			// this.setState({listClaim : json});
			this.state.listClaim.push(temp);
			this.props.setClaimPolis(temp);

			// console.log('state',this.state.listClaim);
			// this.props.setUpdateClaim(json);
			// console.log(policy_no, json);
		} catch (error) {
			console.log(error);
		}
		// console.log('test');
		return value;
	}
	
	checkInAsuransiList(value, asuransi){
		if(value.includes('AC') || value.includes('MV')){
			return true
		}
		return false;
	}
	
	sortValidFirst(array){
		//array.sort((a,b) => (''+ a.kode_transaksi).localeCompare(b.kode_transaksi))
		array.sort((a,b) => new Date(b.policy_enddate)- new Date(a.policy_enddate))
		return array;
	}
	
	sortKode(array){
		array.sort((a,b) => b.kode_transaksi - a.kode_transaksi)
		return array;
	}


	renderListSuccess(listSuccessPayment) {
		const { claimPolis, loadingClaimMenu } = this.props;

		var listPolis = [];
		//console.log(listSuccessPayment);
		if (listSuccessPayment.length > 0) {
			// console.log('test');
			var now = new Date(this.props.status.serverdate);
			var endpolis = '';
			var deadlinepolis = null;
			
			this.sortValidFirst(listSuccessPayment);
		



			//Alert.alert(this.props.status.serverdate.toString());

			// console.log(i, listSuccessPayment);
			for (let i = 0; i < listSuccessPayment.length; i++) {


				let isKlaim = true;
				let index = -1;
				//  this.cekClaimAll(listSuccessPayment[i].policyno);
				// this.state.list.push(temp);
				if (listSuccessPayment[i].policyno) {
					endpolis = new Date(listSuccessPayment[i].policy_enddate);
					deadlinepolis = new Date(listSuccessPayment[i].policy_enddate);
					deadlinepolis.setDate(deadlinepolis.getDate() - 45);

					if (claimPolis[i] != null || claimPolis[i] != undefined) {

						if (claimPolis.length == listSuccessPayment.length) {

							index = claimPolis.findIndex(obj => obj.policyno == listSuccessPayment[i].policyno);

							// console.log(listSuccessPayment[i].policyno, index);
							// console.log(listSuccessPayment[i].policyno, claimPolis[index].list_claim.length);
							claimPolis[index].list_claim.forEach(item => {
								if (item.claim_status == 'A' || item.claim_status == 'N') {
									isKlaim = false;
								}
							});

							// console.log(listSuccessPayment[i].policyno, isKlaim);
						}


					}
					//  if(isKlaim == false){
					// 	 console.log(listSuccessPayment[i].policyno, 'false');
					//  }
					//  console.log(listSuccessPayment[i].policyno, isKlaim);
					//var polisdate = new Date('2013-05-23');

					// console.log(this.state.listClaim);
					// this.state.list.push(temp);

					//  console.log('state listClaim stringify ', JSON.stringify(this.state.listClaim));
					//  console.log('state listClaim length ', this.state.listClaim.length);
					//  console.log('state listClaim ', this.state.listClaim);

					//  console.log('length:',this.state.listClaim.length);
					//  console.log('endpolis ',i + ' '  + endpolis);
					//  console.log('now ',i + ' '  + now);
					//  console.log('deadlinepolis ',i + ' '  + deadlinepolis);
					//  console.log('boolean ',i + ' '  + Boolean(endpolis >= now && now >= deadlinepolis));


					listPolis.push(
						<View style={[ styles.width90p]} key={i}>
						<View style={[ styles.width30p, styles.centerItemContent]}>
									{endpolis < now ? <TouchableHighlight style={{marginTop:10, padding: 5, borderBottomWidth: .8, borderBottomColor: '#ff3232', borderRadius: 8 }} underlayColor="transparent" onPress={() => null}>
											<Text style={[styles.font12, styles.bold, { color: '#ff3232' }]}>Polis Expired</Text>
										</TouchableHighlight> :
											<TouchableHighlight style={{marginTop:10, padding: 5, borderBottomWidth: .8, borderBottomColor: '#00e600', borderRadius: 8 }} underlayColor="transparent" onPress={() => null}>
												<Text style={[styles.font12, styles.bold, { color: '#00e600' }]}>Polis Valid</Text>
											</TouchableHighlight>
										}
										</View>
							<View style={[styles.centerItemContent, styles.directionRow, styles.left5, styles.bottom5, styles.top10]}>
								<View style={[styles.width90p, styles.centerItemContent, styles.right10]}>
									{listSuccessPayment[i].kode_transaksi.includes("PR") ? <Text style={[styles.baseText, styles.bold]}>ASURANSI PROPERTI</Text> :
										listSuccessPayment[i].kode_transaksi.includes("MV") ? <Text style={[styles.baseText, styles.bold]}>ASURANSI KENDARAAN</Text> : listSuccessPayment[i].kode_transaksi.includes("AP") ?
											<Text style={[styles.baseText, styles.bold]}>ASURANSI APARTEMEN</Text> : listSuccessPayment[i].kode_transaksi.includes("AC") ?
												<Text style={[styles.baseText, styles.bold]}>ASURANSI KECELAKAAN</Text> : listSuccessPayment[i].kode_transaksi.includes("HT") ?
													<Text style={[styles.baseText, styles.bold]}>ASURANSI KESEHATAN</Text> : listSuccessPayment[i].kode_transaksi.includes("LF") ?
														<Text style={[styles.baseText, styles.bold]}>ASURANSI JIWA</Text> : listSuccessPayment[i].kode_transaksi.includes("TR") ?
															<Text style={[styles.baseText, styles.bold]}>ASURANSI TRAVEL</Text> : listSuccessPayment[i].kode_transaksi.includes("DL") ?
																<Text style={[styles.baseText, styles.bold]}>ASURANSI DELAY</Text> : listSuccessPayment[i].kode_transaksi.includes("CG") ?
																	<Text style={[styles.baseText, styles.bold]}>ASURANSI CARGO</Text> : listSuccessPayment[i].kode_transaksi.includes("BC") ?
																		<Text style={[styles.baseText, styles.bold]}>ASURANSI BICYCLE</Text> : null}
									<View style={[styles.centerItemContent, styles.width90p, styles.bottom5, styles.top5]}>
										<Text style={[styles.baseText]}>Nomor Polis</Text>
										<Text style={[styles.baseText, styles.italic]}>{listSuccessPayment[i].policyno}</Text>
									</View>
									{listSuccessPayment[i].policyno_eqvet ?
										<View style={[styles.centerItemContent, styles.width90p, styles.bottom5, styles.top5]}>
											<Text style={[styles.baseText]}>Nomor Polis EQVET</Text>
											<Text style={[styles.baseText, styles.italic]}>{listSuccessPayment[i].policyno_eqvet}</Text>
										</View>
										: null}
									
								</View>

							</View>
							
							<View style={[styles.directionRow, styles.centerItemContent, {flexWrap: 'wrap', marginBottom: 10}]}>
									
									<TouchableHighlight  style={{marginTop:5, padding: 5, borderWidth: 1, borderColor: 'blue', backgroundColor: 'blue', borderRadius: 3, elevation: 10 }} underlayColor="#e6e6e6" onPress={() => this.doGetPolis(listSuccessPayment[i].kode_transaksi)}>
											<Text style={[styles.font12, styles.bold,{ color: 'white' }]}>Lihat Polis</Text>
										</TouchableHighlight> 
										
										{this.checkInAsuransiList(listSuccessPayment[i].kode_transaksi) && ((endpolis >= now && now >= deadlinepolis)) && listSuccessPayment[i].status_renewal == 'N' ? <Text> | </Text> : null}
										{this.checkInAsuransiList(listSuccessPayment[i].kode_transaksi) && ((endpolis >= now && now >= deadlinepolis)) && listSuccessPayment[i].status_renewal == 'N' ? <TouchableHighlight  style={{marginTop:5, padding: 5, borderWidth: 1, borderColor: '#0080ff', backgroundColor: '#0080ff', borderRadius: 3, elevation: 10 }} underlayColor="transparent" onPress={() => this.doRenewalPolis(listSuccessPayment[i])}>
											<Text style={[styles.font12, styles.bold, { color: 'white' }]}>Renewal Sekarang</Text>
										</TouchableHighlight> :
											null
										}
										
										{endpolis > now && listSuccessPayment[i].kode_transaksi.substring(0, 2) == 'MV' && isKlaim ? <Text> | </Text> : null}

										{endpolis > now && listSuccessPayment[i].kode_transaksi.substring(0, 2) == 'MV' && isKlaim ?
											<TouchableHighlight style={{marginTop:5, padding: 5, borderWidth: 1, borderColor: '#FFA500', backgroundColor: '#FFA500', borderRadius: 3, elevation: 10 }}  underlayColor="transparent" onPress={() => { this.doClaim(listSuccessPayment[i]) }}>
												<Text style={[styles.font12, styles.bold, { color: 'white' }]}>Buat Laporan</Text>
											</TouchableHighlight>
											:
											null

										}			

										{endpolis > now && listSuccessPayment[i].kode_transaksi.substring(0, 2) == 'MV' ? <Text> | </Text> : null}

										{endpolis > now && listSuccessPayment[i].kode_transaksi.substring(0, 2) == 'MV' ?
											<TouchableHighlight style={{marginTop:5, padding: 5, borderWidth: 1, borderColor: '#ffb732', backgroundColor: '#ffb732', borderRadius: 3, elevation: 10 }}   underlayColor="transparent" onPress={() => { this.lihatKlaim(listSuccessPayment[i]) }}>
												<Text style={[styles.font12,  styles.bold, { color: 'white' }]}>Lihat Laporan</Text>
											</TouchableHighlight>
											: null
										}
									</View>


							{i != listSuccessPayment.length - 1 ? <View style={[styles.bgWhite, styles.top10, styles.width100p]}>
								<View style={[styles.directionRow, styles.left5]}>
									<View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
										<View style={{ width: '90%', borderBottomWidth: 1, borderBottomColor: '#e6e6e6' }} />
									</View>
								</View>
							</View> : null}

						</View>
					);
				}
			}
			
		} else {
			listPolis.push(
				<View style={[styles.centerItemContent, styles.pad20]} key='datapolis-kosong'>
					<Text style={[styles.baseText, styles.alignCenter]}>Belum Ada Polis Terbit</Text>
				</View>
			);
		}

		//  console.log('state listClaim ', this.state.listClaim);
		//  console.log('state list ', this.state.list);
		return listPolis;
	}
	
	componentWillReceiveProps(NextProps){
		if(this.props.dataPolis != NextProps.dataPolis){
			if(this.state.navtodatapolis){
				if(NextProps.dataPolis.policy){
					this.props.navigate('DataPolis');
				}else{
					Alert.alert('Polis tidak ditemukan di database kami', 'Harap hubungi Customer Service kami untuk info lebih lanjut');
				}
			}
		}
		if(this.props.detailPolis != NextProps.detailPolis){
			if(NextProps.detailPolis.policy_no){
					console.log('doRenewal', NextProps.detailPolis.policy_no + ' ' + this.state.nextNav);
					//this.props.setScreen('CariAsuransiRenewal','renewal');
				if (this.state.nextNav == 'claim') {
					// this.props.navigate('ClaimVehicle');
					// this.props.navigate('ClaimVehicleUpload');
					// this.props.navigate('ClaimMVPengemudi');
					this.props.navigate('ClaimMVUmum');
					// this.props.navigate('ClaimMVPenumpangSaksi');
					// this.props.navigate('ClaimMVPolisi');
					// this.props.navigate('ClaimMVPihakKetiga');
					// this.props.navigate('ClaimMVBerkasUnggahan');
				} else if (this.state.nextNav == 'lihatClaim') {
					this.props.navigate('ClaimLihatKlaim');
				} else {
					if(this.state.nextNav == 'PA'){
						this.props.makeRenewalAccident(NextProps.detailPolis);
						this.props.makeRenewalDataAccident(NextProps.detailPolis, NextProps.renewalLabel.order_id); 
					}else if(this.state.nextNav == 'MV'){
						this.props.makeRenewalKendaraan(NextProps.detailPolis);
						this.props.makeRenewalPerluasanKendaraan(NextProps.detailPolis);
						this.props.makeRenewalDataKendaraan(NextProps.detailPolis, NextProps.renewalLabel.order_id); 
					}else if(this.state.nextNav == 'PR'){
						console.log('test renewal PR');
					}
						//this.props.getListAsuransi(NextProps.detailPolis, 'Accident');
						//this.props.navigate('Renewal');
				}
					
			}
		}
		if (this.props.renewalAccident != NextProps.renewalAccident || this.props.renewalKendaraan != NextProps.renewalKendaraan) {
			if (NextProps.renewalAccident.pekerjaan || (NextProps.renewalKendaraan.vehicle_type && NextProps.renewalKendaraan.vehicle_type !== 'Pilih')) {
				console.log('renewalKendaraan');
				this.props.setScreen('CariAsuransiRenewal', 'renewal');
				//this.props.makeRenewalAccident(NextProps.detailPolis, NextProps.renewalLabel.order_id);
				//	this.props.getListAsuransi(NextProps.detailPolis, 'Accident'); 
				this.props.navigate('Renewal');
			}else{
				appFunction.toastError('Ada data Anda yang kurang di database, hubungi customer service untuk keterangan lebih lanjut.');
			}
		}
		if (this.props.listSuccessPayment != NextProps.listSuccessPayment) {
			// console.log(NextProps.listSuccessPayment);


			if (NextProps.listSuccessPayment.length > 0) {
				
				this.getListKlaimPolis(NextProps.listSuccessPayment);
			}
			this.forceUpdate();
		}
		

		if (this.props.claimPolis != NextProps.claimPolis) {
			if (NextProps.claimPolis.length == this.props.listSuccessPayment.length) {
				this.props.setLoadingClaimMenu(false);
			}
			// console.log('listSuccessPayment', NextProps.listSuccessPayment)
			// console.log('claimPolis',NextProps.claimPolis);
			this.forceUpdate();

		}


		if (this.props.loadingClaimMenu != NextProps.loadingClaimMenu) {
			this.forceUpdate();
		}

	

	

	}


	
	toPDFURL(url){
		//console.log(url);
			if(url && url != 'http://'){
				var xhttp = new XMLHttpRequest();
				xhttp.open('HEAD', url);
				xhttp.onreadystatechange = function () {
					if (this.readyState == this.DONE) {
						console.log(this.status);
						console.log(this.getResponseHeader("Content-Type"));
						if(this.getResponseHeader("Content-Type").includes('application/pdf')){
							Linking.openURL(url);
						}else{
							appFunction.toastError('File PDF Tidak Ditemukan, Coba Cek Email Anda Untuk Polis Tersebut');
						}
					}
				};
				xhttp.send();
			}else{
				appFunction.toastError('File PDF Tidak Ditemukan, Coba Cek Email Anda Untuk Polis Tersebut');
			}
		
	}




	componentDidMount() {
		const event_name = 'Halaman My Polis';

		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
		appFunction.setScreenGA(event_name, userID);

		const { username } = this.props.session;
		console.log('my polis notificationcontent',this.props.notificationcontent);
		this.props.setLoadingClaimMenu(true); // loading untuk claim menu
		this.props.getSuccessPayment(username);
		
	}


	render() {
		var {height, width} = Dimensions.get('window');
		const { listSuccessPayment, loadingState, loadingClaimMenu, notificationcontent} = this.props;
		console.log( 'notificationcontent', notificationcontent);
		//Alert.alert(this.props.status.notificationrenewal, this.props.notificationcontent);
		if (!appFunction.emptyObject(notificationcontent)) {
			//console.log(this.props.notificationcontent);
			var temp = this.props.notificationcontent.body.split(" ")//now you have 3 words in temp
			//temp[1]//is your second word
			console.log('order_id', temp[4])// is your third word
			console.log('index', findIndex(listSuccessPayment, temp[4]))// is your third word

		}
		if(loadingState.loadingListSuccessPayment || loadingClaimMenu){
			// console.log('--');
			return(
				<LoadingPage />
			)
		}

		
	
    //console.log(listDataPolis);
	    return (
			<View style={[styles.centerItemContent]}>
				<View style={[styles.centerItemContent, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.bgWhite, styles.radius5]}>
					{this.renderListSuccess(listSuccessPayment)}
				</View>
			</View>
		  );
	  }

}


function mapStateToProps(state) {
  return { 
  renewalLabel: state.renewalLabel,
	    detailPolis: state.detailPolis,
	    status:state.status,
		screen: state.screen,
		dataPolis: state.dataPolis,
		session: state.session,
		listSuccessPayment: state.listSuccessPayment,
		loadingState:state.loadingState,
		renewalAccident: state.renewalAccident,
		renewalKendaraan: state.renewalKendaraan,
		listOccupation: state.listOccupation,
		listKlaimTerkini: state.listKlaimTerkini,
		claimPolis: state.claimPolis,
		loadingClaimMenu: state.loadingClaimMenu,
	  notificationcontent: state.notificationcontent,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListDataPolis);
