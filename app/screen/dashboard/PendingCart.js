import React, { Component } from 'react';
import { Alert, Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../function';
import styles from '../../assets/styles';
import LoadingPage from '../../components/loading/LoadingPage';
import Fetch from 'react-native-fetch';

import DetailPendingPayment from './DetailPendingPayment';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import Accordion from 'react-native-collapsible/Accordion';
import Icon2 from 'react-native-vector-icons/Ionicons';


class PendingCart extends Component{

  constructor(props) {
    super(props);
	this.state = {
		net_premi:[],
		total_premi: 0,
		
	}
  }


	renderTotalPremi(listPendingCartPremi){
			var listTransaksi= [];
		 var json_total_premi = 0;
		 var i;

		for (i = 0; i < listPendingCartPremi.length; i++){
			json_total_premi = json_total_premi + listPendingCartPremi[i].net_premi;
			}

			if (json_total_premi == null || json_total_premi == undefined || json_total_premi == 0) {
				json_total_premi = 0.00;
			} else {
				json_total_premi = json_total_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
			}
			console.log(json_total_premi);


		 listTransaksi.push(
		 	<View style={[styles.pad20]} key = 'datatransaksi-kosong'>
			  <Text style={[styles.baseText, styles.alignCenter, styles.font18]}>{ 'Total Premi : Rp. ' + json_total_premi }</Text>
		 	</View>
		 );
	
      return listTransaksi;
	  
	  
    }
	
	
	renderListPending(listPendingCartPremi, listPendingCart, i){
   	   var listTransaksi = [];
		 var json_net_premi = listPendingCartPremi.net_premi;
	   
		if(json_net_premi == null || json_net_premi == undefined || json_net_premi == 0){
		  json_net_premi = 0.00;
		}else{
		  json_net_premi = json_net_premi.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		}
		//console.log(json_net_premi);
		
	
	
		 if(!listPendingCart.kode_transaksi.includes("DL") 
		 || !listPendingCart.kode_transaksi.includes("CG") 
		 || !listPendingCart.kode_transaksi.includes("BC")
		 || !listPendingCart.kode_transaksi.includes("PT")){
			if(listPendingCart.status == 'Y'){
		   listTransaksi.push(
			<View style={[styles.centerItemContent, styles.width90p]} key={i}>
				{this.deleteIcon(listPendingCart)}
				{this.cartTitle(listPendingCart)}
				<View style={{marginTop: 10}}/>
				<View style={[styles.directionRow, styles.left5, styles.bottom5, styles.top10]}>
				  <View style={[styles.width90p, styles.centerItemContent, styles.right10]}>
				  
					 <View style={[styles.centerItemContent, styles.width90p, styles.bottom5]}>
					   <Text style={[styles.baseText]}>Kode Transaksi</Text>
					   <Text style={[styles.baseText, styles.italic]}>{listPendingCart.kode_transaksi}</Text>
					 </View>
					 
					 <View style = {[styles.directionRow, styles.centerItemContent]}>
					 <TouchableHighlight underlayColor="#e6e6e6" onPress={() => { this.props.setKodeTransaksiDataPending(listPendingCart.kode_transaksi); this.props.navigate('DetailPendingPayment')}}>
						<Text style={[styles.font12, styles.underline,{color:'#0080ff'}]}>Detail Pembayaran</Text>
					 </TouchableHighlight>
					 </View>
					 <View styles={{paddingTop : 25}}>
						<Text style = {[styles.bold]}>{'Premi : Rp. ' + json_net_premi}</Text>
					 </View>
				  </View>
				
			    </View>
			  
			 
			  
			</View>
		   );
			}
			}
	
		
	
		
	
      return listTransaksi;
	  
	  
    }
	
	componentWillUnmount(){

		this.props.resetListPendingCartPremi();
	}

  componentWillMount(){
		const {username} = this.props.session;
	//console.log(username);
	 	 this.props.resetListPendingCartPremi();
		this.props.getPendingCart(username);
		this.setState({total_premi: 0, net_premi:[]})
  }
  
  deleteIcon(listPendingCart){
	const {token, session} = this.props;
	
	return( 
		<View style={{position:'absolute', right: 0, top: 10}}>
			<TouchableHighlight underlayColor='transparent'  onPress={() => Alert.alert(
																		  'Hapus Pemesanan Asuransi?',
																		  '1 Pemesanan Asuransi akan dihapus dari keranjang belanja',
																		[
																			{text: 'Hapus', onPress: () => this.props.doDeleteCart(session.username, token, listPendingCart.kode_transaksi)},
																			{text: 'Batal', onPress: () => console.log('Ask me later pressed')},
																		])}>
				<Icon2 name="ios-trash-outline" style={{textShadowOffset:{width:2, height:1}, shadowColor:'#000000', shadowOpacity:0.8}} size={26} color="#FF1F17" />
			</TouchableHighlight>
		</View>
	)
  }
  
  cartTitle(listPendingCart){
	  return(
	  <View style={[styles.directionRow, styles.centerItemContent, {marginTop: 15}]}>
						{ listPendingCart.kode_transaksi.includes("PR") ? <Text style={[styles.baseText, styles.bold]}>ASURANSI RUMAH TINGGAL</Text> : 
						listPendingCart.kode_transaksi.includes("MV")? <Text style={[styles.baseText, styles.bold]}>ASURANSI KENDARAAN</Text>: listPendingCart.kode_transaksi.includes("AP")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI APARTEMEN</Text> : listPendingCart.kode_transaksi.includes("AC")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI KECELAKAAN</Text> : listPendingCart.kode_transaksi.includes("HT")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI KESEHATAN</Text> : listPendingCart.kode_transaksi.includes("LF")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI JIWA</Text> : listPendingCart.kode_transaksi.includes("TR")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI TRAVEL</Text> : listPendingCart.kode_transaksi.includes("DL")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI DELAY</Text> : listPendingCart.kode_transaksi.includes("CG")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI CARGO</Text> : listPendingCart.kode_transaksi.includes("BC")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI SEPEDA</Text> : listPendingCart.kode_transaksi.includes("PT")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI HEWAN</Text> : null }
					</View>
	  )
	  
	}
	

  
	renderPendingCart(listPendingCart, listPendingCartPremi){
		var count = 0;
		var pendingCart = [];
		
				if(listPendingCart.length > 0){
					console.log(listPendingCart);
					 for(let i = 0; i < listPendingCart.length; i++){
					 //console.log('list pending',listPendingCartPremi)
					 pendingCart.push(
					 <View style={[styles.centerItemContent, styles.width100p]} key={i}>
						<View style={[styles.bgWhite, styles.centerItemContent, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.radius5]}>
								 {this.renderListPending(appFunction.findKodeTransaksi(listPendingCartPremi, listPendingCart[i].kode_transaksi), listPendingCart[i], i)}
						</View>
						
						</View>
						)
					}
						}else{
						pendingCart.push(
						<View style={[styles.bgWhite, styles.centerItemContent, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.radius5]} key='cart-null'>
							<View style={[styles.centerItemContent, styles.pad20]} key = 'datatransaksi-kosong'>
							  <Text style={[styles.baseText, styles.alignCenter]}>Belum Ada Pembelian</Text>
							</View>
								</View>
							)
						
					}
					
					
				return pendingCart;	
	
	
	
			}
			

	componentWillReceiveProps(NextProps){
		console.log('ganti data');
		 if(this.props.listPendingCart != NextProps.listPendingCart){
			 var i;
			 this.props.resetListPendingPayment();
			 this.props.resetListPendingCartPremi();
			 console.log('lengthpending cart',NextProps.listPendingCart.length);
		 	for (i = 0; i < NextProps.listPendingCart.length; i++){
				  console.log('listpending cart', NextProps.listPendingCart[i].kode_transaksi);
				 this.props.getPendingCartPremi(NextProps.listPendingCart[i].kode_transaksi);
			 }
		 }
	
		
	}

	componentDidMount(){
		var i;
		this.props.resetListPendingCartPremi();
		console.log('lengthpending cart', this.props.listPendingCart.length);
		for (i = 0; i < this.props.listPendingCart.length; i++) {
			console.log('listpending cart', this.props.listPendingCart[i].kode_transaksi);
			this.props.getPendingCartPremi(this.props.listPendingCart[i].kode_transaksi);
		}
	}


	render() {
    var {height, width} = Dimensions.get('window');
		const { listPendingCart, loadingState, listPendingCartPremi, itemsLoading} = this.props;
		console.log('render list pending', listPendingCart);
		if (listPendingCart.length > 0){
			if (loadingState.loadingListPendingCart || loadingState.loadingListPendingCartPremi){
			return(
				<LoadingPage />
			)
			}
		}else{
			if (loadingState.loadingListPendingCart || loadingState.loadingListPendingCartPremi) {
				return (
					<LoadingPage />
				)
			}
		}
		console.log("listPendingCart", listPendingCart);
              
	    return (
		
		<ScrollView>
			<View style={[styles.centerItemContent,{paddingBottom: height* .001}]}>
			
				<View style={[styles.top5, styles.bottom5]}>
					<Text style={[styles.baseText, styles.font11, styles.italic, styles.pad5, {color: '#ff4c4c'}]}>* Dengan mengklik bayar berarti anda setuju dengan syarat dan ketentuan yang berlaku di Jagain</Text>
				</View>
				<View style={[styles.left5, styles.pad5, styles.directionRow, styles.width100p]}>
					<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText]}>
					* 
					</Text>
					<TouchableHighlight underlayColor='blue' onPress={()=> {this.props.navigate('Agreement')} }>
						<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText, styles.underline]}>
						syarat dan ketentuan
						</Text>
					</TouchableHighlight>
				</View>
				
						{this.renderPendingCart(listPendingCart, listPendingCartPremi)}		

					{ listPendingCart.length > 0 ? 
					
					<View style={[styles.bgWhite, styles.centerItemContent, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.radius5]}>
								{this.renderTotalPremi(listPendingCartPremi)}
						</View> 
						:null
					}
				
				
					 
			</View>
			</ScrollView>
		);
	}
}


function mapStateToProps(state) {
  return {
    session: state.session,
	token: state.token,
    listPendingCart: state.listPendingCart,
	loadingState:state.loadingState,
		listPendingCartPremi: state.listPendingCartPremi,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PendingCart);
