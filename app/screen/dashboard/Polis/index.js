import React, { Component } from 'react';
import { PermissionsAndroid, BackHandler, Alert, ScrollView, Text, WebView, View, RefreshControl, StyleSheet, Dimensions, Image, TouchableHighlight, StatusBar, Linking, TouchableOpacity, AsyncStorage } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';
import RNFetchBlob from 'react-native-fetch-blob'

import SubmitButton from '../../../components/button/SubmitButton';
import LoadingPage from '../../../components/loading/LoadingPage';

import Slide from '../../../animated/Slide';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import Pdf from 'react-native-pdf';
import * as appFunction from '../../../function'
import OfflineNotice from '../../../components/offline_notice';



class DataPolis extends Component {

	constructor(props) {

	    super(props);

	    this.state = {
				link_pembayaran: '',
				exists: false,
				refreshing: false
			}
		}


	onNavigationStateChange(state){
		//console.log(state);
		if(state.url == 'http://dev.jagain.com/' ||
			state.url == 'https://dev.jagain.com/' ||
			state.url == 'http://jagain.com/' ||
			state.url == 'https://jagain.com/' ||
			state.url == 'http://jagain.co.id/' ||
			state.url == 'https://jagain.co.id/' ||
			state.url == 'http://dev.jagain.co.id/' ||
			state.url == 'https://dev.jagain.co.id/' ||
			state.url.substr(0, 21) == 'http://www.jagain.com' ||
			state.url.substr(0, 22) == 'https://www.jagain.com' ||
			state.url.substr(0, 34) == 'http://www.jagain.com/main/landing' ||
			state.url.substr(0, 35) == 'https://www.jagain.com/main/landing' ){
		}else{
			Linking.openURL(state.url);
		}
	}


	downloadPolis(dataPolis, path){
				const android = RNFetchBlob.android;

	            var downloadURL = dataPolis.policy;
		if (dataPolis.policy.includes('sinarmas.co.id') || dataPolis.policy.includes('simasnet.id')) {
			Linking.openURL(dataPolis.policy);
		} else {
			if (dataPolis.policy.includes('jagain.com') || dataPolis.policy.includes('jagain.co.id')) 
								downloadURL = 'http://'+ dataPolis.policy;

							RNFetchBlob.config({

								addAndroidDownloads : {
								  useDownloadManager : true,
								  path,
								  title: dataPolis.policyno +'.pdf',
								  description : 'Polis Berhasil di download',
								  mime : 'application/pdf',
								  notification : false,
								}
						  })
						  .fetch('GET',downloadURL)
						  .then((res) => {
								this.setState({ exists: true, refreshing: false});
							// the path should be dirs.DownloadDir + 'path-to-file.anything'
							appFunction.toastError('Polis telah tersimpan di  '+ res.path());
							//android.actionViewIntent( res.path(), 'application/pdf');
														
							})
						}
            
	}


	loadPolis(dataPolis, path){
			const dirs = RNFetchBlob.fs.dirs;
			//const android = RNFetchBlob.android
			//appFunction.toastError(`${dirs.DownloadDir}` + '/Polis_'+ dataPolis.policyno +'.pdf');
			var downloadURL = dataPolis.policy;
		if (dataPolis.policy.includes('sinarmas.co.id') || dataPolis.policy.includes('simasnet.id')) {
			console.log('dataPolis1', dataPolis.policy);
			Linking.openURL(dataPolis.policy);
		} else {
			console.log('dataPolis2',dataPolis.policy);
			if (dataPolis.policy.includes('jagain.com') || dataPolis.policy.includes('jagain.co.id')) 
				downloadURL = 'http://'+ dataPolis.policy;

			RNFetchBlob.config({

				addAndroidDownloads : {
				  useDownloadManager : true,
				  path,
				  title: dataPolis.policyno +'.pdf',
				  description : 'Polis Berhasil di download',
				  mime : 'application/pdf',
				  notification : false,
				}
			  })
			  .fetch('GET',downloadURL)
			  .then((res) => {
					this.setState({ exists: true, refreshing: false});
				  //android.actionViewIntent(res.path(), 'application/pdf')
					// the path should be dirs.DownloadDir + 'path-to-file.anything'
				//appFunction.toastError('The file saved to '+ res.path())
				})
			}


	}


	async doLoadPolis(dataPolis, path){
		await RNFetchBlob.fs.exists(path).then((exist) => {
			if(exist){
				this.setState({ exists: true, refreshing: false});
				//appFunction.toastError('true');
			}else{
				this.loadPolis(dataPolis, path);
				//appFunction.toastError('false');
			}
		})
		.catch((err) => { console.log( err ) })							
	}
	
	async doDownloadPolis(dataPolis, path){	
		const android = RNFetchBlob.android;
		
		//await RNFetchBlob.fs.exists(path).then((exist) => {
			// if(exist){
			// 	appFunction.toastError('Polis telah tersimpan di  '+ path);
			// 	android.actionViewIntent( 'file://' + path, 'application/pdf');			
			// }else{			
				this.downloadPolis(dataPolis, path);
			//}		
		//})
		//.catch((err) => { console.log( err ) })	
	}

	onBackPress = () => {
		const screen = 'Main';

		const initialDataPolis = {
		  policyno_eqvet: '',
		  status_eqvet: '',
		  policy_startdate: '',
		  policy_eqvet: '',
		  policyno: '',
		  policy_enddate: null,
		  policy: '',
		  status: ''
		}
		this.props.setScreen('', 'cariasuransi');
		this.props.navigate(screen);
		this.props.setDataPolis(initialDataPolis);
		return true;
	};

	


async deletePolis(path){

	await	RNFetchBlob.fs.unlink(path)
			.then(() => { this.setState({ exists: false, refreshing: true }); })
		.catch(() => { this.setState({ refreshing: false });  })

}

	async movePolis(path, pathdownload){

		await	RNFetchBlob.fs.cp(pathdownload, path)
			.then(() => { this.setState({ exists: true, refreshing: false }); })
			.catch(() => { this.setState({ refreshing: false }); })
	}

	async checkdownloadexist(path, pathdownload) {
		await RNFetchBlob.fs.exists(pathdownload).then((exist) => {
			if (exist) {
				this.movePolis(path, pathdownload).done();
				//appFunction.toastError('true');
			} else {
				var policyLink = this.props.dataPolis.policy;
				if (dataPolis.policy.includes('jagain.com') || dataPolis.policy.includes('jagain.co.id')) 
					policyLink = 'http://' + policyLink;
				Linking.openURL(policyLink);
				//appFunction.toastError('false');
			}
		})
			.catch((err) => { console.log(err) })
	}

	async checkfileexist(path, pathdownload){
		await RNFetchBlob.fs.exists(path).then((exist) => {
			if (exist) {
				this.setState({ exists: true, refreshing: false });
				//appFunction.toastError('true');
			} else {
				this.checkdownloadexist(path, pathdownload).done();
				//appFunction.toastError('false');
			}
		})
			.catch((err) => { console.log(err) })		
	}

	onRefresh = () => {
		const dirs = RNFetchBlob.fs.dirs;
		const { dataPolis } = this.props;
		this.setState({refreshing: true});
		const path = dirs.SDCardApplicationDir + '/files/' + dataPolis.policyno + '.pdf';
		const pathdownload = dirs.DownloadDir + '/'+ dataPolis.policyno + '.pdf';
		this.deletePolis(path).done();
		if (dataPolis.policy.includes('sinarmas.co.id') || dataPolis.policy.includes('simasnet.id')) {
			this.checkfileexist(path, pathdownload).done();
		}else{
			this.doLoadPolis(dataPolis, path).done();
		}
	}

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress');
	}
	


	componentDidMount() {
		//appFunction.checkWritePermission();
		//this.checkExistsFile().done();
					
		const event_name = 'Halaman Lihat Polis Client (My Polis)';	
		
		var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
		appFunction.setScreenGA(event_name, userID); 
		appFunction.setFAScreen(event_name, 'DataPolis');

		
		const {dataPolis} = this.props;
		const dirs = RNFetchBlob.fs.dirs;
		const path = dirs.SDCardApplicationDir + '/files/'+ dataPolis.policyno +'.pdf';
		
		this.doLoadPolis(dataPolis, path).done();
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
	}





  render() {
		const {dataTransaksi, dataPolis, oid, loadingState} = this.props;
		const { exists, refreshing} = this.state;
		const dirs = RNFetchBlob.fs.dirs;
		const path = dirs.SDCardApplicationDir + '/files/' + dataPolis.policyno + '.pdf';
		const pathdownload = dirs.DownloadDir + '/' + dataPolis.policyno + '.pdf';

		const title = 'POLIS';
		var {height, width} = Dimensions.get('window');		
		console.log(dataPolis);
		
    return (
	<Container>
  <Slide>
	 <Header
        backgroundColor={'white'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorRed]} name='ios-arrow-back' /></Button>}
				centerComponent={{ text: title , style: { color: 'red', padding: 12 } }}
						rightComponent={
							<TouchableHighlight style={{ padding: 10, borderColor: 'forestgreen' }} underlayColor='#00ffff' onPress={this.onRefresh}>
								<Text style={[styles.baseText, { color: 'forestgreen' }]}>Refresh</Text>
							</TouchableHighlight>}
        />
		<OfflineNotice />
					
			
		<View style={[styles.centerItem, styles.bgWhite]}>
					
						<View style={[styles.width88p, styles.bgWhite, styles.border1, styles.left4, styles.borderColorffe5e5, styles.radius3, { marginBottom: height * .01, marginTop: height * .01}, styles.elev5]}>
              <View style={styles.left15}><Text style={[styles.baseText]}>Data Polis</Text></View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5, styles.borderTop1, styles.borderColorTopGrey]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>No. Transaksi</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{oid}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>No. Polis</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{dataPolis.policyno}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>Start Date Polis</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataPolis.policy_startdate))}</Text></View>
              </View>
              <View style={[styles.directionRow, styles.left15, styles.bottom5]}>
                <View style={styles.width30p}><Text style={[styles.baseText]}>End Date Polis</Text></View>
                <View style={styles.width5p}><Text style={[styles.baseText]}>:</Text></View>
                <View style={styles.width65p}><Text style={[styles.baseText]}>{appFunction.formatDate(new Date(dataPolis.policy_enddate))}</Text></View>
              </View>
            </View>
						{exists?
							<View style={[styles.directionRow, styles.top10, styles.bottom10, styles.centerItemContent]}>
								<View style={[styles.width50p, styles.centerItemContent]}>
									<TouchableHighlight style={{ borderWidth: 1, borderRadius: 5, padding: 5, borderColor: 'blue' }} underlayColor='#00ffff' onPress={() => { this.doDownloadPolis(dataPolis, pathdownload) }}>
										<Text style={[styles.baseText, { color: 'blue' }]}>Download Polis</Text>
									</TouchableHighlight>
								</View>

							</View>
							:
							<View style={[styles.top10, styles.bottom10, styles.centerItemContent]}>
								<Text style={{ color: '#0000ff', fontStyle: 'italic' }}>*note : Jika belum memberikan ijin penyimpanan, mohon untuk memberikan ijin penyimpanan agar dapat melihat polis anda.</Text>
							</View>
						}
						
          </View>
					{exists?  
		 <View style={styles2.container}>
			
					   <Pdf
						source={{uri:'file://'+ path,cache:false}}
						onLoadComplete={(numberOfPages,filePath)=>{
							console.log(`number of pages: ${numberOfPages}`);
						}}
						onPageChanged={(page,numberOfPages)=>{
							console.log(`current page: ${page}`);
						}}
						onError={(error)=>{
							console.log(error);
						}}
						style={styles2.pdf}/>
						
						</View> : <LoadingPage />
			 }			
						

					
      </Slide>
	  </Container>
    );
  }
}

const styles2 = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
		borderRadius: 2,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});

function mapStateToProps(state) {
  return {
    dataTransaksi: state.dataTransaksi,
	oid: state.oid,
	dataPolis: state.dataPolis,
	loadingState: state.loadingState,
	permission: state.permission,
	status: state.status,
	session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DataPolis);
