import React, { Component } from 'react';
import { Platform, Picker, Dimensions, TextInput, View, ScrollView, Text, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Slider, AsyncStorage,  ActivityIndicator } from 'react-native';
import { Footer, Container, Button, Header, Content, Body, Title, Left, Right, Card, CardItem, Icon, List, ListItem, Root } from 'native-base';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import md5 from "react-native-md5";
import * as appFunction from '../../function';
import styles from '../../assets/styles';
import LoadingPage from '../../components/loading/LoadingPage';

import DetailPendingPayment from './DetailPendingPayment';

//import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
//import { ImagePicker, Constants } from 'expo';
//import ImagePicker from 'react-native-customized-image-picker';
//import { create } from 'apisauce';
import CheckBox from 'react-native-check-box';
import Accordion from 'react-native-collapsible/Accordion';


class PendingPayment extends Component{

  constructor(props) {
    super(props);
  }


    renderListPending(listPendingPayment){
   	   var listTransaksi = [];
	   var count = 0;
	if(listPendingPayment.length > 0){
		 for(let i = 0; i < listPendingPayment.length; i++){
		 if(!listPendingPayment[i].kode_transaksi.includes("DL") 
		 || !listPendingPayment[i].kode_transaksi.includes("CG") 
		 || !listPendingPayment[i].kode_transaksi.includes("BC")
		 || !listPendingPayment[i].kode_transaksi.includes("PT")){
			if(listPendingPayment[i].status == 'Y'){
				count = count + 1;
		   listTransaksi.push(
			<View style={[styles.centerItemContent, styles.width90p]} key = {i}>
			  	
				<View style={[styles.directionRow, styles.left5, styles.bottom10, styles.top10]}>
				  <View style={[styles.width90p, styles.centerItemContent, styles.right10]}>
					<View style={[styles.directionRow, styles.centerItemContent, styles.bottom5]}>
						{ listPendingPayment[i].kode_transaksi.includes("PR") ? <Text style={[styles.baseText, styles.bold]}>ASURANSI PROPERTI</Text> : 
						listPendingPayment[i].kode_transaksi.includes("MV")? <Text style={[styles.baseText, styles.bold]}>ASURANSI KENDARAAN</Text>: listPendingPayment[i].kode_transaksi.includes("AP")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI APARTEMEN</Text> : listPendingPayment[i].kode_transaksi.includes("AC")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI KECELAKAAN</Text> : listPendingPayment[i].kode_transaksi.includes("HT")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI KESEHATAN</Text> : listPendingPayment[i].kode_transaksi.includes("LF")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI JIWA</Text> : listPendingPayment[i].kode_transaksi.includes("TR")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI TRAVEL</Text> : listPendingPayment[i].kode_transaksi.includes("DL")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI DELAY</Text> : listPendingPayment[i].kode_transaksi.includes("CG")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI CARGO</Text> : listPendingPayment[i].kode_transaksi.includes("BC")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI SEPEDA</Text> : listPendingPayment[i].kode_transaksi.includes("PT")? 
						<Text style={[styles.baseText, styles.bold]}>ASURANSI HEWAN</Text> : null }
					</View>
					 <View style={[styles.centerItemContent, styles.width90p, styles.bottom5]}>
					   <Text style={[styles.baseText]}>Kode Transaksi</Text>
					   <Text style={[styles.baseText, styles.italic]}>{listPendingPayment[i].kode_transaksi}</Text>
					 </View>
					 <View style={[styles.top5, styles.bottom5]}><Text style={[styles.baseText, styles.font11, styles.italic, styles.pad5, {color: '#ff4c4c'}]}>* Dengan mengklik bayar berarti anda setuju dengan syarat dan ketentuan yang berlaku di Jagain</Text></View>
					 <View style = {[styles.directionRow, styles.centerItemContent]}>
						<TouchableHighlight style={{ marginTop: 5, padding: 5, borderWidth: 1, borderColor: 'blue', backgroundColor: 'blue', borderRadius: 3, elevation: 10 }} underlayColor="transparent" onPress={() => { this.props.setDataTransaksi(listPendingPayment[i]); this.props.navigate('PaymentGateway') }}>
									 <Text style={[styles.font12, styles.bold, { color: 'white' }]}>Bayar</Text>
						</TouchableHighlight>
					 
					 <Text> | </Text>
					 
								 <TouchableHighlight style={{ marginTop: 5, padding: 5, borderWidth: 1, borderColor: 'forestgreen', backgroundColor: 'forestgreen', borderRadius: 3, elevation: 10 }} underlayColor="transparent" onPress={() => { this.props.setKodeTransaksiDataPending(listPendingPayment[i].kode_transaksi); this.props.navigate('DetailPendingPayment') }}>
									 <Text style={[styles.font12, styles.bold, { color: 'white' }]}>Detail Pembayaran</Text>
					</TouchableHighlight>
					 </View>
				  </View>
				
			    </View>
			  
			  { i != listPendingPayment.length-1 ? <View style={[styles.bgWhite, styles.top10, styles.width100p]}>
				<View style={[styles.directionRow, styles.left5]}>
				  <View style={[styles.width100p, styles.centerItemContent, styles.right10]}>
					<View style={{width: '90%', borderBottomWidth:1, borderBottomColor:'#e6e6e6'}} />
				  </View>
				</View>
			  </View>: null}
			  
			</View>
		   );
			}
			}
		}
		
		if(!count){
			listTransaksi.push(
				<View style={[styles.centerItemContent, styles.pad20]} key = 'datatransaksi-kosong'>
				  <Text style={[styles.baseText, styles.alignCenter]}>Belum Ada Transaksi</Text>
				</View>
			); 
		}
		
	}else{
		listTransaksi.push(
			<View style={[styles.centerItemContent, styles.pad20]} key = 'datatransaksi-kosong'>
			  <Text style={[styles.baseText, styles.alignCenter]}>Belum Ada Transaksi</Text>
			</View>
		);
	}
      return listTransaksi;
	  
	  
    }

  componentDidMount(){
	const event_name = 'Halaman My Cart';	
		
	var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	appFunction.setScreenGA(event_name, userID); 
	appFunction.setFAScreen(event_name, 'PendingPayment');
		
    const {username} = this.props.session;
    //console.log(username);
    this.props.getPendingPayment(username);
  }


	render() {
    var {height, width} = Dimensions.get('window');
    const {listPendingPayment, loadingState} = this.props;
	if(loadingState.loadingListPendingPayment){
		return(
			<LoadingPage />
		)
	}
    //console.log(listPendingPayment);
	    return (
			<View style={[styles.centerItemContent]}>
				<View style={[styles.top5, styles.bottom5, styles.left5, styles.pad5]}>
					<Text style={[styles.baseText, styles.font12, styles.italic, {color: '#0073e5'}]}>
						* Produk Asuransi Yang Telah Dipilih Akan Dihapus Dari My Cart Jika Tidak Melakukan Pembayaran Dalam Waktu 3 Hari!
					</Text>
				</View>
				<View style={[styles.bottom5, styles.left5, styles.pad5]}>
					<Text style={[styles.baseText, styles.font12, styles.italic, {color: '#0073e5'}]}>
						* Jika Sudah Melakukan Pembayaran Dengan Menggunakan Kartu Kredit dan Pembayaran Gagal, Mohon Untuk Melakukan Pembelian Ulang Dikarenakan Tidak Dapat Melakukan Pembayaran Dengan Kartu Kredit >2 Kali Dalam Satu Transaksi. 
					</Text>
				</View>
				<View style={[styles.bottom5, styles.left5, styles.pad5, styles.directionRow, styles.width100p]}>
					<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText]}>
					* 
					</Text>
					<TouchableHighlight underlayColor='blue' onPress={()=> {this.props.navigate('Agreement')} }>
						<Text style={[{color:'#4c4cff'}, styles.font14, styles.baseText, styles.underline]}>
						syarat dan ketentuan
						</Text>
					</TouchableHighlight>
				</View>
				<View style={[styles.bgWhite, styles.centerItemContent, styles.width96p, styles.top5, styles.bottom5, styles.elev2, styles.radius5]}>
					{this.renderListPending(listPendingPayment)}
				</View>
			</View>
		);
	}
}


function mapStateToProps(state) {
  return {
	  status:state.status,
    session: state.session,
    listPendingPayment: state.listPendingPayment,
	loadingState:state.loadingState,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PendingPayment);
