import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import CariAsuransiMV from '../../lobItems/MV/CariAsuransi';
import CariAsuransiProperty from '../../lobItems/Property/CariAsuransi';
import CariAsuransiApartment from '../../lobItems/Apartment/CariAsuransi';
import CariAsuransiPA from '../../lobItems/PA/CariAsuransi';
import CariAsuransiHealth from '../../lobItems/Health/CariAsuransi';
import CariAsuransiLife from '../../lobItems/Life/CariAsuransi';
import CariAsuransiDelay from '../../lobItems/Delay/CariAsuransi';
import CariAsuransiTravel from '../../lobItems/Travel/CariAsuransi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Slide from '../../../animated/Slide';
import styles from '../../../assets/styles';

import * as appFunction from '../../../function';
import OfflineNotice from '../../../components/offline_notice';

class CariAsuransi extends Component{
  constructor(props) {
      super(props);
  }

  activeContent(screen){
    if(screen == 'MV'){
      return <CariAsuransiMV />;
    }else if(screen == 'Property'){
      return <CariAsuransiProperty />;
    }
    else if(screen == 'Apartment'){
      return <CariAsuransiApartment />;
    }
    else if(screen == 'PA'){
      return <CariAsuransiPA />;
    }
	 else if(screen == 'Health'){
      return <CariAsuransiHealth />;
    }
	 else if(screen == 'Life'){
      return <CariAsuransiLife />;
    }
	 else if(screen == 'Delay'){
      return <CariAsuransiDelay />;
    }
	 else if(screen == 'Travel'){
      return <CariAsuransiTravel />;
    }
  }

  onBackPress = () => {
    var screen = 'Main';
    this.props.setScreen('', 'cariasuransi');
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username, visitor_id } = this.props.session;
	
	const event_name = 'Halaman Pencarian Asuransi ' + asuransiScreen;	
		
	var userID = isLogin ? visitor_id : '';
	appFunction.setScreenGA(event_name , userID); 
	
	if(!username)
		appFunction.lobEvent(event_name );
	else
		appFunction.lobEvent(event_name , username);
		
	
	this.props.resetDataVoucher();
	this.props.setKodeVoucher('');
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

	render() {

    const { asuransiScreen } = this.props.screen;
    const title = appFunction.getTitle(asuransiScreen);
    //console.log('Content',cariAsuransiScreen);

    return(
      <Container>
      <Slide>
        <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
		<OfflineNotice />
        <Content style={[styles.bgWhite]}>
            {this.activeContent(asuransiScreen)}
        </Content>
      </Slide>
      </Container>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
	status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CariAsuransi);
