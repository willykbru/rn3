import React, { Component } from 'react';
import { BackHandler, Alert, Text, WebView, View, StyleSheet, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';
import * as appFunction from '../../../function'

import Slide from '../../../animated/Slide';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import RNAmplitute from 'react-native-amplitude-analytics';
import OfflineNotice from '../../../components/offline_notice';

class PaymentGateway extends Component {

	constructor(props) {

	    super(props);
		this.amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	    this.state = {
				link_pembayaran: '',
			}
		}
		
	onNavigationStateChange(state){
		console.log(state);
		if(state.url == 'http://dev.jagain.com/' ||
			state.url == 'https://dev.jagain.com/' ||
			state.url == 'http://dev.jagain.co.id/' || 
			state.url == 'https://dev.jagain.co.id/'  ||
			state.url == 'http://jagain.co.id/' ||
			state.url == 'http://www.jagain.com' ||
			state.url == 'http://www.jagain.com/' ||
			state.url == 'http://jagain.com' ||
			state.url == 'http://jagain.com/' ||
			state.url == 'https://jagain.co.id/' || 
			state.url == 'https://www.jagain.com' ||
			state.url == 'https://www.jagain.com/' ||
			state.url == 'https://jagain.com' ||
			state.url == 'https://jagain.com/' 
			){
		  this.props.navigate('Main');
		}
		//http://dev.jagain.com/
	}
	
	alertOnBack(){
		// Works on both iOS and Android
		Alert.alert(
		  'Kembali ke Halaman Home',
		  'Anda akan diarahkan ke Halaman Home, Untuk Melanjutkan Transaksi dapat dilakukan di Menu My Cart',
		  [
			{text: 'Batal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
			{text: 'Kembali ke Home', onPress: () =>{this.onBackPress()}},
		  ],
		  { cancelable: false }
		)
		
	}
	
	onBackPress = () => {
		const screen = 'Main';
		this.props.setScreen('', 'cariasuransi');
		this.props.navigate(screen);
		return true;
	};

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress');
	}

	componentDidMount() {	
		const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username, visitor_id } = this.props.session;
	
	const event_name = 'Halaman Bayar (Payment Gateway) Asuransi ' + asuransiScreen;	
		
		var userID = isLogin ? visitor_id : '';
		appFunction.setScreenGA(event_name, userID); 
		
	
	if(!username)
		appFunction.lobEvent(event_name);
	else
		appFunction.lobEvent(event_name, username);
	
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
	}

  render() {
		const {dataTransaksi} = this.props;
		console.log('payment gatewy',dataTransaksi);

		const title = 'PEMBAYARAN';    
		

    return (
	<Container>
  <Slide>
	 <Header
  			backgroundColor={'red'}
  			leftComponent={<Button transparent onPress={()=> this.alertOnBack()}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
  			centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
  		  />
		  <OfflineNotice />
      <WebView
        ref={(view) => this.webView = view}
        source={{uri: dataTransaksi.link_pembayaran}}
				onNavigationStateChange={this.onNavigationStateChange.bind(this)}
						automaticallyAdjustContentInsets={false}
						javaScriptEnabled={true}
						domStorageEnabled={true}
						decelerationRate="normal"
						startInLoadingState={true}
      />
      </Slide>
	  </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataTransaksi: state.dataTransaksi,
	screen: state.screen,
	status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentGateway);
