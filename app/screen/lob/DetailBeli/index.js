import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import DetailBeliMV from '../../lobItems/MV/DetailBeli';
import DetailBeliProperty from '../../lobItems/Property/DetailBeli';
import DetailBeliApartment from '../../lobItems/Apartment/DetailBeli';
import DetailBeliPA from '../../lobItems/PA/DetailBeli';
import DetailBeliHealth from '../../lobItems/Health/DetailBeli';
import DetailBeliLife from '../../lobItems/Life/DetailBeli';
import DetailBeliDelay from '../../lobItems/Delay/DetailBeli';
import DetailBeliTravel from '../../lobItems/Travel/DetailBeli';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Slide from '../../../animated/Slide';
import styles from '../../../assets/styles';

import * as appFunction from '../../../function';
import OfflineNotice from '../../../components/offline_notice';

class DetailBeli extends Component{
  constructor(props) {
      super(props);
  }

  activeContent(screen){
    if(screen == 'MV'){
      return <DetailBeliMV />;
    }
    else if(screen == 'Property'){
      return <DetailBeliProperty />;
    }
    else if(screen == 'Apartment'){
      return <DetailBeliApartment />;
    }
    else if(screen == 'PA'){
      return <DetailBeliPA />;
    }
	else if(screen == 'Health'){
      return <DetailBeliHealth />;
    }	
	else if(screen == 'Life'){
      return <DetailBeliLife />;
    }
	else if(screen == 'Delay'){
      return <DetailBeliDelay />;
    }
	else if(screen == 'Travel'){
      return <DetailBeliTravel />;
    }
	
  }

  onBackPress = () => {
    const {isLogin} = this.props.status;
    var screen = 'ListAsuransi';
	this.props.resetDataVoucher();
	this.props.setKodeVoucher('');
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username, visitor_id } = this.props.session;
	
	const event_name = 'Halaman Detail Pembelian Asuransi ' + asuransiScreen;	
		
	var userID = isLogin ? visitor_id : '';
	appFunction.setScreenGA(event_name, userID); 
	
	
	if(!username)
		appFunction.lobEvent(event_name);
	else
		appFunction.lobEvent(event_name, username);
	
	//Alert.alert('dataVoucher');
	
	if(this.props.dataVoucher.description !== 'ok'){
 		this.props.resetDataVoucher();	
	}
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
   }

	render() {

    const { asuransiScreen } = this.props.screen;
    const title = 'DETAIL PEMBELIAN';
    //console.log('Content',cariAsuransiScreen);

    return(
      <Container>
      <Slide>
        <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
		<OfflineNotice />
        <Content style={[styles.bgWhite]}>
            {this.activeContent(asuransiScreen)}
        </Content>
      </Slide>
      </Container>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
    status: state.status,
    session: state.session,
	dataVoucher: state.dataVoucher,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailBeli);
