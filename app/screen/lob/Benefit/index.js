import React, { Component } from 'react';
import { BackHandler, Alert, Text, WebView, View, StyleSheet, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Footer, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import { Header } from 'react-native-elements';
import styles from '../../../assets/styles';
import * as appFunction from '../../../function'

import Slide from '../../../animated/Slide';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';
import OfflineNotice from '../../../components/offline_notice';
//import RNAmplitute from 'react-native-amplitude-analytics';

class Benefit extends Component {

	constructor(props) {
	    super(props);
		}
	
	onBackPress = () => {
		var screen = null;
		if(this.props.screen.renewalScreen){
			screen = 'Renewal';
		}else{
			screen = 'ListAsuransi';
		}
		this.props.navigate(screen);
		this.props.resetDataBenefit;
		return true;
	};
	
	//intializeAsuransi(screen){
		
		//if(screen == 'Travel'){
		 		//}
		//this.setState({benefit_url});
	//}

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress');
	}

	componentDidMount() {	
		const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username, visitor_id } = this.props.session;
	const event_name = 'Halaman Benefit Asuransi ' + asuransiScreen;	
		
	var userID = isLogin ? visitor_id : '';
	appFunction.setScreenGA(event_name, userID); 
	
	if(!username)
		appFunction.lobEvent(event_name);
	else
		appFunction.lobEvent(event_name, username);
	
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
	}

  render() {
		const {dataBenefit} = this.props;
		const { asuransiScreen } = this.props.screen;
		//const {benefit_url} = this.state;
	//	console.log(dataBenefit.benefit_url);
		const title = 'BENEFIT';   
			//console.log(dataBenefit);
			 const asimgsrc = {
      'ADIRA'  : require('../../../assets/icons/asuransi/adira.png'),
      'ALLIANZ'    : require('../../../assets/icons/asuransi/allianz.png'),
      'ASOKAMAS'    : require('../../../assets/icons/asuransi/asoka.png'),
      'AVRIST'    : require('../../../assets/icons/asuransi/avrist.png'),
      'AXA'    : require('../../../assets/icons/asuransi/axa.png'),
      'FPG'    : require('../../../assets/icons/asuransi/fpg.png'),
      'MAG Fairfax'    : require('../../../assets/icons/asuransi/mag.jpg'),
      'SIMASNET'    : require('../../../assets/icons/asuransi/simasnet.png'),
      'ASURANSI SINARMAS'    : require('../../../assets/icons/asuransi/sinarmas.png'),
      'ZURICH'    : require('../../../assets/icons/asuransi/zurich.png'),
      'SIMAS JIWA' : require('../../../assets/icons/asuransi/simasjiwa.jpg'),
    }
		
    return (
	<Container>
  <Slide> 
	 <Header
  			backgroundColor={'red'}
  			leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
  			centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
  		  />
		  <OfflineNotice />
		  <View style={[{backgroundColor: 'white', borderBottomWidth:.05}, styles.directionRow, styles.centerItemContent, styles.elev3]}>
 
		   <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                  <TouchableHighlight>
                    <View style={styles.centerItemContent}>
                       <View style={[styles.centerItemContent, styles.height50, styles.padTop5]}>
                           <Image source={require('../../../assets/icons/logo/logo-big.png')} style={[styles.height40, styles.width60, styles.resizeContain]}/>
                        </View>
                      </View>
                    </TouchableHighlight>                
                  </View>
		  <View style={[styles.width30p, styles.centerItemContent, styles.right10]}>
                  <TouchableHighlight>
                    <View style={styles.centerItemContent}>
                       <View style={[styles.centerItemContent, styles.height50]}>
                           <Image source={asimgsrc[dataBenefit.insurance_name]} style={[styles.height25, styles.width100, styles.resizeContain]}/>
                        </View>
                      </View>
                    </TouchableHighlight>
                    <View style={styles.bottom10}></View>
                  </View>
		  </View>
		   {dataBenefit.benefit_url?<WebView
			ref={(view) => this.webView = view}
			source={{uri: dataBenefit.benefit_url}}
				automaticallyAdjustContentInsets={false}
				 javaScriptEnabled={true}
				 domStorageEnabled={true}
				 decelerationRate="normal"
				 startInLoadingState={true}
		  />:null}		  
      </Slide>
	  </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataTransaksi: state.dataTransaksi,
	screen: state.screen,
	status: state.status,	
    session: state.session,
	travelState: state.travelState,
	dataBenefit: state.dataBenefit,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Benefit);
