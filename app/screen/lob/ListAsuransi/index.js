import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import ListAsuransiMV from '../../lobItems/MV/ListAsuransi';
import HeaderListAsuransiMV from '../../lobItems/MV/HeaderListAsuransi';
import HeaderListAsuransiProperty from '../../lobItems/Property/HeaderListAsuransi';
import ListAsuransiProperty from '../../lobItems/Property/ListAsuransi';
import ListAsuransiApartment from '../../lobItems/Apartment/ListAsuransi';
import ListAsuransiPA from '../../lobItems/PA/ListAsuransi';
import ListAsuransiHealth from '../../lobItems/Health/ListAsuransi';
import ListAsuransiLife from '../../lobItems/Life/ListAsuransi';
import ListAsuransiDelay from '../../lobItems/Delay/ListAsuransi';
import ListAsuransiTravel from '../../lobItems/Travel/ListAsuransi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Slide from '../../../animated/Slide';
import styles from '../../../assets/styles';

import * as appFunction from '../../../function';
import OfflineNotice from '../../../components/offline_notice';

class ListAsuransi extends Component{
  constructor(props) {
      super(props);
  }

	getTitle(screen){
		var title = '';
		if(screen == 'MV'){
		  title = 'LIST ASURANSI KENDARAAN';
		}else if(screen == 'Property'){
		  title = 'LIST ASURANSI RUMAH';
		}
		else if(screen == 'Apartment'){
		  title = 'LIST ASURANSI APARTEMEN';
		}
		else if(screen == 'PA'){
		  title = 'LIST ASURANSI KECELAKAAN';
		}
		else if(screen == 'Health'){
		  title = 'LIST ASURANSI KESEHATAN';
		}
		else if(screen == 'Life'){
		  title = 'LIST ASURANSI JIWA';
		}
		else if(screen == 'Delay'){
		  title = 'LIST ASURANSI DELAY';
		}
		else if(screen == 'Travel'){
		  title = 'LIST ASURANSI TRAVEL';
		}
		return title;
		
	}
	
  activeContent(screen){
    if(screen == 'MV'){
      return <ListAsuransiMV />;
    }else if(screen == 'Property'){
      return <ListAsuransiProperty />;
    }
    else if(screen == 'Apartment'){
      return <ListAsuransiApartment />;
    }
    else if(screen == 'PA'){
      return <ListAsuransiPA />;
    }
	else if(screen == 'Health'){
      return <ListAsuransiHealth />;
    }
	else if(screen == 'Life'){
      return <ListAsuransiLife />;
    }
	else if(screen == 'Delay'){
      return <ListAsuransiDelay />;
    }
	else if(screen == 'Travel'){
      return <ListAsuransiTravel />;
    }

  }

  headerContent(screen, loadingState){
	if(screen == 'MV'){
		return (
			<HeaderListAsuransiMV />
		)
	}else if(screen == 'Property'){
		return <HeaderListAsuransiProperty />;
    }
  }

  onBackPress = () => {
    var screen = 'CariAsuransi';
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	  	  
	const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
     const { username, visitor_id } = this.props.session;
	
				
		const event_name = 'Halaman Pemilihan Asuransi dari List Asuransi ' + asuransiScreen;	
		
		var userID = isLogin ? visitor_id : '';
		appFunction.setScreenGA(event_name, userID); 
		
	if(!username)
		appFunction.lobEvent(asuransiScreen);
	else
		appFunction.lobEvent(asuransiScreen, username);

    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

	render() {

    const { asuransiScreen } = this.props.screen;
	const {loadingState} = this.props;
	
	const title = this.getTitle(asuransiScreen);
    
    //console.log('Content',cariAsuransiScreen);

    return(
      <Container>
      <Slide>
        <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
		<OfflineNotice />
        {this.headerContent(asuransiScreen, loadingState)}
		{ asuransiScreen == 'MV' ? <View style={[styles.centerItemContent, styles.elev2, styles.bottom5, {borderTopWidth:.2, borderTopColor: 'white'}]} /> : null }
		<Content>
				{this.activeContent(asuransiScreen)}
		</Content>
      </Slide>
      </Container>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
	loadingState:state.loadingState,
	status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAsuransi);
