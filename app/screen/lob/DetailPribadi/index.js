import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import DetailPribadiMV from '../../lobItems/MV/DetailPribadi';
import DetailPribadiProperty from '../../lobItems/Property/DetailPribadi';
import DetailPribadiApartment from '../../lobItems/Apartment/DetailPribadi';
import DetailPribadiPA from '../../lobItems/PA/DetailPribadi';
import DetailPribadiHealth from '../../lobItems/Health/DetailPribadi';
import DetailPribadiLife from '../../lobItems/Life/DetailPribadi';
import DetailPribadiDelay from '../../lobItems/Delay/DetailPribadi';
import DetailPribadiTravel from '../../lobItems/Travel/DetailPribadi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Slide from '../../../animated/Slide';
import styles from '../../../assets/styles';

import * as appFunction from '../../../function';
import OfflineNotice from '../../../components/offline_notice';

class DetailPribadi extends Component{
  constructor(props) {
      super(props);
  }

  activeContent(screen){
    if(screen == 'MV'){
      return <DetailPribadiMV />;
    }
    else if(screen == 'Property'){
      return <DetailPribadiProperty />;
    }
    else if(screen == 'Apartment'){
      return <DetailPribadiApartment />;
    }
    else if(screen == 'PA'){
      return <DetailPribadiPA />;
    }
	else if(screen == 'Health'){
      return <DetailPribadiHealth />;
    }
	else if(screen == 'Life'){
      return <DetailPribadiLife />;
    }
	else if(screen == 'Delay'){
      return <DetailPribadiDelay />;
    }
	else if(screen == 'Travel'){
      return <DetailPribadiTravel />;
    }
  }

  onBackPress = () => {
    const {isLogin} = this.props.status;
    var screen = 'DetailBeli';
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	  
	const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username, visitor_id } = this.props.session;
	
				
		const event_name = 'Halaman Pengisian Data Polis Asuransi ' + asuransiScreen;	
		
		var userID = isLogin ? visitor_id : '';
		appFunction.setScreenGA(event_name, userID); 

	
	if(!username)
		appFunction.lobEvent(event_name);
	else
		appFunction.lobEvent(event_name, username);
		
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

	render() {

    const { asuransiScreen } = this.props.screen;
    const title = 'DATA PRIBADI';
    //console.log('Content',cariAsuransiScreen);

    return(
      <Container>
      <Slide>
        <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
		<OfflineNotice />
        <Content style={[styles.bgWhite]}>
            {this.activeContent(asuransiScreen)}
        </Content>
      </Slide>
      </Container>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
    status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPribadi);
