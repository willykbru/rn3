import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

import PembayaranMV from '../../lobItems/MV/Pembayaran';
import PembayaranProperty from '../../lobItems/Property/Pembayaran';
import PembayaranApartment from '../../lobItems/Apartment/Pembayaran';
import PembayaranPA from '../../lobItems/PA/Pembayaran';
import PembayaranHealth from '../../lobItems/Health/Pembayaran';
import PembayaranLife from '../../lobItems/Life/Pembayaran';
import PembayaranDelay from '../../lobItems/Delay/Pembayaran';
import PembayaranTravel from '../../lobItems/Travel/Pembayaran';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Slide from '../../../animated/Slide';
import styles from '../../../assets/styles';

import * as appFunction from '../../../function';

class Pembayaran extends Component{
  constructor(props) {
      super(props);
  }

  activeContent(screen){
    if(screen == 'MV'){
      return <PembayaranMV />;
    }
    else if(screen == 'Property'){
		
      return <PembayaranProperty />;
    }
    else if(screen == 'Apartment'){
      return <PembayaranApartment />;
    }
    else if(screen == 'PA'){
      return <PembayaranPA />;
    }
	 else if(screen == 'Health'){
      return <PembayaranHealth />;
    }
	 else if(screen == 'Life'){
      return <PembayaranLife />;
    }
	 else if(screen == 'Delay'){
      return <PembayaranDelay />;
    }
	 else if(screen == 'Travel'){
      return <PembayaranTravel />;
    }
  }
  
  	// alertOnBack(){
		// Works on both iOS and Android
		// Alert.alert(
		  // 'Kembali ke Halaman Home',
		  // 'Anda akan diarahkan ke Halaman Home, Untuk Melanjutkan Transaksi dapat dilakukan di Menu My Cart',
		  // [
			// {text: 'Batal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
			// {text: 'Kembali ke Home', onPress: () =>{this.onBackPress()}},
		  // ],
		  // { cancelable: false }
		// )
		
	// }

  onBackPress = () => {
    const {isLogin} = this.props.status;
    var screen = 'DetailPribadi';
	//this.props.setScreen('', 'cariasuransi');
    this.props.navigate(screen);
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username } = this.props.session;
	//appFunction.setTransactionGA(transactions);
	if(!username)
		appFunction.lobEvent('User Sedang Melihat Detail Pembayaran Asuransi '+ asuransiScreen);
	else
		appFunction.lobEvent('User Sedang Melihat Detail Pembayaran Asuransi '+ asuransiScreen, username);		
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

	render() {

    const { asuransiScreen } = this.props.screen;
    const title = appFunction.getTitle(asuransiScreen);
    //console.log('Content',cariAsuransiScreen);

    return(
      <Container>
      <Slide>
        <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={() => this.onBackPress()}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
        <Content style={[styles.bgWhite]}>
            {this.activeContent(asuransiScreen)}
        </Content>
      </Slide>
      </Container>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
    status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Pembayaran);
