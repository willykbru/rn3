import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';

//import DetailPribadiMV from '../../lobItems/MV/DetailPribadi';
//import DetailPribadiProperty from '../../lobItems/Property/DetailPribadi';
//import DetailPribadiApartment from '../../lobItems/Apartment/DetailPribadi';
import RenewalPA from '../../lobItems/PA/Renewal';
import CariAsuransiRenewalPA from '../../lobItems/PA/CariAsuransiRenewal';
import ListAsuransiRenewalPA from '../../lobItems/PA/ListAsuransiRenewal';
import PembayaranRenewalPA from '../../lobItems/PA/PembayaranRenewal';
import CariAsuransiRenewalMV from '../../lobItems/MV/CariAsuransiRenewal';
import ListAsuransiRenewalMV from '../../lobItems/MV/ListAsuransiRenewal';
import RenewalMV from '../../lobItems/MV/Renewal';
import PembayaranRenewalMV from '../../lobItems/MV/PembayaranRenewal'; 
import HeaderListAsuransiRenewalMV from '../../lobItems/MV/HeaderListAsuransiRenewal';
//import DetailPribadiHealth from '../../lobItems/Health/DetailPribadi';
//import DetailPribadiLife from '../../lobItems/Life/DetailPribadi';
//import DetailPribadiDelay from '../../lobItems/Delay/DetailPribadi';
//import DetailPribadiTravel from '../../lobItems/Travel/DetailPribadi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Slide from '../../../animated/Slide';
import styles from '../../../assets/styles';

import * as appFunction from '../../../function';
import OfflineNotice from '../../../components/offline_notice';

class Renewal extends Component{
  constructor(props) {
      super(props);
  }

  activeContent(screen, renewalScreen){
    //if(screen == 'MV'){
    //  return <DetailPribadiMV />;
    //}
    //else if(screen == 'Property'){
    //  return <DetailPribadiProperty />;
    //}
    //else if(screen == 'Apartment'){
    //  return <DetailPribadiApartment />;
   // }
    //else 
		if(screen == 'PA'){
      if (renewalScreen == 'CariAsuransiRenewal')
        return <CariAsuransiRenewalPA />;
			else if(renewalScreen == 'ListAsuransiRenewal')
				return <ListAsuransiRenewalPA />;
			else if(renewalScreen == 'DataRenewal')
				return <RenewalPA />;
			else if(renewalScreen == 'PembayaranRenewal')
				return <PembayaranRenewalPA />
    } else if (screen == 'MV') {
      if (renewalScreen == 'CariAsuransiRenewal')
        return <CariAsuransiRenewalMV />;
       else if (renewalScreen == 'ListAsuransiRenewal')
         return <ListAsuransiRenewalMV />;
       else if (renewalScreen == 'DataRenewal')
         return <RenewalMV />;
      else if (renewalScreen == 'PembayaranRenewal')
        return <PembayaranRenewalMV />
    }
	//else if(screen == 'Health'){
    //  return <DetailPribadiHealth />;
   // }
	//else if(screen == 'Life'){
    //  return <DetailPribadiLife />;
   // }
	//else if(screen == 'Delay'){
    //  return <DetailPribadiDelay />;
    //}
	//else if(screen == 'Travel'){
   //   return <DetailPribadiTravel />;
   // }
  }

  headerContent(screen, renewalScreen) {
    if (screen == 'MV') {
      if (renewalScreen == 'ListAsuransiRenewal')
        return (
          <HeaderListAsuransiRenewalMV />
        )
    } 
    // else if (screen == 'Property') {
    //   return <HeaderListAsuransiRenewalProperty />;
    // }
  }

  onBackPress = () => {
    const {isLogin} = this.props.status;
    const {screen} = this.props;
	 var backscreen = 'Main';
   
  if (screen.renewalScreen == 'ListAsuransiRenewal')
      this.props.setScreen('CariAsuransiRenewal', 'renewal');
	else if(screen.renewalScreen == 'DataRenewal')
		this.props.setScreen('ListAsuransiRenewal', 'renewal');
	else if(screen.renewalScreen == 'PembayaranRenewal')
		this.props.setScreen('ListAsuransiRenewal', 'renewal');
	else{
		this.props.setScreen('', 'cariasuransi');
		this.props.setScreen('', 'renewal');
		this.props.navigate(backscreen);
	}
    return true;
  };

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress');
  }

  componentDidMount() {
	  
	const { asuransiScreen } = this.props.screen;
	const { isLogin } = this.props.status;
    const { username } = this.props.session;
	if(!username)
		appFunction.lobEvent('User Sedang Melakukan Renewal Polis Asuransi '+ asuransiScreen);
	else
		appFunction.lobEvent('User Sedang Melakukan Renewal Polis Asuransi '+ asuransiScreen, username);
		
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

	render() {

    const { asuransiScreen, renewalScreen } = this.props.screen;
    const title = appFunction.getTitle(asuransiScreen);  
    //console.log('Content',cariAsuransiScreen);

    return(
      <Container>
      <Slide>
        <Header
        backgroundColor={'red'}
        leftComponent={<Button transparent onPress={this.onBackPress}><Icon style={[styles.colorWhite]} name='ios-arrow-back' /></Button>}
        centerComponent={{ text: title , style: { color: 'white', padding: 12 } }}
        />
		<OfflineNotice />
          {this.headerContent(asuransiScreen, renewalScreen)}
          {asuransiScreen == 'MV' ? <View style={[styles.centerItemContent, styles.elev2, styles.bottom5, { borderTopWidth: .2, borderTopColor: 'white' }]} /> : null}
        <Content>
            {this.activeContent(asuransiScreen, renewalScreen)}
        </Content>
      </Slide>
      </Container>
    );
	}
}

function mapStateToProps(state) {
  return {
    screen: state.screen,
    status: state.status,	
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Renewal);
