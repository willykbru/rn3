/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

// import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');

RkTheme.setType('RkButton', 'button1', {
    container: {
        backgroundColor: '#FF0000',
        width: '60%',
        marginBottom: 16,
    },
    content: {
        color: 'white',
        fontSize: 14,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

class LaporMVMenu1 extends Component {
    constructor(props) {
        super(props);
    }

    kembali = () => {
        //this.goBack(); // works best when the goBack is async
        this.props.navigate('Main');
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        navigator.geolocation.getCurrentPosition(
            (position) => {
                // console.log(position);
                // this.setState({
                //     latitude: position.coords.latitude,
                //     longitude: position.coords.longitude,
                //     error: null,
                //     speed: position.coords.speed,
                // });

                temp = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                    speed: position.coords.speed,
                }

                this.props.setLokasiSekarang(temp);
            },
            (error) => { console.log('lapor mv kecelakaan bengkel', error) },
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
        );
    }

    componentWillUnmount() {

    }

    componentWillMount(){
        
    }

    shouldComponentUpdate(nextProps, nextState){
        if(nextProps.listLokasiSekarang != this.props.listLokasiSekarang){
            return true;
        }
    }

    componentWillReceiveProps(nextProps, prevProps){
        if(nextProps.listLokasiSekarang != prevProps){
            this.forceUpdate();
        }
    }

    render() {
        const {listLokasiSekarang} = this.props;
        console.log(listLokasiSekarang);
        return(
            <View style={{flex:1}}>
                <View>
                    {this.view_header()}
                </View>
                <View style={styles.container}>
                    {this.view_title()}
                    {this.view_menu()}
                </View>
            </View>
        );
    }

    goToKecelakaan = () =>{
        items = [{
            'end' : 1,
            'current' : 1
        }]
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanUmum');
        // this.props.navigate('LaporMVKecelakaanPengemudi');
        // this.props.navigate('LaporMVKecelakaanPihakKetiga');
        // this.props.navigate('LaporMVKecelakaanKronologis');
        // this.props.navigate('LaporMVKecelakaanBengkel');
        // this.props.navigate('LaporMVKecelakaanFoto');
        // this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }

    goToKehilanganTotal = () => {
        items = [{
            'end' : 1,
            'current' : 1
        }]

        this.props.setHalaman(items);

        // this.props.navigate('LaporMVKehilanganTotalUmum');
        // this.props.navigate('LaporMVKehilanganTotalDokumenFisik');
        // this.props.navigate('LaporMVKehilanganTotalPengemudi');
        // this.props.navigate('LaporMVKehilanganTotalKepolisian');
        // this.props.navigate('LaporMVKehilanganTotalSaksi');
        // this.props.navigate('LaporMVKehilanganTotal')
        this.props.navigate('LaporMVKehilanganTotalKonfirmasi');
    }

    view_header = () => {
        return(
            <Header
                leftComponent={
                    <TouchableOpacity onPress={() => { this.kembali() }}>
                    <Icon name='md-arrow-round-back' size={25} color='#FFF' />
                    </TouchableOpacity>
                }
                centerComponent={{
                    text: 'Pelaporan',
                    style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                }}
                outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
            />
        );
    }

    view_title = () => {
        return(
            <View style={{marginBottom: 16}}>
                <Text style={{fontSize: 18, fontWeight:'bold'}}>JENIS PELAPORAN</Text>
            </View>
        );
    }

    view_menu = () => {
        return(
            <View>
                <RkButton rkType='danger button1' onPress={()=>{this.goToKecelakaan()}}>Kecelakaan Kendaraan Bermotor</RkButton>
                <RkButton rkType='danger button1'>Kehilangan Bagian Kendaraan Bermotor</RkButton>
                <RkButton rkType='danger button1' onPress={()=>{this.goToKehilanganTotal()}}>Kehilangan Kendaraan Bermotor</RkButton>
            </View>
        );
    }

    
}

function mapStateToProps(state) {
  return {
    listLokasiSekarang: state.listLokasiSekarang,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        marginTop: 0,
        height: height - 80,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVMenu1);
