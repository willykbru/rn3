/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

// import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

// import Icon from 'react-native-vector-icons/Ionicons';
// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

// import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');


class Class_name extends Component {
  constructor(props) {
    super(props);

  }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    // this.props.navigate('ClaimMVPengemudi');
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);



  }

  componentWillUnmount() {

  }


  render() {
  
    }
}

function mapStateToProps(state) {
  return {

    loadingStateKlaim: state.loadingStateKlaim, //dummy
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  
});

export default connect(mapStateToProps, mapDispatchToProps)(class_name);
