/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

// import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

// import Icon from 'react-native-vector-icons/Ionicons';
// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

// import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

const { width, height } = Dimensions.get('window');


class LaporMVLihat1 extends Component {
    constructor(props) {
        super(props);

    }

    kembali = () => {
        //this.goBack(); // works best when the goBack is async
        this.props.navigate('Main');
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {

    }


    render() {
        <View>
            {this.view_header()}
        </View>
    }

    view_header = () => {
        return(
            <Header
                leftComponent={
                    <TouchableOpacity onPress={() => { this.kembali() }}>
                    <Icon name='md-arrow-round-back' size={25} color='#FFF' />
                    </TouchableOpacity>
                }
                centerComponent={{
                    text: 'Pelaporan',
                    style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                }}
                outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
            />
        );
    }
}

function mapStateToProps(state) {
  return {

    loadingStateKlaim: state.loadingStateKlaim, //dummy
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVLihat1);
