/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, ImageBackground, Alert, KeyboardAvoidingView } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Navigasi from '../kecelakaan/laporMVKecelakaanNavigasi';

const { width, height } = Dimensions.get('window');

RkTheme.setType('RkButton', 'button1', {
    container: {
        backgroundColor: '#FF0000',
        width: '60%',
        marginBottom: 16,
    },
    content: {
        color: 'white',
        fontSize: 14,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

class LaporMVKecelakaanUmum1 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      namaLengkap: this.props.detailPolis.nama_tertanggung,
      emailTertanggung: this.props.detailPolis.email_tertanggung,
      noTeleponTertanggung: this.props.detailPolis.phone_tertanggung,
      noPolisi: this.props.detailPolis.no_plat,
      kecepatanInsiden: this.props.dataClaim.kecepatanInsiden,
      deskripsiInsiden: this.props.dataClaim.deskripsiInsiden,
      alamatKejadian: this.props.dataClaim.alamatKejadian,

      visibleTipeInsiden: false,
      pickedTipeInsiden: null,
      labelTipeInsiden: this.props.dataClaim.labelTipeInsiden,

      visibleLokasiProvinsi: false,
      pickedLokasiProvinsi: null,
      labelProvinsiInsiden: this.props.dataClaim.labelProvinsiInsiden,

      visibleLokasiKota: false,
      pickedLokasiKota: null,
      labelKotaInsiden: this.props.dataClaim.labelKotaInsiden,

      tanggalSekarang: new Date(),
      pickedTanggal: this.props.dataClaim.tanggalKejadian,
      isPickedTanggal: false,

      pickedJam: this.props.dataClaim.jamInsiden,
      jamSekarang: new Date().getHours() + ":" + new Date().getMinutes(),
      isPickedJam: false,

    };

    this.onFocus = this.onFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    this.noPolisRef = this.updateRef.bind(this, 'noPolis');
    this.alamatKejadianRef = this.updateRef.bind(this, 'alamatKejadian');
    this.noTeleponTertanggungRef = this.updateRef.bind(this,'noTeleponTertanggung');
    this.emailTertanggungRef = this.updateRef.bind(this,'emailTertanggung');
   }

  handleBackPress = () => {
    //this.goBack(); // works best when the goBack is async
    const {dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
      this.props.navigate('LaporMVMenu');
    } else{
      temp = {
        visibleNavigation : true,
      }

      this.props.setDataClaimMV(temp);
      this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }

    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    this.props.getProvinsi();
    this.props.getTipeInsiden();

    if (this.props.claimState.provinsiInsiden && this.props.claimState.provinsiInsiden != 'Pilih') {
      this.props.getDistrict(this.props.claimState.provinsiInsiden);
    }
  }

  componentWillMount(){
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position);
        // this.setState({
        //   latitude: position.coords.latitude,
        //   longitude: position.coords.longitude,
        //   error: null,
        //   speed: position.coords.speed,
        // });
        temp = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
          speed: position.coords.speed,
        }
        this.props.setLokasiSekarang(temp);
      },
      (error) => {
        temp = {
          error: error.message,
        }
        this.props.setLokasiSekarang(temp);
      }, {
        enableHighAccuracy: true,
        timeout: 200000,
        maximumAge: 1000
      },
    );
  }

  componentWillUnmount() {

  }


  lanjut() {

  }

  onChangeText(text) {
    try {
      ['alamatKejadian','noTeleponTertanggung','emailTertanggung']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onSelectLokasiProvinsi(key, label) {
    this.setState({
      pickedLokasiProvinsi: key,
      labelProvinsiInsiden: label,
      visibleLokasiProvinsi: false,
      labelKotaInsiden: null,
    });

    this.props.getDistrict(key);
    //console.log(this.props.getDistrict(key));
  }

  onShowTipeInsiden() {
    this.setState({ visibleTipeInsiden: true });
  }

  onSelectTipeInsiden(picked, label) {
    // console.log('picked ',picked);
    this.setState({
      pickedTipeInsiden: picked,
      labelTipeInsiden: label,
      visibleTipeInsiden: false,
    })
  }

  onCancelTipeInsiden() {
    this.setState({
      visibleTipeInsiden: false,
    })
  }

  onShowLokasiProvinsi() {
    this.setState({
      visibleLokasiProvinsi: true,
    });
  }

  onCancelLokasiProvinsi() {
    this.setState({
      visibleLokasiProvinsi: false,
    })
  }

  onSelectLokasiKota(key, label) {
    this.setState({
      pickedLokasiKota: key,
      labelKotaInsiden: label,
      visibleLokasiKota: false,
    });

  }

  onShowLokasiKota() {
    this.setState({
      visibleLokasiKota: true,
    })
  }

  onCancelLokasiKota() {
    this.setState({
      visibleLokasiKota: false,
    })
  }

  onSubmit() {
    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error

    const { detailPolis, listHalaman } = this.props;



    ['alamatKejadian','noTeleponTertanggung','emailTertanggung']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Tidak boleh kosong';
          isError = 1;
        }
      });

    this.setState({ errors });

    if (isError == 0 && this.state.pickedJam != '' && this.state.pickedTanggal != '' &&  this.state.labelProvinsiInsiden != 'Pilih Provinsi Insiden' && this.state.labelProvinsiInsiden != 'Pilih' && this.state.labelKotaInsiden != 'Pilih Kota Insiden' && this.state.labelKotaInsiden != null && this.state.labelKotaInsiden != 'Pilih') {
      temp = {
        policy_no: detailPolis.policy_no,
        nama_tertanggung: this.state.namaLengkap,
        nomorPolisi: detailPolis.no_plat,
        email_tertanggung: this.state.emailTertanggung,
        alamat_tertanggung: detailPolis.alamat_tertanggung,
        insurance_id: 'INS005',
        no_ktp: detailPolis.no_ktp,
        no_hp_tertanggung: this.state.noTeleponTertanggung,
        alamatKejadian: this.state.alamatKejadian,
        phone_tertanggung: detailPolis.phone_tertanggung,
        policy_enddate: detailPolis.policy_enddate,
        tanggal_kejadian: this.state.pickedTanggal + ' ' + this.state.pickedJam + ':00',
        tipe_kejadian: this.state.pickedTipeInsiden,
        deskripsiInsiden: this.state.deskripsiInsiden,
        kecepatanInsiden: this.state.kecepatanInsiden,
        labelTipeInsiden: this.state.labelTipeInsiden,
        labelKotaInsiden: this.state.labelKotaInsiden,
        labelProvinsiInsiden: this.state.labelProvinsiInsiden,
        tanggalKejadian: this.state.pickedTanggal,
        jamInsiden: this.state.pickedJam,
        nomorMesin: detailPolis.no_mesin,
        nomorRangka: detailPolis.no_rangka,
        merek_kendaraan: detailPolis.merek_kendaraan,
        seri_kendaraan: detailPolis.seri_kendaraan,
        tahun_kendaraan: detailPolis.tahun_kendaraan,
        nomorPolis: detailPolis.policy_no,
        visibleNavigation: true,
      }
      this.props.setDataClaimMV(temp);

      items = [{
        'current': 2,
        'end': listHalaman[0].end > 2 ? listHalaman[0].end : 2
      }]
      this.props.setHalaman(items);
      this.props.navigate('LaporMVKecelakaanPengemudi');
    } else {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    }
  }

  onSubmitAlamatKejadian() {

  }

  updateJam(time) {
    this.setState({
      pickedJam: time,
      isPickedJam: true,
    });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  updateTanggal(tanggal) {
    this.setState({
      pickedTanggal: tanggal,
      isPicked: true,
    });
  }

  onUbah = () => {
    let errors = {};
    let isError = 0; // cek ada error? 0 = tidak ada; 1 = ada error

    const { detailPolis, listHalaman } = this.props;

    ['alamatKejadian','noTeleponTertanggung','emailTertanggung']
      .forEach((name) => {
        let value = this[name].value();

        if (!value) {
          errors[name] = 'Tidak boleh kosong';
          isError = 1;
        }
      });

    this.setState({ errors });

    if (isError == 0 && this.state.pickedJam != '' && this.state.pickedTanggal != '' &&  this.state.labelProvinsiInsiden != 'Pilih Provinsi Insiden' && this.state.labelProvinsiInsiden != 'Pilih' && this.state.labelKotaInsiden != 'Pilih Kota Insiden' && this.state.labelKotaInsiden != null && this.state.labelKotaInsiden != 'Pilih') {
      temp = {
        policy_no: detailPolis.policy_no,
        nama_tertanggung: this.state.namaLengkap,
        nomorPolisi: detailPolis.no_plat,
        email_tertanggung: this.state.emailTertanggung,
        alamat_tertanggung: detailPolis.alamat_tertanggung,
        insurance_id: 'INS005',
        no_ktp: detailPolis.no_ktp,
        no_hp_tertanggung: this.state.noTeleponTertanggung,
        alamatKejadian: this.state.alamatKejadian,
        phone_tertanggung: detailPolis.phone_tertanggung,
        policy_enddate: detailPolis.policy_enddate,
        tanggal_kejadian: this.state.pickedTanggal + ' ' + this.state.pickedJam + ':00',
        tipe_kejadian: this.state.pickedTipeInsiden,
        deskripsiInsiden: this.state.deskripsiInsiden,
        kecepatanInsiden: this.state.kecepatanInsiden,
        labelTipeInsiden: this.state.labelTipeInsiden,
        labelKotaInsiden: this.state.labelKotaInsiden,
        labelProvinsiInsiden: this.state.labelProvinsiInsiden,
        tanggalKejadian: this.state.pickedTanggal,
        jamInsiden: this.state.pickedJam,
        nomorMesin: detailPolis.no_mesin,
        nomorRangka: detailPolis.no_rangka,
        merek_kendaraan: detailPolis.merek_kendaraan,
        seri_kendaraan: detailPolis.seri_kendaraan,
        tahun_kendaraan: detailPolis.tahun_kendaraan,
        nomorPolis: detailPolis.policy_no,
        visibleNavigation: true,
      }
      this.props.setDataClaimMV(temp);
      
      this.props.navigate('LaporMVKecelakaanKonfirmasi');
    } else {
      Snackbar.show('Mohon lengkapi data', { duration: Snackbar.SHORT });
    }
  }

  view_navigasi = () => {
    return(
      <View style={{height: 53, backgroundColor: '#FFFFFF', justifyContent:'center', paddingLeft:10}}>
        <View style={{flexDirection:'row'}}>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-FF0000.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>1</Text>
              </View>
              </ImageBackground>
            </View>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>2</Text>
              </View>
              </ImageBackground>
            </View>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>3</Text>
              </View>
              </ImageBackground>
            </View>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>4</Text>
              </View>
              </ImageBackground>
            </View>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>5</Text>
              </View>
              </ImageBackground>
            </View>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>6</Text>
              </View>
              </ImageBackground>
            </View>
            <View>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>7</Text>
              </View>
              </ImageBackground>
            </View>
        </View>
      </View>
    );
  }

  view_button = () =>{
    const {dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
      return(
        <View style={[styles.button,{marginTop:10}]}>
  
          <RkButton rkType='danger button1' onPress={()=>{this.onSubmit()}}>Lanjut</RkButton>
  
        </View>
      );
    } else {
      return(
        <View style={[styles.button,{marginTop:10}]}>
  
          <RkButton rkType='danger button1' onPress={()=>{this.onUbah()}}>Ubah</RkButton>
  
        </View>
      )
    }

    // return(
    //   <View style={[styles.button,{marginTop:10}]}>

    //     <RkButton rkType='danger button1' onPress={()=>{this.onSubmit()}}>Lanjut</RkButton>

    //   </View>
    // );
  }

  render() {
    let { errors = {}, ...data } = this.state;
    let { alamatKejadian = null, kecepatanInsiden = null, deskripsiInsiden = null } = data;
    const { dataClaim, detailPolis, listHalaman } = this.props;
    const optionsProvinsi = this.props.listProvinsi;
    const optionsTipeInsiden = this.props.listTipeInsiden;
    const optionsDistrict = this.props.listDistrict;

    // console.log('detail polis', detailPolis);
    // console.log('data polis', dataPolis);
    console.log('data claim umum', dataClaim);

    const {
      visibleTipeInsiden,
      isPickedTanggal,
      isPickedJam,
      pickedTanggal,
      pickedJam,
      pickedTipeInsiden,
      pickedLokasiKota,
      pickedLokasiProvinsi,
      visibleLokasiKota,
      visibleLokasiProvinsi,
    } = this.state;

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var maxDate = new Date(year + 10, 12, 31);
    var minDate = new Date(year - 1, 1, 1);

    // console.log('Detail Polis ', JSON.stringify(detailPolis));

    return (
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.handleBackPress() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'FORM PELAPORAN',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.noPolisRef, this.alamatKejadianRef, this.kecepatanInsidenRef, this.deskripsiInsidenRef];
          }}
        >
        <Navigasi />          
          <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>
            <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                <Text style={{fontSize:18, fontWeight:'bold',color:'#FF0000'}}>INFORMASI UMUM</Text>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-list' size={40} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.noPolisRef}
                  value={detailPolis.policy_no}
                  label='No Polis'
                  keyboardType='default'
                  tintColor='#228B22'
                  editable={false}
                  textColor='rgba(0,0,0, .38)'
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.nama_tertanggungRef}
                  value={detailPolis.nama_tertanggung}
                  label='Nama Tertanggung'
                  keyboardType='default'
                  tintColor='#228B22'
                  editable={false}
                  textColor='rgba(0,0,0, .38)'
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Icon name='ios-car' size={30} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.no_platRef}
                  value={detailPolis.no_plat}
                  label='No Polisi'
                  keyboardType='default'
                  tintColor='#228B22'
                  editable={false}
                  textColor='rgba(0,0,0, .38)'
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Text>K</Text></View>
              <View style={styles.field}>
                <TextField
                  ref={this.noTeleponTertanggungRef}
                  value={this.state.noTeleponTertanggung}
                  label='No HP Tertanggung / WhatsApp *'
                  keyboardType='phone-pad'
                  tintColor='#228B22'
                    onChangeText={this.onChangeText}
                    error={errors.alamatKejadian}
                    onFocus={this.onFocus}
                  onSubmitEditing={this.onSubmitAlamatKejadian}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><Text>K</Text></View>
              <View style={styles.field}>
                <TextField
                  ref={this.emailTertanggungRef}
                  value={this.state.emailTertanggung}
                  label='Email Tertanggung *'
                  keyboardType='email-address'
                  tintColor='#228B22'
                    onChangeText={this.onChangeText}
                    onFocus={this.onFocus}
                  onSubmitEditing={this.onSubmitAlamatKejadian}
                    error={errors.alamatKejadian}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='calendar' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.fieldTanggal}>
                <DatePicker
                  style={{ width: '100%', }}
                  placeholder="Pilih Tanggal Kejadian (DD-MM-YYYY)"
                  mode="date"
                  date={pickedTanggal == null ? null : this.state.pickedTanggal}
                  androidMode="spinner"
                  format="DD-MM-YYYY"
                  minDate={minDate}
                  maxDate={maxDate}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {

                    },
                    dateInput: {
                      borderWidth: 0,
                      alignItems: 'flex-start',
                      paddingTop: 15,
                      borderBottomWidth: .5,
                    },
                    dateText: {
                      fontSize: 18,
                      //color: 'rgba(0,0,0, .38)'
                    }

                    // ... You can check the source to find the other keys.
                  }}
                  showIcon={false}
                  onDateChange={(tanggal) => { this.updateTanggal(tanggal) }}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='clock-o' color='rgba(0,0,0, .38)' size={25} /></View>
              <View style={styles.fieldTanggal}>
                <DatePicker
                  style={{ width: '100%' }}
                  mode="time"
                  date={pickedJam == null ? null : this.state.pickedJam}
                  androidMode="spinner"
                  placeholder="Pilih Jam Kejadian"
                  format="HH:mm"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  ref='datepicker1'
                  customStyles={{
                    dateIcon: {

                    },
                    dateInput: {
                      alignItems: 'flex-start',
                      borderWidth: 0,
                      borderBottomWidth: .5,
                      paddingTop: 15,
                    },
                    dateText: {
                      fontSize: 18,
                      // color: 'rgba(0,0,0, .38)',
                    }
                  }}
                  minuteInterval={10}
                  onDateChange={(datetime) => { this.updateJam(datetime) }}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='address-book' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TextField
                  ref={this.alamatKejadianRef}
                  label='Alamat Kejadian *'
                  keyboardType='default'
                  onFocus={this.onFocus}
                  onSubmitEditing={this.onSubmitAlamatKejadian}
                  onChangeText={this.onChangeText}
                  returnKeyType='next'
                  tintColor='#228B22'
                  error={errors.alamatKejadian}
                  maxLength={80}
                  value={this.state.alamatKejadian}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}><FontAwesome name='map-marker' size={25} color='rgba(0, 0, 0, .38)' /></View>
              <View style={styles.field}>
                <TouchableOpacity onPress={() => this.onShowLokasiProvinsi()}>
                  <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelProvinsiInsiden == null ? 'Lokasi Insiden (Provinsi) *' : this.state.labelProvinsiInsiden}</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                  visible={visibleLokasiProvinsi}
                  onSelect={(key, label) => this.onSelectLokasiProvinsi(key, label)}
                  onCancel={() => this.onCancelLokasiProvinsi()}
                  options={optionsProvinsi}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.icon}>{/*kosong */}</View>
              <View style={styles.field}>
                <TouchableOpacity onPress={() => this.onShowLokasiKota()}>
                  <Text textAlignVertical='center' style={styles.textPicker}>{this.state.labelKotaInsiden == null ? 'Lokasi Insiden (Kota) *' : this.state.labelKotaInsiden}</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                  visible={visibleLokasiKota}
                  onSelect={(key, label) => this.onSelectLokasiKota(key, label)}
                  onCancel={() => this.onCancelLokasiKota()}
                  options={optionsDistrict}
                />
              </View>
            </View>

            <View style={[styles.row,{marginTop: 10}]}>
                  <Text>* Harus diisi</Text>
            </View>
            {this.view_button()}
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    marginTop: 0,
    height: hp('100%') - 80 - 53,
  },
  containerEdit: {
       marginBottom: 20,
         marginTop: 0,
         height: hp('100%') - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  fieldTanggal: {
    flex: 9,
    paddingLeft: 10,
    paddingBottom: 15
  },
  textPicker: {
    height: 56,
    fontSize: 16,
    color: 'rgba(0,0,0, .87)',
    borderBottomWidth: .5,
    borderColor: 'rgba(0,0,0, .38)',
    paddingTop: 20,
  },
  button: {
    alignItems: 'center',
  },
  navigasi: {
    width: (width - 20) / 7,
    height: (width - 20) / 7,
  }
});


function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    listProvinsi: state.listProvinsi,
    listDistrict: state.listDistrict,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listTipeInsiden: state.listTipeInsiden,
    listHalaman : state.listHalaman,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanUmum1);
