/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

import Icon2 from 'react-native-vector-icons/Ionicons';
import { Container, Content, Tab, Tabs, TabHeading, Icon, Button, Footer, Card, CardItem, Left, Center, Right } from 'native-base';
// import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import Navigasi from '../kecelakaan/laporMVKecelakaanNavigasi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

RkTheme.setType('RkButton', 'buttonCard', {
    container: {
        backgroundColor: '#0000FF',
        height:20,
        width:'70%'
    },
    content: {
        color: 'white',
        fontSize: 12,
    }
});

class LaporMVKecelakaanKonfirmasi1 extends Component {
  constructor(props) {
    super(props);

  }

  handleBackPress = () => {
    const {listHalaman, dataClaim} = this.props;

    if(dataClaim.visibleNavigation ==  true){
        items = [{
          'end' : listHalaman[0].end,
            'current': 6,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanFoto');
    } else {
        temp ={
            visibleNavigation : true,
        }
        this.props.setDataClaimMV(true);
        this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }

    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);



  }

  componentWillUnmount() {

  }


  render() {
      const {listDataGambar} = this.props;
        console.log('listDataGambar Foto', listDataGambar);

        const { dataClaim } = this.props;
        console.log('data claim konfirmasi', dataClaim);

      let output = [];
        if (listDataGambar != null) {
        // console.log('images',this.state.images);
        listDataGambar.forEach(function (item) {
            fileuri = item.path;

            output.push(
            <TouchableOpacity
                onPress={() => { this.tambahFotoLain() }}
                style={{ width: width / 6, height: width / 6, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
                <Image
                source={{ uri: fileuri }} style={{ width: (width / 6), height: (width / 6), resizeMode: 'stretch', backgroundColor: '#000' }}
                />
            </TouchableOpacity>
            );
        }.bind(this));
        }
      return(
        <View>
            <Header
                leftComponent={
                    <TouchableOpacity onPress={() => { this.handleBackPress() }}>
                    <Icon2 name='md-arrow-round-back' size={25} color='#FFF' />
                    </TouchableOpacity>
                }
                centerComponent={{
                    text: 'FORM PELAPORAN',
                    style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                }}
                outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                />
            <Navigasi />
                <View style={{height: height - 53 - 24 - 53}}> 
                    <Container>
                        <ScrollView>                                    
                            <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                                <Text style={{fontSize:18, fontWeight:'bold',color:'#FF0000'}}>KONFIRMASI</Text>
                            </View>    
                            {this.view_umum()}
                            {this.view_pengemudi()}
                            {this.view_pihakKetiga()}
                            {this.view_kronologis()}
                            {this.view_bengkel()}
                            {this.view_fotoKerusakan()}
                            <View style={[styles.row,{justifyContent:'center', alignItems:'center', margin:10}]}>
                                <View style={styles.button}>
                                    <RkButton rkType='danger' onPress={()=>{this.onSubmit()}}>Lanjut</RkButton>
                                </View>         
                            </View>
                        </ScrollView>        
                    </Container>
                </View>
            
        </View>
      );
    }

    onSubmit = () => {
        
    }

    editPengemudi = () => {
        temp = {
            visibleNavigation : false,
        }
        this.props.setDataClaimMV(temp);

        
        this.props.navigate('LaporMVKecelakaanPengemudi');
    }

    editUmum = () => {
         temp = {
             visibleNavigation: false,
         }

         this.props.setDataClaimMV(temp);

        
        this.props.navigate('LaporMVKecelakaanUmum');
    }

    editPihakKetiga = () => {
         temp = {
             visibleNavigation: false,
         }

         this.props.setDataClaimMV(temp);

        
        this.props.navigate('LaporMVKecelakaanPihakKetiga');
    }

    editKronologis = () => {
        temp = {
            visibleNavigation : false,
        }

        this.props.setDataClaimMV(temp);

        this.props.navigate('LaporMVKecelakaanKronologis');
    }

    editBengkel = () => {
        temp = {
            visibleNavigation: false,
        }

        this.props.setDataClaimMV(temp);


        this.props.navigate('LaporMVKecelakaanBengkel');
    }

    editFotoKerusakan = () => {
        temp = {
            visibleNavigation: false,
        }

        this.props.setDataClaimMV(temp);


        this.props.navigate('LaporMVKecelakaanFoto');
    }

    view_bengkel = () => {
        const { dataClaim} =this.props;
        // console.log('view_bengkel',listBengkelClaim);
        if(dataClaim.bengkel_id == null){
            return (
                <View style={styles.viewCard}>
                    <View>
                        <View style={styles.viewTitleCard}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text>BENGKEL</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end' }}>
                                <RkButton rkType='buttonCard' onPress={() => { this.editBengkel() }}>Edit</RkButton>
                            </View>
                        </View>
                        <Card style={styles.card}>
                            <CardItem>
                                <View>
                                    <Text>dummy Bengkel A</Text>
                                    <Text>dummy Jalan letjen suprapto</Text>
                                    <Text>dummy Jakarta Pusat</Text>
                                    <Text>dummy 08112345678</Text>

                                </View>
                            </CardItem>
                        </Card>
                    </View>
                </View>
            );
        } else {
            return(
                <View style={styles.viewCard}>
                    <View>
                        <View style={styles.viewTitleCard}>
                            <View style={{flex:1, justifyContent:'center'}}>
                                <Text>BENGKEL</Text>
                            </View>
                            <View style={{flex:1, alignItems:'center', justifyContent:'flex-end'}}>
                                <RkButton rkType='buttonCard' onPress={() => { this.editBengkel() }}>Edit</RkButton>
                            </View>
                        </View>
                        <Card style={styles.card}>
                            <CardItem>
                                <View>
                                    <Text style={styles.title}>Nama Bengkel</Text>
                                    <Text style={styles.content}>{dataClaim.bengkel_id.nama_bengkel}</Text>
                                    <Text style={styles.title}>Alamat Bengkel</Text>
                                    <Text style={styles.content}>{dataClaim.bengkel_id.alamat_bengkel}</Text>
                                    <Text style={styles.title}>Kota Bengkel</Text>
                                    <Text style={styles.content}>{dataClaim.bengkel_id.kota_bengkel}</Text>
                                    <Text style={styles.title}>Telepon Bengkel</Text>
                                    <Text style={styles.content}>{dataClaim.bengkel_id.telp_bengkel == null ? '-' : dataClaim.bengkel_id.telp_bengkel}</Text>
                                    <Text style={styles.title}>Fasilitas Derek</Text>
                                    <Text style={styles.content}>Dummy Ada</Text>
                                </View>
                            </CardItem>
                        </Card>
                    </View>
                </View>
            );
        }

    }

    view_kronologis = () => {
        const {dataClaim} = this.props;
        return (
            <View style={styles.viewCard}>
                <View>
                    <View style={styles.viewTitleCard}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text>Kronologis</Text>
                        </View>
                        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                            <RkButton rkType='buttonCard' onPress={() => { this.editKronologis() }}>Edit</RkButton>
                        </View>
                    </View>
                    <Card style={styles.card}>
                        <CardItem>
                            <View>
                                <Text>{dataClaim.deskripsiInsiden}</Text>
                            </View>
                        </CardItem>
                    </Card>
                </View>
            </View>
        );
    }

    view_fotoKerusakan = () => {
        const {dataClaim} = this.props;
        return (
            <View style={styles.viewCard}>
                <View>
                    <View style={styles.viewTitleCard}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text>Foto Kerusakan</Text>
                        </View>
                        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                            <RkButton rkType='buttonCard' onPress={() => { this.editFotoKerusakan() }}>Edit</RkButton>
                        </View>
                    </View>
                    <Card style={styles.card}>
                        <CardItem>
                            <View>
                                <Text>Foto Depan</Text>
                                <View style={{ width: width * 0.25, height: width * 0.25 }}>
                                    <Image source={{ uri: dataClaim.file_foto_depan }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                                </View>
                                <Text>Foto Belakang</Text>
                                <View style={{ width: width * 0.25, height: width * 0.25 }}>
                                    <Image source={{ uri: dataClaim.file_foto_belakang }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                                </View>
                                <Text>Foto Kiri</Text>
                                <View style={{ width: width * 0.25, height: width * 0.25 }}>
                                    <Image source={{ uri: dataClaim.file_foto_kiri }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                                </View>
                                <Text>Foto Kanan</Text>
                                <View style={{ width: width * 0.25, height: width * 0.25 }}>
                                    <Image source={{ uri: dataClaim.file_foto_kanan }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                                </View>
                                <Text>Foto Lain</Text>
                                {this.view_lampiran_foto()}
                            </View>
                        </CardItem>
                    </Card>
                </View> 
            </View>
        );
    }

    view_pihakKetiga = () => {   
        const{dataClaim} = this.props;     
        return(
            <View style={styles.viewCard}>
                <View style={styles.viewTitleCard}>
                    <View style={{flex:1, justifyContent:'center'}}>
                        <Text>Informasi Pihak Ketiga</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <RkButton rkType='buttonCard' onPress={() => {this.editPihakKetiga() }}>Edit</RkButton>
                    </View>
                </View>

                {dataClaim.status_pihak3 == 'N' ? this.view_pihakKetiga_N() : this.view_pihakKetiga_Y()}

            </View>
        );
    }

    view_pihakKetiga_N = () => {
        const {dataClaim} = this.props;
        
        return (
            <Card style={styles.card}>
                <CardItem>
                    <View>
                        <Text style={styles.title}>Adakah orang lain yang dirugikan?</Text>
                        <Text style={styles.content}>TIDAK</Text>
                    </View>
                </CardItem>                
            </Card>
        )
    }

    view_jenis_pihak_ketiga = () =>{
        const {dataClaim} = this.props;
        let jenis = null;
        if(dataClaim.jenis_pihak3 == 'KB'){
            jenis = "Kenderaan Bermotor";
        } else if(dataClaim.jenis_pihak3 == 'O'){
            jenis = "Orang";
        } else if(dataClaim.jenis_pihak3 == 'PA'){
            jenis = "Properti / Aset";
        } else {
            jenis = 'N/A';
        }

        return(
            <CardItem>
                <View>
                    <Text style={styles.title}>Jenis Pihak Ketiga</Text>
                    <Text style={styles.content}>{jenis}</Text>
                </View>
            </CardItem>
        )
    }

    view_pihakKetiga_Y = () => {
        const {dataClaim} = this.props;
        
        return (
            <Card style={styles.card}>
                <CardItem>
                    <View>
                        <Text style={styles.title}>Adakah orang lain yang dirugikan?</Text>
                        <Text style={styles.content}>Ada</Text>
                    </View>
                </CardItem>
                {this.view_jenis_pihak_ketiga()}
                <CardItem>
                    <View>
                        <Text style={styles.title}>Nomor Handphone Pihak Ketiga</Text>
                        <Text style={styles.content}>{dataClaim.phone_pihak3}</Text>
                    </View>
                </CardItem>

                {this.view_sim_pihak3()}    
                {this.view_stnk_pihak3()}
                {this.view_ktp_pihak3()}                
            </Card>
        )
    }

    view_sim_pihak3 = () => {
        const {dataClaim, listDataGambarSimPihak3} = this.props;
        if (dataClaim.jenis_pihak3 == 'KB') {
            return(
                <CardItem>
                    <View>
                        <Text style={styles.title}>Foto SIM Pihak Ketiga</Text>
                        <View style={{width:width * 0.25, height: width * 0.25}}>
                            <Image source={{ uri: listDataGambarSimPihak3[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                        </View>
                    </View>                       
                </CardItem>
            );
        } else {
            return null;
        }
    }

    view_stnk_pihak3 = () => {
        const {dataClaim, listDataGambarStnkPihak3} = this.props;

        if(dataClaim.jenis_pihak3 == 'KB'){
            return(
                <CardItem>
                    <View>
                        <Text style={styles.title}>Foto STNK Pihak Ketiga</Text>
                        <View style={{width:width * 0.25, height: width * 0.25}}>
                            <Image source={{ uri: listDataGambarStnkPihak3[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                        </View>
                    </View>                       
                </CardItem>
            );  
        } else {
            return null;  
        }

    }

    view_ktp_pihak3 = () => {
        const {dataClaim, listDataGambarKtpPihak3} = this.props;

        if(dataClaim.jenis_pihak3 == 'O' || dataClaim.jenis_pihak3 == 'PA'){
            return(
                <CardItem>
                    <View>
                        <Text style={styles.title}>Foto KTP Pihak Ketiga</Text>
                        <View style={{width:width * 0.25, height: width * 0.25}}>
                            <Image source={{ uri: listDataGambarKtpPihak3[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                        </View>
                    </View>                       
                </CardItem>
            );
        } else {
            return null;
        }
    }

    view_lampiran_foto = () => {
        const { listDataGambar } = this.props;
        console.log('konfirmasi', listDataGambar);

        let output = [];
        if (listDataGambar != null) {
            // console.log('images',this.state.images);
            listDataGambar.forEach(function (item) {
                fileuri = item.path;

                output.push(
                    <View
                        style={{ width: width / 4, height: width / 4, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
                        <Image
                            source={{ uri: fileuri }} style={{ width: (width / 4), height: (width / 4), resizeMode: 'contain', backgroundColor: '#000' }}
                        />
                    </View>
                );
            }.bind(this));
        }

        return(
            <View style={{flexDirection:'row', width:'100%', flexWrap:'wrap'}}>
                {output}
            </View>
        );
    }

    view_umum = () => {
        const {dataClaim} = this.props;
        return(
            <View style={styles.viewCard}>
                <View style={styles.viewTitleCard}>
                    <View style={{flex:1, justifyContent:'center'}}>
                        <Text>INFORMASI UMUM</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <RkButton rkType='buttonCard' onPress={() => { this.editUmum() }}>Edit</RkButton>
                    </View>
                </View>
                <Card style={styles.card}>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>No Polis</Text>
                            <Text style={styles.content}>{dataClaim.nomorPolis}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Nama Tertanggung</Text>
                            <Text style={styles.content}>{dataClaim.nama_tertanggung}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>No Polisi</Text>
                            <Text style={styles.content}>{dataClaim.nomorPolisi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>No Handphone Tertanggung</Text>
                            <Text style={styles.content}>{dataClaim.no_hp_tertanggung}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Email Tertanggung</Text>
                            <Text style={styles.content}>{dataClaim.email_tertanggung}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Tanggal Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.tanggalKejadian}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Jam Insiden</Text>
                            <Text style={styles.content}>{dataClaim.jamInsiden}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Alamat Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.alamatKejadian}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Provinsi Tempat Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.labelProvinsiInsiden}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Kota Tempat Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.labelKotaInsiden}</Text>
                        </View>
                    </CardItem>
                </Card>
            </View>
        );
    }

    view_pengemudi = () => {
        const {dataClaim, listDataGambarSim} = this.props;
        return(
            <View style={styles.viewCard}>
                <View style={styles.viewTitleCard}>
                    <View style={{flex:1, justifyContent:'center'}}>
                        <Text>Informasi Pengemudi</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                        <RkButton rkType='buttonCard' onPress={() => { this.editPengemudi() }}>Edit</RkButton>
                    </View>
                </View>
                <Card style={styles.card}>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Nama Pengemudi</Text>
                            <Text style={styles.content}>{dataClaim.nama_pengemudi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Alamat Pengemudi</Text>
                            <Text style={styles.content}>{dataClaim.alamat_pengemudi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>SIM Pengemudi</Text>
                            <Text style={styles.content}>{dataClaim.labelHurufSIM} {dataClaim.no_sim_pengemudi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Foto SIM</Text>
                            <View style={{width:width * 0.25, height: width * 0.25}}>
                                <Image source={{ uri: listDataGambarSim[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                            </View>
                        </View>                       
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Kecepatan kendaraan sebelum terjadi kecelakaan (KM/jam)</Text>
                            <Text style={styles.content}>{dataClaim.kecepatanInsiden} KM/jam</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Apakah Pengemudi cedera?</Text>
                            <Text style={styles.content}>YA</Text>
                        </View>
                    </CardItem>

                    {this.view_pengemudi_bekerja()}

                </Card>
            </View>
        );
    }

    view_pengemudi_bekerja = () => {
        const {dataClaim} = this.props;
        if(dataClaim.pengemudiChecked == false){
            return (
                 <CardItem>
                    <View>
                        <Text style={styles.title}>Apakah pengemudi bekerja pada tertanggung?</Text>
                        <Text style={styles.content}>{dataClaim.pengemudi_bekerja}</Text>
                    </View>
                </CardItem>

            );
        } else {
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        dataClaim : state.dataClaim,
        listHalaman: state.listHalaman,
        listDataGambarSim: state.listDataGambarSim,
        listDataGambarSimPihak3: state.listDataGambarSimPihak3,
        listDataGambarStnkPihak3: state.listDataGambarStnkPihak3,
        listDataGambarKtpPihak3: state.listDataGambarKtpPihak3,
        listBengkelClaim: state.listBengkelClaim,
        listDataGambar: state.listDataGambar
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        height: height - 80 - 53,
        width:width,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    }, 
    card: {
        marginRight: 10,
        marginLeft: 10,
    },
    content: {
        fontWeight:'bold',
        paddingLeft: 10,
    },
    col: {
        flexDirection: 'column',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    title: {
        
    },
    viewCard:{
        marginTop: 10,
    },
    viewTitleCard: {
        paddingLeft: 10,
        flexDirection: 'row'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanKonfirmasi1);
