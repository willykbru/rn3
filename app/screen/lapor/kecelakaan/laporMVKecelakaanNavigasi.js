import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, ImageBackground, Alert, KeyboardAvoidingView } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

class LaporMVKecelakaanNavigasi1 extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const {listHalaman, dataClaim} = this.props;
        // console.log('render navigasi',dataClaim);
        // console.log('Navigasi', listHalaman);
        
        if(dataClaim.visibleNavigation == true){
            return(
                <View style={{height: 53, backgroundColor: '#FFFFFF', justifyContent:'center', paddingLeft:10}}>
                    <View style={{flexDirection:'row'}}>
                        {this.view_1()}
                        {this.view_2()}
                        {this.view_3()}
                        {this.view_4()}
                        {this.view_5()}
                        {this.view_6()}
                        {this.view_7()}                    
                    </View>
                </View>
            );
        } else {
            return <View></View>;
        }

    }

    view_1 = () => {
        const {listHalaman} = this.props;
        // console.log('view_1',listHalaman);
        if(listHalaman[0].current == 1){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>1</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else if (listHalaman[0].current > 1){
            return (
                <TouchableOpacity onPress={() => {this.on1()}}>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-2AD268.png')
                    } style={styles.navigasi} >
                    <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                        <Text>1</Text>
                    </View>
                    </ImageBackground>
                </TouchableOpacity>
            )
        }
    }

    view_2 = () => {
        const {listHalaman} = this.props;
        // console.log('view_2', listHalaman);
        if(listHalaman[0].current == 2){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>2</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else {
            if(listHalaman[0].end >= 2){
                return (
                    <TouchableOpacity onPress={() => {this.on2()}}>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-2AD268.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>2</Text>
                        </View>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            } else if(listHalaman[0].end < 2){
                return (
                    <View>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-CCCCCC.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>2</Text>
                        </View>
                        </ImageBackground>
                    </View>
                )
            }
        }
    }

    view_3 = () => {
        const {listHalaman} = this.props;
        // console.log('view_3', listHalaman);
        if(listHalaman[0].current == 3){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>3</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else {
            if(listHalaman[0].end >= 3){
                return (
                    <TouchableOpacity onPress={() => {this.on3()}}>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-2AD268.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>3</Text>
                        </View>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            } else if(listHalaman[0].end < 3){
                return (
                    <View>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-CCCCCC.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>3</Text>
                        </View>
                        </ImageBackground>
                    </View>
                )
            }
        }
    }

    view_4 = () => {
        let idx = 4;
        const {listHalaman} = this.props;
        // console.log('view_4', listHalaman);
        if(listHalaman[0].current == idx){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else {
            if(listHalaman[0].end >= idx){
                return (
                    <TouchableOpacity onPress={() => {this.on4()}}>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-2AD268.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            } else if(listHalaman[0].end < idx){
                return (
                    <View>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-CCCCCC.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </View>
                )
            }
        }
    }

    view_5 = () => {
        let idx = 5;
        const {listHalaman} = this.props;
        // console.log('view_4', listHalaman);
        if(listHalaman[0].current == idx){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else {
            if(listHalaman[0].end >= idx){
                return (
                    <TouchableOpacity onPress={() => {this.on5()}}>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-2AD268.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            } else if(listHalaman[0].end < idx){
                return (
                    <View>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-CCCCCC.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </View>
                )
            }
        }
    }

    view_6 = () => {
        let idx = 6;
        const {listHalaman} = this.props;
        // console.log('view_6', listHalaman);
        if(listHalaman[0].current == idx){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else {
            if(listHalaman[0].end >= idx){
                return (
                    <TouchableOpacity onPress={() => {this.on6()}}>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-2AD268.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            } else if(listHalaman[0].end < idx){
                return (
                    <View>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-CCCCCC.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </View>
                )
            }
        }
    }

    view_7 = () => {
        let idx = 7;
        const {listHalaman} = this.props;
        // console.log('view_7', listHalaman);
        if(listHalaman[0].current == idx){
            return (
                <View>
                    <ImageBackground source = {
                        require('../../../assets/icons/component/circular-shape-FF0000.png')
                    } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                    </ImageBackground>
                </View>
            );
        } else {
            if(listHalaman[0].end >= idx){
                return (
                    <TouchableOpacity onPress={() => {this.on7()}}>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-2AD268.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </TouchableOpacity>
                )
            } else if(listHalaman[0].end < idx){
                return (
                    <View>
                        <ImageBackground source = {
                            require('../../../assets/icons/component/circular-shape-CCCCCC.png')
                        } style={styles.navigasi} >
                        <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                            <Text>{idx}</Text>
                        </View>
                        </ImageBackground>
                    </View>
                )
            }
        }
    }

    on1 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 1,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanUmum');
    }

    on2 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 2,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanPengemudi');
    }

    on3 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 3,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanPihakKetiga');
    }

    on4 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 4,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanKronologis');
    }

    on5 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 5,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanBengkel');
    }

    on6 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 6,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanFoto');
    }

    on7 = () => {
        const {listHalaman} = this.props;
        items = [{
            'end': listHalaman[0].end,
            'current': 7,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }
}

const styles = StyleSheet.create({
  navigasi: {
    width: (width - 20) / 7,
    height: (width - 20) / 7,
  }
});

function mapStateToProps(state) {
  return {
    listHalaman : state.listHalaman,
    dataClaim : state.dataClaim,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanNavigasi1);