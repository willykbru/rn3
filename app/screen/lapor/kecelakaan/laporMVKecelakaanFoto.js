/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import ImageResizer from 'react-native-image-resizer';

// import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import Navigasi from '../kecelakaan/laporMVKecelakaanNavigasi';
import DialogAndroid from 'react-native-dialogs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');


class LaporMVKecelakaanFoto1 extends Component {
  constructor(props) {
    super(props);

  }

  handleBackPress = () => {
    const {listHalaman, dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
        items = [{
          'end' : listHalaman[0].end,
          'current': 5,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanBengkel');
    } else {
        temp = {
            visibleNavigation : true,
        }
        this.props.setDataClaimMV(temp);
        this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }

    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);



  }

  componentWillUnmount() {

  }


    render() {
        const { dataClaim, listLokasiSekarang } = this.props;
        console.log('data claim foto', dataClaim);
        console.log('data claim foto', listLokasiSekarang);

        const {listDataGambar} = this.props;
        console.log('listDataGambar Foto', listDataGambar);
        let output = [];
        if (listDataGambar != null) {
        // console.log('images',this.state.images);
        listDataGambar.forEach(function (item) {
            fileuri = item.path;

            output.push(
            <TouchableOpacity
                onPress={() => { this.konfirmasi(item.path) }}
                style={{ width: width / 6, height: width / 6, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
                <Image
                source={{ uri: fileuri }} style={{ width: (width / 6), height: (width / 6), resizeMode: 'stretch', backgroundColor: '#000' }}
                />
            </TouchableOpacity>
            );
        }.bind(this));
        }
      return(
        <View>
            <Header
                leftComponent={
                    <TouchableOpacity onPress={() => { this.handleBackPress() }}>
                    <Icon name='md-arrow-round-back' size={25} color='#FFF' />
                    </TouchableOpacity>
                }
                centerComponent={{
                    text: 'FORM PELAPORAN',
                    style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                }}
                outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                />
            <Navigasi />
            <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>  
                <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                    <Text style={{fontSize:18, fontWeight:'bold',color:'#FF0000'}}>FOTO KERUSAKAN KENDARAAN</Text>
                </View>
                {this.view_ambilDepan()}
                <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                    {this.view_ambilKiri()}                   
                    <View>   
                        <View style={{flexDirection:'row',justifyContent:'center', alignItems:'center'}}>
                            <Image source={require('../../../assets/images/mobil-panjang-01.png')} style={{ width: width/3, height: height/3, resizeMode: 'contain' }} />
                        </View>                         
                    </View> 
                    {this.view_ambilKanan()}             
                </View>
                {this.view_ambilBelakang()}
                <View>
                    <Text style={{fontWeight:'bold'}}>Foto Tambahan Lain</Text>   
                    <View style={{flexWrap:'wrap', flexDirection:'row'}}>
                        {output}   
                        {this.view_buttonTambah()}                         
                    </View>     
                </View>    
                <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                    <View style={[styles.button, {marginLeft:10, marginTop:10, marginBottom:10}]}>
                        <RkButton rkType='danger' onPress={()=>{this.onSubmit()}}>Lanjut</RkButton>
                    </View>         
                </View>   
            </ScrollView>
        </View>
      );
    }

    onSubmit = () => {
        const {listHalaman, listDataMobilDepan, listDataMobilKiri, listDataMobilKanan, listDataMobilBelakang} = this.props;
        
        temp = {
            file_foto_depan : listDataMobilDepan[0].path,
            file_foto_belakang : listDataMobilBelakang[0].path,
            file_foto_kiri : listDataMobilKiri[0].path,
            file_foto_kanan : listDataMobilKanan[0].path,

        };

        this.props.setDataClaimMV(temp);

        items = [{
            'end': listHalaman[0].end > 7 ? listHalaman[0].end : 7,
            'current': 7,
        }];
        this.props.setHalaman(items);
        this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }

    tambahFotoBelakang = () => {

        ImagePicker.openCamera({
            cropping: false,
            includeExif: false,
            avoidEmptySpaceAroundImage: true,
        }).then(image => {
        //  console.log('received image', image.path);
        ImageResizer.createResizedImage(image.path, image.width * 0.4, image.width * 0.4, 'JPEG', 80, 0).then((response) => {

            image = {
            'size': response.size,
            'path': response.uri,
            'caption': '',
            };

            this.props.setDataGambarMobilBelakangTemp(image);

        });

        }).catch(e => alert(e));
    }

    tambahFotoDepan = () => {
 
        ImagePicker.openCamera({
            cropping: false,
            includeExif: false,
            avoidEmptySpaceAroundImage: true,
        }).then(image => {
        //  console.log('received image', image.path);
        ImageResizer.createResizedImage(image.path, image.width * 0.4, image.width * 0.4, 'JPEG', 80, 0).then((response) => {

            image = {
            'size': response.size,
            'path': response.uri,
            'caption': '',
            };

            this.props.setDataGambarMobilDepanTemp(image);

        });

        }).catch(e => alert(e));
    }
    
    tambahFotoKanan = () => {

        ImagePicker.openCamera({
            cropping: false,
            includeExif: false,
            avoidEmptySpaceAroundImage: true,
        }).then(image => {
            //  console.log('received image', image.path);
            ImageResizer.createResizedImage(image.path, image.width * 0.4, image.width * 0.4, 'JPEG', 80, 0).then((response) => {

                image = {
                    'size': response.size,
                    'path': response.uri,
                    'caption': '',
                };

                this.props.setDataGambarMobilKananTemp(image);

            });

        }).catch(e => alert(e));
    }

    tambahFotoKiri = () => {
        ImagePicker.openCamera({
            cropping: false,
            includeExif: false,
            avoidEmptySpaceAroundImage: true,
        }).then(image => {
            //  console.log('received image', image.path);
            ImageResizer.createResizedImage(image.path, image.width * 0.4, image.width * 0.4, 'JPEG', 80, 0).then((response) => {

                image = {
                    'size': response.size,
                    'path': response.uri,
                    'caption': '',
                };

                this.props.setDataGambarMobilKiriTemp(image);

            });

        }).catch(e => alert(e));
    }

    lihatgambar = (path) => {
        // Alert.alert(path);
        const { listDataGambar } = this.props;

        //JSON.parse(jsondata).filter(({website}) => website === 'yahoo');

        const temp = listDataGambar.filter((item) => item.path === path);
        // console.log('temp',temp[0]);
        let image = {
            'path': temp[0].path,
            'size': temp[0].size,
            'caption': temp[0].caption,
        }
        // console.log('image', image);
        this.props.setDataGambarKameraTemp(image);
        this.props.navigate('LaporMVKecelakaanLampiranGambar');
        // console.log('path', temp);
        // console.log('lihat gambar',listDataGambar);
    }

    konfirmasi = async (path) => {
        const {listDataGambar} =  this.props;
        const { selectedItem } = await DialogAndroid.showPicker(null, null, {
        items: [
            { label: 'Lihat Gambar', id: 'lihatGambar' },
            { label: 'Hapus gambar', id: 'hapusGambar' }
        ], positiveText: null,
        });
        if (selectedItem) {
            if (selectedItem.id == 'lihatGambar') {
                // when negative button is clicked, selectedItem is not present, so it doesn't get here
                this.lihatgambar(path);
            } else if (selectedItem.id == 'hapusGambar') {
                // console.log(this.state.images);
                let i = listDataGambar.length;
                // Alert.alert(i.toString());
                let data = [];
                data.push(path);
                // console.log(data);

                this.props.hapusDataGambarKamera(data);
                // while (i--) {
                //     if(data.indexOf(listDataGambar[i].path)!=-1){
                //         listDataGambar.splice(i,1);
                //         // console.log('state images: ',this.state.images);
                //         this.forceUpdate();
                //     }
                // }
                this.forceUpdate();

            } else {
                // kosong
            }
        }
    }
    
    tambahFotoLain = () => {
        const { listDataGambar } = this.props;

        image = [];
        ImagePicker.openCamera({
            cropping: false,
            includeExif: false,
            avoidEmptySpaceAroundImage: true,
        }).then(image => {
            // console.log('received image 6', image);
            ImageResizer.createResizedImage(image.path, image.width * 0.4, image.height * 0.4, 'JPEG', 100, 0).then((response) => {

                image = {
                'size': response.size,
                'path': response.uri,
                'caption': '',
                };

                this.props.setDataGambarKameraTemp(image);
                this.props.navigate('LaporMVKecelakaanLampiranGambar');

            });
        }).catch(e => alert(e));
      // images.push(image);
      // this.setState({images : images});
    }

    view_ambilDepan = () => {
        const {listDataMobilDepan} = this.props;
        if (listDataMobilDepan[0].path == null){
            return (
                <View style={[styles.row,{justifyContent:'center', alignItems:'center' }]}>
                    <View style={{flexDirection: 'column'}}>                        
                        <View style={{justifyContent:'center', alignItems:'center'}}>   
                            <TouchableOpacity onPress={() => { this.tambahFotoDepan() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: 'transparent' }}>
                                <Image source={require('../../../assets/icons/component/plus.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                            </TouchableOpacity>                             
                        </View>      
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontWeight:'bold'}}>Depan</Text>
                        </View>             
                    </View>
                </View>
            );            
        } else {
            return (
                <View style={[styles.row,{justifyContent:'center', alignItems:'center' }]}>
                    <View style={{flexDirection: 'column'}}>
                        <View style={{justifyContent:'center', alignItems:'center'}}>   
                            <TouchableOpacity onPress={() => { this.tambahFotoDepan() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: '#000000' }}>
                                <Image source={{uri: listDataMobilDepan[0].path}} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                            </TouchableOpacity>                             
                        </View>      
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontWeight:'bold'}}>Depan</Text>
                        </View>             
                    </View>
                </View>
            );
        }
    }

    view_ambilBelakang = () => {
        const {listDataMobilBelakang} = this.props;
        if (listDataMobilBelakang[0].path == null) {
            return (
                <View style={[styles.row,{justifyContent:'center', alignItems:'center' }]}>
                    <View style={{flexDirection: 'column'}}>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontWeight:'bold'}}>Belakang</Text>
                        </View>
                        <View style={{justifyContent:'center', alignItems:'center'}}>   
                            <TouchableOpacity onPress={() => { this.tambahFotoBelakang() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: 'transparent' }}>
                                <Image source={require('../../../assets/icons/component/plus.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                            </TouchableOpacity>                             
                        </View>                   
                    </View>
                </View>
            );            
        } else {
            return (
                <View style={[styles.row,{justifyContent:'center', alignItems:'center' }]}>
                    <View style={{flexDirection: 'column'}}>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontWeight:'bold'}}>Belakang</Text>
                        </View>
                        <View style={{justifyContent:'center', alignItems:'center'}}>   
                            <TouchableOpacity onPress={() => { this.tambahFotoBelakang() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: '#000000' }}>
                                <Image source={{uri: listDataMobilBelakang[0].path}} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                            </TouchableOpacity>                             
                        </View>                   
                    </View>
                </View>
            );
        }
    }

    view_ambilKiri = () => {
        const {listDataMobilKiri} = this.props;
        if (listDataMobilKiri[0].path == null) {
            return (
                <View>
                    <View style={{flexDirection:'row'}}> 
                        <TouchableOpacity onPress={() => { this.tambahFotoKiri() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: 'transparent' }}>
                            <Image source={require('../../../assets/icons/component/plus.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                        </TouchableOpacity>                 
                    </View>    
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontWeight:'bold'}}>Kiri</Text>
                    </View>
                </View>   
            );            
        } else {
            return (
                <View>
                    <View style={{flexDirection:'row'}}> 
                        <TouchableOpacity onPress={() => { this.tambahFotoKiri() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: '#000000' }}>
                            <Image source={{uri: listDataMobilKiri[0].path}} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                        </TouchableOpacity>                 
                    </View>    
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontWeight:'bold'}}>Kiri</Text>
                    </View>
                </View>   
            );
        }
    }

    view_ambilKanan = () => {
        const {listDataMobilKanan} = this.props;
        if (listDataMobilKanan[0].path == null) {
            return (
                <View>
                    <View style={{flexDirection:'row'}}> 
                        <TouchableOpacity onPress={() => { this.tambahFotoKanan() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: 'transparent' }}>
                            <Image source={require('../../../assets/icons/component/plus.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                        </TouchableOpacity>                 
                    </View>    
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontWeight:'bold'}}>Kanan</Text>
                    </View>
                </View>   
            );            
        } else {
            return (
                <View>
                    <View style={{flexDirection:'row'}}> 
                        <TouchableOpacity onPress={() => { this.tambahFotoKanan() }} style={{ width: width / 5, height: width / 5, margin: 10, backgroundColor: '#000000' }}>
                            <Image source={{uri: listDataMobilKanan[0].path}} style={{ width: width / 5, height: width / 5, resizeMode: 'contain' }} />
                        </TouchableOpacity>                 
                    </View>     
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontWeight:'bold'}}>Kanan</Text>
                    </View>
                </View>   
            );
        }
    }

    view_buttonTambah = () => {
        const { listDataGambar } = this.props;

        if (listDataGambar.length < 4) {
            return(
                <TouchableOpacity onPress={() => { this.tambahFotoLain() }} style={{ width: width / 6, height: width / 6, margin: 10, backgroundColor: 'transparent' }}>                                    
                    <Image source={require('../../../assets/icons/component/plus_BCE0FD.png')} style={{ width: width / 6, height: width / 6, resizeMode: 'contain' }} />
                </TouchableOpacity> 
            );
        } else {
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        dataClaim : state.dataClaim,
        listDataMobilKanan: state.listDataMobilKanan,
        listDataMobilKiri: state.listDataMobilKiri,
        listDataMobilDepan: state.listDataMobilDepan,
        listDataMobilBelakang : state.listDataMobilBelakang,
        listDataGambar : state.listDataGambar,
        listHalaman : state.listHalaman,
        listLokasiSekarang: state.listLokasiSekarang,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        height: height - 80 - 53,
        width:width,
    },
    containerEdit:{
        marginTop: 0,
        height: height - 80,
        width: width,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },

});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanFoto1);
