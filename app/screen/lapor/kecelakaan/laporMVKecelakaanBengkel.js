/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput,  Animated, ActivityIndicator } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';
import {Card} from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import Navigasi from '../kecelakaan/laporMVKecelakaanNavigasi';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { showLocation } from 'react-native-map-link'
import MapView, { Marker } from 'react-native-maps';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Container, Content, Picker, Form, Item, Label, Input, Separator } from "native-base";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

// const data = [{
//   'nama' : 'bengkel A',
//   'latitude': -6.162157,
//   'longitude': 106.875617
// }, {
//   'nama' : 'bengkel B',
//   'latitude': -6.163340,
//   'longitude': 106.872571
// }];

RkTheme.setType('RkButton', 'buttonBengkel', {
  container: {
    backgroundColor: '#FF0000',
  },
  content: {
    color: 'white',
    fontSize: 12,
  }
});

class LaporMVKecelakaanBengkel1 extends Component {
  constructor(props) {
    super(props);
    const {dataClaim, listLokasiSekarang} = this.props;
    this.state = {
      latitude: listLokasiSekarang.latitude,
      longitude: listLokasiSekarang.longitude,
      error: listLokasiSekarang.error,
      speed : listLokasiSekarang.speed,
      // markers : [{
      //   'title': 'Bengkel A',
      //   'alamat': 'Jalan Cempaka Mas',
      //   'coordinates': {
      //     'latitude': -6.144185,
      //     'longitude': 106.865104
      //   },
      //   'fasilitas_derek': 'ada',
      //   'telepon' : '081212345671',
      //   'bengkel_id' : 1,
      // }, {
      //   'title': 'Bengkel B',
      //   'alamat': 'Jalan Sumur Batu',
      //   'coordinates': {
      //     'latitude': -6.163340,
      //     'longitude': 106.8725719
      //   },
      //   'fasilitas_derek': 'tidak ada',
      //   'telepon': '081212345672',
      //   'bengkel_id': 2,
      // }, {
      //   'title': 'Bengkel C',
      //   'alamat': 'Jalan Letjen Suprapto',
      //   'coordinates': {
      //     'latitude': -6.164340,
      //     'longitude': 106.8725719
      //   },
      //   'fasilitas_derek': 'ada',
      //   'telepon' : '081212345673',
      //   'bengkel_id': 3,
      // }],
      selected_bengkel: (dataClaim.bengkel_id == null || dataClaim.bengkel_id == "" )? null : dataClaim.bengkel_id,
      boundingBox: {
        westLng: 0,
        southLat: 0,
        eastLng: 0,
        northLat: 0
      },
      region: {
        latitude: null,
        longitude: null,
        latitudeDelta: null,
        longitudeDelta: null
      },
      kata_kunci : null,
      bengkel : null,
    };
    

  }

  methodPlusMinus = () => {
    var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
    return plusOrMinus;
  }

  generateLatitude = () => {
    temp = (Math.random() * 5) * this.methodPlusMinus();
    // console.log('temp', temp);
    return temp;
  }

  generateLongitude = () => {
    temp = (Math.random() * 128) * this.methodPlusMinus();
    return temp;
  }

  generateData = (idx) => {
    for (i = 0; i < idx; i++) {
      console.log('longitude : ', this.generateLongitude(), 'latitude : ', this.generateLongitude());
    }
  }

  handleBackPress = () => {
    const {listHalaman, dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
      items = [{
        'end' : listHalaman[0].end,
        'current': 4,
      }];
      this.props.setHalaman(items);
      this.props.navigate('LaporMVKecelakaanKronologis');
    } else {
      temp = {
        visibleNavigation: true,
      }
      this.props.setDataClaimMV(temp);
      this.props.navigate('LaporMVKecelakaanKonfirmasi')
    }

    return true;
  }

  onLihatPetunjukArah = (selected_bengkel) => {
    console.log('bengkel : ',selected_bengkel);
    /*
showLocation({
  latitude: 38.8976763,
  longitude: -77.0387185,
  sourceLatitude: -8.0870631, // optionally specify starting location for directions
  sourceLongitude: -34.8941619, // not optional if sourceLatitude is specified
  title: 'The White House', // optional
  googleForceLatLon: false, // optionally force GoogleMaps to use the latlon for the query instead of the title
  googlePlaceId: 'ChIJGVtI4by3t4kRr51d_Qm_x58', // optionally specify the google-place-id
  dialogTitle: 'This is the dialog Title', // optional (default: 'Open in Maps')
  dialogMessage: 'This is the amazing dialog Message', // optional (default: 'What app would you like to use?')
  cancelText: 'This is the cancel button text', // optional (default: 'Cancel')
  appsWhiteList: ['google-maps'] // optionally you can set which apps to show (default: will show all supported apps installed on device)
  // app: 'uber'  // optionally specify specific app to use
})
    */
   showLocation({
     latitude: selected_bengkel.coordinates.latitude,
     longitude: selected_bengkel.coordinates.longitude,
     sourceLatitude: this.state.latitude,
     sourceLongitude: this.state.longitude,
     title: selected_bengkel.title,
     appsWhiteList: ['google-maps']
   })
  }

  onCari = () => {
    const {kata_kunci} = this.state;
    const {listAllBengkelClaim} = this.props;

    if(kata_kunci == null || kata_kunci == ''){

    } else {
      this.setState({selected_bengkel : null});
      // console.log('bengkel kata kunci',kata_kunci);

      // listAllBengkelClaim.bengkel.filter(function (i, n) {
      //   return n.website === 'google';
      // });
      data = listAllBengkelClaim.bengkel.filter((item) => {
        return (item.nama_bengkel.match(kata_kunci) || item.kota_bengkel.match(kata_kunci));
      });

      this.setState({bengkel : data});
      console.log(data);
    }
  }

  onUbah = (selected_bengkel) => {

    // if(kronologis.length < 20){
    //   Snackbar.show('Kronologis harus memiliki minimal 20 karakter', { duration: Snackbar.SHORT });
    // } else{
    //   // console.log(kronologis);
    //   temp = { 
    //     deskripsiInsiden : kronologis
    //   }

    //   items = [{
    //     'end': listHalaman[0].end > 5 ? listHalaman[0].end : 5,
    //     'current': 5,
    //   }];
    //   this.props.setHalaman(items);
    //   this.props.setDataClaimMV(temp);
    //   this.props.navigate('LaporMVKecelakaanBengkel');
    // }

    temp = {
      bengkel_id : selected_bengkel,
      visibleNavigation: true,
    }

    this.props.setDataClaimMV(temp);
    this.props.navigate('LaporMVKecelakaanKonfirmasi');
  }

  onSubmit = () => {
    const {listHalaman} = this.props;
    const{selected_bengkel} = this.state;
    // console.log('selected_bengkel',selected_bengkel);


    temp = {
      bengkel_id : selected_bengkel,
    };

    this.props.setDataClaimMV(temp);


    items = [{
      'end': listHalaman[0].end > 6 ? listHalaman[0].end : 6,
      'current': 6,
    }];
    this.props.setHalaman(items);
    this.props.navigate('LaporMVKecelakaanFoto');
  }

  componentWillMount(){
    navigator.geolocation.getCurrentPosition(
        (position) => {
            console.log(position);
            // this.setState({
            //     latitude: position.coords.latitude,
            //     longitude: position.coords.longitude,
            //     error: null,
            //     speed: position.coords.speed,
            // });

            temp = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
              speed: position.coords.speed,
            }

          this.props.setLokasiSekarang(temp);
        },
        (error) => {console.log('lapor mv kecelakaan bengkel',error)},
        { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
    );

    this.props.getAllBengkelClaim();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

     navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
                // this.setState({
                //     latitude: position.coords.latitude,
                //     longitude: position.coords.longitude,
                //     error: null,
                //     speed: position.coords.speed,
                // });

                temp ={
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                    speed: position.coords.speed,
                }

                this.props.setLokasiSekarang(temp);
            },
            (error) => {console.log('lapor mv kecelakaan bengkel',error)},
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
        );

        // this.props.getAllBengkelClaim();

  }

  componentDidUpdate(){
    // console.log(this.state);
  }

  componentWillUnmount() {
    this.setState({
      latitude: null,
      longitude: null,
      error: null,
      speed: null,
      markers: null,
      selected_bengkel: null,
      boundingBox: null,
      region: null
    });

    this.props.resetLokasiSekarang();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.latitude != nextState.latitude || this.state.longitude != nextState.longitude || this.state.error != nextState.error) {
      console.log('jalan');
      this.setState({
        latitude: nextState.latitude,
        longitude: nextState.longitude,
        error: nextState.error,
      });
      temp = {
        latitude: nextState.latitude,
        longitude: nextState.longitude,
        error: nextState.error,
        speed: null
      }

      this.props.setLokasiSekarang(temp);
      return true;
    }

    if(this.state.selected_bengkel != nextState.selected_bengkel){
      this.setState({
        selected_bengkel : nextState.selected_bengkel,
      })
      return true;
    }

    if(this.state.region != nextState.region){
      return true;
    }

    if(this.state.boundingBox != nextState.boundingBox){
      return true;
    }

    if(this.state.bengkel != nextState.bengkel){
      return true;
    }

    if(this.props.listLokasiSekarang != nextProps.listLokasiSekarang){
      this.setState({
        latitude: nextProps.latitude,
        longitude: nextProps.longitude,
        error: nextProps.error,
        speed: nextProps.speed,
      })
      return true;
    }

    return false;
  }

  getCurrentPosition= ()=> {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position);
        // this.setState({
        //     latitude: position.coords.latitude,
        //     longitude: position.coords.longitude,
        //     error: null,
        //     speed: position.coords.speed,
        // });

        temp = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
          speed: position.coords.speed,
        }

        this.props.setLokasiSekarang(temp);
      },
      (error) => { console.log('lapor mv kecelakaan bengkel', error) },
      { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
    );
  }

  segarkan = () =>{
    this.setState({
      latitude: null,
      longitude: null,
      selected_bengkel: null,
    });

    this.getCurrentPosition();


    this.render();
  }

  render() {
    const { dataClaim, listLokasiSekarang, listAllBengkelClaim } = this.props;
    console.log('data claim bengkel', dataClaim);
    // console.log('list lokasi', listLokasiSekarang);
    console.log('listAllBengkelClaim bengkel', listAllBengkelClaim);
    // this.generateData(50);
    return (
    <Container>
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.handleBackPress() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'FORM PELAPORAN',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          rightComponent={
            <TouchableOpacity onPress={() => { this.segarkan() }}>
              <Icon name='ios-refresh' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <Navigasi />
      
        <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>   
          <KeyboardAvoidingView>
            <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                <Text style={{fontSize:18, fontWeight:'bold',color:'#FF0000'}}>BENGKEL</Text>
            </View>     
            <View style={[styles.row,{flexDirection:'row', paddingRight:5, paddingLeft:5}]}>
              <View style={{flex:9}}>
                <TextInput placeholder="Ketik Kata Kunci Pencarian" style={{width:'100%'}} onChangeText={(text)=>{this.setState({kata_kunci : text})}}></TextInput>
              </View>
              <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={() => {this.onCari() }}>
                  <Text>Cari</Text>
                </TouchableOpacity>
              </View>
            </View>
            {this.view_bengkel()}
            {this.view_map()}
            <View style={[styles.row,{flexDirection:'row', paddingRight:5, paddingLeft:5}]}>
              <View style={{flex:1, justifyContent:'center', alignItems:'center',}}>
                <Text style={{width:'100%', textAlign:'center', backgroundColor:'#FFFFFF'}}>Pilih bengkel yang tersedia pada peta</Text>
              </View>
            </View>
          </KeyboardAvoidingView>
          
        </ScrollView>
      </View>
    </Container>
    );
  }


  updateRef(name, ref) {
    this[name] = ref;
  }

  view_bengkel = () => {
    const {selected_bengkel} = this.state;
    const {dataClaim} = this.props;

    if (selected_bengkel == null){
      return null;
    } else {
      return (
        // <View style={{marginBottom:8}}>
        //   <View style={{alignItems:'flex-end', marginRight:10}}>
        //     <TouchableOpacity onPress={()=>{this.setState({selected_bengkel:null})}}>
        //       <Text>Tutup</Text>
        //     </TouchableOpacity>
        //   </View>
        //   <Card title={selected_bengkel.title}>
        //     <Text style={{ fontWeight: 'bold' }}>Alamat</Text>
        //     <Text>{selected_bengkel.alamat}</Text>
        //     <Text style={{ fontWeight: 'bold' }}>Fasilitas Derek</Text>
        //     <Text>{selected_bengkel.fasilitas_derek}</Text>
        //     <Text style={{ fontWeight: 'bold' }}>No Kontak / WhatsApp</Text>
        //     <Text>{selected_bengkel.telepon}</Text>
        //     <View style={[styles.button,{marginBottom:5}]}>
        //       <View style={{width:'100%', flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
        //         <View style={{flex:1, justifyContent:'center',alignItems:'center',padding:5}}>
        //           <RkButton rkType='danger buttonBengkel' onPress={()=>{this.onSubmit(selected_bengkel)}}>Pilih</RkButton>
        //         </View>
        //         <View style={{flex:1, justifyContent:'center',alignItems:'center',padding:5}}>
        //           <RkButton rkType='danger buttonBengkel' onPress={()=>{this.onLihatPetunjukArah(selected_bengkel)}}>Petunjuk Arah</RkButton>
        //         </View>
        //       </View>
        //     </View>
        //   </Card>
        // </View>

        <View style={{marginBottom:8}}>
          <View style={{alignItems:'flex-end', marginRight:10}}>
            <TouchableOpacity onPress={()=>{this.setState({selected_bengkel:null})}}>
              <Text>Tutup</Text>
            </TouchableOpacity>
          </View>
          <Card title={selected_bengkel.nama_bengkel}>
            <Text style={{ fontWeight: 'bold' }}>Alamat</Text>
            <Text>{selected_bengkel.alamat_bengkel}</Text>
            <Text style={{ fontWeight: 'bold' }}>Fasilitas Derek</Text>
            <Text>ada</Text>
            <Text style={{ fontWeight: 'bold' }}>No Kontak / WhatsApp</Text>
            <Text>{selected_bengkel.telp_bengkel == null ? '-' : selected_bengkel.telp_bengkel}</Text>
            <Text style={{ fontWeight: 'bold' }}>Kota</Text>
            <Text>{selected_bengkel.telp_bengkel == null ? '-' : selected_bengkel.kota_bengkel}</Text>
            {this.view_button()}
          </Card>
        </View>

      )
    }
  }

  view_button = () => {
    const {dataClaim} = this.props;
    const{selected_bengkel} = this.state;
    if(dataClaim.visibleNavigation == true){
      return(
        <View style={[styles.button, { marginBottom: 5 }]}>
          <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
              <RkButton rkType='danger buttonBengkel' onPress={() => { this.onSubmit(selected_bengkel) }}>Pilih</RkButton>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
              <RkButton rkType='danger buttonBengkel' onPress={() => { this.onLihatPetunjukArah(selected_bengkel) }}>Petunjuk Arah</RkButton>
            </View>
          </View>
        </View>
      );
    } else {
      return(
        <View style={[styles.button, { marginBottom: 5 }]}>
          <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
              <RkButton rkType='danger buttonBengkel' onPress={() => { this.onUbah(selected_bengkel) }}>Ubah</RkButton>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
              <RkButton rkType='danger buttonBengkel' onPress={() => { this.onLihatPetunjukArah(selected_bengkel) }}>Petunjuk Arah</RkButton>
            </View>
          </View>
        </View>
      )
    }
    
  }

  view_map_bengkel = () => {
    const {listAllBengkelClaim} = this.props;
    const {kata_kunci, bengkel} = this.state;
    


    if(kata_kunci == null || kata_kunci == ''){
      return (
        listAllBengkelClaim.bengkel.map((item, index) => (
          <Marker
            key={index}
            coordinate={{
              longitude: this.generateLongitude(),
              latitude: this.generateLatitude(),
            }}
            title={item.nama_bengkel}
            onPress={() => { this.onPressMarker(item) }}
          >

          </Marker>
        )
        ));
    } else {
      return (
        bengkel.map((item, index) => (
          <Marker
            key={index}
            coordinate={{
              longitude: this.generateLongitude(),
              latitude: this.generateLatitude(),
            }}
            title={item.nama_bengkel}
            onPress={() => { this.onPressMarker(item) }}
          >

          </Marker>
        ))
      )
    }

    

    // return (
    //   this.state.markers.map((marker, index) =>
    //     (

    //       <Marker
    //         key={index}
    //         coordinate={marker.coordinates}
    //         title={marker.title}
    //         onPress={() => { this.onPressMarker(marker) }}
    //         pinColor={marker.fasilitas_derek == 'ada' ? '#0000FF' : '#FF0000'}
    //       >

    //       </Marker>

    //     )
    //   )
    // )
  }

  view_map = () => {
    const {listLokasiSekarang} = this.props;
    console.log('lokasi',listLokasiSekarang);
    if (listLokasiSekarang.latitude != null || listLokasiSekarang.longitude != null) {
      return(
        <View style={styles.row}>
          <View style={{ width:width, height: width  }}>
              <MapView style={styles.map}
                  initialRegion={{
                      latitude: listLokasiSekarang.latitude,
                      longitude: listLokasiSekarang.longitude,
                      latitudeDelta: 0.026477831051446188,
                      longitudeDelta: 0.026631318032741547,
                  }}
                  
                  onRegionChangeComplete={(region)=>{this.onRegionChangeComplete(region)}}
                  showsUserLocation={true}
              >
                  
                  {this.view_map_bengkel()}
              </MapView>
          </View>
        </View>
      );
    } else {
      return (
        <View style={[styles.row,{height:width, width:width, justifyContent:'center', alignItems:'center'}]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
  }
  
  onPressMarker = (marker) =>{
    this.setState({selected_bengkel : marker});
  }

  onRegionChangeComplete = (region) => {
    console.log('region ',region);
  }

}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listDataGambarSim : state.listDataGambarSim,
    listHalaman: state.listHalaman,
    listLokasiSekarang : state.listLokasiSekarang,
    listAllBengkelClaim: state.listAllBengkelClaim,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginTop: 0,
    height: height - 80 - 53,
  },
  containerEdit: {
    marginTop: 0,
    height: height - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    marginBottom: 8,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  fieldButton: {
      width: (width * 0.6) - 20,
      height: width * 0.4,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: .5,
  },
    fieldImage: {
        width: width * 0.4,
        height: width * 0.4,
        borderWidth: .5,
    },
  map: {
    height: '100%',
    width: '100%',
  },
  marker: {
    backgroundColor: "#FF0000",
    padding: 5,
    borderRadius: 5,
  },
  text: {
    color: "#FFF",
    fontWeight: "bold"
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: "rgba(130,4,150, 0.3)",
    position: "absolute",
    borderWidth: 1,
    borderColor: "rgba(130,4,150, 0.5)",
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanBengkel1);
