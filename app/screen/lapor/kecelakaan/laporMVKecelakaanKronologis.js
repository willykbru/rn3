/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import Navigasi from '../kecelakaan/laporMVKecelakaanNavigasi';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Container, Content, Picker, Form, Item, Label, Input, Separator } from "native-base";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

let radio_pihak_ketiga = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
]

class LaporMVKecelakaanKronologis1 extends Component {
  constructor(props) {
    super(props);
    const {dataClaim} = this.props;
    this.state = {
      kronologis : dataClaim.deskripsiInsiden,
    }
    

  }

  handleBackPress = () => {
    const {listHalaman, dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
      items = [{
        'end' : listHalaman[0].end,
        'current': 3,
      }];
      this.props.setHalaman(items);
      this.props.navigate('LaporMVKecelakaanPihakKetiga');
    } else {
      temp = {
        visibleNavigation : true,
      }
      this.props.setDataClaimMV(temp);
      this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }
    return true;
  }

  onSubmit = () => {
      const {kronologis} = this.state;
      const {listHalaman} = this.props;

      // if(kronologis.length < 20){
      //   Snackbar.show('Kronologis harus memiliki minimal 20 karakter', { duration: Snackbar.SHORT });
      // } else{
      //   // console.log(kronologis);
      //   temp = { 
      //     deskripsiInsiden : kronologis
      //   }
        
      //   items = [{
      //     'end': listHalaman[0].end > 5 ? listHalaman[0].end : 5,
      //     'current': 5,
      //   }];
      //   this.props.setHalaman(items);
      //   this.props.setDataClaimMV(temp);
      //   this.props.navigate('LaporMVKecelakaanBengkel');
      // }

        temp = { 
          deskripsiInsiden : kronologis
        }
        
        items = [{
          'end': listHalaman[0].end > 5 ? listHalaman[0].end : 5,
          'current': 5,
        }];
        this.props.setHalaman(items);
        this.props.setDataClaimMV(temp);
        this.props.navigate('LaporMVKecelakaanBengkel');
  }

  onUbah = () => {
    const {kronologis} = this.state;
      
      // if(kronologis.length < 20){
      //   Snackbar.show('Kronologis harus memiliki minimal 20 karakter', { duration: Snackbar.SHORT });
      // } else{
      //   // console.log(kronologis);
      //   temp = { 
      //     deskripsiInsiden : kronologis
      //   }
        
      //   items = [{
      //     'end': listHalaman[0].end > 5 ? listHalaman[0].end : 5,
      //     'current': 5,
      //   }];
      //   this.props.setHalaman(items);
      //   this.props.setDataClaimMV(temp);
      //   this.props.navigate('LaporMVKecelakaanBengkel');
      // }

        temp = { 
          deskripsiInsiden : kronologis,
          visibleNavigation: true,
        }
        
        this.props.setDataClaimMV(temp);
        this.props.navigate('LaporMVKecelakaanKonfirmasi');
  }
 
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

  }

  componentWillUnmount() {

  }

  render() {
    const { dataClaim } = this.props;
    console.log('data claim kronologis', dataClaim);
    // console.log('state', this.state);
    
    return (
    <Container>
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.kembali() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'FORM PELAPORAN',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <Navigasi />
      
        <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>   
          <KeyboardAvoidingView>
            <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                <Text style={{fontSize:18, fontWeight:'bold',color:'#FF0000'}}>KRONOLOGIS</Text>
            </View>     
            <View style={styles.row}>
                <View><Text>Terangkan lebih jauh sebab-sebab terjadinya kecelakaan (Maksimal 500 karakter)!*</Text></View>
            </View>
            <View style={styles.row}>
                <View style={{width:'100%', height:300, borderWidth: 1}}>
                  <TextInput 
                    maxLength={500}
                    multiline={true}
                    value={this.state.kronologis}
                    style={{width:'100%', height:'100%', alignItems:'flex-start', justifyContent:'flex-start', textAlignVertical:'top'}}
                    spellCheck={false}
                    onChangeText={(text)=>{this.updateKronologisText(text)}}                  
                  />
                </View>
            </View>
            {this.view_button()}
          </KeyboardAvoidingView>
          
        </ScrollView>


      </View>
    </Container>
    );
  }

  updateKronologisText = (text) => {
    this.setState({kronologis : text});
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  view_button = () => {
    const {dataClaim}=  this.props;
    if(dataClaim.visibleNavigation == true){
      return(
        <View style={styles.button}>
            <RkButton rkType='danger' onPress={()=>{this.onSubmit()}}>Lanjut</RkButton>
          </View>
      );
    } else{
      return(
        <View style={styles.button}>
          <RkButton rkType='danger' onPress={()=>{this.onUbah()}}>Ubah</RkButton>
        </View>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listDataGambarSim : state.listDataGambarSim,
    listHalaman: state.listHalaman,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  container: {
    marginTop: 0,
    height: height - 80 - 53,
  },
  containerEdit: {
    marginTop: 0,
    height: height - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 8,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  fieldButton: {
      width: (width * 0.6) - 20,
      height: width * 0.4,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: .5,
  },
    fieldImage: {
        width: width * 0.4,
        height: width * 0.4,
        borderWidth: .5,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanKronologis1);
