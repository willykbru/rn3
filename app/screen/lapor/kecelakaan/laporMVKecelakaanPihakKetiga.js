/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput, ImageBackground } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import ImageResizer from 'react-native-image-resizer';
import ImagePicker from 'react-native-image-crop-picker';
import Navigasi from '../kecelakaan/laporMVKecelakaanNavigasi';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Container, Content, Picker, Form, Item, Label, Input, Separator } from "native-base";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';


const { width, height } = Dimensions.get('window');

let radio_pihak_ketiga = [
  { label: 'Ya', value: 'Y' },
  { label: 'Tidak', value: 'N' },
]

class LaporMVKecelakaanPihakKetiga1 extends Component {
  constructor(props) {
    super(props);
    const {dataClaim} = this.props;
    this.state = {
      nama_pihak_ketiga: dataClaim.nama_pihak3,
      no_telepon_pihak_ketiga: dataClaim.phone_pihak3,
      email_pihak_ketiga: dataClaim.email_pihak3,
      alamat_pihak_ketiga: dataClaim.alamat_pihak3,
      perkiraan_kecepatan_pihak_ketiga: dataClaim.kecepatan_pihak3,
      status_pihak_ketiga: dataClaim.status_pihak3,
      pihak_ketiga: dataClaim.status_pihak3,
      pihak_ketigaIndex: 1,
      selected_jenis_pihak_ketiga: dataClaim.jenis_pihak3 == null ? 'KB' : dataClaim.jenis_pihak3,
      perkiraan_kecepatan_pihak_ketiga_editable : true,
      keterangan_pihak_ketiga : dataClaim.keterangan_pihak_ketiga,      
    }



    this.onSubmit = this.onSubmit.bind(this);

    this.onFocus = this.onFocus.bind(this);
    this.onChangeText = this.onChangeText.bind(this);

    // this.namaPihakKetigaRef = this.updateRef.bind(this, 'nama_pihak_ketiga');
    this.noTeleponPihakKetigaRef = this.updateRef.bind(this, 'no_telepon_pihak_ketiga');
    // this.emailPihakKetigaRef = this.updateRef.bind(this, 'email_pihak_ketiga');
    // this.alamatPihakKetigaRef = this.updateRef.bind(this, 'alamat_pihak_ketiga');
    // this.perkiraanKecepatanPihakKetigaRef = this.updateRef.bind(this, 'perkiraan_kecepatan_pihak_ketiga');

  }

  onValueChange(value) {
    this.setState({
      selected_jenis_pihak_ketiga: value
    });

  }

  handleBackPress = () => {
    const {listHalaman, dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
      items = [{
        'end' : listHalaman[0].end,
        'current': 2,
      }];
      this.props.setHalaman(items);
      this.props.navigate('LaporMVKecelakaanPengemudi');
    } else {
      temp = {
        visibleNavigation : true,        
      }

      this.props.setDataClaimMV(temp);
      this.props.navigate('LaporMVKecelakaanKonfirmasi');
    }

    return true;
  }

  onUbah = () =>{
    const {listDataGambarKtpPihak3, listDataGambarSimPihak3, listDataGambarStnkPihak3, listHalaman} = this.props;
      if (this.state.pihak_ketiga == 'Y') {
        temp = {
            jenis_pihak3: this.state.selected_jenis_pihak_ketiga,
            phone_pihak3: this.state.no_telepon_pihak_ketiga,
            status_pihak3: this.state.pihak_ketiga,  
            sim_pihak3: listDataGambarSimPihak3[0].path,
            ktp_pihak3: listDataGambarKtpPihak3[0].path,
            stnk_pihak3: listDataGambarStnkPihak3[0].path,
            visibleNavigation: true,
        }
      } else {
          temp = {
            jenis_pihak3: '',
            phone_pihak3: '',
            status_pihak3: this.state.pihak_ketiga,
            sim_pihak3: '' ,
            ktp_pihak3: '',
            stnk_pihak3: '',
            visibleNavigation:true,
        }

    }
    this.props.setDataClaimMV(temp);

    this.props.navigate('LaporMVKecelakaanKonfirmasi');
  }

  onSubmit = () => {
      const {listDataGambarKtpPihak3, listDataGambarSimPihak3, listDataGambarStnkPihak3, listHalaman} = this.props;
      if (this.state.pihak_ketiga == 'Y') {
        temp = {
            jenis_pihak3: this.state.selected_jenis_pihak_ketiga,
            phone_pihak3: this.state.no_telepon_pihak_ketiga,
            status_pihak3: this.state.pihak_ketiga,  
            sim_pihak3: listDataGambarSimPihak3[0].path,
            ktp_pihak3: listDataGambarKtpPihak3[0].path,
            stnk_pihak3: listDataGambarStnkPihak3[0].path,
        }
      } else {
          temp = {
            jenis_pihak3: '',
            phone_pihak3: '',
            status_pihak3: this.state.pihak_ketiga,
            sim_pihak3: '' ,
            ktp_pihak3: '',
            stnk_pihak3: '',
        }

    }
    this.props.setDataClaimMV(temp);

    items = [{
      'end': listHalaman[0].end > 4 ? listHalaman[0].end : 4,
      'current': 4,
    }];
    this.props.setHalaman(items);
    this.props.navigate('LaporMVKecelakaanKronologis');
  }

  onSubmitNo_telepon_pihak_ketiga = () =>{

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress());

    const {dataClaim} = this.props;

    if (dataClaim.status_pihak3 == 'Y') {
      this.setState({
        pihak_ketigaIndex: 0,
      });
    } else if (dataClaim.status_pihak3 == 'N') {
      this.setState({
        pihak_ketigaIndex: 1,
      });

    }
  }

  componentWillUnmount() {

  }

  lanjut() {

  }


  laporan_pihak_ketiga() {
    let { errors = {}, ...data } = this.state;

    if (this.state.pihak_ketiga == 'Y') {
      return (
        <KeyboardAwareScrollView keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
          getTextInputRefs={() => {
            return [this.namaPihakKetigaRef, this.emailPihakKetigaRef, this.keadaanKendaraanRef, this.jumlahTaksiranRef];
          }}
        >
          
             
              <Form>
              <View style={styles.row}>               
                <View style={styles.field}>
                  <Text>Jenis Pihak Ketiga</Text>
                </View>
              </View> 
              <View style={styles.row}>              
                <View style={styles.field}>
                  <Picker
                    mode="dialog"
                    placeholder="Jenis Pihak Ketiga"
                    placeholderStyle={{ color: "#2874F0" }}
                    note={false}
                    selectedValue={this.state.selected_jenis_pihak_ketiga}
                    onValueChange={this.onValueChange.bind(this)}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10,
                    }}
                    style={{ width: undefined}}
                  >
                    <Picker.Item label="Kendaraan Bermotor" value="KB" />
                    <Picker.Item label="Orang" value="O" />
                    <Picker.Item label="Properti / Aset" value="PA" />
                  </Picker>      
                </View>
              </View> 
            </Form>          
            <View style={styles.row}>
                <View style={styles.icon}><FontAwesome name='address-book' size={25} color='rgba(0, 0, 0, .38)' /></View>
                <View style={styles.field}>
                    <TextField
                    ref={this.noTeleponPihakKetigaRef}
                    label='No Handphone Pihak Ketiga / WhatsApp *'
                    keyboardType='phone-pad'
                    onFocus={this.onFocus}
                    onSubmitEditing={this.onSubmitNo_telepon_pihak_ketiga}
                    onChangeText={this.onChangeText}
                    returnKeyType='next'
                    tintColor='#228B22'
                    error={errors.no_telepon_pihak_ketiga}
                    maxLength={14}
                    value={this.state.no_telepon_pihak_ketiga}
                    />
                </View>
            </View>
                {this.view_stnk_pihak_ketiga()}
                {this.view_sim_pihak_ketiga()}
                {this.view_ktp_pihak_ketiga()}
               

                {this.view_button()}
            
        </KeyboardAwareScrollView>

      );
    } else {
      return (
        <View>
          {this.view_button()}
        </View>
      );
    }
  }

  view_button = () => {
    const {dataClaim} = this.props;

    if(dataClaim.visibleNavigation == true){
      return(
        <View style={styles.button}>
          <RkButton rkType='danger' onPress={this.onSubmit}>Lanjut</RkButton>
        </View>
      );
    }
    else {
      return(
        <View style={styles.button}>
          <RkButton rkType='danger' onPress={()=>{this.onUbah()}}>Ubah</RkButton>
        </View>
      )
    }
  }

  view_stnk_pihak_ketiga = () => {
      const {listDataGambarStnkPihak3} = this.props;
      if (this.state.selected_jenis_pihak_ketiga == 'KB') {
          return (
                <View>
              <View style={styles.row}>
                  <Text>Foto STNK Pihak Ketiga</Text>
              </View>
              <View style={styles.row}>
                  <View style={styles.fieldImage}>
                    {listDataGambarStnkPihak3[0].path == null ?
                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSTNK()} >
        
                        <Image source={require('../../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} />
        
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSTNK()} >
        
                        <Image source={{ uri: listDataGambarStnkPihak3[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
        
                        </TouchableOpacity>
                    }
                  </View>
                  <View style={styles.fieldButton}>
                    <RkButton rkType='danger' onPress={() => this.uploadSTNK()}>Unggah Foto STNK</RkButton>
                  </View>
              </View>
              </View>
          );
      } else {
          return (
              null
          );
      }
  }

  view_sim_pihak_ketiga = () => {
      const {listDataGambarSimPihak3} = this.props;
      if(this.state.selected_jenis_pihak_ketiga == 'KB'){
          return(
              <View>
            <View style={styles.row}>
                <Text>Foto SIM Pihak Ketiga</Text>
            </View>
            <View style={styles.row}>
                <View style={styles.fieldImage}>
                {listDataGambarSimPihak3[0].path == null ?
                    <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSIM()} >
    
                    <Image source={require('../../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} />
    
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadSIM()} >
    
                    <Image source={{ uri: listDataGambarSimPihak3[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
    
                    </TouchableOpacity>
                }
                </View>
                <View style={styles.fieldButton}>
                <RkButton rkType='danger' onPress={() => this.uploadSIM()}>Unggah Foto SIM</RkButton>
                </View>
            </View>
            </View>
          );
      } else {
          return(
              null
          );
      }
  }

  view_navigasi = () => {
    return(
      <View style={{height: 53, backgroundColor: '#FFFFFF', justifyContent:'center', paddingLeft:10}}>
        <View style={{flexDirection:'row'}}>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-2AD268.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>1</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-2AD268.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>2</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-FF0000.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>3</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>4</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>5</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>6</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity>
              <ImageBackground source = {
                  require('../../../assets/icons/component/circular-shape-CCCCCC.png')
              } style={styles.navigasi} >
              <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
                  <Text>7</Text>
              </View>
              </ImageBackground>
            </TouchableOpacity>
        </View>
      </View>
    );
  }

  view_ktp_pihak_ketiga = () => {
      const {listDataGambarKtpPihak3} = this.props;
      if(this.state.selected_jenis_pihak_ketiga == 'O' || this.state.selected_jenis_pihak_ketiga == 'PA'){
          return(
              <View>
                <View style={styles.row}>
                    <Text>Foto KTP Pihak Ketiga</Text>
                </View>
                <View style={styles.row}>
                <View style={styles.fieldImage}>
                {listDataGambarKtpPihak3[0].path == null ?
                    <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadKTP()} >
    
                    <Image source={require('../../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} />
    
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => this.uploadKTP()} >
    
                    <Image source={{ uri: listDataGambarKtpPihak3 [0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
    
                    </TouchableOpacity>
                }
                </View>
                <View style={styles.fieldButton}>
                <RkButton rkType='danger' onPress={() => this.uploadKTP()}>Unggah Foto KTP</RkButton>
                </View>
            </View>
              </View>
            
          );
      } else {
          return null;
      }
  }

  uploadKTP = () => {
    ImagePicker.openCamera({
        cropping: false,
        includeExif: false,
        avoidEmptySpaceAroundImage: true,
    }).then(image => {
        ImageResizer.createResizedImage(image.path, image.width * 0.4, image.height * 0.4, 'JPEG', 100, 0).then((response) => {

            image = {
                'size': response.size,
                'path': response.uri,
                'caption': '',
            };

            this.props.setDataGambarKtpPihak3KameraTemp(image);
        });

    }).catch(e => alert(e));
  }

  uploadSIM = () => {

    ImagePicker.openCamera({
        cropping: false,
        includeExif: false,
        avoidEmptySpaceAroundImage: true,
    }).then(image => {
        ImageResizer.createResizedImage(image.path, image.width * 0.4, image.height * 0.4, 'JPEG', 100, 0).then((response) => {

            image = {
                'size': response.size,
                'path': response.uri,
                'caption': '',
            };

            this.props.setDataGambarSimPihak3KameraTemp(image);
        });

    }).catch(e => alert(e));
  }

  uploadSTNK = () => {

    ImagePicker.openCamera({
        cropping: false,
        includeExif: false,
        avoidEmptySpaceAroundImage: true,
    }).then(image => {
        ImageResizer.createResizedImage(image.path, image.width * 0.4, image.height * 0.4, 'JPEG', 100, 0).then((response) => {

            image = {
                'size': response.size,
                'path': response.uri,
                'caption': '',
            };

            this.props.setDataGambarStnkPihak3KameraTemp(image);
        });

    }).catch(e => alert(e));
  }

  onChangeText(text) {
    try {
      ['no_telepon_pihak_ketiga']
        .map((name) => ({ name, ref: this[name] }))
        .forEach(({ name, ref }) => {
          if (ref.isFocused()) {
            this.setState({ [name]: text });
          }
        });
    } catch (error) {
      console.log('error', error);
    }

  }

  onFocus() {
    let { errors = {}, ...data } = this.state;


    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }
    this.setState({ errors });
  }

  onPressRadioLaporPolisi(value) {
    this.setState({ status_pihak_ketiga: value })
  }

  updateRef(name, ref) {
    this[name] = ref;
  }


  render() {
    const { dataClaim } = this.props;
    console.log('data claim pihak ketiga', dataClaim);
    // console.log('data claim 5', dataClaim);
    // console.log('state', this.state);
    return (
    <Container>
      <View>
        <Header
          leftComponent={
            <TouchableOpacity onPress={() => { this.handleBackPress() }}>
              <Icon name='md-arrow-round-back' size={25} color='#FFF' />
            </TouchableOpacity>
          }
          centerComponent={{
            text: 'FORM PELAPORAN',
            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
          }}
          outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
        />
        <Navigasi />
        <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>   
            <View style={[styles.row,{justifyContent:'center', alignItems:'center'}]}>
                <Text style={{fontSize:18, fontWeight:'bold',color:'#FF0000'}}>INFORMASI PIHAK KETIGA</Text>
            </View>     
          <View style={styles.row}>
            <View style={styles.text}>
              <Text>Apakah ada orang lain yang dirugikan?</Text>
              <RadioForm
                formHorizontal={true}
                animation={true}
                initial={1}
              >
                {radio_pihak_ketiga.map((obj, i) => {
                  var onPress = (value, index) => {
                    this.setState({
                      pihak_ketigaIndex: index,
                      pihak_ketiga: value,
                    })

                    // console.log(this.state.pihak_ketigaIndex);
                    // console.log(this.state.pihak_ketiga);
                  }
                  return (
                    <RadioButton labelHorizontal={true} key={i} >
                      {/*  You can set RadioButtonLabel before RadioButtonInput */}
                      <RadioButtonInput
                        obj={obj}
                        index={i}
                        isSelected={this.state.pihak_ketigaIndex === i}
                        onPress={onPress}
                        buttonStyle={{}}
                        buttonWrapStyle={{ marginRight: 10 }}
                      />
                      <RadioButtonLabel
                        obj={obj}
                        index={i}
                        onPress={onPress}
                        labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                      />
                    </RadioButton>
                  )
                })}
              </RadioForm>
            </View>
          </View>

          {this.laporan_pihak_ketiga()}
        </ScrollView>
        

      </View>
    </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    claimState: state.claimState,
    claimLabelState: state.claimLabelState,
    dataClaim: state.dataClaim,
    // listNoPolisMV : state.listNoPolisMV,
    dataPolis: state.dataPolis, //polis per masing masing orang
    detailPolis: state.detailPolis,
    listDataGambarSimPihak3: state.listDataGambarSimPihak3,
    listDataGambarKtpPihak3: state.listDataGambarKtpPihak3,
    listDataGambarStnkPihak3: state.listDataGambarStnkPihak3,
    listHalaman: state.listHalaman,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  container: {
    marginTop: 0,
    height: height - 80 - 53,
  },
  containerEdit: {
    marginTop: 0,
    height: height - 80,
  },
  row: {
    flexDirection: 'row',
    width: Dimensions.get('window').width,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 8,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  field: {
    flex: 9,
    paddingLeft: 10,
  },
  fieldButton: {
      width: (width * 0.6) - 20,
      height: width * 0.4,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: .5,
  },
  fieldImage: {
      width: width * 0.4,
      height: width * 0.4,
      borderWidth: .5,
  },
  navigasi: {
    width: (width - 20) / 7,
    height: (width - 20) / 7,
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKecelakaanPihakKetiga1);
