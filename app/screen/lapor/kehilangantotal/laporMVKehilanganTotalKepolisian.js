/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

// import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Container, Content, Picker, Form, Item, Label, Input, Separator } from "native-base";
import Navigasi from '../kehilangantotal/laporMVKehilanganTotalNavigasi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

let radio_lapor_polisi = [
    { label: 'Ya', value: 'Y' },
    { label: 'Tidak', value: 'N' },
]

class LaporMVKehilanganTotalKepolisian1 extends Component {
    
    constructor(props) {
        super(props);
        const { dataClaim } = this.props;

        this.state = {
            lapor_polisi : dataClaim.lapor_polisi,
            lapor_polisiIndex : (dataClaim.lapor_polisi == 'Y') ? 0 : 1,
            tempat_lapor_polisi : '',
        }

        this.onFocus = this.onFocus.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onSubmitTempatLaporPolisi = this.onSubmitTempatLaporPolisi.bind(this);
    
        this.tempatLaporPolisiRef = this.updateRef.bind(this, 'tempat_lapor_polisi');
    }

    handleBackPress = () => {
        //this.goBack(); // works best when the goBack is async
        this.props.navigate('LaporMVKehilanganTotalPengemudi');
        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {

    }

    onSubmitTempatLaporPolisi = () => {

    }

    onSubmit = () => {
        const {lapor_polisi} = this.state;
        this.props.navigate('LaporMVKehilanganTotalSaksi');        

        // if (this.state.lapor_polisi == 'N') {
        //     // Alert.alert(lapor_polisi);
        //     this.props.navigate('LaporMVKehilanganTotalSaksi');
        
        // } else {
        //     let errors = {};
        //     let isError = 0;
        //     ['tempat_lapor_polisi']
        //         .forEach((name) => {
        //             let value = this[name].value();

        //             if (!value) {
        //                 errors[name] = 'Tidak boleh kosong';
        //                 isError = 1;
        //             }
        //         });

        //     this.setState({ errors });

        //     if (isError == 0) {
        //         this.props.navigate('LaporMVKehilanganTotalSaksi');
        //     } else {

        //     }
        // }


    }

    onUbah = () => {

    }

    lanjut = () => {
        console.log('test');
        this.props.navigate('LaporMVKehilanganTotalSaksi');
    }

    onChangeText(text) {
        try {
            ['tempat_lapor_polisi']
                .map((name) => ({ name, ref: this[name] }))
                .forEach(({ name, ref }) => {
                    if (ref.isFocused()) {
                        this.setState({ [name]: text });
                    }
                });
        } catch (error) {
            console.log('error', error);
        }

    }

    onFocus() {
        let { errors = {}, ...data } = this.state;


        for (let name in errors) {
            let ref = this[name];

            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }
        this.setState({ errors });
    }

    onPressRadioLaporPolisi(value) {
        this.setState({ lapor_polisi: value })
    }


    updateRef(name, ref) {
        this[name] = ref;
    }

    render() {
        const {dataClaim} = this.props;


        return(
            <Container>
                <View>
                    <Header
                        leftComponent={
                            <TouchableOpacity onPress={() => { this.handleBackPress() }}>
                                <Icon name='md-arrow-round-back' size={25} color='#FFF' />
                            </TouchableOpacity>
                        }
                        centerComponent={{
                            text: 'FORM PELAPORAN',
                            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                        }}
                        outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                    />
                    <Navigasi />
                    <KeyboardAwareScrollView keyboardDismissMode="interactive"
                        keyboardShouldPersistTaps="always"
                        getTextInputRefs={() => {
                            return [this.nama_pengemudiRef, this.alamat_pengemudiRef, this.no_sim_pengemudiRef, this.kecepatanInsidenRef];
                        }}
                    >
                        <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>
                            <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FF0000' }}>INFORMASI KEPOLISIAN</Text>
                            </View>
                            {this.view_pilihanLapor()}
                            
                            {this.view_laporan_polisi()}

                            {/* {this.view_button()} */}
                            <View style={styles.button}>
                                <RkButton rkType='danger' onPress={() => { this.lanjut() }}>Lanjut</RkButton>
                            </View>
                        </ScrollView>
                    </KeyboardAwareScrollView>
                </View>
            </Container>
        );
    }

    view_laporan_polisi = () => {
        const {listDataLaporanPolisi} = this.props;

        let { errors = {}, ...data } = this.state;
        let { tempat_lapor_polisi = null } = data;

        if(this.state.lapor_polisi == 'Y'){
            return(
                <View>
                    <View style={styles.row}>
                        <View style={styles.icon}><FontAwesome name='map-marker' color='rgba(0,0,0, .38)' size={25} /></View>
                        <View style={styles.field}>
                            <TextField
                                label='Tempat Lapor Polisi'
                                keyboardType='default'
                                ref={this.tempatLaporPolisiRef}
                                maxLength={80}
                                value={this.state.tempat_lapor_polisi}
                                onChangeText={this.onChangeText}
                                onSubmitEditing={this.onSubmitTempatLaporPolisi}
                                returnKeyType='next'
                                tintColor='#228B22'
                                error={errors.tempat_lapor_polisi}
                                onFocus={this.onFocus}
                            />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.fieldImage}>
                            {
                                listDataLaporanPolisi[0].path == null ?

                                    <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={this.onSubmitLaporanPolisi} >

                                        <Image source={require('../../../assets/icons/component/id-card.png')} style={{ width: '100%', height: '100%', backgroundColor: 'transparent', resizeMode: 'contain' }} />

                                    </TouchableOpacity>

                                    :

                                    <TouchableOpacity style={{ alignItems: 'center', margin: 10 }} onPress={() => { this.konfirmasiPolisi(listDataLaporanPolisi[0].path).done() }} >
                                        <Image source={{ uri: listDataLaporanPolisi[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                                    </TouchableOpacity>

                            }

                        </View>
                        <View style={styles.fieldButton}>
                            <RkButton rkType='danger' onPress={this.onSubmitLaporanPolisi}>Unggah Foto Laporan Polisi</RkButton>
                        </View>
                    </View>
                    
                </View>
            );
        } else{
            return null;
        }
    }

    view_pilihanLapor = () => {

        return (
            <View style={styles.row}>
                <View style={styles.text}>
                    <Text>Apakah kejadian ini sudah dilaporkan ke kepolisian?</Text>
                    <RadioForm
                        formHorizontal={true}
                        animation={true}
                        initial={1}
                    >
                        {radio_lapor_polisi.map((obj, i) => {
                            var onPress = (value, index) => {
                                this.setState({
                                    lapor_polisiIndex: index,
                                    lapor_polisi: value,
                                })
                            }
                            return (
                                <RadioButton labelHorizontal={true} key={i} >
                                    {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                    <RadioButtonInput
                                        obj={obj}
                                        index={i}
                                        isSelected={this.state.lapor_polisiIndex === i}
                                        onPress={onPress}
                                        buttonStyle={{}}
                                        buttonWrapStyle={{ marginRight: 10 }}
                                    />
                                    <RadioButtonLabel
                                        obj={obj}
                                        index={i}
                                        onPress={onPress}
                                        labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                                    />
                                </RadioButton>
                            )
                        })}
                    </RadioForm>
                </View>
            </View>
        );
    }

    view_button = () => {
        const { dataClaim } = this.props;

        if (dataClaim.visibleNavigation == true) {
            return (
                <View style={styles.button}>
                    <RkButton rkType='danger' onPress={() => { this.onSubmit() }}>Lanjut</RkButton>
                </View>
            );
        } else {
            return (
                <View style={styles.button}>
                    <RkButton rkType='danger' onPress={() => { this.onUbah() }}>Ubah</RkButton>
                </View>
            );
        }

    }
}

function mapStateToProps(state) {
    return {
        dataClaim : state.dataClaim,
        listDataLaporanPolisi: state.listDataLaporanPolisi,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        marginBottom: 10,
        marginTop: 5,
    },
    container: {
        marginTop: 0,
        height: height - 80 - 53,
    },
    containerEdit: {
        marginTop: 0,
        height: height - 80,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    field: {
        flex: 9,
        paddingLeft: 10,
    },
    fieldButton: {
        width: (width * 0.6) - 20,
        height: width * 0.4,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: .5,
    },
    fieldImage: {
        width: width * 0.4,
        height: width * 0.4,
        borderWidth: .5,

    },
    fieldTanggal: {
        flex: 9,
        paddingLeft: 10,
        paddingBottom: 15
    },
    text: {

    },
    textPicker: {
        height: 56,
        fontSize: 16,
        borderBottomWidth: .5,
        borderColor: 'rgba(0,0,0, .38)',
        paddingTop: 20,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKehilanganTotalKepolisian1);
