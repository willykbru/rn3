/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

import Icon2 from 'react-native-vector-icons/Ionicons';
import { Container, Content, Tab, Tabs, TabHeading, Icon, Button, Footer, Card, CardItem, Left, Center, Right } from 'native-base';
// import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import Navigasi from '../kehilangantotal/laporMVKehilanganTotalNavigasi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

RkTheme.setType('RkButton', 'buttonCard', {
    container: {
        backgroundColor: '#0000FF',
        height: 20,
        width: '70%'
    },
    content: {
        color: 'white',
        fontSize: 12,
    }
});

class LaporMVKehilanganTotalKonfirmasi1 extends Component {
    constructor(props) {
        super(props);

    }

    handleBackPress = () => {
        const { listHalaman, dataClaim } = this.props;

        if (dataClaim.visibleNavigation == true) {
            items = [{
                'end': listHalaman[0].end,
                'current': 6,
            }];
            this.props.setHalaman(items);
            this.props.navigate('LaporMVKecelakaanFoto');
        } else {
            temp = {
                visibleNavigation: true,
            }
            this.props.setDataClaimMV(true);
            this.props.navigate('LaporMVKecelakaanKonfirmasi');
        }

        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);



    }

    componentWillUnmount() {

    }


    render() {
        const { listDataGambar } = this.props;
        console.log('listDataGambar Foto', listDataGambar);

        const { dataClaim } = this.props;
        console.log('data claim konfirmasi', dataClaim);

        let output = [];
        if (listDataGambar != null) {
            // console.log('images',this.state.images);
            listDataGambar.forEach(function (item) {
                fileuri = item.path;

                output.push(
                    <TouchableOpacity
                        onPress={() => { this.tambahFotoLain() }}
                        style={{ width: width / 6, height: width / 6, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
                        <Image
                            source={{ uri: fileuri }} style={{ width: (width / 6), height: (width / 6), resizeMode: 'stretch', backgroundColor: '#000' }}
                        />
                    </TouchableOpacity>
                );
            }.bind(this));
        }
        return (
            <View>
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() => { this.handleBackPress() }}>
                            <Icon2 name='md-arrow-round-back' size={25} color='#FFF' />
                        </TouchableOpacity>
                    }
                    centerComponent={{
                        text: 'FORM PELAPORAN',
                        style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                    }}
                    outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                />
                <Navigasi />
                <View style={{ height: height - 53 - 24 - 53 }}>
                    <Container>
                        <ScrollView>
                            <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FF0000' }}>KONFIRMASI</Text>
                            </View>
                            {this.view_umum()}
                            {this.view_pengemudi()}
                            {this.view_kepolisian()}
                            {this.view_kronologis()}
                            <View style={[styles.row, { justifyContent: 'center', alignItems: 'center', margin: 10 }]}>
                                <View style={styles.button}>
                                    <RkButton rkType='danger' onPress={() => { this.onSubmit() }}>Lanjut</RkButton>
                                </View>
                            </View>
                        </ScrollView>
                    </Container>
                </View>

            </View>
        );
    }

    onSubmit = () => {

    }

    editPengemudi = () => {
        temp = {
            visibleNavigation: false,
        }
        this.props.setDataClaimMV(temp);


        this.props.navigate('LaporMVKehilanganTotalPengemudi');
    }

    editUmum = () => {
        temp = {
            visibleNavigation: false,
        }

        this.props.setDataClaimMV(temp);


        this.props.navigate('LaporMVKehilanganTotalUmum');
    }

    editKepolisian = () => {
        temp = {
            visibleNavigation: false,
        }

        this.props.setDataClaimMV(temp);


        this.props.navigate('LaporMVKehilanganTotalKepolisian');
    }

    editKronologis = () => {
        temp = {
            visibleNavigation: false,
        }

        this.props.setDataClaimMV(temp);

        this.props.navigate('LaporMVKehilanganTotalKronologis');
    }

    view_kronologis = () => {
        const { dataClaim } = this.props;
        return (
            <View style={styles.viewCard}>
                <View>
                    <View style={styles.viewTitleCard}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text>Kronologis</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <RkButton rkType='buttonCard' onPress={() => { this.editKronologis() }}>Edit</RkButton>
                        </View>
                    </View>
                    <Card style={styles.card}>
                        <CardItem>
                            <View>
                                <Text>{dataClaim.deskripsiInsiden}</Text>
                            </View>
                        </CardItem>
                    </Card>
                </View>
            </View>
        );
    }

    view_kepolisian = () => {
        const { dataClaim } = this.props;
        return (
            <View style={styles.viewCard}>
                <View style={styles.viewTitleCard}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text>Informasi Kepolisian</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <RkButton rkType='buttonCard' onPress={() => { this.editKepolisian() }}>Edit</RkButton>
                    </View>
                </View>

                {dataClaim.lapor_polisi== 'Y' ? this.view_kepolisian_N() : this.view_kepolisian_Y()}

            </View>
        );
    }

    view_kepolisian_N = () => {
        const { dataClaim } = this.props;

        return (
            <Card style={styles.card}>
                <CardItem>
                    <View>
                        <Text style={styles.title}>Apakah kejadian ini sudah dilaporkan ke kepolisian?</Text>
                        <Text style={styles.content}>TIDAK</Text>
                    </View>
                </CardItem>
            </Card>
        )
    }

    view_kepolisian_Y = () => {
        const { dataClaim } = this.props;

        return (
            <Card style={styles.card}>
                <CardItem>
                    <View>
                        <Text style={styles.title}>Apakah kejadian ini sudah dilaporkan ke kepolisian?</Text>
                        <Text style={styles.content}>Ada</Text>
                    </View>
                </CardItem>

                {this.view_tempat_lapor_polisi()}
                {this.view_file_laporan_polisi()}
            </Card>
        )
    }

    view_tempat_lapor_polisi = () => {
        const { dataClaim} = this.props;
       
        return (
            <CardItem>
                <View>
                    <Text style={styles.title}>Tempat Lapor Polisi</Text>
                    <Text style={styles.content}>Dummy</Text> 
                </View>
            </CardItem>
        );
        
    }

    view_file_laporan_polisi = () => {
        const { dataClaim, listDataLaporanPolisi } = this.props;

        
        return (
            <CardItem>
                <View>
                    <Text style={styles.title}>Foto Laporan Polisi</Text>
                    <View style={{ width: width * 0.25, height: width * 0.25 }}>
                        <Image source={{ uri: listDataLaporanPolisi[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                    </View>
                </View>
            </CardItem>
        );
    }

    view_lampiran_foto = () => {
        const { listDataGambar } = this.props;
        console.log('konfirmasi', listDataGambar);

        let output = [];
        if (listDataGambar != null) {
            // console.log('images',this.state.images);
            listDataGambar.forEach(function (item) {
                fileuri = item.path;

                output.push(
                    <View
                        style={{ width: width / 4, height: width / 4, backgroundColor: 'rgba(0,0,0,.38)', margin: 10 }} key={item.path}>
                        <Image
                            source={{ uri: fileuri }} style={{ width: (width / 4), height: (width / 4), resizeMode: 'contain', backgroundColor: '#000' }}
                        />
                    </View>
                );
            }.bind(this));
        }

        return (
            <View style={{ flexDirection: 'row', width: '100%', flexWrap: 'wrap' }}>
                {output}
            </View>
        );
    }

    view_umum = () => {
        const { dataClaim } = this.props;
        return (
            <View style={styles.viewCard}>
                <View style={styles.viewTitleCard}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text>INFORMASI UMUM</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <RkButton rkType='buttonCard' onPress={() => { this.editUmum() }}>Edit</RkButton>
                    </View>
                </View>
                <Card style={styles.card}>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>No Polis</Text>
                            <Text style={styles.content}>{dataClaim.nomorPolis}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Nama Tertanggung</Text>
                            <Text style={styles.content}>{dataClaim.nama_tertanggung}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>No Polisi</Text>
                            <Text style={styles.content}>{dataClaim.nomorPolisi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>No Handphone Tertanggung</Text>
                            <Text style={styles.content}>{dataClaim.no_hp_tertanggung}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Email Tertanggung</Text>
                            <Text style={styles.content}>{dataClaim.email_tertanggung}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Tanggal Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.tanggalKejadian}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Jam Insiden</Text>
                            <Text style={styles.content}>{dataClaim.jamInsiden}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Alamat Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.alamatKejadian}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Provinsi Tempat Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.labelProvinsiInsiden}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Kota Tempat Kejadian</Text>
                            <Text style={styles.content}>{dataClaim.labelKotaInsiden}</Text>
                        </View>
                    </CardItem>
                </Card>
            </View>
        );
    }

    view_pengemudi = () => {
        const { dataClaim, listDataGambarSim } = this.props;
        return (
            <View style={styles.viewCard}>
                <View style={styles.viewTitleCard}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text>Informasi Pengemudi</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <RkButton rkType='buttonCard' onPress={() => { this.editPengemudi() }}>Edit</RkButton>
                    </View>
                </View>
                <Card style={styles.card}>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Nama Pengemudi</Text>
                            <Text style={styles.content}>{dataClaim.nama_pengemudi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Alamat Pengemudi</Text>
                            <Text style={styles.content}>{dataClaim.alamat_pengemudi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>SIM Pengemudi</Text>
                            <Text style={styles.content}>{dataClaim.labelHurufSIM} {dataClaim.no_sim_pengemudi}</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Foto SIM</Text>
                            <View style={{ width: width * 0.25, height: width * 0.25 }}>
                                <Image source={{ uri: listDataGambarSim[0].path }} style={{ width: '100%', height: '100%', resizeMode: 'contain', backgroundColor: '#000' }} />
                            </View>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Hubungan antara pengemudi dan tertanggung</Text>
                            <Text style={styles.content}>Dummy</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Tujuan penggunaan kendaraan sebelum hilang</Text>
                            <Text style={styles.content}>Dummy</Text>
                        </View>
                    </CardItem>
                    <CardItem>
                        <View>
                            <Text style={styles.title}>Apakah pengemudi sudah yakin kendaraan dalam keadaan terkunci?</Text>
                            <Text style={styles.content}>Dummy</Text>
                        </View>
                    </CardItem>

                    {this.view_pengemudi_bekerja()}

                </Card>
            </View>
        );
    }

    view_pengemudi_bekerja = () => {
        const { dataClaim } = this.props;
        if (dataClaim.pengemudiChecked == false) {
            return (
                <CardItem>
                    <View>
                        <Text style={styles.title}>Apakah pengemudi bekerja pada tertanggung?</Text>
                        <Text style={styles.content}>Dummy</Text>
                    </View>
                </CardItem>

            );
        } else {
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        dataClaim: state.dataClaim,
        listHalaman: state.listHalaman,
        listDataGambarSim: state.listDataGambarSim,
        listDataGambarSimPihak3: state.listDataGambarSimPihak3,
        listDataGambarStnkPihak3: state.listDataGambarStnkPihak3,
        listDataGambarKtpPihak3: state.listDataGambarKtpPihak3,
        listBengkelClaim: state.listBengkelClaim,
        listDataGambar: state.listDataGambar
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        height: height - 80 - 53,
        width: width,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    card: {
        marginRight: 10,
        marginLeft: 10,
    },
    content: {
        fontWeight: 'bold',
        paddingLeft: 10,
    },
    col: {
        flexDirection: 'column',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    title: {

    },
    viewCard: {
        marginTop: 10,
    },
    viewTitleCard: {
        paddingLeft: 10,
        flexDirection: 'row'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKehilanganTotalKonfirmasi1);
