/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, Alert, KeyboardAvoidingView, TextInput } from 'react-native';
import { Header } from 'react-native-elements';

// import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import DatePicker from 'react-native-datepicker';
// import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
// import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
// import Foundation from 'react-native-vector-icons/Foundation';
// import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
// import DialogAndroid from 'react-native-dialogs';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';
import { Container, Content, Picker, Form, Item, Label, Input, Separator } from "native-base";
import Navigasi from '../kehilangantotal/laporMVKehilanganTotalNavigasi';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

const { width, height } = Dimensions.get('window');

let radio_status_saksi = [
    { label: 'Ya', value: 'Y' },
    { label: 'Tidak', value: 'N' },
]

class LaporMVKehilanganTotalSaksi1 extends Component {

    constructor(props) {
        super(props);
        const { dataClaim } = this.props;

        this.state = {
            status_saksi: dataClaim.status_saksi,
            status_saksiIndex: (dataClaim.status_saksi == 'Y') ? 0 : 1,  
            list_saksi: this.props.dataClaim.list_saksi == null ? [] : this.props.dataClaim.list_saksi,         
        }

        this.onTambahSaksi = this.onTambahSaksi.bind(this);
        this.onKurangSaksi = this.onKurangSaksi.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentDidUpdate() {
        if (this.state.status_saksi == 'Y') {
            let outputSaksi = [];
            const { list_saksi } = this.state;

            if (list_saksi.length == 0) {
                let list_saksi = this.state.list_saksi;
                let count = list_saksi.length + 1;

                // console.log(count);
                list_saksi.push(
                    {
                        telepon: '',
                        jenis: 'S',
                        nama: '',
                        id: count,
                    }
                );

                this.setState({
                    list_saksi: list_saksi
                });
            }
        }
    }

    componentWillUnmount() {

    }

    handleBackPress = () => {
        this.props.navigate('LaporMVKehilanganTotalKepolisian');
        return true;
    }

    onSubmit = () => {
        // ['tempat_lapor_polisi']
        //     .forEach((name) => {
        //         let value = this[name].value();

        //         if (!value) {
        //             errors[name] = 'Tidak boleh kosong';
        //             isError = 1;
        //         }
        //     });

        // this.setState({ errors });
        this.props.navigate('LaporMVKehilanganTotalKronologis');
    }

    onTambahSaksi = () => {
        let list_saksi = this.state.list_saksi;
        let count = list_saksi.length + 1;

        // console.log(count);
        list_saksi.push(
            {
                telepon: '',
                jenis: 'S',
                nama: '',
                id: count,
            }
        );

        this.setState({
            list_saksi: list_saksi
        });
    }

    onKurangSaksi = () => {
        let list_saksi = this.state.list_saksi;

        // console.log(list_saksi);

        list_saksi.pop();
        //
        this.setState({
            list_saksi: list_saksi
        });
    }

    onUbah = () => {

    }

    onChangeTextNamaSaksi(item, text) {
        const { list_saksi } = this.state;
        let items = list_saksi;

        objIndex = list_saksi.findIndex((obj => obj.id == item.id));
        // console.log('obj',objIndex);
        list_saksi[objIndex].nama = text;

        // console.log(list_penumpang_cedera);
        this.setState({
            list_saksi: list_saksi
        })
    }

    onChangeTextTeleponSaksi(item, text) {
        const { list_saksi } = this.state;

        let items = list_saksi;


        objIndex = list_saksi.findIndex((obj => obj.id == item.id));
        // console.log('obj',objIndex);
        list_saksi[objIndex].telepon = text;

        // console.log(list_penumpang_cedera);
        this.setState({
            list_saksi: list_saksi
        })

        // console.log('alamat', this.state.list_penumpang_cedera);
    }

    onPressRadioStatusSaksi(value) {
        this.setState({ status_saksi: value })
    }

    updateRef(name, ref) {
        this[name] = ref;
    }

    render() {
        const { dataClaim } = this.props;
        
        return (
            <Container>
                <View>
                    <Header
                        leftComponent={
                            <TouchableOpacity onPress={() => { this.handleBackPress() }}>
                                <Icon name='md-arrow-round-back' size={25} color='#FFF' />
                            </TouchableOpacity>
                        }
                        centerComponent={{
                            text: 'FORM PELAPORAN',
                            style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                        }}
                        outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                    />
                    <Navigasi />
                    <KeyboardAwareScrollView keyboardDismissMode="interactive"
                        keyboardShouldPersistTaps="always"
                        getTextInputRefs={() => {
                            return [this.nama_pengemudiRef, this.alamat_pengemudiRef, this.no_sim_pengemudiRef, this.kecepatanInsidenRef];
                        }}
                    >
                        <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>
                            <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FF0000' }}>INFORMASI SAKSI</Text>
                            </View>
                            {this.view_pilihanSaksi()}

                            {this.view_saksi()}

                            {this.view_button()}
                        </ScrollView>
                    </KeyboardAwareScrollView>
                </View>
            </Container>
        );
    }

    view_saksi = () => {
        let outputSaksi = [];
        const { list_saksi } = this.state;

        if (this.state.status_saksi == 'Y') {
            if (list_saksi.length != 0) {
                list_saksi.forEach(function (item) {
                    namaSaksi = item.saksi;
                    outputSaksi.push(
                        <View key={item.id}>
                            <View style={styles.row}>
                                <View style={styles.icon}><Icon name='ios-person' size={40} color='rgba(0, 0, 0, .38)' /></View>
                                <View style={styles.field}>
                                    <TextField
                                        label='Nama Saksi'
                                        keyboardType='default'
                                        onChangeText={(text) => { this.onChangeTextNamaSaksi(item, text) }}
                                        returnKeyType='next'
                                        tintColor='#228B22'
                                        maxLength={40}
                                        value={item.nama}
                                    />
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.icon}><FontAwesome name='address-book' size={25} color='rgba(0, 0, 0, .38)' /></View>
                                <View style={styles.field}>
                                    <TextField
                                        label='Telepon Saksi'
                                        keyboardType='phone-pad'
                                        onChangeText={(text) => { this.onChangeTextTeleponSaksi(item, text) }}
                                        returnKeyType='next'
                                        tintColor='#228B22'
                                        maxLength={80}
                                        value={item.alamat}
                                    />
                                </View>
                            </View>
                            <View style={{ borderBottomWidth: 0.5 }}></View>
                        </View>
                    );
                }.bind(this));

            } else {
                console.log('ClaimMVPenumpangSaksi', '2');

            }
            return (
                <View>
                    {outputSaksi}
                    <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
                        <View style={styles.button}>
                            <RkButton onPress={this.onTambahSaksi}>Tambah</RkButton>
                        </View>
                        <View style={styles.button}>
                            <RkButton rkType='danger' onPress={this.onKurangSaksi}>Hapus</RkButton>
                        </View>
                    </View>
                </View>
            );
        }
        else {

            return null;
        }
    }

    view_pilihanSaksi = () => {

        return (
            <View style={styles.row}>
                <View style={styles.text}>
                    <Text>Apakah ada saksi sekitar kejadian kehilangan kendaraan?</Text>
                    <RadioForm
                        formHorizontal={true}
                        animation={true}
                        initial={1}
                    >
                        {radio_status_saksi.map((obj, i) => {
                            var onPress = (value, index) => {
                                this.setState({
                                    status_saksiIndex: index,
                                    status_saksi: value,
                                })
                            }
                            return (
                                <RadioButton labelHorizontal={true} key={i} >
                                    {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                    <RadioButtonInput
                                        obj={obj}
                                        index={i}
                                        isSelected={this.state.status_saksiIndex === i}
                                        onPress={onPress}
                                        buttonStyle={{}}
                                        buttonWrapStyle={{ marginRight: 10 }}
                                    />
                                    <RadioButtonLabel
                                        obj={obj}
                                        index={i}
                                        onPress={onPress}
                                        labelWrapStyle={{ marginRight: (width / 3) - 20 }}
                                    />
                                </RadioButton>
                            )
                        })}
                    </RadioForm>
                </View>
            </View>
        );
    }

    view_button = () => {
        const { dataClaim } = this.props;

        if (dataClaim.visibleNavigation == true) {
            return (
                <View style={styles.button}>
                    <RkButton rkType='danger' onPress={() => { this.onSubmit() }}>Lanjut</RkButton>
                </View>
            );
        } else {
            return (
                <View style={styles.button}>
                    <RkButton rkType='danger' onPress={() => { this.onUbah() }}>Ubah</RkButton>
                </View>
            );
        }

    }
}

function mapStateToProps(state) {
    return {
        dataClaim: state.dataClaim,
        listDataLaporanPolisi: state.listDataLaporanPolisi,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        marginBottom: 10,
        marginTop: 5,
    },
    container: {
        marginTop: 0,
        height: height - 80 - 53,
    },
    containerEdit: {
        marginTop: 0,
        height: height - 80,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    field: {
        flex: 9,
        paddingLeft: 10,
    },
    fieldButton: {
        width: (width * 0.6) - 20,
        height: width * 0.4,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: .5,
    },
    fieldImage: {
        width: width * 0.4,
        height: width * 0.4,
        borderWidth: .5,

    },
    fieldTanggal: {
        flex: 9,
        paddingLeft: 10,
        paddingBottom: 15
    },
    text: {

    },
    textPicker: {
        height: 56,
        fontSize: 16,
        borderBottomWidth: .5,
        borderColor: 'rgba(0,0,0, .38)',
        paddingTop: 20,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKehilanganTotalSaksi1);
