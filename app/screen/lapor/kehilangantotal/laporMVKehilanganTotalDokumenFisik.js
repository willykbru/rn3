/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, BackHandler, Image, ScrollView, ImageBackground, Alert, KeyboardAvoidingView } from 'react-native';

import { Header } from 'react-native-elements';

import ModalFilterPicker from 'react-native-modal-filter-picker';
import { TextField } from 'react-native-material-textfield';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-android-snackbar';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { RkButton, RkType, RkTheme } from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

import Navigasi from './laporMVKehilanganTotalNavigasi';

const { width, height } = Dimensions.get('window');

RkTheme.setType('RkButton', 'button1', {
    container: {
        backgroundColor: '#FF0000',
        width: '60%',
        marginBottom: 16,
    },
    content: {
        color: 'white',
        fontSize: 14,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

class LaporMVKehilanganTotalDokumenFisik1 extends Component {

    constructor(props) {
        super(props);

        this.state = {
            namaLengkap: this.props.detailPolis.nama_tertanggung,
            emailTertanggung: this.props.detailPolis.email_tertanggung,
            noTeleponTertanggung: this.props.detailPolis.phone_tertanggung,
            noPolisi: this.props.detailPolis.no_plat,
            kecepatanInsiden: this.props.dataClaim.kecepatanInsiden,
            deskripsiInsiden: this.props.dataClaim.deskripsiInsiden,
            alamatKejadian: this.props.dataClaim.alamatKejadian,

            visibleTipeInsiden: false,
            pickedTipeInsiden: null,
            labelTipeInsiden: this.props.dataClaim.labelTipeInsiden,

            visibleLokasiProvinsi: false,
            pickedLokasiProvinsi: null,
            labelProvinsiInsiden: this.props.dataClaim.labelProvinsiInsiden,

            visibleLokasiKota: false,
            pickedLokasiKota: null,
            labelKotaInsiden: this.props.dataClaim.labelKotaInsiden,

            tanggalSekarang: new Date(),
            pickedTanggal: this.props.dataClaim.tanggalKejadian,
            isPickedTanggal: false,

            pickedJam: this.props.dataClaim.jamInsiden,
            jamSekarang: new Date().getHours() + ":" + new Date().getMinutes(),
            isPickedJam: false,

        };

    }

    handleBackPress = () => {
        //this.goBack(); // works best when the goBack is async
        const { dataClaim } = this.props;

        if (dataClaim.visibleNavigation == true) {
            this.props.navigate('LaporMVMenu');
        } else {
            temp = {
                visibleNavigation: true,
            }

            this.props.setDataClaimMV(temp);
            this.props.navigate('LaporMVKecelakaanKonfirmasi');
        }

        return true;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        this.props.getProvinsi();
        this.props.getTipeInsiden();

        if (this.props.claimState.provinsiInsiden && this.props.claimState.provinsiInsiden != 'Pilih') {
            this.props.getDistrict(this.props.claimState.provinsiInsiden);
        }
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
                // this.setState({
                //   latitude: position.coords.latitude,
                //   longitude: position.coords.longitude,
                //   error: null,
                //   speed: position.coords.speed,
                // });
                temp = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                    speed: position.coords.speed,
                }
                this.props.setLokasiSekarang(temp);
            },
            (error) => {
                temp = {
                    error: error.message,
                }
                this.props.setLokasiSekarang(temp);
            }, {
                enableHighAccuracy: true,
                timeout: 200000,
                maximumAge: 1000
            },
        );
    }

    componentWillUnmount() {

    }


    onSubmit = () => {
        this.props.navigate('LaporMVKehilanganTotalPengemudi');
    }
    
    onUbah = () => {
        
    }

    view_button = () => {
        const { dataClaim } = this.props;

        if (dataClaim.visibleNavigation == true) {
            return (
                <View style={[styles.button, { marginTop: 10 }]}>

                    <RkButton rkType='danger button1' onPress={() => { this.onSubmit() }}>Ok, saya mengerti</RkButton>

                </View>
            );
        } else {
            return (
                <View style={[styles.button, { marginTop: 10 }]}>

                    <RkButton rkType='danger button1' onPress={() => { this.onUbah() }}>Ok, saya mengerti</RkButton>

                </View>
            )
        }

        // return(
        //   <View style={[styles.button,{marginTop:10}]}>

        //     <RkButton rkType='danger button1' onPress={()=>{this.onSubmit()}}>Lanjut</RkButton>

        //   </View>
        // );
    }

    view_dokumen = () => {
        // tanpa database
        return (
            <View>
                <View style={styles.row}><Text>Surat Tanda Bukti Lapor Kehilangan Kepolisian (Polsek)</Text></View>
                <View style={styles.row}><Text>Asli Surat Keterangan Pemblokiran STNK Ditlantas Polda</Text></View>
                <View style={styles.row}><Text>Asli Surat Keterangan Kehilangan (Polda/Direkrimum)</Text></View>
                <View style={styles.row}><Text>Copy KTP Tertanggung</Text></View>
                <View style={styles.row}><Text>Copy SIM Pengemudi</Text></View>
                <View style={styles.row}><Text>Asli STNK</Text></View>
                <View style={styles.row}><Text>Asli Polis Asuransi</Text></View>
                <View style={styles.row}><Text>Kunci Kontak Asli</Text></View>
                <View style={styles.row}><Text>Kunci Kontak Duplikat</Text></View>
                <View style={styles.row}><Text>Asli BPKB</Text></View>
                <View style={styles.row}><Text>Copy KTP sesuai nama BPKB</Text></View>
                <View style={styles.row}><Text>Buku KIR (khusus kendaraan wajib KIR / kendaraan umum)</Text></View>
            </View>
        );
    }
    
    render() {
        const { dataClaim, listHalaman } = this.props;

        // console.log('detail polis', detailPolis);
        // console.log('data polis', dataPolis);
        console.log('data claim umum', dataClaim);
        // console.log('Detail Polis ', JSON.stringify(detailPolis));

        return (
            <View>
                <Header
                    leftComponent={
                        <TouchableOpacity onPress={() => { this.handleBackPress() }}>
                            <Icon name='md-arrow-round-back' size={25} color='#FFF' />
                        </TouchableOpacity>
                    }
                    centerComponent={{
                        text: 'FORM PELAPORAN',
                        style: { color: '#FFF', fontWeight: 'bold', fontSize: 18, },
                    }}
                    outerContainerStyles={{ backgroundColor: '#FF0000', height: 53 }}
                />
                <KeyboardAwareScrollView keyboardDismissMode="interactive"
                    keyboardShouldPersistTaps="always"
                    getTextInputRefs={() => {
                        return [this.noPolisRef, this.alamatKejadianRef, this.kecepatanInsidenRef, this.deskripsiInsidenRef];
                    }}
                >
                    <Navigasi />
                    <ScrollView style={dataClaim.visibleNavigation == true ? styles.container : styles.containerEdit}>
                        <View style={[styles.row, { justifyContent: 'center', alignItems: 'center' }]}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FF0000' }}>Dokumen Fisik yang dipersiapkan</Text>
                        </View>
                        
                        {this.view_dokumen()}
                        
                        {this.view_button()}
                    </ScrollView>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        marginTop: 0,
        height: hp('100%') - 80 - 53,
    },
    containerEdit: {
        marginBottom: 20,
        marginTop: 0,
        height: hp('100%') - 80,
    },
    row: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        paddingLeft: 10,
        paddingRight: 10,
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    field: {
        flex: 9,
        paddingLeft: 10,
    },
    fieldTanggal: {
        flex: 9,
        paddingLeft: 10,
        paddingBottom: 15
    },
    textPicker: {
        height: 56,
        fontSize: 16,
        color: 'rgba(0,0,0, .87)',
        borderBottomWidth: .5,
        borderColor: 'rgba(0,0,0, .38)',
        paddingTop: 20,
    },
    button: {
        alignItems: 'center',
    },
    navigasi: {
        width: (width - 20) / 7,
        height: (width - 20) / 7,
    }
});


function mapStateToProps(state) {
    return {
        claimState: state.claimState,
        claimLabelState: state.claimLabelState,
        dataClaim: state.dataClaim,
        listProvinsi: state.listProvinsi,
        listDistrict: state.listDistrict,
        // listNoPolisMV : state.listNoPolisMV,
        dataPolis: state.dataPolis, //polis per masing masing orang
        detailPolis: state.detailPolis,
        listTipeInsiden: state.listTipeInsiden,
        listHalaman: state.listHalaman,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LaporMVKehilanganTotalDokumenFisik1);
