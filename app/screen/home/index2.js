import React, { Component } from 'react';
import * as appFunction from '../../function';

import HomePage from './Home';

export default class Home extends Component{

  constructor(props) {
      super(props);
  }

	render() {
    return <HomePage />;
	}
}
