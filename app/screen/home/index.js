import React, { NativeModules, Component } from 'react';
import { BackHandler, Alert, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, StyleSheet, Linking, WebView, Dimensions, ScrollView } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import {Header} from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import styles from '../../assets/styles';
import SplashScreen from 'react-native-smart-splash-screen';
import RNAmplitute from 'react-native-amplitude-analytics';
import MenuPencarian from '../../components/menu/MenuPencarian';
import DeviceInfo from 'react-native-device-info';   
import PopupEvent from '../../components/popup_event';
import ActionButton from 'react-native-action-button';
//Require the module

import {
  RkText,
  RkButton,
  RkStyleSheet
} from 'react-native-ui-kitten';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import * as appFunction from '../../function';

class Home extends React.Component {

  constructor(props) {
    super(props);
  }

  toLob(lob) {
    this.props.setScreen(lob, 'cariasuransi');
    this.props.navigate('CariAsuransi');
  }


 menuItems(){
    var lobmenu = []; // menu lob
    var lobpush = []; // tambah menu lob
    //var lob = ['Travel','Bicycle','MV','Delay','Property','Apartment','Cargo','Health','PA','Life','Pet']; // menu-menu lob yang akan di push
    var lob = ['Travel', 'MV','Property','Apartment', 'PA','Health', 'Life'];
    //var lobtitle = ['Asuransi Kendaraan', 'Asuransi Rumah Tinggal', 'Asuransi Apartemen'];
    //console.log(sisalob + ' '+ roundlob);
    //var {height, width} = Dimensions.get('window');
    let items = <View/>;
    let size = appFunction.width() / 3;
    let emptyCount = this._getEmptyCount(size, lob);
  //  var {height, width} = Dimensions.get('window');

    const imgsrc = {
      'Travel'  : require('../../assets/icons/menu/perjalanan.png'),
      'MV'    : require('../../assets/icons/menu/kendaraan.png'),
      'Bicycle'    : require('../../assets/icons/menu/sepeda.png'),
      'Delay'    : require('../../assets/icons/menu/delay.png'),
      'Property'    : require('../../assets/icons/menu/properti.png'),
      'Apartment'    : require('../../assets/icons/menu/apartemen.png'),
      'Cargo'    : require('../../assets/icons/menu/cargo.png'),
      'Health'    : require('../../assets/icons/menu/kesehatan.png'),
      'PA'    : require('../../assets/icons/menu/kecelakaan.png'),
      'Life'    : require('../../assets/icons/menu/jiwa.png'),
      'Pet'    : require('../../assets/icons/menu/pet.png'),
	  'More' : require('../../assets/icons/menu/more.png'),
    }

    const asname = {
      'Travel'  : 'Perjalanan',
      'MV'    : 'Kendaraan',
      'Bicycle'    : 'Sepeda',
      'Delay'    : 'Delay',
      'Property'    : 'Properti',
      'Apartment'    : 'Apartemen',
      'Cargo'    : 'Cargo',
      'Health'    : 'Kesehatan',
      'PA'    : 'Kecelakaan',
      'Life'    : 'Jiwa',
      'Pet'    : 'Pet',
    }

    var count = 0;
    items = lob.map((route, i)=> {
		var sisalob = lob.length % 3;
		
		if(sisalob === 0)
		  return (
		  
			<View style={[styles.bgWhite]} key={route}>
			  <RkButton rkType='tile'
						style={[appFunction.buttonHomeMenu(size)]}
						onPress={() => {
						  this.toLob(route)
						}}>
						<View style={[appFunction.circle(size)]}>
						  <Image style= {[appFunction.circleImage(size)]} source={imgsrc[route]} />
						</View>
			  </RkButton>
			  <View style={[styles.centerItemContent, styles.width100p]}>
				<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>{asname[route]}</RkText>
			  </View>
			  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
			</View>
		  
		  );
		  else if(sisalob === 1){
			    if(i === lob.length-2)
					return (
		  
					<View style={[styles.bgWhite, styles.centerItemContent]} key={route}>
					  <RkButton rkType='tile'
								style={[appFunction.buttonHomeMenu(size)]}	onPress={() => {
									this.props.navigate('Menu');
								}}>
								<View style={[appFunction.circle(size)]}>
								  <Image style= {[appFunction.circleImageMore(size)]} source={imgsrc['More']} />
								</View>
					  </RkButton>
					  <View style={[styles.centerItemContent, styles.width100p]}>
						<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>More</RkText>
					  </View>
					  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
					</View>
				  
				  ); 
	
				  
				else if(i === lob.length-1)
					return null;
				else 
				    return (
		  
					<View style={[styles.bgWhite]} key={route}>
					  <RkButton rkType='tile'
								style={[appFunction.buttonHomeMenu(size)]}
								onPress={() => {
								  this.toLob(route)
								}}>
								<View style={[appFunction.circle(size)]}>
								  <Image style= {[appFunction.circleImage(size)]} source={imgsrc[route]} />
								</View>
					  </RkButton>
					  <View style={[styles.centerItemContent, styles.width100p]}>
						<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>{asname[route]}</RkText>
					  </View>
					  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
					</View>
				  
				  ); 					
									
			}  
		  else if(sisalob === 2){
			    if(i === lob.length-3)
					return (
		  
					<View style={[styles.bgWhite]} key={route}>
					  <RkButton rkType='tile'
								style={[appFunction.buttonHomeMenu(size)]}
								onPress={() => {
								  this.props.navigate('Menu');
								}}>
								<View style={[appFunction.circle(size)]}>
								  <Image style= {[appFunction.circleImageMore(size)]} source={imgsrc['More']} />
								</View>
					  </RkButton>
					  <View style={[styles.centerItemContent, styles.width100p]}>
						<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>More</RkText>
					  </View>
					  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
					</View>
				  
				  ); 
	
				  
				else if(i === lob.length-1 || i === lob.length-2)
					return null;
				else 
				    return (
		  
					<View style={[styles.bgWhite]} key={route}>
					  <RkButton rkType='tile'
								style={[appFunction.buttonHomeMenu(size)]}
								onPress={() => {
								  this.toLob(route)
								}}>
								<View style={[appFunction.circle(size)]}>
								  <Image style= {[appFunction.circleImage(size)]} source={imgsrc[route]} />
								</View>
					  </RkButton>
					  <View style={[styles.centerItemContent, styles.width100p]}>
						<RkText rkType='small' style={[styles.colorGrey, styles.fontContinuum, styles.font14]}>{asname[route]}</RkText>
					  </View>
					  <View style={[styles.borderbottom1, styles.top10, styles.borderColorBotSilver]}/>
					</View>
				  
				  ); 					
									
			
			}
		
		    });
	

    return items;


  }

  _getEmptyCount(size, menu) {
    let rowCount = Math.ceil((appFunction.height() - 20) / size);
    return rowCount * 3 - menu.length;
  };


  componentDidMount(){
	const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	amplitude.logEvent('Halaman Home');
	var userID = this.props.status.isLogin ? this.props.session.visitor_id : '';
	
	appFunction.setScreenGA('Halaman Home', userID);	
  }

render () {
  //console.log(this.menuItems().length);
  const images = [
      'https://www.jagain.com/assets/img-mobile/load.gif'
    ];
	
  const sliderImage = this.props.sliderImageLink;
	var {height, width} = Dimensions.get('window');

  return(
    <ScrollView
      contentContainerStyle={[styles.rootContainer, styles.centerContent]}>
         <View style={[styles.radius3]}>
         <View style={styles2.container}>
			 {sliderImage.length == 0 ? <ImageSlider
          autoPlayWithInterval={5000}
          images={images}
          onPress={({ index }) => alert(index)}
          customSlide={({ index, item, style, width }) => (
            // It's important to put style here because it's got offset inside
            <View
              key={index}
              style={[
                style,
                styles2.customSlide,
              ]}
            >
              <Image source={{ uri: item }} style={styles2.customImage} />
            </View>
          )}
          customButtons={(position, move) => (
            <View style={styles2.buttons}>
					<Text style={[styles.colorWhite, styles.baseText]}>
                      {position + 1}/1
                    </Text>
            </View>
          )}
        />:
		<ImageSlider
          autoPlayWithInterval={5000}
          images={sliderImage}
          onPress={({ index }) => alert(index)}
          customSlide={({ index, item, style, width }) => (
            // It's important to put style here because it's got offset inside
            <View
              key={index}
              style={[
                style,
                styles2.customSlide,
              ]}
            >
              <Image source={{ uri: item }} style={styles2.customImage} />
            </View>
          )}
          customButtons={(position, move) => (
            <View style={styles2.buttons}>
					<Text style={[styles.colorWhite, styles.baseText]}>
                      {position + 1}/{sliderImage.length}
                    </Text>
            </View>
          )}
        />
			 }
     
      </View>
        </View>
			<PopupEvent />
			
		 <View style={[styles.bgWhite, styles.width96p, styles.directionRow, styles.radius3, styles.top5]} >
		 </View>
		 {this.menuItems()}
		 
		  <View style={[styles.width100p, styles.bgWhite, styles.top5, styles.centerItemContent]}>
		 <MenuPencarian title='List Pencarian'> 
			<View style={[styles.width100p, styles.directionRow, styles.centerItemContent]}>
				<View style={[{width:width/2}, styles.centerItemContent]}>
					<TouchableOpacity style={[{borderWidth: .5, borderRadius: 30, borderColor: '#ff0000'}]} onPress={()=>{this.props.navigate('BengkelRekanan');this.props.setDataSearch(Object.assign({},{insurance_id: 'all'}))}} >
					<View style={[{width:width/2.2}, styles.centerItemContent]}>
						<Text style={[{padding: 10}, styles.baseText]}>Bengkel Rekanan</Text>
						</View> 
					</TouchableOpacity>
				</View>
				<View style={[{width:width/2}, styles.centerItemContent]}>
				<TouchableOpacity style={[{borderWidth: .5, borderRadius: 30, borderColor: '#ff0000'}]} onPress={()=>{this.props.navigate('RumahSakitProvider');}}>
					<View style={[{width:width/2.2}, styles.centerItemContent]}>
						<Text style={[{padding: 10}, styles.baseText]}>Rumah Sakit</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		 </MenuPencarian>
		 </View>
		  
		 
		<View style={[styles.width100p, styles.bgWhite, styles.top5, styles.centerItemContent]}>
		 
		  
		<View style={[styles.width100p, styles.top5, styles.bottom5]} />
		<View style={[styles.width100p, styles.radius5]}>
		 <WebView
			ref={(view) => this.webView = view}
			source={{uri: 'https://blog.jagain.com/last-post.php'}}
			automaticallyAdjustContentInsets={true}
            style={{backgroundColor: 'rgba(255, 255, 255, 0)', height: 170, elevation:4}}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            decelerationRate="normal"
            startInLoadingState={true}
			onNavigationStateChange={(event) => {
			  if (event.url !== 'https://blog.jagain.com/last-post.php' &&
				event.url !== 'https://blog.jagain.com/last-post-2.php' && 
				event.url !== 'https://blog.jagain.com/last-post-3.php'){
				this.webView.stopLoading();
				Linking.openURL(event.url);
			  }
			}}
		  />
		  
		</View>
		
		<View style={[styles.width100p, styles.top5, styles.bottom5]} />
		<View style={[styles.width100p, styles.radius5, styles.bottom10]}>
		 <WebView
			ref={(view) => this.webView2 = view}
			source={{uri: 'https://blog.jagain.com/last-post-2.php'}}
			automaticallyAdjustContentInsets={true}
            style={{backgroundColor: 'rgba(255, 255, 255, 0)', height: 170, elevation:4}}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            decelerationRate="normal"
            startInLoadingState={true}
			onNavigationStateChange={(event) => {
			  if (event.url !== 'https://blog.jagain.com/last-post.php' &&
				event.url !== 'https://blog.jagain.com/last-post-2.php' && 
				event.url !== 'https://blog.jagain.com/last-post-3.php') {
				this.webView2.stopLoading();
				Linking.openURL(event.url);
			  }
			}}
		  />
		  
	
		</View>
		<View style={[styles.top10]} />
		<View style={[styles.centerItemContent]}>
		<Text style={[{fontSize: 10}, styles.italic]}>{DeviceInfo.getVersion()}</Text>
		</View>
		</View>
			
    </ScrollView>
  )
  //end of return
}
//end of render
}
// end of class
const styles2 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  slider: { backgroundColor: '#000', height: 350 },
  
  contentText: { color: '#fff' },
  buttons: {
    zIndex: 1,
    height: 15,
    marginTop: -25,
    marginBottom: 10,
	width: '10%',
	borderRadius: 5, 
	marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    flexDirection: 'row',
	},
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	},
  button: {
    margin: 3,
    width: 15,
    height: 15,
    opacity: 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSelected: {
    opacity: 1,
    color: 'red',
  },
  customSlide: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  customImage: {
    height: Dimensions.get('window').width * (4 / 9),
    width: Dimensions.get('window').width,
  },
});

function mapStateToProps(state) {
  return {
    navReducer: state.navReducer,
    dataToken: state.dataToken,
    dataSession: state.dataSession,
	session: state.session,
	status: state.status,
	sliderImageLink: state.sliderImageLink,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
