import React, { Component } from 'react';
import { Linking, ToastAndroid, Modal, NetInfo, BackHandler, Alert, Platform, TouchableWithoutFeedback, Text, View, Image, TouchableHighlight, StatusBar, TouchableOpacity, AsyncStorage, Dimensions, ScrollView, Button as Button2 } from 'react-native';
import {Header} from 'react-native-elements';
import { Footer, FooterTab, Badge, Container, Button, Content, Body, Title, Left, Right, Card, CardItem, Icon } from 'native-base';
import Home from '../home';
import User from '../user';
import PendingPayment from '../dashboard/PendingPayment';
import ListDataPolis from '../dashboard/ListDataPolis';
import styles from '../../assets/styles';
import SplashScreen from 'react-native-smart-splash-screen';
import PushNotification from 'react-native-push-notification';
import PushController from '../../components/push_notification/PushController'; //The push controller created earlier
import RNAmplitute from 'react-native-amplitude-analytics';
import DeviceInfo from 'react-native-device-info';
import OfflineNotice from '../../components/offline_notice';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import ChatModule from '../../components/popup_event/chat';

import * as appFunction from '../../function';


class Main extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentScreen:'',
      notificationStatus: false,
      chatvisibility: false,
    };
	  // this.handleAppStateChange = this.handleAppStateChange.bind(this);
    // this.sendNotification = this.sendNotification.bind(this);
  }
  
  setModalVisible(value) {
    //this.setState({modalVisible: visible});
	this.props.setModalUpdateStatus(value);
  }

  sendNotification(){
    console.log('send notif');
  }
  

  titleScreen(currentScreen){
    if(currentScreen == 'Home'){
      return 'Home'.toUpperCase();
    }else if(currentScreen == 'User'){
      return 'User'.toUpperCase();
    }else if(currentScreen == 'MyCart'){
      return 'My Cart'.toUpperCase();
    }else if(currentScreen == 'More'){
      return 'More'.toUpperCase();
    }else if(currentScreen == 'ListDataPolis'){
      return 'My Polis'.toUpperCase();
    }
  }

  activeContent(currentScreen){
    if(currentScreen == 'Home'){
      return <Home />;
    }
    if(currentScreen == 'User'){
      return <User />;
    }
    if(currentScreen == 'MyCart')
    {
      return <PendingPayment />
    }
	if(currentScreen == 'ListDataPolis')
    {
      return <ListDataPolis />
    }
  }

  activeFooter(currentScreen, isLogin) {
    let badgeCount = this.props.totalPendingCart ? this.props.totalPendingCart : 0; // jumlah notifikasi yang belum dibaca
    if (currentScreen == 'Home') {
      return (
        <Footer>
          <FooterTab style={[styles.bgWhite]}>
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toHome()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon active name="ios-home" style={[styles.colorRed]} />
                <Text style={[styles.colorRed, styles.baseText]}>Home</Text>
              </TouchableOpacity>
            </Button>
            {isLogin ? badgeCount > 0 ? <Button badge transparent vertical>
              <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Badge style={[styles.centerItemContent, { backgroundColor: '#4D4DFF', width: 20, transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }]}><Text style={{ color: 'white' }}>{badgeCount}</Text></Badge>
                <Icon name="ios-cart" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
              </TouchableOpacity>
            </Button> :
              <Button transparent vertical>
                <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                  <Icon name="ios-cart" style={[styles.colorGrey]} />
                  <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
                </TouchableOpacity>
              </Button>
              : null}
            {isLogin ? <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toListDataPolis()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="md-bookmarks" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Polis</Text>
              </TouchableOpacity>
            </Button> : null}
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toUser()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="person" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>User</Text>
              </TouchableOpacity>
            </Button>
          </FooterTab>
        </Footer>
      );
    } else if (currentScreen == 'User') {
      return (
        <Footer>
          <FooterTab style={[styles.bgWhite]}>
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toHome()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="ios-home" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>Home</Text>
              </TouchableOpacity>
            </Button>
            {isLogin ? badgeCount > 0 ? <Button badge transparent vertical>
              <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Badge style={[styles.centerItemContent, { backgroundColor: '#4D4DFF', width: 20, transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }]}><Text style={{ color: 'white' }}>{badgeCount}</Text></Badge>
                <Icon name="ios-cart" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
              </TouchableOpacity>
            </Button> :
              <Button transparent vertical>
                <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                  <Icon name="ios-cart" style={[styles.colorGrey]} />
                  <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
                </TouchableOpacity>
              </Button>
              : null}
            {isLogin ? <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toListDataPolis()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="md-bookmarks" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Polis</Text>
              </TouchableOpacity>
            </Button> : null}
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toUser()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon active name="person" style={[styles.colorRed]} />
                <Text style={[styles.colorRed, styles.baseText]}>User</Text>
              </TouchableOpacity>
            </Button>
          </FooterTab>
        </Footer>
      );
    } else if (currentScreen == 'MyCart') {
      return (
        <Footer>
          <FooterTab style={[styles.bgWhite]}>
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toHome()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="ios-home" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>Home</Text>
              </TouchableOpacity>
            </Button>
            {isLogin ? badgeCount > 0 ? <Button badge transparent vertical>
              <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Badge style={[styles.centerItemContent, { backgroundColor: '#4D4DFF', width: 20, transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }]}><Text style={{ color: 'white' }}>{badgeCount}</Text></Badge>
                <Icon name="ios-cart" style={[styles.colorRed]} />
                <Text style={[styles.colorRed, styles.baseText]}>My Cart</Text>
              </TouchableOpacity>
            </Button> :
              <Button transparent vertical>
                <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                  <Icon name="ios-cart" style={[styles.colorRed]} />
                  <Text style={[styles.colorRed, styles.baseText]}>My Cart</Text>
                </TouchableOpacity>
              </Button>
              : null}
            {isLogin ? <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toListDataPolis()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="md-bookmarks" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Polis</Text>
              </TouchableOpacity>
            </Button> : null}
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toUser()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="person" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>User</Text>
              </TouchableOpacity>
            </Button>
          </FooterTab>
        </Footer>
      );
    } else if (currentScreen == 'ListDataPolis') {
      return (
        <Footer>
          <FooterTab style={[styles.bgWhite]}>
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toHome()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="ios-home" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>Home</Text>
              </TouchableOpacity>
            </Button>
            {isLogin ? badgeCount > 0 ? <Button badge transparent vertical>
              <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Badge style={[styles.centerItemContent, { backgroundColor: '#4D4DFF', width: 20, transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }]}><Text style={{ color: 'white' }}>{badgeCount}</Text></Badge>
                <Icon name="ios-cart" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
              </TouchableOpacity>
            </Button> :
              <Button transparent vertical>
                <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                  <Icon name="ios-cart" style={[styles.colorGrey]} />
                  <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
                </TouchableOpacity>
              </Button>
              : null}
            {isLogin ? <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toListDataPolis()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon active name="md-bookmarks" style={[styles.colorRed]} />
                <Text style={[styles.colorRed, styles.baseText]}>My Polis</Text>
              </TouchableOpacity>
            </Button> : null}
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toUser()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="person" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>User</Text>
              </TouchableOpacity>
            </Button>
          </FooterTab>
        </Footer>
      );
    } else if (currentScreen == 'More') {
      return (
        <Footer>
          <FooterTab style={[styles.bgWhite]}>
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toHome()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="ios-home" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>Home</Text>
              </TouchableOpacity>
            </Button>
            {isLogin ? badgeCount > 0 ? <Button badge transparent vertical>
              <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Badge style={[styles.centerItemContent, { backgroundColor: '#4D4DFF', width: 20, transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }]}><Text style={{ color: 'white' }}>{badgeCount}</Text></Badge>
                <Icon name="ios-cart" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
              </TouchableOpacity>
            </Button> :
              <Button transparent vertical>
                <TouchableOpacity onPress={() => { this.toMyCart(); if (this.props.status.notificationcart == false) { this.sendNotification() } }} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                  <Icon name="ios-cart" style={[styles.colorGrey]} />
                  <Text style={[styles.colorGrey, styles.baseText]}>My Cart</Text>
                </TouchableOpacity>
              </Button>
              : null}
            {isLogin ? <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toListDataPolis()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="md-bookmarks" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>My Polis</Text>
              </TouchableOpacity>
            </Button> : null}
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toMore()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon active name="ios-more" style={[styles.colorRed]} />
                <Text style={[styles.colorRed, styles.baseText]}>More</Text>
              </TouchableOpacity>
            </Button>
            <Button transparent vertical>
              <TouchableOpacity onPress={() => this.toUser()} underlayColor="#ff9999" style={[styles.centerItemContent]}>
                <Icon name="person" style={[styles.colorGrey]} />
                <Text style={[styles.colorGrey, styles.baseText]}>User</Text>
              </TouchableOpacity>
            </Button>
          </FooterTab>
        </Footer>
      );
    }
  }

  toHome(){
    this.toHomeScreen();
  }

  toUser(){
    this.toUserScreen();
  }

  toMyCart(){
    this.toMyCartScreen();
  }

  toMore(){
    this.toMoreScreen();
  }
  
  toListDataPolis(){
    this.toListDataPolisScreen();
  }

  toHomeScreen(screen = 'Home'){
    this.props.setScreen(screen);
  }

  toUserScreen(screen = 'User'){
    this.props.setScreen(screen);
  }

  toMyCartScreen(screen = 'MyCart'){
	this.props.setStatusNotifCart(true);
    this.props.setScreen(screen);
  }
  
  toListDataPolisScreen(screen = 'ListDataPolis'){
    this.props.setScreen(screen);
  }

  toMoreScreen(screen = 'More'){
    this.props.setScreen(screen);
  }
  
  alertUpdate(){
	  	  Alert.alert(
			  'Update Versi Aplikasi Jagain',
			  'Update Versi Jagain Anda untuk Mendapatkan Fitur Terbaru.',
			  [
				{text: 'OK', onPress: () => {BackHandler.exitApp();Linking.openURL('https://play.google.com/store/apps/details?id=com.jagain');}},
			  ],
			  { cancelable: false }
			)
  }
  
  
  // _handleConnectionChange = (isConnected) => {
    // if(!isConnected)
		// Alert.alert('Oppsss... Koneksi Hilang', 'Beberapa fitur membutuhkan koneksi internet agar dapat berfungsi dengan baik');
  // };

  componentDidMount(){
    

    
    if (!this.props.status.isLogin && !this.props.status.notificationrenewal) {
      console.log('melakukan login');
      this.props.setScreen('Home');
      this.props.getTokenLocalStorage().done();
    }else{

      this.props.countPendingCart(this.props.session.username);
    }
    
	  this.props.setScreen('', 'renewal');
	  this.props.getSlider();
    appFunction.checkWritePermission();
    appFunction.checkNotifPermission();
	//appFunction.createChannelNotif();
    this.props.getServerDate();

    //this.toListDataPolis()
   

	
    //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
    SplashScreen.close({
        animationType: SplashScreen.animationType.scale,
        duration: 850,
        delay: 500,
    })
	
	const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
	const version = DeviceInfo.getVersion();
	
	 // log an event
	if(this.props.status.notificationcart == false){
		amplitude.logEvent('Launching Aplikasi');
	}else{
		amplitude.logEvent('Main Screen');
	}
	
  //NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
   

	//const version = 'v1.2018.05.24.01';
	//this.props.checkUpdateVersion('v1.2018.06.29.02');
	this.props.checkUpdateVersion(version);
 
	//console.log(Platform.OS);
  //AsyncStorage.removeItem('STORAGE');
    this.props.resetStatusRegister();
	this.props.resetListAsuransi();
	this.props.resetDataTransaksi();
	this.props.resetDataVoucher();
    this.props.fetchPopup('http://api.jagain.com:8080/getpopup');
	//AppState.addEventListener('change', this.handleAppStateChange);
	
  }
  //handle double back to exit
  async componentWillMount() {  
    OneSignal.setLogLevel(7, 0);

    let requiresConsent = false;

    this.setState({
      emailEnabled: false,
      animatingEmailButton: false,
      initialOpenFromPush: "Did NOT open from push",
      activityWidth: 0,
      width: 0,
      activityMargin: 0,
      buttonColor: Platform.OS == "ios" ? "#ffffff" : "#d45653",
      jsonDebugText: "",
      privacyButtonTitle: "Privacy Consent: Not Granted",
      requirePrivacyConsent: requiresConsent
    });

    OneSignal.setRequiresUserPrivacyConsent(requiresConsent);

    OneSignal.init("0526971b-920f-4d42-b154-ad31feda6d61", { kOSSettingsKeyAutoPrompt: true });

    var providedConsent = await OneSignal.userProvidedPrivacyConsent();

    this.setState({ privacyButtonTitle: `Privacy Consent: ${providedConsent ? "Granted" : "Not Granted"}`, privacyGranted: providedConsent });

    OneSignal.setLocationShared(true);

    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', this.onReceived.bind(this));
    OneSignal.addEventListener('ids', this.onIds.bind(this));
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    

    BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);
  }



  async onOpened(openResult) {
    // ScreenName is the name of the screen you defined in StackNavigator
    
    if (openResult.notification.payload.title.includes('Renewal') || openResult.notification.payload.title.includes('renewal')){
      await this.props.getTokenLocalStorage(true).done();
      //this.toListDataPolis();
      this.props.setStatusNotifRenewal(true);
    }
    console.log('Test Buka My Polis', 'ListDataPolis')
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

   onReceived(notification) {
        console.log("Notification received: ", notification);
        this.setState({notificationStatus: true});
    }

    // onOpened(openResult) {
		
    //   console.log('Message: ', openResult.notification.payload.body);
    //   console.log('Data: ', openResult.notification.payload.additionalData);
    //   console.log('isActive: ', openResult.notification.isAppInFocus);
    //   console.log('openResult: ', openResult);
    // }

     onIds(device) {
       console.log('Device info: ', device);
       this.setState({ notificationStatus: true });
     }

  componentWillUnmount(){
	//AppState.removeEventListener('change', this.handleAppStateChange);
	//NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
   // BackHandler.removeEventListener('hardwareBackPress');
    //handle double back to exit
    OneSignal.removeEventListener('received', this.onReceived.bind(this));
    //       OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('opened', this.onOpened.bind(this));
	
   BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
    clearTimeout(this.timer.ref);
    this.timer = {
      ref: null,
      isTimerRunning: false
    };
  }
   //handle double back to exit
  timer = {
    ref: null,
    isTimerRunning: false
  }
   //handle double back to exit
  _handleExit = () => {
    if (!this.timer.isTimerRunning) {
      this.timer.isTimerRunning = true;
      const backInterval = 3000; 
      clearTimeout(this.timer.ref);
      this.timer.ref = setTimeout(() => this.timer.isTimerRunning = false, backInterval);
      ToastAndroid.show('Tekan Tombol Back Sekali Lagi Untuk Keluar Dari Aplikasi', ToastAndroid.SHORT);
      return true; // don't do anything
    }
    return BackHandler.exitApp();
  } 
 //handle double back to exit
  _handleBackPress = () => {
    return this._handleExit();  
  }

  viewCart() {
    //Alert.alert('tekan notifikasi');
    this.props.navigate('MyCart');
  }  
  
  componentWillReceiveProps(NextProps){
	  if(this.props.status.isLogin != NextProps.status.isLogin){
		  if(NextProps.status.isLogin){
        this.props.getSuccessPayment(NextProps.session.username);
        this.props.countPendingCart(NextProps.session.username);
		  }
    }
    
    if (this.props.listPendingCart != NextProps.listPendingCart) {
      if (NextProps.status.isLogin) {
        this.props.countPendingCart(NextProps.session.username);
      }
    }
		  // if(this.props.listSuccessPayment != NextProps.listSuccessPayment){
			  // if(!NextProps.status.notificationrenewal){
			  // var i;
			  // var message = '';
			  // var now = new Date(this.props.status.serverdate);
			// var endpolis = null; 
			// var deadlinepolis = null;
			// PushNotification.cancelAllLocalNotifications();
			// for (i = 0; i < NextProps.listSuccessPayment.length; i++){
				 // if (NextProps.listSuccessPayment[i].policy_enddate && NextProps.listSuccessPayment[i].kode_transaksi.includes("AC")){
					 // deadlinepolis = new Date(NextProps.listSuccessPayment[i].policy_enddate);
					 // deadlinepolis.setDate(deadlinepolis.getDate() - 45) 
					 
					 // endpolis = new Date(NextProps.listSuccessPayment[i].policy_enddate);
					 
					// if((endpolis >= now && now >= deadlinepolis)){
						// message = 'Masa Berlaku Polis dengan Nomor Polis: '+ NextProps.listSuccessPayment[i].policyno +' akan Habis Pada Tanggal '+ appFunction.formatIndoDate(endpolis) +'. Segera Lakukan Renewal Polis pada Menu My Polis.';
					
					 // this.handleAppStateChange(message, endpolis);
					// }
				 // }
			// } 
			// this.props.setStatusNotifRenewal(true);
		  // }
	  // }
  }
 



  
  
render () {
  const {mainScreen} = this.props.screen;
  const {isLogin} = this.props.status;
   //console.log('main');
  console.log('notificationStatus', this.state.notificationStatus);
  
  if(this.props.status.updmodalstatus){
	  //this.alertUpdate();
  }
  
  const { height} = Dimensions.get('window');


  return(
    <Container>	
      {
        mainScreen == 'Home' ?
          <Header
            backgroundColor={'white'}
            centerComponent={<Image style={[{ width: 60, height: 45 }]} source={require('../../assets/icons/logo/logo-big.png')} />}
          />
          : 
            <Header
              backgroundColor={'white'}
              centerComponent={{ text: this.titleScreen(mainScreen), style: { color: 'red', padding: 10 } }}
            /> 
      }
     
	  <View>
        <PushController />
      </View>
	  <OfflineNotice />
        
 
          <Content>
            {this.activeContent(mainScreen)}
          </Content>
        
      
        <ChatModule />
        <TouchableOpacity
          style={{
            borderWidth: 1,
            borderColor: 'rgba(255,0,0,0.7)',
            justifyContent: 'center',
            width: 70,
            position: 'absolute',
            bottom: height * .09,
            right: -height * .05,
            height: 70,
            backgroundColor: '#fff',
            borderRadius: 100,
          }}
          onPress={() => { this.props.setStatusChatModal(true) }}
        >
          <Icon name="chatboxes" size={30} style={{ left: 5, color: 'rgba(255,0,0,0.9)' }} />
        </TouchableOpacity>
      {this.activeFooter(mainScreen, isLogin)}
	  
    </Container>
  )
  //end of return
}
//end of render
}
// end of class;
function mapStateToProps(state) {
  return {
    screen: state.screen,
    status: state.status,
    session: state.session,
    listSuccessPayment: state.listSuccessPayment,
  itemsPopup: state.itemsPopup,
    totalPendingCart: state.totalPendingCart,
    listPendingCart: state.listPendingCart,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
