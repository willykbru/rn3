import React, { Component } from 'react';
import {
  View,
  Animated
} from 'react-native';

// const styles = ...

export default class Slide extends Component {

  state = {
    //visible: false,
    x: new Animated.Value(-100),
  };
/*
  slide = () => {
    Animated.spring(this.state.x, {
      toValue: 0,
    }).start();
    this.setState({
      visible: true,
    });
  };
  */
  componentDidMount() {
    Animated.timing(                  // Animate over time
      this.state.x,            // The animated value to drive
      {
        toValue: 0,                   // Animate to opacity: 1 (opaque)
        duration: 200,              // Make it take a while
      }
    ).start();                        // Starts the animation
  }

   componentWillUnmount() {
    Animated.timing(                  // Animate over time
      this.state.x,            // The animated value to drive
      {
        toValue: -100,                   // Animate to opacity: 1 (opaque)
        duration: 10,              // Make it take a while
      }
    ).start();                        // Starts the animation
  }

  render() {
    // in practice you wanna toggle this.slide() after some props validation, I hope
    //this.slide();
    return (
        <Animated.View
			style={[{height:'100%'},
				{...this.props.style}, {
            transform: [
              {
                translateX: this.state.x
              }
            ]
          }]}
        >
          {this.props.children}
        </Animated.View>
    );
  }
}
