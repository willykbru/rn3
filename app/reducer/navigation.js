import MainStack from '../navigator/MainStack';

const initialState = MainStack.router.getStateForAction(MainStack.router.getActionForPathAndParams('Main'));

export const navReducer = (state = initialState, action) => {
  const nextState = MainStack.router.getStateForAction(action, state);
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
  // return state;
};
/*
export const navReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'JUMP_TO_TAB':
      return { ...state, index: 0 };
    case 'Navigation/BACK':
      return state;
    case 'Navigation/RESET':
      return MainStack.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          key: 'MainStack',
          actions: [NavigationActions.navigate({ routeName: 'EventsStackNavigation' })],
        }),
        state,
      );
    default:
      return MainStack.router.getStateForAction(action, state) || state;
  }
};

export const bottomTabBarReducer = (state, action) => {
  switch (action.type) {
    case 'JUMP_TO_TAB':
      return { ...state, index: 0 };
    case 'Navigation/BACK':
      return state;
    case 'Navigation/RESET':
      return BottomTabBar.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          key: 'BottomTabBar',
          actions: [NavigationActions.navigate({ routeName: 'EventsStackNavigation' })],
        }),
        state,
      );
    default:
      return BottomTabBar.router.getStateForAction(action, state) || state;
  }
};
*/
