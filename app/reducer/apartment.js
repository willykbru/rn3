const initialDataApartment= {
  alamat_pemilik: '',
  kode_voucher: '',
  mobile_apps: 'Y',
  nama_pemilik: '',
  building_content: '',
  namabankleasing: '',
  ktp: '',
  photo_belakang: '',
  photo_depan: '',
  photo_kanan: '',
  photo_kiri: '',
  photo_ktp: '',
  statusqq: 'N',
  risk_address: '',
  tgl_lahir: '',
  tittle: '',
  visitor_id: '',
}

const initialApartmentState = {
  building_tsi: 0,
  content_tsi: 0,
  district_id: '',
  eqvet: true,
  flood: true,
  kecamatan_id: '',
  kelurahan_id: '',
  lantai: 0,
  package_id: 0,
  province_id: '',
  tipe: 'apartemen',
}

const initialApartmentLabelState = {
    label_lantai: 'Pilih',
    label_provinsi: 'Pilih',
    label_district:'Pilih',
    label_kecamatan:'Pilih',
    label_kelurahan:'Pilih',
}

export function dataApartment(state= initialDataApartment , action){
  switch (action.type) {
      case 'SET_DATA_APARTMENT':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_APARTMENT':
          return state;
      default:
          return state;
  }
}

export function asuransiApartment(state = {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_APARTMENT':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function apartmentState(state = initialApartmentState, action) {
    switch (action.type) {
        case 'SET_APARTMENT_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

export function apartmentLabelState(state = initialApartmentLabelState, action) {
    switch (action.type) {
        case 'SET_APARTMENT_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
