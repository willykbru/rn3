const initialDataDelay =  {
    birth_date: null,
	city_destination: 'Pilih',
	city_origin: 'Pilih',
	country_destination: 'Pilih',
    district_id: 'Pilih',
    end_date: null,
    first_name: '',
    kode_booking: '',
    kode_voucher: '',
    last_name: '',
    member_ahliwaris: '',
    member_alamat: '',
    member_email: '',
    member_gender: 'L',
    member_hp: '',
    member_hubahliwaris: 'Pilih',
    member_idno: '',
    member_kitas: 'wni',
    member_phone: '',
    mobile_apps: 'Y',
    no_ktp: '',
    photo_ktp: '',
    province_id: 'Pilih',
    start_date: null,
    tempat_lahir: '',
    tittle: 'Pilih',
    travel_type: 'Pilih',
    visitor_id: '',
  }


const initialDelayState = {
  package_id: 0,
  type: 'Individu'
}

const initialOtDelayState = {
  maskapai: '',
  perjalanan: ''
}


const initialDelayLabelState = {
	label_city_destination:'',
	label_city_origin:'',
	label_country_destination:'',
    label_provinsi:'',
    label_district:'',
}

export function dataDelay(state= initialDataDelay , action){
  switch (action.type) {
      case 'SET_DATA_DELAY':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function asuransiDelay(state = {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_DELAY':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function delayLabelState(state = initialDelayLabelState, action) {
    switch (action.type) {
        case 'SET_DELAY_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
export function delayState(state = initialDelayState, action) {
    switch (action.type) {
        case 'SET_DELAY_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

export function otDelayState(state = initialOtDelayState, action) {
    switch (action.type) {
        case 'SET_OT_DELAY_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
