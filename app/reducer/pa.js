const initialDataAccident =  {
    alamat: '',
    district_id: 'Pilih',
    file_identitas: '',
    gender: 'L',
    hub_ahliwaris: 'Pilih',
    kewarganegaraan: 'wni',
    kode_voucher: '',
    mobile_apps: 'Y',
    nama: '',
    nama_ahliwaris: '',
    no_identitas: '',
    province_id: 'Pilih',
    tgl_lahir: '',
    tittle: '',
    visitor_id: '',
	tempat_lahir: '',
  }


const initialAccidentState = {
  package_id: 0,
  pekerjaan: '',
  tgl_lahir: ''
}


const initialAccidentLabelState = {
    label_pekerjaan: '',
    label_provinsi:'',
    label_district:'',
}

export function dataAccident(state= initialDataAccident , action){
  switch (action.type) {
      case 'SET_DATA_ACCIDENT':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_ACCIDENT':
          return state;
      default:
          return state;
  }
}

export function asuransiAccident(state = {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_ACCIDENT':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function accidentLabelState(state = initialAccidentLabelState, action) {
    switch (action.type) {
        case 'SET_ACCIDENT_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
export function accidentState(state = initialAccidentState, action) {
    switch (action.type) {
        case 'SET_ACCIDENT_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
