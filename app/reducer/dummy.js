/* tambah list peserta */
const initialListPeserta = [{
    id: 1,
    email_login: 'adrian.wijaya@kbru.co.id',
    name: 'Adrian Prasetya Wijaya',
    address: 'Jalan U No.9A Palmerah Jakarta Barat',
    phone: '081315556594',
    file_identitas: null,
    email: 'adrian.wijaya@kbru.co.id',
    id_card_type: '01',
    no_identitas: '1871022603940008',
    tempat_lahir: 'Bandar Lampung',
    tanggal_lahir: '1994-03-26',
    jenis_kelamin: 'L',
    kewarganegaraan: 'wni',
    negara_asal: null,
    status_profile: '1',
}, {
        id: 2,
        email_login: 'adrian.wijaya@kbru.co.id',
        name: 'Willy',
        address: 'Sumur Batu, Jakarta Pusat',
        phone: '0897147277',
        file_identitas: null,
        email: 'willy@kbru.co.id',
        id_card_type: '01',
        no_identitas: '1234567890',
        tempat_lahir: 'Palembang',
        tanggal_lahir: '1986-05-24',
        jenis_kelamin: 'L',
        kewarganegaraan: 'wni',
        negara_asal: null,
        status_profile: '2',
    }, {
        id: 3,
        email_login: 'adrian.wijaya@kbru.co.id',
        name: 'Furqon',
        address: 'Sumur Batu, Jakarta Pusat',
        phone: '089123456789',
        file_identitas: null,
        email: 'fahrizal.furqon@kbru.co.id',
        id_card_type: '01',
        no_identitas: '1234567890',
        tempat_lahir: 'Purwakarta',
        tanggal_lahir: '1992-05-20',
        jenis_kelamin: 'L',
        kewarganegaraan: 'wni',
        negara_asal: null,
        status_profile: '2',
    }];
const initialPerserta = {
    id: 1,
    email_login : 'adrian.wijaya@kbru.co.id',
    name : 'Adrian Prasetya Wijaya',
    address : 'Jalan U No.9A Palmerah Jakarta Barat',
    phone : '081315556594',
    file_identitas : null,
    email: 'adrian.wijaya@kbru.co.id',
    id_card_type: '01',
    no_identitas: '1871022603940008',
    tempat_lahir: 'Bandar Lampung',
    tanggal_lahir: '1994-03-26',
    jenis_kelamin: 'L',
    kewarganegaraan: 'wni',
    negara_asal: null, 
    status_profile: '1',
}

export function pesertaAsuransiDummy(state = initialPerserta, action) {
    switch (action.type) {
        default:
            return state;
    }
}

export function listPesertaAsuransiDummy(state = initialListPeserta, action) {
    switch (action.type) {
        default:
            return state;
    }
}

/* end tambah peserta client */

