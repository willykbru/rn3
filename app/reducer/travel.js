const initialDataPemegangPolisTravel =  {
	district_id: 'Pilih',
    file_identitas: '',
    member_ahliwaris: '',
    member_alamat: '',
    member_birthdate: null,
    member_email: '',
    member_firstname: '',
    member_gender: 'L',
    member_hp: '',
    member_hub_ahliwaris: 'Pilih',
    member_idno: '',
    member_kitas: '',
    member_lastname: '',
    member_phoneno: '',
    member_status: '1',
    province_id: 'Pilih',
    tempat_lahir: '',
    tittle: 'Pilih'
}
  
const initialDataPasanganTravel =  {
	district_id: '',
    file_identitas: '',
    member_ahliwaris: '',
    member_alamat: '',
    member_birthdate: null,
    member_email: '',
    member_firstname: '',
    member_gender: 'L',
    member_hp: '',
    member_hub_ahliwaris: '',
    member_idno: '',
    member_kitas: '',
    member_lastname: '',
    member_phoneno: '',
    member_status: '2',
    province_id: '',
    tempat_lahir: '',
    tittle: 'Mr.'
}

const initialDataAnakPertamaTravel =  {
	district_id: '',
    file_identitas: '',
    member_ahliwaris: '',
    member_alamat: '',
    member_birthdate: null,
    member_email: '',
    member_firstname: '',
    member_gender: 'L',
    member_hp: '',
    member_hub_ahliwaris: '',
    member_idno: '',
    member_kitas: '',
    member_lastname: '',
    member_phoneno: '',
    member_status: '3',
    province_id: '',
    tempat_lahir: '',
    tittle: 'Mr.'
}

const initialDataAnakKeduaTravel =  {
	district_id: '',
    file_identitas: '',
    member_ahliwaris: '',
    member_alamat: '',
    member_birthdate: null,
    member_email: '',
    member_firstname: '',
    member_gender: 'L',
    member_hp: '',
    member_hub_ahliwaris: '',
    member_idno: '',
    member_kitas: '',
    member_lastname: '',
    member_phoneno: '',
    member_status: '3',
    province_id: '',
    tempat_lahir: '',
    tittle: 'Mr.'
}

const initialDataAnakKetigaTravel =  {
	district_id: '',
    file_identitas: '',
    member_ahliwaris: '',
    member_alamat: '',
    member_birthdate: null,
    member_email: '',
    member_firstname: '',
    member_gender: 'L',
    member_hp: '',
    member_hub_ahliwaris: '',
    member_idno: '',
    member_kitas: '',
    member_lastname: '',
    member_phoneno: '',
    member_status: '3',
    province_id: '',
    tempat_lahir: '',
    tittle: 'Mr.'
}

const initialDataTravel = {
	kode_voucher: '',
	mobile_apps: 'Y',
	quota: 1,
	visitor_id: ''
}


const initialOtherDataTravel = {
	kewarganegaraan_pp: 'wni',
	kewarganegaraan_ps: 'wni',
	kewarganegaraan_ap: 'wni',
	kewarganegaraan_ad: 'wni',
	kewarganegaraan_at: 'wni',
}

const initialTravelState = {
  city_id: 'Pilih',
  country_id: 'Pilih',
  daftar_keluarga: 0,
  end_date: null,
  package_id: 0,
  package_type: 'Pilih',
  start_date: null,
  travel_type: 'Pilih',
  trip_type: 'Pilih'
}


const initialTravelLabelState = {
	label_city:'Pilih',
	label_country:'Pilih',
	label_trip_type:'Pilih',
	label_package_type:'Pilih',
    label_provinsi:'Pilih',
    label_district:'Pilih',
    label_daftar_keluarga:'Pilih',
	label_start_date:null,
	label_end_date:null,
	label_travel_type: 'Pilih',
}

export function dataTravel(state= initialDataTravel , action){
  switch (action.type) {
      case 'SET_DATA_TRAVEL':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_TRAVEL':
          return state;
      default:
          return state;
  }
}

export function otherDataTravel(state= initialOtherDataTravel , action){
  switch (action.type) {
      case 'SET_OTHER_DATA_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function dataPemegangPolisTravel(state= initialDataPemegangPolisTravel , action){
  switch (action.type) {
      case 'SET_DATA_PEMEGANG_POLIS_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function dataPasanganTravel(state= initialDataPasanganTravel , action){
  switch (action.type) {
      case 'SET_DATA_PASANGAN_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function dataAnakPertamaTravel(state= initialDataAnakPertamaTravel , action){
  switch (action.type) {
      case 'SET_DATA_ANAK_PERTAMA_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function dataAnakKeduaTravel(state= initialDataAnakKeduaTravel , action){
  switch (action.type) {
      case 'SET_DATA_ANAK_KEDUA_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function dataAnakKetigaTravel(state= initialDataAnakKetigaTravel , action){
  switch (action.type) {
      case 'SET_DATA_ANAK_KETIGA_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}


export function asuransiTravel(state = {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_TRAVEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function travelLabelState(state = initialTravelLabelState, action) {
    switch (action.type) {
        case 'SET_TRAVEL_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
export function travelState(state = initialTravelState, action) {
    switch (action.type) {
        case 'SET_TRAVEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
