/* Accident */
const initialRenewalAccident = {
  package_id: 0,
  pekerjaan: '',
  tgl_lahir: null,
}

const initialRenewalDataAccident = {
    alamat: '',
    district_id: 'Pilih',
    file_identitas: '',
    gender: 'L',
    hub_ahliwaris: 'Pilih',
    kewarganegaraan: 'wni',
    kode_voucher: '',
    mobile_apps: 'Y',
    nama: '',
    nama_ahliwaris: '',
    no_identitas: '',
    order_id: '',
    province_id: 'Pilih',
    status_renewal: '',
    tempat_lahir: '',
    tgl_lahir: '',
    tittle: '',
    visitor_id: '',
}

export function renewalAccident(state= initialRenewalAccident , action){
  switch (action.type) {
      case 'SET_RENEWAL_ACCIDENT':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function renewalDataAccident(state = initialRenewalDataAccident, action) {
    switch (action.type) {
        case 'SET_RENEWAL_DATA_ACCIDENT':
            return Object.assign({}, state, action.value );
        default:
            return state;
    }
}

/* End Accident */

/* MV */

const initialRenewalKendaraan = {
    vehicle_type: 'Pilih',
    coverage: 'comprehensive',
    merek: 'Pilih',
    seri_tipe: 'Pilih',
    plat: 'Pilih',
    penggunaan:'Pribadi',
    tsi:0,
    build_year: 'Pilih',
    harga_aksesoris: 0,
    aksesoris:'',
    insurance_id:'',
}

const initialRenewalDataKendaraan= {
  alamat_pemilik: '',
  kode_voucher: '',
  merek: '',
  mobile_apps: 'Y',
  nama_pemilik: '',
  nama_stnk: '',
  no_ktp: '',
  no_mesin: '',
  no_plat: '',
  no_rangka: '',
    order_id: '',
  penggunaan: '',
  photo_belakang: '',
  photo_depan: '',
  photo_kanan: '',
  photo_kiri: '',
  photo_ktp: '',
  photo_stnk: '',
  seri_tipe: '',
  status_renewal: '',
  status_qq: 'N',
  tgl_lahir: '',
  tittle: '',
  visitor_id: '',
  warna: ''
}

const initialRenewalPerluasanKendaraan = {
  tpl_value: 10000000,
  pll_value: 10000000,
  padriver_value: 10000000,
  pap_value: 10000000,
  pap_people: 1,
  tpl: false,
  pll: false,
  padriver: false,
  pap: false,
  srcc: false,
  ts: false,
  eqvet: false,
  tsfwd: false,
  atpm: false,
}


export function renewalPerluasanKendaraan(state= initialRenewalPerluasanKendaraan , action){
  switch (action.type) {
      case 'SET_RENEWAL_PERLUASAN_KENDARAAN':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function renewalKendaraan(state= initialRenewalKendaraan , action){
  switch (action.type) {
      case 'SET_RENEWAL_KENDARAAN':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function renewalDataKendaraan(state = initialRenewalDataKendaraan, action) {
    switch (action.type) {
        case 'SET_RENEWAL_DATA_KENDARAAN':
            return Object.assign({}, state, action.value );
        default:
            return state;
    }
}

/* End MV */

/* Property */

const initialRenewalDataProperty= {
  alamat_pemilik: '',
  kode_voucher: '',
  mobile_apps: 'Y',
  nama_pemilik: '',
  building_content: '',
  namabankleasing: '',
  ktp: '',
  photo_belakang: '',
  photo_depan: '',
  photo_kanan: '',
  photo_kiri: '',
  photo_ktp: '',
  statusqq: 'N',
  risk_address: '',
  tgl_lahir: '',
  tittle: '',
  visitor_id: '',
}


const initialRenewalProperty = {
  building_tsi: 0,
  content_tsi: 0,
  district_id: '',
  eqvet: false,
  flood: false,
  kecamatan_id: '',
  kelurahan_id: '',
  lantai: 0,
  package_id: 0,
  province_id: '',
  tipe: '',
}

export function renewalProperty(state= initialRenewalProperty , action){
  switch (action.type) {
      case 'SET_RENEWAL_PROPERTY':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function renewalDataProperty(state = initialRenewalDataProperty, action) {
    switch (action.type) {
        case 'SET_RENEWAL_DATA_PROPERTY':
            return Object.assign({}, state, action.value );
        default:
            return state;
    }
}

/* END Property*/



const initialLabel = {
	province: '', 
	district: '',
	kecamatan: '',
	kelurahan: '',
    order_id: '',
    pekerjaan: '',
    data_sama: false,
}


export function postDataRenewal(state= {} , action){
  switch (action.type) {
      case 'SET_POST_DATA_RENEWAL':
          return action.value;
      default:
          return state;
  }
}


export function renewalLabel(state= initialLabel , action){
  switch (action.type) {
      case 'SET_RENEWAL_LABEL':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}