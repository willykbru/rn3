/* tambah list peserta */
const initialList = [];
const initialPerserta = {
}

export function listPesertaAsuransi(state = initialList, action) {
    switch (action.type) {
        case 'SET_LIST_PESERTA_ASURANSI':
            //console.log(action.items);
            return action.items;
        case 'RESET_LIST_PESERTA_ASURANSI':
            return [];
        default:
            return state;
    }
}

export function pesertaAsuransi(state = {}, action) {
    switch (action.type) {
        case 'SET_PESERTA_ASURANSI':
            //console.log(action.items);
            return Object.assign({}, state, action.items);
        case 'RESET_PESERTA_ASURANSI':
            return {};
        default:
            return state;
    }
}


/* end tambah peserta client */

