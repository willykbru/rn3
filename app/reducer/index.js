import { combineReducers } from 'redux';
//import { reducer as formReducer } from "redux-form";
import * as navReducer from './navigation';
import * as serviceReducer from './service';
import * as lobListReducer from './lobList';
import * as lobReducer from './lob';
import * as mvReducer from './mv';
import * as propertyReducer from './property';
import * as apartmentReducer from './apartment';
import * as paReducer from './pa';
import * as healthReducer from './health';
import * as lifeReducer from './life';
import * as delayReducer from './delay';
import * as travelReducer from './travel';
import * as renewalReducer from './renewal';
import * as claimReducer from './claim';
import * as listReducer from './list';
import * as dummyReducer from './dummy';
//import { items, itemsHasErrored, itemsIsLoading } from './fetchData';
//import loginReducer from "../components/login/reducer";
//import * as authReducer from './auth';

export default combineReducers(Object.assign(
  //form: formReducer,
  navReducer,
  serviceReducer,
  lobListReducer,
  lobReducer,
  mvReducer,
  propertyReducer,
  apartmentReducer,
  paReducer,
  healthReducer,
  lifeReducer,
  delayReducer,
  travelReducer,
  renewalReducer,
  claimReducer,
  listReducer,
  dummyReducer,
  //items,
  //itemsHasErrored,
  //itemsIsLoading,
  //loginReducer,
//  authReducer,
));
