const initialStatusState = {
  verify: {username:'', flag:''},
  changefp: {flag: ''},
  changepassword: {flag: ''},
  isLogin: false,
  registered: {username:'', password: '', device:'', flag: ''},
  activation: {username: ''},
  notificationcart: false,
  updmodalstatus: false,
  notificationrenewal: false,
  popupevent: true,
  serverdate: null,
  deleteCart: null,
  chatmodal: false,
}

const initialScreenState = {
  mainScreen : '',
  fpScreen: 'EmailForgotPassword',
  registerScreen: 'Register',
  asuransiScreen: '',
  cpscreen: 'ChangeMyPassword',
  polisScreen: 'DataPolis',
  actScreen: 'EmailActivation',
  renewalScreen: ''
}

const initialPermission = {
	storage: false,
}

//New Register Process
const initialDataRegister = { 
		username: null,
		password: null,
		name: null,
		phone: null,
		token: null,
      }
      
const initialDataLogin = {
    username: null,
    visitor_name: null,
    visitor_phone: null,
    token: null,
    last_login: null,
    last_access:null,
}

export function dataRegister(state = initialDataRegister, action) {
    switch (action.type) {
        case 'SET_DATA_REGISTER':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

/* login screen */
export function dataLogin(state = initialDataLogin, action) {
    switch (action.type) {
        case 'SET_DATA_LOGIN':
            return Object.assign({}, state, action.items);
        default:
            return state;
    }
}
/* end login screen */


export function itemsErrored(state = false, action) {
    switch (action.type) {
        case 'ITEMS_HAS_ERRORED':
            return action.hasErrored;
        default:
            return state;
    }
}

export function itemsLoading(state = false, action) {
    switch (action.type) {
        case 'ITEMS_IS_LOADING':
            return action.isLoading;
        default:
            return state;
    }
}

export function items(state = {}, action) {
    switch (action.type) {
        case 'ITEMS_FETCH_DATA_SUCCESS':
            return action.items;
        default:
            return state;
    }
}


/* popup screen */
export function itemsPopup (state = {}, action){
  switch(action.type){
    case 'ITEMS_FETCH_POPUP_SUCCESS':
      return action.items;
    default:
      return state;
  }
}
/* end popup screen */

export function oid(state = '', action) {
    switch (action.type) {
        case 'SET_OID':
            return action.items;
        default:
            return state;
    }
}

export function screen(state = initialScreenState, action) {
    switch (action.type) {
        case 'SET_MAIN_SCREEN':
            return Object.assign({}, state, {mainScreen : action.screen});
        case 'SET_FP_SCREEN':
            return Object.assign({}, state, {fpScreen : action.screen});
        case 'SET_REGISTER_SCREEN':
            return Object.assign({}, state, {registerScreen : action.screen});
        case 'SET_ASURANSI_SCREEN':
            return Object.assign({}, state, {asuransiScreen : action.screen});
        case 'SET_CP_SCREEN':
            return Object.assign({}, state, {cpScreen : action.screen});
		case 'SET_ACT_SCREEN':
            return Object.assign({}, state, {actScreen : action.screen});
		case 'SET_POLIS_SCREEN':
            return Object.assign({}, state, {polisScreen : action.screen});
		case 'SET_RENEWAL_SCREEN':
            return Object.assign({}, state, {renewalScreen : action.screen});
        default:
            return state;
    }
}

export function sliderImageLink(state = [], action) {
    switch (action.type) {
        case 'FETCH_SLIDER':
            return action.value;
        default:
            return state;
    }
}

export function token(state = '', action) {
    switch (action.type) {
        case 'SET_DATA_TOKEN':
            return action.token;
        default:
            return state;
    }
}

export function session(state = initialDataLogin, action) {
    switch (action.type) {
        case 'SET_DATA_SESSION':
            return Object.assign({}, state, action.session);
        default:
            return state;
    }
}

export function status(state = initialStatusState, action) {
    switch (action.type) {
        case 'SET_STATUS_VERIFY':
            return Object.assign({}, state, {verify : action.status});
        case 'SET_STATUS_LOGIN':
            return Object.assign({}, state, {isLogin : action.status});
        case 'SET_STATUS_CHANGE_FP':
            return Object.assign({}, state, {changefp : action.status});
        case 'SET_STATUS_CHANGE_PASSWORD':
            return Object.assign({}, state, {changepassword : action.status});
        case 'SET_STATUS_REGISTER':
            return Object.assign({}, state, {registered : action.status});
		case 'SET_STATUS_ACTIVATION':
            return Object.assign({}, state, {activation : action.status});
		case 'SET_STATUS_NOTIF_CART':
            return Object.assign({}, state, {notificationcart : action.status});
		case 'SET_MODAL_UPDATE_STATUS':
            return Object.assign({}, state, {updmodalstatus : action.status});
		case 'SET_STATUS_NOTIF_RENEWAL':
            return Object.assign({}, state, {notificationrenewal : action.status});
		case 'SET_STATUS_POPUP_EVENT':
            return Object.assign({}, state, {popupevent : action.status});
		case 'FETCH_SERVER_DATE':
            return Object.assign({}, state, {serverdate : action.status});		
		case 'SET_STATUS_DELETE_CART':
            return Object.assign({}, state, {deleteCart : action.status});
        case 'SET_STATUS_CHAT_MODAL':
            return Object.assign({}, state, { chatmodal: action.status });
        default:
            return state;
    }
}
export function permission(state = initialPermission, action) {
    switch (action.type) {
        case 'SET_STATUS_PERM_STORAGE':
            return Object.assign({}, state, {storage : action.status});
       
        default:
            return state;
    }
}

/*cart*/
export function totalPendingCart(state = 0, action) {
    switch (action.type) {
        case 'SET_TOTAL_PENDING_CART':
		// console.log('pendingcart', action.value)
            return action.value;
        default:
            return state;
    }
}
/*end of cart*/

export function notificationcontent(state = {}, action) {
    switch (action.type) {
       
        case 'SET_NOTIFICATION_CONTENT':
            console.log('get notification content', action.value)
            return action.value;
        default:
            return state;
    }
}
