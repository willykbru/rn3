import * as appFunction from '../function';
const initialPenggunaan = 'Pribadi';

const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];

const initialJenisKendaraan = [{
	key: 'Pilih',
	label: 'Pilih',
},{
	key: 'Mobil',
	label: 'Mobil',
},{
	key: 'Motor',
	label: 'Motor',
}];

const initialCoverageList = [
{
	key:'comprehensive',
	label:'Comprehensive'
},{
  key:'total loss only',
  label:'Total Loss Only'
}];

const initialListAsuransi = [];

const initialList = [];
const initialPolis = {};

const initialLoadingState = {
	loadingListAsuransiMV: false,
	loadingListAsuransiProperty:false,
	loadingListAsuransiApartment: false,
	loadingListAsuransiAccident: false,
	loadingListAsuransiLife: false,
	loadingListAsuransiDelay: false,
	loadingListAsuransiTravel: false,
	loadingDataTransaksi: false,
	loadingListPendingCart: false,
	loadingListPendingPayment: false,
	loadingDataPendingCart:false,
	loadingDataPolis: false,
  loadingListSuccessPayment:false,
  loadingListPendingCartPremi:false,
}



export function listJenisKendaraan(state = initialJenisKendaraan, action) {
          return state;
}

export function listPendingPayment(state = initialList, action) {
  switch (action.type) {
      case 'SET_PENDING_PAYMENT_LIST':
      //console.log(action.items);
          return action.items;
      case 'RESET_PENDING_PAYMENT_LIST':
        return [];
      default:
          return state;
  }
}

export function listSuccessPayment(state = initialList, action) {
  switch (action.type) {
      case 'SET_SUCCESS_PAYMENT_LIST':
          return action.items;
      default:
          return state;
  }
}

export function listMerekKendaraan(state = initial, action) {
  switch (action.type) {
      case 'MERK_KENDARAAN_LIST':
          return action.items;
      default:
          return state;
  }
}

export function listSeriKendaraan(state = initial, action) {
  switch (action.type) {
      case 'SERI_KENDARAAN_LIST':
          return action.items;
      default:
          return state;
  }
}

export function listPlatKendaraan(state = initial, action) {
  switch (action.type) {
      case 'PLAT_KENDARAAN_LIST':
          return action.items;
      default:
          return state;
  }
}

export function loadingState(state = initialLoadingState, action) {
  switch (action.type) {
      case 'LOADING_LIST_ASURANSI_MV':
         return Object.assign({}, state, {loadingListAsuransiMV: action.bool});
	  case 'LOADING_LIST_ASURANSI_PROPERTY':
		return Object.assign({}, state, {loadingListAsuransiProperty: action.bool});
	  case 'LOADING_LIST_ASURANSI_APARTMENT':
		return Object.assign({}, state, {loadingListAsuransiApartment: action.bool});
	  case 'LOADING_LIST_ASURANSI_ACCIDENT':
		return Object.assign({}, state, {loadingListAsuransiAccident: action.bool});
	  case 'LOADING_LIST_ASURANSI_HEALTH':
		return Object.assign({}, state, {loadingListAsuransiHealth: action.bool});
      case 'LOADING_LIST_ASURANSI_LIFE':
		return Object.assign({}, state, {loadingListAsuransiLife: action.bool});
	  case 'LOADING_LIST_ASURANSI_DELAY':
		return Object.assign({}, state, {loadingListAsuransiDelay: action.bool});
	  case 'LOADING_LIST_ASURANSI_TRAVEL':
		return Object.assign({}, state, {loadingListAsuransiTravel: action.bool});
      case 'LOADING_DATA_TRANSAKSI':
		return Object.assign({}, state, {loadingDataTransaksi: action.bool});
	  case 'LOADING_DATA_PENDING_CART':
		return Object.assign({}, state, {loadingDataPendingCart: action.bool});
	  case 'LOADING_LIST_PENDING_CART':
		return Object.assign({}, state, {loadingListPendingCart: action.bool});
	  case 'LOADING_DATA_PENDING_PAYMENT':
		return Object.assign({}, state, {loadingDataPendingPayment: action.bool});
	  case 'LOADING_LIST_PENDING_PAYMENT':
		return Object.assign({}, state, {loadingListPendingPayment: action.bool});
	  case 'LOADING_DATA_POLIS':
      return Object.assign({}, state, { loadingDataPolis: action.bool }); 
	  case 'LOADING_LIST_SUCCESS_PAYMENT':
    return Object.assign({}, state, {loadingListSuccessPayment: action.bool});
    case 'LOADING_LIST_PENDING_CART_PREMI':
      return Object.assign({}, state, { loadingListPendingCartPremi: action.bool });
      default:
          return state;
  }
}

export function listAsuransi(state = initialListAsuransi, action) {
  switch (action.type) {
      case 'SET_LIST_ASURANSI':
          return action.items;
      default:
          return state;
  }
}

export function listAhliWaris(state = initial, action) {
  switch (action.type) {
      case 'SET_LIST_AHLI_WARIS':
          return action.items;
      default:
          return state;
  }
}

export function listCoverageKendaraan(state = initialCoverageList, action) {
  switch (action.type) {
    case 'COVERAGE_MOTOR_LIST':
        return action.items;
    case 'COVERAGE_MOBIL_LIST':
        return state;
    default:
      return state;
  }
}

export function penggunaanKendaraan(state = initialPenggunaan, action) {
  switch (action.type) {
      default:
          return state;
  }
}

export function listCountry(state = initial, action){
  switch (action.type) {
    case 'LIST_COUNTRY':
        return action.items;
      default:
          return state;
  }
}

export function listCity(state = initial, action){
  switch (action.type) {
    case 'LIST_CITY':
        return action.items;
      default:
          return state;
  }
}

export function listProvinsi(state = initial, action){
  switch (action.type) {
    case 'LIST_PROVINSI':
        return action.items;
      default:
          return state;
  }
}

export function listDaftarKeluargaTravel(state = initial, action){
  switch (action.type) {
    case 'LIST_DAFTAR_KELUARGA_TRAVEL':
        return action.items;
      default:
          return state;
  }
}

export function listPackageTravel(state = initial, action){
  switch (action.type) {
    case 'LIST_PACKAGE_TRAVEL':
        return action.items;
      default:
          return state;
  }
}

export function listDistrict(state = initial, action){
  switch (action.type) {
    case 'LIST_DISTRICT':
        return action.items;
      default:
          return state;
  }
}

  export function listKecamatan(state = initial, action){
    switch (action.type) {
      case 'LIST_KECAMATAN':
          return action.items;
        default:
            return state;
    }
}
    export function listKelurahan(state = initial, action){
      switch (action.type) {
        case 'LIST_KELURAHAN':
            return action.items;
          default:
              return state;
      }
}
      export function listOccupation(state = initial, action){
        switch (action.type) {
          case 'LIST_OCCUPATION':
              return action.items;
            default:
                return state;
        }
      }


      export function listHubunganAhliWaris(state = initial, action){
        switch (action.type) {
          case 'LIST_HUBUNGAN_AHLI_WARIS':
              return action.items;
            default:
                return state;
        }
      }
	  
/*cart*/
export function listPendingCart(state = initialList, action) {
  switch (action.type) {
      case 'SET_PENDING_CART_LIST':
          return action.items;
      default:
          return state;
  }
}

export function listPendingCartPremi(state = [], action) {
  switch (action.type) {
    case 'LIST_PENDING_CART_PREMI':
      //return [...state, action.items];
      return [...state, action.items];
    case 'RESET_LIST_PENDING_CART_PREMI':
      return [];
    default:
      return state;
  }
}

/*end of cart*/
	  
	  
