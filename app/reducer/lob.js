const initialObject = {};

const initialVoucher={
  kode_voucher: '',
  voucher_persen: 0,
  voucher_amount: 0,
  description: ''
}

const initialTransaksi={
  kode_transaksi: '',
  link_pembayaran: '',
}

const initialVisible = {
  plat: false
}

const initialDataBenefit = {
  package_name: '',
  insurance_id: '',
  insurance_name: '',
  benefit_url:'',
}

const initialDataPolis = {
  policyno_eqvet: '',
  status_eqvet: '',
  policy_startdate: '',
  policy_eqvet: '',
  policyno: '',
  policy_enddate: null,
  policy: '',
  status: ''
}

const initialLabelState = {
	no_plat:'',
    plat: 'Pilih',
    tipe_property: 'Pilih',
    provinsi_property: 'Pilih',
    district_property:'Pilih',
    kecamatan_property:'Pilih',
    kelurahan_property:'Pilih',
}

const initialStatusLob = {
	pa_gratis : 'N',
}

const initialSearch = {};

export function dataSearch(state = initialSearch, action) {
    switch (action.type) {
        case 'SET_DATA_SEARCH':
            return action.value;
        default:
            return state;
    }
}

export function dataBenefit(state = initialDataBenefit, action) {
    switch (action.type) {
        case 'SET_DATA_BENEFIT':
            return Object.assign({}, state, action.value); 
        default:
            return state;
    }
}

export function postDataValue(state = {}, action) {
    switch (action.type) {
        case 'SET_POST_DATA':
            return action.value; 
        default:
            return state;
    }
}

export function dataTransaksi(state = initialTransaksi, action) {
    switch (action.type) {
        case 'SET_DATA_TRANSAKSI':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

export function dataPembayaran(state = {}, action) {
    switch (action.type) {
        case 'SET_DATA_PEMBAYARAN':
            return action.value;
        default:
            return state;
    }
}


export function dataPending(state = {}, action){
  switch (action.type) {
      case 'SET_DATA_PENDING':
          return action.items;
      default:
          return state;
  }
}

export function dataPolis(state = initialDataPolis, action){
  switch (action.type) {
      case 'SET_DATA_POLIS':
          return action.items;
      default:
          return state;
  }
}


export function detailPolis(state = {}, action){
  switch (action.type) {
      case 'SET_DETAIL_POLIS':
          return action.items;
      default:
          return state;
  }
}

export function kodeTransaksiDataPending(state = '', action){
  switch (action.type) {
      case 'SET_KODE_TRANSAKSI_DATA_PENDING':
          return action.value;
      default:
          return state;
  }
}

export function statusLob(state = initialStatusLob, action) {
    switch (action.type) {
        case 'SET_STATUS_LOB':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

export function dataVoucher(state = initialVoucher, action) {
    switch (action.type) {
        case 'SET_DATA_VOUCHER':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}


export function labelState(state = initialLabelState, action) {
    switch (action.type) {
        case 'SET_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

export function visible(state = initialVisible, action) {
    switch (action.type) {
        case 'SET_VISIBLE_PLAT':
            return  Object.assign({}, ...state, {plat: action.bool});
        default:
            return state;
    }
}
