const initialDataClaim = {
  nama_pengemudi: '',
  alamat_pengemudi: '',
  no_sim_pengemudi: '',
  periode_sim_pengemudi: '',
  file_foto_sim_pengemudi: '',
  pengemudi_bekerja: '',
  tertanggung_tahu: '',
  nomorPolisi: '',
  nomorRangka: '',
  nomorMesin: '',
  nomorPolis: '',
  tipeInsiden: 'Pilih',
  tanggalKejadian: '',
  jamInsiden: '',
  provinsiInsiden: 'Pilih',
  alamatKejadian: '',
  kotaInsiden: 'Pilih', // kabupaten kota insiden
  kecepatanInsiden: '',
  deskripsiInsiden: '',
  labelTipeInsiden: 'Pilih Tipe Insiden',
  labelKotaInsiden: 'Pilih Kota Insiden',
  labelProvinsiInsiden: 'Pilih Provinsi Insiden',
  file_foto_lampiran: [],
  penumpang_cedera: 'N',
  status_saksi: 'N',
  claim_mv_detail: [],
  posisi_kendaraan: '',
  lapor_polisi: 'N',
  file_foto_laporan_polisi: '',
  estimasi_kerusakan: '',
  keadaan_kendaraan: '',
  posisi_kendaraan: '',
  nama_pihak3: '',
  phone_pihak3: '',
  email_pihak3: '',
  alamat_pihak3: '',
  kecepatan_pihak3: '',
  file_foto_sim_pihak3: '',
  file_foto_ktp_pihak3: '',
  file_foto_stnk_pihak3: '',
  jenis_pihak3: null,
  status_pihak3: 'N',
  alamat_tertanggung: '',
  order_id: '',
  tanggal_kejadian: '',
  policy_no: '',
  policy_enddate: '',
  phone_tertanggung: '',
  list_saksi: [],
  list_penumpang_cedera: [],
  claim_photo: [],
  merek_kendaraan: '',
  seri_kendaraan: '',
  tahun_kendaraan: '',
  keterangan_pihak_ketiga: '',
  pengemudiChecked : false,
  labelHurufSIM : null,
  bengkel_id: '',
  file_foto_depan : '',
  file_foto_belakang : '',
  file_foto_kiri : '',
  file_foto_kanan : '',
  pengemudiChecked : false,
  visibleNavigation: true,
}

const initialDataClaimAll =
{
  alamat_tertanggung: '',
  claim_mv: {
    alamat_pengemudi: '',
    claim_mv_detail: [],
    claim_photo: [],
    deskripsi_kejadian: '',
    email_pihak3: '',
    estimasi_kerusakan: 0,
    file_polisi: '',
    keadaan_kendaraan: '',
    lapor_polisi: '',
    merek_kendaraan: '',
    nama_pengemudi: '',
    nama_pihak3: '',
    no_mesin: '',
    no_plat: '',
    no_rangka: '',
    no_sim: '',
    pengemudi_bekerja: '',
    penumpang_cidera: '',
    periode_sim: '',
    phone_pihak3: '',
    photo_sim: '',
    posisi_kendaraan: '',
    seri_kendaraan: '',
    status_pihak3: '',
    status_saksi: '',
    tahun_kendaraan: '',
    tertangung_tau: '',
    tipe_kejadian: ''
  },
  email_tertanggung: '',
  insurance_id: '',
  nama_tertanggung: '',
  no_ktp: '',
  order_id: '',
  phone_tertanggung: '',
  policy_enddate: '',
  policy_no: '',
  tanggal_kejadian: '',
  tempat_kejadian: '',
  bengkel_id: '',
}

const initialClaimState = {
  provinsiInsiden: '',
}

const initialClaimLabelState = {
  labelProvinsiInsiden: 'Pilih',
  labelKotaInsiden: 'Pilih',
  labelTipeInsiden: 'Pilih',
}

const initialClaimList = []

const initialClaimDone = []

const initialClaimTerkini = []

const initialListClaimPolis =[]

const initialLoadingClaimMenu = false;

// const initialNoPolis =[{
//   key : 'Pilih',
//   label : 'Pilih',
// }]

const initialTipeInsiden = [{
  key: 'Pilih',
  label: 'Pilih',
}]

const initialDataCamera = []

const initialDataCameraTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataSimCameraTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataKtpPihak3CameraTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataStnkPihak3CameraTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataSimPihak3CameraTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataLaporanPolisiCameraTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataMobilDepanTemp = [{
    'size': 0,
    'path': null,
    'caption': null,
}]

const initialDataMobilBelakangTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataMobilKiriTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialDataMobilKananTemp = [{
  'size': 0,
  'path': null,
  'caption': null,
}]

const initialTransaksiKlaimMV = {
  claim_no: null,
}

const initialLoadingKlaimState = {
  loadingListKlaimMV: false,
}

const initialLoadingHistoriKlaim = {
  loadingHistoriKlaim: false,
}

const initialLoadingKlaimTerkini = {
  loadingKlaimTerkini: false,
}

const initialStateClaimNumber = '';

const initialStateLinkSPK = {
  "items": null,
  "title" : null,
};

export function dataClaim(state = initialDataClaim, action) {
  switch (action.type) {
    case 'SET_DATA_CLAIM':
      return Object.assign({}, state, action.value);
    case 'RESET_DATA_CLAIM':
      return initialDataClaim;
    default:
      return state;
  }
}

export function dataClaimAll(state = initialDataClaimAll, action) {
  switch (action.type) {
    case 'SET_DATA_CLAIM_ALL':
      return Object.assign({}, state, action.value);
    case 'RESET_DATA_CLAIM_ALL':
      return initialDataClaimAll;
    default:
      return state;
  }
}

export function dataTransaksiKlaimMV(state = initialTransaksiKlaimMV, action) {
  switch (action.type) {
    case 'SET_DATA_TRANSAKSI_KLAIM_MV':
      return Object.assign({}, state, action.value);
    case 'RESET_DATA_TRANSAKSI_KLAIM_MV':
      return initialTransaksiKlaimMV;
    default:
      return state;
  }
}


export function claimState(state = initialClaimState, action) {
  switch (action.type) {
    case 'SET_CLAIM_STATE':
      return Object.assign({}, state, action.value);
    case 'RESET_CLAIM_STATE':
      return initialClaimState;
    default:
      return state;
  }
}

export function claimPolis(state = initialListClaimPolis, action){
  switch(action.type){
    case 'SET_CLAIM_POLIS':
      console.log('claimpolis',action.value)
      return [...state, action.value];
      // return Object.assign(state, action.value);
    case 'RESET_CLAIM_POLIS':
        return initialListClaimPolis;
    default:
      return state;
  }
}

export function claimLabelState(state = initialClaimLabelState, action) {
  switch (action.type) {
    case 'SET_CLAIM_LABEL_STATE':
      return Object.assign({}, state, action.value);
    case 'RESET_CLAIM_LABEL_STATE':
      return initialClaimLabelState;
    default:
      return state;
  }
}

export function listTipeInsiden(state = initialTipeInsiden, action) {
  switch (action.type) {
    case 'TIPE_INSIDEN_LIST':
      return action.items;
    default:
      return state;
  }
}

export function listClaimGeneral(state = initialClaimList, action) {
  switch (action.type) {
    case 'CLAIM_LIST_ALL':
      return Object.assign(state, action.items);
    case 'RESET_CLAIM_ALL_LIST':
      return initialClaimList;
    default:
      return state;
  }
}

export function listClaimDone(state = initialClaimDone, action) {
  switch (action.type) {
    case 'SET_CLAIM_LIST_DONE':
      return action.value;
    // console.log('temp',action.temp);
    // return state.concat(action.temp);
    // return state.push(action.temp);
    // return Object.assign({}, state, action.temp);
    // return action.temp;
    case 'RESET_CLAIM_LIST_DONE':
      return initialClaimDone;
    default:
      return state;
  }
}

export function listClaimTerkini(state = initialClaimTerkini, action) {
  switch (action.type) {
    case 'SET_CLAIM_LIST_TERKINI':
      return action.value;
    case 'RESET_CLAIM_LIST_TERKINI':
      return initialClaimTerkini;
    default:
      return state;
  }
}

export function listClaimMV(state = initialDataClaim, action) {
  switch (action.type) {
    case 'DATA_CLAIM_MV_LIST':
      return action.items;
    default:
      return state;
  }
}

export function listDataGambar(state = initialDataCamera, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_KAMERA':
      return [...state, action.value];
    case 'HAPUS_DATA_GAMBAR_KAMERA':
      let i = state.length;
      while (i--) {
        if (action.value.indexOf(state[i].path) != -1) {
          state.splice(i, 1);
        }
      }
      return state;
    case 'EDIT_DATA_GAMBAR_KAMERA':
      for (var i in state) {
        if (state[i].path == action.value.path) {
          state[i].caption = action.value.caption;
          break;
        }
      }
      return state;
    case 'RESET_DATA_GAMBAR_KAMERA':
      return initialDataCamera;
    default:
      return state;
  }
}

export function listDataGambarTemp(state = initialDataCameraTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'RESET_DATA_GAMBAR_KAMERA_TEMP':
      return Object.assign({}, state);
    default:
      return state;
  }
}

export function listDataGambarSim(state = initialDataSimCameraTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_SIM_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_SIM_KAMERA':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);


      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataGambarSimPihak3(state = initialDataSimPihak3CameraTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_SIM_PIHAK3_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_SIM_PIHAK3_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);


      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataGambarKtpPihak3(state = initialDataKtpPihak3CameraTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_KTP_PIHAK3_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_KTP_PIHAK3_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);


      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataGambarStnkPihak3(state = initialDataStnkPihak3CameraTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_STNK_PIHAK3_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_STNK_PIHAK3_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);


      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataLaporanPolisi(state = initialDataLaporanPolisiCameraTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_LAPORAN_POLISI_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_LAPORAN_POLISI_KAMERA':
      // console.log('hapusDataGambarLaporanPolisiKameraTemp reducer');

      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);

      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataMobilDepan(state = initialDataMobilDepanTemp, action){
  switch (action.type) {
    case 'SET_DATA_GAMBAR_MOBIL_DEPAN_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_MOBIL_DEPAN_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);

      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataMobilBelakang(state = initialDataMobilBelakangTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_MOBIL_BELAKANG_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_MOBIL_BELAKANG_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);

      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataMobilKiri(state = initialDataMobilKiriTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_MOBIL_KIRI_KAMERA_TEMP':
      console.log('test listDataMobilKiri', action);
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_MOBIL_KIRI_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);

      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function listDataMobilKanan(state = initialDataMobilKananTemp, action) {
  switch (action.type) {
    case 'SET_DATA_GAMBAR_MOBIL_KANAN_KAMERA_TEMP':
      return Object.assign({}, state, action.value);
    case 'HAPUS_DATA_GAMBAR_MOBIL_KANAN_KAMERA_TEMP':
      let items = {
        'size': 0,
        'path': null,
        'caption': null,
      }

      let images = [];
      images.push(items);

      return Object.assign({}, state, images);
    default:
      return state;
  }
}

export function linkSPK(state = initialStateLinkSPK, action){
  switch(action.type){
    case 'SET_LINK_SPK':
      return Object.assign({}, state, action.value);
    case 'RESET_LINK_SPK':
      return initialStateLinkSPK;
    default:
      return state;
  }
}
 

// untuk dialogandroid progress
export function loadingStateKlaim(state = initialLoadingKlaimState, action) {
  switch (action.type) {
    case 'LOADING_DATA_KLAIM_MV':
      return Object.assign({}, state, { loadingListKlaimMV: action.bool });
    default:
      return state;
  }
}

export function loadingKlaimHistori(state = initialLoadingHistoriKlaim, action) {
  switch (action.type) {
    case 'LOADING_HISTORI_KLAIM':
      return Object.assign({}, state, { loadingHistoriKlaim: action.bool });
    default:
      return state;
  }
}

export function loadingKlaimTerkini(state = initialLoadingKlaimTerkini, action) {
  switch (action.type) {
    case 'LOADING_KLAIM_TERKINI':
      console.log('LOADING_KLAIM_TERKINI',action.bool ? 'true' : 'false');
      return Object.assign({}, state, { loadingKlaimTerkini: action.bool });
    default:
      return state;
  }
}

export function loadingClaimMenu(state = initialLoadingClaimMenu, action){
  switch(action.type){
    case 'SET_LOADING_CLAIM_MENU':
      return action.bool;
    default:
      return state;
  }
}

export function claim_number(state = initialStateClaimNumber, action) {
  switch (action.type) {
    case 'SET_CLAIM_NUMBER':
      // console.log(action.value);
      return action.value;
    case 'RESET_CLAIM_NUMBER':
      return initialStateClaimNumber;
    default:
      return state;
  }
}

export function claim_status_detail(state = [], action){
  switch(action.type){
    case 'SET_CLAIM_STATUS_DETAIL':
    return action.value;
    case 'RESET_CLAIM_STATUS_DETAIL':
    return [];
    default:
    return state;
  }
}

export function listHalaman(state = [], action){
  let temp = [];
  
  temp = [{
    'current' : 1,
    'end' : 1,
  }]
  
  switch(action.type){
    case 'SET_HALAMAN':
    return action.value;
    case 'RESET_HALAMAN':
    return temp;
    default:
    return state;
  }
}

const initialListLokasiSekarang = [{
    latitude: null,
    longitude: null,
    error: null,
    speed: null,
}];

export function listLokasiSekarang (state = initialListLokasiSekarang, action){  
  switch(action.type){
    case 'SET_LOKASI_SEKARANG':
    // console.log('value lokasi',action.value);
      return action.value;
    case 'RESET_LOKASI_SEKARANG':
      return initialListLokasiSekarang;
    default:
      return state;
  }
}

export function listBengkelClaim(state = [], action){
  switch(action.type){
    case 'SET_BENGKEL_CLAIM':
      return action.value;
    case 'RESET_BENGKEL_CLAIM':
      state
    default:
      return state;
  }
}

export function listAllBengkelClaim(state = [], action){
  switch(action.type){
    case 'SET_LIST_ALL_BENGKEL_CLAIM':
      return action.value;
    default:
      return state;
  }
}

// export function listBengkelClaim(state = [], action) {
//   switch (action.type) {
//     case 'SET_DATA_BENGKEL_CLAIM':
//       return [...state, action.value];
//     case 'RESET_DATA_BENGKEL_CLAIM':
//       temp = [];      
//       return temp;
//     default:
//       return state;
//   }
// }
