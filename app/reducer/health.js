const initialDataHealth =  {
    alamat: '',
    district_id: 'Pilih',
    file_identitas: '',
    gender: 'L',
    hub_ahliwaris: '',
    kewarganegaraan: 'wni',
    kode_voucher: '',
    mobile_apps: 'Y',
    nama: '',
    nama_ahliwaris: '',
    no_identitas: '',
    province_id: 'Pilih',
    tgl_lahir: '',
    tittle: '',
    visitor_id: '',
  }

  


const initialHealthState = {
  pekerjaan:'kantoran',
  package_id: 0,
  tgl_lahir: ''
}


const initialHealthLabelState = {
    label_provinsi:'',
    label_district:'',
}

export function dataHealth(state= initialDataHealth , action){
  switch (action.type) {
      case 'SET_DATA_HEALTH':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_HEALTH':
          return state;
      default:
          return state;
  }
}

export function asuransiHealth(state = {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_HEALTH':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function healthLabelState(state = initialHealthLabelState, action) {
    switch (action.type) {
        case 'SET_HEALTH_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
export function healthState(state = initialHealthState, action) {
    switch (action.type) {
        case 'SET_HEALTH_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
