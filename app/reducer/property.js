const initialDataProperty= {
  alamat_pemilik: '',
  kode_voucher: '',
  mobile_apps: 'Y',
  nama_pemilik: '',
  building_content: '',
  namabankleasing: '',
  ktp: '',
  photo_belakang: '',
  photo_depan: '',
  photo_kanan: '',
  photo_kiri: '',
  photo_ktp: '',
  statusqq: 'N',
  risk_address: '',
  tgl_lahir: '',
  tittle: '',
  visitor_id: '',
}


const initialPropertyState = {
  building_tsi: 0,
  content_tsi: 0,
  district_id: '',
  eqvet: false,
  flood: false,
  kecamatan_id: '',
  kelurahan_id: '',
  lantai: 0,
  package_id: 0,
  province_id: '',
  tipe: '',
}


export function dataProperty(state= initialDataProperty , action){
  switch (action.type) {
      case 'SET_DATA_PROPERTY':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_PROPERTY':
          return state;
      default:
          return state;
  }
}

export function asuransiProperty(state={} , action){
  switch (action.type) {
      case 'SET_ASURANSI_PROPERTY':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function propertyState(state = initialPropertyState, action) {
    switch (action.type) {
        case 'SET_PROPERTY_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
