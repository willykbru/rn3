const initialDataLife =  {
    alamat: '',
    district_id: 'Pilih',
    file_identitas: '',
    gender: 'L',
    hub_ahliwaris: 'Pilih',
    kewarganegaraan: 'wni',
    kode_voucher: '',
    mobile_apps: 'Y',
    nama: '',
    nama_ahliwaris: '',
    no_identitas: '',
    province_id: 'Pilih',
    tgl_lahir: '',
    tittle: '',
    visitor_id: '',
  }


const initialLifeState = {
  package_id: 0,
  pekerjaan: '',
  tgl_lahir: ''
}


const initialLifeLabelState = {
    label_pekerjaan: '',
    label_provinsi:'',
    label_district:'',
}

export function dataLife(state= initialDataLife , action){
  switch (action.type) {
      case 'SET_DATA_LIFE':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_LIFE':
          return state;
      default:
          return state;
  }
}

export function asuransiLife(state = {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_LIFE':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function lifeLabelState(state = initialLifeLabelState, action) {
    switch (action.type) {
        case 'SET_LIFE_LABEL_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
export function lifeState(state = initialLifeState, action) {
    switch (action.type) {
        case 'SET_LIFE_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
