const initialMvState = {
    vehicle_type: 'Pilih',
    coverage: 'comprehensive',
    merek: 'Pilih',
    seri_tipe: 'Pilih',
    plat: 'Pilih',
    penggunaan:'Pribadi',
    tsi:0,
    build_year: 'Pilih',
    harga_aksesoris: 0,
    aksesoris:'',
    insurance_id:'',
}

const initialOtherDataKendaraan= {
	plat_tengah: '',
	plat_belakang: '',	
	label_coverage: 'comprehensive',
}

const initialDataKendaraan= {
  alamat_pemilik: '',
  kode_voucher: '',
  merek: '',
  mobile_apps: 'Y',
  nama_pemilik: '',
  nama_stnk: '',
  no_ktp: '',
  no_mesin: '',
  no_plat: '',
  no_rangka: '',
  penggunaan: '',
  photo_belakang: '',
  photo_depan: '',
  photo_kanan: '',
  photo_kiri: '',
  photo_ktp: '',
  photo_stnk: '',
  seri_tipe: '',
  status_qq: 'N',
  tgl_lahir: '',
  tittle: '',
  visitor_id: '',
  warna: ''
}

const initialPerluasanMV = {
  tpl_value: 10000000,
  pll_value: 10000000,
  padriver_value: 10000000,
  pap_value: 10000000,
  pap_people: 1,
  tpl: false,
  pll: false,
  padriver: false,
  pap: false,
  srcc: false,
  ts: false,
  eqvet: false,
  tsfwd: false,
  atpm: false,
}

export function perluasanMV(state = initialPerluasanMV, action) {
    switch (action.type) {
        case 'SET_PERLUASAN_MV':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}

export function dataKendaraan(state= initialDataKendaraan , action){
  switch (action.type) {
      case 'SET_DATA_KENDARAAN':
          return Object.assign({}, state, action.value);
	  case 'RESET_DATA_KENDARAAN':
          return state;
      default:
          return state;
  }
}

export function otherDataKendaraan(state= initialOtherDataKendaraan , action){
  switch (action.type) {
      case 'SET_LABEL_COVERAGE_KENDARAAN':
		  return Object.assign({}, state, {label_coverage:action.value});
	  case 'SET_PLAT_TENGAH_KENDARAAN':
		  return Object.assign({}, state, {plat_tengah:action.value});
	  case 'SET_PLAT_BELAKANG_KENDARAAN':
          return Object.assign({}, state, {plat_belakang:action.value});
      default:
          return state;
  }
}

export function asuransiMV(state= {} , action){
  switch (action.type) {
      case 'SET_ASURANSI_MV':
          return Object.assign({}, state, action.value);
      default:
          return state;
  }
}

export function mvState(state = initialMvState, action) {
    switch (action.type) {
        case 'SET_MV_STATE':
            return Object.assign({}, state, action.value);
        default:
            return state;
    }
}
