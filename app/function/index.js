import { PermissionsAndroid, AsyncStorage, View, Text, Alert, Dimensions, StyleSheet } from 'react-native';
//import firebase from 'react-native-firebase';
import Toast from 'react-native-same-toast';
import md5 from "react-native-md5";
import CheckBox from 'react-native-check-box';
import DeviceInfo from 'react-native-device-info';
import RNAmplitute from 'react-native-amplitude-analytics';
import Snackbar from 'react-native-android-snackbar';
import firebase from 'react-native-firebase';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import {
  Analytics,
  Hits as GAHits,
  Experiment as GAExperiment
} from 'react-native-google-analytics';


//firebase analytics
export const setFAScreen=(screenName, screenClassOverride = '')=>{ 
	firebase.analytics().setCurrentScreen(screenName, screenClassOverride);
}

export const setFACollectionEnabled=(enabled)=>{ 
	firebase.analytics().setAnalyticsCollectionEnabled(enabled);
}

export const setFALogEvent=(event, params = '')=>{ 
	firebase.analytics().logEvent(event, params);
}

export const setFAUserId=(id)=>{ 
	firebase.analytics().setUserId(id);
}

export const setFAUserProperty=(name, value)=>{ 
	firebase.analytics().setUserProperty(name, value);
}
//sort array object
export const sortGratis=(items)=>{ 
	return items;
}

//onesignal
export const setEmailToken=(email, token)=>{ 

  OneSignal.init("0526971b-920f-4d42-b154-ad31feda6d61");
		
		
		
		let emailAuthToken = token; //from your backend server

		OneSignal.setEmail(email, emailAuthToken, (error) => {
			if (error == undefined) {
				//successfully set email
				console.log('berhasil set email OK ' + email+ ' ' + emailAuthToken);

			} else {
				//encountered an error
				console.log('error set email YA');

			}
		});
}


export const logoutEmailOneSignal=()=>{
  OneSignal.init("0526971b-920f-4d42-b154-ad31feda6d61");
	OneSignal.logoutEmail((error) => {
		if (error) {
			console.log("Encountered error while attempting to log out: ", error);
		} else {
			console.log("Logged out successfully");
		}
	});
}


export const findLabel=(arr, id)=>{ 
	//console.log('findProvinceName ' + JSON.stringify(arr));
	//console.log('findProvinceName id ' + id);

	let obj = arr.find(o => o.key === id.toString());
  //console.log('obj', obj); 
  if (module.exports.isEmpty(obj)){
    return '';
  }
	return obj.label;
} 


 export const toArrSlider = (arr) => {

  var valarr = [];
  for (let i = 0; i < arr.length; i++){
    valarr.push(arr[i].sliderLink);
  }
  return valarr;
}

//notifications
export const uuidv4=()=>{
   return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export const objToArr=(obj)=>{
   var arr = Object.keys(obj).map(function(k) { return obj[k] });
   
   return arr;
}

export const arrObjToArr=(obj)=>{
   var arr = Object.keys(obj).map(function(k) { return obj[k] });
   
   return arr;
}

export const createChannelNotif=()=>{
	
	// const uniqueId = DeviceInfo.getUniqueID();
	// const deviceId = DeviceInfo.getDeviceId();
	// console.log('create channel '+ uniqueId+ ' ' + deviceId);
	// Build a channel
	// const channel = new firebase.notifications.Android.Channel(uniqueId, deviceId, firebase.notifications.Android.Importance.Max)
	  // .setDescription('Jagain Channel Notification');

	// Create the channel
	// firebase.notifications().android.createChannel(channel);
}

export const renewalNotif=()=>{
	// const uniqueId = DeviceInfo.getUniqueID();
	// const notification = new firebase.notifications.Notification()
	  // .setNotificationId(module.exports.uuidv4())
	  // .setTitle('Renewal Polis')
	  // .setBody('Masa berlaku Polis Anda akan segera berakhir pada tanggal... ');  
	  
	  // notification.android.setChannelId(uniqueId); 
	  
	  // Schedule the notification for 1 minute in the future
	// const date = new Date();
	 // date.setMinutes(date.getMinutes()+1);

	// firebase.notifications().scheduleNotification(notification, {
		// exact: true,
		// fireDate: date.getTime(),
		// timeZone: DeviceInfo.getTimezone(),
		// autoCancel: true,
	// })
	// console.log('create notif '+ uniqueId + ' ' + date.getTime() + ' ' + DeviceInfo.getTimezone());  
}

export const setScreenGA=(screen, userId = '')=>{
	let clientId = userId ? userId : DeviceInfo.getUniqueID();
    ga = new Analytics('UA-118268662-1', clientId, 1, DeviceInfo.getUserAgent());
    let screenView = new GAHits.ScreenView(
      'Jagain Android Apps',
      screen,
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);	
}

export const setImpressionGA=(impressions, userId = '')=>{
	let clientId = userId ? userId : DeviceInfo.getUniqueID();
    ga = new Analytics('UA-118268662-1', clientId, 1, DeviceInfo.getUserAgent());
	
	var gaImpression = new GAHits.Impression(
      impressions.package_id,
      impressions.package_name,
      "",
      impressions.insurance_name,
      impressions.lob,
      "",
      0, // Position
      impressions.price // Price
    );
    ga.add(gaImpression);
}

export const setTransactionGA=(transactions, userId = '')=>{
	let clientId = userId ? userId : DeviceInfo.getUniqueID();
    ga = new Analytics('UA-118268662-1', clientId, 1, DeviceInfo.getUserAgent());
	//transactionId, transactionAffiliation, transactionRevenue, transactionShipping, transactionTax, currency, experiment
	var gaTransaction = new GAHits.Transaction(
      transactions.kode_transaksi,
      transactions.insurance_name,
      0,
      0,
      transactions.tax,
	  "IDR",
	  ""
    );
    ga.send(gaTransaction);
}

export const setEventGA=(events, userId = '')=>{
	let clientId = userId ? userId : DeviceInfo.getUniqueID();
    ga = new Analytics('UA-118268662-1', clientId, 1, DeviceInfo.getUserAgent());
	
	 let gaEvent = new GAHits.Event(
      events.event_name,
      events.event_action,
      events.event_label,
      events.event_value,
      events.event_category
    );
    ga.send(gaEvent);
}


export const setLogEvent=(event, username)=>{
	const amplitude = new RNAmplitute('8248144134d1bae55b1b42b3');
		amplitude.logEvent(event, {username: username});
}

export const lobEvent=(namaEvent, username, lob = '')=>{
	
	var event = namaEvent;
	if(lob){
		event =  namaEvent + ' ' + lob;
	}
	module.exports.setLogEvent(event, username);
}

//stylesheet
export const sizeLogoAsuransi =(x = 1) =>{
  var {height, width} = Dimensions.get('window');
  return{
	  height:height*.0390625*x,
	  width:width*.278*x, 
	  resizeMode: 'contain'
	};
}

export const circle =(size) =>{
  return{
    width: size*.6,
    height: size*.6,
    borderRadius: 100/2,
    backgroundColor: '#f3f3f3',
  };
}

export const circleWhite =(size) =>{
  return{
    width: size*.6,
    height: size*.6,
    borderRadius: 100/2,
    backgroundColor: '#fff',
  };
}

export const circleImage =(size) =>{
  return{
    width: size*.6,
    height: size*.6,
  };
}

export const circleImageMore =(size) =>{
  return{
    width: size*.4,
    height: size*.4,
	top: size*.11,
	left: size*.1,
  };
}

export const circleLogo =(size) =>{
  return{
    width: size*.8,
    height: size*.8,
    borderRadius: 100/2,
    borderWidth: .5,
    backgroundColor: '#f3f3f3',
  };
}

export const circleLogoImage =(size) =>{
  return{
    width: size*.8,
    height: size*.8,
  };
}

export const buttonHomeMenu =(size) =>{
  return{
    height: size,
    width: size,
    backgroundColor: 'transparent'
  };
}

export const buttonHomeMenuWhite =(size) =>{
  return{
    height: size,
    width: size,
    backgroundColor: '#fff'
  };
}

export const homeMenu =(size, jumlah) =>{
  return{
    height: size*jumlah,
  };
}

//device info
export const deviceId = () =>{
  return DeviceInfo.getDeviceId();
}

export const deviceBrand = () =>{
  return DeviceInfo.getBrand();
}

export const deviceName = () =>{
  const deviceName = module.exports.deviceId() + ' ' + module.exports.deviceBrand();
  return deviceName;
}


//function
export const doUnshift = (initial, hasil) => {
  hasil.unshift(initial);
  return hasil;
}

export const validateEmail = (value) => {
    var x = value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        //email salah
        return true;
    }
	return false;
}

export const validateTelpon = (value) => {
	var reg = /(6|0)\d{9,15}/m;
	if (reg.test(value))
		return false;
	// kalau tidak +62 atau 0 dan panjang 9 sampai 13 karakter
	return true;
}

export const nomorKTP = (value) =>{
    var reg= /[^0-9]+\d{9,13}/g;

    if (reg.test(value))
		return false;
	// ktp
	return true;
  }


export const validateHandphone = (value) => {
	var reg = /(6|0)\d{9,16}/m;
	if (reg.test(value))
		return false;
	// kalau tidak 62 atau 0 dan panjang 9 sampai 13 karakter
	return true;
}
//get mime type

//function
export const todayDate = () => {
	var now = new Date();
	var today = module.exports.formatIsoDate(now); // 30-Dec-2011

	return today;
}

export const todayFullDate = () => {
	var now = new Date();
	var today = module.exports.formatFullDate(now); // 30-Dec-2011

	return today;
}

export const yesterdayDate = () => {
	var now = new Date();
	now.setDate(now.getDate() - 1);
	var yesterday = module.exports.formatIsoDate(now); // 30-Dec-2011

	return yesterday;
}

export const tommorowDate = () => {
	var now = new Date();
	now.setDate(now.getDate() + 1);
	var tommorow = module.exports.formatIsoDate(now);  // 30-Dec-2011

	return tommorow;
}

export const toNextYear = (val, date) => {

	var d = new Date(date);
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var toNextYear = new Date(year + val, month, day);

	return toNextYear;
}

export const toLastYear = (val, date) => {

	var d = new Date(date);
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var toLastYear = new Date(year - val, month, day);

	return toLastYear;
}


export const nextYearDate = () => {

	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var nextDateYear = new Date(year + 1, month, day);

	return nextDateYear;
}

export const lastYear = (value) => {

	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var lastDateYear = new Date(year - value, month, day);

	return lastDateYear;
}

export const nextYear = (value) => {

	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var nextDateYear = new Date(year + value, month, day);

	return nextDateYear;
}

export const add365Days = (value) => {

	var duedate = new Date(value);
	duedate.setDate(duedate.getDate() + 364);
	const newValue = module.exports.formatIsoDate(duedate);



	return newValue;
}

export const formatFullDate = (value) => {

  var duedate = new Date(value);
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = duedate.getDate();
  var monthIndex = duedate.getMonth();
  var year = duedate.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

export const formatIndoDate = (value) => {

  var duedate = new Date(value);
  var monthNames = [
    "Januari", "Februari", "Maret",
    "April", "Mei", "Juni", "Juli",
    "Agustus", "September", "Oktober",
    "November", "Desember"
  ];

  var day = duedate.getDate();
  var monthIndex = duedate.getMonth();
  var year = duedate.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


export const formatIsoDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

export const toIsoDate = (value) => {
	var newValue = module.exports.formatIsoDate(value);

	return newValue;
}

export const getTitle = (lob) => {
  var title = '';
  if(lob == 'MV'){
    title = 'Asuransi Kendaraan';
  }else if(lob == 'Property'){
    title = 'Asuransi Properti';
  }else if(lob == 'Apartment'){
    title = 'Asuransi Apartemen';
  }else if(lob == 'PA'){
    title = 'Asuransi Kecelakaan';
  }else if(lob == 'Health'){
    title = 'Asuransi Kesehatan';
  }else if(lob == 'Life'){
    title = 'Asuransi Jiwa';
  }else if(lob == 'Delay'){
    title = 'Asuransi Delay';
  }else if(lob == 'Travel'){
    title = 'Asuransi Perjalanan';
  }
  return title.toUpperCase();
}

export const height = () =>{
  const height = Dimensions.get('window').height;
  return Number(height);
}
export const width = () =>{
  const width = Dimensions.get('window').width;
  return Number(width);
}

export const passwordMD5 = (password) =>{
  let hex_md5v = md5.hex_md5( password );
  return hex_md5v;
}

//change status
export function changeStatus(status){
  return !status;
}

export const resetLoginData = async () =>{
  let dataLogin = {
    token: '',
    isLogin: false,
    device: '',
    lastAccess: '',
    loginTime: '',
    visitorID: '',
    visitorName: '',
    visitorPhone: '',
    User: '',
    dataTransaksi:'',
  };
  let STORAGE_object =  {
    dataLogin: dataLogin,
  };
  await AsyncStorage.mergeItem('STORAGE', JSON.stringify(STORAGE_object));
}

export const requestWritePermission = async() =>{
  try {
	const granted = await PermissionsAndroid.request(
	  PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
	  {
		'title': 'Ijin Penyimpanan',
		'message': 'Jagain membutuhkan akses ke penyimpanan ' +
				   'untuk melihat dan menyimpan polis asuransi.'
	  }
	)
	if (granted === PermissionsAndroid.RESULTS.GRANTED) {
	  module.exports.toastError('Akses Penyimpanan telah diberikan.');
	} else {
	  module.exports.toastError('Untuk dapat melihat dan menyimpan Polis anda harus memberi akses penyimpanan.');
	}
  } catch (err) {
	console.warn(err)
  }
}

export const checkWritePermission = async() =>{
	const permissionWrite = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
	//module.exports.toastError('permissionWrite: ', permissionWrite);
	if(!permissionWrite){
		module.exports.requestWritePermission();
	}
}

export const emptyObject = (obj)=>{

    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});

}

 export const toObject = (arr) => {
  var val = {key:'Pilih', label: 'Pilih'};
  var valarr = [];
  for (let i = 0; i < arr.length; i++){
    valarr.push({key:arr[i],label:arr[i]});
  }
  valarr.unshift(val);
  return valarr;
}

export const toPlatObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      label = arr[i].plat + ' - ' + arr[i].kabupaten;
      val = Object.assign({}, val,{ key:arr[i].plat, label:label });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toProvinsiObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      //label = arr[i].provinceId + ' - ' + arr[i].provinceName;
      val = Object.assign({}, val,{ key:arr[i].provinceId, label: arr[i].provinceName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toCityObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      //label = arr[i].provinceId + ' - ' + arr[i].provinceName;
    if (arr[i].cityId !== '0'){
      val = Object.assign({}, val,{ key:arr[i].cityId, label: arr[i].cityName });
      valarr.push(val);
    }
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toCountryObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      //label = arr[i].provinceId + ' - ' + arr[i].provinceName;
    if (arr[i].countryId !== '0') {
      val = Object.assign({}, val,{ key:arr[i].countryId, label: arr[i].countryName });
      valarr.push(val);
    }
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}


export const toDaftarKeluargaTravelObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      val = Object.assign({}, val,{ key:arr[i].idDaftarKeluarga, label: arr[i].keterangan });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  return valarr;
}

export const toPaketTravelObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      val = Object.assign({}, val,{ key:arr[i].typeName, label: arr[i].typeAlias });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  return valarr;
}


export const toAhliWarisObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].districtId + ' - ' + arr[i].districtName;
      val = Object.assign({}, val,{ key:arr[i].ahliwarisId, label:arr[i].ahliwarisName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toDistrictObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].districtId + ' - ' + arr[i].districtName;
      val = Object.assign({}, val,{ key:arr[i].districtId, label:arr[i].districtName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toKelurahanObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
      //label = arr[i].kelurahanId + ' - ' + arr[i].kelurahanName;
      val = Object.assign({}, val,{ key:arr[i].kelurahanId, label:arr[i].kelurahanName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toKecamatanObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].kecamatanId + ' - ' + arr[i].kabupaten;
      val = Object.assign({}, val,{ key:arr[i].kecamatanId, label:arr[i].kecamatanName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toOkupasiApartmentObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].kecamatanId + ' - ' + arr[i].kabupaten;
      val = Object.assign({}, val,{ key:arr[i].lantai, label:arr[i].occupationName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toOkupasiPropertyObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';

  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].kecamatanId + ' - ' + arr[i].kabupaten;
      val = Object.assign({}, val,{ key:arr[i].jenisProperty, label:arr[i].occupationName });
      valarr.push(val);
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toAccidentObject = (arr) => {
  var val = {key:'', label: 'Pilih'};
  var valarr = [];
  var label = '';
  //console.log('arr',arr);
  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].kecamatanId + ' - ' + arr[i].kabupaten;
    if (arr[i].status == 'Y'){
      //console.log('arry',arr[i]);
      val = Object.assign({}, val,{ key:arr[i].pekerjaan, label:arr[i].detailPekerjaan });
      valarr.push(val);
    }
  }
  val = {key:'Pilih', label: 'Pilih'};
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const toLifeObject = (arr) => {
  var val = { key: '', label: 'Pilih' };
  var valarr = [];
  var label = '';
  console.log('arr', arr);

  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].kecamatanId + ' - ' + arr[i].kabupaten;
    if (arr[i].statusJiwa == 'Y') {
      console.log('arry', arr[i]);
      val = Object.assign({}, val, { key: arr[i].pekerjaan, label: arr[i].detailPekerjaan });
      valarr.push(val);
    }
  }
  val = { key: 'Pilih', label: 'Pilih' };
  valarr.unshift(val);
  //console.log('toplat',valarr);
  return valarr;
}

export const yearlist = () => {
  var yearlist = [];
  var yearRangeList = [];
  for (i = new Date().getFullYear(); i > new Date().getFullYear()-11; i--) {
    yearlist.push(i);
  }
  yearRangeList = module.exports.toObject(yearlist);
  return yearRangeList;
}

export const setDataLogin = async (dataLogin) =>{
  //console.log('ini data login');
  //console.log(dataLogin);
  let STORAGE_object =  {
    dataLogin: dataLogin,
  };
  await AsyncStorage.mergeItem('STORAGE', JSON.stringify(STORAGE_object));
//  console.log('success async');
}

export const formatDate = (date) => {
   var monthNames = [
     "January", "February", "March",
     "April", "May", "June", "July",
     "August", "September", "October",
     "November", "December"
   ];

   var day = date.getDate();
   var monthIndex = date.getMonth();
   var year = date.getFullYear();

   return day + ' ' + monthNames[monthIndex] + ' ' + year;
 }

 export const numToCurrency = (value) => {
     //var val = value.replace(/[^0-9\.]+/g,"");
     var bilangan = Number.parseInt(value, 10);

     var reverse = bilangan.toString().split('').reverse().join(''),
       ribuan 	= reverse.match(/\d{1,3}/g);
       ribuan	= ribuan.join(',').split('').reverse().join('');

     // Cetak hasil
    // console.log(ribuan); // Hasil: 23.456.789
     return ribuan;
   }

   export const currencyToNum = (value) => {
       //var vp = vehiclePrice.replace(/[^0-9\.]+/g,"");
       var bilangan = 0;

       if(value !== "" && value !== undefined && value !== null && value !== 0){
         bilangan = Number(value.replace(/[^0-9\.]+/g,""));
       }
       return bilangan;
     }

   export const anySymAlphaNumeric = (value) =>{
     var newVal= value.replace(/[^a-zA-Z0-9_ .-]+/g, "");

     return newVal;
   }

   export const emailCharacterOnly = (value) =>{
     var newVal= value.replace(/[^a-zA-Z0-9_.-@]+/g, "");

     return newVal.toLowerCase();
   }

  export const alphabetOnly = (value) =>{
    var alphabet = value.replace(/[^a-zA-Z]+/g, "");

    return alphabet;
  }

  export const alphaNumericDashOnly = (value) =>{
    var alphabet = value.replace(/[^a-zA-Z0-9-]+/g, "");

    return alphabet;
  }

    export const numericPlusOnly = (value) =>{
    var alphabet = value.replace(/[^a-zA-Z0-9+]+/g, "");

    return alphabet;
  }

    export const alphabetSpaceOnly = (value) =>{
    var alphabet = value.replace(/[^a-zA-Z ]+/g, "");

    return alphabet;
  }

  export const numericOnly = (value) =>{
    var numeric= value.replace(/[^0-9]+/g, "");

    return numeric;
  }

  export const alphaNumericOnly = (value) =>{
    var alphaNumeric = value.replace(/[^a-zA-Z0-9]+/g, "");

    return alphaNumeric;
  }

  export const dotalphaNumericOnly = (value) =>{
    var alphaNumeric = value.replace(/[^a-zA-Z0-9 .,\n]+/g, "");

    return alphaNumeric;
  }

//service get token
export const getDataFromToken = async (token) => {
  //console.log('my token ini');
  //console.log(token);
  //  if(token !== ''){

 await fetch( module.exports.serviceURL + 'session/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': token,
      }
    }).then((response) => response.json())
          .then((responseJson) => {
            //console.log(responseJson);
            //  console.log("username: " +responseJson.username);
            if(responseJson.username !== ""){

              let dataLogin = {
                User:responseJson.username,
                isLogin: true,
                device: responseJson.device,
                lastAccess: responseJson.last_access,
                loginTime: responseJson.login_time,
                visitorID: responseJson.visitor_id,
                visitorName: responseJson.visitor_name,
                visitorPhone: responseJson.visitor_phone,
              };

              //console.log('berhasil login');
              module.exports.setDataLogin(dataLogin);

          //  return token_user;
           }else{
             module.exports.resetLoginData().done();
            //  return token_user;
            }
    }).catch((error) => {
            console.error(error);
            Alert.alert(
              'Error',
              'There seems to be an issue connecting to the network.'
            );
          //  return 'There seems to be an issue connecting to the network';
    });

//  }else{
    //this.setState({isLoading: false,});
  //  module.exports.resetLoginData();
    //console.log('token kosong');
//  }
}

//service aktivasi
export const activateEmail = async (email) => {

  if(email !== ''){

    await fetch( module.exports.serviceURL + 'active/'+ email + '/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => response.json())
          .then((responseJson) => {
           console.log(responseJson);
          module.exports.toastError(responseJson.description);
        //  return responseJson;
    }).catch((error) => {
            console.error(error);
            Alert.alert(
              'Error',
              'There seems to be an issue connecting to the network.'
            );
          //  return 'There seems to be an issue connecting to the network';
    });
  }else{
    //this.setState({isLoading: false,});
    module.exports.toastError('email belum diisi');

  }
}

//service logout
export const logout = async (token) => {

  if(token !== ''){

    await fetch( module.exports.serviceURL + 'logout/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': token,
      }
    }).then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);

            module.exports.resetLoginData().done();

            if(responseJson.description !== 'session timeout'){

              module.exports.toastError(responseJson.description);
            }else{

              module.exports.toastError(responseJson.description);
            }

    }).catch((error) => {
            console.error(error);
            Alert.alert(
              'Error',
              'There seems to be an issue connecting to the network.'
            );
          //  return 'There seems to be an issue connecting to the network';
    });

  }else{
    //this.setState({isLoading: false,});
    module.exports.resetLoginData().done();

  }
}

export const tolowerObj= (obj) => {
	var key, keys = Object.keys(obj);
	var n = keys.length;
	var newobj={}
	while (n--) {
	  key = keys[n];
	  newobj[key.toLowerCase()] = obj[key];
	}
	return newobj;
}

export const isEmpty = (obj) => {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export const checkSession = async () => {
  //import getDataFromToken;
  const result = await AsyncStorage.getItem('STORAGE');

}

export const toastError = (message) =>{
   Toast.showWithGravityAndOffset(message, Toast.SHORT, Toast.BOTTOM,  25, 50);
   /*
   Snackbar.show(message, {
	  duration: Snackbar.LONG,
      actionLabel: 'TUTUP',
      actionCallback: (() => Snackbar.dismiss())
    });
	*/
}

export const navscreen = (screen) =>{
  var listscreen = ['Home'];

  if(screen == "MV"){
    listscreen.push('MV','ListAsuransiMV','Login','DetailBeliMV','DetailPribadiMV','PembayaranMV','PaymentGateway');
  }else if(screen == "Property"){
    listscreen.push('Property','ListAsuransiProperty','Login','DetailBeliProperty','DetailPribadiProperty','PembayaranProperty','PaymentGateway');
  }else if(screen == "Apartment"){
    listscreen.push('Apartment','ListAsuransiApartment','Login','DetailBeliApartment','DetailPribadiApartment','PembayaranApartment','PaymentGateway');
  }else if(screen == "PA"){
    listscreen.push('PA','ListAsuransiPA','Login','DetailBeliPA','DetailPribadiPA','PembayaranPA','PaymentGateway');
  }else if(screen == "User"){
    listscreen.push('Login', 'Profile', 'Register','Verification', 'IndexForgotPassword','ForgotPassword');
  }else if(screen == "MyCart"){
    listscreen.push('PendingPayment');
  }
  return listscreen;
}

export const toObjectTipeInsiden = (arr) => {

  var val = { key: 'Pilih', label: 'Pilih' };
  var valarr = [];
  for (let i = 0; i < arr.length; i++) {
    valarr.push({ key: arr[i].tipeKejadian, label: arr[i].namaKejadian });
  }
  valarr.unshift(val);
  // console.log('arr', arr);
  // console.log('toObjectTipeInsiden', valarr);
  return valarr;
}

/* delete annual city/country */
export const mergeArrayObject = (array) => {
  var output = array.reduce(function (o, cur) {

    // Get the index of the key-value pair.
    var occurs = o.reduce(function (n, item, i) {
      return (item.payment_id === cur.payment_id) ? i : n;
    }, -1);

    // If the name is found,
    if (occurs >= 0) {

      // append the current value to its list of values.
      o[occurs].order_id = o[occurs].order_id.concat(cur.order_id);

      // Otherwise,
    } else {

      // add the current item to o (but make sure the value is an array).
      var obj = { payment_id: cur.payment_id, order_id: [cur.order_id], link_pembayaran: cur.link_pembayaran, input_date: cur.input_date};
      o = o.concat([obj]);
    }

    return o;
  }, []);

  console.log('output',output);
  return output;
}
export const reverseDateTime = (date) => {
  date.split("-").reverse().join("-");
}

/* find */
export const findKodeTransaksi = (arr, id) => {
  //console.log('findProvinceName ' + JSON.stringify(arr));
  //console.log('findProvinceName id ' + id);

  let obj = arr.find(o => o.kode_transaksi === id.toString());
  //console.log('obj', obj); 
  if (module.exports.isEmpty(obj)) {
    return {};
  }
  return obj;
} 

/*end delete annual city/country */

/*
group by 
*/
export const groupBy = (list, keyGetter) => {
  const map = new Map();
  list.forEach((item) => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}


export const newArrayFilter = (array) => {
var count = 0;
  for (var i = 0; i < array.length; ++i) {
    if (array[i].status == "Y")
      count++;
  }
  return count;
}
/*
reactend group by 
*/

export const getValidPolicy = (arr, serverDate) => {
  var val = {};
  var valarr = [];
  var endpolis = null;
  var now = new Date(serverDate);
  for (var i = 0; i < arr.length; i++) {
    //  label = arr[i].kecamatanId + ' - ' + arr[i].kabupaten;
    if (arr[i].status == 'Y') {
      endpolis = new Date(arr[i].policy_enddate);
      //console.log('arry', arr[i]);
      if (endpolis > now) {
        val = Object.assign({}, val, arr[i]);
        valarr.push(val);
      }
    }
  }
  
  //console.log('toplat',valarr);
  return valarr;
}

export const findIndexOrderId = (arr, id, serverDate) => {
  const newarr = module.exports.getValidPolicy(arr, serverDate);
  console.log('findIndex ' + JSON.stringify(newarr));
  console.log('findIndexName id ' + id);

  let index = newarr.findIndex(o => o.kode_transaksi === id.toString());
  console.log('index', index); 
  if (newarr.length <= 0) {
    return -1;
  }
  return index;
} 


export const findNameArray = (newarr, id) => {
  let index = newarr.find(o => o.name === id.toString());
  //console.log('index', index);
  if (newarr.length <= 0) {
    return 0;
  }
  return index;
} 

export const getNameProfile = (arr) => {
  var newarr = ['Tambah Profil'];

  console.log('arr',arr);
  let i;
  if(arr.length > 0){
    console.log('name', arr);
    for(i=0; i < arr.length; i++){
      newarr.push(arr[i].name);
    }
  }
  console.log(newarr);
  return newarr;
} 
//export const serviceURL = 'http://117.102.73.16:8081/';
//export const devURL = 'http://10.222.6.85:8081/';
//web live jagain
export const serviceURL = 'http://api.jagain.com:8081/'; 
export const serviceURL110 = 'http://10.222.10.110:8081/'; 

export const webJagain = 'https://www.jagain.com/';
//webdev jagain
//export const webJagain = 'http://10.222.10.200/jagain/';
//andre URL
//export const serviceURL = 'http://10.222.10.110:8080/';