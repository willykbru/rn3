import Autocomplete from 'react-native-autocomplete-input';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
 
//var customData = require('./data.json');
 
//const API = 'https://swapi.co/api';
//const ROMAN = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII'];
 
export default class DataAutoComplete extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      list: [],
      query: '',
      searchedNama : [],
	  textInputStyle:{
			borderBottomColor: 'black',
			borderBottomWidth: .5,
		},	
    };
  }
  
  onFocus(color = '#00e500', width = 3) {
    this.setState({textInputStyle:{
      borderBottomColor: color,
		borderBottomWidth: width
    },

	})
  }

  onBlur(color = 'black', width = .5) {
    this.setState({textInputStyle:{
      borderBottomColor:color,
		borderBottomWidth: width
    },
	})
  }
  
 
  componentDidMount(){
      const list = this.props;
      this.setState({people});
  }
 
 
  findInList(query){
    if(query === ''){
      return [];
    }
 
    const {list} = this.state;
 
    const regex = new RegExp(`${query.trim()}`, 'i');
 
    return list.filter(list => list.data.search(regex) >= 0);
  }
 
  render(){
 
    //const {query} = this.props;
 
    //const people = this.findFilm(query);
    const hasilCari = this.findInList(query);
    //console.log(hasilCari);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
	
	const {
	  TIWidth,
	  title,
	  autoCapitalize,
	  onChangeText,
	  colorOnFocus, 
	  widthOnFocus,
	  colorOnBlur, 
	  widthOnBlur, 
	  value,
	  keyboardType,
	  query
	  } = this.props;
	  

	  
	  var textInputWidth = null;
	  var tipeKeyboard = null;
	  
	  var placeholder = title;
   
 
    // sambungan
    return(
      <View style={styles.container}>
        <Autocomplete
          autoCapitalize={autoCapitalize}
          autoCorrect = {false}
          containerStyle={styles.autocompleteContainer}
          data={hasilCari.length === 1 && comp(query, hasilCari[0].data) ? [] : hasilCari}
		  onChangeText={(value) => {onChangeText? onChangeText(value): null}}
          defaultValue={value}
		  onBlur={()=>this.onBlur()}
		  onFocus={()=>this.onFocus()}
		  style={[styles.baseText, this.state.textInputStyle, styles.padv0 ]}
		  placeholder={placeholder}
          renderItem={({data}) => (
            <TouchableOpacity onPress={() => this.setState({query : data})}>
              <Text style={[styles.baseText]}>
                {data}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    paddingTop: 25, 
	width: '80%'
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  descriptionContainer: {
    // `backgroundColor` needs to be set otherwise the
    // autocomplete input will disappear on text input.
    backgroundColor: '#F5FCFF',
    marginTop: 25
  },
  infoText: {
    textAlign: 'center'
  },
  titleText: {
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center'
  },
  directorText: {
    color: 'grey',
    fontSize: 12,
    marginBottom: 10,
    textAlign: 'center'
  },
  openingText: {
    textAlign: 'center'
  }
});