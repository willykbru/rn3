import React, {Component} from 'react';
import { View, Text, Dimensions, TextInput } from 'react-native';
import styles from '../../assets/styles';


export default class JagainTextInputEditable extends Component{
	
  constructor(props) {
    super(props);
	this.state={
		textInputStyle:{
			borderBottomColor: 'black',
			borderBottomWidth: .5,
		},	
		
	}
  }
  
  onFocus(color, width) {
    this.setState({textInputStyle:{
      borderBottomColor: color,
		borderBottomWidth: width
    },

	})
  }

  onBlur(color = 'black', width = .5) {
    this.setState({textInputStyle:{
      borderBottomColor:color,
		borderBottomWidth: width
    },
	})
  }
  
  render(){
	  const {
	  TIWidth,
	  title,
	  autoCapitalize,
	  onChangeText,
	  colorOnFocus, 
	  widthOnFocus,
	  colorOnBlur, 
	  widthOnBlur, 
	  value,
	  maxLength,
	  multiline,
	  numberOfLines,
	  keyboardType,
	  editable, 
	  style
	  } = this.props;
	  

	  
	  var textInputWidth = null;
	  var tipeKeyboard = null;
	  
	  var placeholder = title;
	  
	  
	  if(!TIWidth){
		textInputWidth = styles.width80p;
	  }else{
		textInputWidth = {width: TIWidth};
	  }
	  
	  if(keyboardType){
		tipeKeyboard = keyboardType;
	  }else{
		tipeKeyboard = 'default';
	  }
	  
	  

	   

	  return(
	  <View style={[textInputWidth]}>
			<Text style={[styles.baseText]}>{value ? title: null}</Text>    
	  { !maxLength ? 
				<View>
				{ !multiline ? 
				
				<TextInput
					onBlur={()=>this.onBlur(colorOnBlur, widthOnBlur)}
					onFocus={()=>this.onFocus(colorOnFocus, widthOnFocus)}
					value={value}
					keyboardType={tipeKeyboard}
					autoCapitalize = {autoCapitalize}
					onChangeText={(value) => {onChangeText? onChangeText(value): null}}
					underlineColorAndroid='transparent'
					style={[styles.baseText, this.state.textInputStyle, styles.padv0,style ]}
					placeholder={placeholder}
					editable={editable}
				/>
				:
				<TextInput
					onBlur={()=>this.onBlur(colorOnBlur, widthOnBlur)}
					onFocus={()=>this.onFocus(colorOnFocus, widthOnFocus)}
					value={value}
					keyboardType={tipeKeyboard}
					autoCapitalize = {autoCapitalize}
					onChangeText={(value) => {onChangeText? onChangeText(value): null}}
					underlineColorAndroid='transparent'
					style={[styles.baseText, this.state.textInputStyle, styles.padv0,style]}
					multiline = {multiline}
					numberOfLines = {numberOfLines}
					placeholder={placeholder}
					editable={editable}
				/>
				}
				</View>
		:      
			<View>
			{ !multiline ? 
			<TextInput
				onBlur={()=>this.onBlur(colorOnBlur, widthOnBlur)}
				onFocus={()=>this.onFocus(colorOnFocus, widthOnFocus)}
				value={value}
				keyboardType={tipeKeyboard}
				autoCapitalize = {autoCapitalize}
				onChangeText={(value) => {onChangeText? onChangeText(value): null}}
				underlineColorAndroid='transparent'
				style={[styles.baseText, this.state.textInputStyle, styles.padv0,style]}
				maxLength={maxLength}
				placeholder={placeholder}
					editable={editable}
			/>
			:
			<TextInput
				onBlur={()=>this.onBlur(colorOnBlur, widthOnBlur)}
				onFocus={()=>this.onFocus(colorOnFocus, widthOnFocus)}
				value={value}
				keyboardType={tipeKeyboard}
				autoCapitalize = {autoCapitalize}
				onChangeText={(value) => {onChangeText? onChangeText(value): null}}
				underlineColorAndroid='transparent'
				style={[styles.baseText, this.state.textInputStyle, styles.padv0,style]}
				maxLength={maxLength}
				multiline = {multiline}
				numberOfLines = {numberOfLines}
				placeholder={placeholder}
					editable={editable}
			/>
			}
			</View>
		}
		</View>
    
	  );
	  
  }

}