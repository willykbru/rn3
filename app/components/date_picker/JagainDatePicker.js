import React, {Component} from 'react';
import { View, Text, Dimensions, TextInput } from 'react-native';
import styles from '../../assets/styles';
import DatePicker from 'react-native-datepicker';


export default class JagainDatePicker extends Component{

  constructor(props) {
    super(props);

  }


  render(){
	  const {
	  title,
	  date,
	  minDate,
	  maxDate,
	  iconSource,
	  borderColor,
	  placeholder,
	  onDateChange,
		formatDate,
		disabled,
	  } = this.props;
		var format = formatDate;
	  var borderCol = 'rgba(255,255,255,0)';
		if(!formatDate){
			format = "YYYY-MM-DD";
		}
		var dsbl = false;

	  if(borderColor){
		borderCol = borderColor;
	  }
	  //console.log('date: '+date+' '+title)
	  if(disabled){
		  dsbl = true;
	  }

	  return(
	  <View style={[styles.pickerViewBlack]}>
			<Text style={[styles.baseText]}>{date ? title : ''}</Text>
			  <DatePicker
				style={styles.width200}
				date={date}
				androidMode="spinner"
				mode="date"
				placeholder={placeholder}
				format={format}
				disabled={dsbl}
				minDate={minDate}
				maxDate={maxDate}
				iconSource={iconSource}
				confirmBtnText="Confirm"
				cancelBtnText="Cancel"
				customStyles={{
				  dateIcon: {
					position: 'absolute',
					left: 0,
					top: 4,
					marginLeft: 0
				  },
				  dateInput: {
					marginLeft: 36,
					borderColor: borderCol
				  }
				  // ... You can check the source to find the other keys.
				}}
				onDateChange={(value) => onDateChange(value)}
			  />
		</View>

	  );

  }

}
