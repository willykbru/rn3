import React, {Component} from 'react';
import { View, ActivityIndicator, Text, Dimensions } from 'react-native';

import styles from '../../assets/styles';

export default class LoadingPage extends React.Component{
	 constructor(props){
		super(props);
	  }


	render(){
	 var {height, width} = Dimensions.get('window');	
		
		return(
			<View style={[styles.centerFlexContent, styles.directionRow, styles.pad10]}>
			  <View style={{height: height*.3}} />
              <View style={styles.directionColumn}>
                <ActivityIndicator size="large" color="grey" />
                <Text style={[styles.colorGrey, styles.baseText]}>
                  Silakan Tunggu
                </Text>
              </View>
            </View> 
		);
	}
}