import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(204,0,0,0.7)',
  },
  closeContainer: {
    flex: 0.08,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: 30,
  },
  menuContainer: {
    flex: 0.52,
  },
  menus: {
    color: 'white',
    fontSize: 20,
    marginLeft: 30,
    marginBottom: 30,
  },
  text: {
    color: 'white',

  }
});

export default styles;
