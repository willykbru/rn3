'use strict';
import React, { PropTypes } from 'react';
import {
  AsyncStorage,
  Text,
  TouchableOpacity,
  View,
  Image,
  ListView,
} from 'react-native';
import styles from './style';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import * as appFunction from '../../function';

class DrawerComponent extends React.Component {
  constructor(props) {
    super(props);
    this.nav = this.nav.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.pressLink = this.pressLink.bind(this);
  }

  closeDrawer() {
    this.props.drawerMethods.closeDrawer();
  }

  pressLink(screen) {
    this.props.resetRoute(screen);
    this.props.drawerMethods.closeDrawer();
  }

  nav(screen){
      this.props.resetRoute(screen);
  }

  doLogout(token, screen){
	  console.log(token);
	  appFunction.logout(token).done();
    //this.getLocalStorage().done();
    this.setState({isLogin: false, token: ''});
    if(screen !== ''){
      this.nav('Login');
    }
	  this.props.drawerMethods.closeDrawer();

  }

  logoutMenu(isLogin, token, screen){
	  if(isLogin !== false){
		  return(
        <View>
        <TouchableOpacity onPress={() => this.pressLink('PendingPayment')}>
          <Text style={styles.menus}>Pending Payment</Text>
        </TouchableOpacity>
			  <TouchableOpacity onPress={() => this.doLogout(token, screen)}>
				    <Text style={styles.menus}>Log Out</Text>
			  </TouchableOpacity>
      </View>
		  );
	  }else if(isLogin !== true){
      return(
        <View>
        <TouchableOpacity onPress={() => this.pressLink('Login')}>
          <Text style={styles.menus}>Login</Text>
        </TouchableOpacity>
      </View>
     );
    }
  }


  render() {
    //const {isLogin, token} = this.state;

    return (
      <View style={styles.container}>
		    <TouchableOpacity style={styles.closeContainer}>
			     <Text></Text>
        </TouchableOpacity>
        <View style={styles.menuContainer}>
          <TouchableOpacity onPress={() => this.pressLink('Home')}>
            <Text style={styles.menus}>Home</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
   }
}

function mapStateToProps(state) {
  return {
    navReducer: state.navReducer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent);
