import React, {Component} from 'react';
import { View, Text, Dimensions, TextInput } from 'react-native';
import styles from '../../assets/styles';


export default class JagainErrorText extends Component{

  constructor(props) {
    super(props);
  }
  


  render(){
	  const {
	     textError,		 
		 style,
		 ref,
	  } = this.props; 
		
	
		
	  return(
	  <View style={[styles.width80p]}>
			<TextInput 
			  ref={ref}
			  editable={false} 
			  style={[styles.baseText, styles.padv0]} 
			  underlineColorAndroid='transparent' 
			  value={textError} 
			  style={[styles.baseText, {color: 'red'}]} />
		</View>

	  );

  }

}
