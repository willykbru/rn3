import React, {Component} from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import styles from '../../assets/styles';

export default class SubmitButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      title,
      onPress,
    } = this.props;

    return (
      <View style={[styles.top20, styles.width80p, styles.bottom20, styles.elev3]}>
        <TouchableHighlight
          style={[styles.btnForgotPassword]}
          onPress={this.props.onPress}
          underlayColor='#fff'>
            <Text style={[styles.btnLoginText, styles.baseText]}>{title}</Text>
        </TouchableHighlight>
      </View>
    )
  }
}
