import React, {Component} from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import styles from '../../assets/styles';

export default class UploadButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      title,
      onPress,
    } = this.props;

    return (
      <View style={[styles.btnViewUpload]}>
        <TouchableHighlight
          style={[styles.btnUpload, styles.elev2]}
          onPress={this.props.onPress}
          underlayColor='#fff'>
            <Text style={[styles.btnLoginText, styles.baseText]}>{title}</Text>
        </TouchableHighlight>
      </View>
    )
  }
}
