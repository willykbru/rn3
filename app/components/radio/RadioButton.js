import React, {Component} from 'react';
import { View, Text, Dimensions, TouchableHighlight } from 'react-native';
import styles from '../../assets/styles';
import {ListItem,TouRight,Left,Body,Radio,Container, Content,Footer, Header,Card, CardItem, Icon} from 'native-base';

export default class RadioButton extends Component{
  constructor(props) {
    super(props);
  }

  render(){
    const {
      selected,
      lbl,
	  onPress1,
	  onPress2,
	  direction,
    } = this.props;
	var directionstyle = [];
    //console.log(tooltipText);
 	  
	 var init1 = true;
	var init2 = false;
	
    if(selected){
      init1 = true;
	  init2 = false;
    }else{
      init1 = false;
	  init2 = true;
    }	
	
	if(direction == 'column'){
		directionstyle.push(styles.directionColumn);
	}else{
		directionstyle.push(styles.directionRow);
	}
	
    return(
	 <View style={directionstyle}>
          <View style={[styles.directionRow, styles.right10]}>
              <Radio selected={init1} onPress={onPress1} />
           <Text style={[styles.baseText]}> {lbl.label1}</Text>
		   </View>
		   <View style={[styles.directionRow]}>
		     <Radio selected={init2} onPress={onPress2} />
			 <Text style={[styles.baseText]}> {lbl.label2}</Text>
			</View>
      </View>
    );
    
  }
}
