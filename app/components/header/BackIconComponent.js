import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text
} from 'react-native';
import {Icon} from 'native-base';

const styles = StyleSheet.create({
  menuIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 15,
  },
});

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../../actions';

class BackIconComponent extends React.Component {
  constructor(props) {
    super(props);
    this.pressMenu = this.pressMenu.bind(this);
  }

  pressMenu() {
    this.props.resetRoute('Home');
  }

  render() {
    return (
      <TouchableOpacity style={styles.menuIcon} onPress={this.pressMenu}>
        <Icon style={{color: 'white'}}  name='arrow-round-back' />
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state) {
  return {
    navReducer: state.navReducer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BackIconComponent);
