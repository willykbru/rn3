import React, { Component } from 'react';
import {
    Platform,
    TouchableHighlight,
    StyleSheet,
    Text,
    View,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';


import Picker from 'react-native-wheel-picker'
var PickerItem = Picker.Item;

export default class WheelPicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedItem: 0,
            itemList: this.props.itemList,
            isVisible: false
        };
    }

    onPickerSelect(index) {
        this.setState({
            selectedItem: index,
        })
    }

    onAddItem = () => {
        var name = '司马懿'
        if (this.state.itemList.indexOf(name) == -1) {
            this.state.itemList.push(name)
        }
        this.setState({
            selectedItem: this.state.itemList.indexOf(name),
        })
    }

    componentWillUnmount(){
        this.setState({ isVisible: false});
    }

    render() {
        const { isVisible } = this.state;
        return (
            <View>
            <TouchableHighlight underlayColor="blue" onPress={()=>{this.setState({isVisible: true})}}>
                <Text>Tambah Peserta</Text>
            </TouchableHighlight>
            <Modal
                    onRequestClose={() => null}
                    animationType="slide"//slide
                    visible={isVisible}
                    transparent={true}>
                    <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#44404052", padding: 10, justifyContent: "center" }}>
                            <TouchableWithoutFeedback onPress={() => this.setState({ isVisible: false })}>
                                <View style={{ opacity: 0, flex: 1 }} />
                        </TouchableWithoutFeedback>
                                <View style={styles.container}>
                            <View style={[{ backgroundColor:'lightgray', height: '20%', width:'100%'}, styles.centerItemContent]}>
                            <Text style={[styles.baseText,{color: 'grey'}]}>Pilih Profil</Text>
                                    </View>
                <Picker style={{ width: '100%', height: '40%' }}
                    selectedValue={this.state.selectedItem}
                    itemStyle={{ borderColor: 'black',color: "black", fontSize: 26 }}
                    onValueChange={(index) => this.onPickerSelect(index)}>
                    {this.state.itemList.map((value, i) => (
                        <PickerItem label={value} value={i} key={"money" + value} />
                    ))}
                </Picker>
                            <View style={[{ height: '20%' }, styles.centerItemContent]}>
                <Text style={{ margin: 20, color: '#000' }}>
                    Nama Terpilih：{this.state.itemList[this.state.selectedItem]}
                </Text>
                </View>
                            <View style={[{ height: '20%' }, styles.centerItemContent]}>
                <Text style={{ margin: 20, color: '#000' }}
                    onPress={this.onAddItem}>
                    Tambah Item
				</Text>
                </View>

                </View>
                </View>
             
            </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'blue',
        borderWidth: .5,
        borderColor: 'white',
        width: '100%',
        height: '30%'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});