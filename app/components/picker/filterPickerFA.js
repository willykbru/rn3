import React, {Component} from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import styles from '../../assets/styles';

export class FilterPickerFA extends React.Component {

  constructor(props) {
    super(props);
  }


  render() {
    let {
      title,
      value,
      options,
      iconName,
      bordered,
      onSelect,
      onCancel,
      onShow,
      visible,
    } = this.props;

    var icon = iconName;
    var mystyle = [styles.pickerViewBlack];
    if (!icon){
      icon = 'ios-arrow-down';
    }
    var border = []
    if(bordered){
      border= [styles.directionRow, styles.flex2, styles.spaceBetween, styles.border1, styles.borderColorGrey];
      mystyle = [styles.pickerViewTr];
      icon='md-arrow-dropdown';
    }else{
      border= [styles.directionRow, styles.flex2, styles.spaceBetween];
    }




    return (
      <View style={mystyle}>
        <Text style={[styles.font16, styles.baseText]}>
          {title}
        </Text>
        <TouchableOpacity onPress={onShow}>
          <View style={ border }>
            <Text style={[styles.font16, styles.top10, styles.baseText]}>
              {value ? value : 'Pilih'}
            </Text>
            <Icon name={icon} />
          </View>
        </TouchableOpacity>
        <ModalFilterPicker
          visible={visible}
          onSelect={onSelect}
          onCancel={onCancel}
          options={options}

        />
      </View>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(null, mapDispatchToProps)(FilterPickerFA);
