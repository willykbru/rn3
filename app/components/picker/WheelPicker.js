import React, { Component } from 'react';
import {
    Platform,
    TouchableHighlight,
    StyleSheet,
    Text,
    View,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Alert, 
    Dimensions
} from 'react-native';
import styles from '../../assets/styles'; 
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import * as appFunction from '../../function';
import LoadingPage from '../../components/loading/LoadingPage';

import { Icon } from 'native-base';

const { height, width } = Dimensions.get('window');

import Picker from 'react-native-wheel-picker'
var PickerItem = Picker.Item;

class WheelPicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedItem: 0,
            itemList: ['Tambah Profil'],
            selectedName: 'Tambah Profil',
            isVisible: false,
        };
    }

    onPickerSelect(index) {
       // Alert.alert(value);  
        this.setState({
            selectedItem: index,
        })
    }

    onAddItem = () => {
        console.log('selectedItem', this.state.selectedItem);
        if (this.state.selectedItem !== 0 && appFunction.findNameArray(this.props.listPesertaAsuransi, this.state.itemList[this.state.selectedItem]) !== 0){
            console.log(appFunction.findNameArray(this.props.listPesertaAsuransi, this.state.itemList[this.state.selectedItem]));   
            this.props.getProfile(appFunction.findNameArray(this.props.listPesertaAsuransi, this.state.itemList[this.state.selectedItem]).id);
        }else{
            this.props.resetPesertaAsuransi();
        }
        this.setState({
            isVisible: false,
            selectedName: this.state.itemList[this.state.selectedItem]
        })
    }

    componentWillMount(){
        this.props.getProfileList(this.props.session.username);
    }

    componentWillUnmount(){
        this.setState({ isVisible: false});
        this.props.resetListPesertaAsuransi();
    }
    componentDidMount(){      
        this.setState({ itemList: appFunction.getNameProfile(this.props.listPesertaAsuransi)});
    }

    componentWillReceiveProps(NextProps){
        if(this.props.listPesertaAsuransi != NextProps.listPesertaAsuransi){
            this.setState({ itemList: appFunction.getNameProfile(NextProps.listPesertaAsuransi) });
        }
    }

    render() {
        const { isVisible, selectedItem, itemsList } = this.state;
        var textColor = null;
       // console.log('tampilkan pesertaAsuransi', this.props.pesertaAsuransi);
    

        if (selectedItem == 0 || !selectedItem) {
            textColor = { color: 'darkgrey' }
        }
        return (
            <View style={{ width: '80%', marginBottom: 10, marginTop: 10 }}>
                <View style={[{ borderBottomWidth: .5, width: '100%', backgroundColor: 'white' }]} >
                    <Text style={[styles.baseText]}>
                        Pilih Profil
                    </Text>
                    <TouchableHighlight underlayColor="transparent" onPress={()=>{this.setState({isVisible: true})}}>
                        
                        <View style={[styles.directionRow, styles.flex2, styles.spaceBetween]}>
                            <Text style={[textColor, styles.baseText, styles.top10]}>{this.state.selectedName}</Text>
                            <Icon name='ios-arrow-down' style={{ color: 'darkgrey' }} /> 
                        </View>
                    </TouchableHighlight>
                </View>
                <Modal
                    onRequestClose={() => null}
                    animationType="slide"//slide
                    visible={isVisible}
                    transparent={true}>
                    <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#44404075"}}>
                        <View style={styles2.container2}>
                            <TouchableWithoutFeedback onPress={() => this.setState({ isVisible: false })}>
                                <View style={{ opacity: 0, flex: 1 }} />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles2.container}>
                            <View style={[{ backgroundColor:'white', height: '15%', width:'100%'}, styles.centerItemContent]}>
                                <Text style={[styles.baseText,{textAlign: 'center'}]}>Pilih Profil</Text>
                            </View>
                            <View style={[{ backgroundColor: '#f0efed', height: '55%', width: '100%', elevation: 20 }]}>
                                <Picker style={{ height: '100%', width: '100%' }}
                                    selectedValue={this.state.selectedItem}
                                    itemStyle={{ borderColor: 'black',color: "black", fontSize: 18 }}
                                    onValueChange={(index) => this.onPickerSelect(index)}>
                                        {this.state.itemList.map((value, i) => (
                                            <PickerItem label={value} value={i} key={value} />
                                        ))}
                                </Picker>
                            </View> 
                            <View style={[{ width: '100%', height: '25%', backgroundColor: 'white' }, styles.centerItemContent]}>
                                <TouchableHighlight onPress={this.onAddItem} style={{ backgroundColor: '#ff4c4c', paddingVertical: 10, paddingHorizontal: 100, borderRadius: 10, elevation: 5, marginTop: 2}}>
                                    <Text style={[{ color: '#fff' }, styles.baseText]}>
                                        Pilih
                                    </Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>   
                </Modal>
            </View>
        );
    }
}

const styles2 = StyleSheet.create({
    container: {
        width: '100%',
        height: height * .45
    },
    container2: {
        width: '100%',
        height: height* .55
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

function mapStateToProps(state) {
    return {
        listPesertaAsuransiDummy: state.listPesertaAsuransiDummy,
        listPesertaAsuransi: state.listPesertaAsuransi,
        session:state.session,
       // pesertaAsuransi:state.pesertaAsuransi,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WheelPicker);