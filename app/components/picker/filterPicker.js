import React, {Component} from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

import styles from '../../assets/styles';

export class FilterPicker extends React.Component {

  constructor(props) {
    super(props);
    this.state = {visible: false};

  }

  onShow(){
    this.setState({ visible: true });
  }

  onCancel(){
    this.setState({ visible: false });
  }

  onSelect(picked, type){
      this.setState({ visible: false });
      this.props.setValueState(picked, type);
  }


  render() {
    let {
      title,
      value,
      options,
      type,
      iconName,
      bordered,
	  useFunction,
	  onPick, 
	  autoFocus,
	  disabled,
    } = this.props;

    const{
      visible,
    } = this.state;

    var icon = 'ios-arrow-down';
    var mystyle = [styles.pickerViewBlack];

    var border = []
    if(bordered){
      border= [styles.directionRow, styles.flex2, styles.spaceBetween, styles.border1, styles.borderColordarkgrey];
      mystyle = [styles.pickerViewTr];
      icon='md-arrow-dropdown';
    }else if(disabled){
		border= [styles.directionRow, styles.flex2, styles.spaceBetween];
	}else{
      border= [styles.directionRow, styles.flex2, styles.spaceBetween];
    }

	var textColor = null;

	if(value == 'Pilih' || !value){
		textColor = {color:'darkgrey'}
	}
	if(!disabled){
		isDisabled = false;
	}else{
		isDisabled = disabled;
		textColor = {color:'silver'}
	}


    return (
      <View style={mystyle}>
        <Text style={[styles.baseText]}>
           { value !== 'Pilih' && value  ? title : null}
        </Text>
        <TouchableOpacity disabled={isDisabled} onPress={()=>this.onShow()}>
          <View style={border}>
            <Text style={[textColor, styles.top10, styles.baseText]}>
              {value !== 'Pilih' && value ? value : title}
            </Text>
            {!disabled ? <Icon name={icon} style = {{color: 'darkgrey'}}/> : null}
          </View>
        </TouchableOpacity>
        <ModalFilterPicker
          visible={visible}
          onSelect={(value) => {this.onSelect(value, type); useFunction? onPick(value) : null}}
          onCancel={() => this.onCancel()}
          options={options}
		  autoFocus={autoFocus}

        />
      </View>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(null, mapDispatchToProps)(FilterPicker);
