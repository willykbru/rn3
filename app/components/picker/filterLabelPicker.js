import React, {Component} from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import ModalFilterPicker from 'react-native-modal-filter-picker';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import styles from '../../assets/styles';



export class FilterLabelPicker extends React.Component {

  constructor(props) {
    super(props);
    this.state = {visible: false};

  }

  onShow(){
    this.setState({ visible: true });
  }

  onCancel(){
    this.setState({ visible: false });
  }

  onSelect(picked, label, type){
      this.setState({ visible: false });
      const value = Object.assign({},{val: picked, label});
      this.props.setValueState(value, type);
  }


  render() {
    const {
      title,
      options,
      type,
      value,
      bordered,
      iconName,
	  useFunction,
	  onPick,
	  disabled,
    } = this.props;
/*
    const {lobLabelState} = this.props;
    var valueLabel = '';
    if(type.lob == 'MV' && type.state == 'plat'){
      valueLabel = lobLabelState.mv.plat;
    }
*/
    const{
      visible,
    } = this.state;

    var icon = 'ios-arrow-down';
    var mystyle = [styles.pickerViewBlack];
    var border = []
    if(bordered){
      border= [styles.directionRow, styles.flex2, styles.spaceBetween, styles.border1, styles.borderColordarkgrey];
      mystyle = [styles.pickerViewTr];
      icon='md-arrow-dropdown';
    }else if(disabled){
		border= [styles.directionRow, styles.flex2, styles.spaceBetween];
	}else{
      border= [styles.directionRow, styles.flex2, styles.spaceBetween];
      icon = 'ios-arrow-down';
    }
	var textColor = null;

	if(value == 'Pilih' || !value){
		textColor = {color:'darkgrey'}
	}
	
	if(!disabled){
		isDisabled = false;
	}else{
		isDisabled = disabled;
		textColor = {color:'silver'}
	}


    return (
      <View style={mystyle}>
        <Text style={[styles.baseText]}>
          { value !== 'Pilih' && value ? title : null}
        </Text>
        <TouchableOpacity disabled={isDisabled} onPress={() => this.onShow()}>
          <View style={border}>
            <Text style={[textColor, styles.top10, styles.baseText]}>
              {value !== 'Pilih' && value   ? value : title}
            </Text>
            {!disabled ? <Icon name={icon} style = {{color: 'darkgrey'}}/> : null} 
          </View>
        </TouchableOpacity>
        <ModalFilterPicker
          visible={visible}
          onSelect={(value, label) => {this.onSelect(value, label, type); useFunction? onPick(value, label) : null}}
          onCancel={() => this.onCancel()}
          options={options}
        />
      </View>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(null, mapDispatchToProps)(FilterLabelPicker);
