import React, { Component } from 'react';
import { Modal, BackHandler, Alert, Dimensions, View, Text, TouchableHighlight, StyleSheet, Image, AsyncStorage } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import UploadButton from '../../components/button/UploadButton';
import styles from '../../assets/styles';
//import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';

export default class UploadImage extends Component{
	
	constructor(props) {
    super(props);
	this.state={
		modalupload: false,
		
	}
    
  }
  
    setModalVisible(modalupload) {
    //this.setState({modalVisible: visible});
	this.setState({modalupload});
  }
  
  alertUpload(){
	Alert.alert(
		'Upload Foto',
		  'Pilih Sumber Foto ' + this.props.filetitle,
		  
		  [
			{text: 'Dari Library Gambar', onPress: () => this.pickfile()},
			{text: 'Dari Kamera', onPress: () => this.pickcamera()},
			
		  ]
		)
  }
  
  modalUpload(){
	  var {height, width} = Dimensions.get('window');
	  
		  return(
		   <Modal
		   animationType='fade'
			 transparent={false}
			  visible={this.state.modalupload}
			  onRequestClose={() => null}>
			  <View style={[{marginTop: height*.35}, styles.centerItemContent]}>
				<View style={[styles.width90p, {backgroundColor: 'black'},  styles.centerItemContent]}>
				
				  <Text style={[styles.baseText, styles.pad10, {color:'white'}]}>Pilih Sumber Foto {this.props.filetitle}</Text>
					<View style={[styles.directionRow, styles.width100p]}>
				  <TouchableHighlight
					style={[{padding: 10, borderColor: 'white', borderWidth:.5, width :'50%'}, styles.centerItemContent]}
					onPress={() => {
					  this.pickfile(); this.setModalVisible(false);
					}}>
					<Text style={[styles.baseText, {color:'white'}]}>Dari Library Gambar</Text>
				  </TouchableHighlight>
				  
				  <TouchableHighlight
				  style={[{padding: 10, borderColor: 'white', borderWidth:.5, width :'50%'}, styles.centerItemContent]}
					onPress={() => {
					  this.pickcamera();this.setModalVisible(false);
					}}>
					<Text style={[styles.baseText, {color:'white'}]}>Dari Kamera</Text>
				  </TouchableHighlight>
				  </View>
				</View>
			  </View>
			</Modal>
		  );
		  
	  }

	pickfile = async () => {
		
    await ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      includeBase64: true,
      mediaType:'photo',
      }).then(image => {
        if(image.size / 1024 <= 20480){
          this.props.onUploadFile(image.data);
		  //this.popupDialog.dismiss();
        }else{
          appFunction.toastError('Foto Harus Kurang Dari 20MB');
		  //this.popupDialog.dismiss();
        }
    });
  }
  pickcamera = async () => {
		
    await ImagePicker.openCamera({
      width: 800,
      height: 600,
      cropping: true,
      includeBase64: true,
      mediaType:'photo',
      }).then(image => {
        if(image.size / 1024 <= 20480){
          this.props.onUploadFile(image.data);
		  //this.popupDialog.dismiss();
        }else{
          appFunction.toastError('Foto Harus Kurang Dari 20MB');
		  //this.popupDialog.dismiss();
        }
    });
  }  
  
  render(){
	  //props 
	  //filename //onUploadFile //filetitle
	  const file_name = this.props.filename;
	  var {height, width} = Dimensions.get('window');
	  
	  let imageUriFile = file_name ? `data:image/jpg;base64,${file_name}` : null;
	  
		//const slideAnimation = new SlideAnimation({
		//  slideFrom: 'bottom',
		//});


    //imageUri && console.log({uri: imageUri.slice(0, 100)});
	  return(
			<View>
			{this.modalUpload()}
			<Text style={[styles.baseText]}>{this.props.filetitle}</Text>
			<View style={[styles.border1, styles.borderColorGrey, styles.directionColumn]}>
			 <View style={[styles.directionRow]}>
				<View style={[styles.directionRow, styles.width80p]}>
				  <Image style={[styles.width25, styles.height25]} source={require('../../assets/icons/button/upload.png')}/>
				  <Text>{file_name ? 'Sudah Terupload' : 'Belum Upload'}</Text>
				</View>
				<View>
				  <UploadButton title='Upload' onPress={()=> this.alertUpload() } />
				</View>
			  </View>
			   {file_name
				 ? <Image
					 source={{uri: imageUriFile}}
					 style={[styles.width200, styles.height150]}
				   />
				 : null}
			 </View>			 
			</View>
    
	  );
	  
  }

}