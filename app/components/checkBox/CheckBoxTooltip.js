import React, {Component} from 'react';
import { View, Text, Dimensions } from 'react-native';
import styles from '../../assets/styles';
import CheckBox from 'react-native-check-box';
import { Icon } from 'native-base';
import PopoverTooltip from 'react-native-popover-tooltip';

export default class CheckBoxTooltip extends Component{
  constructor(props) {
    super(props);
  }

  usetooltip(text){
    var {height, width} = Dimensions.get('window');
    return(
      <PopoverTooltip
        buttonComponent={
          <Icon name="ios-information-circle-outline" style={styles.colorRed} />
        }
        items={[
          {
            label: text,
            onPress: () => {}
          }
        ]}
        tooltipContainerStyle = {{ width: width*.8, }}
        // animationType='timing'
        // using the default timing animation
       />
     );
  }

  render(){
    const {
      parentStyle,
      title,
      onClick,
      tooltipText,
      isChecked,
      color,
    } = this.props;

    var style = [styles.directionRow];
    style.push(parentStyle);
    //console.log(tooltipText);
    return(
      <View style={ style }>
        <View style={ styles.right20 }>
          <CheckBox isChecked={ isChecked } checkBoxColor={color} onClick={onClick} />
        </View>
        <View style={[styles.directionRow, styles.flex1, styles.spaceBetween]}>
          <Text style={[styles.baseText]}>{title}</Text>
          {this.usetooltip(tooltipText)}
        </View>
      </View>
    )
  }
}
