import React, {Component} from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import styles from '../../assets/styles';

export default class MenuPencarian extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      title,
    } = this.props;

    return (
      <View style={[styles.top10, styles.width100p, styles.bottom20]}>
		<Text style={[styles.baseText, {paddingLeft:10}, styles.bold]}>{title}</Text>
		<View style={[styles.centerItemContent, styles.top10]}>
			{this.props.children}
			</View>
      </View>
    )
  }
}
