import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';



export default class BaseShape extends React.Component {

    render(){
        return (
            <View style={styles.base}>
                <View style={styles.baseRight} />
                <View style={styles.baseLeft} />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    base: {
        marginTop: 10
    },
    baseRight: {
        borderLeftWidth: 30,
        position: 'absolute',    
        borderLeftColor: 'white',
        borderTopWidth: 10,
        borderTopColor: 'black',
        borderBottomWidth: 10,
        borderBottomColor: 'black',
        height: 0,
        width: 0,
        bottom: 0,
        right: -30,                                   
    },
    baseLeft: {
        //backgroundColor: 'red',
        borderBottomWidth: .5,
        borderTopWidth: .5,
        borderLeftWidth: .5, 
        height: 20,
        width: 50
    }
})