import React from 'react';
import { Dimensions, Modal, TouchableWithoutFeedback, View, WebView, Image, TouchableOpacity, Button } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';



class ChatModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
        };
        // this.handleAppStateChange = this.handleAppStateChange.bind(this);
        // this.sendNotification = this.sendNotification.bind(this);
    }


    setIsVisibleFalse() {
        this.setState({ isVisible: false });
    }

    setIsVisibleTrue() {
        this.setState({ isVisible: true });
    }


    goToLob(lobPopup) {
        //console.log(lobPopup);
        // let jenisLob  = {"PR" , "MV", "AP", "AC", "HT", "LF", "TR", "DL", "CG", "BC", "PT"];
        // let lob       = ["Property", "MV"  , "Apartment"  , "PA", "Health"  , "Life"  , "Travel"  , "Delay"  , "Cargo"  , "Bicycle"  , "Pet"];
        let mapLob = [{ key: "PR", value: "Property" },
        { key: "MV", value: "MV" },
        { key: "AP", value: "Apartment" },
        { key: "AC", value: "PA" },
        { key: "HT", value: "Health" },
        { key: "LF", value: "Life" },
        { key: "TR", value: "Travel" },
        { key: "DL", value: "Delay" },
        { key: "CG", value: "Cargo" },
        { key: "BC", value: "Bicycle" },
        { key: "PT", value: "Pet" },
        ];

        let obj = mapLob.find(item => item.key === lobPopup);

        this.props.setScreen(obj.value, 'cariasuransi');
        this.props.navigate('CariAsuransi');

    }

    componentWillUnmount() {
        this.setState({ isVisible: false });
    }

    componentWillReceiveProps(NextProps){
        
        
    }

    render() {
        const { isVisible } = this.state;
        const { visible } = this.props;
        const { popupId, lobPopup, statusEvent, linkPopup } = this.props.itemsPopup;
        const { popupevent } = this.props.status;
        const { height, width } = Dimensions.get('window');
        const myUrl = 'https://tawk.to/chat/5b836f90afc2c34e96e7ecb9/default'; 
        return (
            <View>
                
                        <Modal
                    visible={this.props.status.chatmodal}
                            animationType="fade"//slide
                            transparent={true}
                    onRequestClose={() => this.props.setStatusChatModal(false)}>
                    <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#44404052", padding: 10, justifyContent: "center" }}>
                        <View style={{ alignItems: 'flex-end', }}>
                            <View style={{ borderColor: 'white' }}>
                                <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', borderRadius: wp('10%'), width: width * 0.06, height: width * 0.06, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.props.setStatusChatModal(false); }}>

                                    <Image style={{ width: width * 0.03, height: width * 0.03 }} source={require('../../assets/icons/component/cancel-music.png')} />

                                </TouchableOpacity>
                            </View>
                        </View>
                         <View style={{ height: '80%' }}>
                        <WebView
                            ref={(view) => this.webView = view}
                            source={{ uri: myUrl }}
                            automaticallyAdjustContentInsets={false}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            startInLoadingState={true}
                                    /></View>
                            </View>
                              


                        </Modal> 
            
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        screen: state.screen,
        status: state.status,
        session: state.session,
        listSuccessPayment: state.listSuccessPayment,
        itemsPopup: state.itemsPopup,
        

    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatModal);