import React from 'react';
import { Dimensions, Modal, TouchableWithoutFeedback, View, Image, TouchableOpacity, Button } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';



class PopupEvent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isVisible: true,
        };
        // this.handleAppStateChange = this.handleAppStateChange.bind(this);
        // this.sendNotification = this.sendNotification.bind(this);
    }


    setIsVisibleFalse() {
        this.setState({ isVisible: false });
    }


    goToLob(lobPopup){
        //console.log(lobPopup);
        // let jenisLob  = {"PR" , "MV", "AP", "AC", "HT", "LF", "TR", "DL", "CG", "BC", "PT"];
        // let lob       = ["Property", "MV"  , "Apartment"  , "PA", "Health"  , "Life"  , "Travel"  , "Delay"  , "Cargo"  , "Bicycle"  , "Pet"];
        let mapLob = [{ key: "PR", value: "Property" },
        { key: "MV", value: "MV" },
        { key: "AP", value: "Apartment" },
        { key: "AC", value: "PA" },
        { key: "HT", value: "Health" },
        { key: "LF", value: "Life" },
        { key: "TR", value: "Travel" },
        { key: "DL", value: "Delay" },
        { key: "CG", value: "Cargo" },
        { key: "BC", value: "Bicycle" },
        { key: "PT", value: "Pet" },
        ];

        let obj = mapLob.find(item => item.key === lobPopup);

        this.props.setScreen(obj.value, 'cariasuransi');
        this.props.navigate('CariAsuransi');

    }

    componentWillUnmount(){
        this.setState({ isVisible: false});
    }

    render(){
        const { isVisible } = this.state;
        const { popupId, lobPopup, statusEvent, linkPopup } = this.props.itemsPopup;
        const { popupevent } = this.props.status;
        const { height, width } = Dimensions.get('window');
        return(
            <View>
            {
                statusEvent == 'Y' && popupevent ?
                    <Modal
                        visible={isVisible}
                        animationType="fade"//slide
                        transparent={true}
                        onRequestClose={() => this.props.setStatusPopupEvent(false)}>
                        <TouchableWithoutFeedback onPress={() => { this.setIsVisibleFalse(); }}>

                            <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#44404052", padding: 10, justifyContent: "center" }}>
                                <View style={{ alignItems: 'flex-end', }}>
                                    <View style={{ borderColor: 'white' }}>
                                        <TouchableOpacity style={{ backgroundColor: 'rgba(0,0,0,0.5)', borderRadius: wp('10%'), width: width * 0.06, height: width * 0.06, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.props.setStatusPopupEvent(false); }}>

                                            <Image style={{ width: width * 0.03, height: width * 0.03 }} source={require('../../assets/icons/component/cancel-music.png')} />

                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ height: '60%', borderRadius: 10 }}>
                                    <TouchableOpacity onPress={() => { this.goToLob(lobPopup); this.props.setStatusPopupEvent(false); }}>
                                        <Image source={{ uri: linkPopup }} style={{ width: '100%', height: '100%' }} resizeMode='stretch' />
                                    </TouchableOpacity>
                                </View>

                                <View style={{ width: '100%', flexDirection: "row", backgroundColor: "#1494f414" }}>
                                    <View style={{ width: '50%' }}>
                                        <Button onPress={() => { this.goToLob(lobPopup); this.props.setStatusPopupEvent(false); }} title="Mau" color="#407ec9" />
                                    </View>

                                    <View style={{ width: '50%' }}>
                                        <Button onPress={() => { this.props.setStatusPopupEvent(false); }} title="Nanti Saja" color="#FF0000" />
                                    </View>

                                </View>

                            </View>
                        </TouchableWithoutFeedback>


                    </Modal> :
                    null
            }
                </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        screen: state.screen,
        status: state.status,
        session: state.session,
        listSuccessPayment: state.listSuccessPayment,
        itemsPopup: state.itemsPopup,
    
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PopupEvent);