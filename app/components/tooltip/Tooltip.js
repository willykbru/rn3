import React, {Component} from 'react';
import { View, Dimensions } from 'react-native';
import styles from '../../assets/styles';
import { Icon } from 'native-base';
import PopoverTooltip from 'react-native-popover-tooltip';

export default class Tooltip extends Component{
  constructor(props) {
    super(props);
  }

  usetooltip(text){
    var {height, width} = Dimensions.get('window');
    return(
      <PopoverTooltip
        buttonComponent={
          <Icon name="ios-information-circle-outline" style={styles.colorRed} />
        }
        items={[
          {
            label: text,
            onPress: () => {}
          }
        ]}
        tooltipContainerStyle = {{ width: width*.8, }}
		labelStyle={{marginLeft:80}}
        // animationType='timing'
        // using the default timing animation
       />
     );
  }

  render(){
    const {
      tooltipText,
    } = this.props;

    //console.log(tooltipText);
    return(
	<View style={[styles.directionRow, styles.flex1, styles.spaceBetween, styles.left10]}>
          {this.usetooltip(tooltipText)}
	</View>
    )
  }
}
