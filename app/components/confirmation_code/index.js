import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Alert,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import firebase from 'react-native-firebase';
import TimerCountdown from 'react-native-timer-countdown';
import styles from '../../assets/styles';
import * as appFunction from '../../function';
import SubmitButton from '../../components/button/SubmitButton';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LoadingPage from '../../components/loading/LoadingPage';
import DialogAndroid from 'react-native-dialogs';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

class RNCodeInput extends Component {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
    
    this.state = {
      code: '', 
	  phone: this.props.dataRegister.phone,
	  resend: false,
	  idToken : null,
	  user: null,
      message: '',
      code: '',
      confirmResult: null,
    };
  }

  clearState(){
    const blankState = {};
    Object.keys(this.state).forEach(stateKey => {
      blankState[stateKey] = undefined;
    });
    this.setState(blankState);
  }
  
  componentDidMount() {
	  this.setState({user: null, idToken:null});
	this.signUp();
	
  }
  
  signOut = () => {
	  	if(firebase.auth().currentUser){
        console.log('currentuser', firebase.auth().currentUser)
			 this.setState({
				  user: null,
				  message: '',
				  code: '',
				});
			firebase.auth().signOut();
		}
  }
  
    componentWillUnmount() {
     if (this.unsubscribe) this.unsubscribe();
   this.signOut();
      this.clearState();
  }
  
  onchangestate(){
	  firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user: user.toJSON() });
		user.getIdToken().then((idToken)=> {this.setState({ idToken }); this.props.setTokenRegister(idToken); })
		console.log(user);
		
      } else {
        // User has been signed out, reset the state
       this.setState({
          user: null,
          message: '',
          code: '',
        });
      }
    });
  }
  

  
 confirmCode = () => {
    const { code, confirmResult } = this.state;
	console.log(code);
    if (confirmResult && code.length) {
      confirmResult.confirm(code)
        .then((user) => {
			if(user){
				this.setState({ message: 'Code Confirmed!' });
				//this.setState({ user: user.toJSON() });
				//user.getIdToken().then((idToken)=> {this.setState({ idToken }); this.props.setTokenRegister(token); })
			}else{
				appFunction.toastError('Kode Verifikasi tidak sesuai');
			}
        })
	.catch(error => {this.setState({ message: `Code Confirm Error: ${error.message}` });appFunction.toastError('Kode Verifikasi tidak sesuai'); });
    }else if(!confirmResult){
		appFunction.toastError('Kode Verifikasi tidak sesuai');	
	}else{
		appFunction.toastError('Kode Verifikasi belum dilengkapi');	
	}
  };

  
  
  signUp = () => {
    const { phone } = this.state;
	var phoneNumber = phone;
	
	if(phone.substr(0,1) == '0'){
		 phoneNumber = phone.replace('0', '+62');
	
	}else if(phone.substr(0,1) == '6'){
		phoneNumber = phone.replace('6', '+6');
	
	}
	//Alert.alert(phoneNumber);
	
    this.setState({ message: 'Sending code ...' });

    firebase.auth().signInWithPhoneNumber(phoneNumber)
		.then(confirmResult => {this.setState({ confirmResult, message: 'Code has been sent!' });this.onchangestate();console.log('confirmResult',confirmResult);})
      .catch(error => {this.setState({ message: `Sign In With Phone Number Error: ${error.message}` });});
  };
  
  _onFulfill(code) {
    // TODO: call API to check code here
    // If code does not match, clear input with: this.refs.codeInputRef1.clear()
    if (code == 'Q234E') {
      Alert.alert(
        'Confirmation Code',
        'Successful!',
        [{text: 'OK'}],
        { cancelable: false }
      );
    } else {
      Alert.alert(
        'Confirmation Code',
        'Code not match!',
        [{text: 'OK'}],
        { cancelable: false }
      );
      
      this.refs.codeInputRef1.clear();
    }
  }
  
  _onFinishCheckingCode1(code) {
    this.setState({code});   
  }
  
  _onFinishCheckingCode2(isValid, code) {
    //console.log(isValid);
    if (!isValid) {
      Alert.alert(
        'Confirmation Code',
        'Code not match!',
        [{text: 'OK'}],
        { cancelable: false }
      );
    } else {
      this.setState({ code });
      Alert.alert(
        'Confirmation Code',
        'Successful!',
        [{text: 'OK'}],
        { cancelable: false }
      );
    }
  }
  async resend(){
    //console.log('kirim ulang');
    this.setState({resend: false});

	await this.signOut();
	this.signUp();
    
  }
  
   renderTimerActivate(resend){
    if(!resend){
      return(
        <View style={[styles.directionRow, styles.centerItemContent]}>
          <Text style={[styles.font12, styles.baseText, { color: 'rgba(255, 25, 25, 0.7)', }]}>Kirim ulang kode dalam </Text>
           <TimerCountdown
               initialSecondsRemaining={30000}
               interval={1000}
               onTimeElapsed={() => this.setState({resend: true})}
               allowFontScaling={true}
               style={[styles.baseText, styles.font12, {color: 'rgba(255, 25, 25, 0.7)' }]}
           />
         </View>
      );
    }else{
      return(
          <View style={[styles.centerItemContent]}>
        <TouchableHighlight underlayColor='white'
          style={[styles.centerItemContent, styles.width50p]}
          onPress={() => this.resend()}>
            <Text style={[styles.font14, styles.bold, styles.baseText, {color: 'rgba(255, 25, 25, 0.7)'}]}>KIRIM ULANG</Text>
        </TouchableHighlight>
      </View>
      );
    }
  }
  
  componentWillReceiveProps(NextProps){
	  if(this.props.dataRegister != NextProps.dataRegister){
		 // console.log('dataRegister Berubah')
		  if(NextProps.dataRegister.token && NextProps.dataRegister.token !== '0'){
			    //console.log('dataRegister Berubah2')
				setTimeout(()=>{
					if(this.props.screen.asuransiScreen){
						console.log('assScreen',this.props.screen.asuransiScreen)
						this.props.doCheckEmail(NextProps.dataRegister,'doRegisterAsuransi');
					}
					else{
						console.log('nonassScreen',this.props.screen.asuransiScreen)
						this.props.doCheckEmail(NextProps.dataRegister,'doRegisterLogin');
					}
					this.signOut();
					}, 5000)
				DialogAndroid.showProgress('Loading');
				//setTimeout(DialogAndroid.dismiss, 5000);
		  }
	  }
	  
  }
  
  render() {
	  const {phone, resend, message, confirmResult, idToken,  user} = this.state;
	  //console.log(message);

	
    return (
      <View style={styles2.container}>
		 			
        <ScrollView style={styles2.wrapper}>
          <View style={styles2.titleWrapper}>
            <Text style={styles2.title}>Verifikasi Nomor Handphone Kamu</Text>
            <View style={[styles.centerItemContent, {padding: 10}]}><Text style={[styles.baseText]}>Hanya tinggal memasukkan kode verifikasi yang kami SMS ke nomor Handphone yang telah kamu daftarkan</Text></View>
			<View>
			 <Text style={[styles.bold, styles.font16]}>{phone}</Text>
				
			 </View>
			 
          </View>
		  
		
             
		 <View style={styles2.inputWrapper2}>
            <CodeInput
              ref="codeInputRef2"
              keyboardType="numeric"
              activeColor='rgba(49, 180, 4, 1)'
              inactiveColor='rgba(49, 180, 4, 1.3)'
              autoFocus={false}
              ignoreCase={true}
              inputPosition='center'
              size={40}
              codeLength={6}
              onFulfill={(code) => this._onFinishCheckingCode1(code)}
              containerStyle={{ marginTop: 10 }}
              codeInputStyle={{ borderWidth: 0.5 }}
            />
			<View style={[styles.top10]} />
			{this.renderTimerActivate(resend)}
          </View>
		  <View style={[styles.top40]} />
		  <View style={styles2.titleWrapper}>
			  <SubmitButton title='Konfirmasi' onPress={() => this.confirmCode()} />
		 </View>
	
         
        </ScrollView> 
      </View>
    );
  }
}

const styles2 = StyleSheet.create({
  container: {
    flex: 1,
   // backgroundColor: '#E0F8F1'
    backgroundColor: '#FFF'
  },
  titleWrapper: {
    flex: 1,
    justifyContent: 'center',
	alignItems: 'center',
    flexDirection: 'column',
  },
  title: {
    color: 'black',
    fontSize: 18,
    fontWeight: '800',
    paddingVertical: 10
  },
  wrapper: {
    marginTop: 30
  },
  inputWrapper1: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    backgroundColor: '#009C92'
  },
  inputWrapper2: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    backgroundColor: '#FFF'
  },
  inputWrapper3: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    backgroundColor: '#2F0B3A'
  },
  inputLabel1: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '800'
  },
  inputLabel2: {
    color: '#31B404',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'center'
  },
  inputLabel3: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'center'
  }
});

function mapStateToProps(state) {
  return {
    dataRegister: state.dataRegister,
	screen: state.screen,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(RNCodeInput);
