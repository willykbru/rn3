import { React } from 'react';
import { Image } from 'react-native';
import { StackNavigator } from 'react-navigation';
import transition from './transitions';

//Main & LOB Screen
import Main from '../screen/main';
import Activation from '../screen/user/activation';
import PhoneVerification from '../screen/user/phone_verification';
import ForgotPassword from '../screen/user/password/forgotPassword';
import ChangePassword from '../screen/user/password/changePassword';
import CariAsuransi from '../screen/lob/CariAsuransi';
import ListAsuransi from '../screen/lob/ListAsuransi';
import DetailBeli from '../screen/lob/DetailBeli';
import DetailPribadi from '../screen/lob/DetailPribadi';
import Pembayaran from '../screen/lob/Pembayaran';
import PaymentGateway from '../screen/lob/PaymentGateway';
import Benefit from '../screen/lob/Benefit';
import Renewal from '../screen/lob/Renewal';

//claim
import ClaimVehicle1 from '../screen/claim/ClaimVehicle';
import ClaimVehicleUpload1 from '../screen/claim/ClaimVehicleUpload';
import ClaimGambar1 from '../screen/claim/ClaimGambar';

import ClaimMVUmum1 from '../screen/claim/ClaimMVUmum';
import ClaimMVPengemudi1 from '../screen/claim/ClaimMVPengemudi';
import ClaimMVPenumpangSaksi1 from '../screen/claim/ClaimMVPenumpangSaksi';
import ClaimMVPolisi1 from '../screen/claim/ClaimMVPolisi';
import ClaimMVPihakKetiga1 from '../screen/claim/ClaimMVPihakKetiga';
import ClaimMVBerkasUnggahan1 from '../screen/claim/ClaimMVBerkasUnggahan';
import ClaimMVInformasi1 from '../screen/claim/ClaimMVInformasi';
import ClaimLihatKlaim1 from '../screen/claim/ClaimLihatKlaim';
import ClaimLihatDetailKlaim1 from '../screen/claim/ClaimLihatDetailKlaim';
import ClaimLihatSPK1 from '../screen/claim/ClaimLihatSPK';

//lapor
import LaporLihat1 from '../screen/lapor/laporLihat';
import LaporMVUmum1 from '../screen/lapor/laporMVUmum';
import LaporMVMenu1 from '../screen/lapor/laporMVMenu';

//lapor kecelakaan
import LaporMVKecelakaanUmum1 from '../screen/lapor/kecelakaan/laporMVKecelakaanUmum';
import LaporMVKecelakaanPengemudi1 from '../screen/lapor/kecelakaan/laporMVKecelakaanPengemudi';
import LaporMVKecelakaanPihakKetiga1 from '../screen/lapor/kecelakaan/laporMVKecelakaanPihakKetiga';
import LaporMVKecelakaanKronologis1 from '../screen/lapor/kecelakaan/laporMVKecelakaanKronologis';
import LaporMVKecelakaanBengkel1 from '../screen/lapor/kecelakaan/laporMVKecelakaanBengkel';
import LaporMVKecelakaanFoto1 from '../screen/lapor/kecelakaan/laporMVKecelakaanFoto';
import LaporMVKecelakaanKonfirmasi1 from '../screen/lapor/kecelakaan/laporMVKecelakaanKonfirmasi';
import LaporMVKecelakaanLampiranGambar1 from '../screen/lapor/kecelakaan/laporMVKecelakaanLampiranGambar';
// import LaporMVKecelakaanInformasi1 from '../screen/lapor/kecelakaan/laporMVKecelakaanInformasi';

// lapor kehilangan total
import LaporMVKehilanganTotalDokumenFisik1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalDokumenFisik';
import LaporMVKehilanganTotalKepolisian1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalKepolisian';
import LaporMVKehilanganTotalKonfirmasi1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalKonfirmasi';
import LaporMVKehilanganTotalPengemudi1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalPengemudi';
import LaporMVKehilanganTotalSaksi1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalSaksi';
import LaporMVKehilanganTotalUmum1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalUmum';
import LaporMVKehilanganTotalKronologis1 from '../screen/lapor/kehilangantotal/laporMVKehilanganTotalKronologis';

//More Screen
import Agreement from '../screen/more/agreement';
import PrivacyPolicy from '../screen/more/PrivacyPolicy';
import Menu from '../screen/more/menu';
import BengkelRekanan from '../screen/more/BengkelRekanan';
import RumahSakitProvider from '../screen/more/RumahSakitProvider';

import DetailPendingPayment from '../screen/dashboard/DetailPendingPayment';
import DataPolis from '../screen/dashboard/Polis';
//import About from '../components/about/About';

//import TravelScreenTwo from '../components/lob/Travel/listAsuransi';
//import TravelScreenThree from '../components/lob/Travel/pembayaran';

//import ListAsuransi from '../components/screen/listAsuransi';

import LihatProfile from '../screen/user/profile/LihatProfile'; 
import ListProfile from '../screen/user/profile/ListProfile'; 
import ListProfileForm from '../screen/user/profile/ListProfileForm';
import Login from '../screen/user/login';
import Register from '../screen/user/register';
import Verification from '../screen/user/verification';
// import LeftIconComponent from '../header/LeftIconComponent';

const navigationOptions = {
  headerStyle: {
    backgroundColor: '#fff',
    height: 50,
    // paddingTop: 20,
    justifyContent: 'flex-end',
    elevation: 0,
  },
  headerTitleStyle: {
    color: 'red',
    alignSelf: 'center',
  },
  headerTintColor: '#FFFFFF',
};


const MainStack = new StackNavigator({
  Main: {
    screen: Main,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  Menu: {
    screen: Menu,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  ForgotPassword: {
    screen: ForgotPassword,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  ChangePassword: {
    screen: ChangePassword,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  RumahSakitProvider: {
    screen: RumahSakitProvider,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  BengkelRekanan: {
    screen: BengkelRekanan,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  Benefit: {
    screen: Benefit,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  Renewal: {
    screen: Renewal,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  CariAsuransi: {
    screen: CariAsuransi,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  ListAsuransi: {
    screen: ListAsuransi,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  DataPolis: {
    screen: DataPolis,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  DetailPribadi: {
    screen: DetailPribadi,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  DetailBeli: {
    screen: DetailBeli,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  Login: {
   screen: Login,
    navigationOptions: navigation => ({
      header:null,
    }),
  },
   Activation: {
   screen: Activation,
    navigationOptions: navigation => ({
      header:null,
    }),
  },
  Register: {
   screen: Register,
    navigationOptions: navigation => ({
      header:null,
    }),
  },
  Verification: {
   screen: Verification,
    navigationOptions: navigation => ({
      header:null,
    }),
  },
  PaymentGateway: {
    screen: PaymentGateway,
    navigationOptions: navigation => ({
      header:null,
    }),
  },
  DetailPendingPayment: {
    screen: DetailPendingPayment,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  Pembayaran: {
    screen: Pembayaran,
    navigationOptions: navigation => ({
      header:null,
    }),
  },
  Agreement: {
    screen: Agreement,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  LihatProfile: {
    screen: LihatProfile,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ListProfile: {
    screen: ListProfile,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },

  ListProfileForm: {
    screen: ListProfileForm,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  PhoneVerification: {
    screen: PhoneVerification,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  }, 
  PrivacyPolicy: {
    screen: PrivacyPolicy,
    transitionConfig: transition,
    cardStyle: {backgroundColor: 'transparent'},
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header:null,
    }),
  },
  ClaimVehicle: {
    screen: ClaimVehicle1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimVehicleUpload: {
    screen: ClaimVehicleUpload1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimGambar: {
    screen: ClaimGambar1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVUmum: {
    screen: ClaimMVUmum1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVPengemudi: {
    screen: ClaimMVPengemudi1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVPenumpangSaksi: {
    screen: ClaimMVPenumpangSaksi1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVPolisi: {
    screen: ClaimMVPolisi1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVPihakKetiga: {
    screen: ClaimMVPihakKetiga1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVBerkasUnggahan: {
    screen: ClaimMVBerkasUnggahan1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimMVInformasi: {
    screen: ClaimMVInformasi1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimLihatKlaim: {
    screen: ClaimLihatKlaim1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimLihatDetailKlaim: {
    screen: ClaimLihatDetailKlaim1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  ClaimLihatSPK: {
    screen: ClaimLihatSPK1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporLihat:{
    screen: LaporLihat1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVUmum:{
    screen: LaporMVUmum1,
    transitionConfig: transition,
    cardStyle: { backgroundColor: 'transparent' },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVMenu: {
    screen: LaporMVMenu1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKecelakaanUmum: {
    screen: LaporMVKecelakaanUmum1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKecelakaanPengemudi: {
     screen: LaporMVKecelakaanPengemudi1,
       transitionConfig: transition,
       cardStyle: {
         backgroundColor: 'transparent'
       },
       navigationOptions: navigation => ({
         gesturesEnabled: false,
         header: null,
       }),
  },
  LaporMVKecelakaanPihakKetiga: {
    screen: LaporMVKecelakaanPihakKetiga1,
      transitionConfig: transition,
      cardStyle: {
        backgroundColor: 'transparent'
      },
      navigationOptions: navigation => ({
        gesturesEnabled: false,
        header: null,
      }),
  },
  LaporMVKecelakaanKronologis: {
    screen: LaporMVKecelakaanKronologis1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKecelakaanBengkel: {
    screen: LaporMVKecelakaanBengkel1,
      transitionConfig: transition,
      cardStyle: {
        backgroundColor: 'transparent'
      },
      navigationOptions: navigation => ({
        gesturesEnabled: false,
        header: null,
      }),
  },
  LaporMVKecelakaanFoto : {
    screen: LaporMVKecelakaanFoto1,
      transitionConfig: transition,
      cardStyle: {
        backgroundColor: 'transparent'
      },
      navigationOptions: navigation => ({
        gesturesEnabled: false,
        header: null,
      }),
  },
  LaporMVKecelakaanKonfirmasi: {
    screen: LaporMVKecelakaanKonfirmasi1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKecelakaanLampiranGambar: {
    screen: LaporMVKecelakaanLampiranGambar1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  // LaporMVKecelakaanInformasi:{
  //   screen: LaporMVKecelakaanInformasi1,
  //   transitionConfig: transition,
  //   cardStyle: {
  //     backgroundColor: 'transparent'
  //   },
  //   navigationOptions: navigation => ({
  //     gesturesEnabled: false,
  //     header: null,
  //   }),
  // },
  LaporMVKehilanganTotalUmum:{
    screen: LaporMVKehilanganTotalUmum1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKehilanganTotalSaksi:{
    screen: LaporMVKehilanganTotalSaksi1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKehilanganTotalPengemudi:{
    screen: LaporMVKehilanganTotalPengemudi1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKehilanganTotalKonfirmasi: {
    screen: LaporMVKehilanganTotalKonfirmasi1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  }, 
  LaporMVKehilanganTotalKepolisian: {
    screen: LaporMVKehilanganTotalKepolisian1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKehilanganTotalDokumenFisik: {
    screen: LaporMVKehilanganTotalDokumenFisik1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  },
  LaporMVKehilanganTotalKronologis: {
    screen: LaporMVKehilanganTotalKronologis1,
    transitionConfig: transition,
    cardStyle: {
      backgroundColor: 'transparent'
    },
    navigationOptions: navigation => ({
      gesturesEnabled: false,
      header: null,
    }),
  }
}, {
  navigationOptions
})

export default MainStack;
