
export function setQuotaDtTrv(quota){
  return{
    type: 'SET_DATA_TRAVEL',
    value: Object.assign({},{quota})
  }
}

export function setVisitorIdDtTrv(visitor_id){
  return{
    type: 'SET_DATA_TRAVEL',
    value: Object.assign({},{visitor_id})
  }
}

export function setKodeVoucherDtTrv(kode_voucher){
  return{
    type: 'SET_DATA_TRAVEL',
    value: Object.assign({},{kode_voucher})
  }
}

//kewarganegaraan
export function setKewarganegaraanPpDtTrv(kewarganegaraan_pp){
  return{
    type: 'SET_OTHER_DATA_TRAVEL',
    value: Object.assign({},{kewarganegaraan_pp})
  }
}

export function setKewarganegaraanPsDtTrv(kewarganegaraan_ps){
  return{
    type: 'SET_OTHER_DATA_TRAVEL',
    value: Object.assign({},{kewarganegaraan_ps})
  }
}

export function setKewarganegaraanApDtTrv(kewarganegaraan_ap){
  return{
    type: 'SET_OTHER_DATA_TRAVEL',
    value: Object.assign({},{kewarganegaraan_ap})
  }
}

export function setKewarganegaraanAdDtTrv(kewarganegaraan_ad){
  return{
    type: 'SET_OTHER_DATA_TRAVEL',
    value: Object.assign({},{kewarganegaraan_ad})
  }
}

export function setKewarganegaraanAtDtTrv(kewarganegaraan_at){
  return{
    type: 'SET_OTHER_DATA_TRAVEL',
    value: Object.assign({},{kewarganegaraan_at})
  }
}

//data member
export function setPhoneDtTrv(member_phoneno){
  return (dispatch) => {
    dispatch(setPhonePpDtTrv(member_phoneno));
    dispatch(setPhonePsDtTrv(member_phoneno));
    dispatch(setPhoneApDtTrv(member_phoneno));
    dispatch(setPhoneAdDtTrv(member_phoneno));
    dispatch(setPhoneAtDtTrv(member_phoneno));
  }
}

export function setHandphoneDtTrv(member_hp){
  return (dispatch) => {
    dispatch(setHandphonePpDtTrv(member_hp));
    dispatch(setHandphonePsDtTrv(member_hp));
    dispatch(setHandphoneApDtTrv(member_hp));
    dispatch(setHandphoneAdDtTrv(member_hp));
    dispatch(setHandphoneAtDtTrv(member_hp));
  }
}

export function setEmailDtTrv(member_email){
  return (dispatch) => {
    dispatch(setEmailPpDtTrv(member_email));
    dispatch(setEmailPsDtTrv(member_email));
    dispatch(setEmailApDtTrv(member_email));
    dispatch(setEmailAdDtTrv(member_email));
    dispatch(setEmailAtDtTrv(member_email));
  }
}

export function setAlamatDtTrv(member_alamat){
  return (dispatch) => {
    dispatch(setAlamatPpDtTrv(member_alamat));
    dispatch(setAlamatPsDtTrv(member_alamat));
    dispatch(setAlamatApDtTrv(member_alamat));
    dispatch(setAlamatAdDtTrv(member_alamat));
    dispatch(setAlamatAtDtTrv(member_alamat));
  }
}

export function setHubAhliWarisDtTrv(member_hub_ahliwaris){
  return (dispatch) => {
    dispatch(setHubAhliWarisPpDtTrv(member_hub_ahliwaris));
    //dispatch(setHubAhliWarisPsDtTrv(member_hub_ahliwaris));
    //dispatch(setHubAhliWarisApDtTrv(member_hub_ahliwaris));
    //dispatch(setHubAhliWarisAdDtTrv(member_hub_ahliwaris));
    //dispatch(setHubAhliWarisAtDtTrv(member_hub_ahliwaris));
  }
}

export function setNamaAhliWarisDtTrv(member_ahliwaris){
  return (dispatch) => {
    dispatch(setNamaAhliWarisPpDtTrv(member_ahliwaris));
    //dispatch(setNamaAhliWarisPsDtTrv(member_ahliwaris));
    //dispatch(setNamaAhliWarisApDtTrv(member_ahliwaris));
    //dispatch(setNamaAhliWarisAdDtTrv(member_ahliwaris));
    //dispatch(setNamaAhliWarisAtDtTrv(member_ahliwaris));
  }
}

export function setTittleDtTrv(tittle){
  return (dispatch) => {
    dispatch(setTittlePpDtTrv(tittle));
    //dispatch(setTittlePsDtTrv(tittle));
    //dispatch(setTittleApDtTrv(tittle));
    //dispatch(setTittleAdDtTrv(tittle));
    //dispatch(setTittleAtDtTrv(tittle));
  }
}

export function setProvinsiDtTrv(province_id){
  return (dispatch) => {
    dispatch(setProvinsiPpDtTrv(province_id));
    dispatch(setProvinsiPsDtTrv(province_id));
    dispatch(setProvinsiApDtTrv(province_id));
    dispatch(setProvinsiAdDtTrv(province_id));
    dispatch(setProvinsiAtDtTrv(province_id));
  }
}

export function setDistrictDtTrv(district_id){
	var district_id_mem = district_id;
	if(district_id == 'Pilih')
	{
		district_id_mem = '';
	}
	
  return (dispatch) => {
    dispatch(setDistrictPpDtTrv(district_id));
    dispatch(setDistrictPsDtTrv(district_id_mem));
    dispatch(setDistrictApDtTrv(district_id_mem));
    dispatch(setDistrictAdDtTrv(district_id_mem));
    dispatch(setDistrictAtDtTrv(district_id_mem));
  }
}


//ID NO & KITAS
export function setIdNoPpDtTrv(member_idno){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_idno})
  }
}

export function setKitasPpDtTrv(member_kitas){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_kitas})
  }
}

export function setIdNoPsDtTrv(member_idno){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_idno})
  }
}

export function setKitasPsDtTrv(member_kitas){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_kitas})
  }
}

export function setIdNoApDtTrv(member_idno){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_idno})
  }
}

export function setKitasApDtTrv(member_kitas){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_kitas})
  }
}

export function setIdNoAdDtTrv(member_idno){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_idno})
  }
}

export function setKitasAdDtTrv(member_kitas){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_kitas})
  }
}

export function setIdNoAtDtTrv(member_idno){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_idno})
  }
}

export function setKitasAtDtTrv(member_kitas){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_kitas})
  }
}

//Tgl Lahir & Tempat Lahir
export function setTglLahirPpDtTrv(member_birthdate){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_birthdate})
  }
}

export function setTempatLahirPpDtTrv(tempat_lahir){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{tempat_lahir})
  }
}

export function setTglLahirPsDtTrv(member_birthdate){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_birthdate})
  }
}

export function setTempatLahirPsDtTrv(tempat_lahir){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{tempat_lahir})
  }
}

export function setTglLahirApDtTrv(member_birthdate){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_birthdate})
  }
}

export function setTempatLahirApDtTrv(tempat_lahir){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{tempat_lahir})
  }
}

export function setTglLahirAdDtTrv(member_birthdate){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_birthdate})
  }
}

export function setTempatLahirAdDtTrv(tempat_lahir){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{tempat_lahir})
  }
}

export function setTglLahirAtDtTrv(member_birthdate){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_birthdate})
  }
}

export function setTempatLahirAtDtTrv(tempat_lahir){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{tempat_lahir})
  }
}



//Gender
export function setGenderPpDtTrv(member_gender){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_gender})
  }
}

export function setGenderPsDtTrv(member_gender){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_gender})
  }
}

export function setGenderApDtTrv(member_gender){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_gender})
  }
}

export function setGenderAdDtTrv(member_gender){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_gender})
  }
}

export function setGenderAtDtTrv(member_gender){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_gender})
  }
}

//Nama Depan & Nama Belakang
export function setFirstNamePpDtTrv(member_firstname){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_firstname})
  }
}

export function setLastNamePpDtTrv(member_lastname){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_lastname})
  }
}

export function setFirstNamePsDtTrv(member_firstname){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_firstname})
  }
}

export function setLastNamePsDtTrv(member_lastname){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_lastname})
  }
}

export function setFirstNameApDtTrv(member_firstname){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_firstname})
  }
}

export function setLastNameApDtTrv(member_lastname){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_lastname})
  }
}

export function setFirstNameAdDtTrv(member_firstname){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_firstname})
  }
}

export function setLastNameAdDtTrv(member_lastname){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_lastname})
  }
}

export function setFirstNameAtDtTrv(member_firstname){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_firstname})
  }
}

export function setLastNameAtDtTrv(member_lastname){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_lastname})
  }
}


//File Identitas
export function setFileIdentitasDtTrv(file_identitas){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{file_identitas})
  }
}

//Provinsi & District
export function setDistrictPpDtTrv(district_id){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiPpDtTrv(province_id){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{province_id})
  }
}

export function setDistrictPsDtTrv(district_id){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiPsDtTrv(province_id){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{province_id})
  }
}


export function setDistrictApDtTrv(district_id){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiApDtTrv(province_id){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{province_id})
  }
}


export function setDistrictAdDtTrv(district_id){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiAdDtTrv(province_id){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{province_id})
  }
}


export function setDistrictAtDtTrv(district_id){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiAtDtTrv(province_id){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{province_id})
  }
}

//Title
export function setTittlePpDtTrv(tittle){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{tittle})
  }
}

export function setTittlePsDtTrv(tittle){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{tittle})
  }
}

export function setTittleApDtTrv(tittle){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{tittle})
  }
}

export function setTittleAdDtTrv(tittle){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{tittle})
  }
}

export function setTittleAtDtTrv(tittle){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{tittle})
  }
}

//Telpon Rumah

export function setPhonePpDtTrv(member_phoneno){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_phoneno})
  }
}

export function setHandphonePpDtTrv(member_hp){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_hp})
  }
}

export function setEmailPpDtTrv(member_email){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_email})
  }
}

export function setAlamatPpDtTrv(member_alamat){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_alamat})
  }
}

export function setNamaAhliWarisPpDtTrv(member_ahliwaris){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_ahliwaris})
  }
}

export function setHubAhliWarisPpDtTrv(member_hub_ahliwaris){
  return{
    type: 'SET_DATA_PEMEGANG_POLIS_TRAVEL',
    value: Object.assign({},{member_hub_ahliwaris})
  }
}

export function setPhonePsDtTrv(member_phone){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_phone})
  }
}

export function setHandphonePsDtTrv(member_hp){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_hp})
  }
}

export function setEmailPsDtTrv(member_email){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_email})
  }
}

export function setAlamatPsDtTrv(member_alamat){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_alamat})
  }
}

export function setNamaAhliWarisPsDtTrv(member_ahliwaris){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_ahliwaris})
  }
}

export function setHubAhliWarisPsDtTrv(member_hub_ahliwaris){
  return{
    type: 'SET_DATA_PASANGAN_TRAVEL',
    value: Object.assign({},{member_hub_ahliwaris})
  }
}

export function setPhoneApDtTrv(member_phone){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_phone})
  }
}

export function setHandphoneApDtTrv(member_hp){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_hp})
  }
}

export function setEmailApDtTrv(member_email){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_email})
  }
}

export function setAlamatApDtTrv(member_alamat){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_alamat})
  }
}

export function setNamaAhliWarisApDtTrv(member_ahliwaris){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_ahliwaris})
  }
}

export function setHubAhliWarisApDtTrv(member_hub_ahliwaris){
  return{
    type: 'SET_DATA_ANAK_PERTAMA_TRAVEL',
    value: Object.assign({},{member_hub_ahliwaris})
  }
}

export function setPhoneAdDtTrv(member_phone){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_phone})
  }
}

export function setHandphoneAdDtTrv(member_hp){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_hp})
  }
}

export function setEmailAdDtTrv(member_email){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_email})
  }
}

export function setAlamatAdDtTrv(member_alamat){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_alamat})
  }
}

export function setNamaAhliWarisAdDtTrv(member_ahliwaris){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_ahliwaris})
  }
}

export function setHubAhliWarisAdDtTrv(member_hub_ahliwaris){
  return{
    type: 'SET_DATA_ANAK_KEDUA_TRAVEL',
    value: Object.assign({},{member_hub_ahliwaris})
  }
}

export function setPhoneAtDtTrv(member_phone){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_phone})
  }
}

export function setHandphoneAtDtTrv(member_hp){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_hp})
  }
}

export function setEmailAtDtTrv(member_email){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_email})
  }
}

export function setAlamatAtDtTrv(member_alamat){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_alamat})
  }
}

export function setNamaAhliWarisAtDtTrv(member_ahliwaris){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_ahliwaris})
  }
}

export function setHubAhliWarisAtDtTrv(member_hub_ahliwaris){
  return{
    type: 'SET_DATA_ANAK_KETIGA_TRAVEL',
    value: Object.assign({},{member_hub_ahliwaris})
  }
}



