import * as service from './service'; 
import * as appFunction from '../function';
/* tambah list peserta */

export function setListPesertaAsuransi(items) {
    console.log('setListPesertaAsuransi ', items);
    return {
        type: 'SET_LIST_PESERTA_ASURANSI',
        items
    }
}

export function resetListPesertaAsuransi() {
    return {
        type: 'RESET_LIST_PESERTA_ASURANSI',
    }
}


/* end tambah list peserta */

/* tambah peserta */

export function setPesertaAsuransi(items) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        items
    }
}

export function resetPesertaAsuransi() {
    return {
        type: 'RESET_PESERTA_ASURANSI'
    }
}

export function setNamePesertaAsuransi(name) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        name
    }
}

export function setAddressPesertaAsuransi(address) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        address
    }
}

export function setPhonePesertaAsuransi(phone) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        phone
    }
}

export function setFileIdentitasPesertaAsuransi(file_identitas) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        file_identitas
    }
}

export function setEmailPesertaAsuransi(email) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        email
    }
}

export function setIdCardTypePesertaAsuransi(id_card_type) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        id_card_type
    }
}

export function setNoIdentitasPesertaAsuransi(no_identitas) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        no_identitas
    }
}

export function setTempatLahirPesertaAsuransi(tempat_lahir) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        tempat_lahir
    }
}

export function setTanggalLahirPesertaAsuransi(tanggal_lahir) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        tanggal_lahir
    }
}

export function setJenisKelaminPesertaAsuransi(jenis_kelamin) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        jenis_kelamin
    }
} 

export function setKewarganegaraanPesertaAsuransi(kewarganegaraan) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        kewarganegaraan
    }
}

export function setNegaraAsalPesertaAsuransi(negara_asal) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        negara_asal
    }
}

export function setStatusProfilePesertaAsuransi(status_profile) {
    return {
        type: 'SET_PESERTA_ASURANSI',
        status_profile
    }
}
/* end tambah peserta */

/* get list peserta */

export function setProfile(items, request) {
    return (dispatch) => {
       if(request == 'profile'){
           dispatch(setPesertaAsuransi(items));
       } else if (request == 'profilelist'){
           console.log('profilelist ', items);
           dispatch(setListPesertaAsuransi(items));
       }
    }
}

export function getProfileList(username) {
    return (dispatch) => {
        const request = 'profilelist';
        console.log('profilelist');
        var url = appFunction.serviceURL + 'getprofilelist?username=' + username;
        dispatch(fetchGetProfile(url, request));
    }
}

/* end get list peserta */

/* get peserta */

export function getProfile(id) {
    return (dispatch) => {
        const request = 'profile';
        var url = appFunction.serviceURL + 'getprofile?Id='+ id;
        dispatch(fetchGetProfile(url, request));
    }
}


export function fetchGetProfile(url, request){
    return (dispatch) => {

        fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.status !== 200) {
                //throw Error(response.statusText);
                Alert.alert(
                    'Error',
                    response.message
                );
            }
          
            return response;
        })
            .then((response) => response.json())
            .then((items) => {

                console.log('items fetch', items);
                dispatch(setProfile(items, request));
            })
            .catch(() => dispatch(service.itemsHasErrored(true)));
    };
}
/* end get peserta */


/* post peserta */

/* end post peserta */