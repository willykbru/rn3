import * as service from './service';
import * as appFunction from '../function';

export function setListMerek(items) {
    return{
      type: 'MERK_KENDARAAN_LIST',
      items: appFunction.toObject(items)
    }
}

export function setListSeri(items) {
    return{
      type: 'SERI_KENDARAAN_LIST',
      items: appFunction.toObject(items)
    }
}

export function setListPendingPayment(items) {
  //console.log('SET_PENDING_PAYMENT_LIST',items);
    return{
      type: 'SET_PENDING_PAYMENT_LIST',
      items
    }
}

export function setListSuccessPayment(items) {
    return{
      type: 'SET_SUCCESS_PAYMENT_LIST',
      items
    }
}

export function setPlatKendaraan(items) {

    return{
      type: 'PLAT_KENDARAAN_LIST',
      items
    }
}

export function setListAsuransi(items) {
    //console.log('setlistsuccess', items)
  return{
      type: 'SET_LIST_ASURANSI',
      items
  }
}

export function setCoverageMotor(items) {
  return{
      type: 'COVERAGE_MOTOR_LIST',
      items
  }
}

export function setCoverageMobil() {
  return{
      type: 'COVERAGE_MOBIL_LIST'
  }
}

export function setListPackageTravel(items) {
    return{
      type: 'LIST_PACKAGE_TRAVEL',
      items
    }
}
export function setListDaftarKeluargaTravel(items) {
    return{
      type: 'LIST_DAFTAR_KELUARGA_TRAVEL',
      items
    }
}

export function setListCity(items) {
    return{
      type: 'LIST_CITY',
      items
    }
}

export function setListCountry(items) {
    return{
      type: 'LIST_COUNTRY',
      items
    }
}

export function setListProvinsi(items) {
    return{
      type: 'LIST_PROVINSI',
      items
    }
}

export function setListDistrict(items) {
    return{
      type: 'LIST_DISTRICT',
      items
    }
}

export function setListKelurahan(items) {
    return{
      type: 'LIST_KELURAHAN',
      items
    }
}

export function setListKecamatan(items) {
    return{
      type: 'LIST_KECAMATAN',
      items
    }
}

export function setListOccupation(items) {
    return{
      type: 'LIST_OCCUPATION',
      items
    }
}

export function setListAhliWaris(items) {
  //console.log(items)
    return{
      type: 'SET_LIST_AHLI_WARIS',
      items
    }
}

export function setLoadingListAsuransiMV(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_MV',
      bool
    }
}

export function setLoadingListAsuransiProperty(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_PROPERTY',
      bool
    }
}

export function setLoadingListAsuransiApartment(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_APARTMENT',
      bool
    }
}

export function setLoadingListAsuransiAccident(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_ACCIDENT',
      bool
    }
}

export function setLoadingListAsuransiHealth(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_HEALTH',
      bool
    }
}

export function setLoadingListAsuransiLife(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_LIFE',
      bool
    }
}

export function setLoadingListAsuransiDelay(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_DELAY',
      bool
    }
}

export function setLoadingListAsuransiTravel(bool) {
  //console.log(items)
    return{
      type: 'LOADING_LIST_ASURANSI_TRAVEL',
      bool
    }
}

export function loadListPendingPayment(bool){
	return{
      type: 'LOADING_LIST_PENDING_PAYMENT',
      bool
    }
}

export function loadListSuccessPayment(bool){
	return{
      type: 'LOADING_LIST_SUCCESS_PAYMENT',
      bool
    }
}

export function loadDataPolis(bool){
	return{
      type: 'LOADING_DATA_POLIS',
      bool
    }
}

export function loadListAsuransi(bool, lob){
	return (dispatch) => {
		if(lob == 'MV')
			dispatch(setLoadingListAsuransiMV(bool));
		else if(lob == 'Property')
			dispatch(setLoadingListAsuransiProperty(bool));
		else if(lob == 'Apartment')
			dispatch(setLoadingListAsuransiApartment(bool));
		else if(lob == 'Accident')
			dispatch(setLoadingListAsuransiAccident(bool));
		else if(lob == 'Health')
			dispatch(setLoadingListAsuransiHealth(bool));
		else if(lob == 'Life')
			dispatch(setLoadingListAsuransiLife(bool));
		else if(lob == 'Delay')
			dispatch(setLoadingListAsuransiDelay(bool));
		else if(lob == 'Travel')
			dispatch(setLoadingListAsuransiTravel(bool));
	}	
}

export function resetListMerek(initial){
  return (dispatch) => {

    dispatch(setListMerek(initial));
  }
}

export function resetListSeri(initial){
  return (dispatch) => {

    dispatch(setListSeri(initial));
  }
}

export function resetListCoverage(){
  return (dispatch) => {
    dispatch(setCoverageMobil());
  }
}

export function resetListAsuransi(){
  return (dispatch) => {
    dispatch(setListAsuransi([]));
  }
}

export function resetListKecamatan(){
  const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];
  return (dispatch) => {

    dispatch(setListKecamatan(initial));
  }
}

export function resetListKelurahan(){
  const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];
  return (dispatch) => {

    dispatch(setListKelurahan(initial));
  }
}
export function resetListDistrict(){
  const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];
  return (dispatch) => {

    dispatch(setListDistrict(initial));
  }
}

export function resetListOccuption(){
  const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];
  return (dispatch) => {
	
    dispatch(setListOccupation(initial));
  }
}

export function resetListCity(){
  const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];
  return (dispatch) => {
	
    dispatch(setListCity(initial));
  }
}  
  
export function resetListCountry(){
  const initial = [{
    key: 'Pilih',
    label: 'Pilih',
  }];
  return (dispatch) => {
	
    dispatch(setListCountry(initial));
  }
}

export function getDataLobSuccess(items, request){
  return (dispatch) =>{
    if(request == 'listMerekMobil'){
      dispatch(setListMerek(items));
    }else if(request == 'listSeriMobil'){
      dispatch(setListSeri(items));
    }else if(request == 'listMerekMotor'){
      dispatch(setListMerek(items));
    }else if(request == 'listSeriMotor'){
      dispatch(setListSeri(items));
    }else if(request == 'listPlatKendaraan'){
      //console.log(request, items);
      const newItems = appFunction.toPlatObject(items);
    //  console.log(request, newItems);
      dispatch(setPlatKendaraan(newItems));
    }else if(request == 'listCity'){
      //console.log(request, items);
      const newItems = appFunction.toCityObject(items);
    //  console.log(request, newItems);
      dispatch(setListCity(newItems));
    }else if(request == 'listCountry'){
      //console.log(request, items);
      const newItems = appFunction.toCountryObject(items);
    //  console.log(request, newItems);
      dispatch(setListCountry(newItems));
    }else if(request == 'listProvinsi'){
      //console.log(request, items);
      const newItems = appFunction.toProvinsiObject(items);
    //  console.log(request, newItems);
      dispatch(setListProvinsi(newItems));
    }else if(request == 'listDistrict'){
      //console.log(request, items);
      const newItems = appFunction.toDistrictObject(items);
    //  console.log(request, newItems);
      dispatch(setListDistrict(newItems));
    }else if(request == 'listKecamatan'){
      //console.log(request, items);
      const newItems = appFunction.toKecamatanObject(items);
    //  console.log(request, newItems);
      dispatch(setListKecamatan(newItems));
    }else if(request == 'listKelurahan'){
      //console.log(request, items);
      const newItems = appFunction.toKelurahanObject(items);
    //  console.log(request, newItems);
      dispatch(setListKelurahan(newItems));
    }else if(request == 'listOkupasiProperty'){
      //console.log(request, items);
      const newItems = appFunction.toOkupasiPropertyObject(items);
    //  console.log(request, newItems);
      dispatch(setListOccupation(newItems));
    }else if(request == 'listOkupasiApartment'){
      //console.log(request, items);
      const newItems = appFunction.toOkupasiApartmentObject(items);
    //  console.log(request, newItems);
      dispatch(setListOccupation(newItems));
    }else if(request == 'listOkupasiAccident'){
      //console.log(request, items);
      const newItems = appFunction.toAccidentObject(items);
    //  console.log(request, newItems);
      dispatch(setListOccupation(newItems));
    }else if(request == 'listOkupasiLife'){
      console.log(request, items);
      const newItems = appFunction.toLifeObject(items);
    //  console.log(request, newItems);
      dispatch(setListOccupation(newItems));
    }else if(request == 'listAhliWaris'){
      //console.log(request, items);
      console.log('listahli', items)
      const newItems = appFunction.toAhliWarisObject(items);
    //  console.log(request, newItems);
      dispatch(setListAhliWaris(newItems));
    }else if(request == 'listPendingCart'){
      //console.log(request, items);
      //console.log('listahli', items)

    //  console.log(request, newItems);
      dispatch(setListPendingCart(items));
    }else if(request == 'listPendingPayment'){
      //console.log(request, items);
      //console.log('listahli', items)
      //const newItems = appFunction.mergeArrayObject(items); //cart
      console.log('listahli', items); //cart
      //dispatch(setListPendingPayment(newItems)); //cart
      dispatch(setListPendingPayment(appFunction.groupBy(items, item => item.payment_id)));
    }else if(request == 'listSuccessPayment'){
      //console.log(request, items);
      //console.log('listahli', items)

    //  console.log(request, newItems);
      dispatch(setListSuccessPayment(items));
    }else if(request == 'listPackageTravel'){
	  const newItems = appFunction.toPaketTravelObject(items);
      dispatch(setListPackageTravel(newItems));
    }else if(request == 'listDaftarKeluargaTravel'){
		const newItems = appFunction.toDaftarKeluargaTravelObject(items);
      dispatch(setListDaftarKeluargaTravel(newItems));
	}
  }
}

export function getListAsuransiSuccess(items){

  console.log('getlistsuccess', items);
  return (dispatch) =>{
      dispatch(setListAsuransi(items));
  }
}

export function getMerekMobil(){
  return (dispatch) => {
    //.log(data);
    const request = 'listMerekMobil';
    var url = appFunction.serviceURL + '/vehicle/getmerkmobil';
    dispatch(getDataLob(url, request));
  }
}

export function getSeriMobil(merek){
  return (dispatch) => {
    //.log(data);
    const request = 'listSeriMobil';
    var url = appFunction.serviceURL + '/vehicle/getmerkmobil/' + merek;
    dispatch(getDataLob(url, request));
  }
}

export function getMerekMotor(){
  return (dispatch) => {
    //.log(data);
    const request = 'listMerekMotor';
    var url = appFunction.serviceURL + '/vehicle/getmerkmotor';
    dispatch(getDataLob(url, request));
  }
}

export function getSeriMotor(merek){
  return (dispatch) => {
    //.log(data);
    const request = 'listSeriMotor';
    var url = appFunction.serviceURL + '/vehicle/getmerkmotor/' + merek;
    dispatch(getDataLob(url, request));
  }
}

export function getPlatKendaraan(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listPlatKendaraan';
    var url = appFunction.serviceURL + 'vehicle/getplat';
    dispatch(getDataLob(url, request));
  }
}

export function getCountry(){
  return (dispatch) => {
    //console.log('getoc');
    const request = 'listCountry';
    var url = appFunction.serviceURL + 'travel/country';
    dispatch(getDataLob(url, request));
  }
}

export function getCity(){
  return (dispatch) => {
    //console.log('getoc');
    const request = 'listCity';
    var url = appFunction.serviceURL + 'travel/city';
    dispatch(getDataLob(url, request));
  }
}

export function getProvinsi(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listProvinsi';
    var url = appFunction.serviceURL + 'properties/getprovince';
    dispatch(getDataLob(url, request));
  }
}

export function getDistrict(province_id = ''){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listDistrict';
    var url = appFunction.serviceURL + 'properties/getdistrict/' + province_id;
	if(!province_id){
		url = appFunction.serviceURL + 'properties/getdistrict/';
	} 
    dispatch(getDataLob(url, request));
  }
}

export function getKecamatan(district_id){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listKecamatan';
    var url = appFunction.serviceURL + 'properties/getkecamatan/' + district_id;
    dispatch(getDataLob(url, request));
  }
}

export function getKelurahan(kecamatan_id){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listKelurahan';
    var url = appFunction.serviceURL + 'properties/getkelurahan/' + kecamatan_id;
    dispatch(getDataLob(url, request));
  }
}

export function getOkupasiProperty(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listOkupasiProperty';
    var url = appFunction.serviceURL + 'properties/getoccupationproperties';
    dispatch(getDataLob(url, request));
  }
}

export function getOkupasiApartment(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listOkupasiApartment';
    var url = appFunction.serviceURL + 'properties/getoccupationapartement';
    dispatch(getDataLob(url, request));
  }
}
export function getOkupasiAccident(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listOkupasiAccident';
    var url = appFunction.serviceURL + 'accident/getpekerjaan/';
    dispatch(getDataLob(url, request));
  }
}

export function getOkupasiLife(){
  return (dispatch) => {
    console.log('getoc');
    const request = 'listOkupasiLife';
    var url = appFunction.serviceURL + 'life/getpekerjaan/';
    dispatch(getDataLob(url, request));
  }
}

export function getAhliWaris(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listAhliWaris';
    var url = appFunction.serviceURL + 'accident/getahliwaris/';
    dispatch(getDataLob(url, request));
  }
}


export function getPendingPayment(username){
  return (dispatch) => {
    console.log('getPendingPayment');
    const request = 'listPendingPayment';
    var url = appFunction.serviceURL + 'getpaymentlist?username=' + username;
    //var url = appFunction.serviceURL + 'getpaymentlist?username=' + username; //cart
    dispatch(getDataLob(url, request));
  }
}

export function getSuccessPayment(username){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listSuccessPayment';
    var url = appFunction.serviceURL + 'transaction/'+ username +'/success';
    dispatch(getDataLob(url, request));
  }
}


export function getPackageTravel(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listPackageTravel';
    var url = appFunction.serviceURL + 'travel/packagetype/';
    dispatch(getDataLob(url, request));
  }
}

export function getDaftarKeluargaTravel(){
  return (dispatch) => {
    //console.log('getplat');
    const request = 'listDaftarKeluargaTravel';
    var url = appFunction.serviceURL + 'travel/daftarkeluarga/';
    dispatch(getDataLob(url, request));
  }
}


export function getListAsuransi(body, lob){
  return (dispatch) => {
    if(lob == 'MV'){
      //console.log(body);
      var url = appFunction.serviceURL + 'vehicle/getrate';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Property'){
      var url = appFunction.serviceURL + 'properties/getpackage';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Apartment'){
      var url = appFunction.serviceURL + 'properties/getpackageapartemen';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Accident'){
      var url = appFunction.serviceURL + 'accident/getpackageaccident';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Health'){
      var url = appFunction.serviceURL + 'health/getpackagehealth';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Life'){
      var url = appFunction.serviceURL + 'life/getpackagelife';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Delay'){
      var url = appFunction.serviceURL + 'delay/getpackagedelay';
      dispatch(fetchListAsuransi(url, body, lob));
    }else if(lob == 'Travel'){
      var url = appFunction.serviceURL + 'travel/getpackage';
      dispatch(fetchListAsuransi(url, body, lob));
    }
  }
}


export function getCoverageKendaraan(kendaraan){
  return (dispatch) => {
    var coverageList = [{
        key:'Total Loss Only',
        label:'Total Loss Only'
      },
      {
        key:'Comprehensive',
        label:'Comprehensive'
      }];
    if(kendaraan == 'Mobil'){
        dispatch(setCoverageMobil());
      }else if(kendaraan == 'Motor'){
        coverageList.pop();
          dispatch(setCoverageMotor(coverageList));
    }
  }
}

export function fetchListAsuransi(url, body, lob){
  return (dispatch) => {
    //console.log(body);
      dispatch(loadListAsuransi(true, lob));
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
        }).then((response) => {
              if (!response.ok) {
                  //throw Error(response.statusText);
                  Alert.alert(
                    'Error',
                      response.statusText
                  );
              }
              dispatch(loadListAsuransi(false, lob));
              return response;
          })
          .then((response) => response.json())
          .then((items) => {
          //  console.log(items);
            dispatch(getListAsuransiSuccess(items));
          })
          .catch(() => dispatch(service.itemsHasErrored(true)));
  };
}

export function getDataLob(url, request){
  return (dispatch) => {
    console.log(request, url);
	if(request == 'listPendingCart')
	  dispatch(loadListPendingCart(true));
    else if(request == 'listPendingPayment')
	  dispatch(loadListPendingPayment(true));
	else if(request == 'listSuccessPayment')
	  dispatch(loadListSuccessPayment(true));
	else
      dispatch(service.itemsIsLoading(true));
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        }).then((response) => {
              if (!response.ok) {
                  //throw Error(response.statusText);
                  Alert.alert(
                    'Error',
                      response.statusText
                  );
              }
              if(request == 'listPendingCart')
				  dispatch(loadListPendingCart(false));
			  else if(request == 'listPendingPayment')
				  dispatch(loadListPendingPayment(false));
			  else if(request == 'listSuccessPayment')
				dispatch(loadListSuccessPayment(false));
			  else
				  dispatch(service.itemsIsLoading(false));
              return response;
          })
          .then((response) => response.json())
          .then((items) => {
            console.log('datapayment',items);
            dispatch(getDataLobSuccess(items, request));
          })
          .catch(() => dispatch(service.itemsHasErrored(true)));
  };
}

/* set pending cart net premi */
export function resetListPendingPayment() {
  return {
    type: 'RESET_PENDING_PAYMENT_LIST',
  }
}

export function setListPendingCartPremi(net_premi, kode_transaksi) {
  var itemsnet_premi = net_premi;
  if (net_premi == null || net_premi == undefined){
    itemsnet_premi=0;
  }
  console.log(kode_transaksi + ' 2', net_premi);
  return {
    type: 'LIST_PENDING_CART_PREMI',
    items: Object.assign({}, { kode_transaksi, net_premi: itemsnet_premi })
  }
}

export function resetListPendingCartPremi() {
  return {
    type: 'RESET_LIST_PENDING_CART_PREMI',
  }
}

export function getPendingCartPremi(kode_transaksi){
  return (dispatch) => {
    console.log('getplat ', kode_transaksi);
    var url = appFunction.serviceURL + 'detail/'+ kode_transaksi;
    dispatch(fetchListPendingCartPremi(url, kode_transaksi));
  }
}

export function fetchListPendingCartPremi(url, kode_transaksi) {
  console.log('url',url);
  return (dispatch) => {
    dispatch(loadListPendingCartPremi(true));
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (!response.ok) {
        //throw Error(response.statusText);
        Alert.alert(
          'Error',
          response.statusText
        );
      }
      dispatch(loadListPendingCartPremi(false));
      return response;
    })
      .then((response) => response.json())
      .then((items) => {
       // console.log(kode_transaksi, items.net_premi);
        dispatch(setListPendingCartPremi(items.net_premi, kode_transaksi));
      })
      .catch(() => dispatch(service.itemsHasErrored(true)));
  };
}



/* end set pending cart net premi */


/*cart list*/
	export function getPendingCart(username){
	  return (dispatch) => {
    //console.log('getplat');
		const request = 'listPendingCart';
		var url = appFunction.serviceURL + 'transaction/'+ username +'/pending';
		dispatch(getDataLob(url, request));
	  }
	}


	export function setListPendingCart(items) {
		return{
		  type: 'SET_PENDING_CART_LIST',
		  items
		}
	}
	
	export function loadListPendingCart(bool){
		return{
		  type: 'LOADING_LIST_PENDING_CART',
		  bool
		}
  }


export function loadListPendingCartPremi(bool) {
  return {
    type: 'LOADING_LIST_PENDING_CART_PREMI',
    bool
  }
}

  


    /*end of cart list*/
    
    /* payment */
export function getPendingList(username) {
      return (dispatch) => {
        console.log('getplat');
        const request = 'getpaymentlist';
        var url = appFunction.serviceURL + 'getpaymentlist?username=' + username ;
        dispatch(fetchPaymentList(url, request));
      }
    }


export function fetchPaymentList(url) {
  console.log('url', url);
  return (dispatch) => {
    dispatch(service.itemsIsLoading(true));
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (!response.ok) {
        //throw Error(response.statusText);
        Alert.alert(
          'Error',
          response.statusText
        );
      }
      dispatch(service.itemsIsLoading(false));
      return response;
    })
      .then((response) => response.json())
      .then((items) => {
        //console.log('fetchListPendingCartPremi', items);
        dispatch(setListPendingPayment(items));
      })
      .catch(() => dispatch(service.itemsHasErrored(true)));
  };
}

    /*end payment*/
