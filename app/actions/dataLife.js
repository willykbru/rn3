
export function setTittleDtLfe(tittle){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{tittle})
  }
}

export function setAlamatPemilikDtLfe(alamat){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{alamat})
  }
}
export function setKodeVoucherDtLfe(kode_voucher){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{kode_voucher})
  }
}

export function setNamaPemilikDtLfe(nama){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{nama})
  }
}

export function setNoIdentitasDtLfe(no_identitas){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{no_identitas})
  }
}

export function setGenderDtLfe(gender){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{gender})
  }
}

export function setHubAhliWarisDtLfe(hub_ahliwaris){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{hub_ahliwaris})
  }
}

export function setKewarganegaraanDtLfe(kewarganegaraan){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{kewarganegaraan})
  }
}
export function setNamaAhliWarisDtLfe(nama_ahliwaris){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{nama_ahliwaris})
  }
}

export function setDistrictDtLfe(district_id){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiDtLfe(province_id){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{province_id})
  }
}

export function setFileIdentitasDtLfe(file_identitas){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{file_identitas})
  }
}

export function setTglLahirDtLfe(tgl_lahir){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{tgl_lahir})
  }
}
export function setVisitorIdDtLfe(visitor_id){
  return{
    type: 'SET_DATA_LIFE',
    value: Object.assign({},{visitor_id})
  }
}
