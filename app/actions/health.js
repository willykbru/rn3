import * as dataHealth from './dataHealth';


export function setHealthState(value, state){
  return (dispatch) => {
    if(state == 'tittle'){
      dispatch(dataHealth.setTittleDtHlt(value));
    }else if(state == 'province_id'){
      dispatch(dataHealth.setProvinsiDtHlt(value.val))
      dispatch(setLabelProvinsiHealth(value.label))
    }else if(state == 'district_id'){
      dispatch(dataHealth.setDistrictDtHlt(value.val))
      dispatch(setLabelDistrictHealth(value.label))
    }
  }
}


export function resetDistrictDtHlt(){
  return (dispatch)=>{
    dispatch(dataHealth.setDistrictDtHlt('Pilih'));
    dispatch(setLabelDistrictHealth('Pilih'));
  }
}

export function setPackageHealth(package_id){
  return{
    type: 'SET_HEALTH_STATE',
    value: Object.assign({},{package_id})
  }
}

export function setLabelProvinsiHealth(label_provinsi){
  return{
    type: 'SET_HEALTH_LABEL_STATE',
    value: Object.assign({},{label_provinsi})
  }
}

export function setLabelDistrictHealth(label_district){
  return{
    type: 'SET_HEALTH_LABEL_STATE',
    value: Object.assign({},{label_district})
  }
}

export function setTglLahirHealth(tgl_lahir){
  return{
    type: 'SET_HEALTH_STATE',
    value: Object.assign({},{tgl_lahir})
  }
}

export function setAsuransiHealth(value){
  console.log(value);
  return{
    type: 'SET_ASURANSI_HEALTH',
    value
  }
}
