export function setEQVETProperty(eqvet){
  //console.log('eqvet',Object.assign({},{eqvet}));
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{eqvet})
  }
}

export function setTSFWDProperty(flood){
  //console.log('flood',Object.assign({},{flood}));
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{flood})
  }
}

export function setAsuransiProperty(value){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_ASURANSI_PROPERTY',
    value
  }
}

export function setStatusPerluasanProperty(status, perluasan){
  return (dispatch) => {
    //console.log('set perluasanmv',status);
    //  console.log('set perluasanmv',perluasan);
  if(perluasan == 'TSFWD'){
      dispatch(setTSFWDProperty(status));
    }else if(perluasan == 'EQVET'){
      dispatch(setEQVETProperty(status));
    }
  }
}
