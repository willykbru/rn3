import * as dataTravel from './dataTravel';


export function setTravelState(value, state){
  return (dispatch) => {
    if(state == 'tittle'){
      dispatch(dataTravel.setTittleDtTrv(value));
    }else if(state == 'travel_type'){
      dispatch(setTravelTypeTravel(value.val));
      dispatch(setLabelTravelTypeTravel(value.label));
    }else if(state == 'member_hub_ahliwaris'){
	  dispatch(dataTravel.setHubAhliWarisDtTrv(value));
	}else if(state == 'package_type'){
      dispatch(setPackageTypeTravel(value.val));
	  dispatch(setLabelPackageTypeTravel(value.label));
    }else if(state == 'trip_type'){
      dispatch(setTripTypeTravel(value.val));
	  dispatch(setLabelTripTravel(value.label));
    }else if(state == 'daftar_keluarga'){
      dispatch(setDaftarKeluargaTravel(value.val));
	  dispatch(setLabelDaftarKeluargaTravel(value.label));
    }else if(state == 'city_id'){
      dispatch(setCityTravel(value.val))
      dispatch(setLabelCityTravel(value.label))
    }else if(state == 'country_id'){
      dispatch(setCountryTravel(value.val))
      dispatch(setLabelCountryTravel(value.label))
    }else if(state == 'province_id'){
      dispatch(dataTravel.setProvinsiDtTrv(value.val))
      dispatch(setLabelProvinsiTravel(value.label))
    }else if(state == 'district_id'){
      dispatch(dataTravel.setDistrictDtTrv(value.val))
      dispatch(setLabelDistrictTravel(value.label))
    }
  }
}
  
export function setLabelTravelTypeTravel(label_travel_type){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_travel_type})
  }
}

export function setLabelCityTravel(label_city){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_city})
  }
}

export function setLabelCountryTravel(label_country){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_country})
  }
}

export function setLabelPackageTypeTravel(label_package_type){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_package_type})
  }
}

export function setPackageTravel(package_id){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{package_id})
  }
}

export function setCityTravel(city_id){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{city_id})
  }
}

export function setCountryTravel(country_id){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{country_id})
  }
}

export function setDaftarKeluargaTravel(daftar_keluarga){

  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{daftar_keluarga})
  }
}

export function resetCityTravel(value){
  return (dispatch)=>{
	dispatch(setCityTravel(value));
	dispatch(setLabelCityTravel(value));
  }
}

export function resetCountryTravel(value){
  return (dispatch)=>{
	dispatch(setCountryTravel(value));
	dispatch(setLabelCountryTravel(value));
  }
}

export function resetDistrictDtTrv(value = 'Pilih'){
  return (dispatch)=>{
	dispatch(dataTravel.setDistrictDtTrv(value));
	dispatch(setLabelDistrictTravel(value));
  }
}



export function resetDaftarKeluargaTravel(value){
  return (dispatch)=>{
	dispatch(setDaftarKeluargaTravel(value));
	dispatch(setLabelDaftarKeluargaTravel(value));
  }
}

export function setStartDateTravel(start_date){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{start_date})
  }
}

export function setEndDateTravel(end_date){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{end_date})
  }
}

export function setPackageTypeTravel(package_type){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{package_type})
  }
}

export function setTravelTypeTravel(travel_type){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{travel_type})
  }
}

export function setTripTypeTravel(trip_type){
  return{
    type: 'SET_TRAVEL_STATE',
    value: Object.assign({},{trip_type})
  }
}

export function setLabelStartDateTravel(label_start_date){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_start_date})
  }
}

export function setLabelEndDateTravel(label_end_date){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_end_date})
  }
}

export function setLabelTripTravel(label_trip_type){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_trip_type})
  }
}
export function setLabelDaftarKeluargaTravel(label_daftar_keluarga){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_daftar_keluarga})
  }
}
export function setLabelProvinsiTravel(label_provinsi){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_provinsi})
  }
}

export function setLabelDistrictTravel(label_district){
  return{
    type: 'SET_TRAVEL_LABEL_STATE',
    value: Object.assign({},{label_district})
  }
}

export function setAsuransiTravel(value){
 // console.log(value);
  return{
    type: 'SET_ASURANSI_TRAVEL',
    value
  }
}
