export function setTittleDtDly(tittle){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{tittle})
  }
}

export function setCityDestinationDtDly(city_destination){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{city_destination})
  }
}

export function setCityOriginDtDly(city_origin){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{city_origin})
  }
}

export function setCountryDestinationDtDly(country_destination){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{country_destination})
  }
}

export function setFirstNameDtDly(first_name){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{first_name})
  }
}

export function setLastNameDtDly(last_name){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{last_name})
  }
}

export function setKodeVoucherDtDly(kode_voucher){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{kode_voucher})
  }
}

export function setKodeBookingDtDly(kode_booking){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{kode_booking})
  }
}

export function setEndDateDtDly(end_date){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{end_date})
  }
}

export function setStartDateDtDly(start_date){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{start_date})
  }
}

export function setNoKtpDtDly(no_ktp){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{no_ktp})
  }
}

export function setPhotoKtpDtDly(photo_ktp){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{photo_ktp})
  }
}


export function setHubAhliWarisDtDly(member_hubahliwaris){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_hubahliwaris})
  }
}

export function setNamaAhliWarisDtDly(member_ahliwaris){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_ahliwaris})
  }
}

export function setAlamatDtDly(member_alamat){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_alamat})
  }
}

export function setEmailDtDly(member_email){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_email})
  }
}

export function setGenderDtDly(member_gender){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_gender})
  }
}

export function setHandphoneDtDly(member_hp){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_hp})
  }
}

export function setPhoneDtDly(member_phone){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_phone})
  }
}

export function setIdNoDtDly(member_idno){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_idno})
  }
}

export function setKitasDtDly(member_kitas){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{member_kitas})
  }
}

export function setDistrictDtDly(district_id){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiDtDly(province_id){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{province_id})
  }
}

export function resetCityOriginDtDly(){
  return (dispatch)=>{
    dispatch(setCityOriginDtDly('Pilih'));
  }
}

export function resetCityDestinationDtDly(){
  return (dispatch)=>{
    dispatch(setCityDestinationDtDly('Pilih'));
  }
}

export function resetCountryDestinationDtDly(){
  return (dispatch)=>{
    dispatch(setCountryDestinationDtDly('Pilih'));
  }
}

export function resetDistrictDtDly(){
  return (dispatch)=>{
    dispatch(setDistrictDtDly('Pilih'));
  }
}

export function setTglLahirDtDly(birth_date){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{birth_date})
  }

}

export function setTempatLahirDtDly(tempat_lahir){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{tempat_lahir})
  }
}

export function setTravelTypeDtDly(travel_type){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{travel_type})
  }
}

export function setVisitorIdDtDly(visitor_id){
  return{
    type: 'SET_DATA_DELAY',
    value: Object.assign({},{visitor_id})
  }
}
