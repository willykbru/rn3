export function resetDataKendaraan(){
  return{
    type: 'RESET_DATA_KENDARAAN',
  }
}

export function setTittleDK(tittle){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{tittle})
  }
}

export function setAlamatPemilikDK(alamat_pemilik){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{alamat_pemilik})
  }
}
export function setKodeVoucherDK(kode_voucher){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{kode_voucher})
  }
}
export function setMerekDK(merek){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{merek})
  }
}
export function setNamaPemilikDK(nama_pemilik){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{nama_pemilik})
  }
}

export function setNamaSTNKDK(nama_stnk){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{nama_stnk})
  }
}
export function setKTPDK(no_ktp){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{no_ktp})
  }
}
export function setNoMesinDK(no_mesin){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{no_mesin})
  }
}
export function setNoPlatDK(no_plat){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{no_plat})
  }
}
export function setNoRangkaDK(no_rangka){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{no_rangka})
  }
}
export function setPenggunaanDK(penggunaan){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{penggunaan})
  }
}
export function setPhotoBelakangDK(photo_belakang){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{photo_belakang})
  }
}
export function setPhotoDepanDK(photo_depan){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{photo_depan})
  }
}

export function setPhotoKananDK(photo_kanan){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{photo_kanan})
  }
}
export function setPhotoKiriDK(photo_kiri){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{photo_kiri})
  }
}
export function setPhotoKtpDK(photo_ktp){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{photo_ktp})
  }
}
export function setPhotoStnkDK(photo_stnk){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{photo_stnk})
  }
}
export function setTipeDK(seri_tipe){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{seri_tipe})
  }
}
export function setQQDK(status_qq){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{status_qq})
  }
}
export function setTglLahirDK(tgl_lahir){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{tgl_lahir})
  }
}
export function setVisitorIdDK(visitor_id){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{visitor_id})
  }
}

export function setWarnaDK(warna){
  return{
    type: 'SET_DATA_KENDARAAN',
    value: Object.assign({},{warna})
  }
}
