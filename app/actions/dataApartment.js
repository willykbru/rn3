
export function resetDataApartement(){
  return{
    type: 'RESET_DATA_APARTMENT',
  }
}

export function setTittleDtApt(tittle){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{tittle})
  }
}

export function setAlamatPemilikDtApt(alamat_pemilik){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{alamat_pemilik})
  }
}
export function setKodeVoucherDtApt(kode_voucher){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{kode_voucher})
  }
}

export function setNamaPemilikDtApt(nama_pemilik){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{nama_pemilik})
  }
}

export function setKTPDtApt(ktp){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{ktp})
  }
}
export function setNamaBankDtApt(namabankleasing){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{namabankleasing})
  }
}

export function setPhotoKtpDtApt(photo_ktp){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{photo_ktp})
  }
}

export function setTipeDtApt(seri_tipe){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{seri_tipe})
  }
}
export function setQQDtApt(statusqq){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{statusqq})
  }
}
export function setTglLahirDtApt(tgl_lahir){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{tgl_lahir})
  }
}
export function setVisitorIdDtApt(visitor_id){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{visitor_id})
  }
}
