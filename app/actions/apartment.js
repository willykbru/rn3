import * as dataApartment from './dataApartment';

export function setLantaiApartment(lantai){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{lantai})
  }
}

export function setContentTsiApartment(content_tsi){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{content_tsi})
  }
}

export function setProvinsiApartment(province_id){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{province_id})
  }
}

export function setDistrictApartment(district_id){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{district_id})
  }
}

export function setKecamatanApartment(kecamatan_id){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{kecamatan_id})
  }
}

export function setKelurahanApartment(kelurahan_id){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{kelurahan_id})
  }
}

export function setRiskAddressApartment(risk_address){
  return{
    type: 'SET_DATA_APARTMENT',
    value: Object.assign({},{risk_address})
  }
}

export function setPackageApartment(package_id){
  return{
    type: 'SET_APARTMENT_STATE',
    value: Object.assign({},{package_id})
  }
}

export function setLabelProvinsiApartment(label_provinsi){
  return{
    type: 'SET_APARTMENT_LABEL_STATE',
    value: Object.assign({},{label_provinsi})
  }
}
export function setLabelDistrictApartment(label_district){
  return{
    type: 'SET_APARTMENT_LABEL_STATE',
    value: Object.assign({},{label_district})
  }
}
export function setLabelKecamatanApartment(label_kecamatan){
  return{
    type: 'SET_APARTMENT_LABEL_STATE',
    value: Object.assign({},{label_kecamatan})
  }
}
export function setLabelKelurahanApartment(label_kelurahan){
  return{
    type: 'SET_APARTMENT_LABEL_STATE',
    value: Object.assign({},{label_kelurahan})
  }
}

export function setLabelLantaiApartment(label_lantai){
  return{
    type: 'SET_APARTMENT_LABEL_STATE',
    value: Object.assign({},{label_lantai})
  }
}

export function setAsuransiApartment(value){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_ASURANSI_APARTMENT',
    value
  }
}

export function resetProvinsiApartment(){
  return (dispatch) => {
    dispatch(setProvinsiApartment('Pilih'));
    dispatch(setLabelProvinsiApartment('Pilih'));
  }
}

export function resetDistrictApartment(){
  return (dispatch) => {
    dispatch(setDistrictApartment('Pilih'));
    dispatch(setLabelDistrictApartment('Pilih'));
  }
}

export function resetKecamatanApartment(){
  return (dispatch) => {
    dispatch(setKecamatanApartment('Pilih'));
    dispatch(setLabelKecamatanApartment('Pilih'));
  }
}

export function resetKelurahanApartment(){
  return (dispatch) => {
    dispatch(setKelurahanApartment('Pilih'));
    dispatch(setLabelKelurahanApartment('Pilih'));
  }
}

export function setApartmentState(value, state){
  return (dispatch) => {
    if(state == 'province_id'){
      dispatch(setProvinsiApartment(value.val));
      dispatch(setLabelProvinsiApartment(value.label));
    }else if(state == 'district_id'){
      dispatch(setDistrictApartment(value.val));
      dispatch(setLabelDistrictApartment(value.label));
    }else if(state == 'kecamatan_id'){
      dispatch(setKecamatanApartment(value.val));
      dispatch(setLabelKecamatanApartment(value.label));
    }else if(state == 'kelurahan_id'){
      dispatch(setKelurahanApartment(value.val));
      dispatch(setLabelKelurahanApartment(value.label));
    }else if(state == 'lantai'){
      //console.log(value);
      dispatch(setLantaiApartment(value.val));
      dispatch(setLabelLantaiApartment(value.label));
    }else if(state == 'tittle'){
      dispatch(dataApartment.setTittleDtApt(value));
    }
  }
}
