
export function setTittleDtAcd(tittle){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{tittle})
  }
}

export function setTempatLahirDtAcd(tempat_lahir){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{tempat_lahir})
  }
}

export function setAlamatPemilikDtAcd(alamat){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{alamat})
  }
}
export function setKodeVoucherDtAcd(kode_voucher){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{kode_voucher})
  }
}

export function setNamaPemilikDtAcd(nama){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{nama})
  }
}

export function setNoIdentitasDtAcd(no_identitas){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{no_identitas})
  }
}

export function setGenderDtAcd(gender){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{gender})
  }
}

export function setHubAhliWarisDtAcd(hub_ahliwaris){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{hub_ahliwaris})
  }
}

export function setKewarganegaraanDtAcd(kewarganegaraan){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{kewarganegaraan})
  }
}
export function setNamaAhliWarisDtAcd(nama_ahliwaris){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{nama_ahliwaris})
  }
}

export function setDistrictDtAcd(district_id){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiDtAcd(province_id){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{province_id})
  }
}

export function setFileIdentitasDtAcd(file_identitas){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{file_identitas})
  }
}

export function setTglLahirDtAcd(tgl_lahir){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{tgl_lahir})
  }
}
export function setVisitorIdDtAcd(visitor_id){
  return{
    type: 'SET_DATA_ACCIDENT',
    value: Object.assign({},{visitor_id})
  }
}
