import * as dataAccident from './dataAccident';


export function setAccidentState(value, state){
  return (dispatch) => {
    if(state == 'pekerjaan'){
      dispatch(setPekerjaanAccident(value.val));
      dispatch(setLabelPekerjaanAccident(value.label));
    }else if(state == 'tittle'){
      dispatch(dataAccident.setTittleDtAcd(value));
    }else if(state == 'hub_ahliwaris'){
      console.log('hub ahli')
      dispatch(dataAccident.setHubAhliWarisDtAcd(value));
    }else if(state == 'province_id'){
      dispatch(dataAccident.setProvinsiDtAcd(value.val))
      dispatch(setLabelProvinsiAccident(value.label))
    }else if(state == 'district_id'){
      dispatch(dataAccident.setDistrictDtAcd(value.val))
      dispatch(setLabelDistrictAccident(value.label))
    }
  }
}

export function setPackageAccident(package_id){
  return{
    type: 'SET_ACCIDENT_STATE',
    value: Object.assign({},{package_id})
  }
}

export function resetDistrictDtAcd(){
  return (dispatch)=>{
    dispatch(dataAccident.setDistrictDtAcd('Pilih'));
    dispatch(setLabelDistrictAccident('Pilih'));
  }
}

export function setPekerjaanAccident(pekerjaan){
  return{
    type: 'SET_ACCIDENT_STATE',
    value: Object.assign({},{pekerjaan})
  }
}

export function setLabelPekerjaanAccident(label_pekerjaan){
  return{
    type: 'SET_ACCIDENT_LABEL_STATE',
    value: Object.assign({},{label_pekerjaan})
  }
}

export function setLabelProvinsiAccident(label_provinsi){
  return{
    type: 'SET_ACCIDENT_LABEL_STATE',
    value: Object.assign({},{label_provinsi})
  }
}

export function setLabelDistrictAccident(label_district){
  return{
    type: 'SET_ACCIDENT_LABEL_STATE',
    value: Object.assign({},{label_district})
  }
}

export function setTglLahirAccident(tgl_lahir){
  return{
    type: 'SET_ACCIDENT_STATE',
    value: Object.assign({},{tgl_lahir})
  }
}

export function setAsuransiAccident(value){
  console.log(value);
  return{
    type: 'SET_ASURANSI_ACCIDENT',
    value
  }
}
