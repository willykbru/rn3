
export function setTittleDtHlt(tittle){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{tittle})
  }
}

export function setAlamatPemilikDtHlt(alamat){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{alamat})
  }
}
export function setKodeVoucherDtHlt(kode_voucher){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{kode_voucher})
  }
}

export function setNamaPemilikDtHlt(nama){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{nama})
  }
}

export function setNoIdentitasDtHlt(no_identitas){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{no_identitas})
  }
}

export function setGenderDtHlt(gender){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{gender})
  }
}

export function setKewarganegaraanDtHlt(kewarganegaraan){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{kewarganegaraan})
  }
}


export function setDistrictDtHlt(district_id){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{district_id})
  }
}

export function setProvinsiDtHlt(province_id){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{province_id})
  }
}

export function setFileIdentitasDtHlt(file_identitas){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{file_identitas})
  }
}

export function setTglLahirDtHlt(tgl_lahir){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{tgl_lahir})
  }
}
export function setVisitorIdDtHlt(visitor_id){
  return{
    type: 'SET_DATA_HEALTH',
    value: Object.assign({},{visitor_id})
  }
}
