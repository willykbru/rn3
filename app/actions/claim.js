import * as service from './service';
import * as appFunction from '../function';
import { Alert } from 'react-native';

// nama lengkap
export function setNamaLengkap(namaLengkap) {
  return {
    type: 'SET_NAMA_LENGKAP_STATE',
    value: Object.assign({}, { namaLengkap })
  }
}

// Email
export function setEmail(email) {
  return {
    type: 'SET_EMAIL_STATE',
    value: Object.assign({}, { email })
  }
}

// Handphone
export function setHandphone(handphone) {
  return {
    type: 'SET_HANDPHONE_STATE',
    value: Object.assign({}, { handphone })
  }
}

// Telepon
export function setTelepon(telepon) {
  return {
    type: 'SET_TELEPON_STATE',
    value: Object.assign({}, { telepon })
  }
}

// Nama Pengemudi
export function setNamaPengemudi(namaPengemudi) {
  return {
    type: 'SET_NAMA_PENGEMUDI_STATE',
    value: Object.assign({}, { namaPengemudi })
  }
}

// Nomor Polisi
export function setNomorPolisi(nomorPolisi) {
  return {
    type: 'SET_NO_POLISI_STATE',
    value: Object.assign({}, { nomorPolisi })
  }
}

// Nomor Rangka Kendaraan
export function setNomorRangka(nomorRangka) {
  return {
    type: 'SET_NOMOR_RANGKA_STATE',
    value: Object.assign({}, { nomorRangka })
  }
}

// Nomor Mesin Kendaraan
export function setNomorKendaraan(nomorKendaraan) {
  return {
    type: 'SET_NOMOR_KENDARAAN_STATE',
    value: Object.assign({}, { nomorKendaraan })
  }
}

// Nomor Polis
export function setNomorPolis(nomorPolis) {
  return {
    type: 'SET_NOMOR_POLIS_STATE',
    value: Object.assign({}, { nomorPolis })
  }
}

// Tanggal Kejadian
export function setTanggalKejadian(tanggalKejadian) {
  return {
    type: 'SET_TANGGAL_KEJADIAN_STATE',
    value: Object.assign({}, { tanggalKejadian })
  }
}

// Jam Insiden
export function setJamInsiden(jamInsiden) {
  return {
    type: 'SET_JAM_INSIDEN_STATE',
    value: Object.assign({}, { jamInsiden })
  }
}

// Provinsi Insiden
export function setProvinsiInsiden(provinsiInsiden) {
  return {
    type: 'SET_CLAIM_STATE',
    value: Object.assign({}, { provinsiInsiden })
  }
}

// Label provinsi Insiden
export function setLabelProvinsiInsiden(labelProvinsiInsiden) {
  return {
    type: 'SET_CLAIM_LABEL_STATE',
    value: Object.assign({}, { labelProvinsiInsiden })
  }
}

// Kabupaten Kota Insiden (District)
export function setKotaInsiden(kotaInsiden) {
  return {
    type: 'SET_CLAIM_STATE',
    value: Object.assign({}, { kotaInsiden })
  }
}

// Label Kota Insiden (District)
export function setLabelKotaInsiden(labelKotaInsiden) {
  return {
    type: 'SET_CLAIM_LABEL_STATE',
    value: Object.assign({}, { labelKotaInsiden })
  }
}

// Kecepatan Insiden
export function setKecepatanInsiden(kecepatanInsiden) {
  return {
    type: 'SET_CLAIM_STATE',
    value: Object.assign({}, { kecepatanInsiden })
  }
}

// Deskrippsi Insiden
export function setDeskripsiInsiden(deskripsiInsiden) {
  return {
    type: 'SET_CLAIM_STATE',
    value: Object.assign({}, { deskripsiInsiden })
  }
}

// set Data Claim MV
export function setDataClaimMV(state) {
  // console.log('setDataClaimMV', state);
  return {
    type: 'SET_DATA_CLAIM',
    value: Object.assign({}, state)
  }
}

// set data claim all MV
export function setDataClaimAllMV(state) {
  // console.log('setDataClaimAllMV', state);
  return {
    type: 'SET_DATA_CLAIM_ALL',
    value: Object.assign({}, state)
  }
}

// set data gambar foto temp
export function setDataGambarKamera(items) {
  // console.log('setDataGambarKamera', items);
  return {
    type: 'SET_DATA_GAMBAR_KAMERA',
    value: Object.assign({}, items)
  }
}

// set data gambar foto untuk diberikan caption secara langsung nantinya
export function setDataGambarKameraTemp(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

// edit data gambar foto
export function editDataGambarKamera(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'EDIT_DATA_GAMBAR_KAMERA',
    value: Object.assign({}, items)
  }
}

// simpan data gambar SIM
export function setDataGambarSimKameraTemp(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_SIM_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

// simpan data gambar KTP Pihak 3
export function setDataGambarKtpPihak3KameraTemp(items){
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_KTP_PIHAK3_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

// simpan data gambar SIM Pihak 3
export function setDataGambarSimPihak3KameraTemp(items){
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_SIM_PIHAK3_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

// simpan data gambar STNK Pihak 3
export function setDataGambarStnkPihak3KameraTemp(items){
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_STNK_PIHAK3_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

export function setDataGambarMobilKiriTemp(items){
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_MOBIL_KIRI_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

export function setDataGambarMobilKananTemp(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_MOBIL_KANAN_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

export function setDataGambarMobilDepanTemp(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_MOBIL_DEPAN_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

export function setDataGambarMobilBelakangTemp(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_MOBIL_BELAKANG_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

// hapus data gambar SIM
export function hapusDataGambarSimKameraTemp() {

  return {
    type: 'HAPUS_DATA_GAMBAR_SIM_KAMERA'
  }
}

// simpan data gambar laporan polisi
export function setDataGambarLaporanPolisiKameraTemp(items) {
  temp = [];
  temp.push(items);
  return {
    type: 'SET_DATA_GAMBAR_LAPORAN_POLISI_KAMERA_TEMP',
    value: Object.assign({}, temp)
  }
}

export function hapusDataGambarLaporanPolisiKamera() {
  // console.log('hapusDataGambarKamera', data);
  return {
    type: 'HAPUS_DATA_GAMBAR_LAPORAN_POLISI_KAMERA',
  }
}


// set data gambar foto untuk diberikan caption secara langsung nantinya
export function resetDataGambarKameraTemp() {

  let items = {
    'size': 0,
    'path': null,
    'caption': null,
  };

  return {
    type: 'SET_DATA_GAMBAR_KAMERA_TEMP',
    value: Object.assign({}, items)
  }
}

export function hapusDataGambarKamera(data) {
  // console.log('hapusDataGambarKamera', data);
  return {
    type: 'HAPUS_DATA_GAMBAR_KAMERA',
    value: data
  }
}

export function resetProvinsiInsiden() {
  return (dispatch) => {
    dispatch(setProvinsiInsiden('Pilih'));
    dispatch(setLabelProvinsiInsiden('Pilih'));
  }
}

export function resetKotaInsiden() {
  return (dispatch) => {
    dispatch(setKotaInsiden('Pilih'));
    dispatch(setLabelKotaInsiden('Pilih'));
  }
}

export function resetDataDistrict() {
  return {
    type: 'RESET_DATA_DISTRICT',
  }
}

export function getTipeInsiden() {
  return (dispatch) => {
    //.log(data);
    const request = 'TipeInsiden';
    var url = appFunction.serviceURL + 'claim/gettipekejadian';
    // console.log('getTipeInsiden',url);
    dispatch(getDataClaim(url, request));
  }
}

export function setTipeInsiden(items) {
  console.log('settipeinsiden', items)
  return {
    type: 'TIPE_INSIDEN_LIST',
    items: appFunction.toObjectTipeInsiden(items)

  }
}

export function setAllListClaim(items) {
  console.log('setAllListClaim', items)
  return {
    type: 'CLAIM_LIST_ALL',
    items

  }
}

// mengembalikan list claim yang telah selesai diproses baik reject atau approve
export function setListClaimDone(items) {
  let temp = [];
  // // console.log('claim_done', items);
  items.forEach(item => {
    // console.log(item.claim_status);
    // if (item.claim_status == 'Y' || item.claim_status == 'R') {
    //   temp.push(item);
    // }
    if (item.claim_status == 'X' || item.claim_status == 'Y' || item.claim_status == 'R'){
      temp.push(item);
    }
  });
  // console.log('claim_done_actions', temp.length);
  return {
    type: 'SET_CLAIM_LIST_DONE',
    value: temp
  }
}

export function resetListClaimDone() {

  return {
    type: 'RESET_CLAIM_LIST_DONE'
  }
}

export function setListClaimTerkini(items) {
  let temp = [];
  console.log('terkini',items);
  items.forEach(item => {
    if (item.claim_status == 'S' || item.claim_status == 'A' || item.claim_status == 'N' || item.claim_status == 'P') {
      temp.push(item);
    }
  });

  return {
    type: 'SET_CLAIM_LIST_TERKINI',
    value: temp
  }
}

export function resetListClaimTerkini(){
  return {
    type: 'RESET_CLAIM_LIST_TERKINI'
  }
}

export function getDataClaim(url, request) {
  
  return (dispatch) => {
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (!response.ok) {
        Alert.alert('Error', response.statusText);
      }
      return response;
    }).then((response) => response.json()).then((items) => {
      console.log('sukses', items, request);
      dispatch(getDataClaimSuccess(items, request));
    }).catch(() => { console.log('error getdataclaim') });
  }
}

// untuk memeriksa apakah policy_no ada klaim atau tidak
export function getIsClaim(policy_no = '') {
  var url = appFunction.serviceURL + '/claim/getclaimlist?policy_no=' + policy_no;
  var request = 'AllListClaim';
  // console.log(url);

  return (dispatch) => {
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (!response.ok) {
        Alert.alert('Error', response.statusText);
      }
      return response;
    }).then((response) => response.json()).then((items) => {
      dispatch(getDataClaimSuccess(items, request));
    }).catch(() => { console.log('error getIsClaim') });
  }
}

export function getClaimDone(policy_no = '') {
  var url = appFunction.serviceURL + '/claim/getclaimlist?policy_no=' + policy_no;
  var request = 'ListClaimDone';
  // console.log('url',url);
  
  
  return (dispatch) => {
    dispatch(setLoadDataHistoriKlaim(true));
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (!response.ok) {
        Alert.alert('Error', response.statusText);
      }
      return response;
    }).then((response) => response.json()).then((items) => {
      dispatch(setLoadDataHistoriKlaim(false));
      dispatch(getDataClaimSuccess(items, request));
    }).catch((error) => { console.log('error getIsClaim', error) });
  }
}

export function getClaimTerkini(policy_no = '') {
  var url = appFunction.serviceURL + '/claim/getclaimlist?policy_no=' + policy_no;
  var request = 'ListClaimTerkini';
  // console.log('url', url);

  return (dispatch) => {
    dispatch(setLoadDataKlaimTerkini(true));
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {
      if (!response.ok) {
        Alert.alert('Error', response.statusText);
      }
      return response;
    }).then((response) => response.json()).then((items) => {
      dispatch(setLoadDataKlaimTerkini(false));
      dispatch(getDataClaimSuccess(items, request));
    }).catch((error) => { console.log('error getIsClaim', error) });
  }
}

export function getDataClaimSuccess(items, request) {
  // console.log('getDataClaimSuccess',items, request);
  return (dispatch) => {
    if (request == 'TipeInsiden') {
      // console.log('berhasil getDataClaimSuccess', items);
      dispatch(setTipeInsiden(items));
    } else if (request == 'AllListClaim') {
      dispatch(setAllListClaim(items));
    } else if (request == 'ListClaimDone') {
      dispatch(setListClaimDone(items));
    } else if (request == 'ListClaimTerkini') {
      dispatch(setListClaimTerkini(items));
      
    }
  }
}



export function setDataPolisSemua(items) {
  return {
    type: 'SET_DATA_POLIS'.
      items,
  }
}

export function setClaimState(value, state) {
  return (dispatch) => {
    if (state == 'provinsiInsiden') {
      dispatch(setProvinsiInsiden(value.val));
      dispatch(setLabelProvinsiInsiden(value.label));
    } else if (state == 'kotaInsiden') {
      dispatch(setKotaInsiden(value.val));
      dispatch(setLabelKotaInsiden(value.label));
    }
  }
}

export function doPostDataClaimMV(body) {
  //console.log('doPostDataMV');
  return (dispatch) => {
    var url = appFunction.serviceURL + 'claim/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function setLoadDataKlaimMV(bool) {
  // console.log('setLoadDataKlaimMV', bool);
  return {
    type: 'LOADING_DATA_KLAIM_MV',
    bool
  }
}

export function setLoadDataHistoriKlaim(bool) {
  return {
    type: 'LOADING_HISTORI_KLAIM',
    bool
  }
}

export function setLoadDataKlaimTerkini(bool) {
  return {
    type: 'LOADING_KLAIM_TERKINI',
    bool
  }
}

export function fetchPostData(url, body) {
  return (dispatch) => {
    console.log('claim all',body);
    dispatch(setLoadDataKlaimMV(true));
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    }).then((response) => {
      if (!response.ok) {
        //throw Error(response.statusText);
        Alert.alert(
          'Error',
          response.statusText
        );
      }


      return response;
    })
      .then((response) => response.json())
      .then((items) => {
        // console.log(items);
        dispatch(setLoadDataKlaimMV(false));
        dispatch(getFetchPostDataSuccess(items));

      })
      .catch((error) => console.log(error));
  };
}

export function getFetchPostDataSuccess(items) {
  // console.log('getFetchPostDataSuccess', items);
  return (dispatch) => {
    dispatch(setDataTransaksiKlaimMV(items));
  }
}

export function setDataTransaksiKlaimMV(value) {
  // console.log(value);
  return {
    type: 'SET_DATA_TRANSAKSI_KLAIM_MV',
    value
  }
}

export function resetClaimMVAll() {
  // console.log('resetClaimMVAll');
  return (dispatch) => {
    dispatch(resetDataClaim());
    dispatch(resetClaimDataAll());
    // dispatch(resetDataTransaksiKlaimMV());
    dispatch(resetClaimState());
    dispatch(resetClaimLabelState());
    dispatch(resetListDataGambar());
    dispatch(resetListDataGambarTemp());
    dispatch(resetListDataGambarSim());
    dispatch(resetListDataLaporanPolisi());
  }
}

export function resetDataClaim() {
  // console.log('resetDataClaim');
  return {
    type: 'RESET_DATA_CLAIM'
  }
}

export function resetClaimDataAll() {
  return {
    type: 'RESET_DATA_CLAIM_ALL'
  }
}

export function resetDataTransaksiKlaimMV() {
  return {
    type: 'RESET_DATA_TRANSAKSI_KLAIM_MV'
  }
}

export function resetClaimState() {
  return {
    type: 'RESET_CLAIM_STATE'
  }
}

export function resetClaimLabelState() {
  return {
    type: 'RESET_CLAIM_LABEL_STATE'
  }
}

export function resetListDataGambar() {
  // console.log('RESET_DATA_GAMBAR_KAMERA');
  return {
    type: 'RESET_DATA_GAMBAR_KAMERA'
  }
}

export function resetListDataGambarTemp() {
  return {
    type: 'RESET_DATA_GAMBAR_KAMERA_TEMP'
  }
}

export function resetListDataGambarSim() {
  return {
    type: 'HAPUS_DATA_GAMBAR_SIM_KAMERA'
  }
}

export function resetListDataLaporanPolisi() {
  return {
    type: 'HAPUS_DATA_GAMBAR_LAPORAN_POLISI_KAMERA'
  }
}

export function setClaimNumber(claim_number) {
  return {
    type: 'SET_CLAIM_NUMBER',
    value: claim_number
  }
}

export function resetClaimNumber() {
  return {
    type: 'RESET_CLAIM_NUMBER',
  }
}

export function setClaimPolis(items){
  return{
    type: 'SET_CLAIM_POLIS',
    value: items
  }
}

export function resetClaimPolis(){
  return {
    type: 'RESET_CLAIM_POLIS',
  }
}


export function setLoadingClaimMenu(bool){
  return {
    type: 'SET_LOADING_CLAIM_MENU',
    bool
  }
}

export function setClaimStatusDetail(items){
  return {
    type: 'SET_CLAIM_STATUS_DETAIL',
    value: items
  }
}

export function resetClaimStatusDetail(){
  return{
    type: 'RESET_CLAIM_STATUS_DETAIL'
  }
}

export function setLinkSPK(items){
  return{
    type:'SET_LINK_SPK',
    value: items
  }
}

export function resetLinkSPK(){
  return{
    type:'RESET_LINK_SPK',  
  }
}

export function setHalaman(items){
  return{
    type: 'SET_HALAMAN',
    value: items
  }
}

export function resetHalaman(){
  return{
    type: 'RESET_HALAMAN',
  }
}

export function setLokasiSekarang(items){
  // console.log('setLokasi',items);
  return{
    type: 'SET_LOKASI_SEKARANG',
    value: items
  }
}

export function resetLokasiSekarang(){
  return{
    type: 'RESET_LOKASI_SEKARANG',
  }
}

export function setBengkelClaim(items){
  return{
    type: 'SET_BENGKEL_CLAIM',
    value: items,
  }
}

export function resetBengkelClaim(){
  return{
    type: 'RESET_BENGKEL_CLAIM',
  }
}

export function getClaimStatusDetail(claim_number){
  
  var url = appFunction.serviceURL + '/claim/getclaimstatus/' + claim_number;
  // var request = 'ListClaimTerkini';
    console.log('url', url);

    return (dispatch) => {
      // dispatch(setLoadDataKlaimTerkini(true));
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
      }).then((response) => {
        if (!response.ok) {
          Alert.alert('Error', response.statusText);
        }
        return response;
      }).then((response) => response.json()).then((items) => {
        // console.log(items);
        // dispatch(setLoadDataKlaimTerkini(false));
        // dispatch(getDataClaimSuccess(items, request));
        dispatch(setClaimStatusDetail(items));
      }).catch((error) => { console.log('error getIsClaim', error) });
    }
  
}

/* list claim all */
export function cekClaimAll(policy_no) {
  var url = appFunction.serviceURL + '/claim/getclaimlist?policy_no=' + policy_no;



  // console.log('test');
  return async (dispatch) => {
    try {
      const response = await fetch(url);

      const json = await response.json();

      var temp = {
        "policyno": policy_no,
        "list_claim": json

      }

      dispatch(setClaimPolis(temp));

      // console.log('state',this.state.listClaim);
      // this.props.setUpdateClaim(json);
      // console.log(policy_no, json);
    } catch (error) {
      console.log(error);
    }
  };
}
/* end list claim all */

export function getAllBengkelClaim(insurance_id = 'ins001'){
  var url = appFunction.serviceURL + '/vehicle/getbengkel/' + insurance_id;
  console.log('getbengkelclaim', url);
  return async (dispatch) => {
    try{
      const response = await fetch(url);

      const json = await response.json();

      // console.log('getbengkelclaim',json);
      var temp = {
        'bengkel' : json
      }
      dispatch(setListAllBengkelClaim(temp));
    } catch(error){
      console.log(error);
    }
  }
}

export function setListAllBengkelClaim(items) {
  return {
    type: 'SET_LIST_ALL_BENGKEL_CLAIM',
    value: items
  }
}