import * as dataDelay from './dataDelay';


export function setDelayState(value, state){
  return (dispatch) => {
    if(state == 'tittle'){
      dispatch(dataDelay.setTittleDtDly(value));
    }else if(state == 'perjalanan'){
      dispatch(setPerjalananDelay(value));
    }else if(state == 'travel_type'){
      dispatch(dataDelay.setTravelTypeDtDly(value));
    }else if(state == 'type'){
      dispatch(setTypeDelay(value));
    }else if(state == 'hub_ahliwaris'){
      dispatch(dataDelay.setHubAhliWarisDtDly(value));
    }else if(state == 'city_origin'){
      dispatch(dataDelay.setCityOriginDtDly(value.val))
      dispatch(setLabelCityOriginDelay(value.label))
    }else if(state == 'city_destination'){
      dispatch(dataDelay.setCityDestinationDtDly(value.val))
      dispatch(setLabelCityDestinationDelay(value.label))
    }else if(state == 'country_destination'){
      dispatch(dataDelay.setCountryDestinationDtDly(value.val))
      dispatch(setLabelCountryDestinationDelay(value.label))
    }else if(state == 'province_id'){
      dispatch(dataDelay.setProvinsiDtDly(value.val))
      dispatch(setLabelProvinsiDelay(value.label))
    }else if(state == 'district_id'){
      dispatch(dataDelay.setDistrictDtDly(value.val))
      dispatch(setLabelDistrictDelay(value.label))
    }
  }
}

export function setPackageDelay(package_id){
  return{
    type: 'SET_DELAY_STATE',
    value: Object.assign({},{package_id})
  }
}


export function setTypeDelay(type){
  return{
    type: 'SET_DELAY_STATE',
    value: Object.assign({},{type})
  }
}

export function setMaskapaiDelay(maskapai){
  return{
    type: 'SET_OT_DELAY_STATE',
    value: Object.assign({},{maskapai})
  }
}

export function setPerjalananDelay(perjalanan){
  return{
    type: 'SET_OT_DELAY_STATE',
    value: Object.assign({},{perjalanan})
  }
}

export function setLabelCityDestinationDelay(label_city_destination){
  return{
    type: 'SET_DELAY_LABEL_STATE',
    value: Object.assign({},{label_city_destination})
  }
}

export function setLabelCityOriginDelay(label_city_origin){
  return{
    type: 'SET_DELAY_LABEL_STATE',
    value: Object.assign({},{label_city_origin})
  }
}
export function setLabelCountryDestinationDelay(label_country_destination){
  return{
    type: 'SET_DELAY_LABEL_STATE',
    value: Object.assign({},{label_country_destination})
  }
}

export function setLabelProvinsiDelay(label_provinsi){
  return{
    type: 'SET_DELAY_LABEL_STATE',
    value: Object.assign({},{label_provinsi})
  }
}

export function setLabelDistrictDelay(label_district){
  return{
    type: 'SET_DELAY_LABEL_STATE',
    value: Object.assign({},{label_district})
  }
}

export function setAsuransiDelay(value){
 // console.log(value);
  return{
    type: 'SET_ASURANSI_DELAY',
    value
  }
}
