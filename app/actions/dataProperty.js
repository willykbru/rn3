
export function setTittleDP(tittle){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{tittle})
  }
}

export function setAlamatPemilikDP(alamat_pemilik){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{alamat_pemilik})
  }
}
export function setKodeVoucherDP(kode_voucher){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{kode_voucher})
  }
}

export function setNamaPemilikDP(nama_pemilik){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{nama_pemilik})
  }
}

export function setKTPDP(ktp){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{ktp})
  }
}

export function setPhotoBelakangDP(photo_belakang){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{photo_belakang})
  }
}
export function setPhotoDepanDP(photo_depan){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{photo_depan})
  }
}

export function setNamaBankDP(namabankleasing){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{namabankleasing})
  }
}

export function setPhotoKananDP(photo_kanan){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{photo_kanan})
  }
}
export function setPhotoKiriDP(photo_kiri){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{photo_kiri})
  }
}
export function setPhotoKtpDP(photo_ktp){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{photo_ktp})
  }
}

export function setTipeDP(seri_tipe){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{seri_tipe})
  }
}
export function setQQDP(statusqq){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{statusqq})
  }
}
export function setTglLahirDP(tgl_lahir){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{tgl_lahir})
  }
}
export function setVisitorIdDP(visitor_id){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{visitor_id})
  }
}
