import * as dataLife from './dataLife';


export function setLifeState(value, state){
  return (dispatch) => {
    if(state == 'pekerjaan'){
      dispatch(setPekerjaanLife(value.val));
      dispatch(setLabelPekerjaanLife(value.label));
    }else if(state == 'tittle'){
      dispatch(dataLife.setTittleDtLfe(value));
    }else if(state == 'hub_ahliwaris'){
      console.log('hub ahli')
      dispatch(dataLife.setHubAhliWarisDtLfe(value));
    }else if(state == 'province_id'){
      dispatch(dataLife.setProvinsiDtLfe(value.val))
      dispatch(setLabelProvinsiLife(value.label))
    }else if(state == 'district_id'){
      dispatch(dataLife.setDistrictDtLfe(value.val))
      dispatch(setLabelDistrictLife(value.label))
    }
  }
}

export function resetDistrictDtLfe(){
  return (dispatch)=>{
    dispatch(dataLife.setDistrictDtLfe('Pilih'));
    dispatch(setLabelDistrictLife('Pilih'));
  }
}

export function setPackageLife(package_id){
  return{
    type: 'SET_LIFE_STATE',
    value: Object.assign({},{package_id})
  }
}


export function setPekerjaanLife(pekerjaan){
  return{
    type: 'SET_LIFE_STATE',
    value: Object.assign({},{pekerjaan})
  }
}

export function setLabelPekerjaanLife(label_pekerjaan){
  return{
    type: 'SET_LIFE_LABEL_STATE',
    value: Object.assign({},{label_pekerjaan})
  }
}

export function setLabelProvinsiLife(label_provinsi){
  return{
    type: 'SET_LIFE_LABEL_STATE',
    value: Object.assign({},{label_provinsi})
  }
}

export function setLabelDistrictLife(label_district){
  return{
    type: 'SET_LIFE_LABEL_STATE',
    value: Object.assign({},{label_district})
  }
}

export function setTglLahirLife(tgl_lahir){
  return{
    type: 'SET_LIFE_STATE',
    value: Object.assign({},{tgl_lahir})
  }
}

export function setAsuransiLife(value){
 // console.log(value);
  return{
    type: 'SET_ASURANSI_LIFE',
    value
  }
}
