import { NavigationActions, StackNavigator } from 'react-navigation';

/*
export function backAction(){
  return (dispatch) => {
    dispatch(NavigationActions.back({
      key: null
    }));
  };
}*/

export function resetRoute(routeName, params) {
  return (dispatch) => {
  //  setTimeout(() => {
  //  dispatch(NavigationActions.navigate({ routeName: route, params }));
    dispatch(NavigationActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({
           routeName: routeName, params
        })
      ]
    }));
//  },
  //0)
  };
}

export function nav(routeName, params) {
  return (dispatch) => {
    dispatch(
      NavigationActions.navigate({ routeName: routeName, params })
    );
  };
}
/*
export function navigate(route, params) {
  return (dispatch) => {
  //  dispatch(NavigationActions.navigate({ routeName: route, params }));
    dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
           routeName: route, params
        })
      ]
    }));
  };
}*/
