import * as appFunction from '../function';

export function setOrderId(order_id){
  return{
    type: 'SET_RENEWAL_LABEL',
    value: Object.assign({},{order_id} )
  }
}

export function setLblPekerjaanRenewal(pekerjaan) {
  return {
    type: 'SET_RENEWAL_LABEL',
    value: Object.assign({}, { pekerjaan })
  }
} 

export function setDataRenewalSama(data_sama) {
  return {
    type: 'SET_RENEWAL_LABEL',
    value: Object.assign({}, { data_sama })
  }
} 

export function setProvinceName(province){
  return{
    type: 'SET_RENEWAL_LABEL',
    value: Object.assign({},{province})
  }
}

export function setDistrictName(district){
  return{
    type: 'SET_RENEWAL_LABEL',
    value: Object.assign({},{district})
  }
}

export function setPostDataRenewal(value){
  return{
    type: 'SET_POST_DATA_RENEWAL',
    value
  }
}

export function resetPostDataRenewal(){
  return{
    type: 'SET_POST_DATA_RENEWAL',
    value: {}
  }
}

export function resetProvinceName() {
  return (dispatch) => {
    dispatch(setProvinceName(''));
  }
}

export function resetDistrictName() {
  return (dispatch) => {
    dispatch(setDistrictName(''));
  }
}

export function resetRenewalLabel() {
  const initialLabel = {
    province: '',
    district: '',
    order_id: '',
    pekerjaan: '',
    data_sama: false,
  }
  return {
    type: 'SET_RENEWAL_LABEL',
    value: Object.assign({}, initialLabel )
  }
}


export function setPackage(package_id, lob){
    if(lob == 'PA'){
      return{
      type: 'SET_RENEWAL_ACCIDENT',
      value: Object.assign({},{package_id})
      }
    }
	}


export function setInsuranceId(insurance_id, lob){
	if(lob == 'MV'){
	  return{
		type: 'SET_RENEWAL_KENDARAAN',
		value: Object.assign({},{insurance_id})
	  }
	}
}


/* Accident */
export function setRenewalAccident(value){
  return{
    type: 'SET_RENEWAL_ACCIDENT',
    value
  }
}

export function setRenewalDataAccident(value) {
  return {
    type: 'SET_RENEWAL_DATA_ACCIDENT',
    value
  }
}

export function setPekerjaanRenewalPA(pekerjaan) {
  return {
    type: 'SET_RENEWAL_ACCIDENT',
    value: Object.assign({}, { pekerjaan })
  }
} 

export function setAlamatRenewalPA(alamat) {
  return {
    type: 'SET_RENEWAL_DATA_ACCIDENT',
    value: Object.assign({}, { alamat })
  }
} 

export function resetProvinceRenewalPA() {
  return(dispatch)=> {
    dispatch(setProvinceRenewalPA('Pilih'));
  }
} 

export function setAhliWarisRenewalPA(nama_ahliwaris) {
  return {
    type: 'SET_RENEWAL_DATA_ACCIDENT',
    value: Object.assign({}, { nama_ahliwaris })
  }
} 

export function setHubAhliWarisRenewalPA(hub_ahliwaris) {
  return {
    type: 'SET_RENEWAL_DATA_ACCIDENT',
    value: Object.assign({}, { hub_ahliwaris })
  }
}

export function setProvinceRenewalPA(province_id) {
  return {
    type: 'SET_RENEWAL_DATA_ACCIDENT',
    value: Object.assign({}, { province_id })
  }
} 


export function resetDistrictRenewalPA() {
  return (dispatch) => {
    dispatch(setDistrictRenewalPA('Pilih'));
  }
}


export function setDistrictRenewalPA(district_id) {
  return {
    type: 'SET_RENEWAL_DATA_ACCIDENT',
    value: Object.assign({}, { district_id })
  }
} 

/*
{
  "total_premi": 500000,
  "disc_insurance": 0,
  "disc_voucher": 0,
  "net_premi": 500000,
  "tsi": null,
  "disc_amount": null,
  "disc_premi": 0,
  "nama_tertanggung": "ADRIAN TEST",
  "alamat_tertanggung": "JL U",
  "email_tertanggung": "adrian.wijaya@kbru.co.id",
  "phone_tertanggung": "08121213131321",
  "no_ktp": "454646644646",
  "policy_no": "SMN-GOATRUN-180305-00000037",
  "policy_enddate": "2018-07-13",
  "no_rangka": null,
  "no_mesin": null,
  "tahun_kendaraan": null,
  "package_id": "24",
  "file_identitas": "http://dev.jagain.com/img-ac/AC201800000388-identitas.jpg",
  "merek_kendaraan": null,
  "seri_kendaraan": null,
  "tipe_kendaraan": null,
  "insurance_name": "SIMASNET",
  "no_plat": null,
  "penggunaan": null,
  "coverage": null,
  "premidasar": null,
  "premi_tpl": null,
  "premi_pll": null,
  "premi_pad": null,
  "premi_pap": null,
  "premi_srcc": null,
  "premi_ts": null,
  "premi_tsfwd": null,
  "premi_eqvet": null,
  "premi_atpm": null,
  "package_name": "limit Rp. 1,000,000,000",
  "jenis_property": null,
  "risk_address": null,
  "nama": "ADRIAN TEST",
  "statusqq": null,
  "namaqq": null
}
*/


export function makeRenewalAccident(dataPolis){
	//console.log('data polis', dataPolis);
	//console.log('data polis', dataPolis);
	const value = { 
				  package_id: dataPolis.package_id,
				  pekerjaan: dataPolis.pekerjaan,
				  tgl_lahir: dataPolis.tgl_lahir,
				}
	return (dispatch)=>{			
		dispatch(setRenewalAccident(value));
	}
}

export function resetRenewalAccident() {
  const value = {
    package_id: 0,
    pekerjaan: '',
    tgl_lahir: null,
  }
  return (dispatch) => {
    dispatch(setRenewalAccident(value));
  }
}

export function makeRenewalDataAccident(dataPolis, order_id) {
  //console.log('data polis', dataPolis);
  //console.log('data polis', dataPolis);
  const value = {
    alamat: dataPolis.alamat_tertanggung,
    district_id: dataPolis.district_id,
    file_identitas: dataPolis.file_identitas,
    gender: dataPolis.gender,
    hub_ahliwaris: dataPolis.hub_ahliwaris,
    kewarganegaraan: dataPolis.kewarganegaraan,
    kode_voucher: '',
    mobile_apps: 'Y',
    order_id: order_id,
    nama: dataPolis.nama_tertanggung,
    nama_ahliwaris: dataPolis.nama_ahliwaris,
    no_identitas: dataPolis.no_ktp,
    province_id: dataPolis.province_id,
    status_renewal: 'Y',
    tempat_lahir: dataPolis.tempat_lahir,
    tgl_lahir: dataPolis.tgl_lahir,
    tittle: dataPolis.tittle,
    visitor_id: dataPolis.visitor_id,
  }
  return (dispatch) => {
    dispatch(setRenewalDataAccident(value));
  }
}

export function resetRenewalDataAccident(){
	const value = { 	
    alamat: '',
    district_id: 'Pilih',
    file_identitas: '',
    gender: 'L',
    hub_ahliwaris: 'Pilih',
    kewarganegaraan: 'wni',
    kode_voucher: '',
    mobile_apps: 'Y',
    nama: '',
    nama_ahliwaris: '',
    no_identitas: '',
    order_id: '',
    province_id: 'Pilih',
    status_renewal: '',
    tempat_lahir: '',
    tgl_lahir: '',
    tittle: '',
    visitor_id: '',
	}
	return (dispatch)=>{			
		dispatch(setRenewalDataAccident(value));
	}
}
/* End of Accident */


/*MV*/
export function setRenewalKendaraan(value){
  return{
    type: 'SET_RENEWAL_KENDARAAN',
    value
  }
}

export function setAlamatRenewalKendaraan(alamat_pemilik) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { alamat_pemilik })
  }
} 

export function setRenewalKendaraanPhotoKtp(photo_ktp) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { photo_ktp })
  }
} 


export function setRenewalKendaraanPhotoStnk(photo_stnk) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { photo_stnk })
  }
} 

export function setRenewalKendaraanPhotoDepan(photo_depan) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { photo_depan })
  }
}

export function setRenewalKendaraanPhotoBelakang(photo_belakang) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { photo_belakang })
  }
}

export function setRenewalKendaraanPhotoKanan(photo_kanan) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { photo_kanan })
  }
}

export function setRenewalKendaraanPhotoKiri(photo_kiri) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value: Object.assign({}, { photo_kiri })
  }
} 

export function setRenewalPerluasanKendaraan(value) {
  return {
    type: 'SET_RENEWAL_PERLUASAN_KENDARAAN',
    value
  }
}

export function setRenewalDataKendaraan(value) {
  return {
    type: 'SET_RENEWAL_DATA_KENDARAAN',
    value
  }
}

export function setRenewalValuePerluasanKendaraan(value, perluasan) {
	return (dispatch)=>{			
			
		var new_value = null;
		if(perluasan == 'TPL'){
			new_value = Object.assign({},{tpl_value:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'PLL'){
			new_value = Object.assign({},{pll_value:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'PAD'){
			new_value = Object.assign({},{padriver_value:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'PAP'){
			new_value = Object.assign({},{pap_value:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}
	}
}

export function setRenewalAmountPerluasanKendaraan(value, perluasan) {
	return (dispatch)=>{			
			
		var new_value = null;
	
			new_value = Object.assign({},{pap_people:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		
	}
}

export function setRenewalStatusPerluasanKendaraan(value, perluasan) {
	return (dispatch)=>{			
			
		var new_value = null;
		if(perluasan == 'TPL'){
			new_value = Object.assign({},{tpl:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'PLL'){
			new_value = Object.assign({},{pll:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'PAD'){
			new_value = Object.assign({},{padriver:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'PAP'){
			new_value = Object.assign({},{pap:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'SRCC'){
			new_value = Object.assign({},{srcc:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'SRCC'){
			new_value = Object.assign({},{srcc:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'TS'){
			new_value = Object.assign({},{ts:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'EQVET'){
			new_value = Object.assign({},{eqvet:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'TSFWD'){
			new_value = Object.assign({},{tsfwd:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}else if(perluasan == 'ATPM'){
			new_value = Object.assign({},{atpm:value});
			dispatch(setRenewalPerluasanKendaraan(new_value))
		}
	}
}


export function makeRenewalPerluasanKendaraan(dataPolis){
	const tpl = dataPolis.premi_tpl ? true : false;
	const pll = dataPolis.premi_pll ? true : false;
	const padriver = dataPolis.premi_pad ? true : false;
	const pap = dataPolis.premi_pap ? true : false;
	const srcc = dataPolis.premi_srcc ? true : false;
	const ts = dataPolis.premi_ts ? true : false;
	const tsfwd = dataPolis.premi_tsfwd ? true : false;
	const eqvet = dataPolis.premi_eqvet ? true : false;
	const atpm = dataPolis.premi_atpm ? true : false;
	
	const tpl_si = dataPolis.tpl_si;
	const pll_si = dataPolis.pll_si;
	const pad_si = dataPolis.pad_si;
	const pap_si = dataPolis.pap_si
	const pap_seat = dataPolis.pap_seat;
	
	const value = { 
	  tpl_value: tpl_si,
	  pll_value: pll_si,
	  padriver_value: pad_si,
	  pap_value: pap_si,
	  pap_people: pap_seat,
	  tpl,
	  pll,
	  padriver,
	  pap,
	  srcc,
	  ts,
	  eqvet,
	  tsfwd,
	  atpm,
	}
	return (dispatch)=>{			
		dispatch(setRenewalPerluasanKendaraan(value));
	}
}


export function makeRenewalKendaraan(dataPolis){

	const value = { 
			vehicle_type: dataPolis.tipe_kendaraan,
			coverage: dataPolis.coverage,
      merek:  dataPolis.merek_kendaraan,
    seri_tipe: dataPolis.seri_kendaraan,
			penggunaan:dataPolis.penggunaan,
			tsi:dataPolis.tsi,
			plat:dataPolis.no_plat.substring(0,dataPolis.no_plat.indexOf(" ")),
			build_year: dataPolis.tahun_kendaraan,
			harga_aksesoris: 0,
			insurance_id: "",
			aksesoris:'',
		}
	return (dispatch)=>{			
		dispatch(setRenewalKendaraan(value));
	}
}

export function makeRenewalDataKendaraan(dataPolis, order_id) {
  
  const value = {
    alamat_pemilik: dataPolis.alamat_tertanggung,
	merek:  dataPolis.merek_kendaraan,
	seri_tipe: dataPolis.seri_kendaraan,
    nama_pemilik: dataPolis.nama,
	nama_stnk: dataPolis.namaqq,
	no_ktp: dataPolis.no_ktp,
	status_qq: dataPolis.statusqq,
	no_mesin: dataPolis.no_mesin,
	no_plat: dataPolis.no_plat,
    no_rangka: dataPolis.no_rangka,
    order_id: order_id,
	penggunaan: dataPolis.penggunaan,
    photo_ktp: "", //kosong
   // photo_ktp: dataPolis.file_identitas, //kosong
    status_renewal: 'Y',
	tgl_lahir: dataPolis.tgl_lahir, 
	tittle: dataPolis.tittle,
	visitor_id: dataPolis.visitor_id,
	warna: dataPolis.warna,
  }
  return (dispatch) => {
    dispatch(setRenewalDataKendaraan(value));
  }
}

export function resetRenewalDataKendaraan() {
  const value = {
    alamat_pemilik: '',
    kode_voucher: '',
    merek: '',
    mobile_apps: 'Y',
    nama_pemilik: '',
    nama_stnk: '',
    no_ktp: '',
    no_mesin: '',
    no_plat: '',
    no_rangka: '',
    penggunaan: '',
    photo_belakang: '',
    photo_depan: '',
    photo_kanan: '',
    photo_kiri: '',
    photo_ktp: '',
    photo_stnk: '',
    seri_tipe: '',
    status_qq: 'N',
    tgl_lahir: '',
    tittle: '',
    visitor_id: '',
    warna: ''
  }
  return (dispatch) => {
    dispatch(setRenewalDataKendaraan(value));
  }
}
/*End of MV*/



/*Property*/
export function setRenewalProperty(value){
  return{
    type: 'SET_RENEWAL_PROPERTY',
    value
  }
}

export function setAlamatRenewalProperty(alamat_pemilik) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value: Object.assign({}, { alamat_pemilik })
  }
} 

export function setRenewalPropertyPhotoKtp(photo_ktp) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value: Object.assign({}, { photo_ktp })
  }
} 

export function setRenewalPropertyPhotoDepan(photo_depan) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value: Object.assign({}, { photo_depan })
  }
}

export function setRenewalPropertyPhotoBelakang(photo_belakang) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value: Object.assign({}, { photo_belakang })
  }
}

export function setRenewalPropertyPhotoKanan(photo_kanan) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value: Object.assign({}, { photo_kanan })
  }
}

export function setRenewalPropertyPhotoKiri(photo_kiri) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value: Object.assign({}, { photo_kiri })
  }
} 

export function setRenewalDataProperty(value) {
  return {
    type: 'SET_RENEWAL_DATA_PROPERTY',
    value
  }
}

export function setRenewalStatusPerluasanProperty(value, perluasan) {
	return (dispatch)=>{			
			
		var new_value = null;
		if(perluasan == 'EQVET'){
			new_value = Object.assign({},{eqvet:value});
			dispatch(setRenewalProperty(new_value))
		}else if(perluasan == 'FLOOD'){
			new_value = Object.assign({},{flood:value});
			dispatch(setRenewalProperty(new_value))
		}
	}
}



export function makeRenewalProperty(dataPolis){
	const flood = dataPolis.premi_flood ? true : false;
	const eqvet = dataPolis.premi_eqvet ? true : false;
	const value = { 
			  building_tsi: dataPolis.building_tsi,
			  content_tsi: dataPolis.content_tsi,
			  district_id: dataPolis.district_id,
			  eqvet: eqvet,
			  flood: flood,
			  kecamatan_id: dataPolis.kecamatan_id,
			  kelurahan_id: dataPolis.kelurahan_id,
			  lantai: dataPolis.lantai,
			  package_id: dataPolis.package_id,
			  province_id: dataPolis.province_id,
			  tipe: dataPolis.jenis_property,
		}
	return (dispatch)=>{			
		dispatch(setRenewalProperty(value));
	}
}

export function makeRenewalDataProperty(dataPolis, order_id) {
  
  const value = {
   alamat_pemilik: dataPolis.alamat_pemilik,
  kode_voucher: dataPolis.kode_voucher,
  mobile_apps: 'Y',
  nama_pemilik: dataPolis.nama_pemilik,
  building_content: dataPolis.building_content,
  namabankleasing: dataPolis.namabankleasing,
  ktp: dataPolis.no_ktp,
  photo_belakang: '',
  photo_depan: '',
  photo_kanan: '',
  photo_kiri: '',
  photo_ktp: '',
  statusqq: dataPolis.statusqq,
  risk_address: dataPolis.risk_address,
  tgl_lahir: dataPolis.tgl_lahir,
  tittle: dataPolis.tittle,
  visitor_id: dataPolis.visitor_id,
  }
  return (dispatch) => {
    dispatch(setRenewalDataProperty(value));
  }
}



/*End of Property*/
