import { Alert, AsyncStorage, Platform, Linking } from 'react-native';
import * as appFunction from '../function';
import * as appNavigation from './navigation';
import RNAmplitute from 'react-native-amplitude-analytics';
import DialogAndroid from 'react-native-dialogs';
import * as lobListActions from './lobList'

//set status dan fetch data
export function fetchDataSuccess(items, request) {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
  return (dispatch) => {
    if(request == 'doLogin' || request == 'doLoginAsuransi'){
      if(items.description === ''){
        if(request == 'doLogin'){
          dispatch(checkSession(items.token));
        }
        else{
          const req = 'setSessionAsuransi';
          dispatch(checkSession(items.token, req));
          //dispatch(getTokenLocalStorage())
          //dispatch(checkSession(items.token));
        }
      }else{
		amplitude.logEvent(items.description);
        appFunction.toastError(items.description);
      }
    }else if(request == 'doRegisterLogin' || request == 'doRegisterAsuransi'){
      if(items.description === ''){
      //  console.log('test regist', items)
          
		  if(request == 'doRegisterAsuransi')
			  dispatch(setSessionRegisterLogin(items, request));
		  else
			  dispatch(setSessionRegisterLogin(items));
          //dispatch(getTokenLocalStorage())
          //dispatch(checkSession(items.token));
      }else{
		amplitude.logEvent(items.description);
        appFunction.toastError(items.description);
      }
    }else if(request == 'sendVerify'){
      if(items.flag == 'success'){
		amplitude.logEvent('Kode Verifikasi Sudah dikirim ke Email');
        //appFunction.toastError('Kode Verifikasi Sudah dikirim ke Email');
        dispatch(setStatusVerify(items))
      }else{
		amplitude.logEvent('Kode Verifikasi Gagal dikirim, Pastikan Email Terdaftar dan Coba Kembali');
        appFunction.toastError('Kode Verifikasi Gagal dikirim, Pastikan Email Terdaftar dan Coba Kembali');
        dispatch(resetStatusVerify())
      }
    }else if(request == 'sendChangeForgotPassword'){
      if(items.flag == 'success'){
		amplitude.logEvent('Password berhasil diubah');
        //appFunction.toastError('Kode Verifikasi Sudah dikirim ke Email');
        //console.log('sendfp1',items);
        appFunction.toastError('Password berhasil diubah');
        dispatch(setStatusChangeFP(items))
      }else{
		amplitude.logEvent('Kode Verifikasi Lupa Password Tidak Sesuai');
      //  console.log('sendfp2',items);
        appFunction.toastError('Kode verifikasi tidak sesuai');
        dispatch(resetStatusChangeFP())
      }
    }else if(request == 'sendChangePassword'){
      if(items.description == 'success'){
		amplitude.logEvent('Password Berhasil diubah');
        //appFunction.toastError('Kode Verifikasi Sudah dikirim ke Email');
        //console.log('sendfp1',items);
        appFunction.toastError('Password Berhasil diubah');
        dispatch(setStatusChangePassword(items))
      }else{
		amplitude.logEvent('Password Lama / Kode Verifikasi Ubah Password Tidak Sesuai');
      //console.log('sendcp2',items);
        appFunction.toastError('Password Lama / Kode Verifikasi Tidak Sesuai');
        dispatch(resetStatusChangePassword())
      }
    }else if(request == 'doRegister'){
      if(items.description == 'Menunggu Konfirmasi Email'){
		amplitude.logEvent('Menunggu Konfirmasi Email Registrasi');
        dispatch(navToVerification());
        dispatch(setRegister(items.auth));
        //console.log(items.auth);
        //console.log(appFunction.deviceName());
      }else{
		amplitude.logEvent(items.description);
        appFunction.toastError(items.description);
      }
    }
  }
}

export function fetchActivateSuccess(items, auth, request) {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
  return (dispatch) => {
	if(request == 'emailActivation'){
		if(items.description == 'menunggu konfirmasi email'){
		    amplitude.logEvent('Menunggu Konfirmasi Email User');
			dispatch(setActivation(auth));
			dispatch(navToActivateAccount());
		}else{
		    amplitude.logEvent(items.description);
			appFunction.toastError(items.description);
		}
	}else if(request !== 'reactivate'){
      if(items.description == 'success'){
        //appFunction.toastError('Kode Verifikasi Sudah Dikirim Ke Email');
		if(request == 'activateAsuransi')
			dispatch(doRegisterAsuransi(auth));
		else if(auth.password){
			dispatch(doRegisterLogin(auth));
		}
		else{
		     amplitude.logEvent('Aktivasi Akun Sukses');
			  appFunction.toastError('Aktivasi Akun Sukses Silakan Login');
		}
        //dispatch(setActivateStatus(items));
      }else{
	     amplitude.logEvent('Kode verifikasi tidak sesuai');
        appFunction.toastError('Kode verifikasi tidak sesuai');
		
        //dispatch(resetStatusActivate())
      }
    }else{
	    amplitude.logEvent(items.description);
      appFunction.toastError(items.description);
    }
  }
}

export function fetchSessionSuccess(items, request, token = '') {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
  return (dispatch) => {
   if(request == 'setSession' || request == 'setSessionAsuransi'){
      if(items.username !== ''){
        dispatch(setTokenLocalStorage(token, items, request));

		    amplitude.logEvent('User Login', {username: items.username});
        amplitude.setUserId(items.visitor_id + ' ('+ items.username +')');
        //firebase
        appFunction.setFAUserProperty('visitor_id', items.visitor_id);
        appFunction.setFAUserProperty('visitor_name', items.visitor_name);
        appFunction.setFAUserProperty('visitor_phone', items.visitor_phone);
        appFunction.setFAUserProperty('username', items.username);
        appFunction.setFAUserProperty('login_time', items.login_time);
        appFunction.setFAUserProperty('last_access', items.last_access);
		   //onesignal
		    appFunction.setEmailToken(items.username, token);
		  
		 
      }else{
        //console.log('3', items);
		  amplitude.logEvent('Login User Gagal');
          
          dispatch(setTokenLocalStorage());
     
      }
    }else if(request == 'setRegisterLoginSession' || request == 'setRegisterLoginAsuransiSession'){
      if(items.username !== ''){

          dispatch(setTokenLocalStorage(token, items, request));
		    amplitude.logEvent('User Login', {username: items.username});
        amplitude.setUserId(items.visitor_id + ' ('+ items.username +')');
        //firebase
        appFunction.setFAUserProperty('visitor_id', items.visitor_id);
        appFunction.setFAUserProperty('visitor_name', items.visitor_name);
        appFunction.setFAUserProperty('visitor_phone', items.visitor_phone);
        appFunction.setFAUserProperty('username', items.username);
        appFunction.setFAUserProperty('login_time', items.login_time);
        appFunction.setFAUserProperty('last_access', items.last_access);
		  //onesignal
		   appFunction.setEmailToken(items.username, token);
		 
      }else{
        //console.log('3', items);

		    amplitude.logEvent('Login User Baru Gagal');
		    appFunction.toastError('Login Gagal Silakan Coba Login Ulang!');
    
          dispatch(setTokenLocalStorage());
         
		    dispatch(navigate('Main'));
      }
    }else if(request == 'sessionDestroy'){
        appFunction.toastError(items.description);
     appFunction.logoutEmailOneSignal();
        //console.log(items);	  
		    amplitude.logEvent('User Logout');
        dispatch(setTokenLocalStorage());
    }
  }
}


export function fetchSendVerifySuccess(items, body, request){
  return (dispatch) =>{
    const newItems  = Object.assign({},{flag: items.description}, body);
    dispatch(fetchDataSuccess(newItems, request))
  }
}

export function fetchChangeFPSuccess(items, request){
  return (dispatch) =>{
    const newItems  = Object.assign({},{flag: items.description});
    //console.log('fetchfpstatus', newItems);
    dispatch(fetchDataSuccess(newItems, request))
  }
}

export function fetchRegisterSuccess(items, body, request){
  return (dispatch) =>{
    const newItems  = Object.assign({},items, {auth: body});
    //console.log('fetchrgstatus', newItems);
    dispatch(fetchDataSuccess(newItems, request))
  }
}

//New Login Process
export function setDataLogin(items) {
  return {
    type: 'SET_DATA_LOGIN',
    value: Object.assign({}, items),
  };
}

/* fetch register */

//New Register Process
export function setDataRegister(items) {
    return {
        type: 'SET_DATA_REGISTER',
        value: Object.assign({},items),
    };
} 

export function setTokenRegister(token) {
	console.log('token',token);
    return {
        type: 'SET_DATA_REGISTER',
        value: Object.assign({},{token}),
    };
}

export function doCheckEmail(data, req = '') {
  return (dispatch) => {
      var request = 'doCheckEmail';
      const body = data;
      //console.log(body);
	  if(req == 'doRegisterLogin'){
		request = 'doRegisterLogin';
		}
	  else if(req == 'doRegisterAsuransi'){
		request = 'doRegisterAsuransi'

		}
		console.log('testtrequest', request)
      var url = appFunction.serviceURL + 'register/';
      dispatch(fetchRegister(url, body, request))
  }
}

//fetch data
export function fetchRegister(url, body, request='') {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
    return (dispatch) => {
        dispatch(itemsIsLoading(true));
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
          }).then((response) => {
		  
                if (response.status !== 200) {
                  DialogAndroid.dismiss();
                    //throw Error(response.statusText);
					    amplitude.logEvent('Error '+request, {response: "Gagal Melakukan Registrasi, Tunggu beberapa saat."});
                    
                }
                dispatch(itemsIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => {
              
              if(request == 'doCheckEmail'){
				dispatch(fetchCheckEmailSuccess(items, body));
			  }else{
				dispatch(fetchNewRegisterSuccess(items, body, request));
			  }
             
            })
          .catch(() => { dispatch(itemsHasErrored(true)); DialogAndroid.dismiss(); appFunction.toastError('Gagal Melakukan Registrasi, Tunggu beberapa saat.');});
    };
}

export function fetchCheckEmailSuccess(items, body){
	
		//console.log('testtrwegister', items)
	if(items.description == "Email Sudah Terdaftar"){
    //appFunction.toastError(items.description);
    Alert.alert('Gagal Melakukan Registrasi', items.description +', Silakan Menggunakan Email Lain.'); 
	}else{
		return(dispatch)=>{
		//console.log('testtrwegister', items)
			dispatch(setDataRegister(body));
		}
	}
}


export function fetchNewRegisterSuccess(items, body, request){
	return(dispatch) =>{
		if(request == 'doRegisterLogin' || request == 'doRegisterAsuransi'){
			//console.log(request);
			//console.log('description1', items.description);
			  if(items.description === 'Registrasi Berhasil'){
				
					//console.log('description2', items.description);
					
          const data = Object.assign({}, { device: appFunction.deviceName() ,password: body.password, username: body.username});
					//const data = Object.assign({},{device: "",password: '8277e0910d750195b448797616e091ad', username: 'dian.bojezff@gmail.com'});
				
					if(request == 'doRegisterLogin'){
						dispatch(doLogin(data));
						//console.log('doRegisterLogin',data)
					}
					else if(request == 'doRegisterAsuransi'){
						dispatch(doLogin(data, 'loginModulAsuransi'));
						//console.log('doRegisterAsuransi',data)
					}
					 
				}else if(items.description === 'Registrasi Gagal'){
					amplitude.logEvent(items.description);
          appFunction.toastError(items.description);
          
				}
				
		}
	}
}

/* end fetch register */

//set data
export function setStatusPermissionStorage(bool) {
    return {
        type: 'SET_STATUS_PERM_STORAGE',
        status: bool
    };
}

export function setStatusNotifCart(bool) {
    return {
        type: 'SET_STATUS_NOTIF_CART',
        status: bool
    };
}

export function setOID(items) {
    return {
        type: 'SET_OID',
        items
    };
}

export function itemsHasErrored(bool) {
    return {
        type: 'ITEMS_HAS_ERRORED',
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type: 'ITEMS_IS_LOADING',
        isLoading: bool
    };
}

export function setToken(token){
  return {
      type: 'SET_DATA_TOKEN',
      token
  };
}

export function setSession(session){
  return {
      type: 'SET_DATA_SESSION',
      session
  };
}

export function setMainScreen(screen){
  return {
      type: 'SET_MAIN_SCREEN',
      screen
  };
}

export function setFPScreen(screen){
  return {
      type: 'SET_FP_SCREEN',
      screen
  };
}

export function setCPScreen(screen){
  return {
      type: 'SET_CP_SCREEN',
      screen
  };
}

export function setACTScreen(screen){
  return {
      type: 'SET_ACT_SCREEN',
      screen
  };
}

export function setRenewalScreen(screen){
  return {
      type: 'SET_RENEWAL_SCREEN',
      screen
  };
}

export function setRegisterScreen(screen){
  return {
      type: 'SET_REGISTER_SCREEN',
      screen
  };
}

export function setAsuransiScreen(screen){
  return {
      type: 'SET_ASURANSI_SCREEN',
      screen
  };
}

export function setStatusVerify(status) {
    return {
        type: 'SET_STATUS_VERIFY',
        status
    };
}

export function setStatusRegister(status) {
    return {
        type: 'SET_STATUS_REGISTER',
        status
    };
}

export function setStatusActivation(status) {
    return {
        type: 'SET_STATUS_ACTIVATION',
        status
    };
}

export function setStatusChangeFP(status) {
    return {
        type: 'SET_STATUS_CHANGE_FP',
        status
    };
}

export function setStatusChangePassword(status) {
    return {
        type: 'SET_STATUS_CHANGE_PASSWORD',
        status
    };
}

export function setStatusLogin(status) {
    return {
        type: 'SET_STATUS_LOGIN',
        status
    };
}

export function setStatusNotifRenewal(status) {
    return {
        type: 'SET_STATUS_NOTIF_RENEWAL',
        status
    };
}

export function setStatusNotifContent(value) {
  return {
    type: 'SET_NOTIFICATION_CONTENT',
    value
  };
}

export function setStatusPopupEvent(status) {
    return {
        type: 'SET_STATUS_POPUP_EVENT',
        status
    };
}


export function setModalUpdateStatus(status) {
    return {
        type: 'SET_MODAL_UPDATE_STATUS',
        status
    };
}

//timeout
export function errorAfterFiveSeconds() {
    // We return a function instead of an action object
    return (dispatch) => {
        setTimeout(() => {
            // This function is able to dispatch other action creators
            dispatch(itemsHasErrored(true));
        }, 5000);
    };
}


//local storage
export function setTokenLocalStorage( token = '', items = '', request= '' ){
  console.log('setTokenLocalStorage', request)
  DialogAndroid.dismiss();
  return async dispatch =>{
    var dataLogin = {
      token: token,
      login_time: null,
      last_access: null,
      username: null,
      visitor_id: null,
      visitor_name: null,
      visitor_phone: null,
    };
    if(!appFunction.emptyObject(items) && items){
      dispatch(setToken(token));
      dispatch(setSession(items));
      dispatch(setLogin(true));
      if (request == 'setSessionAsuransi') {
        dispatch(navigate('DetailBeli'));
      } else if (request == 'setSession') {
        dispatch(setScreen('Home'));
        dispatch(navigate('Main'));
      } else if (request == 'setRegisterLoginSession') {
        dispatch(setScreen('Home'));
        dispatch(navigate('Main'));
      } else if (request == 'setRegisterLoginAsuransiSession') {
        dispatch(navToDetailBeli());
      } else if (request == 'setHome' || !request) {
        console.log('Home');
        dispatch(setScreen('Home'));
      } else if (request == 'setMyPolis') {
        dispatch(setScreen('MyPolis'));
      } else {
        dispatch(setScreen('Home'));
      }

      dataLogin = {
        token: token,
        login_time: items.login_time, 
        last_access: items.last_access,
        username: items.username,
        visitor_id: items.visitor_id,
        visitor_name: items.visitor_name,
        visitor_phone: items.visitor_phone,
      }
    }else{
      dispatch(resetSession());
      dispatch(setLogin(false));
    }

    //console.log('settokenlocal',dataLogin);
    await AsyncStorage.mergeItem('STORAGE', JSON.stringify(dataLogin));
    

  }
}

export function getTokenLocalStorage(req = '') {
  console.log('ini tokennya 1');
  return async (dispatch) =>{
    const result = await AsyncStorage.getItem('STORAGE');
    
    
    
    console.log(result);
    const request = 'setToken';
    var token = '';
    var items = {token:'', description: ''};
    if (result !== null){
      // We have data!!
      const value = JSON.parse(result);
      token = value.token ? value.token : '';
      console.log('ini tokennya 3', value.token);
      //items = value.token ? Object.assign({},{ description: '', token: value.token })  : items;
      if(token){
        if (value.username){
          var setStatus = 'setHome';
          if(req){
            setStatus = 'setMyPolis';
            console.log(setStatus);
          }
          dispatch(setTokenLocalStorage(token, value, setStatus));  
        }else{
          dispatch(checkSession(token));
        }
        console.log('tokenlogin', value);
      }else{
        dispatch(resetSession());
        dispatch(setLogin(false));
        dispatch(setTokenLocalStorage());
        
        //console.log('ini tokennya', token);
      }
      //dispatch(setSessionLogin(items, req));
    }else{
      dispatch(resetSession());
      dispatch(setLogin(false));
      dispatch(setTokenLocalStorage());
     console.log('ini tokennya 2', token);
    }
   // console.log('ini tokennya 4', items);
  }
}

//function


export function setScreen(screen, type = 'main') {
  return (dispatch) =>{
    if(type == 'main'){
      dispatch(setMainScreen(screen));
    }else if(type == 'fp'){
      dispatch(setFPScreen(screen));
    }else if(type == 'register'){
      dispatch(setRegisterScreen(screen));
    }else if(type == 'cariasuransi'){
      dispatch(setAsuransiScreen(screen));
    }else if(type == 'cp'){
      dispatch(setCPScreen(screen));
    }else if(type == 'act'){
      dispatch(setACTScreen(screen));
    }else if(type == 'renewal'){
      dispatch(setRenewalScreen(screen));
    }
  }
}

export function setSessionLogin(items, req=''){
  return (dispatch) => {
    dispatch(checkSession(items.token, req));
  }
}

export function setSessionRegisterLogin(items, request = ''){
  return (dispatch) => {
	if(request == 'doRegisterAsuransi') 
		dispatch(checkRegisterLoginAsuransiSession(items.token));
	else
		dispatch(checkRegisterLoginSession(items.token));
	
  }
}

export function setLogin(status){
  return (dispatch) => {
    dispatch(setStatusLogin(status));
  }
}

export function setRegister(auth){
  return (dispatch) => {
    const newStatus = Object.assign({},{username: auth.username, password: auth.password, device: Platform.OS});
    dispatch(setStatusRegister(newStatus));
  }
}

export function setActivation(auth){
  return (dispatch) => {
    const newStatus = Object.assign({},{username: auth.username});
    dispatch(setStatusActivation(newStatus));
  }
}

export function setActivateStatus(items){
  return (dispatch) => {
    const newStatus = Object.assign({},{flag: items.description});
    dispatch(setStatusRegister(newStatus));
  }
}

export function navigate(screen){
  return (dispatch) => {dispatch(appNavigation.resetRoute(screen))};
}

export function checkSession(token, req = ''){
	
  var request = 'setSession';
  if(req)
	  request = req;
  var url = appFunction.serviceURL + 'session/';
  return (dispatch) => {dispatch(fetchSession(url, token, request))}
}

export function checkRegisterLoginSession(token){
  const request = 'setRegisterLoginSession';
  var url = appFunction.serviceURL + 'session/';
  return (dispatch) => {dispatch(fetchSession(url, token, request))}
}

export function checkRegisterLoginAsuransiSession(token){
  const request = 'setRegisterLoginAsuransiSession';
  var url = appFunction.serviceURL + 'session/';
  return (dispatch) => {dispatch(fetchSession(url, token, request))}
}

export function sendVerify(username){
  const request = 'sendVerify';
  //console.log('sendverify',username);
  var url = appFunction.serviceURL + 'sendverify/';
  var body = {
    username: username,
  };
  return (dispatch) => {dispatch(fetchData(url, body, request))}
}

export function sendChangeForgotPassword(body){
  const request = 'sendChangeForgotPassword';
  //console.log('sendChangeForgotPassword',body);
  var url = appFunction.serviceURL + 'forgotpassword/';
  return (dispatch) => {dispatch(fetchData(url, body, request))}
}

export function sendChangePassword(body){
  const request = 'sendChangePassword';
  //console.log('sendChangePassword',body);
  var url = appFunction.serviceURL + 'changepassword/';
  return (dispatch) => {dispatch(fetchData(url, body, request))}
}


export function doRegister(data) {
  return (dispatch) => {
      var request = 'doRegister';
      const body = data;
      //console.log(body);
      var url = appFunction.serviceURL + 'register/';
      dispatch(fetchData(url, body, request))
  }
}

export function doRegisterAsuransi(data) {
  return (dispatch) => {
      const request = 'doRegisterAsuransi';
      const body = data;
      //console.log(body);
      var url = appFunction.serviceURL + 'login/';
      dispatch(fetchData(url, body, request))
  }
}

export function doRegisterLogin(data) {
  return (dispatch) => {
    const request = 'doRegisterLogin';
    const body = data;
    //console.log(data);
    var url = appFunction.serviceURL + 'login/';
    dispatch(fetchData(url, body, request))
  }
}

export function doLogin(data, asuransiScreen) {
  return (dispatch) => {
    var request = 'doLogin';
	if(asuransiScreen){
		request = 'doLoginAsuransi';
	}
    const body = data;
    console.log(data);
    var url = appFunction.serviceURL + 'login/';
    dispatch(fetchData(url, body, request))
  }
}

export function doActivate(data, req = '') {
  return (dispatch) => {
    const auth = data.auth;
	
    var request = req;
	
	if(req !== 'activateAsuransi'){
		request = 'activate';
	}
	
    var url = appFunction.serviceURL + 'verify/' + auth.username + '/' + data.verify_code;
    dispatch(fetchActivate(url, auth, request))
  }
}

export function doReactivate(auth) {
  return (dispatch) => {
    //console.log(auth);
    const request = 'reactivate';
    var url = appFunction.serviceURL + 'active/' + auth.username + '/';
    dispatch(fetchActivate(url, auth, request))
  }
}

export function doEmailActivation(auth) {
  return (dispatch) => {
    //console.log(auth);
    const request = 'emailActivation';
    var url = appFunction.serviceURL + 'active/' + auth.username + '/';
    dispatch(fetchActivate(url, auth, request))
  }
}

export function doLogout(token){
  const request = 'sessionDestroy';
  console.log('dologout',token);
  var url = appFunction.serviceURL + 'logout';
  return (dispatch) => {dispatch(fetchSession(url, token, request))}
}

export function resetSession(){
  const initialDataLogin = {
    username: null,
    visitor_name: null,
    visitor_phone: null,
    token: null,
    login_time: null,
    last_access: null,
  }
  return (dispatch) => {
    dispatch(setSession(initialDataLogin));
    dispatch(setToken(''));
  }
}

export function resetStatusVerify(){
  return (dispatch) => {
    const initialVerify = {username:'', flag:''};
    dispatch(setStatusVerify(initialVerify));
  }
}

export function resetStatusChangeFP(){
  return (dispatch) => {
    const initialStatusFP = {flag:''};
    dispatch(setStatusChangeFP(initialStatusFP));
  }
}

export function resetStatusChangePassword(){
  return (dispatch) => {
    const initialStatusCP = {flag:''};
    dispatch(setStatusChangePassword(initialStatusCP));
  }
}

export function resetStatusRegister(){
  return (dispatch) => {
    const initialStatusRegister = {username:'', password:'', device: '', flag:''};
    dispatch(setStatusRegister(initialStatusRegister));
  }
}

export function resetStatusActivation(){
  return (dispatch) => {
    const initialStatusActivation = {username:''};
    dispatch(setStatusActivation(initialStatusActivation));
  }
}

export function resetStatusActivate(){
  return (dispatch) => {
    const initialStatusActivate = {flag:''};
    dispatch(setStatusRegister(initialStatusActivate));
  }
}

export function resetMainScreen(){
  return (dispatch) => {
    const initialFPScreen = 'Home';
    dispatch(setFPScreen(initialFPScreen));
  }
}

export function resetCPScreen(){
  return (dispatch) => {
    const initialCPScreen = 'ChangeMyPassword';
    dispatch(setCPScreen(initialCPScreen));
  }
}

export function resetACTScreen(){
  return (dispatch) => {
    const initialACTScreen = 'EmailActivation';
    dispatch(setACTScreen(initialACTScreen));
  }
}


export function resetFPScreen(){
  return (dispatch) => {
    const initialFPScreen = 'EmailForgotPassword';
    dispatch(setFPScreen(initialFPScreen));
  }
}

export function resetRegisterScreen(){
  return (dispatch) => {
    const initialRegisterScreen = 'Register';
    dispatch(setRegisterScreen(initialRegisterScreen));
  }
}

//navigation
export function navToForgotPassword(){
  return (dispatch) => {
    const navTo = 'ForgotPassword';
    dispatch(resetFPScreen());
    dispatch(resetStatusVerify());
    dispatch(resetStatusChangeFP());
    dispatch(navigate(navTo));
  }
}

//navigation
export function navToChangePassword(){
  return (dispatch) => {
    const navTo = 'ChangePassword';
    dispatch(resetCPScreen());
    dispatch(resetStatusVerify());
    dispatch(resetStatusChangePassword());
    dispatch(navigate(navTo));
  }
}

//navigation
export function navToActivation(){
  return (dispatch) => {
    const navTo = 'Activation';
	dispatch(resetStatusActivation());
    dispatch(resetACTScreen());
    dispatch(navigate(navTo));
  }
}

export function navToActivateAccount(){
  return (dispatch) => {
    const nscreen = 'ActivateAccount';
    const type = 'act';
	dispatch(setScreen(nscreen, type));
  }
}

export function navToRegister(){
  return (dispatch) => {
    const navTo = 'Register';
    dispatch(resetStatusRegister());
    dispatch(navigate(navTo));
  }
}

export function navToVerification(){
  return (dispatch) => {
    const navTo = 'Verification';
    //dispatch(resetStatusRegister());
    dispatch(navigate(navTo));
  }
}

export function navToDetailBeli(){
  return (dispatch) => {
    const navTo = 'DetailBeli';
    //dispatch(resetStatusRegister());
    dispatch(navigate(navTo));
  }
}




//fetch data
export function fetchData(url, body, request = '') {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
    return (dispatch) => {
        dispatch(itemsIsLoading(true));
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
          }).then((response) => {
                if (!response.ok) {
                    //throw Error(response.statusText);
					amplitude.logEvent('Error '+request, {response: response.statusText});
                    Alert.alert(
                      'Error',
                        response.statusText
                    );
                }
                dispatch(itemsIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => {
              if(request == 'sendVerify'){dispatch(fetchSendVerifySuccess(items, body, request))}
              else if(request == 'sendChangeForgotPassword'){dispatch(fetchChangeFPSuccess(items, request))}
              else if(request == 'doRegister'){dispatch(fetchRegisterSuccess(items, body, request))}
              else{dispatch(fetchDataSuccess(items, request))}
            })
          .catch(() => { dispatch(itemsHasErrored(true)); appFunction.toastError('Gagal login, koneksi internet ke server tidak stabil');});
    };
}

export function checkUpdateVersion(version){
	return (dispatch)=>{
		const url = appFunction.serviceURL + '/version';
		dispatch(checkVersionUpdate(url, version));
	}
}

export function fetchVersionSuccess(items){
	return (dispatch) => {
		dispatch(setModalUpdateStatus(!items.status));
	}
}

//check version
export function checkVersionUpdate(url, body) {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
    return (dispatch) => {
        dispatch(itemsIsLoading(true));
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({version: body})
          }).then((response) => {
                if (!response.ok) {
                    //throw Error(response.statusText);
					amplitude.logEvent('Error '+request, {response: response.statusText});
                    Alert.alert(
                      'Error',
                        response.statusText
                    );
                }
                dispatch(itemsIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => { 
              dispatch(fetchVersionSuccess(items))
            })
            .catch(() => dispatch(itemsHasErrored(true)));
    };
}

export function fetchActivate(url, auth, request = '') {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
    return (dispatch) => {
        dispatch(itemsIsLoading(true));
        fetch(url, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          }).then((response) => {
                if (!response.ok) {
                    //throw Error(response.statusText);
					amplitude.logEvent('Error '+request, {response: response.statusText});
                    Alert.alert(
                      'Error',
                        response.statusText
                    );
                }
                dispatch(itemsIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => {
              dispatch(fetchActivateSuccess(items, auth, request))
            })
            .catch(() => dispatch(itemsHasErrored(true)));
    };
}

export function fetchSession(url, token, request = '') {
  const amplitude = new RNAmplitute('da45877d8248144134d1bae55b1b42b3');
    return (dispatch) => {
        dispatch(itemsIsLoading(true));
      DialogAndroid.showProgress('Loading');
        fetch(url, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-auth-token': token,
          },
          }).then((response) => {
                if (!response.ok) {
                    //throw Error(response.statusText);
					amplitude.logEvent('Error '+request, {response: response.statusText});
                    Alert.alert(
                      'Error',
                        response.statusText
                    );
                }
                dispatch(itemsIsLoading(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(fetchSessionSuccess(items, request, token)))
          .catch(() => { dispatch(itemsHasErrored(true)); appFunction.toastError('Gagal login, Koneksi internet ke server tidak stabil'); DialogAndroid.dismiss();});
    };
}


/* popup screen */
export function setPopup(items){
  return {
      type: 'ITEMS_FETCH_POPUP_SUCCESS',
      items
  };
}

export function fetchPopup(url){
  return (dispatch) => {
    fetch(url , {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {

      if(!response.ok){
        Alert.alert('Error', response.statusText);
      }
      dispatch(itemsIsLoading(false));
      return response;
    })
    .then((response) => response.json())
    .then((items) => {
      dispatch(setPopup(items));
    } )
    .catch(() => dispatch(itemsHasErrored(true)) )
  }
}

/* end popup screen */
/* fetch server date */
export function setServerDate(status){
  return {
      type: 'FETCH_SERVER_DATE',
      status
  };
}

export function getServerDate(){
  return (dispatch) => {
    fetch(appFunction.serviceURL + 'getdate/' , {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {

      if(!response.ok){
        Alert.alert('Error', response.statusText);
      }
      dispatch(itemsIsLoading(false));
      return response;
    })
    .then((response) => response.json())
    .then((items) => {
      dispatch(setServerDate(items.tanggal));
    } )
    .catch(() => dispatch(itemsHasErrored(true)) )
  }
}

/* fetch server date */

/* fetch slider */
export function setSlider(value){
  return {
      type: 'FETCH_SLIDER',
      value: appFunction.toArrSlider(value)
  };
}

export function getSlider(){
  return (dispatch) => {
    fetch(appFunction.serviceURL + 'getslider/' , {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    }).then((response) => {

      if(!response.ok){
        Alert.alert('Error', response.statusText);
      }
      dispatch(itemsIsLoading(false));
      return response;
    })
    .then((response) => response.json())
    .then((items) => {
      dispatch(setSlider(items));
    } )
    .catch(() => dispatch(itemsHasErrored(true)) )
  }
}

/* fetch slider */

/* count Pending Cart */

export function setTotalPendingCart(items) {
	// console.log('setTotalPendingCart',parseInt(items.length))
    return {
        type: 'SET_TOTAL_PENDING_CART',
        value: parseInt(items.length)
    };
}


export function countPendingCart(username){
  return (dispatch) => {
    console.log(username);
    const request = 'countPendingCart';
    var url = appFunction.serviceURL + 'transaction/'+ username +'/pending';
    dispatch(fetchCountPending(url));
  }
}

export function fetchCountPending(url){
  return (dispatch) => {
    //console.log('getlob');

      dispatch(itemsIsLoading(true));
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        }).then((response) => {
              if (!response.ok) {
                  //throw Error(response.statusText);
                  Alert.alert(
                    'Error',
                      response.statusText
                  );
              }
				  dispatch(itemsIsLoading(false));
              return response;
          })
          .then((response) => response.json())
          .then((items) => {
			  
             console.log('datalob',items);
            dispatch(setTotalPendingCart(appFunction.newArrayFilter(items)));
          })
          .catch(() => dispatch(itemsHasErrored(true)));
  };
}



/* end count Pending Cart */
/* Delete Cart */

export function setStatusDeleteCart(status) {
    return {
        type: 'SET_STATUS_DELETE_CART',
        status
    };
}

export function doDeleteCart(username, token, order_id){
  return (dispatch) => {
    console.log('getplat', username + ' ' + token + ' '+ order_id);
    var url = appFunction.serviceURL + 'removetransaction/'+ username +'/'+ order_id;
    dispatch(fetchDeleteCart(url, token, username));
  }
}


export function fetchDeleteCart(url, token, username){
  return (dispatch) => {

    dispatch(lobListActions.loadListPendingCart(true));
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'x-auth-token': token,
        },
        }).then((response) => {
              if (response.status !== 200) {
                  //throw Error(response.statusText);
                  Alert.alert(
                    'Error',
                      response.message
                  );
              }
          dispatch(lobListActions.loadListPendingCart(false));
              return response;
          })
          .then((response) => response.json())
          .then((items) => {
			  
            console.log('datalob',items);
            dispatch(setStatusDeleteCart(items.description));
			dispatch(lobListActions.getPendingCart(username));
			//this.forceUpdate();
          })
          .catch(() => dispatch(itemsHasErrored(true)));
  };
}



/* End Delete Cart */

/*set status modal*/


export function setStatusChatModal(status) {
  return {
    type: 'SET_STATUS_CHAT_MODAL',
    status
  };
}

/*end set status modal*/
