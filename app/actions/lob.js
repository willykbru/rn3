import * as service from './service';
import * as lobListActions from './lobList';
import * as appFunction from '../function';
import * as dataKendaraan from './dataKendaraan';
import * as dataProperty from './dataProperty';
import * as dataAccident from './dataAccident';
import * as dataHealth from './dataHealth';
import * as dataLife from './dataLife';
import * as propertyActions from './property';
import * as apartmentActions from './apartment';
import * as accidentActions from './accident';
import * as healthActions from './health';
import * as lifeActions from './life';
import * as delayActions from './delay';
import * as travelActions from './travel';
import { Alert } from 'react-native';


export function setVisible(bool, type){
  return{
    type,
    bool
  }
}




export function setDataSearch(value){
  return{
    type: 'SET_DATA_SEARCH',
    value
  }
}

export function resetDataSearch(){
  return{
    type: 'SET_DATA_SEARCH',
    value : {}
  }
}

export function setLoadDataPendingPayment(bool){
  return{
    type: 'LOADING_DATA_PENDING_PAYMENT',
    bool
  }
}


export function setPlatTengah(value){
  return{
    type : 'SET_PLAT_TENGAH_KENDARAAN',
    value
  }
}


export function setPlatBelakang(value){
  return {
    type : 'SET_PLAT_BELAKANG_KENDARAAN',
    value
  }
}


export function setPlatVisible(bool){
  return (dispatch)=>
  {
    const type = 'SET_VISIBLE_PLAT';
    dispatch(setVisible(bool,type));
  }
}

export function setJenisKendaraan(vehicle_type){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{vehicle_type})
  }
}

export function setSeriKendaraan(seri_tipe){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{seri_tipe})
  }
}

export function setMerekKendaraan(merek){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{merek})
  };
}

export function setKodeVoucher(kode_voucher){
  return{
    type: 'SET_DATA_VOUCHER',
    value: Object.assign({},{kode_voucher})
  };
}
export function resetKodeVoucher(){
	const initialVoucher={
	  kode_voucher: '',
	  voucher_persen: 0,
	  voucher_amount: 0,
	  description: ''
	}
	
  return{
    type: 'SET_DATA_VOUCHER',
    value: initialVoucher
  };
}

export function setKodeTransaksiDataPending(value){
  return{
    type: 'SET_KODE_TRANSAKSI_DATA_PENDING',
    value
  };
}

export function setDataVoucher(dataVoucher){
	var value = Object.assign({}, dataVoucher);
	delete value.kode_voucher;
  return{
    type: 'SET_DATA_VOUCHER',
    value
  };
}
/*
export function setPenggunaanKendaraan(penggunaan = 'Pribadi'){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{penggunaan})
  };
}
*/
export function setBuildYear(build_year){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{build_year:build_year})
  };
}

export function setPlatKendaraan(plat){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{plat})
  }
}

export function setLabelPlatKendaraan(plat){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{plat})
  }
}

export function setLabelNoPlatKendaraan(no_plat){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{no_plat})
  }
}

export function setLabelProvinsiProperty(provinsi_property){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{provinsi_property})
  }
}
export function setLabelDistrictProperty(district_property){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{district_property})
  }
}
export function setLabelKecamatanProperty(kecamatan_property){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{kecamatan_property})
  }
}
export function setLabelKelurahanProperty(kelurahan_property){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{kelurahan_property})
  }
}

export function setLabelTipeProperty(tipe_property){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{tipe_property})
  }
}

export function setLoadingDataTransaksi(bool){
  return{
    type: 'LOADING_DATA_TRANSAKSI',
    bool
  }
}

export function setLabelPekerjaanAccident(pekerjaan_accident){
  return{
    type: 'SET_LABEL_STATE',
    value: Object.assign({},{pekerjaan_accident})
  }
}

export function setLabelCoverageKendaraan(label_coverage){
  return{
    type: 'SET_LABEL_COVERAGE_KENDARAAN',
    value: label_coverage
  }
}

export function setHargaKendaraan(tsi){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{tsi})
  }
}

export function setHargaAksesorisKendaraan(harga_aksesoris){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{harga_aksesoris})
  }
}

export function setAksesorisKendaraan(aksesoris){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{aksesoris})
  }
}

export function setCoverageKendaraan(coverage){
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{coverage})
  }
}

export function setTPLmvValue(tpl_value){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{tpl_value})
  }
}

export function setTPLmvStatus(tpl){
  //console.log('setken',Object.assign({},{tpl}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{tpl})
  }
}

export function setPLLmvValue(pll_value){
  //console.log('setken',Object.assign({},{pll_value}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{pll_value})
  }
}

export function setPLLmvStatus(pll){
  //console.log('setken',Object.assign({},{pll}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{pll})
  }
}

export function setPAPmvValue(pap_value){
  //console.log('setken',Object.assign({},{pap_value}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{pap_value})
  }
}

export function setPAPmvStatus(pap){
  //console.log('setken',Object.assign({},{pap}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{pap})
  }
}

export function setPAPmvAmount(pap_people){
  //console.log('setken',Object.assign({},{pap_people}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{pap_people})
  }
}


export function setPADmvValue(padriver_value){
  ///console.log('setken',Object.assign({},{padriver_value}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{padriver_value})
  }
}

export function setPADmvStatus(padriver){
  //console.log('setken',Object.assign({},{padriver}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{padriver})
  }
}

export function setSRCCmv(srcc){
//  console.log('setken',Object.assign({},{srcc}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{srcc})
  }
}

export function setTSmv(ts){
  //console.log('setken',Object.assign({},{ts}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{ts})
  }
}

export function setEQVETmv(eqvet){
  //console.log('setken',Object.assign({},{eqvet}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{eqvet})
  }
}

export function setTSFWDmv(tsfwd){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{tsfwd})
  }
}

export function setATPMmv(atpm){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},{atpm})
  }
}

export function setAsuransiMV(value){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_ASURANSI_MV',
    value
  }
}

export function setInsuranceIdMV(insurance_id){
  //console.log('setken',Object.assign({},{vehicle_type}));
  return{
    type: 'SET_MV_STATE',
    value: Object.assign({},{insurance_id})
  }
}

export function setBuildingTsiProperty(building_tsi){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{building_tsi})
  }
}

export function setContentTsiProperty(content_tsi){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{content_tsi})
  }
}

export function setProvinsiProperty(province_id){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{province_id})
  }
}

export function setDistrictProperty(district_id){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{district_id})
  }
}

export function setKecamatanProperty(kecamatan_id){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{kecamatan_id})
  }
}

export function setKelurahanProperty(kelurahan_id){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{kelurahan_id})
  }
}

export function setTipeProperty(tipe){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{tipe})
  }
}


export function setRiskAddressProperty(risk_address){
  return{
    type: 'SET_DATA_PROPERTY',
    value: Object.assign({},{risk_address})
  }
}

export function setPackageProperty(package_id){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{package_id})
  }
}

export function setEQVETProperty(eqvet){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{eqvet})
  }
}

export function setFLOODProperty(flood){
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},{flood})
  }
}

export function resetPerluasanProperty(){
  const initialPerluasanProperty = {
    eqvet: false,
    flood: false,
  }
  return{
    type: 'SET_PROPERTY_STATE',
    value: Object.assign({},initialPerluasanProperty)
  }
}

export function setDataTransaksi(value){
  console.log(value);
  return{
    type: 'SET_DATA_TRANSAKSI',
    value
  }
}

export function resetDataTransaksi(){
  return (dispatch) =>{
	dispatch(setDataTransaksi({kode_transaksi: '',link_pembayaran: ''}));
  }
}

export function resetDataVoucher(){
  return (dispatch) =>{
	dispatch(setDataVoucher({kode_voucher: '',voucher_persen: 0,voucher_amount: 0,description: ''}));
  }
}



export function resetPerluasanMV(){
  const initialPerluasanMV = {
    tpl_value: 10000000,
    pll_value: 10000000,
    padriver_value: 10000000,
    pap_value: 10000000,
    pap_people: 1,
    tpl: false,
    pll: false,
    padriver: false,
    pap: false,
    srcc: false,
    ts: false,
    eqvet: false,
    tsfwd: false,
    atpm: false,
  }
  return{
    type: 'SET_PERLUASAN_MV',
    value: Object.assign({},initialPerluasanMV)
  }
}

export function resetMerekKendaraan(){
  return (dispatch) => {
    dispatch(setMerekKendaraan('Pilih'));
  }
}

export function resetSeriKendaraan(){
  return (dispatch) => {
    dispatch(setSeriKendaraan('Pilih'));
  }
}

export function resetCoverageKendaraan(){
  return (dispatch) => {
    dispatch(setCoverageKendaraan('comprehensive'));
    dispatch(setLabelCoverageKendaraan('Comprehensive'));
  }
}

export function resetProvinsiProperty(){
  return (dispatch) => {
    dispatch(setProvinsiProperty('Pilih'));
      dispatch(setLabelProvinsiProperty('Pilih'));
  }
}

export function resetDistrictProperty(){
  return (dispatch) => {
    dispatch(setDistrictProperty('Pilih'));
    dispatch(setLabelDistrictProperty('Pilih'));
  }
}

export function resetKecamatanProperty(){
  return (dispatch) => {
    dispatch(setKecamatanProperty('Pilih'));
    dispatch(setLabelKecamatanProperty('Pilih'));
  }
}

export function resetKelurahanProperty(){
  return (dispatch) => {
    dispatch(setKelurahanProperty('Pilih'));
    dispatch(setLabelKelurahanProperty('Pilih'));
  }
}

export function setMvState(value, state){
  return (dispatch) => {
    if(state == 'vehicle_type'){
      dispatch(setJenisKendaraan(value));
    }else if(state == 'merek'){
      dispatch(setMerekKendaraan(value));
    }else if(state == 'seri_tipe'){
      dispatch(setSeriKendaraan(value));
    }else if(state == 'build_year'){
      //console.log(state);
      dispatch(setBuildYear(value));
    }else if(state == 'plat'){
      //console.log(value);
      dispatch(setPlatKendaraan(value.val));
      dispatch(setLabelPlatKendaraan(value.label));
    }else if(state == 'coverage'){
      dispatch(setCoverageKendaraan(value.val));
      dispatch(setLabelCoverageKendaraan(value.label));
    }else if(state = 'tittle'){
      dispatch(dataKendaraan.setTittleDK(value));
    }
  }
}

export function setPropertyState(value, state){
  return (dispatch) => {
    if(state == 'province_id'){
      dispatch(setProvinsiProperty(value.val));
      dispatch(setLabelProvinsiProperty(value.label));
    }else if(state == 'district_id'){
      dispatch(setDistrictProperty(value.val));
      dispatch(setLabelDistrictProperty(value.label));
    }else if(state == 'kecamatan_id'){
      dispatch(setKecamatanProperty(value.val));
      dispatch(setLabelKecamatanProperty(value.label));
    }else if(state == 'kelurahan_id'){
      dispatch(setKelurahanProperty(value.val));
      dispatch(setLabelKelurahanProperty(value.label));
    }else if(state == 'tipe'){
      //console.log(value);
      dispatch(setTipeProperty(value.val));
      dispatch(setLabelTipeProperty(value.label));
    }else if(state = 'tittle'){
      dispatch(dataProperty.setTittleDP(value));
    }
  }
}

export function setValueState(value, type){
  return (dispatch) => {
    if(type.lob == 'MV'){
      dispatch(setMvState(value, type.state));
    }else if(type.lob == 'Property'){
      dispatch(setPropertyState(value, type.state));
    }else if(type.lob == 'Apartment'){
      dispatch(apartmentActions.setApartmentState(value, type.state));
    }else if(type.lob == 'Accident'){
      dispatch(accidentActions.setAccidentState(value, type.state));
    }else if(type.lob == 'Health'){
      dispatch(healthActions.setHealthState(value, type.state));
    }else if(type.lob == 'Life'){
      dispatch(lifeActions.setLifeState(value, type.state));
    }else if(type.lob == 'Delay'){
      dispatch(delayActions.setDelayState(value, type.state));
    }else if(type.lob == 'Travel'){
      dispatch(travelActions.setTravelState(value, type.state));
    }
  }
}

export function setValuePerluasanMV(value, perluasan){
  //console.log('set perluasanmv',value);
    //console.log('set perluasanmv',perluasan);
  return (dispatch) => {
    if(perluasan == 'TPL'){
      dispatch(setTPLmvValue(value));
    }else if(perluasan == 'PLL'){
      dispatch(setPLLmvValue(value));
    }else if(perluasan == 'PAD'){
      dispatch(setPADmvValue(value));
    }else if(perluasan == 'PAP'){
      dispatch(setPAPmvValue(value));
    }
  }
}

export function setStatusPerluasanMV(status, perluasan){
  return (dispatch) => {
    //console.log('set perluasanmv',status);
    //  console.log('set perluasanmv',perluasan);
    if(perluasan == 'TPL'){
      dispatch(setTPLmvStatus(status));
    }else if(perluasan == 'PLL'){
      dispatch(setPLLmvStatus(status));
    }else if(perluasan == 'PAD'){
      dispatch(setPADmvStatus(status));
    }else if(perluasan == 'PAP'){
      dispatch(setPAPmvStatus(status));
    }else if(perluasan == 'SRCC'){
      dispatch(setSRCCmv(status));
    }else if(perluasan == 'TS'){
      dispatch(setTSmv(status));
    }else if(perluasan == 'TSFWD'){
      dispatch(setTSFWDmv(status));
    }else if(perluasan == 'EQVET'){
      dispatch(setEQVETmv(status));
    }else if(perluasan == 'ATPM'){
      dispatch(setATPMmv(status));
    }
  }
}


export function setAmountPerluasanMV(amount, perluasan){
  return (dispatch) => {
    if(perluasan == 'PAP'){
      dispatch(setPAPmvAmount(amount));
    }
  }
}

export function setValuePerluasan(value, type){
  return (dispatch) => {
    if(type.lob == 'MV'){
      dispatch(setValuePerluasanMV(value, type.perluasan));
    }
  }
}

export function setStatusPerluasan(status, type){
  return (dispatch) => {
    if(type.lob == 'MV'){
      dispatch(setStatusPerluasanMV(status, type.perluasan));
    }
  }
}

export function setAmountPerluasan(amount, type){
  return (dispatch) => {
    if(type.lob == 'MV'){
      dispatch(setAmountPerluasanMV(amount, type.perluasan));
    }
  }
}

export function checkVoucher(data, request){
  return (dispatch) =>{
    if(request == 'checkVoucher'){

            //console.log(body);
            var url = appFunction.serviceURL + 'getvoucher/' + data.insurance_id + '/' + data.kode_voucher + '/' + data.lob + '/' + data.package_id;
            dispatch(fetchGetData(url, request));
    }
  }
}

export function getDataPolis(kode){
  return (dispatch) =>{
    const request = 'getDataPolis';

            //console.log('kd',kode);
            var url = appFunction.serviceURL+ 'getpolicy/' + kode;
            dispatch(fetchGetData(url, request));
  }
}

export function getDataPending(kode){
  return (dispatch) =>{
    const request = 'getDataPending';

            console.log('kd',kode);
            var url = appFunction.serviceURL+ 'detail/' + kode;
            dispatch(fetchGetData(url, request));
  }
}


export function getDetailPolis(kode){
  return (dispatch) =>{
    const request = 'getDetailPolis';

            //console.log('kd',kode);
            var url = appFunction.serviceURL+ 'detailpolicy/' + kode;
            dispatch(fetchGetData(url, request));
  }
}

export function setDataPending(items){
	//console.log('sdp',items);
  return{
    type: 'SET_DATA_PENDING',
    items
  }
}

export function setDataBenefit(value){
  return{
    type: 'SET_DATA_BENEFIT',
    value
  }
}

export function setPackageName(package_name){
	//console.log('sdp',items);
  return{
    type: 'SET_DATA_BENEFIT',
    value: Object.assign({},{package_name})
  }
}

export function setInsuranceId(insurance_id){
	//console.log('sdp',items);
  return{
    type: 'SET_DATA_BENEFIT',
    value: Object.assign({},{insurance_id})
  }
}


export function resetDataBenefit(){
	const initialDataBenefit = {
	  package_name: '',
	  insurance_id: '',
	  insurance_name: '',
	  benefit_url:'',
	}
	//console.log('sdp',items);
  return{
    type: 'RESET_DATA_BENEFIT',
	value: initialDataBenefit
  }
}

export function setDataPolis(items){
	//console.log('sdp',items);
  return{
    type: 'SET_DATA_POLIS',
    items
  }
}

export function setStatusPAGratis(pa_gratis){
	return{
		type: 'SET_STATUS_LOB',
		pa_gratis
		
	}
}

//detail polis
export function setDetailPolis(items){
	//console.log('sdp',items);
  return{
    type: 'SET_DETAIL_POLIS',
    items
  }
}

export function getFetchDataSuccess(items, request){
  return (dispatch) =>{
    if(request == 'checkVoucher'){
      dispatch(setDataVoucher(items));
    }else if(request == 'getDataPending'){
		//console.log('fds',items);
      dispatch(setDataPending(items));
    }else if(request == 'getDataPolis'){
		dispatch(setDataPolis(items));
	}else if(request == 'getDetailPolis'){
	   // console.log('ok'+items.nama);
		dispatch(setDetailPolis(items));
	}
  }
}

export function getFetchPostDataSuccess(items){
  return (dispatch) =>{
    dispatch(setDataTransaksi(items));
  }
}

export function doPostDataMV(body){
  console.log('doPostDataMV');
  return (dispatch) =>{
    var url = appFunction.serviceURL + 'vehicle/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataProperty(body){
  //console.log('doPostDataProperty');
  return (dispatch) =>{
    var url = appFunction.serviceURL + 'properties/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataApartment(body){
 // console.log('doPostDataApartment', body);

  return (dispatch) =>{
    var url = appFunction.serviceURL + 'properties/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataAccident(body){
  //console.log('doPostDataAccident', body);

  return (dispatch) =>{
    var url = appFunction.serviceURL + 'accident/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataHealth(body){
  //console.log('doPostDataHealth', body);

  return (dispatch) =>{
    var url = appFunction.serviceURL + 'health/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataLife(body){
  //console.log('doPostDataHealth', body);

  return (dispatch) =>{
    var url = appFunction.serviceURL + 'life/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataDelay(body){
 // console.log('doPostDataApartment', body);

  return (dispatch) =>{
    var url = appFunction.serviceURL + 'delay/postdata';
    dispatch(fetchPostData(url, body));
  }
}

export function doPostDataTravel(body){
 // console.log('doPostDataApartment', body);

  return (dispatch) =>{
    var url = appFunction.serviceURL + 'travel/postdata';
    dispatch(fetchPostData(url, body));
  }
}


export function fetchGetData(url, request = ''){
  return (dispatch) => {
    //console.log('getlob');
	if(request == 'getDataPending')
	  dispatch(setLoadDataPendingPayment(true));
	else if(request == 'getDataPolis')
		dispatch(lobListActions.loadDataPolis(true));
	else
      dispatch(service.itemsIsLoading(true));
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
        }).then((response) => {
              if (!response.ok) {
                  //throw Error(response.statusText);
                console.log(response);
                  Alert.alert(
                    'Error',
                      response.statusText
                  );
              }
		    if(request == 'getDataPending')
			  dispatch(setLoadDataPendingPayment(false));
			else if(request == 'getDataPolis')
				dispatch(lobListActions.loadDataPolis(false));
			else
              dispatch(service.itemsIsLoading(false));
              return response;
          })
          .then((response) => response.json())
          .then((items) => {
            //console.log(request);
            dispatch(getFetchDataSuccess(items, request));
          })
          .catch(() => dispatch(service.itemsHasErrored(true)));
  };
}


export function fetchPostData(url, body) {
    return (dispatch) => {
          console.log(body);
        dispatch(setLoadingDataTransaksi(true));
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
          }).then((response) => {
                if (!response.ok) {
                  //throw Error(response.statusText);
                  console.log(response);
                    Alert.alert(
                      'Error',
                        response.statusText
                    );
                }

                dispatch(setLoadingDataTransaksi(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => {
              console.log(items);
              dispatch(getFetchPostDataSuccess(items))
            })
        .catch((error) => {dispatch(service.itemsHasErrored(true)); console.log(error);});
    };
}

/* Post Data */

export function setPostData(value){
  console.log('setPostData', value);
  return{
    type: 'SET_POST_DATA',
    value
  }
}
export function resetPostData(value={}){
  console.log('setPostData', value);
  return{
    type: 'SET_POST_DATA',
    value
  }
}

/* End Post Data */
/* Pembayaran */

export function setDataPembayaran(value){
  console.log('setdatapembayaran', value);
  return{
    type: 'SET_DATA_PEMBAYARAN',
    value
  }
}

export function resetDataPembayaran(value = {}){
  console.log('resetdatapembayaran', value);
  return{
    type: 'SET_DATA_PEMBAYARAN',
    value
  }
}

export function doBayarCart(body){
	
  return (dispatch) => {
    var url = appFunction.serviceURL + 'postpayment';
    dispatch(fetchBayarCart(url, body));
  }
	
}

export function fetchBayarCart(url, body) {
    return (dispatch) => {
          console.log(body);
        dispatch(setLoadingDataTransaksi(true));
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
          }).then((response) => {
                if (!response.ok) {
                  //throw Error(response.statusText);
                  console.log(response);
                    Alert.alert(
                      'Error',
                        response.statusText
                    );
                }
				
                dispatch(setLoadingDataTransaksi(false));
                return response;
            })
            .then((response) => response.json())
            .then((items) => {
              console.log(items);
              dispatch(getFetchBayarCartSuccess(items))
            })
            .catch(() => dispatch(service.itemsHasErrored(true)));
    };
}


export function getFetchBayarCartSuccess(items){
  return (dispatch) =>{
    dispatch(setDataPembayaran(items));
  }
}
/* end pembayaran */

