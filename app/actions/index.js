import * as navigationActions from './navigation';
import * as serviceActions from './service';
import * as lobActions from './lob';
import * as lobListActions from './lobList';
import * as propertyActions from './property';
import * as healthActions from './health';
import * as apartmentActions from './apartment';
import * as accidentActions from './accident';
import * as lifeActions from './life';
import * as delayActions from './delay';
import * as travelActions from './travel';
import * as dataKendaraanActions from './dataKendaraan';
import * as dataPropertyActions from './dataProperty';
import * as dataApartmentActions from './dataApartment';
import * as dataAccidentActions from './dataAccident';
import * as dataHealthActions from './dataHealth';
import * as dataLifeActions from './dataLife';
import * as dataDelayActions from './dataDelay';
import * as dataTravelActions from './dataTravel';
import * as renewalActions from './renewal';
import * as claimActions from './claim';
import * as listActions from './list';

export const ActionCreators = Object.assign({},
  navigationActions,
  serviceActions,
  lobActions,
  lobListActions,
  propertyActions,
  apartmentActions,
  accidentActions,
  healthActions,
  lifeActions,
  delayActions,
  travelActions,
  dataKendaraanActions,
  dataPropertyActions,
  dataApartmentActions,
  dataAccidentActions,
  dataHealthActions,
  dataLifeActions,
  dataDelayActions,
  dataTravelActions,
  renewalActions,
  claimActions,
  listActions,
);
