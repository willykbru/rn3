package com.jagain.dev;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.zyu.ReactNativeWheelPickerPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.reactnative.photoview.PhotoViewPackage;
import com.aakashns.reactnativedialogs.ReactNativeDialogsPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage; // <-- Add this line
import io.invertase.firebase.crash.RNFirebaseCrashPackage; // <-- Add this line
import io.invertase.firebase.perf.RNFirebasePerformancePackage; // <-- Add this line
import io.invertase.firebase.auth.RNFirebaseAuthPackage; // <-- Add this line
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.lugg.ReactSnackbar.ReactSnackbarPackage;
import com.rnfs.RNFSPackage;
import org.wonday.pdf.RCTPdfView;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.reactlibrary.PDFViewPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.sudoplz.reactnativeamplitudeanalytics.RNAmplitudeSDKPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.keyee.pdfview.PDFView;
import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.airbnb.android.react.maps.MapsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage; // <-- Add this line


import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new ReactNativeWheelPickerPackage(),
            new ImageResizerPackage(),
            new PhotoViewPackage(),
            new ReactNativeDialogsPackage(),
            new RNFirebasePackage(),
			new RNFirebaseAnalyticsPackage(), // <-- Add this line
			new RNFirebaseCrashPackage(),// <-- Add this line
			new RNFirebasePerformancePackage(), // <-- Add this line
			new RNFirebaseAuthPackage(), // <-- Add this line
          new RNFirebaseCrashlyticsPackage(), // <-- Add this line
			new ReactNativeOneSignalPackage(),
			new ReactSnackbarPackage(),
            new RNFSPackage(),
            new RCTPdfView(),
            new RNFetchBlobPackage(),
            new RNDeviceInfo(),
            new RNAmplitudeSDKPackage(MainApplication.this),
		  new ReactNativePushNotificationPackage(),
		  new PDFViewPackage(),
		  new PDFView(),
		  new RCTSplashScreenPackage(),
      new PickerPackage(),
      new MapsPackage()
      );
    }
	

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
  
}
